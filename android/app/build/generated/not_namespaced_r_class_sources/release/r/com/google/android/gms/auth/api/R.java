/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms.auth.api;

public final class R {
    private R() {}

    public static final class anim {
        private anim() {}

        public static final int fragment_fast_out_extra_slow_in = 0x7f010026;
    }
    public static final class attr {
        private attr() {}

        public static final int alpha = 0x7f04002d;
        public static final int buttonSize = 0x7f040073;
        public static final int circleCrop = 0x7f04009c;
        public static final int colorScheme = 0x7f0400bb;
        public static final int coordinatorLayoutStyle = 0x7f0400dc;
        public static final int elevation = 0x7f04010c;
        public static final int font = 0x7f04013d;
        public static final int fontProviderAuthority = 0x7f04013f;
        public static final int fontProviderCerts = 0x7f040140;
        public static final int fontProviderFetchStrategy = 0x7f040141;
        public static final int fontProviderFetchTimeout = 0x7f040142;
        public static final int fontProviderPackage = 0x7f040143;
        public static final int fontProviderQuery = 0x7f040144;
        public static final int fontProviderSystemFontFamily = 0x7f040145;
        public static final int fontStyle = 0x7f040146;
        public static final int fontVariationSettings = 0x7f040147;
        public static final int fontWeight = 0x7f040148;
        public static final int imageAspectRatio = 0x7f040167;
        public static final int imageAspectRatioAdjust = 0x7f040168;
        public static final int keylines = 0x7f040188;
        public static final int layout_anchor = 0x7f04018d;
        public static final int layout_anchorGravity = 0x7f04018e;
        public static final int layout_behavior = 0x7f04018f;
        public static final int layout_dodgeInsetEdges = 0x7f040192;
        public static final int layout_insetEdge = 0x7f040193;
        public static final int layout_keyline = 0x7f040194;
        public static final int nestedScrollViewStyle = 0x7f0401cc;
        public static final int queryPatterns = 0x7f0401f0;
        public static final int scopeUris = 0x7f040210;
        public static final int shortcutMatchRequired = 0x7f040224;
        public static final int statusBarBackground = 0x7f04024c;
        public static final int ttcIndex = 0x7f0402b5;
    }
    public static final class color {
        private color() {}

        public static final int androidx_core_ripple_material_light = 0x7f06001a;
        public static final int androidx_core_secondary_text_default_material_light = 0x7f06001b;
        public static final int common_google_signin_btn_text_dark = 0x7f060043;
        public static final int common_google_signin_btn_text_dark_default = 0x7f060044;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f060045;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f060046;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f060047;
        public static final int common_google_signin_btn_text_light = 0x7f060048;
        public static final int common_google_signin_btn_text_light_default = 0x7f060049;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f06004a;
        public static final int common_google_signin_btn_text_light_focused = 0x7f06004b;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f06004c;
        public static final int common_google_signin_btn_tint = 0x7f06004d;
        public static final int notification_action_color_filter = 0x7f0600cf;
        public static final int notification_icon_bg_color = 0x7f0600d0;
        public static final int notification_material_background_media_default_color = 0x7f0600d1;
        public static final int primary_text_default_material_dark = 0x7f0600d6;
        public static final int secondary_text_default_material_dark = 0x7f0600dc;
    }
    public static final class dimen {
        private dimen() {}

        public static final int compat_button_inset_horizontal_material = 0x7f070065;
        public static final int compat_button_inset_vertical_material = 0x7f070066;
        public static final int compat_button_padding_horizontal_material = 0x7f070067;
        public static final int compat_button_padding_vertical_material = 0x7f070068;
        public static final int compat_control_corner_material = 0x7f070069;
        public static final int compat_notification_large_icon_max_height = 0x7f07006a;
        public static final int compat_notification_large_icon_max_width = 0x7f07006b;
        public static final int notification_action_icon_size = 0x7f070163;
        public static final int notification_action_text_size = 0x7f070164;
        public static final int notification_big_circle_margin = 0x7f070165;
        public static final int notification_content_margin_start = 0x7f070166;
        public static final int notification_large_icon_height = 0x7f070167;
        public static final int notification_large_icon_width = 0x7f070168;
        public static final int notification_main_column_padding_top = 0x7f070169;
        public static final int notification_media_narrow_margin = 0x7f07016a;
        public static final int notification_right_icon_size = 0x7f07016b;
        public static final int notification_right_side_padding_top = 0x7f07016c;
        public static final int notification_small_icon_background_padding = 0x7f07016d;
        public static final int notification_small_icon_size_as_large = 0x7f07016e;
        public static final int notification_subtext_size = 0x7f07016f;
        public static final int notification_top_pad = 0x7f070170;
        public static final int notification_top_pad_large_text = 0x7f070171;
    }
    public static final class drawable {
        private drawable() {}

        public static final int common_full_open_on_phone = 0x7f08007d;
        public static final int common_google_signin_btn_icon_dark = 0x7f08007e;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f08007f;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f080080;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f080081;
        public static final int common_google_signin_btn_icon_disabled = 0x7f080082;
        public static final int common_google_signin_btn_icon_light = 0x7f080083;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f080084;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f080085;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f080086;
        public static final int common_google_signin_btn_text_dark = 0x7f080087;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f080088;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f080089;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f08008a;
        public static final int common_google_signin_btn_text_disabled = 0x7f08008b;
        public static final int common_google_signin_btn_text_light = 0x7f08008c;
        public static final int common_google_signin_btn_text_light_focused = 0x7f08008d;
        public static final int common_google_signin_btn_text_light_normal = 0x7f08008e;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f08008f;
        public static final int googleg_disabled_color_18 = 0x7f0800e9;
        public static final int googleg_standard_color_18 = 0x7f0800ea;
        public static final int notification_action_background = 0x7f08011d;
        public static final int notification_bg = 0x7f08011e;
        public static final int notification_bg_low = 0x7f08011f;
        public static final int notification_bg_low_normal = 0x7f080120;
        public static final int notification_bg_low_pressed = 0x7f080121;
        public static final int notification_bg_normal = 0x7f080122;
        public static final int notification_bg_normal_pressed = 0x7f080123;
        public static final int notification_icon_background = 0x7f080124;
        public static final int notification_template_icon_bg = 0x7f080125;
        public static final int notification_template_icon_low_bg = 0x7f080126;
        public static final int notification_tile_bg = 0x7f080127;
        public static final int notify_panel_notification_icon_bg = 0x7f080128;
    }
    public static final class id {
        private id() {}

        public static final int accessibility_action_clickable_span = 0x7f0a000a;
        public static final int accessibility_custom_action_0 = 0x7f0a000c;
        public static final int accessibility_custom_action_1 = 0x7f0a000d;
        public static final int accessibility_custom_action_10 = 0x7f0a000e;
        public static final int accessibility_custom_action_11 = 0x7f0a000f;
        public static final int accessibility_custom_action_12 = 0x7f0a0010;
        public static final int accessibility_custom_action_13 = 0x7f0a0011;
        public static final int accessibility_custom_action_14 = 0x7f0a0012;
        public static final int accessibility_custom_action_15 = 0x7f0a0013;
        public static final int accessibility_custom_action_16 = 0x7f0a0014;
        public static final int accessibility_custom_action_17 = 0x7f0a0015;
        public static final int accessibility_custom_action_18 = 0x7f0a0016;
        public static final int accessibility_custom_action_19 = 0x7f0a0017;
        public static final int accessibility_custom_action_2 = 0x7f0a0018;
        public static final int accessibility_custom_action_20 = 0x7f0a0019;
        public static final int accessibility_custom_action_21 = 0x7f0a001a;
        public static final int accessibility_custom_action_22 = 0x7f0a001b;
        public static final int accessibility_custom_action_23 = 0x7f0a001c;
        public static final int accessibility_custom_action_24 = 0x7f0a001d;
        public static final int accessibility_custom_action_25 = 0x7f0a001e;
        public static final int accessibility_custom_action_26 = 0x7f0a001f;
        public static final int accessibility_custom_action_27 = 0x7f0a0020;
        public static final int accessibility_custom_action_28 = 0x7f0a0021;
        public static final int accessibility_custom_action_29 = 0x7f0a0022;
        public static final int accessibility_custom_action_3 = 0x7f0a0023;
        public static final int accessibility_custom_action_30 = 0x7f0a0024;
        public static final int accessibility_custom_action_31 = 0x7f0a0025;
        public static final int accessibility_custom_action_4 = 0x7f0a0026;
        public static final int accessibility_custom_action_5 = 0x7f0a0027;
        public static final int accessibility_custom_action_6 = 0x7f0a0028;
        public static final int accessibility_custom_action_7 = 0x7f0a0029;
        public static final int accessibility_custom_action_8 = 0x7f0a002a;
        public static final int accessibility_custom_action_9 = 0x7f0a002b;
        public static final int action0 = 0x7f0a0031;
        public static final int action_container = 0x7f0a0039;
        public static final int action_divider = 0x7f0a003b;
        public static final int action_image = 0x7f0a003c;
        public static final int action_text = 0x7f0a0042;
        public static final int actions = 0x7f0a0043;
        public static final int adjust_height = 0x7f0a0046;
        public static final int adjust_width = 0x7f0a0047;
        public static final int all = 0x7f0a0049;
        public static final int async = 0x7f0a004b;
        public static final int auto = 0x7f0a004c;
        public static final int blocking = 0x7f0a0050;
        public static final int bottom = 0x7f0a0051;
        public static final int cancel_action = 0x7f0a005a;
        public static final int center = 0x7f0a005d;
        public static final int center_horizontal = 0x7f0a0060;
        public static final int center_vertical = 0x7f0a0061;
        public static final int chronometer = 0x7f0a0066;
        public static final int clip_horizontal = 0x7f0a0068;
        public static final int clip_vertical = 0x7f0a0069;
        public static final int dark = 0x7f0a007e;
        public static final int dialog_button = 0x7f0a0087;
        public static final int end = 0x7f0a008c;
        public static final int end_padder = 0x7f0a008d;
        public static final int fill = 0x7f0a00c4;
        public static final int fill_horizontal = 0x7f0a00c5;
        public static final int fill_vertical = 0x7f0a00c6;
        public static final int forever = 0x7f0a00d4;
        public static final int fragment_container_view_tag = 0x7f0a00d6;
        public static final int icon = 0x7f0a00df;
        public static final int icon_group = 0x7f0a00e0;
        public static final int icon_only = 0x7f0a00e1;
        public static final int info = 0x7f0a00e4;
        public static final int italic = 0x7f0a00e6;
        public static final int left = 0x7f0a00eb;
        public static final int light = 0x7f0a00ec;
        public static final int line1 = 0x7f0a00ed;
        public static final int line3 = 0x7f0a00ee;
        public static final int media_actions = 0x7f0a00f3;
        public static final int media_controller_compat_view_tag = 0x7f0a00f4;
        public static final int none = 0x7f0a0118;
        public static final int normal = 0x7f0a0119;
        public static final int notification_background = 0x7f0a011a;
        public static final int notification_main_column = 0x7f0a011b;
        public static final int notification_main_column_container = 0x7f0a011c;
        public static final int right = 0x7f0a0130;
        public static final int right_icon = 0x7f0a0131;
        public static final int right_side = 0x7f0a0132;
        public static final int standard = 0x7f0a0168;
        public static final int start = 0x7f0a0169;
        public static final int status_bar_latest_event_content = 0x7f0a016a;
        public static final int tag_accessibility_actions = 0x7f0a0170;
        public static final int tag_accessibility_clickable_spans = 0x7f0a0171;
        public static final int tag_accessibility_heading = 0x7f0a0172;
        public static final int tag_accessibility_pane_title = 0x7f0a0173;
        public static final int tag_on_apply_window_listener = 0x7f0a0174;
        public static final int tag_on_receive_content_listener = 0x7f0a0175;
        public static final int tag_on_receive_content_mime_types = 0x7f0a0176;
        public static final int tag_screen_reader_focusable = 0x7f0a0177;
        public static final int tag_state_description = 0x7f0a0178;
        public static final int tag_transition_group = 0x7f0a0179;
        public static final int tag_unhandled_key_event_manager = 0x7f0a017a;
        public static final int tag_unhandled_key_listeners = 0x7f0a017b;
        public static final int tag_window_insets_animation_callback = 0x7f0a017c;
        public static final int text = 0x7f0a017f;
        public static final int text2 = 0x7f0a0180;
        public static final int time = 0x7f0a018b;
        public static final int title = 0x7f0a018c;
        public static final int top = 0x7f0a018f;
        public static final int visible_removing_fragment_view_tag = 0x7f0a01a3;
        public static final int wide = 0x7f0a01a5;
    }
    public static final class integer {
        private integer() {}

        public static final int cancel_button_image_alpha = 0x7f0b0004;
        public static final int google_play_services_version = 0x7f0b000a;
        public static final int status_bar_notification_info_maxnum = 0x7f0b0019;
    }
    public static final class layout {
        private layout() {}

        public static final int custom_dialog = 0x7f0d0023;
        public static final int notification_action = 0x7f0d0063;
        public static final int notification_action_tombstone = 0x7f0d0064;
        public static final int notification_media_action = 0x7f0d0065;
        public static final int notification_media_cancel_action = 0x7f0d0066;
        public static final int notification_template_big_media = 0x7f0d0067;
        public static final int notification_template_big_media_custom = 0x7f0d0068;
        public static final int notification_template_big_media_narrow = 0x7f0d0069;
        public static final int notification_template_big_media_narrow_custom = 0x7f0d006a;
        public static final int notification_template_custom_big = 0x7f0d006b;
        public static final int notification_template_icon_group = 0x7f0d006c;
        public static final int notification_template_lines_media = 0x7f0d006d;
        public static final int notification_template_media = 0x7f0d006e;
        public static final int notification_template_media_custom = 0x7f0d006f;
        public static final int notification_template_part_chronometer = 0x7f0d0070;
        public static final int notification_template_part_time = 0x7f0d0071;
    }
    public static final class string {
        private string() {}

        public static final int common_google_play_services_enable_button = 0x7f110058;
        public static final int common_google_play_services_enable_text = 0x7f110059;
        public static final int common_google_play_services_enable_title = 0x7f11005a;
        public static final int common_google_play_services_install_button = 0x7f11005b;
        public static final int common_google_play_services_install_text = 0x7f11005c;
        public static final int common_google_play_services_install_title = 0x7f11005d;
        public static final int common_google_play_services_notification_channel_name = 0x7f11005e;
        public static final int common_google_play_services_notification_ticker = 0x7f11005f;
        public static final int common_google_play_services_unknown_issue = 0x7f110060;
        public static final int common_google_play_services_unsupported_text = 0x7f110061;
        public static final int common_google_play_services_update_button = 0x7f110062;
        public static final int common_google_play_services_update_text = 0x7f110063;
        public static final int common_google_play_services_update_title = 0x7f110064;
        public static final int common_google_play_services_updating_text = 0x7f110065;
        public static final int common_google_play_services_wear_update_text = 0x7f110066;
        public static final int common_open_on_phone = 0x7f110067;
        public static final int common_signin_button_text = 0x7f110068;
        public static final int common_signin_button_text_long = 0x7f110069;
        public static final int status_bar_notification_info_overflow = 0x7f1100f5;
    }
    public static final class style {
        private style() {}

        public static final int TextAppearance_Compat_Notification = 0x7f12018f;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f120190;
        public static final int TextAppearance_Compat_Notification_Info_Media = 0x7f120191;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f120192;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 0x7f120193;
        public static final int TextAppearance_Compat_Notification_Media = 0x7f120194;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f120195;
        public static final int TextAppearance_Compat_Notification_Time_Media = 0x7f120196;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f120197;
        public static final int TextAppearance_Compat_Notification_Title_Media = 0x7f120198;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f12027e;
        public static final int Widget_Compat_NotificationActionText = 0x7f12027f;
        public static final int Widget_Support_CoordinatorLayout = 0x7f1202e6;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] Capability = { 0x7f0401f0, 0x7f040224 };
        public static final int Capability_queryPatterns = 0;
        public static final int Capability_shortcutMatchRequired = 1;
        public static final int[] ColorStateListItem = { 0x10101a5, 0x101031f, 0x7f04002d };
        public static final int ColorStateListItem_android_color = 0;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_alpha = 2;
        public static final int[] CoordinatorLayout = { 0x7f040188, 0x7f04024c };
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] CoordinatorLayout_Layout = { 0x10100b3, 0x7f04018d, 0x7f04018e, 0x7f04018f, 0x7f040192, 0x7f040193, 0x7f040194 };
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int[] FontFamily = { 0x7f04013f, 0x7f040140, 0x7f040141, 0x7f040142, 0x7f040143, 0x7f040144, 0x7f040145 };
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int FontFamily_fontProviderSystemFontFamily = 6;
        public static final int[] FontFamilyFont = { 0x1010532, 0x1010533, 0x101053f, 0x101056f, 0x1010570, 0x7f04013d, 0x7f040146, 0x7f040147, 0x7f040148, 0x7f0402b5 };
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int[] Fragment = { 0x1010003, 0x10100d0, 0x10100d1 };
        public static final int Fragment_android_name = 0;
        public static final int Fragment_android_id = 1;
        public static final int Fragment_android_tag = 2;
        public static final int[] FragmentContainerView = { 0x1010003, 0x10100d1 };
        public static final int FragmentContainerView_android_name = 0;
        public static final int FragmentContainerView_android_tag = 1;
        public static final int[] GradientColor = { 0x101019d, 0x101019e, 0x10101a1, 0x10101a2, 0x10101a3, 0x10101a4, 0x1010201, 0x101020b, 0x1010510, 0x1010511, 0x1010512, 0x1010513 };
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_type = 2;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int[] GradientColorItem = { 0x10101a5, 0x1010514 };
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int[] LoadingImageView = { 0x7f04009c, 0x7f040167, 0x7f040168 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = { 0x7f040073, 0x7f0400bb, 0x7f040210 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
