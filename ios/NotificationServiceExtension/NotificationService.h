//
//  NotificationService.h
//  NotificationServiceExtension
//
//  Created by Appoids on 03/10/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
