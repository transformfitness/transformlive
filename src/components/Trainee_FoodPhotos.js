import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Text,
    ImageBackground,
    Modal,
    Dimensions,
} from 'react-native';
import { CustomDialog, Loader, NoInternet } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
import LogUtils from '../utils/LogUtils.js';
import AppIntroSlider from 'react-native-app-intro-slider';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_FoodPhotos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAlert: false,
            loading: false,
            alertMsg: '',
            programsArray: [],
            isNoInternet: false,
            sucMsg: '',
            titMsg: '',
            selUpId: 0,
            isPopupVisible: false,
            pageNo: 1,
            pageSize: 20,
            isShowInfoPopup: false,
            selItem: '',
            resObj: {},
            foodPhotos: [],

        };
        this.getFoodPhotos = this.getFoodPhotos.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getFoodPhotos();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async onAccept() {
        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

    }

    async getFoodPhotos() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        // console.log(JSON.stringify({
        //     page: this.state.pageNo,
        //     pagesize: this.state.pageSize,
        //     width: screenWidth,
        // }));
        fetch(
            `${BASE_URL}/trainee/userfoodimglist`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    page: this.state.pageNo,
                    pagesize: this.state.pageSize,
                    width: screenWidth,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ resObj: data });
                    if (data.data.length > 0) {
                        this.setState({ loading: false, resObj: data, programsArray: data.data });
                    }
                    else {
                        this.setState({ loading: false });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    onBackPressed() {
        if (this.props.from === 'rewards' || this.props.from === 'feed') {
            Actions.pop();
        }
        else {
            Actions.popTo('traineeHome');
        }

    }

    renderFoodImages(item) {
        if (item.img_arr.length) {
            let arrProfiles = item.img_arr.map((item1, i) => {
                return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            item.img_arr.map((imgitem) => {
                                this.state.foodPhotos.push(imgitem);
                            })
                            this.setState({ isShowInfoPopup: true, });
                            // this.chooseImage();
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item1.img }}
                            style={{
                                width: wp('16%'),
                                height: wp('16%'),
                                borderRadius: 8,
                                marginLeft: i > 0 ? 10 : 0,
                                alignSelf: 'center',
                                resizeMode: 'cover',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            });
            return (
                <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                    {arrProfiles}
                </View>
            );
        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    renderAllSuscriprionsList() {

        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.programsArray) && this.state.programsArray.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.programsArray}
                            keyExtractor={item => item.umd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                return <View
                                    key={item.img_date}
                                    style={styles.aroundListStyle}>
                                    <Text
                                        numberOfLines={1}
                                        style={styles.textWorkName}>{`${item.img_date}`}</Text>
                                    {this.renderFoodImages(item)}

                                    {item.comments
                                        ?
                                        (
                                            <View style={{ flexDirection: 'column', marginTop: 10, alignItems: 'flex-start' }}>
                                                <Text style={styles.tvCommentsTit}>Dietician Comments :</Text>
                                                <Text style={styles.tvComments}>{`${item.comments}`}</Text>
                                            </View>
                                        )
                                        :
                                        (
                                            <View>
                                            </View>
                                        )}
                                </View>
                            }} />
                    </View>

                );

            }
            else {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                source={require('../res/ic_add_foodinfo.png')}
                                style={styles.imgNoSuscribe}
                            />
                            <Text style={styles.textNodataTitle}>{this.state.resObj.title}</Text>
                            <Text style={styles.textNodata}>{this.state.resObj.message}</Text>
                        </View>
                    </View>

                );
            }

        }
        else {
            return (
                <View></View>
            );
        }
    }

    async onButtonClicked() {
        Actions.myMdlRedsAdd();
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getFoodPhotos();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoInternet}
                        onRetry={this.onRetry.bind(this)} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Food Photos</Text>
                    </View>

                    {this.renderAllSuscriprionsList()}


                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <Modal
                        transparent={true}
                        animationType={'none'}
                        visible={this.state.isShowInfoPopup}
                        onRequestClose={() => {
                            //console.log('close modal');
                        }}>
                        <View style={{
                            backgroundColor: '#ffffff',
                            // position: 'relative',
                            flex: 1,
                            justifyContent: 'center',
                        }}>

                            <View style={{ flex: 1, backgroundColor: '#000000', justifyContent: 'center' }}>
                                <AppIntroSlider
                                    slides={this.state.foodPhotos}
                                    // onDone={this._onDone}
                                    showSkipButton={false}
                                    showDoneButton={false}
                                    showNextButton={false}
                                    dotStyle={{ backgroundColor: '#ffffff' }}
                                    activeDotStyle={{ backgroundColor: '#8c52ff' }}
                                    renderItem={({ item }) => {
                                        return <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.img }}
                                            style={{
                                                width: wp('90%'),
                                                height: hp('80%'),
                                                flex: 1,
                                                alignSelf: 'center',
                                                resizeMode: 'contain'
                                            }}
                                        />
                                            // <FastImage
                                            //     style={{ width: wp('90%'), height: hp('80%'), flex: 1, alignSelf: 'center' }}
                                            //     source={{
                                            //         uri: item.img,
                                            //         headers: { Authorization: '12345' },
                                            //         priority: FastImage.priority.high,
                                            //     }}
                                            //     resizeMode={FastImage.resizeMode.contain}
                                           // />
                                    }}
                                />
                                <TouchableOpacity style={styles.viewTop}
                                    onPress={() => this.setState({ isShowInfoPopup: false, selItem: '', foodPhotos: [] })}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.popBackImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>


                        </View>
                    </Modal>

                </View>
            </ImageBackground >
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        width: wp('95%'),
        backgroundColor: '#ffffff',
        // justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
    },
    textWorkName: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'left',
        lineHeight: 18,
    },
    viewBlueDot: {
        width: 5,
        height: 5,
        backgroundColor: '#6d819c',
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 5 / 2,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    linearGradient1: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    imgNoSuscribe: {
        width: 230,
        height: 230,
        marginBottom: 20,
        alignSelf: 'center',
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 15,
        alignItems: 'flex-start',
        padding: 5,
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        // zIndex: 100,
        top: 1,
    },
    popBackImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#8c52ff',
        alignSelf: 'center',
    },
    tvCommentsTit: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#282c37',
        lineHeight: 15,
    },
    tvComments: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'left',
        marginTop: 3,
        alignSelf: 'flex-start',
        color: '#6d819c',
        lineHeight: 15,
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 15,
        alignItems: 'flex-start',
        padding: 5,
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        // zIndex: 100,
        top: 1,
    },
    popBackImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#8c52ff',
        alignSelf: 'center',
    },
});

export default Trainee_FoodPhotos;
