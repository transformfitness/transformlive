import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, CustomDialog, NoInternet } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { LineChart, Grid, YAxis, XAxis } from 'react-native-svg-charts';
import { Circle } from 'react-native-svg';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
let colors = ['#5ac8fb', '#8c52ff', '#ff5e3a', '#44db5e', '#8b63e6'];
import { allowFunction } from '../utils/ScreenshotUtils.js';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class BodyMeasurements_Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isInternet: false,
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            arrMeasurements: [],
            arrMesHistory: [],
            fill: 0,
            stepArr: [],
            selBody: '',
            selBodyCode: '',
            months_array: [],
            numColumns: 7,
            chartMonths: [],

        };
        this.callService = this.callService.bind(this);
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.callService();

                const interval = setInterval(() => {
                    if (Array.isArray(this.state.arrMeasurements) && this.state.arrMeasurements.length) {
                        var months = [];
                        this.state.arrMeasurements[0].data_arr.map((item) => {
                            months.push(item.name);
                        })
                        this.setState({ chartMonths: months });

                        var weights = [];
                        var selectedArray = [];

                        LogUtils.infoLog1('this.state.resObj.data_arr', this.state.arrMeasurements[0].data_arr);
                        this.state.arrMeasurements[0].data_arr.map((item) => {
                            LogUtils.infoLog1('item.value', item.value);
                            LogUtils.infoLog1('item.name', item.name);
                            weights.push(item.value);
                            selectedArray.push({ 'name': item.name });

                        });

                        var conf = {
                            chart: {
                                type: 'column',
                                height: 150,
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            colors: ['#8c52ff'],
                            xAxis: {
                                visible: false,
                                // categories: [
                                //     '*',
                                //     '',
                                //     '6AM',
                                //     '',
                                //     '',
                                //     '',
                                //     '',
                                //     '12PM',
                                //     '',
                                //     '',
                                //     '',
                                //     '6PM',
                                //     '',
                                //     '',
                                //     '*'
                                // ],
                                crosshair: true,
                                lineWidth: 0,
                                minorGridLineWidth: 0,
                                lineColor: 'transparent',
                            },
                            yAxis: {
                                visible: false,
                                title: false,
                                labels: {
                                    enabled: false,
                                },
                            },
                            tooltip: {
                                // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                // footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.1,
                                    borderWidth: 0,
                                    borderRadius: 3,
                                    showInLegend: false,
                                },
                                allowPointSelect: false,
                            },
                            series: [{
                                enableMouseTracking: false,
                                name: '',
                                data: weights
                                // data: [150, 110, 28, 250, 98, 100, 230]

                            }]

                        }
                        LogUtils.infoLog('weights', weights);
                        LogUtils.infoLog('selectedArray', selectedArray);

                        this.setState({ chartOptions: conf });
                        this.setState({ stepArr: weights, months_array: selectedArray });
                        clearInterval(interval);
                    }
                }, 100);

            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.popTo('bodymeasure');
    }

    async callService() {
        this.setState({ loading: true })
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('token', token);
        fetch(
            `${BASE_URL}/user/bodymesurements`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data.data);
                LogUtils.infoLog1('history', data.history);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data) {
                        data.data.map((item, index) => {
                            if (index === 0) {
                                item.check = true;
                            }
                            else {
                                item.check = false;
                            }
                        })

                        this.setState({ loading: false, arrMeasurements: data.data, arrMesHistory: data.history });
                    }
                    else {
                        this.setState({ loading: false });
                    }

                    this.setState({ selBody: this.state.arrMeasurements[0].name, selBodyCode: this.state.arrMeasurements[0].code })

                    // LogUtils.infoLog1('this.state.resObj.day_arr', this.state.resObj.day_arr);
                    // LogUtils.infoLog1('this.state.resObj.data_arr', this.state.resObj.data_arr);
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: 'Something went wrong please try again later' });
            });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.callService();

                const interval = setInterval(() => {
                    if (Array.isArray(this.state.arrMeasurements) && this.state.arrMeasurements.length) {
                        // this.setState({ fill: this.state.resObj.day_arr })
                        var months = [];
                        this.state.arrMeasurements[0].data_arr.map((item) => {
                            months.push(item.name);
                        })
                        this.setState({ chartMonths: months });

                        var weights = [];
                        var selectedArray = [];

                        LogUtils.infoLog1('this.state.resObj.data_arr', this.state.arrMeasurements[0].data_arr);
                        this.state.arrMeasurements[0].data_arr.map((item) => {
                            LogUtils.infoLog1('item.value', item.value);
                            LogUtils.infoLog1('item.name', item.name);
                            weights.push(item.value);
                            selectedArray.push({ 'name': item.name });

                        })

                        var conf = {
                            chart: {
                                type: 'column',
                                height: 150,
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            colors: ['#8c52ff'],
                            xAxis: {
                                visible: false,
                                // categories: [
                                //     '*',
                                //     '',
                                //     '6AM',
                                //     '',
                                //     '',
                                //     '',
                                //     '',
                                //     '12PM',
                                //     '',
                                //     '',
                                //     '',
                                //     '6PM',
                                //     '',
                                //     '',
                                //     '*'
                                // ],
                                crosshair: true,
                                lineWidth: 0,
                                minorGridLineWidth: 0,
                                lineColor: 'transparent',
                            },
                            yAxis: {
                                visible: false,
                                title: false,
                                labels: {
                                    enabled: false,
                                },
                            },
                            tooltip: {
                                // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                // footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.1,
                                    borderWidth: 0,
                                    borderRadius: 3,
                                    showInLegend: false,
                                },
                                allowPointSelect: false,
                            },
                            series: [{
                                enableMouseTracking: false,
                                name: '',
                                data: weights
                                // data: [150, 110, 28, 250, 98, 100, 230]

                            }]

                        }
                        LogUtils.infoLog('weights', weights);
                        LogUtils.infoLog('selectedArray', selectedArray);

                        this.setState({ chartOptions: conf });
                        this.setState({ stepArr: weights, months_array: selectedArray });
                        // this.setState({ numColumns: selectedArray.length })
                        clearInterval(interval);
                    }
                }, 100);


            }
            else {
                // LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isInternet: true });
            }
        });

    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    jewelStyle = function (height) {
        if (height) {
            height = parseInt(height / 100);
            // LogUtils.infoLog1("height", height);
            var val = height + '%';
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#7064E2', height: val, position: 'absolute', bottom: 0,
            }
        }
        else {
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#ddd', height: 0, position: 'absolute', bottom: 0,
            }
        }

    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }


    renderChartTypesList() {
        if (Array.isArray(this.state.arrMeasurements) && this.state.arrMeasurements.length) {
            return (

                <FlatList
                    horizontal
                    data={this.state.arrMeasurements}
                    // numColumns={5}
                    // extraData={this.state}
                    style={{ marginTop: -15, }}
                    // keyExtractor={item => item.id}
                    keyExtractor={(item, index) => item.id}
                    showsVerticalScrollIndicator={false}
                    // ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item, i }) => {
                        return <View key={i} style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            {item.check
                                ?
                                (
                                    <View style={styles.calViewDetails1}>
                                        <Text style={styles.calDetail1}>{item.name}</Text>
                                    </View>
                                )
                                :
                                (
                                    <TouchableOpacity style={styles.calViewDetails}
                                        onPress={() => {
                                            this.actPress(item);
                                        }}>
                                        <Text style={styles.calDetail}>{item.name}</Text>
                                    </TouchableOpacity>
                                )

                            }
                        </View>
                    }} />
            );
        }
    }

    renderLineGraph() {
        if (Array.isArray(this.state.arrMeasurements) && this.state.arrMeasurements.length) {
            const data = this.state.stepArr;
            const data1 = this.state.chartMonths;
            const axesSvg = { fontSize: 10, fill: 'grey' };
            const verticalContentInset = { top: 10, bottom: 10 };
            const xAxisHeight = 30;

            const Decorator = ({ x, y, data }) => {
                return data.map((value, index) => (
                    <Circle
                        key={index}
                        cx={x(index)}
                        cy={y(value)}
                        r={3}
                        stroke={'#8c52ff'}
                        fill={'#8c52ff'}
                    />
                ))
            }

            return (
                <View style={{ flexDirection: 'column', backgroundColor: '#ffffff' }}>
                    <View style={{ flexDirection: 'row', margin: 15 }}>
                        <YAxis
                            data={data}
                            style={{ marginBottom: xAxisHeight }}
                            contentInset={verticalContentInset}
                            svg={axesSvg}
                            numberOfTicks={3}
                        />
                        <View style={{ flex: 1, marginLeft: 10 }}>
                            <LineChart
                                style={{ height: 200, }}
                                data={data}
                                svg={{ stroke: 'rgb(134, 65, 244)' }}
                                contentInset={{ left: 10, top: 10, bottom: 10, right: 10 }}
                            >
                                <Grid direction={Grid.Direction.HORIZONTAL} />
                                {/* <Line /> */}
                                <Decorator />
                            </LineChart>
                            <XAxis
                                style={{ marginTop: 10, height: xAxisHeight }}
                                data={data1}
                                formatLabel={(value, index) => data1[index]}
                                contentInset={{ left: 13, right: 13 }}
                                svg={axesSvg}
                            />
                        </View>

                    </View >
                    {this.renderChartTypesList()}
                    <View style={{ height: 10, }}></View>
                </View >
            );
        }
        else {
            return (
                <View style={{
                    alignItems: 'center',
                    marginTop: 20,
                    height: 180,
                    alignSelf: 'center',
                }}></View>
            );
        }
    }

    renderDailyMeasurements(item) {
        if (item) {
            let arrProfiles = item.data_arr.map((item, index) => {
                return <View style={{ width: wp('20%'), flexDirection: 'column', alignItems: 'center', alignSelf: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center' }}>
                        <View style={{
                            width: 8,
                            height: 8,
                            borderRadius: 4,
                            alignSelf: 'center',
                            backgroundColor: colors[index % colors.length],
                        }}></View>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 10,
                            fontWeight: '400',
                            color: '#2d3142',
                            marginLeft: 3,
                            letterSpacing: 0.23,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center'

                        }}>{`${item.name}`}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 8, marginBottom: 5, alignItems: 'center', alignSelf: 'center', }}>
                        <Text style={styles.txtAvgVal}>{`${item.value}`}</Text>
                        <Text style={styles.txtMeasurement}>CM</Text>
                    </View>
                </View>
            });
            return (
                <ScrollView horizontal>
                    {arrProfiles}
                </ScrollView>
            );

        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    renderHistoryListItem = ({ item, index }) => {
        return (
            <View style={{ flexDirection: 'column' }} >

                <Text style={{
                    width: wp('100%'),
                    fontSize: 14,
                    fontWeight: '500',
                    fontFamily: 'Rubik-Medium',
                    color: '#282c37',
                    padding: 10,
                }}>{item.date}</Text>

                {this.renderDailyMeasurements(item)}

                <View style={{
                    borderWidth: .3,
                    borderColor: '#ddd',
                    marginTop: 5,
                }}></View>
            </View>

        );
    }

    renderMeasurementsHistory() {
        if (Array.isArray(this.state.arrMesHistory) && this.state.arrMesHistory.length) {
            return (
                <View style={{ flexDirection: 'column', backgroundColor: '#ffffff' }}>
                    <Text style={styles.insighttextStyle}>Daily Body Measurements</Text>
                    <View style={{
                        borderWidth: .3,
                        borderColor: '#ddd',
                    }}></View>
                    <FlatList
                        data={this.state.arrMesHistory}
                        keyExtractor={item => item.code}
                        extraData={this.state}
                        renderItem={this.renderHistoryListItem}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                    />
                </View>

            );
        }
    }   

    actPress = (hey) => {
        var weights = [];
        var selectedArray = [];
        var months = [];
        hey.data_arr.map((item1) => {
            weights.push(item1.value);
            selectedArray.push({ 'name': item1.name });
            months.push(item1.name);
        })
        this.setState({ stepArr: weights, months_array: selectedArray, chartMonths: months });

        this.state.arrMeasurements.map((item) => {
            if (item.code === hey.code) {
                item.check = true;
            }
            else {
                item.check = false;
            }
        });
    }

    renderListItem = ({ item, index }) => {
        return (
            <View style={styles.containeMuscleGain} >
                {item.check
                    ? (
                        <View style={{ flexDirection: 'column', alignItems: 'center', padding: 10, alignSelf: 'center' }}>
                            <Text style={{
                                alignSelf: 'center',
                                fontSize: 12,
                                fontWeight: '400',
                                color: '#8c52ff',
                                letterSpacing: 0.23,
                                fontFamily: 'Rubik-Regular',
                                marginBottom: 10,
                                marginTop: 10,
                                textAlign: 'center'

                            }}>{`${item.name}`}</Text>

                            <Text style={styles.txtAvgVal1}>{`${item.value}`}</Text>
                            <Text style={styles.txtMeasurement1}>CM</Text>
                        </View>
                    )
                    : (
                        <TouchableOpacity
                            onPress={() => {
                                this.actPress(item);
                            }}
                            style={{
                                flexDirection: 'column',
                                alignItems: 'center',
                                padding: 10,
                                alignSelf: 'center'
                            }}>

                            <Text style={{
                                alignSelf: 'center',
                                fontSize: 12,
                                fontWeight: '400',
                                color: '#2d3142',
                                letterSpacing: 0.23,
                                fontFamily: 'Rubik-Regular',
                                marginBottom: 10,
                                marginTop: 10,
                                textAlign: 'center'

                            }}>{`${item.name}`}</Text>

                            <Text style={styles.txtAvgVal}>{`${item.value}`}</Text>
                            <Text style={styles.txtMeasurement}>CM</Text>
                        </TouchableOpacity>
                    )
                }

            </View>

        );
    }

    renderNoData() {
        if (this.state.loading === false) {
            return (
                <View >
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_body_nodata.png')}
                        style={styles.imgUnSuscribe}
                    />
                    <Text style={styles.textCreditCard}>No Body Measurements Found</Text>

                    <Text style={styles.textDesc}>Keep a record of your body measurements to help you track your progress.</Text>

                </View>
            );
        }
    }


    renderAllData() {
        if (this.state.loading === false) {
            if (Array.isArray(this.state.arrMeasurements) && this.state.arrMeasurements.length) {
                return (
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('5%') }}>

                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.textHeadTitle}>Body Measurements</Text>
                        </View>

                        {this.renderLineGraph()}

                        <View style={{ height: 10, }}></View>
                        {this.renderMeasurementsHistory()}
                    </ScrollView>
                )
            }
            else {
                return (
                    <View style={styles.viewTitleCal}>

                        <View style={{ position: 'absolute', zIndex: 111, top: 0, left: 0 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                        {this.renderNoData()}

                    </View>
                )

            }
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <Loader loading={this.state.loading} />
                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                {this.renderAllData()}

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    calViewDet: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 15,
        justifyContent: 'center'
    },
    calTitle: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calViewDetails: {
        // // width: 60,
        borderRadius: 25,
        borderColor: '#9c9eb9',
        // // borderStyle: 'solid',
        borderWidth: 0.5,
        alignSelf: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 10,
        justifyContent: 'center',
        // backgroundColor: '#ffffff',
    },
    calDetail: {
        color: '#9c9eb9',
        fontFamily: 'Rubik-Regular',
        fontSize: 10,
        alignSelf: 'center',
        fontWeight: '500',
    },
    calViewDetails1: {
        // // width: 60,
        borderRadius: 25,
        // borderColor: '#e9e9e9',
        // // borderStyle: 'solid',
        // borderWidth: 1,
        alignSelf: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 10,
        justifyContent: 'center',
        backgroundColor: '#8c52ff',
    },
    calDetail1: {
        color: '#ffffff',
        fontFamily: 'Rubik-Regular',
        fontSize: 10,
        alignSelf: 'center',
        fontWeight: '500',
    },
    container: {
        width: wp('90%'),
        height: 150,
        borderColor: '#000000',
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    insighttextStyle: {
        fontSize: 16,
        // marginTop: 5,
        fontWeight: '500',
        color: '#2d3142',
        padding: 15,
        fontFamily: 'Rubik-Medium',
    },
    containeMuscleGain: {
        width: wp('24.8%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#dddddd',
        borderWidth: .5,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',

    },
    txtAvgVal: {
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
    },
    txtAvgVal1: {
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        color: '#8c52ff',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        marginRight: 3,
    },
    txtMeasurement: {
        alignSelf: 'flex-start',
        fontSize: 9,
        fontWeight: '400',
        color: '#9c9eb9',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        marginTop: 3,
    },
    txtMeasurement1: {
        alignSelf: 'center',
        fontSize: 10,
        fontWeight: '400',
        color: '#8c52ff',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
        marginBottom: 2,
        textAlign: 'center',
        marginRight: 3,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    imgUnSuscribe: {
        width: 220,
        height: 303,
        alignSelf: 'center',
    },
    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,

    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 20,
        // backgroundColor: '#ffffff',
        marginRight: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
});

export default BodyMeasurements_Home;
