import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  BackHandler,
  FlatList,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import Share1 from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

let p_id = 0;

class Trainee_PlanDayIntro extends Component {

  constructor(props) {
    super(props);

    LogUtils.showSlowLog(this);

    this.state = {
      isAlert: false,
      loading: false,
      alertMsg: '',
      resObj: '',
      isSuccess: false,
      sucMsg: '',
      workoutVersion: []
    };
    this.getTraineeProgramEcecs = this.getTraineeProgramEcecs.bind(this);
  }

  async componentDidMount() {
    try {
      allowFunction();
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          LogUtils.infoLog(this.props.pdObj);
          this.setState({ loading: true });
          if (this.props.p_id) {
            p_id = this.props.p_id;
          } else if (this.props.pdObj.pg_id) {
            p_id = this.props.pdObj.pg_id;
          }
          this.getTraineeProgramEcecs();
        }
        else {
          this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
        }
      });
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.onBackPressed();
        return true;
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    Actions.pop();
  }

  async getTraineeProgramEcecs() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/workoutexercise`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            pd_id: this.props.pdObj.pd_id,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this.setState({ loading: false, resObj: data.data,workoutVersion: data.data.workoutVersion});
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message });
            } else {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message });
            }
          }
        })
        .catch(function (error) {
          this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
        });
    } catch (error) {
      console.log(error);
    }
  }

  async onAccept() {
    try {
      if (this.state.alertMsg === 'You are not authenticated!') {
        AsyncStorage.clear().then(() => {
          Actions.auth({ type: 'reset' });
        });
      }
      this.setState({ alertMsg: '' });
      this.setState({ isAlert: false });
    } catch (error) {
      console.log(error);
    }
  }

  shareImage = (img_url, message) => {
    try {

      const fs = RNFetchBlob.fs;
      let imagePath = null;
      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", img_url)
        // the image is now dowloaded to device's storage
        .then(resp => {
          // the image path you can use it directly with Image component
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          // here's base64 encoded image
          //console.log(base64Data);
          Share1.open({
            title: 'Share Via',
            message: message,
            // social: Share1.Social.WHATSAPP,
            url: `data:image/png;base64,${base64Data}`,
            // url: "file://" + img_url,

          })
            .then(res => {
              LogUtils.infoLog(res);
            })
            .catch(err => {
              err && console.log(err);
            });
          // remove the file from storage
          return fs.unlink(imagePath);
        });
    } catch (error) {
      console.log(error);
    }
  };

  renderStartWorkout() {
    try {
      if (this.state.resObj && !isEmpty(this.state.resObj)) {
        return (
          <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
            <TouchableOpacity
              style={styles.buttonTuch}
              onPress={() => {
                LogUtils.firebaseEventLog('video', {
                  p_id: this.state.resObj.p_id,
                  p_category: 'Programs',
                  p_name: `Start - ${this.state.resObj.programname}-${this.props.pdObj.name}-${this.props.pdObj.title}`,
                });
                if (this.state.resObj.can_share === 1) {
                  this.shareImage(this.state.resObj.day_image, this.state.resObj.title);
                } else {
                  if (this.state.resObj.can_play === 1) {
                    //this.state.resObj.programname -- tr_name
                    var tmp = this.state.workoutVersion.filter(x=>x.is_default == 1 && x.title=="Gym Version");
                    if(tmp.length >= 1){
                      Actions.gymVersion({gymVersionObj: this.state.resObj.day_sets_2, pdObj: this.props.pdObj, planObj: this.state.resObj, from: this.props.from, p_id: p_id});
                    }else{
                      Actions.newvideoplay({ pdObj: this.props.pdObj, planObj: this.state.resObj, from: this.props.from, p_id: p_id, label: `End - ${this.state.resObj.tr_name}-${this.props.pdObj.name}-${this.props.pdObj.title}` })
                    }
                  } else {
                    //Actions.myOrders({ from: 'premiumday' });
                  }
                }
                // if (Platform.OS === 'android') {
                //   Actions.proVideoPlay({ pdObj: this.props.pdObj, planObj: this.state.resObj, from: this.props.from, p_id: p_id, label: `End - ${this.state.resObj.programname}-${this.props.pdObj.name}-${this.props.pdObj.title}` })
                // } else {
                //   Actions.videoplay({ pdObj: this.props.pdObj, planObj: this.state.resObj, from: this.props.from, p_id: p_id, label: `End - ${this.state.resObj.programname}-${this.props.pdObj.name}-${this.props.pdObj.title}` })
                // }
              }}>
              {this.state.resObj.can_share === 1
                ? (
                  <Text style={styles.buttonText}>Share to unlock</Text>
                )
                : (
                  <Text style={styles.buttonText}>START WORKOUT </Text>
                )
              }

            </TouchableOpacity>
          </LinearGradient>
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderWorkoutVersion(){
    try {
      if(!isEmpty(this.state.resObj)){
        if(this.state.resObj.workoutVersion.length > 0){
          this.state.resObj.workoutVersion.map(ele=>{
            return (
                <View style={styles.containerListStyle}>
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    alignContent: 'flex-start',
                    padding: 15,
                  }}>
                    {ele.check ?
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/check.png')}
                        style={styles.profileImage}
                      /> :
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/uncheck.png')}
                        style={styles.profileImage}
                      />
                    }
                    <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                      <Text style={styles.textTitle1}>{ele.title}</Text>
                      <Text style={styles.textWorkName}>{ele.subTitle}</Text>
                    </View>
                  </View>
                </View>
            )
          })
        }
      }
      
    } catch (error) {
      console.log(error);
    }
  }

  onClickVersion = (value) => {
    try {
      var temp = this.state.workoutVersion;
      temp.map((item) => {
        if (item.title === value) {
          item.is_default = 1;
        }
        else {
          item.is_default = 0;
        }
      })

      this.setState({ workoutVersion: temp,  });

    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <Loader loading={this.state.loading} />
        <View style={{
          flexDirection: 'row',
          margin: 20,
        }}>
          <View style={{ position: 'absolute', zIndex: 111 }}>
            <TouchableOpacity
              style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
              onPress={() => this.onBackPressed()}>
              <Image
                source={require('../res/ic_back.png')}
                style={styles.backImageStyle}
              />
            </TouchableOpacity>
          </View>

          {this.props.from === 'workouts'
            ? (
              <Text style={styles.textHeadTitle}>{this.state.resObj.title}</Text>
            )
            : (
              <Text style={styles.textHeadTitle}>{this.state.resObj.name}</Text>
            )
          }

          {/* <Text style={styles.textHeadTitle}>{this.state.resObj.name}</Text> */}

        </View>
        <ScrollView>

          <View style={styles.mediaPlayer}>

            {this.state.resObj.day_image
              ? (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={{ uri: this.state.resObj.day_image }}
                  style={styles.thumbNail}
                />
              )
              : (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_noimage.png')}
                  style={styles.thumbNail}
                />
              )
            }

          </View>

          <View style={styles.introDerails}>
            {/* <Text style={styles.textDay}>{this.state.resObj.name}</Text> */}

            <View style={{ flexDirection: 'column', marginTop: 5, marginBottom: 5, alignSelf: 'center' }}>
              <Text style={styles.textValue}>{this.state.resObj.title}</Text>
            </View>

            {/* <View style={{ flexDirection: 'column', marginTop: 2, alignSelf: 'center' }}>
              <Text style={styles.textTitle}>{this.state.resObj.duration} mins . {this.state.resObj.exercise_cnt} exercises</Text>
              <Text style={styles.textTitle}>{this.state.resObj.equipment}</Text>
              <Text style={styles.textTitle}>{this.state.resObj.rest_time}</Text>
            </View> */}

            <View style={styles.containerListStyle}>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                alignContent: 'flex-start',
                padding: 15,
              }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_duration.png')}
                  style={styles.profileImage}
                />
                <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                  <Text style={styles.textTitle1}>Duration</Text>
                  <Text style={styles.textWorkName}>{`${this.state.resObj.duration}`} mins</Text>
                </View>
              </View>
            </View>

            <View style={styles.containerListStyle}>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                alignContent: 'flex-start',
                padding: 15,
              }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_noof_exercises.png')}
                  style={styles.profileImage}
                />
                <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                  <Text style={styles.textTitle1}> No.of Exercises</Text>
                  <Text style={styles.textWorkName}>{`${this.state.resObj.exercise_cnt}`} exercises</Text>
                </View>
              </View>
            </View>

            <View style={styles.containerListStyle}>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                alignContent: 'flex-start',
                padding: 15,
              }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_equipments.png')}
                  style={styles.profileImage}
                />
                <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                  <Text style={styles.textTitle1}>Equipment</Text>
                  <Text style={styles.textWorkName}>{this.state.resObj.equipment}</Text>
                </View>
              </View>
            </View>

            <View style={{ marginTop: 20,marginLeft:15 }}>
              <Text style={[styles.textValue, { alignSelf: 'flex-start' }]}>Please select the workout version</Text>
            </View>

              {/* { this.renderWorkoutVersion()} */}
            {
              (!isEmpty(this.state.resObj)) && (this.state.workoutVersion.length > 0) &&
              this.state.workoutVersion.map(ele => {
                return (
                  <View style={styles.containerListStyle}>
                    <View style={{
                      flexDirection: 'row',
                      alignItems: 'flex-start',
                      alignContent: 'flex-start',
                      padding: 15,
                    }}>
                      {ele.is_default ?
                        (<TouchableOpacity key={ele.title + "check"} onPress={() => {this.onClickVersion(ele.title)}}>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={require('../res/check.png')}
                              style={styles.profileImage}
                            />
                          </TouchableOpacity>)
                        :
                        (<TouchableOpacity key={ele.title + "uncheck"} onPress={() => { this.onClickVersion(ele.title) }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/uncheck.png')}
                            style={styles.profileImage}
                          />
                        </TouchableOpacity>)
                      }
                      <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                        <Text style={styles.textTitle1}>{ele.title}</Text>
                        <Text style={styles.textWorkName}>{ele.sub_title}</Text>
                      </View>
                    </View>
                  </View>
                )
              })
            }

            <View style={{paddingBottom:hp("10%")}}></View>
            

            {/* <View style={styles.containerListStyle}>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                alignContent: 'flex-start',
                padding: 15,
              }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_alarm.png')}
                  style={styles.profileImage1}
                />
                <View style={{ marginLeft: 15, flexDirection: 'column', alignSelf: 'center' }}>
                  <Text style={styles.textTitle1}>Rest Time</Text>
                  <Text style={styles.textWorkName}>{this.state.resObj.rest_time}</Text>
                </View>
              </View>
            </View> */}

          </View>


          {/* <FlatList
            contentContainerStyle={{ paddingBottom: hp('15%') }}
            showsVerticalScrollIndicator={false}
            data={this.state.resObj.Exercise}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            scrollEnabled={false}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({ item }) => {
              return <View style={styles.containerListStyle}>
                <View style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                  alignContent: 'flex-start',
                  padding: 10,
                }}>

                  {item.image
                    ? (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: item.image }}
                        style={styles.profileImage}
                      />
                    )
                    : (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_noimage.png')}
                        style={styles.profileImage}
                      />
                    )
                  }

                  <View style={{ marginLeft: 10, flexDirection: 'column', alignSelf: 'center' }}>
                    <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                    <Text style={styles.textTitle1}>{item.sets} . {item.reps}</Text>
                  </View>
                </View>
              </View>
            }} /> */}
        </ScrollView>
        {this.renderStartWorkout()}
        <CustomDialog
          visible={this.state.isAlert}
          title=''
          desc={this.state.alertMsg}
          onAccept={this.onAccept.bind(this)}
          no=''
          yes='Ok' />
      </KeyboardAvoidingView>
    )
  }

}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    backgroundColor: '#ffffff',
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: "flex-start"
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textHeadTitle: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  introDerails: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
    alignSelf: 'center',
    margin: 10,
  },
  textTitle: {
    fontSize: 10,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    color: '#6d819c',
  },
  textTitle1: {
    fontSize: 10,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
  },
  textValue: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
    alignSelf: 'center'
  },
  mediaPlayer: {
    width: wp('100%'),
    height: hp('30%'),
    position: 'relative',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  thumbNail: {
    width: wp('100%'),
    height: hp('30%'),
    backgroundColor: 'black',
  },
  play: {
    width: 60,
    height: 60,
    alignSelf: "center",
    position: 'absolute',
  },
  containerListStyle: {
    width: wp('92%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 6,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 15,
    marginTop: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'flex-start',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textWorkName: {
    fontSize: 13,
    width: wp('70%'),
    fontWeight: '500',
    marginTop: 1,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  reps: {
    flexDirection: 'row',
    width: wp('75%'),
  },
  buttonText: {
    fontSize: 14,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  linearGradient: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  profileImage: {
    width: 25,
    height: 25,
    resizeMode: 'cover',
  },
  textDay: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#8c52ff',
    alignSelf: 'center'
  },
  mobileImageStyle: {
    width: 25,
    height: 25,
    position: 'relative',
    alignSelf: 'center',
    marginRight: 10
  },
});

export default Trainee_PlanDayIntro;