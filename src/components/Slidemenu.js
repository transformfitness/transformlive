import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    LayoutAnimation,
    TouchableOpacity,
    Animated,
    Dimensions,
} from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const screenHeight = Math.round(Dimensions.get('window').height);
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class Slidemenu extends Component {

    constructor(props) {
        super(props)

        this.state = {
            active: false,
            fadeAnim: new Animated.Value(0),
            shrink: new Animated.Value(0),
        }

        this.setAnimation();
        this.links = [
            'HOME',
            'MY PROFILE',
            'MY SUBSCRIPTIONS',
            'VIEW PLANS',
            // 'APPOINTMENTS',
            'MY PHOTOS',
            'LINK MY TRACKER',
            'SETUP CALORIE COUNTER',
            'NOTIFICATIONS',
            'REFER A FRIEND',
            // 'DIET',
            // 'LIVE CHAT',
            'MY MEDICAL RECORDS',
            'BODY MEASUREMENTS',
            'HELP',
            'SETTINGS',
            'LOGOUT',
        ];

        this.links1 = [
            'HOME',
            'MY PROFILE',
            'MY SUBSCRIPTIONS',
            'VIEW PLANS',
            'APPOINTMENTS',
            'MY PHOTOS',
            'LINK MY TRACKER',
            'SETUP CALORIE COUNTER',
            'NOTIFICATIONS',
            'REFER A FRIEND',
            'MY MEDICAL RECORDS',
            'BODY MEASUREMENTS',
            //'DIET',
            // 'LIVE CHAT',
            'ONE ON ONE CHAT',
            'HELP',
            'SETTINGS',
            'LOGOUT',
        ];

        this.links2 = [
            'HOME',
            'MY PROFILE',
            'MY SUBSCRIPTIONS',
            // 'MY PLANS',
            'APPOINTMENTS',
            'MY PHOTOS',
            'LINK MY TRACKER',
            'SETUP CALORIE COUNTER',
            'NOTIFICATIONS',
            'REFER A FRIEND',
            // 'LIVE CHAT',
            'MY MEDICAL RECORDS',
            'HELP',
            'SETTINGS',
            'LOGOUT',
        ];

        this.links3 = [
            'HOME',
            'MY PROFILE',
            'MY SUBSCRIPTIONS',
            // 'MY PLANS',
            // 'APPOINTMENTS',
            'MY PHOTOS',
            'LINK MY TRACKER',
            'SETUP CALORIE COUNTER',
            'NOTIFICATIONS',
            'REFER A FRIEND',
            'MY MEDICAL RECORDS',
            'DIET',
            // 'LIVE CHAT',
            'HELP',
            'SETTINGS',
            'LOGOUT',
        ];

        this.animatedInStyles = {
            transform: [
                {
                    scale: this.state.shrink.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 2],
                    })
                },
            ],
            flex: 5,
            right: -180,
        }

        this.animatedOutStyles = {
            // transform: [
            //     {
            //         scale: this.state.shrink.interpolate({
            //             inputRange: [0, 1],
            //             outputRange: [1.5, 4],
            //         })
            //     },
            // ],
            // flex: 1,
            // right: 0,
        }
    }


    componentWillUpdate() {
        LayoutAnimation.easeInEaseOut();

        this.setAnimation();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ active: nextProps.active }, this.handleActivateMenuComplete)
    }

    setAnimation() {

        const animationOptions = {
            toValue: -.2,  // Returns to the start
            velocity: 0,   // Velocity makes it move
            tension: 1,   // Slow
            friction: 4,   // Oscillate a lot
        }

        Animated
            .spring(this.state.shrink, animationOptions)
            .start();
    }

    handleActivateMenu() {
        this.props.afterMenuClick()
    }

    handleActivateMenuComplete() {

        const animationOptions = {
            toValue: this.state.active ? 1 : 0,
            duration: 550,
        }

        Animated
            .timing(this.state.fadeAnim, animationOptions)
            .start()
    }

    handleToggleMenu(selectedMenu) {
        //sending state back to parent
        this.props.handleToggleMenu()
        this.props.handleChangeMenu(selectedMenu)
    }

    renderFreeTrial() {
        if (this.props.freeTrialObj.show_free_call === 1) {
            return (
                <TouchableOpacity key={'START 7 DAYS FREE TRIAL'} style={styles.linkWrapper} onPress={() => this.handleToggleMenu('START 7 DAYS FREE TRIAL')} >
                    <Text style={styles.freeTriallink}>{this.props.freeTrialObj.fcc_btn}</Text>
                </TouchableOpacity>
            );
        } else if (this.props.freeTrialObj.show_free_trial === 1) {

            return (
                <TouchableOpacity key={'START 7 DAYS FREE TRIAL'} style={styles.linkWrapper} onPress={() => this.handleToggleMenu('START 7 DAYS FREE TRIAL')} >
                    <Text style={styles.freeTriallink}>{this.props.freeTrialObj.ftc_btn}</Text>
                </TouchableOpacity>

            );

        }else if (this.props.freeTrialObj.show_free_call === 0 && this.props.freeTrialObj.is_paiduser === 0) {

            return (
                <TouchableOpacity key={'EXPLORE PREMIUM FEATURES'} style={styles.linkWrapper} onPress={() => this.handleToggleMenu('EXPLORE PREMIUM FEATURES')} >
                    <Text style={styles.freeTriallink}>{this.props.freeTrialObj.epf_text}</Text>
                </TouchableOpacity>

            );

        }else {
            return (
                <View>

                </View>
            );
        }

    }

    renderMenu() {

        const menuState = this.props.active
            ? styles.menuActive
            : styles.menu

        const last = this.links.length - 1

        if (this.props.isShowAppt === 1) {
            return (
                <View style={menuState}>
                    {/* {this.props.freeTrialObj.show_free_trial
                        ? (
                            <TouchableOpacity key={'START 7 DAYS FREE TRIAL'} style={styles.linkWrapper} onPress={() => this.handleToggleMenu('START 7 DAYS FREE TRIAL')} >
                                <Text style={styles.freeTriallink}>START 7 DAYS FREE TRIAL</Text>
                            </TouchableOpacity>
                        )
                        : (
                            <View></View>
                        )
                    } */}

                    {this.renderFreeTrial()}
                    <Animated.View style={{ opacity: this.state.fadeAnim, }}>
                        {this.links1.map((name, i) =>
                            <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
                                <Text style={styles.link}>{name}</Text>
                            </TouchableOpacity>
                        )}
                    </Animated.View>
                </View>


            )
        }
        else {
            return (
                <View style={menuState}>
                    {/* {this.props.freeTrialObj.show_free_trial
                        ? (
                            <TouchableOpacity key={'START 7 DAYS FREE TRIAL'} style={styles.linkWrapper} onPress={() => this.handleToggleMenu('START 7 DAYS FREE TRIAL')} >
                                <Text style={styles.freeTriallink}>START 7 DAYS FREE TRIAL</Text>
                            </TouchableOpacity>
                        )
                        : (
                            <View></View>
                        )
                    } */}
                    {this.renderFreeTrial()}
                    <Animated.View style={{ opacity: this.state.fadeAnim, }}>
                        {this.links.map((name, i) =>
                            <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
                                <Text style={styles.link}>{name}</Text>
                            </TouchableOpacity>
                        )}
                    </Animated.View>
                </View>
            )
        }

        // if (this.props.isShowDiet === 0 && this.props.isShowAppt === 0) {
        //     return (
        //         <View style={menuState}>
        //             <Animated.View style={{ opacity: this.state.fadeAnim, }}>
        //                 {this.links.map((name, i) =>
        //                     <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
        //                         <Text style={styles.link}>{name}</Text>
        //                     </TouchableOpacity>
        //                 )}
        //             </Animated.View>
        //         </View>
        //     )
        // }
        // else if (this.props.isShowDiet === 1 && this.props.isShowAppt === 1) {
        //     return (
        //         <View style={menuState}>
        //             <Animated.View style={{ opacity: this.state.fadeAnim, }}>
        //                 {this.links1.map((name, i) =>
        //                     <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
        //                         <Text style={styles.link}>{name}</Text>
        //                     </TouchableOpacity>
        //                 )}
        //             </Animated.View>
        //         </View>
        //     )
        // }
        // else if (this.props.isShowDiet === 0 && this.props.isShowAppt === 1) {
        //     return (
        //         <View style={menuState}>
        //             <Animated.View style={{ opacity: this.state.fadeAnim, }}>
        //                 {this.links2.map((name, i) =>
        //                     <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
        //                         <Text style={styles.link}>{name}</Text>
        //                     </TouchableOpacity>
        //                 )}
        //             </Animated.View>
        //         </View>
        //     )
        // }
        // else if (this.props.isShowDiet === 1 && this.props.isShowAppt === 0) {
        //     return (
        //         <View style={menuState}>
        //             <Animated.View style={{ opacity: this.state.fadeAnim, }}>
        //                 {this.links3.map((name, i) =>
        //                     <TouchableOpacity key={name} style={styles.linkWrapper} onPress={() => this.handleToggleMenu(name)} >
        //                         <Text style={styles.link}>{name}</Text>
        //                     </TouchableOpacity>
        //                 )}
        //             </Animated.View>
        //         </View>
        //     )
        // }

    }

    render() {

        const activeMenu = this.props.active
            ? this.animatedInStyles
            : this.animatedOutStyles

        return (
            <View style={styles.outer}>

                {this.renderMenu()}

                <Animated.View style={[styles.outer, activeMenu]}>

                    {this.props.children}

                </Animated.View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    outer: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
    menu: {
        height: '100%',
        width: '55%',
        position: 'absolute',
        margin: 20,
        left: -50
    },
    menuActive: {
        height: '100%',
        width: '55%',
        position: 'absolute',
        margin: 0,
        justifyContent: 'center',
        alignContent: 'center',
        left: 0,
    },
    linkWrapper: {
        paddingLeft: 10
    },
    last: {
        borderBottomWidth: 0,
    },
    link: {
        fontSize: RFValue(12, screenHeight),
        fontWeight: '500',
        paddingTop: 15,
        fontWeight: '500',
        color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    freeTriallink: {
        width: wp('45%'),
        fontSize: RFValue(10, screenHeight),
        fontWeight: '500',
        padding: 10,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#8c52ff',
        // marginRight : 10,
        // marginLeft : 10,
        alignSelf: 'center'
    }

});