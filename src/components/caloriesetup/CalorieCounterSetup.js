import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  BackHandler,
  TextInput,
  FlatList,
  Dimensions
} from 'react-native';
import { Loader, NoInternet, CustomDialog } from '../common';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';
import { ScrollView, withNavigationFocus } from 'react-navigation';
import { allowFunction } from '../../utils/ScreenshotUtils';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { replace, round } from 'lodash';
import { saveCalorieCounterSetupAction } from '../../actions/traineeActions';

const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

class CalorieCounterSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isInternet: false,
      ideal_weight: "",
      goalWeight: "",
      initialWeight: "",
      initialHeight:0,
      initialGender:0,
      initialAge:0,
      goal: "loss",
      totalWeight: 0,
      weeklyWeight: 0,
      isShowAgeNext: true,

      weightGainCalorieDetails: [],
      weightLossCalorieDetails: [],
      goalReachProcessType: [],
      losegainPerWk:[],
      goalDate: [],
      dailyCalorieBudget: [],
      calStatement: "",
      calDetailsObj:[],
      isShowKG: true,

      isAlert: false,
      alertTitle: '',
      alertMsg: '',

      isSuccess: false,
      successMsg:'',

      loading:false,
      calStatementColourCode:'#2e8b57',

      BMI_Message:'',
      BMI_ColorCode:'',

      isEnable:false

    };
  }

  async componentDidMount() {
    try {

      allowFunction();
      StatusBar.setHidden(true);
      //let gender = await AsyncStorage.getItem('genderId');
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          //this.props.getTraineeGoalsByGender({ gender });
        }
        else {
          this.setState({ isInternet: true });
        }
      });

      var weight = await AsyncStorage.getItem('weight_value');
      var height = await AsyncStorage.getItem('height_value');
      var gender = await AsyncStorage.getItem('genderId');
      var age = await AsyncStorage.getItem('age');

      var bmi_ideal = this.props.calCounterSetup.bmi_ideal != undefined ? this.props.calCounterSetup.bmi_ideal : 22.5;
      var ideaWeight = round(bmi_ideal * ((height / 100) * (height / 100)));

      this.setState({
        goalWeight: ideaWeight.toString(),
        initialWeight: weight,
        initialHeight: height,
        initialGender:gender,
        initialAge:age,
        ideal_weight:ideaWeight.toString(),
      })

      this.showGoalTypes();

      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.isFocused) {
          Actions.pop();
        } else {
          this.props.navigation.goBack(null);
        }
        return true;
      });

    } catch (error) {
      console.log("Error in componentDidMount : ", error);
    }
  }

  onBackPressed() {
    Actions.pop();
  }

  showGoalTypes() {
    try {

      var weightGainCalorieDetails = [];
      var weightLossCalorieDetails = [];

      var goalTypes1 = [];
      var goalTypes2 = [];

      this.props.goals.map(goal => {
        if (goal.g_id == 1) {
          var data = {
            id: goal.id,
            g_id: goal.g_id,
            value: goal.title,
            name:goal.name,
            weight_per_week: goal.weight_per_week,
            cals_per_day: goal.cals_per_day,
            days_cnt: goal.days_cnt,
            is_default:goal.is_default,
            is_bad:goal.is_bad,
          }
          goalTypes1.push(data);
          weightLossCalorieDetails.push(data);
        } else if (goal.g_id == 9) {
          var data = {
            id: goal.id,
            g_id: goal.g_id,
            value: goal.title,
            name:goal.name,
            weight_per_week: goal.weight_per_week,
            cals_per_day: goal.cals_per_day,
            days_cnt: goal.days_cnt,
            is_default:goal.is_default,
            is_bad:goal.is_bad,
          }
          goalTypes2.push(data);
          weightGainCalorieDetails.push(data);
        }
      })

      this.setState({
        goalReachProcessType: this.state.goalWeight > this.state.initialWeight ? goalTypes2 : goalTypes1,
        weightGainCalorieDetails: weightGainCalorieDetails,
        weightLossCalorieDetails: weightLossCalorieDetails
      })

      var defVal = this.state.goalReachProcessType.filter(ele=>ele.is_default == 1);
      this.press(defVal[0].value);

    } catch (error) {
      console.log(error);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
        //this.props.getTraineeGoalsByGender({ gender });
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  press = (hey) => {
    try {
      if (this.state.goalWeight != '') {
        if (this.state.goalWeight != this.state.initialWeight) {

          var goalType = this.state.goalReachProcessType;
          goalType.map((item) => {
            if (item.value === hey) {
              item.is_default = 1;
            }
            else {
              item.is_default = 0;
            }
          })

          this.setState({ goalReachProcessType: goalType, isShowKG: true, });
          this.caloriedDeficit();
        }else{
          this.caloriedDeficit();
        }
      } else {
        this.setState({
          calStatement: '',
          isAlert: true, alertMsg: "Please enter target weight."
        })

      }
    } catch (error) {
      console.log(error);
    }
  }

  async caloriedDeficit() {
    try {

      var traineeWeight = parseInt(await AsyncStorage.getItem('weight_value'));
      var targerWeight = parseInt(this.state.goalWeight);
      var weight = 0;
      var perWeek = 0;
      var NoOfDays = 0;
      var GoalDates = [];
      var calBudget = [];
      var KGSperWeek = [];

      var v_bmr = 0;
      var v_cals = 0;
      var v_weight = parseInt(this.state.initialWeight);
      var v_height = parseInt(this.state.initialHeight);
      var v_gender = parseInt(this.state.initialGender);
      var v_age = parseInt(this.state.initialAge);

      actLevel = await AsyncStorage.getItem('activeId');
      var actLevel = this.props.activeLevels.filter(lvl => lvl.id == actLevel);
      var v_multiplier = actLevel[0].multiplier;

      this.state.goalReachProcessType.map((ele,index)=>{
        if(ele.is_default==1){
          this.state.weightGainCalorieDetails[index]["is_default"]=1;
          this.state.weightLossCalorieDetails[index]["is_default"]=1;
        }else{
          this.state.weightGainCalorieDetails[index]["is_default"]=0;
          this.state.weightLossCalorieDetails[index]["is_default"]=0;
        }
      });

      if (v_gender == 1) {//Male
        v_bmr = ((10 * v_weight) + (6.25 * v_height) - (5 * v_age) + 5);
      } else {//Female
        v_bmr = ((10 * v_weight) + (6.25 * v_height) - (5 * v_age) - 161);
      }
      var BMI = round((targerWeight)/((v_height/100)*(v_height/100)),2);
     

      if(BMI > parseInt(this.props.calCounterSetup.bmi_max)){
        this.setState({
          BMI_Message:this.props.calCounterSetup.bmi_max_msg,
          BMI_ColorCode:'#ff0000'
        });
      }else if(BMI < parseInt(this.props.calCounterSetup.bmi_min)){
        this.setState({
          BMI_Message:this.props.calCounterSetup.bmi_min_msg,
          BMI_ColorCode:'#ff0000'
        });
      }else{
        this.setState({
          BMI_Message:'',
          BMI_ColorCode:''
        });
      }

      if (this.state.goalWeight != this.state.initialWeight) {

        if (targerWeight > traineeWeight) { //Weight gain
          var calsCount = this.state.weightGainCalorieDetails.filter(ele=>ele.is_default == 1);
          weight = targerWeight - traineeWeight;
          this.setState({ 
            goal: 'gain', 
            totalWeight: weight, 
            calDetailsObj:calsCount,
          });

          GoalDates = [];
          calBudget = [];
          KGSperWeek = [];

          this.state.weightGainCalorieDetails.forEach(async (ele) => {

            //No.of days calculation
            perWeek = weight / ele.weight_per_week;
            NoOfDays = perWeek * ele.days_cnt;
            var date = new Date();
            date.setDate(date.getDate() + round(NoOfDays));
            var month = date.getMonth();
            var data = date.getDate() + " " + monthNames[month] + " " +  date.getFullYear();
            GoalDates.push(data);

            //Daily calorie budget calculation
            var cals = ele.cals_per_day;
            v_cals = ((v_bmr * v_multiplier) - parseInt(cals));
           
            calBudget.push(round(v_cals));
            KGSperWeek.push(ele.name);

            if (ele.is_default==1) {

              if (this.props.calCounterSetup.wg_cals_check==1) {
                if (v_gender == 1) {//Male
                  this.setState({
                    calStatement: parseInt(this.props.calCounterSetup.mwl_cals) > parseInt(v_cals) ? this.props.calCounterSetup.wg_bad : this.props.calCounterSetup.wg_good,
                    calStatementColourCode: parseInt(this.props.calCounterSetup.mwl_cals) > parseInt(v_cals) ? '#ff0000' : '#2e8b57'
                  })
                } else {//Female
                  this.setState({
                    calStatement: parseInt(this.props.calCounterSetup.fwl_cals) < parseInt(v_cals) ? this.props.calCounterSetup.wg_bad : this.props.calCounterSetup.wg_good,
                    calStatementColourCode: parseInt(this.props.calCounterSetup.fwl_cals) < parseInt(v_cals) ? '#ff0000' : '#2e8b57'
                  });
                }
              } else {
                this.setState({
                  calStatement: ele.is_bad ? this.props.calCounterSetup.wg_bad : this.props.calCounterSetup.wg_good,
                  calStatementColourCode: ele.is_bad ? '#ff0000' : '#2e8b57'
                })
              }
            }

          })
        
        } else {//Weight loss

          weight = traineeWeight - targerWeight;
          var calsCount = this.state.weightLossCalorieDetails.filter(ele=>ele.is_default == 1);
          this.setState({ goal: 'loss',totalWeight: weight,calDetailsObj:calsCount,});
         
          GoalDates = [];
          calBudget = [];
          KGSperWeek = [];
          
          this.state.weightLossCalorieDetails.map(ele => {
            
            //No.of days calculation
            perWeek = weight / ele.weight_per_week;
            NoOfDays = perWeek * ele.days_cnt;
            var date = new Date();
            date.setDate(date.getDate() + round(NoOfDays));
            var month = date.getMonth();
            var gDate = date.getDate() + " " + monthNames[month] + " " +  date.getFullYear();//weekday[wkDay] + ", " +
            GoalDates.push(gDate);
            
            //Daily calorie budget calculation
            v_cals = ((v_bmr * v_multiplier) - parseInt(ele.cals_per_day));
            calBudget.push(round(v_cals));
            KGSperWeek.push(ele.name);

            if (ele.is_default) {

              if (this.props.calCounterSetup.wl_cals_check==1) {
                if (v_gender == 1) {//Male
                  this.setState({
                    calStatement: parseInt(this.props.calCounterSetup.mwl_cals) > parseInt(v_cals) ? this.props.calCounterSetup.wl_bad : this.props.calCounterSetup.wl_good,
                    calStatementColourCode: parseInt(this.props.calCounterSetup.mwl_cals) > parseInt(v_cals) ? '#ff0000' : '#2e8b57'
                  })
                } else {//Female
                  this.setState({
                    calStatement: parseInt(this.props.calCounterSetup.fwl_cals) > parseInt(v_cals) ? this.props.calCounterSetup.wl_bad : this.props.calCounterSetup.wl_good,
                    calStatementColourCode: parseInt(this.props.calCounterSetup.fwl_cals) > parseInt(v_cals) ? '#ff0000' : '#2e8b57'
                  });
                }
              } else {
                this.setState({
                  calStatement: ele.is_bad ? this.props.calCounterSetup.wl_bad : this.props.calCounterSetup.wl_good,
                  calStatementColourCode: ele.is_bad ? '#ff0000' : '#2e8b57'
                })
              }
            }

          })
        }

        this.setState({
          goalDate: GoalDates,
          dailyCalorieBudget: calBudget,
          losegainPerWk:KGSperWeek,
          isEnable:false
        })

      } else if (this.state.goalWeight == this.state.initialWeight) {
  
        var goalType = this.state.goalReachProcessType;
        goalType.forEach((item) => {
          if (item.value === "Maintain weight") {
            item.is_default = 1;
          }
          else {
            item.is_default = 0;
          }
        })
        
        var mw = this.state.goalReachProcessType.filter(ele=>ele.value == 'Maintain weight');
        v_cals = ((v_bmr * v_multiplier) - parseInt(mw[0].cals_per_day));
        calBudget.push(round(v_cals));
        
        this.setState({
          goalDate: [],
          dailyCalorieBudget: calBudget,
          losegainPerWk: [],
          totalWeight: 0,
          //weeklyWeight: "--",
          isShowKG: false,
          calStatement: '',
          goal:'',
          goalReachProcessType: goalType,
          calDetailsObj:mw
        });

        //this.press("Maintain weight");

      }
    } catch (error) {
      console.log(error);
    }
  }

  async onAccept() {
    this.setState({ isAlert: false, alertMsg: '' });
  }


  async saveCalorieCounterSetup() {
    try {
      if( this.state.goalWeight != "" && this.state.goalWeight > 0 ){

        let actLevel = await AsyncStorage.getItem('activeId');
        let token = await AsyncStorage.getItem('token');
        let params = {
          g_id: this.state.calDetailsObj[0].g_id,
          gt_id: this.state.calDetailsObj[0].id,
          target_weight: this.state.goalWeight,
          age:this.state.initialAge,
          gender_id:this.state.initialGender,
          weight:this.state.initialWeight,
          height:this.state.initialHeight,
          al_id:actLevel
        }
 
       // this.setState({loading:true});
        await this.props.saveCalorieCounterSetupAction(token,params).then(res=>{
          if(res.title== "Success"){
            this.setState({
              isSuccess:true,
              successMsg:res.message,
              loading:false
            })
          }else{
            this.setState({
              isAlert:true,
              alertMsg:res.message,
              loading:false
            })
          }
        });
        
      }else{
        this.setState({
          isAlert:true,alertMsg:"Please enter valid target weight"
        })
      }

    } catch (error) {
      console.log("Error in saveCalorieCounterSetup , Error : ", error);
    }
  }


  async onSuccess() {
    this.setState({ isSuccess: false, successMsg: '' });
    Actions.trackershome();
  }

  onChangeTargetWeight(text) {
    try {

      if (text) {
        let newText = '';
        let numbers = '0123456789';
        for (var i = 0; i < text.length; i++) {
          if (numbers.indexOf(text[i]) > -1) {
            newText = newText + text[i];
          }
          else {
            this.setState({ alertMsg: 'Please enter numbers only' });
            this.setState({ isAlert: true });
          }
        }
        if (newText) {
          this.setState({ goalWeight: newText, isShowKG: true });
          // var trainee = this.state.goalReachProcessType.filter(x => x.check == true);trainee[0].value
          this.caloriedDeficit();
        }
      }
      else {
        this.setState({ goalWeight: '' });
      }

    } catch (error) {
      console.log(error);
    }
  }

  renderReactGoalWeight() {
    return (
      <View>
        <View>
          <Text style={styles.desc}>Enter your target weight</Text>
          <Text style={styles.desc1}>( Ideal weight as per your BMI - {this.state.ideal_weight} Kgs )</Text>
        </View>

        <View
        //style={{ backgroundColor: '#eeee', height: 75, justifyContent: 'center',left:10,width:windowWidth-25 }}
        >
          <View style={[styles.containerMobileStyle]}>
            <TextInput
              ref={input => { this.ageTextInput = input; }}
              style={styles.textInputStyle}
              placeholder="Enter your target weight"
              maxLength={3}
              placeholderTextColor="grey"
              keyboardType="numeric"
              returnKeyType='done'
              value={this.state.goalWeight}
              onChangeText={text => this.onChangeTargetWeight(text)}
            />
          </View>
        </View>
        <View>
          {/* <Text style={[styles.desc1,{paddingVertical:5,color: this.state.BMI_ColorCode}]}>{this.state.BMI_Message} </Text> */}
        </View>
      </View>
    )
  }

  renderReachGoalType() {
    return (

      <View>
        <View>
          <Text style={styles.desc}>How soon do you want to achieve your target?</Text>
        </View>
        <View
        //style={{ backgroundColor: '#eeee',left:10,width:windowWidth-25}}
        >
          <FlatList
            contentContainerStyle={{ paddingBottom: hp('1%') }}
            //data={this.props.goals}
            data={this.state.goalReachProcessType}
            keyExtractor={(item,i) => i+"goalReachProcessType"}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({ item,index }) => {
              return (
                <TouchableOpacity key={item.value + "touch"} style={styles.containerMaleStyle} onPress={() => {
                  this.press(item.value)
                }}>
                
                  {
                    item.value != "Maintain weight" ?
                      (
                        <View>
                          <Text style={styles.countryText}>{`${item.value}`}<Text style={{ fontSize: 11, color: '#9c9eb9' }}>
                            { this.state.losegainPerWk.length > 0 ?  ` (${this.state.losegainPerWk[index]})` : "  "}
                          </Text></Text>
                          <Text style={[styles.countryText, { fontSize: 11, color: '#9c9eb9', paddingVertical: 3 }]}>
                            { this.state.losegainPerWk.length > 0 ? `Reach by – ${this.state.goalDate[index]}` : " Reach by –-"}
                          </Text>
                          <Text style={[styles.countryText, { fontSize: 11, color: '#9c9eb9', }]}>
                            { this.state.dailyCalorieBudget.length > 1 ? `Daily Calorie Budget – ${this.state.dailyCalorieBudget[index]} Cals` : ' Daily Calorie Budget –-'}
                          </Text>
                        </View>
                      ) : (
                        <View>
                          <Text style={styles.countryText}>{`${item.value}`}</Text>
                          <Text style={[styles.countryText, { fontSize: 11, color: '#9c9eb9',paddingVertical: 3  }]}>
                            { `Daily Calorie Budget – ${this.state.dailyCalorieBudget[index]}  Cals`}
                          </Text>
                        </View>
                      )
                  }
                 
                  <View style={styles.mobileImageStyle}>
                    {item.is_default
                      ? (
                        <TouchableOpacity key={item.value + "check"}  onPress={() => {
                          this.press(item.value)
                        }}>
                          <Image key={item.value + "check"} source={require('../../res/check.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )
                      : (
                        <TouchableOpacity key={item.value + "uncheck"} onPress={() => {
                          this.press(item.value)
                        }}>
                          <Image key={item.value + "uncheck"} source={require('../../res/uncheck.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )}
                  </View>


               </TouchableOpacity>
              )
            }} />
        </View>
      </View>
    )
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  renderReachGoalPlan() {
    if(this.state.totalWeight != '--'){
      return (
        <View style={[styles.containerGoalPlan, { flexDirection: 'column' }]}>
          <View>
            <Text style={{ textAlign: 'center', fontFamily: 'Rubik-Medium', fontSize: 16, fontWeight: '500', padding: 10, marginTop: 10 }}>YOUR CALORIE INTAKE PLAN</Text>
          </View>
          <View style={{ justifyContent: 'center' }}>
            <Text style={styles.textTitle}>{`Total weight ${this.state.goal} : ${this.state.totalWeight} ${this.state.isShowKG ? 'kg' : ''}`}</Text>
          </View>
          <View style={{ flex: 1, }}>
            {
              this.state.BMI_Message == '' ? (
                <Text style={[styles.textTitle, { padding: 5, color: this.state.calStatementColourCode }]}>{this.state.calStatement}</Text>
                )
                : (
                  <Text style={[styles.textTitle, { padding: 5, color: this.state.BMI_ColorCode }]}>{this.state.BMI_Message} </Text>
                )
            }
          </View>
        </View>
      )
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.containerStyle}
        enableOnAndroid={false}
        onKeyboardDidShow={(frames) => this.setState({ isShowAgeNext: false })}
        onKeyboardDidHide={(frames) => this.setState({ isShowAgeNext: true })}
        scrollEnabled={false}>
        <ImageBackground source={require('../../res/app_bg.png')} style={{ width: '100%', height: '100%' }}>
          <Loader loading={this.state.loading} />
          <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>

            <View style={{
              flexDirection: 'row',
              margin: 20,
            }}>
              <View style={{ position: 'absolute', zIndex: 111 }}>
                <TouchableOpacity
                  style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                  onPress={() => this.onBackPressed()}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../../res/ic_back.png')}
                    style={styles.backImageStyle}
                  />
                </TouchableOpacity>
              </View>

              <Text style={styles.textIndicator}>Calorie counter setup</Text>
            </View>

            {this.renderReactGoalWeight()}

            {this.renderReachGoalType()}

            {this.renderReachGoalPlan()}

            <View style={[{ flexDirection: 'row' }]}>
              {/* <View style={{padding:10,justifyContent:'center'}}>
                <Image source={require('../../res/ic_coolEmoji.png')} style={{width:60,height:60}} />
              </View> */}
              {/* <View style={{ justifyContent: 'center', flex: 1, }}>
                <Text style={styles.subtextOneStyle}>{this.state.calStatement}</Text>
              </View> */}
            </View>


          </ScrollView>
          {
            this.state.isShowAgeNext && 
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                style={styles.buttonTuch}
                onPress={() => {
                  this.saveCalorieCounterSetup();
                }}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </LinearGradient>
          }

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

          <CustomDialog
            visible={this.state.isSuccess}
            title='Alert'
            desc={this.state.successMsg}
            onAccept={this.onSuccess.bind(this)}
            no=''
            yes='Ok' /> 

        </ImageBackground>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  textStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    marginBottom: hp('3%'),
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    //marginTop: 10,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
  },
  linearGradient: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  //target eight
  containericonStyle: {
    // flexDirection: 'column',
    // alignSelf: 'center',
    // justifyContent: 'center',
    //backgroundColor:'#8c52ff',
    padding: 20
  },
  subtextOneStyle: {
    fontSize: 17,
    marginTop: hp('1%'),
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '600',
    color: '#2d3142',
    lineHeight: 20,
    textAlign: 'center',
    marginBottom: hp('2%'),
    fontFamily: 'Rubik-Medium',
    flexWrap:'wrap'
  },
  sliderImageStyle: {
    width: wp('90%'),
    height: hp('5%'),
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  weighttextStyle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#2d3142',
    textAlign: 'left',
    marginLeft: wp('10%'),
    alignSelf: 'flex-start',
    letterSpacing: 0.72,
    fontFamily: 'Rubik-Medium',
  },
  //Goal
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('9%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  mobileImageStyle: {
    width: 25,
    height: 25,
    position: 'relative',
    alignSelf: 'center',
    marginRight: 10
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    //color: '#9c9eb9',
    lineHeight: 24,
    //textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('2%'),
    //marginBottom: hp('2%'),
  },
  desc1: {
    fontSize: 12,
    // marginTop: 10,
    marginLeft: 20,
    // marginRight: 40,
    fontWeight: '400',
    color: '#9c9eb9',
    //lineHeight: 24,
    //textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    //marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  textTitle: {
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    letterSpacing: 0.2,
    color: '#000000',
    //lineHeight: 18,
    paddingVertical: 5,
    textAlign: 'center',
    
  },
  textValue: {
    fontSize: 13,
    fontFamily: 'Rubik-Regular',
    paddingVertical: 5,
    fontWeight: '500',
    color: '#36454F',

  },
  containerGoalPlan: {
    width: wp('90%'),
    //height: hp('36%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    borderColor: '#ddd',
    borderRadius: 30,
    marginLeft: 20,
    marginRight: 15,
    position: 'relative',
    // alignItems: 'center',
    // alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  description: {
    //width: wp('90%'),
    //height: hp('35%'),
    marginTop: hp('1%'),
    //backgroundColor: '#8c52ff',
    // borderColor: '#ddd',
    // borderRadius: 15,
    // marginLeft: 20,
    // marginRight: 15,
    position: 'relative',
    // alignItems: 'center',
    // alignSelf: 'center',
    justifyContent: 'center',
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 20, 
    //   height: 20
    // },
    // shadowOpacity: 1,
    // //shadowRadius: 3.84,
    // elevation: 5,
  },
});

const mapStateToProps = state => {
  const { activeLevels } = state.masters;
  const loading = state.procre.loading;
  const error = state.procre.error;
  const goals = state.traineeFood.goalSubCtgs;
  const calCounterSetup = state.traineeFood.calCounterSetup
  return { loading, error, goals, calCounterSetup,activeLevels };

};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    {saveCalorieCounterSetupAction},
  )(CalorieCounterSetup),
);
