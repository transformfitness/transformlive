import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    StatusBar,
    FlatList,
    ScrollView,
    TextInput,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    getTraineeGoalsByGender,
    getActiveLevels,
    getFoodAllergies,
    getHealthProblems,
    getEquipments,
    getFoodPreferences,
    updateProfileDet,
    profileRefresh,
    getLanguages,
    getProfessions,
    getWorkoutLocations,
    getFitnessForm,
} from '../actions';
import AsyncStorage from '@react-native-community/async-storage';
import Slider from '@react-native-community/slider';
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppIntroSlider from 'react-native-app-intro-slider';

import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isSelected(selArray) {
    let selected = false;
    selArray.map((item) => {
        if (item.check) {
            selected = true;
        }
    })
    return selected;
}

function isMaxFive(selArray) {
    let selected = 0;
    selArray.map((item) => {
        if (item.check) {
            selected = selected + 1;;
        }
    })
    return selected;
}

function isOthersSelected(selArray) {
    let selected = false;
    selArray.map((item) => {
        if (item.id === 99) {
            selected = true;
        }
    })
    return selected;
}

class MyProfile_Update extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: '',
            goals: [],
            actLevels: [],
            allgs: [],
            commonArray: [],
            goalId: 0,
            goalSubId: 0,
            goalCode: '',
            goalName: '',
            activeId: 0,
            isVeg: 1,
            fakeContact: [],
            SelectedFakeContactList: [],
            isAlert: false,
            alertMsg: '',
            userWeight: '',
            languageId: 0,
            languages: [],
            sliderValue: 0,
            targetWeight: 0,
            proffId: 0,
            arrProfessions: [],
            workLocId: 0,
            genderId: 0,
            arrWorkLocs: [],
            othersText: '',
            foodLikes: [],
            foodLikesarray: [],
            SelectedfoodLikesArray: [],
        };
    }

    async componentDidMount() {
        allowFunction();
        StatusBar.setHidden(true);
        this.setState({ languageId: await AsyncStorage.getItem('languageId') });

        if (this.props.from === 'goal') {
            this.props.getTraineeGoalsByGender({ gender: this.props.genderId });
            this.setState({
                goalId: this.props.goalId, goalSubId: this.props.goalSubId,
                goalCode: this.props.goalCode, goalName: this.props.goalName,
                targetWeight: this.props.weight_target, userWeight: this.props.weight,
            });
            const interval = setInterval(() => {
                if (this.props.goals.length > 0) {
                    this.props.goals.map((item) => {
                        if (item.id === this.props.goalId) {
                            item.check = true;
                        }
                    })
                    this.setState({ goals: this.props.goals });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'activity') {
            this.props.getActiveLevels();
            const interval = setInterval(() => {
                if (this.props.activeLevels.length > 0) {
                    this.props.activeLevels.map((item) => {
                        if (item.id === this.props.actLvlId) {
                            item.check = true;
                            this.setState({ activeId: this.props.actLvlId });
                        }
                        else {
                            item.check = false;
                        }
                    })
                    this.setState({ actLevels: this.props.activeLevels });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'alleries') {
            this.setState({ othersText: this.props.otherText });
            this.props.getFoodAllergies();
            const interval = setInterval(() => {
                if (this.props.foodAllergies.length > 0) {
                    this.props.foodAllergies.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ allgs: this.props.foodAllergies });
                    clearInterval(interval);
                }
            }, 100);

        }
        else if (this.props.from === 'healthpro') {

            this.setState({ othersText: this.props.otherText });

            this.props.getHealthProblems();
            const interval = setInterval(() => {
                if (this.props.healthConditions.length > 0) {
                    this.props.healthConditions.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ commonArray: this.props.healthConditions });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'eqpacc') {
            this.props.getEquipments();
            const interval = setInterval(() => {
                if (this.props.equipments.length > 0) {
                    this.props.equipments.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ commonArray: this.props.equipments });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'food') {
            this.props.getFoodPreferences();

            if (this.props.isVeg !== 0) {
                this.setState({ isVeg: this.props.isVeg });
            }


            LogUtils.infoLog1('is veg', this.state.isVeg)
            const interval = setInterval(() => {
                if (this.props.foodPreferences.length > 0) {
                    this.props.foodPreferences.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ commonArray: this.props.foodPreferences });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'language') {
            this.props.getLanguages();
            const interval = setInterval(() => {
                if (this.props.languages.length > 0) {
                    this.props.languages.map((item) => {
                        if (item.id === parseInt(this.state.languageId, 0)) {
                            item.check = true;
                            this.state.SelectedFakeContactList.push(item);
                        }

                    })
                    this.setState({ commonArray: this.props.languages });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'targetwt') {
            this.setState({ sliderValue: this.props.targetwt });
        }
        else if (this.props.from === 'profession') {

            this.setState({ othersText: this.props.otherText });

            this.props.getProfessions();
            const interval = setInterval(() => {
                if (this.props.professions.length > 0) {
                    this.props.professions.map((item) => {
                        if (item.id === this.props.profeId) {
                            this.setState({ proffId: this.props.profeId });
                            item.check = true;
                        }
                        else {
                            item.check = false;
                        }

                    })
                    this.setState({ arrProfessions: this.props.professions });
                    clearInterval(interval);
                }
            }, 100);
        }
        else if (this.props.from === 'workloc') {
            this.props.getWorkoutLocations();

            const interval = setInterval(() => {
                if (this.props.workoutlocations.length > 0) {
                    this.props.workoutlocations.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ commonArray: this.props.workoutlocations });
                    clearInterval(interval);
                }
            }, 100);


        } else if (this.props.from === 'fitnessform') {
            this.props.getFitnessForm();

            const interval = setInterval(() => {
                if (this.props.fitnessform.length > 0) {
                    this.props.fitnessform.map((item) => {
                        this.props.allgIds.map((item1) => {
                            if (item.ff_id === item1.id) {
                                item.check = true;
                                this.state.SelectedFakeContactList.push(item);
                            }
                        })
                    })
                    this.setState({ commonArray: this.props.fitnessform });
                    clearInterval(interval);
                }
            }, 100);


        } else if (this.props.from === 'likedislikes') {
            // this.getFoodLikesDislikes();
            if (this.props.isVeg !== 0) {
                this.setState({ isVeg: this.props.isVeg });
            }
            this.getFoodLikedItems();

           
        }



        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {

            if (this.props.isFocused) {
                Actions.pop();
            } else {
                this.props.navigation.goBack(null);
            }
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onMobileNumberChanged(text) {
        this.props.mobileNumberChanged(text);
    }

    onBackPressed() {
        Actions.pop();
    }

    async getFoodLikedItems() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/foodliked`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_type: this.state.isVeg,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.food_icons.map((item1) => {
                                item1.check = false;
                            });
                        });

                        data.data.map((item) => {
                            item.food_icons.map((item1) => {
                                this.props.likeDislikeIds.map((item2) => {
                                    if (item1.fi_id === item2.fi_id) {
                                        item1.check = true;
                                    }
                                })
                            });

                        })

                        this.setState({ foodLikes: data.data });
                    }
                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getFoodLikesDislikes() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/healthyfood`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {

                            item.like = false;
                            item.dislike = false;
                        });
                        this.setState({ foodLikes: data.data });

                        LogUtils.infoLog1('foodLikes', this.state.foodLikes);
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async saveSubGoalDetails() {
        let token = await AsyncStorage.getItem('token');
        this.setState({ loading: true});
        fetch(
            `${BASE_URL}/user/updategoaldet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    g_id: this.state.goalId,
                    gt_id: 0,
                    target_weight: 0,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    console.log(data,'---data');
                    if (data) {
                        this.setState({ loading: false,isAlert:true,alertMsg:'Your profile has been successfully updated' });
                        //Actions.myProGoalInfo({ goalName: this.state.goalName, resObj: data });
                        Actions.myProfile();

                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async onButtonPressed() {
        if (this.state.goalId !== 0 && isSelected(this.state.goals)) {

            // if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP' || this.state.goalCode === 'RM' || this.state.goalCode === 'WG') {
            //     Actions.myProSubGoal({
            //         goalId: this.state.goalId, goalSubId: this.state.goalSubId,
            //         goalCode: this.state.goalCode, goalName: this.state.goalName,
            //         targetWeight: this.state.targetWeight, weight: this.state.userWeight
            //     })
            // }
            // else {
                this.saveSubGoalDetails();
            //}

        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please select your fitness goal' });
        }
    }

    async changeActLevel() {
        if (this.state.activeId !== 0) {

            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 6, type_data: this.state.activeId, isveg: 0, others: '' });
        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please select your level of activity' });
        }
    }

    async changeProfession() {
        if (this.state.proffId !== 0) {

            if (this.state.proffId === 99) {
                if (this.state.othersText) {
                    let token = await AsyncStorage.getItem('token');
                    this.props.updateProfileDet({ token, type_id: 9, type_data: this.state.proffId, isveg: 0, others: this.state.othersText });
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enter your profession' });
                }

            }
            else {
                let token = await AsyncStorage.getItem('token');
                this.props.updateProfileDet({ token, type_id: 9, type_data: this.state.proffId, isveg: 0, others: '' });
            }
        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please select your profession' });
        }
    }

    async changeWorkingOutLoc() {


        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select your location preference for working out' });
        }
        else {
            let wIds = '';
            this.state.SelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (wIds.length === 0) {
                        wIds = wIds + item.id;
                    }
                    else {
                        wIds = wIds + ',' + item.id;
                    }
                }
            })
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 10, type_data: wIds, isveg: 0, others: '' });
        }
    }


    async changeFitnessForm() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select fitness form of your preference' });
        }
        else {
            let wIds = '';
            this.state.SelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (wIds.length === 0) {
                        wIds = wIds + item.ff_id;
                    }
                    else {
                        wIds = wIds + ',' + item.ff_id;
                    }
                }
            })
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 11, type_data: wIds, isveg: 0, others: '' });
        }
    }

    async updateAllergies() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select any food you are allergic to' });
        }
        else {
            if (isOthersSelected(this.state.SelectedFakeContactList)) {
                if (this.state.othersText) {
                    let wIds = '';
                    this.state.SelectedFakeContactList.map((item) => {
                        if (item.check) {
                            if (wIds.length === 0) {
                                wIds = wIds + item.id;
                            }
                            else {
                                wIds = wIds + ',' + item.id;
                            }
                        }
                    })
                    let token = await AsyncStorage.getItem('token');
                    this.props.updateProfileDet({ token, type_id: 2, type_data: wIds, isveg: 0, others: this.state.othersText });
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enter any food you are allergic to' });
                }
            }
            else {
                let wIds = '';
                this.state.SelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (wIds.length === 0) {
                            wIds = wIds + item.id;
                        }
                        else {
                            wIds = wIds + ',' + item.id;
                        }
                    }
                })
                let token = await AsyncStorage.getItem('token');
                this.props.updateProfileDet({ token, type_id: 2, type_data: wIds, isveg: 0, others: '' });
            }
        }
    }

    async updatehealthProbs() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select any current health problems we should be aware of' });
        }
        else {
            if (isOthersSelected(this.state.SelectedFakeContactList)) {
                if (this.state.othersText) {
                    let wIds = '';
                    this.state.SelectedFakeContactList.map((item) => {
                        if (item.check) {
                            if (wIds.length === 0) {
                                wIds = wIds + item.id;
                            }
                            else {
                                wIds = wIds + ',' + item.id;
                            }
                        }
                    })
                    let token = await AsyncStorage.getItem('token');
                    this.props.updateProfileDet({ token, type_id: 3, type_data: wIds, isveg: 0, others: this.state.othersText });
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enter any current health problems we should be aware of' });
                }

            }
            else {
                let wIds = '';
                this.state.SelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (wIds.length === 0) {
                            wIds = wIds + item.id;
                        }
                        else {
                            wIds = wIds + ',' + item.id;
                        }
                    }
                })
                let token = await AsyncStorage.getItem('token');
                this.props.updateProfileDet({ token, type_id: 3, type_data: wIds, isveg: 0, others: '' });
            }

        }
    }

    async updateEqpAcc() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select the equipment you have access to' });
        }
        else {
            let wIds = '';
            this.state.SelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (wIds.length === 0) {
                        wIds = wIds + item.id;
                    }
                    else {
                        wIds = wIds + ',' + item.id;
                    }
                }
            })
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 4, type_data: wIds, isveg: 0, others: '' });
        }
    }

    async updateFoodPref() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select your cuisine preference' });
        }
        else {
            let wIds = '';
            this.state.SelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (wIds.length === 0) {
                        wIds = wIds + item.id;
                    }
                    else {
                        wIds = wIds + ',' + item.id;
                    }
                }
            })
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 1, type_data: wIds, isveg: this.state.isVeg, others: '' });
        }
    }

    async updateEqpAcc() {
        if (this.state.SelectedFakeContactList.length === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select the equipment you have access to' });
        }
        else {
            let wIds = '';
            this.state.SelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (wIds.length === 0) {
                        wIds = wIds + item.id;
                    }
                    else {
                        wIds = wIds + ',' + item.id;
                    }
                }
            })
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 4, type_data: wIds, isveg: 0, others: '' });
        }
    }

    async updateLanguage() {
        if (this.state.languageId === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please select your preferred language' });
        }
        else {
            let token = await AsyncStorage.getItem('token');
            await AsyncStorage.setItem('languageId', this.state.languageId.toString());
            this.props.updateProfileDet({ token, type_id: 7, type_data: this.state.languageId, isveg: 0, others: '' });
        }
    }
    async updateTargetWeight() {
        if (this.state.sliderValue === 0) {
            this.setState({ isAlert: true, alertMsg: 'Please slide to your target weight' });
        }
        else {
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfileDet({ token, type_id: 8, type_data: this.state.sliderValue, isveg: 0, others: '' });
        }
    }


    async updateFoodLikedDislike() {

        let likeIds = '';
        this.state.foodLikes.map((item) => {
            item.food_icons.map((item1) => {
                if (item1.check) {
                    if (likeIds.length === 0) {
                        likeIds = likeIds + item1.fi_id;
                    }
                    else {
                        likeIds = likeIds + ',' + item1.fi_id;
                    }
                }

            });
        })

        let token = await AsyncStorage.getItem('token');
        this.props.updateProfileDet({ token, type_id: 13, type_data: likeIds, isveg: this.state.isVeg, others: '', });

    }

    renderVegOrNonVeg() {
        LogUtils.infoLog1('is veg1 ', this.state.isVeg);
        if (this.state.isVeg === 1) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>

                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>
                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            );
        } else if (this.state.isVeg === 2) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row', }} >

                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row', }} >

                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            );
        } else if (this.state.isVeg === 3) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>

                        <View style={{ flexDirection: 'row', }}>

                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            );
        }
    }




    renderListAndDet() {
        if (this.props.from === 'goal') {
            return (
                <View style={styles.containericonStyle}>

                    {/* <Text style={styles.subtextOneStyle}>Fitness Goal</Text>
                    <Text style={styles.desc}>Please select your fitness goal</Text> */}

                    <Text style={styles.subtextOneStyle}>Fitness Goal</Text>
                    {/* <Text style={styles.desc}>We need the below information to personalise your experience</Text> */}

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.goals}
                        keyExtractor={(item,index) => "goals"+index}
                        extraData={this.state}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.press(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'activity') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Active level</Text>
                    <Text style={styles.desc}>How active are you?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.actLevels}
                        keyExtractor={item => item.id.toString()}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.actPress(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.actPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.actPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'alleries') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Food Allergies</Text>
                    <Text style={styles.desc}>Please select any food you are allergic to</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.allgs}
                        keyExtractor={(item,index) => "allgs"+index}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerOthersStyle} onPress={() => {
                                this.allgPress(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.allgPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.allgPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>
                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter your food allergies'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.othersText}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ othersText: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'healthpro') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Health Problems</Text>
                    <Text style={styles.desc}>Please select any current health problems we should be aware of</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.commonArray}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerOthersStyle} onPress={() => {
                                this.healthPress(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.healthPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.healthPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>

                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter your health problems'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.othersText}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ othersText: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}

                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'eqpacc') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Equipment Access</Text>
                    <Text style={styles.desc}>Please select the equipment you have access to</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.commonArray}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.eqpAccPress(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.eqpAccPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.eqpAccPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'food') {
            return (
                <ScrollView>
                    <View style={styles.containericonStyle}>

                        <Text style={styles.subtextOneStyle}>Food Preference</Text>
                        <Text style={styles.desc}>Please select your choice of food</Text>

                        {this.renderVegOrNonVeg()}


                        <Text style={styles.desc}>Please select your cuisine preference</Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%'), alignSelf: 'center' }}
                            data={this.state.commonArray}
                            keyExtractor={item => item.id}
                            extraData={this.state}
                            numColumns={3}
                            scrollEnabled={false}
                            style={{ width: wp('100%') }}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.containerFoodStyle} onPress={() => {
                                    this.foodPrefPress(item)
                                }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.img }}
                                            style={styles.imgVeg}
                                        />
                                        <Text style={styles.vegText}>{`${item.name}`} </Text>

                                        {item.check
                                            ? (
                                                <View style={{
                                                    width: 20,
                                                    height: 20,
                                                    position: 'absolute',
                                                    alignSelf: 'flex-end',
                                                    margin: 4, top: 0, right: 1,
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </View>
                                            )
                                            : (
                                                <View style={{
                                                    width: 15,
                                                    height: 15,
                                                    position: 'absolute',
                                                    alignSelf: 'flex-end',
                                                    margin: 4,
                                                    top: 0,
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </View>
                                            )}

                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>
                </ScrollView>
            );
        }
        else if (this.props.from === 'language') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Preferred Language</Text>
                    <Text style={styles.desc}>You always can change this later</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.commonArray}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.languagePress(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.languagePress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.languagePress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'targetwt') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Your Target Weight</Text>
                    <Text style={styles.desc}>You always can change this later</Text>

                    {this.renderWeightValue()}

                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={150}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                </View>
            );

        }
        else if (this.props.from === 'profession') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Profession</Text>
                    <Text style={styles.desc}>What is your profession?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.arrProfessions}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerOthersStyle} onPress={() => {
                                this.professionPress(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.professionPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.professionPress(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>

                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder="Enter other profession"
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.othersText}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ othersText: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}



                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'workloc') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Workout Location</Text>
                    <Text style={styles.desc}>What is your location preference for working out?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.commonArray}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.workoutLocPress(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.workoutLocPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.workoutLocPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        } else if (this.props.from === 'fitnessform') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Fitness form</Text>
                    <Text style={styles.desc}>Please select fitness form of your preference</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.commonArray}
                        keyExtractor={(item,index) =>"commonArray"+index}
                        extraData={this.state}
                        style={{ width: wp('100%') }}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.fitnessFormPress(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.fitnessFormPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.fitnessFormPress(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.props.from === 'likedislikes') {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Food Likes</Text>
                    <Text style={styles.desc}>Please select any five foods which you like from below</Text>

                    <AppIntroSlider
                        slides={this.state.foodLikes}
                        // onDone={this._onDone}
                        // showSkipButton={false}
                        showPrevButton={false}
                        showDoneButton={false}
                        showNextButton={false}
                        // renderDoneButton={this._renderDoneButton}
                        dotStyle={{ backgroundColor: '#2d3142', elevation: 2, width: 8, height: 8, borderRadius: 4 }}
                        activeDotStyle={{ backgroundColor: '#8c52ff', elevation: 5, width: 10, height: 10, borderRadius: 5 }}
                        paginationStyle={{ top: -25, height: 20 }}
                        renderItem={({ item }) => {
                            return <View key={item.fim_id} style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row', marginLeft: 10, marginRight: 10, marginTop: 10 }}>
                                    <Text style={styles.txtLikeFoodTit}>{item.name}</Text>
                                </View>

                                <FlatList
                                    //    data={formatData(data, numColumns)}
                                    contentContainerStyle={{ paddingBottom: hp('35%') }}
                                    style={{ marginTop: 10, marginLeft: 5, marginRight: 5, }}
                                    data={item.food_icons}
                                    numColumns={3}
                                    extraData={this.state}
                                    renderItem={this.renderListItem}
                                    keyExtractor={this.keyExtractor}
                                    showsVerticalScrollIndicator={false}
                                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                                />
                            </View>
                        }}
                    />

                  
                </View>
            );
        }
    }

    keyExtractor = (item) => {
        return item.fi_id;
    }

    renderListItem = ({ item }) => {
        return (

            <TouchableOpacity key={item.fi_id} style={styles.containerLikeStyle} onPress={() => {
                this.pressFoodLike(item)
            }}>
                <View
                    style={styles.item}>

                    {item.img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.img }}
                                style={{ width: 100, height: 100, alignSelf: 'center', }} />
                        )
                        : (

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{ width: 100, height: 100, borderRadius: 50, alignSelf: 'center', }} />

                        )}

                    <Text
                        numberOfLines={2}
                        style={styles.vegText}>{`${item.name}`}</Text>

                    {item.check
                        ? (
                            <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }}
                                onPress={() => {
                                    this.pressFoodLike(item)
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_box_check.png')}
                                    style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                            </TouchableOpacity>
                        )
                        : (
                            <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, }}
                                onPress={() => {
                                    this.pressFoodLike(item)
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_box_uncheck.png')}
                                    style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                            </TouchableOpacity>
                        )}
                </View>
            </TouchableOpacity>
        )
    }

    renderWeightValue() {
        return (
            <View style={styles.personViewStyle}>

                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_person_height.png')}
                    style={{
                        alignSelf: 'flex-end',
                        width: 65,
                        height: 220,
                        marginTop: 20,
                    }}>

                </Image>

                <Text style={styles.weighttextStyle}>
                    {Math.floor(this.state.sliderValue)} Kg
                </Text>

            </View>
        );

    }

    renderButton() {
        if (this.props.from === 'goal') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onButtonPressed()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'activity') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.changeActLevel()}>
                            <Text style={styles.buttonText}>Change Your Active Level</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'alleries') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateAllergies()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'healthpro') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updatehealthProbs()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'eqpacc') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateEqpAcc()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'food') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateFoodPref()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'language') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateLanguage()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'targetwt') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateTargetWeight()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'profession') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.changeProfession()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'workloc') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.changeWorkingOutLoc()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'fitnessform') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.changeFitnessForm()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (this.props.from === 'likedislikes') {
            return (
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.updateFoodLikedDislike()}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.props.profileRefresh();
        Actions.myProfile();

    }

    removeNone() {
        this.state.SelectedFakeContactList.map((item) => {
            if (item.check) {

            }
            else {
                const i = this.state.SelectedFakeContactList.indexOf(item)
                if (1 != -1) {
                    this.state.SelectedFakeContactList.splice(i, 1)
                    return this.state.SelectedFakeContactList
                }
            }
        })
    }

    onClearAllergiesArray(hey) {
        const interval = setInterval(() => {
            this.setState({ SelectedFakeContactList: [] });
            LogUtils.infoLog(this.state.SelectedFakeContactList);
            this.state.allgs.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check;
                    if (item.check === true) {
                        this.state.SelectedFakeContactList.push(item);
                    }
                }
                else {
                    item.check = false;
                }
            });
            this.setState({ fakeContact: this.state.allgs });
            clearInterval(interval);
        }, 100);
    };

    onClearCommonArray(hey) {
        const interval = setInterval(() => {
            this.setState({ SelectedFakeContactList: [] });
            this.state.commonArray.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check;
                    if (item.check === true) {
                        this.state.SelectedFakeContactList.push(item);
                    }
                    LogUtils.infoLog(this.state.SelectedFakeContactList);
                }
                else {
                    item.check = false;
                }
            });
            this.setState({ fakeContact: this.state.commonArray });
            clearInterval(interval);
        }, 100);
    };



    render() {
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                scrollEnabled={false}>
                {/* <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled> */}
                <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>

                    <Loader loading={this.props.loading} />

                    <TouchableOpacity
                        onPress={() => this.onBackPressed()}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>

                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_app.png')}
                        style={styles.appImageStyle}
                    />


                    {this.renderListAndDet()}

                    {this.renderButton()}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.props.isSuccess}
                        title='Congratulations ! '
                        desc={this.props.sucMsg}
                        onAccept={this.onSuccess.bind(this)}
                        no=''
                        yes='Ok' />
                </ImageBackground>
            </KeyboardAwareScrollView>
        );
    }

    press = (hey) => {
        this.state.goals.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalId: item.id, goalCode: item.code, goalName: item.name });
            }
            else {
                item.check = false;
            }
        })
    }

    actPress = (hey) => {
        this.state.actLevels.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ activeId: item.id });
            }
            else {
                item.check = false;
            }
        });
    }

    professionPress = (hey) => {
        this.state.arrProfessions.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ proffId: item.id });
            }
            else {
                item.check = false;
            }
        });
    }

    workoutLocPress = (hey) => {
        this.state.commonArray.map((item) => {
            if (item.id === hey.id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.SelectedFakeContactList.push(item);
                } else if (item.check === false) {
                    const i = this.state.SelectedFakeContactList.indexOf(item)
                    if (1 != -1) {
                        this.state.SelectedFakeContactList.splice(i, 1)
                        return this.state.SelectedFakeContactList
                    }
                }
            }
        })
        this.setState({ fakeContact: this.state.commonArray })
    }

    fitnessFormPress = (hey) => {

        this.state.commonArray.map((item) => {
            if (item.ff_id === hey.ff_id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.SelectedFakeContactList.push(item);
                } else if (item.check === false) {
                    const i = this.state.SelectedFakeContactList.indexOf(item)
                    if (1 != -1) {
                        this.state.SelectedFakeContactList.splice(i, 1)
                        return this.state.SelectedFakeContactList
                    }
                }
            }
        })
        this.setState({ fakeContact: this.state.commonArray })
    }

    allgPress = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearAllergiesArray(hey);
        }
        else {
            this.state.allgs.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.SelectedFakeContactList.push(item);
                    } else if (item.check === false) {
                        const i = this.state.SelectedFakeContactList.indexOf(item)
                        if (1 != -1) {
                            this.state.SelectedFakeContactList.splice(i, 1)
                            return this.state.SelectedFakeContactList
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })
            this.removeNone();
            this.setState({ fakeContact: this.state.allgs })
        }

    }

    healthPress = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearCommonArray(hey);
        }
        else {
            this.state.commonArray.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.SelectedFakeContactList.push(item);
                    } else if (item.check === false) {
                        const i = this.state.SelectedFakeContactList.indexOf(item)
                        if (1 != -1) {
                            this.state.SelectedFakeContactList.splice(i, 1)
                            return this.state.SelectedFakeContactList
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })
            this.removeNone();
            this.setState({ fakeContact: this.state.commonArray })
        }

    }

    eqpAccPress = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearCommonArray(hey);
        }
        else {
            this.state.commonArray.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.SelectedFakeContactList.push(item);
                    } else if (item.check === false) {
                        const i = this.state.SelectedFakeContactList.indexOf(item)
                        if (1 != -1) {
                            this.state.SelectedFakeContactList.splice(i, 1)
                            return this.state.SelectedFakeContactList
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })
            this.removeNone();
            this.setState({ fakeContact: this.state.commonArray })
        }

    }

    foodPrefPress = (hey) => {
        this.state.commonArray.map((item) => {
            if (item.id === hey.id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.SelectedFakeContactList.push(item);
                } else if (item.check === false) {
                    const i = this.state.SelectedFakeContactList.indexOf(item)
                    if (1 != -1) {
                        this.state.SelectedFakeContactList.splice(i, 1)
                        return this.state.SelectedFakeContactList
                    }
                }
            }
        })
        this.setState({ fakeContact: this.state.commonArray })
    }

    pressFoodLike = (hey) => {
        let newArray = this.state.foodLikes.slice();
        newArray.map((item) => {
            item.food_icons.map((item1) => {
                if (item1.fi_id === hey.fi_id) {
                    if (item1.check) {
                        item1.check = !item1.check
                    }
                    else {
                        if (isMaxFive(item.food_icons) >= 5) {
                            this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'You can select max five foods from each section ' });
                        }
                        else {
                            item1.check = !item1.check
                        }
                    }
                }
            });
        })
        this.setState({ foodLikes: newArray })
    }

    pressFoodDisLike = (hey) => {
        this.state.foodLikes.map((item) => {
            if (item.f_id === hey.f_id) {
                item.dislike = !item.dislike

                if (item.dislike === true) {
                    item.like = false;
                }
            }
        })
        this.setState({ foodLikesarray: this.state.foodLikes })
    }

    languagePress = (hey) => {
        this.state.commonArray.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ languageId: item.id });
            }
            else {
                item.check = false;
            }
        })
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    img: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    back: {
        width: 25,
        height: 18,
        marginTop: 45,
        marginLeft: 20,
        position: 'absolute',
        alignSelf: 'flex-start'
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        marginLeft: 15,
        marginRight: 15,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerOthersStyle: {
        width: wp('90%'),
        // height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        marginLeft: 15,
        marginRight: 15,
        padding: 25,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerFoodStyle: {
        width: wp('28%'),
        marginTop: hp('2%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        margin: 5,
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    desc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('2%'),
        marginBottom: hp('1%'),
    },
    mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginBottom: 15,
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('7%'),
        marginLeft: 20,
        marginBottom: 20,
        alignSelf: 'flex-start',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: -45,
        marginBottom: 20,
        alignSelf: 'center',
    },
    viewBottom: {
        width: wp('90%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
    },
    containerIsVeg: {
        width: wp('90%'),
        flexDirection: 'column',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
    },
    containerVegStyle: {
        width: wp('90%'),
        height: 50,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    imgVeg: { width: wp('28%'), height: 100, alignSelf: 'center', position: 'relative', borderTopLeftRadius: 10, borderTopRightRadius: 10 },
    vegText: {
        alignSelf: 'center',
        fontSize: 12,
        fontWeight: '600',
        flex: 1,
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        padding: 7,
    },
    sliderImageStyle: {
        width: wp('75%'),
        height: hp('15%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    personViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    weighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        position: 'absolute',
        bottom: 0,
        left: wp('12.5%'),
    },
    vegSelect: { width: 20, height: 20, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 10 },
    tiViewBack: {
        // width: wp('80%'),
        borderWidth: 1,
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        marginTop: 10,
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    viewListItem: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    tvFoodLikeText: {
        width: wp('68%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
    mobileImageStyle1: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 12, tintColor: '#d6d5dd', },
    mobileImageStyle2: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 12, },
    mobileImageStyle3: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10, tintColor: '#d6d5dd', },
    txtLikeFoodTit: {
        fontSize: 15,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    containerLikeStyle: {
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        margin: 5,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    item: {
        margin: 1,
        width: wp('29%'),
        alignItems: 'center',
        marginTop: 3,
        justifyContent: 'center',
        height: 150, // approximate a square
    },
});

const mapStateToProps = state => {
    const { goals,
        activeLevels,
        foodAllergies,
        healthConditions,
        equipments,
        foodPreferences,
        languages, professions, workoutlocations, fitnessform } = state.masters;

    const loading = state.procre.loading;
    const error = state.procre.error;
    const sucMsg = state.procre.sucMsg;
    const isSuccess = state.procre.isSuccess;
    return {
        loading,
        error,
        goals,
        activeLevels,
        foodAllergies,
        healthConditions,
        equipments,
        foodPreferences,
        sucMsg,
        isSuccess,
        languages,
        professions,
        workoutlocations,
        fitnessform,
    };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {
            getTraineeGoalsByGender,
            getActiveLevels,
            getFoodAllergies,
            getHealthProblems,
            getEquipments,
            getFoodPreferences,
            updateProfileDet,
            profileRefresh,
            getLanguages,
            getProfessions,
            getWorkoutLocations,
            getFitnessForm,
        },
    )(MyProfile_Update),
);
