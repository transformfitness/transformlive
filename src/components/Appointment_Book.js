import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
} from 'react-native';
import { connect } from 'react-redux';

import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
    DialogContent,
    DialogFooter,
    DialogButton,
} from 'react-native-popup-dialog';
import { Calendar } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
LocaleConfig.locales['en'] = {
    monthNames: ['January', 'February ', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'en';
import moment from "moment";
import DeviceInfo from 'react-native-device-info';

import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getToDate() {
    let now = new Date()
    let next60days = new Date(now.setDate(now.getDate() + 13))

    return next60days;
}

class Appointment_Book extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            noDataMsg: '',
            msgSelectDate: 'Please select date...',
            arrAllTrainers: [],
            selected: '',
            visible: false,
            day: '',
            month: '',
            year: '',
            confirmPop: false,
            isInternet: false,
            dateConString: '',
            arrTrainerSlots: [],
            selTrID: 1,
            selSlotObj: {},
            homeappoint: 0,
        };
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllAppointmentTypes();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        if (this.props.flag) {
            if (this.props.flag === 'home') {
                this.setState({ visible: true, selTrID: 1, arrTrainerSlots: [], selSlotObj: {} });
                // this.setState({ selected: '', day: '',  month: '', year: '', dateConString: '', msgSelectDate: 'Please select date...',});
                //LogUtils.infoLog(moment(new Date()).format("YYYY-MM-DD"));
                var date = moment(new Date()).format("YYYY-MM-DD");
                // let now = new Date()
                var today = new Date();
                let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                // LogUtils.infoLog(newdate);

                // LogUtils.infoLog1('selected day', day);
                this.setState({ selected: date });
                this.setState({ day: today.getDate() });
                this.setState({ month: parseInt(today.getMonth() + 1) });
                this.setState({ year: today.getFullYear() });
                // const date = new Date(2019, 12, 10);  // 2009-11-10
                // const month = date.toLocaleString('default', { month: 'short' });
                // LogUtils.infoLog(month);
                var gsDayNames = [
                    'Sunday',
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday'
                ];
                var gsMonNames = [
                    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ];

                var d = new Date(date);
                var dayName = gsDayNames[d.getDay()];
                var monName = gsMonNames[d.getMonth()];
                this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                this.getAllTrainerSlots();
            }
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getAllAppointmentTypes() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/appointmenttype`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.apnt_type.length > 0) {
                        data.data.apnt_type.map((item) => {
                            item.check = false;
                        })
                        this.setState({ loading: false, arrAllTrainers: data.data.apnt_type, homeappoint: data.data.home_appnt });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

  

    async getAllTrainerSlots() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        // http://fitness.whamzone.com/api/trainee/trainerslot
        fetch(
            `${BASE_URL}/trainee/appointmentslots`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    at_id: this.state.selTrID,
                    seldate: this.state.selected,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data.length > 0) {
                        data.data.map((item,index) => {
                            item.check = false;
                            // if (index === 0) {
                            //     this.state.arrTrainerSlots.push(item);
                            // }

                        })
                        this.setState({ loading: false,arrTrainerSlots : data.data });
                    }
                    else {
                        this.setState({ loading: false, arrTrainerSlots: [] , msgSelectDate: 'No slots available for this trainer' });
                    }


                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                    this.setState({ arrTrainerSlots: [] });
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async bookAppointment() {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            at_id: this.state.selTrID,
            seldate: this.state.selected,
            from_time: this.state.selSlotObj.from_time,
            to_time: this.state.selSlotObj.to_time,
            ua_id: 0,
        }));
        fetch(
            `${BASE_URL}/trainee/saveappointmentslot`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    at_id: this.state.selTrID,
                    seldate: this.state.selected,
                    from_time: this.state.selSlotObj.from_time,
                    to_time: this.state.selSlotObj.to_time,
                    ua_id: 0,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    // Actions.appSuccess({ infoText: data.message });
                    Actions.appSuccess({ infoText: data.message,plantype : this.props.plantype,premiumuser : this.props.premiumuser });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }



    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    ListEmptyView = () => {
        return (
            <View style={styles.viewNoData}>
                <Text style={styles.textNoData}>{this.state.noDataMsg}</Text>
            </View>

        );
    }

    ListSlotsEmptyView = () => {
        return (
            <View style={{
                padding: 10,
                marginTop: 10,
                height: 40,
            }}>
                <Text style={styles.textNoData}>{this.state.msgSelectDate}</Text>
            </View>
        );
    }

    onBackPressed() {
        if (this.state.confirmPop) {
            this.setState({ confirmPop: false });
        }
        else if (this.state.visible) {
            if (this.props.flag && this.props.flag === 'home') {
                this.setState({ visible: false });
                Actions.popTo('traineeHome');
            } else {
                this.setState({ visible: false });
            }
        }
        else {
            if (this.props.flag && this.props.flag === 'home') {
                Actions.popTo('traineeHome');
            } else {
                Actions.pop();
            }
        }
    }

    press = (hey) => {
        this.state.arrTrainerSlots.map((item) => {
            if (item.tas_id === hey.tas_id) {
                item.check = true;
                this.setState({ selSlotObj: item });
            }
            else {
                item.check = false;
            }
        })
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, visible: false, confirmPop: false });
                this.getAllAppointmentTypes();
            }
            else {
                this.setState({ isInternet: true, visible: false, confirmPop: false });
            }
        });
    }


    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Expert Appointments</Text>
                    </View>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('13%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrAllTrainers}
                        keyExtractor={item => item.at_id}
                        ItemSeparatorComponent={this.FlatListItemSeparator1}
                        ListEmptyComponent={this.ListEmptyView}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.at_id} style={styles.aroundListStyle} onPress={() => {

                                // if (item.code === 'BCWHE' && this.state.homeappoint === 0) {
                                //     Actions.traAllBuyPlans();
                                // }
                                // else {
                                this.setState({ visible: true, selTrID: item.at_id, selTraImg: item.trainer_url, arrTrainerSlots: [], selSlotObj: {} });
                                // this.setState({ selected: '', day: '',  month: '', year: '', dateConString: '', msgSelectDate: 'Please select date...',});
                                //LogUtils.infoLog(moment(new Date()).format("YYYY-MM-DD"));
                                var date = moment(new Date()).format("YYYY-MM-DD");
                                // let now = new Date()
                                var today = new Date();
                                let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                                // LogUtils.infoLog(newdate);

                                // LogUtils.infoLog1('selected day', day);
                                this.setState({ selected: date });
                                this.setState({ day: today.getDate() });
                                this.setState({ month: parseInt(today.getMonth() + 1) });
                                this.setState({ year: today.getFullYear() });
                                // const date = new Date(2019, 12, 10);  // 2009-11-10
                                // const month = date.toLocaleString('default', { month: 'short' });
                                // LogUtils.infoLog(month);
                                var gsDayNames = [
                                    'Sunday',
                                    'Monday',
                                    'Tuesday',
                                    'Wednesday',
                                    'Thursday',
                                    'Friday',
                                    'Saturday'
                                ];
                                var gsMonNames = [
                                    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                ];
                                var d = new Date(date);
                                var dayName = gsDayNames[d.getDay()];
                                var monName = gsMonNames[d.getMonth()];
                                this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                                this.getAllTrainerSlots();

                            }}>
                                <View style={{ flexDirection: 'column', paddingLeft: 10, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={styles.viewBlueDot}></View>
                                        <Text style={styles.textWorkName}>{`${item.name}`}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }} />

                </View>
                <Dialog
                    onDismiss={() => {
                        this.setState({ visible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ visible: false });
                    }}
                    width={1}
                    height={0.93}
                    dialogStyle={{ backgroundColor: '#fafafa', borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, position: 'absolute', bottom: 0, left: 0, width: wp('100%') }}
                    visible={this.state.visible}
                // rounded
                // actionsBordered
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff',
                        }}>
                        <View style={{ flexDirection: 'column', backgroundColor: '#ffffff' }}>
                            <View style={{
                                flexDirection: 'row',
                                margin: 15,
                            }}>
                                 <View style={{
                                    flexDirection: 'row',
                                    marginRight: 15,
                                    marginLeft: -15,
                                }}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ visible: false })}>
                                        <Image
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>
                                
                                <Text style={styles.textSelTime}>Choose visit time</Text>
                                <TouchableOpacity onPress={() => {
                                    if (this.state.selected) {
                                        if (this.state.selSlotObj.tas_id) {
                                            this.setState({ visible: false });
                                            this.setState({ confirmPop: true });
                                        }
                                        else {
                                            this.setState({ isAlert: true, alertMsg: 'Please select time slot to book appointment' });
                                        }

                                    }
                                    else {
                                        this.setState({ isAlert: true, alertMsg: 'Please select appointment data and time' });
                                    }

                                }}>
                                    <Text style={styles.textDone}>DONE</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ marginLeft: -20, height: 1.5, width: wp('100%'), backgroundColor: '#e9e9e9' }}></View>
                            <Calendar
                                // Initially visible month. Default = Date()
                                current={this.state.selected}
                                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                minDate={new Date()}
                                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                maxDate={getToDate()}
                                // Handler which gets executed on day press. Default = undefined
                                onDayPress={(day) => {
                                    //LogUtils.infoLog1('selected day', day);
                                    this.setState({ selected: day.dateString });
                                    this.setState({ day: day.day });
                                    this.setState({ month: day.month });
                                    this.setState({ year: day.year });
                                    // const date = new Date(2019, 12, 10);  // 2009-11-10
                                    // const month = date.toLocaleString('default', { month: 'short' });
                                    // LogUtils.infoLog(month);
                                    var gsDayNames = [
                                        'Sunday',
                                        'Monday',
                                        'Tuesday',
                                        'Wednesday',
                                        'Thursday',
                                        'Friday',
                                        'Saturday'
                                    ];
                                    var gsMonNames = [
                                        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                    ];

                                    var d = new Date(day.dateString);
                                    var dayName = gsDayNames[d.getDay()];
                                    var monName = gsMonNames[d.getMonth()];

                                    // LogUtils.infoLog1(dayName, monName);
                                    this.setState({ dateConString: dayName + ', ' + monName + ' ' + day.day + ', ' + day.year });

                                    this.getAllTrainerSlots();
                                }}

                                // Handler which gets executed on day long press. Default = undefined
                                // onDayLongPress={(day) => { LogUtils.infoLog1('selected day', day) }}
                                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                                monthFormat={'MMM yyyy'}
                                // Handler which gets executed when visible month changes in calendar. Default = undefined
                                onMonthChange={(month) => { LogUtils.infoLog1('month changed', month) }}
                                // Hide month navigation arrows. Default = false
                                //hideArrows={true}
                                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                                // renderArrow={(direction) => (<Arrow />)}
                                // Do not show days of other months in month page. Default = false
                                //hideExtraDays={true}
                                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                                // day from another month that is visible in calendar page. Default = false
                                //    disableMonthChange={true}
                                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                                firstDay={1}
                                // Hide day names. Default = false
                                //    hideDayNames={true}
                                // Show week numbers to the left. Default = false
                                //    showWeekNumbers={true}
                                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                                // onPressArrowLeft={substractMonth => substractMonth()}
                                // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                                //onPressArrowRight={addMonth => addMonth()}
                                // Disable left arrow. Default = false
                                //disableArrowLeft={true}
                                // Disable right arrow. Default = false
                                //disableArrowRight={true}
                                markedDates={{ [this.state.selected]: { selected: true, disableTouchEvent: true, selectedDotColor: '#8c52ff' } }}
                                theme={{
                                    // calendarBackground: '#333248',
                                    // textSectionTitleColor: 'white',
                                    // dayTextColor: 'red',
                                    todayTextColor: '#8c52ff',
                                    selectedDayTextColor: 'white',
                                    // monthTextColor: 'white',
                                    // indicatorColor: 'white',
                                    textDayFontSize: 12,
                                    textMonthFontSize: 12,
                                    textMonthFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontSize: 12,
                                    selectedDayBackgroundColor: '#8c52ff',
                                    arrowColor: '#8c52ff',
                                    // textDisabledColor: 'red',
                                    'stylesheet.calendar.header': {
                                        week: {
                                            marginTop: 5,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }
                                    }
                                }}
                            />




                        </View>
                        <View style={{ backgroundColor: '#fafafa', marginLeft: -20, width: wp('100%'), }}>
                            <Text style={styles.selDateText}>{this.state.dateConString}</Text>

                            <FlatList
                                style={{ width: wp('100%'), height: hp('50%') }}
                                contentContainerStyle={{ paddingBottom: hp('5%'), justifyContent: 'center', alignSelf: 'center', marginTop: 10 }}
                                numColumns={2}
                                showsVerticalScrollIndicator={false}
                                data={this.state.arrTrainerSlots}
                                keyExtractor={item => item.tas_id}
                                ItemSeparatorComponent={this.FlatListItemSeparator1}
                                ListEmptyComponent={this.ListSlotsEmptyView}
                                renderItem={({ item }) => {
                                    return <TouchableOpacity style={{
                                        marginRight: 10,
                                        marginLeft: 7,
                                        marginBottom: 7,
                                        width: wp('45%')

                                    }} onPress={() => { this.press(item) }}>
                                        {item.check
                                            ? (
                                                <View style={{
                                                    borderColor: '#8c52ff', borderRadius: 5,
                                                    borderWidth: 1,
                                                    height: 40,
                                                    justifyContent: 'center',
                                                    backgroundColor: '#8c52ff'
                                                }}>
                                                    <Text style={styles.textWorkNameChecked}>{`${item.schedule_time}`}</Text>

                                                </View>

                                            )
                                            : (

                                                <View style={{
                                                    borderColor: '#ddd', borderRadius: 5,
                                                    borderWidth: 1,
                                                    height: 40,
                                                    justifyContent: 'center',
                                                    backgroundColor: '#ffffff'

                                                }}>
                                                    <Text style={styles.textWorkName1}>{`${item.schedule_time}`}</Text>

                                                </View>

                                            )}
                                    </TouchableOpacity>
                                }} />
                        </View>

                    </DialogContent>
                </Dialog>

                {/* confirm popup */}
                <Dialog
                    onDismiss={() => {
                        this.setState({ confirmPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ confirmPop: false });
                    }}
                    width={0.8}
                    // height={0.9}
                    // dialogStyle={{marginTop: 30,}}
                    visible={this.state.confirmPop}
                    // rounded
                    // actionsBordered
                    footer={
                        <DialogFooter style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                            alignSelf: 'center',
                        }}>
                            <DialogButton
                                text="Cancel"
                                bordered
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                }}
                                textStyle={{
                                    fontSize: 13, color: '#6d819c', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="Confirm"
                                bordered
                                textStyle={{
                                    fontSize: 13, color: '#8c52ff', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                    this.bookAppointment();
                                }}
                                key="button-2"
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>
                                <Text style={{
                                    alignSelf: 'center', color: '#282c37', fontSize: 16,
                                    fontWeight: '500',
                                    textAlign: 'center',
                                    marginTop: 15,
                                    fontFamily: 'Rubik-Medium',
                                    letterSpacing: 0.49,
                                }}>Please confirm your appointment</Text>
                                <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                    <Image
                                        source={require('../res/ic_app.png')}
                                        style={styles.profileImage1}
                                    />
                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 13,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{moment(this.state.selected, 'YYYY-MM-DD').format("DD-MM-YYYY")}</Text>
                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 5,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{this.state.selSlotObj.schedule_time}</Text>
                                </View>


                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        height: hp('10%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        padding: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textWorkNameChecked: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#ffffff',
        padding: 10,
    },
    textWorkName1: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
        padding: 10,
    },
    textWorkName: {
        width: wp('80%'),
        fontSize: 14,
        fontWeight: '500',
        marginTop: 3,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textMonth: {
        fontSize: 6,
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        backgroundColor: '#8c52ff',
        textAlign: 'center',
        color: '#ffffff',
        borderRadius: 5,
        textAlign: 'center',
        alignSelf: 'center',
        paddingLeft: 5,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 5,
        letterSpacing: 0.23,
    },
    textSelTime: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#1E1F20',
    },
    textDone: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#8c52ff',
    },
    selDateText: {
        alignSelf: 'flex-start',
        fontSize: 11,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 15,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
    },
    profileImage1: {
       width: 200,
        height: 60,
        marginTop: 10,
        marginBottom: 10,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    viewNoData: {
        padding: 10,
        marginTop: Dimensions.get('window').height / 3,
        height: 40,
    },
    textNoData: {
        textAlign: 'center',
        marginLeft: 20,
        marginRight: 20,
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 22,
        letterSpacing: 0.1,
    },
    viewBlueDot: {
        width: 13,
        height: 13,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 6.5,
    },
});

const mapStateToProps = state => {
   
    return {};
};

export default connect(
    mapStateToProps,
    {
       
    },
)(Appointment_Book);
