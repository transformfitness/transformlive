import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LogUtils from '../utils/LogUtils.js';
import Database from '../Database';
import { BASE_URL } from '../actions/types';
import { NoInternet, CustomDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const db1 = new Database();

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class ChatList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chats: [],
            isAlert: false,
            loading: false,
            alertMsg: '',
            alertTitle:'',
            isInternet: false,
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            resObj:'',
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.getChats();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
       if (this.props.from === 'rewards' || this.props.from === 'feed') {
            Actions.pop();
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    async getChats() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getchatdata`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                this.setState({ resObj: data });
                
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        LogUtils.infoLog1('statusmessage', data.data[0].tr_msg);
                        this.setState({ loading: false, chats: data.data });
                    }
                    else {
                        this.setState({ loading: false, chats: [],alertMsg: data.message,alertTitle: data.title });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message,alertTitle : data.title });
                    } else {
                        this.setState({ loading: false, alertMsg: data.message,alertTitle: data.title});
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1.6,
                    marginRight: 15,
                    marginLeft: 15,
                    backgroundColor: "#F4F6FA",
                }}
            />
        );
    }
    
    renderBody(item) {
        if (item.last_msg_time && item.last_msg_time != null) {
            return (
                <View style={{ width: '96%', }}>
                    <Text style={styles.textTime}>{item.last_msg_time}</Text>
                </View>

            );
        } else {
            return (
                <View>

                </View>

            );
        }
    }

    renderFlatList() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.chats) && this.state.chats.length > 0) {

                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('10%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.chats}
                            keyExtractor={item => "chats"+item.id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                return <TouchableOpacity onPress={() => {
                                    // this.setState({ isPopVisible: true })
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 300,
                                        p_category: 'ChalList',
                                        p_name: 'ChalList->Chat Details',
                                      });
                                    Actions.oneononechat({ item: item })

                                }}>


                                    <View style={styles.aroundListStyle} >
                                        <View >
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.tr_img }}
                                                style={styles.profileImage}
                                            />

                                        </View>
                                        <View style={{ width: '92%', flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                            <View style={{ flexDirection: 'row', width: '90%' }}>
                                                <Text style={{
                                                    fontSize: 14,
                                                    fontWeight: '500',
                                                    lineHeight: 18,
                                                    marginRight: 40,
                                                    fontFamily: 'Rubik-Regular',
                                                    color: '#282c37',
                                                }}>{`${item.tr_name}`}</Text>
                                                {/* <Text style={{
                                                    fontSize: 11,
                                                    fontWeight: '400',
                                                    fontFamily: 'Rubik-Regular',
                                                    color: '#9c9ab9',
                                                    alignSelf: 'flex-end',
                                                    textAlign: 'center',
                                                    marginLeft : 15,
                                                    marginBottom: 10,
                                                }}>28/11/2020</Text> */}
                                            </View>

                                            {this.renderBody(item)}
                                        </View>
                                        <View style={styles.viewOraDot}>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View>
                );
            }
            else {
                return (
                    <View style={styles.viewTitleCal}>
                        <Image
                            source={require('../res/ic_notification_nodata.png')}
                            style={styles.imgUnSuscribe}
                        />

                        <Text style={styles.textCreditCard}>{this.state.alertTitle}</Text>

                        <Text style={styles.textDesc}>{this.state.alertMsg}</Text>

                    </View>
                );

            }
        }
    }

    async onAccept() {
        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.getChats();
            }
            else {
                LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <Loader loading={this.state.loading} />
                {/* top */}
                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>

                    <TouchableOpacity
                        onPress={() => this.onBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>
                    <Text style={styles.textHeadTitle}>Chats</Text>
                </View>
                {this.renderFlatList()}

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    viewTop: {
        width: '100%',
        flexDirection: 'column',
        padding: 20,
        backgroundColor: '#F4F6FA',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    notyTextStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        marginTop: 15,
        fontWeight: '500',
        letterSpacing: 0.8,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        padding: 15,
    },
    profileImage: {
        width: 45,
        height: 45,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 45 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start',
    },
    textWorkName: {
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 18,
        marginRight: 40,
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
    },
    textTime: {
        width: '90%',
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        marginTop: 3,
        color: '#9c9ab9',
    },
    viewOraDot: {
        width: 15,
        height: 15,
        backgroundColor: '#e1ddf5',
        position: 'absolute',
        alignSelf: 'center',
        right: 10,
        borderRadius: 10,
        marginTop: 10,
    },
    viewTitleCal: {
        height: '85%',
        flexDirection: 'column',
        padding: 30,
        justifyContent: 'center',
        alignContent: 'center',
    },
    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    imgUnSuscribe: {
        width: 250,
        height: 250,
        alignSelf: 'center',
    },
});

const mapStateToProps = state => {
    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(ChatList),
);

// export default ProfileCreation;