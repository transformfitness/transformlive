import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
    TextInput,
    Modal,
    Platform,
    StatusBar,
    ScrollView
} from 'react-native';
import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions as RouterActions } from 'react-native-router-flux';
import NetInfo from "@react-native-community/netinfo";
import { GiftedChat, Composer, Actions as GiftedActions } from 'react-native-gifted-chat';
// import { createThumbnail } from "react-native-create-thumbnail";
import LinearGradient from 'react-native-linear-gradient';
import Video from 'react-native-video';
import LogUtils from '../utils/LogUtils.js';
import Lightbox from 'react-native-lightbox';
import DeviceInfo from 'react-native-device-info';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
import { renderInputToolbar, renderComposer, renderSend } from './InputToolBar';
import {
    renderAvatar,
    renderBubble,
    renderSystemMessage,
    renderMessage,
    renderMessageText,
    renderCustomView,
} from './MessageContainer';
let userId = 0, username = '', token = '';
import { BASE_URL } from '../actions/types';
import ImagePicker from 'react-native-image-picker';
import Dialog, {
    DialogContent, SlideAnimation
} from 'react-native-popup-dialog';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RBSheet from "react-native-raw-bottom-sheet";

const ITEMS = [...Array(25).keys()];
let interval;
let trainerId = 0;
import DocumentPicker from 'react-native-document-picker';
import { allowFunction } from '../utils/ScreenshotUtils.js';

const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

const createFormData = (image, body, type) => {
    const data = new FormData();

    if (image) {
        LogUtils.infoLog1("image", image);
        LogUtils.infoLog1("type", type);
        // image.forEach((photo) => {

        // if (photo.includes('mp4') || photo.includes('Mov')) {
        if (type === 2) {
            data.append('file', {
                uri: image,
                name: 'Video',
                type: 'video/mp4', // or photo.type
            });
        }
        else if (type === 1) {
            data.append('file', {
                uri: image,
                name: 'Image',
                type: 'image/jpeg', // or photo.type
            });
        }
        // });
    }
    // }
    data.append('data', body);
    return data;
};


class OneOnOneChat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            isInternet: false,
            alertMsg: '',
            noDataMsg: '',
            paused: true,
            type: 0,
            progress: 0,
            isProgress: false,
            // messages: [{"_id": 11, "createdAt": "30 Nov, 2020 12:50", "image": "", "text": "Soon wat else", "user": {"_id": 3, "avatar": "http://fitness.whamzone.com/gallery/profile/1605861636928.jpg", "name": "Pramod Pramu"}, "video": ""}, {"_id": 10, "createdAt": "30 Nov, 2020 11:54", "image": "", "text": "I am good hw r u.", "user": {"_id": 3, "avatar": "http://fitness.whamzone.com/gallery/profile/1605861636928.jpg", "name": "Pramod Pramu"}, "video": ""}, {"_id": 9, "createdAt": "30 Nov, 2020 11:46", "image": "", "text": "Hiii I am fine.", "user": {"_id": 3, "avatar": "http://fitness.whamzone.com/gallery/profile/1605861636928.jpg", "name": "Pramod Pramu"}, "video": ""}, {"_id": 3, "createdAt": "28 Nov, 2020 12:43", "image": "", "text": "hi how r u", "user": {"_id": 22, "avatar": "https://res.cloudinary.com/appoids-dev/image/upload/v1599634820/fitness/trainer/oie_LZNAay5RHFWs_pgfeiz_ynnm7o.jpg", "name": "Gowtham Setty"}, "video": ""}],
            messages: [],
            setMessages: [],
            text: '',
            setText: '',

            rate: 1,
            volume: 50,
            muted: false,
            ignoreSilentSwitch: 'ignore',
            showVideoPopup: false,
            url: '',

            isAddImgPop: false,
            lastSyncTime: '',
            intervalId: 0,
            cameraText: '',
            sampleMessage: [{
                _id: 85,
                createdAt: "04 Dec, 2020 19:11",
                image: "",
                text: "pramod pls tell you details",
                user: [
                    { _id: 22, avatar: "https://res.cloudinary.com/appoids-dev/image/upload/v1599634820/fitness/trainer/oie_LZNAay5RHFWs_pgfeiz_ynnm7o.jpg", name: "Vijaya Tupurani" }
                ],
                video: ""
            }]
        };
    }

    async componentDidMount() {
        allowFunction();
        StatusBar.setHidden(true);
        token = await AsyncStorage.getItem('token');
        userId = parseInt(await AsyncStorage.getItem('userId'));
        LogUtils.infoLog1("userId", userId);
        username = await AsyncStorage.getItem('userName');
        // this.setState({ messages: this.props.item.tr_msg })

        this.setState({ intervalId: interval });
        // this.getChatData();

        if (Platform.OS === 'android') {
            this.setState({ cameraText: 'Take Photo' });
        } else {
            this.setState({ cameraText: 'Camera' });
        }

        if (this.props.item) {
            trainerId = this.props.item.tr_id;
        } else if (this.props.trainerId) {
            trainerId = this.props.trainerId;
        }

        this.getChatData()
        interval = setInterval(() => this.getChatData(), 15000)

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }


    componentWillUnmount() {
        StatusBar.setHidden(true);
        clearInterval(this.state.intervalId);
        this.backHandler.remove();
        // Actions.pop();
    }


    onBackPressed() {
        // if (interval) {
        //     clearInterval(interval);
        // }
        clearInterval(this.state.intervalId);
        RouterActions.chatlist();

    }

    async getChatData() {
        LogUtils.infoLog1("Token", token);
        LogUtils.infoLog1("Body", JSON.stringify({
            tr_id: trainerId,
            last_sync_time: this.state.lastSyncTime,
        }));


        fetch(
            `${BASE_URL}/trainee/getchatdatadet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    tr_id: trainerId,
                    last_sync_time: this.state.lastSyncTime,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        this.setState({ messages: [...data.data, ...this.state.messages] });
                        // if (this.state.messages.length > 0) {
                        //     this.setState({ messages: this.state.messages.splice(0, 0, this.state.sampleMessage) });
                        // } else {
                        //     LogUtils.infoLog1('index 0', data.data[0].user);
                        //     this.setState({ messages: data.data });
                        // }

                    }
                    this.setState({ lastSyncTime: data.sync_time });


                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions1.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    async saveuserchat(image, body) {
        LogUtils.infoLog1('Body', body);
        futch(`${BASE_URL}/trainee/saveuserchat`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${token}`,
            },
            body: createFormData(image, body, this.state.type),
        }, (progressEvent) => {
            const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            this.setState({ progress: progress });
        })
            .then(res => {
                LogUtils.infoLog1('res', res);
                // this.setState({ isProgress: false })
                const statusCode = res.status;
                LogUtils.infoLog1('statusCode', statusCode);
                const data = res.response;
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (this.state.type === 2) {
                        this.getChatData();
                    }
                    //   this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: "Your post has been uploaded successfully" });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        // this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        // this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
                // this.setState({ isProgress: false })
            })
            .catch(function (error) {
                LogUtils.infoLog1('upload error', error);
                // this.setState({ isProgress: false, loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    onSend(messages = []) {
        LogUtils.infoLog1('messages', messages);
        let body = '';
        body = JSON.stringify({
            file_type: 0,
            msg: messages[0].text,
            tr_id: trainerId,
        });
        this.saveuserchat(
            '',
            body,
        );
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    renderMessageVideo(props) {
        const { lightboxProps, } = this.props;
        return <View style={{ position: 'relative', height: 140, width: 210 }}>
            {props.currentMessage.thumbnail
                ? (
                    <Image
                        style={{
                            position: 'absolute',
                            left: 5,
                            top: 5,
                            height: 130,
                            width: 200,
                            borderRadius: 5,
                        }}
                        resizeMode="cover"
                        height={130}
                        width={200}
                        source={{ uri: props.currentMessage.thumbnail }}>
                    </Image>
                )
                : (

                    // createThumbnail({
                    //     url: props.currentMessage.video,
                    //     timeStamp: 5000,
                    //   })
                    //     .then(response => console.log({ response }))
                    //     .catch(err => console.log({ err }))

                    <View
                        style={{
                            position: 'absolute',
                            left: 5,
                            top: 5,
                            height: 130,
                            width: 200,
                            borderRadius: 5,
                            backgroundColor: '#000000'
                        }}>
                    </View>
                )}
            {/* <Image
                style={{
                    position: 'absolute',
                    left: 5,
                    top: 5,
                    height: 130,
                    width: 200,
                    borderRadius: 5,
                }}
                resizeMode="cover"
                height={130}
                width={200}
                source={{ uri: props.currentMessage.thumbnail }}>
            </Image> */}

            <TouchableOpacity style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                    if (Platform.OS === 'android') {
                        RouterActions.newvideoplay({ videoURL: props.currentMessage.video, from: 'chat', });
                    } else {
                        this.setState({ showVideoPopup: true, url: props.currentMessage.video, paused: false })
                    }

                }}>
                <Image
                    style={{ width: 40, height: 40, }}
                    source={require('../res/ic_transparent_play.png')}>

                </Image>
            </TouchableOpacity>
        </View>
    }

    renderCustomView(props) {
        const { lightboxProps, } = this.props;
        return <View style={{ position: 'relative', height: 140, width: 210 }}>
            <Video
                style={{
                    position: 'absolute',
                    left: 5,
                    top: 5,
                    height: 130,
                    width: 200,
                    borderRadius: 5,
                }}
                resizeMode="cover"
                height={130}
                width={200}
                muted={true}
                paused={this.state.paused}
                source={{ uri: props.currentMessage.video }}
                allowsExternalPlayback={false}>
            </Video>
            <TouchableOpacity style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                    if (Platform.OS === 'android') {
                        Actions.newvideoplay({ videoURL: props.currentMessage.video, from: 'chat', });
                    } else {
                        this.setState({ showVideoPopup: true, url: props.currentMessage.video, paused: false })
                    }

                }}>
                <Image
                    style={{ width: 40, height: 40, }}
                    source={require('../res/ic_transparent_play.png')}>

                </Image>
            </TouchableOpacity>
        </View>
    }

    renderMessageImage(props) {
        //console.log(props.currentMessage.image);
        return <TouchableOpacity style={{ position: 'relative', height: 140, width: 210 }}>
            <Image
                style={{
                    position: 'absolute',
                    left: 5,
                    top: 5,
                    height: 130,
                    width: 200,
                    borderRadius: 10,
                }}
                resizeMode="cover"
                height={130}
                width={200}
                muted={true}
                source={{ uri: props.currentMessage.image }}>
            </Image>
        </TouchableOpacity>
    }

    renderActions = (props) => (
        <GiftedActions
            {...props}
            containerStyle={{
                width: 44,
                height: 44,
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: 4,
                marginRight: 4,
                marginBottom: 0,
            }}
            icon={() => (
                <TouchableOpacity
                    style={{ width: 44, height: 44, justifyContent:'center',alignSelf:'center', alignItems:'center'}}
                    onPress={() => {
                        // this.setState({ isAddImgPop: true })
                        this.Scrollable.open()
                    }}>

                    <Image
                        style={{ width: 25, height: 25 }}
                        source={require('../res/ic_attachment.png')}
                    />

                </TouchableOpacity>

            )}
        // options={{
        //     'Camera': () => {
        //         this.launchCamera();
        //     },
        //     'Choose Image From Library': () => {
        //         this.launchImageLibrary();
        //     },
        //     'Record Video': () => {
        //         this.launchRecordVideo();
        //     },
        //     'Choose Video From Library': () => {
        //         this.launchVideoLibrary();
        //     },
        //     Cancel: () => {
        //         console.log('Cancel');
        //     },
        // }}
        // optionTintColor="#222B45"
        />
    );

    renderItem({ item }) {
        return (
            <View>
                <TouchableOpacity style={styles.button} onPress={() => this[RBSheet + item].open()}>
                    <Text style={styles.buttonText}>ITEM {item}</Text>
                </TouchableOpacity>
                <RBSheet
                    ref={ref => {
                        this[RBSheet + item] = ref;
                    }}
                >
                    <View style={styles.bottomSheetContainer}>
                        <Text style={styles.bottomSheetText}>I AM ITEM {item}</Text>
                    </View>
                </RBSheet>
            </View>
        );
    }


    launchImageLibrary = () => {

        let options = {
            // mediaType: 'mixed',
            // mediaType: 'photo',
            // storageOptions: {
            //     skipBackup: true,
            //     // path: 'images',
            // },

            title: 'You can choose one image',
            maxWidth: 256,
            maxHeight: 256,
            noData: true,
            mediaType: 'mixed',
            storageOptions: {
                skipBackup: true
            }
        };

        // if (Platform.OS === 'android') {

        // }
        ImagePicker.launchImageLibrary(options, response => {

            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
            } else {
                // console.log('launchImageLibrary fileUri', response.uri);
                let fileType = 0, message;
                if (response.uri.includes('JPEG') || response.uri.includes('JPG') || response.uri.includes('PNG') || response.uri.includes('jpeg') || response.uri.includes('jpg') || response.uri.includes('png')) {
                    fileType = 1
                    message = { _id: 0, image: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                    this.setState({ type: 1 })
                } else if (response.uri.includes('MP4') || response.uri.includes('MOV') || response.uri.includes('mp4') || response.uri.includes('mov')) {
                    fileType = 2
                    message = { _id: 0, video: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                    this.setState({ type: 2 })
                }
                let body = '';
                body = JSON.stringify({
                    file_type: fileType,
                    msg: '',
                    tr_id: trainerId,
                });
                this.saveuserchat(
                    response.uri,
                    body,
                );
                this.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, message),
                }))

                // this.setState({
                //   filePath: response,
                //   fileData: response.data,
                //   fileUri: response.uri,
                //   fileType: response.type,
                // });
            }
        });
    };
    // launchCamera = () => {
    //     let options = {
    //         title: 'Select the perfect view',
    //         storageOptions: {
    //             skipBackup: true,
    //             path: 'images'
    //         },
    //         customButtons: [
    //             { name: 'video', title: 'Take Video...' },
    //             { name: 'video_library', title: 'Choose Video from library...' },
    //         ],
    //         maxWidth: 1920,
    //         maxHeight: 1080,
    //         noData: true
    //     };
    //     let optionsVideo = {
    //         storageOptions: {
    //             skipBackup: true,
    //             path: 'movies'
    //         },
    //         noData: true,
    //         mediaType: 'video'
    //     };

    //     ImagePicker.showImagePicker(options, (response) => {
    //         if (response.didCancel) {
    //             console.log('User cancelled image picker');
    //         } else if (response.error) {
    //             console.log('ImagePicker Error: ', response.error);
    //         } else if (response.customButton === 'video') {
    //             ImagePicker.launchCamera(optionsVideo, (response) => {
    //                 //do what you want with the video
    //             });
    //         } else if (response.customButton === 'video_library') {
    //             ImagePicker.launchImageLibrary(optionsVideo, (response) => {
    //                 //do what you want with the video
    //             });
    //         } else {
    //             //do what you want with the picture
    //         }
    //     });
    // };

    captureImage = () => {
        let options = {
            mediaType: 'mixed',
            title: 'Select Image',
            videoQuality: 'medium',
            durationLimit: 20,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                let fileType = 0, message;
                if (response.uri.includes('JPEG') || response.uri.includes('JPG') || response.uri.includes('PNG') || response.uri.includes('jpeg') || response.uri.includes('jpg') || response.uri.includes('png')) {
                    fileType = 1
                    message = { _id: Math.round(Math.random() * 1000000), image: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                    this.setState({ type: 1 })
                } else if (response.uri.includes('MP4') || response.uri.includes('MOV') || response.uri.includes('mp4') || response.uri.includes('mov')) {
                    fileType = 2
                    message = { _id: Math.round(Math.random() * 1000000), video: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                    this.setState({ type: 2 })
                }

                let body = '';
                body = JSON.stringify({
                    file_type: fileType,
                    msg: '',
                    tr_id: trainerId,
                });
                this.saveuserchat(
                    response.uri,
                    body,
                );

                this.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, message),
                }))
            }
        });
    };

    // launchCamera = () => {
    //     let options = {
    //         // mediaType: 'mixed',
    //         storageOptions: {
    //             skipBackup: true,
    //             path: 'images',
    //         },
    //     };
    //     ImagePicker.launchCamera(options, response => {
    //         console.log('Response = ', response);

    //         if (response.didCancel) {
    //             // console.log('User cancelled image picker');
    //         } else if (response.error) {
    //             // console.log('ImagePicker Error: ', response.error);
    //         } else if (response.customButton) {
    //             // console.log('User tapped custom button: ', response.customButton);
    //         } else {
    //             // console.log('launchCamera fileUri', response.uri);

    //             const message = { _id: Math.round(Math.random() * 1000000), image: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
    //             this.setState({ type: 1 })
    //             let body = '';
    //             body = JSON.stringify({
    //                 file_type: 1,
    //                 msg: '',
    //                 tr_id: this.props.item.tr_id,
    //             });
    //             this.saveuserchat(
    //                 response.uri,
    //                 body,
    //             );

    //             this.setState(previousState => ({
    //                 messages: GiftedChat.append(previousState.messages, message),
    //             }))

    //         }
    //     });
    // };

    launchRecordVideo = () => {
        let options = {
            mediaType: 'video',
            videoQuality: 'medium',
            durationLimit: 20,
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.launchCamera(options, response => {
            //console.log('Response = ', response);
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
            } else {
                // console.log('launchCamera fileUri', response.uri);

                const message = { _id: Math.round(Math.random() * 1000000), video: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                this.setState({ type: 2 })
                let body = '';
                body = JSON.stringify({
                    file_type: 2,
                    msg: '',
                    tr_id: trainerId,
                });
                this.saveuserchat(
                    response.uri,
                    body,
                );

                this.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, message),
                }))
            }
        });
    };

    launchVideoLibrary = () => {
        let options = {
            mediaType: 'mixed',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
            } else {
                // console.log('launchImageLibrary fileUri', response.uri);
                const message = { _id: Math.round(Math.random() * 1000000), video: response.uri, createdAt: new Date(), user: { "_id": userId, "name": username }, };
                this.setState({ type: 2 })
                let body = '';
                body = JSON.stringify({
                    file_type: 2,
                    msg: '',
                    tr_id: trainerId,
                });
                this.saveuserchat(
                    response.uri,
                    body,
                );

                this.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, message),
                }))

                // this.setState({
                //   filePath: response,
                //   fileData: response.data,
                //   fileUri: response.uri,
                //   fileType: response.type,
                // });
            }
        });
    };


    handleFullScreenDismiss = () => {
        LogUtils.infoLog1('Dismiss');
        this.setState({ showVideoPopup: false })
    };

    handleFullScreenToPortrait = () => {
        this.player.presentFullscreenPlayer();
    };


    render() {

        return (
            // <ImageBackground source={require('../res/ic_chat_bg.jpg')} style={styles.mainContainer}>
            <View style={styles.mainContainer}>
                {/* <StatusBar hidden /> */}
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <View style={{
                        borderBottomWidth: 1,
                        backgroundColor: '#8c52ff',
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                        position: 'relative',
                        padding: 6,
                        borderColor: '#ddd',

                    }}>
                        <View style={{ position: 'absolute', zIndex: 111, marginLeft: 20, alignSelf: 'center', paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0, }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', marginLeft: 40, paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0, }}>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: this.props.item.tr_img }}
                                style={styles.profileImage}
                            />

                            <Text style={styles.textHeadTitle}>{this.props.item.tr_name}</Text>

                        </View>

                    </View>

                    <GiftedChat
                        contentContainerStyle={{ paddingBottom: hp('12%') }}
                        messages={this.state.messages}
                        onSend={messages => this.onSend(messages)}
                        user={{ _id: userId, name: username }}
                        renderBubble={renderBubble}
                        // renderCustomView={renderCustomView}
                        renderMessageText={renderMessageText}
                        // renderMessage={renderMessage}
                        placeholder='Type your message here...'
                        // showUserAvatar
                        alwaysShowSend
                        scrollToBottom
                        isTyping={true}
                        // renderUsernameOnMessage={true}
                        renderMessageVideo={(props) => this.renderMessageVideo(props)}
                        // renderMessageImage={(props) => this.renderMessageImage(props)}
                        renderComposer={renderComposer}
                        renderInputToolbar={renderInputToolbar}
                        renderActions={(props) => this.renderActions(props)}
                        renderSend={renderSend}
                        // renderCustomView={renderCustomView}
                        keyboardShouldPersistTaps='never'
                        renderAvatar={null}
                    // renderMessage={this.renderMessage}
                    // scrollToBottomComponent={scrollToBottomComponent}
                    />

                    {/* <View style={styles.containerMobileStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Type your message here..."
                            placeholderTextColor="grey"
                            multiline={true}
                            keyboardType="name-phone-pad"
                            value={this.state.text}
                            onChangeText={text => this.setState({ text: text })}
                        />

                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                onPress={() => this.onSendButtonPressed()}>
                                <Text style={styles.buttonText}>Send</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View> */}

                    {/* <FlatList
                        style={styles.container}
                        data={ITEMS}
                        renderItem={this.renderItem}
                        keyExtractor={(_, index) => index.toString()}
                    /> */}

                    {/* Grid Menu */}
                    <RBSheet
                        ref={ref => {
                            this.Scrollable = ref;
                        }}
                        height={150}
                        closeOnDragDown
                        customStyles={{
                            container: {
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10
                            }
                        }}
                    >
                        <ScrollView>
                            <View style={{ flexDirection: 'column', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', width: '100%' }}>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', width: '32%' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                // this.setState({ isAddImgPop: false });
                                                this.Scrollable.close()
                                                setTimeout(() => this.captureImage(), 1000);
                                            }}>
                                            <View style={styles.containerMaleStyle2}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                    source={require('../res/photo.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>

                                        <Text style={styles.desc}>{this.state.cameraText}</Text>
                                    </View>
                                    {Platform.OS === 'android'
                                        ? (
                                            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '32%' }}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        // this.setState({ isAddImgPop: false });
                                                        this.Scrollable.close()
                                                        setTimeout(() => this.launchRecordVideo(), 1000);
                                                    }}>
                                                    <View style={styles.containerMaleStyle2}>

                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            style={{ width: 35, height: 25, tintColor: '#ffffff' }}
                                                            source={require('../res/video.png')}>
                                                        </Image>

                                                    </View>
                                                </TouchableOpacity>

                                                <Text style={styles.desc}>Record Video</Text>
                                            </View>
                                        )
                                        : (
                                            <View></View>


                                        )}
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', width: '32%' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                // this.setState({ isAddImgPop: false });
                                                this.Scrollable.close()
                                                setTimeout(() => this.launchImageLibrary(), 500);


                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Gallery</Text>
                                    </View>

                                    {/* <View style={{ flexDirection: 'column', alignSelf: 'center', width: '32%' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                // this.setState({ isAddImgPop: false });
                                                this.Scrollable.close()
                                                setTimeout(() => this.launchImageLibrary(), 1000);

                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Document</Text>
                                    </View> */}

                                </View>



                            </View>
                        </ScrollView>
                    </RBSheet>





                </View>

                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.showVideoPopup}
                    onRequestClose={() => {
                        //console.log('close modal');
                    }}>
                    <View style={{
                        backgroundColor: '#ffffff',
                        // position: 'relative',
                        // flex: 1,
                        justifyContent: 'center',
                        width: '100%',
                        height: '100%'
                    }}>

                        <View style={{
                            flex: 1, backgroundColor: '#000000', justifyContent: 'center', width: '100%',
                            height: '100%'
                        }}>
                            <Video
                                ref={ref => this.player = ref}
                                // source={{ uri: "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4" }}
                                source={{ uri: this.state.url }}
                                style={styles.fullScreen}
                                rate={this.state.rate}
                                paused={this.state.paused}
                                volume={this.state.volume}
                                muted={this.state.muted}
                                ignoreSilentSwitch={this.state.ignoreSilentSwitch}
                                // resizeMode={this.state.resizeMode}
                                // onLoad={this.onLoad}
                                // onBuffer={this.onBuffer}
                                // onProgress={this.onProgress}
                                // onEnd={this.onEnd}
                                // controls={true}
                                preferredForwardBufferDuration={10}
                                onLoadStart={this.handleFullScreenToPortrait}
                                onFullscreenPlayerDidDismiss={() =>
                                    this.handleFullScreenDismiss()
                                }
                            />
                            {Platform.OS === 'android'
                                ? (
                                    <TouchableOpacity style={styles.viewTop}
                                        onPress={() => this.setState({ showVideoPopup: false, paused: true })}>
                                        <Image
                                            source={require('../res/ic_cross_water.png')}
                                            style={styles.popBackImageStyle}
                                        />
                                    </TouchableOpacity>
                                )
                                : (
                                    <View>
                                    </View>
                                )
                            }
                        </View>


                    </View>
                </Modal>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    width={0.8}
                    // height={0.5}
                    dialogStyle={{

                        // marginRight: 10,
                        // marginBottom: 300,
                        position: 'absolute',
                        bottom: 50,
                        // paddingLeft: 5,
                        // paddingRight: 5,
                        // paddingBottom: 10,
                        // paddingTop: 10,
                        // flexDirection: 'column',
                        justifyContent: 'flex-end',
                    }}
                    visible={this.state.isAddImgPop}
                    animationInTiming={100}
                    animationOutTiming={100}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}>

                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15, }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <View style={{ marginTop: 10, }}></View>
                                {/* <Text style={styles.textAddImgTit}>Choose</Text> */}

                                <View style={{ flexDirection: 'column', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                    <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', }}>
                                        <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({ isAddImgPop: false });
                                                    this.captureImage();
                                                }}>
                                                <View style={styles.containerMaleStyle2}>

                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                        source={require('../res/photo.png')}>
                                                    </Image>

                                                </View>
                                            </TouchableOpacity>
                                            <Text style={styles.desc}>Take Photo</Text>
                                        </View>
                                        <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 30, }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({ isAddImgPop: false });
                                                    setTimeout(() => this.launchImageLibrary(), 500);

                                                }}>
                                                <View style={styles.containerMaleStyle1}>

                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                        source={require('../res/transformation.png')}>
                                                    </Image>

                                                </View>
                                            </TouchableOpacity>
                                            <Text style={styles.desc}>Photo Gallery</Text>
                                        </View>

                                        <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 30, }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({ isAddImgPop: false });
                                                    setTimeout(() => this.launchImageLibrary(), 1000);

                                                }}>
                                                <View style={styles.containerMaleStyle1}>

                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                        source={require('../res/transformation.png')}>
                                                    </Image>

                                                </View>
                                            </TouchableOpacity>
                                            <Text style={styles.desc}>Document</Text>
                                        </View>

                                    </View>

                                    {/* <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center',marginTop : 20 }}>
                                        <View style={{ flexDirection: 'column', alignSelf: 'center' ,marginRight : 10}}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({ isAddImgPop: false });
                                                    this.launchRecordVideo();
                                                }}>
                                                <View style={styles.containerMaleStyle2}>

                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                        source={require('../res/photo.png')}>
                                                    </Image>

                                                </View>
                                            </TouchableOpacity>
                                            <Text style={styles.desc}>Record Video</Text>
                                        </View>
                                        <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({ isAddImgPop: false });
                                                    this.launchVideoLibrary();
                                                }}>
                                                <View style={styles.containerMaleStyle1}>

                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={{ width: 25, height: 25, tintColor: '#ffffff' }}
                                                        source={require('../res/transformation.png')}>
                                                    </Image>

                                                </View>
                                            </TouchableOpacity>
                                            <Text style={styles.desc}>Video Gallery</Text>
                                        </View>

                                    </View> */}

                                </View>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />
                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
                {/* </ImageBackground> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        backgroundColor: '#ffffff'
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'center',
        tintColor: '#ffffff'
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        textAlign: 'center',
        marginLeft: 10,
    },

    textInputStyle: {
        height: 40,
        flex: 1,
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        fontWeight: '400',
        letterSpacing: 0.23,
        marginLeft: 10,
    },
    containerMobileStyle: {
        borderTopWidth: 1,
        backgroundColor: '#fff',
        color: 'white',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'absolute',
        alignSelf: 'center',
        paddingRight: 5,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        bottom: 0,
    },
    linearGradient: {
        width: '25%',
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 8,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    profileImage: {
        width: 40,
        height: 40,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 40 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start',
    },
    imageActive: {
        flex: 1,
        resizeMode: 'contain',
    },
    fullScreen: {
        width: '100%',
        height: '100%',
        // position: 'absolute',
        // top: 0,
        // left: 0,
        // bottom: 0,
        // right: 0,
        flex: 1,
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 15,
        alignItems: 'flex-start',
        padding: 5,
        justifyContent: 'center',
        width: 30,
        height: 30,
        borderRadius: 20,
        backgroundColor: 'white',
        // zIndex: 100,
        top: 1,
    },
    popBackImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#000000',
        alignSelf: 'center',
    },

    textAddImgTit: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    containerMaleStyle2: {
        width: 50,
        height: 50,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    containerMaleStyle1: {
        width: 50,
        height: 50,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },



    container: {
        flex: 1,
        marginVertical: 50,
        marginHorizontal: 20
    },
    button: {
        backgroundColor: "#4EB151",
        paddingVertical: 10,
        alignItems: "center",
        borderRadius: 3,
        marginTop: 20
    },
    buttonText: {
        color: "#FFFFFF",
        fontSize: 16
    },
    bottomSheetContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    bottomSheetText: {
        fontSize: 28
    }

});

export default OneOnOneChat;
