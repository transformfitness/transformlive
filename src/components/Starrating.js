// @flow
import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text
} from 'react-native';

export default class StarRating extends Component {
	render() {
		// Recieve the ratings object from the props
		let ratings = this.props.ratings;

		// This array will contain our star tags. We will include this
		// array between the view tag.
		let stars = [];
		// Loop 5 times
		for (var i = 1; i <= 5; i++) {
			// set the path to filled stars
			let path = require('../res/star_filled.png');
			// If ratings is lower, set the path to unfilled stars
			if (i > ratings) {
				path = require('../res/star_unfilled.png');
			}

			stars.push((<Image style={styles.image} key={i+"star"} source={path} />));
		}

		return (
			<View style={ styles.container }>
				{ stars }
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'transparent',
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 10, 
	},
	image: {
		width: 12,
		height: 12
	},
	text: {
		fontSize: 20,
		marginLeft: 10,
		marginRight: 10
	}
});