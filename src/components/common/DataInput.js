import React from 'react';
import { StyleSheet, View, Modal, Text, TouchableOpacity, TextInput, Keyboard } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const DataInput = ({ visible, title, desc, no, yes, onAccept, onDecline, weight, reps, setValueType, onChangeWeight, onChangeReps }) => {
    // const { containerStyle, textStyle, cardSectionStyle } = styles;
    const renderNo = status => {
        if (status) {
            return (
                <TouchableOpacity
                    style={styles.linearGradient2}
                    onPress={onDecline} >
                    <Text style={styles.buttonText}>{no}</Text>
                </TouchableOpacity>
            )
        }
    }

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={visible}
            onRequestClose={() => {
                //console.log('close modal');
            }}>
            <View style={styles.containerStyle}>
                <View style={styles.content}>
                    <Text style={styles.contentTitle}>{title}</Text>
                    <Text style={styles.contentDesc}>{desc}</Text>


                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={styles.contentDesc1}>Enter {setValueType == 'w' ? `weight` : "time"} : </Text>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <TextInput
                                //ref={input => { this.weights = input; }}
                                style={styles.textInputStyle}
                                placeholder={setValueType == 'w' ? "Kg" : "Sec"}
                                placeholderTextColor="grey"
                                keyboardType="numeric"
                                maxLength={4}
                                value={weight.toString()}
                                returnKeyType='next'
                                onChangeText={text => onChangeWeight(text.replace(/[^0-9]/g, ''))}
                                onSubmitEditing={Keyboard.dismiss}
                            />
                        </View>
                    </View>


                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={styles.contentDesc1}>Enter reps :      </Text>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <TextInput
                                //ref={input => { this.weights = input; }}
                                style={styles.textInputStyle}
                                placeholder="Reps"
                                placeholderTextColor="grey"
                                keyboardType="numeric"
                                maxLength={3}
                                value={reps.toString()}
                                returnKeyType='next'
                                onChangeText={text => onChangeReps(text.replace(/[^0-9]/g, ''))}
                                onSubmitEditing={Keyboard.dismiss}
                            />
                        </View>
                    </View>

                    <View style={styles.viewButtons}>
                        {renderNo(no)}
                       
                        <LinearGradient colors={ (weight != "" && reps != "") ?  ['#8c52ff', '#8c52ff']:['#767682', '#767682']} style={styles.linearGradient1}>
                            <TouchableOpacity
                                disabled={(weight != "" && reps != "") ? false : true}
                                onPress={onAccept}>
                                <Text style={styles.buttonText}>{yes}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    
                    </View>
                </View>
            </View>

        </Modal>
    );
};

const styles = StyleSheet.create({
    cardSectionStyle: {
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40,
    },
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    },
    content: {
        backgroundColor: 'white',
        padding: 20,
        width: wp('90%'),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'relative',
        alignContent: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
        fontSize: 16,
        textAlign: 'left',
        color: '#1c1c1c',
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    contentDesc: {
        fontSize: 13,
        textAlign: 'left',
        color: '#9b9a9f',
        marginTop: 5,
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Regular',
        fontWeight: '500',
        marginBottom: 5
    },
    linearGradient1: {
        width: '25%',
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    linearGradient2: {
        width: '25%',
        height: 35,
        borderRadius: 7,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    buttonText: {
        fontSize: 13,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewButtons: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginTop: 15,
    },
    viewButtons1: {
        flexDirection: 'column',
        alignSelf: 'flex-end',
    },
    fullView: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#8c52ff',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 20,
        marginBottom: 5,
    },
    fullView1: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 5,
        marginBottom: 5,
    },
    textInputStyle: {
        height: 40,
        //flex: 1,
        fontSize: 15,
        //marginLeft: 15,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        //borderWidth: 1,
        //alignSelf: 'flex-start',
        width: 70,
        //justifyContent: 'flex-start',
        //backgroundColor: 'red',
        marginTop: -4
    },
    contentDesc1: {
        fontSize: 15,
        textAlign: 'left',
        //color: '#9b9a9f',
        marginTop: 5,
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Regular',
        fontWeight: '500',
        flex: 1
    },
    InputText: {
        marginRight: 10
    }
});
export { DataInput };
