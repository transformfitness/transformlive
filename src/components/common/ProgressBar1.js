import React from 'react';
import { StyleSheet, View, Modal, ActivityIndicator, Dimensions, Text } from 'react-native';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const ProgressBar1 = ({ loading, progress }) => {
    const barWidth = Dimensions.get('screen').width - 30;
    const progressCustomStyles = {
        backgroundColor: '#8c52ff',
        borderRadius: 10,
        borderColor: '#d6d5dd',
    };
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() => {
                //console.log('close modal');
            }}>
            <View style={styles.modalBackground}>
                <View style={styles.proBg}>
                    <Text style={styles.txtPlsWait}>Sending...</Text>
                    {/* <View style = {{flexDirection:"row"}}> */}

                    <Text style={styles.uploadingText}>{`${progress}`}% Completed</Text>

                    {/* </View> */}

                    {/* <ActivityIndicator animating={loading} size="large" /> */}
                </View>
            </View>
        </Modal>
    );
};


const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        // backgroundColor: '#00000040',
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: '100%',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    barStyle: {
        backgroundColor: '#06a283',
    },
    proBg: {
        width: '60%',
        backgroundColor: '#FFFFFF',
        padding: 10,
        height: 90,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },

    uploadingText: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '400',
        marginTop: 10,
    },
    txtPlsWait: {
        width: wp('90%'),
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'center',
        marginBottom: 5,
    },
});

export { ProgressBar1 };
