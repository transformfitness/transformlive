import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";


  const IconUserInactive = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0 -0 17 17.00000000000037" style={styles.path35}>
        <Path
          strokeWidth={0}
          fill="rgba(214,217,224,1)"
          d="M16.99 16.15 C16.99 16.15 16.14 17.00 16.14 17.00 L0.86 17.00 C0.86 17.00 0.01 16.15 0.01 16.15 C2.84 16.15 -0.27 10.20 8.50 10.20 C17.27 10.20 14.16 16.15 16.99 16.15 Z M8.50 8.50 C6.15 8.50 4.25 6.60 4.25 4.25 C4.25 1.90 6.15 0.00 8.50 0.00 C10.85 0.00 12.75 1.90 12.75 4.25 C12.75 6.60 10.85 8.50 8.50 8.50 Z"
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  path35: {
    width: 17,
    height: 17,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 3,
    marginLeft: 4
  }
});

export {IconUserInactive};
