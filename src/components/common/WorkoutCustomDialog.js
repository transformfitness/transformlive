import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const WorkoutCustomDialog = ({ visible, title, desc, no, yes, onAccept, onDecline,third, onComplete }) => {
    const renderNo = status => {
        if (status) {
            return (
                <TouchableOpacity
                    // style={styles.linearGradient2}
                    onPress={onDecline} >
                    <Text style={styles.buttonText}>{no}</Text>
                </TouchableOpacity>
            )
        }
    }

    const renderComplete = status => {
        if (status) {
            return (
                <TouchableOpacity
                    // style={styles.linearGradient2}
                    onPress={onComplete} >
                    <Text style={styles.buttonText}>{third}</Text>
                </TouchableOpacity>
            )
        }
    }

    return (

        <Modal
            isVisible={visible}
            hideModalContentWhileAnimating={true}
            coverScreen={true}
            animationInTiming={200}
            animationOutTiming={100}
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
        >
            <View style={styles.content}>
                {/* <Text style={styles.contentTitle}>{title}</Text> */}
                <Text style={styles.contentDesc}>{desc}</Text>

                <View style={styles.viewButtons}>
                {renderComplete(third)}
                    {/* <LinearGradient colors={['#8b63e6', '#8c52ff']} style={styles.linearGradient1}> */}
                        <TouchableOpacity
                            onPress={onAccept}>
                            <Text style={styles.buttonText}>{yes}</Text>
                        </TouchableOpacity>
                    {/* </LinearGradient> */}

                    {renderNo(no)}
                </View>


            </View>
        </Modal>
    );
};

const styles = {
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    },
    content: {
        backgroundColor: 'white',
        padding: 20,
        width: wp('85%'),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center',
        borderRadius: 2,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
        fontSize: 16,
        textAlign: 'left',
        color: '#1c1c1c',
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    contentDesc: {
        fontSize: 14,
        textAlign: 'left',
        color: '#576062',
        marginTop: 5,
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Regular',
        fontWeight: '500',
    },
    linearGradient1: {
        width: '25%',
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    linearGradient2: {
        width: '25%',
        height: 35,
        borderRadius: 7,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    buttonText: {
        fontSize: 13,
        textAlign: 'right',
        marginTop: 15,
        marginBottom: 10,
        color: '#3a6a80',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
        letterSpacing: 0.23,
    },
    viewButtons: {
        flexDirection: 'column',
        alignSelf: 'flex-end',
        marginTop: 20,
    },
    viewButtons1: {
        flexDirection: 'column',
        alignSelf: 'flex-end',
    },
    fullView: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#8c52ff',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 20,
        marginBottom: 5,
    },
    fullView1: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 5,
        marginBottom: 5,
    },
};

export { WorkoutCustomDialog };




