import React from 'react';
import { StyleSheet, View, Modal, ImageBackground, Text,TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';

const NoInternet = ({ loading, image, onRetry }) => {
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() => {
                //console.log('close modal');
            }}>
            <ImageBackground source={image} style={styles.mainContainer}>
                <View style={styles.container}>
                    <Text style={styles.title}>No Connection</Text>
                    <Text style={styles.desc}>Your internet connection was interrupted, Please retry.</Text>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                        <TouchableOpacity
                            onPress={onRetry}>
                            <Text style={styles.buttonText}>RETRY</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            </ImageBackground>
        </Modal>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flexDirection: 'column',
        position: 'absolute',
        bottom: 0,
        margin: 50,
    },
    title: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
    },
    desc: {
        fontSize: 13,
        fontWeight: '500',
        color: '#6d819c',
        lineHeight: 25,
        marginTop: 15,
        textAlign: 'left', 
        fontFamily: 'Rubik-Medium',
    },
    linearGradient1: {
        width: '28%',
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        position: 'relative',
        left: 0,
        marginTop: 30,
        alignSelf: 'flex-start',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
    },
    buttonText: {
        fontSize: 13,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
});

export { NoInternet };
