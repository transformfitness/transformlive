import React from 'react';
import { StyleSheet, View, Modal, Text, TouchableOpacity, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SliderBox } from "react-native-image-slider-box";
import Video from 'react-native-video';
import { WebView } from 'react-native-webview';
import { Platform } from 'react-native';

const PopupDialog = ({ visible, image, imageArr, thumbnail, mediaType, desc, no, yes, ok, onAccept, onDecline }) => {
  // const { containerStyle, textStyle, cardSectionStyle } = styles;
  let imagesArray = [];
  imageArr.map((item) => {
    imagesArray.push(item.img_url);
  })

  const renderNo = status => {
    if (status) {
      return (
        <TouchableOpacity
          style={styles.linearGradient2}
          onPress={onDecline} >
          <Text style={styles.contentDesc}>{no}</Text>
        </TouchableOpacity>
      )
    }
  }

  return (
    <Modal
      visible={visible}
      transparent={true}
      animationType={'none'}
      onRequestClose={() => {
        //console.log('close modal');
      }}>
      <View style={styles.containerStyle}>
        <View style={styles.content}>
          {mediaType === 1
            ? (
              <View style={{
                // height:'65%'
              }}>
                {Array.isArray(imageArr) && imageArr.length
                  ?
                  (
                    <View style={{
                      height: hp('22%'),
                    }}>
                      <SliderBox
                        images={imagesArray}
                        sliderBoxHeight={200}
                        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                        dotColor="#8c52ff"
                        inactiveDotColor="#90A4AE"
                        // paginationBoxVerticalPadding={20}
                        autoplay
                        circleLoop
                        resizeMethod={'resize'}
                        resizeMode={'cover'}
                        paginationBoxStyle={{
                          position: "absolute",
                          bottom: 0,
                          padding: 0,
                          alignItems: "center",
                          alignSelf: "center",
                          justifyContent: "center",
                          paddingVertical: 10
                        }}
                        dotStyle={{
                          width: 0,
                          height: 0,
                          borderRadius: 5,
                          marginHorizontal: 0,
                          padding: 0,
                          margin: 0,
                          backgroundColor: "rgba(128, 128, 128, 0.92)"
                        }}
                        ImageComponentStyle={{
                          width: wp('80%'),
                          height: undefined,
                          justifyContent: 'center',
                          alignSelf: 'center',
                          borderTopLeftRadius: 10,
                          borderTopRightRadius: 10,
                          aspectRatio: 16 / 9,
                        }}
                        imageLoadingColor="#2196F3"
                      />
                    </View>

                  ) :
                  (
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      style={{
                        width: wp('80%'),
                        height: undefined,
                        justifyContent: 'center',
                        alignSelf: 'center',
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        aspectRatio: 1 / 1.2,
                      }}
                      source={{ uri: image }}
                    />

                  )}
              </View>

            ) : (

              <View style={{
                width: wp('80%'),
                height: hp('26%'),

              }}>
                {Platform.OS === 'android'
                  ? (
                    <WebView
                      javaScriptEnabled={true}
                      domStorageEnabled={true}
                      style={{
                        flex: 1,
                        width: wp('80%'),
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                        backgroundColor: 'black',
                      }}
                      source={{
                        uri: image
                      }}
                    />

                  ) : (
                    <Video
                      ref={videoPlayer => { this.videoPlayer = videoPlayer }}
                      source={{ uri: image }}
                      controls={true}
                      style={{
                        flex: 1,
                        width: wp('80%'),
                        // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                        backgroundColor: 'black',
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                      }}

                    />

                  )

                }



              </View>

              // <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center' }}>
              //   <Image
              //     progressiveRenderingEnabled={true}
              //     resizeMethod="resize"
              //     style={{
              //       width: wp('80%'),
              //       height: undefined,
              //       justifyContent: 'center',
              //       alignSelf: 'center',
              //       borderTopLeftRadius: 10,
              //       borderTopRightRadius: 10,
              //       aspectRatio: 16 / 9,
              //     }}
              //     source={{ uri: thumbnail }}
              //   />

              //   <View style={styles.playContainer1}>


              //     <TouchableOpacity
              //       activeOpacity={0.7}
              //       style={{ flexDirection: 'row', justifyContent: 'center' }}
              //       onPress={onDecline}>
              //       <Image
              //         progressiveRenderingEnabled={true}
              //         resizeMethod="resize"
              //         source={require('../../res/ic_play_purple.png')}
              //         style={styles.playIcon} />
              //     </TouchableOpacity>


              //   </View>
              // </View>

            )}

          <View style={styles.viewButtons}>
            {/* {renderNo(no)} */}
            <TouchableOpacity
              style={{ width: '50%', padding: 10 }}
              onPress={onDecline} >
              <Text style={styles.contentDesc}>{no}</Text>
            </TouchableOpacity>
            <View
              style={{ width: .5, height: '70%', backgroundColor: '#9b9a9f', alignSelf: 'center' }}>

            </View>

            {/* <LinearGradient colors={['#8b63e6', '#8c52ff']} style={styles.linearGradient1}> */}
            <TouchableOpacity
              style={{ width: '50%', padding: 10 }}
              onPress={onAccept}>
              <Text style={styles.buttonText}>{yes}</Text>
            </TouchableOpacity>
            {/* </LinearGradient> */}


          </View>
        </View>
      </View>

    </Modal>
  );
};

const styles = StyleSheet.create({
  cardSectionStyle: {
    justifyContent: 'center',
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40,
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center',

  },
  content: {
    backgroundColor: 'white',
    // padding: 20,
    width: wp('80%'),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    position: 'relative',
    alignContent: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    // height: hp('50%'),
  },
  contentTitle: {
    fontSize: 16,
    textAlign: 'left',
    color: '#1c1c1c',
    alignSelf: 'flex-start',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  contentDesc: {
    fontSize: 16,
    textAlign: 'center',
    color: 'grey',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    lineHeight: 30,
  },
  linearGradient1: {
    width: '100%',
    height: 45,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'relative',
    right: 0,
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 5,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    marginTop: 3.5,
  },
  linearGradient2: {
    width: '25%',
    height: 35,
    borderRadius: 7,
    justifyContent: 'center',
    position: 'relative',
    right: 0,
    backgroundColor: '#767682',
    marginLeft: 20,
    alignSelf: 'flex-end',
    marginBottom: 5,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 6,
    marginTop: 3.5,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#8c52ff',
    alignSelf: 'center',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
    lineHeight: 30,

    // fontSize: 16,
    // textAlign: 'center',
    // color: '#2d3142',
    // fontFamily: 'Rubik-Medium',
    // fontWeight: '500',
    // lineHeight: 30,
  },
  viewButtons: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  viewButtons1: {
    flexDirection: 'column',
    alignSelf: 'flex-end',
  },
  fullView: {
    width: wp('80%'),
    height: 40,
    borderRadius: 5,
    justifyContent: 'center',
    position: 'relative',
    right: 0,
    padding: 5,
    backgroundColor: '#8c52ff',
    marginLeft: 20,
    alignSelf: 'flex-end',
    marginTop: 20,
    marginBottom: 5,
  },
  fullView1: {
    width: wp('80%'),
    height: 40,
    borderRadius: 5,
    justifyContent: 'center',
    position: 'relative',
    right: 0,
    padding: 5,
    backgroundColor: '#767682',
    marginLeft: 20,
    alignSelf: 'flex-end',
    marginTop: 5,
    marginBottom: 5,
  },
  customSlide: {
    backgroundColor: '#1f1f1f',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  playContainer1: {
    flexDirection: 'row',
    alignContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  playIcon: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
export { PopupDialog };
