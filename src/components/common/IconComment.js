import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";

const IconComment = props => {
  return (
    <View style={[styles.root, props.style]}>
      <View style={styles.path14Stack}>
        <View style={styles.path14}></View>
        <Svg viewBox="-0 -0 16 14.85714285714286" style={styles.rectangle43}>
          <Path
            strokeWidth={0}
            fill="rgba(214,217,224,1)"
            d="M14.00 0.00 C14.00 0.00 16.00 0.00 16.00 2.00 C16.00 2.00 16.00 9.43 16.00 9.43 C16.00 9.43 16.00 11.43 14.00 11.43 C14.00 11.43 11.27 11.43 11.27 11.43 C11.25 11.53 11.23 11.59 11.23 11.59 L8.49 14.63 L7.51 14.63 L4.77 11.59 C4.77 11.59 4.73 11.52 4.69 11.43 C4.69 11.43 2.00 11.43 2.00 11.43 C2.00 11.43 0.00 11.43 0.00 9.43 C0.00 9.43 0.00 2.00 0.00 2.00 C0.00 2.00 0.00 0.00 2.00 0.00 Z"
          ></Path>
        </Svg>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  path14: {
    top: 0,
    left: 0,
    width: 15,
    height: 15,
    backgroundColor: "transparent",
    position: "absolute"
  },
  rectangle43: {
    top: 0,
    left: 0,
    width: 16,
    height: 15,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  path14Stack: {
    width: 16,
    height: 15,
    marginTop: 5,
    marginLeft: 4
  }
});

export {IconComment};
