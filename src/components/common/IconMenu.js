import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";

  const IconMenu = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0 -0 18 2" style={styles.path9}>
        <Path
          strokeWidth={0}
          fill="rgba(45,49,66,1)"
          d="M0.90 2.00 C0.40 2.00 0.00 1.55 0.00 1.00 C0.00 0.45 0.40 0.00 0.90 0.00 L17.10 0.00 C17.60 0.00 18.00 0.45 18.00 1.00 C18.00 1.55 17.60 2.00 17.10 2.00 Z"
        ></Path>
      </Svg>
      <Svg viewBox="-0 -0 14 2" style={styles.path10}>
        <Path
          strokeWidth={0}
          fill="rgba(45,49,66,1)"
          d="M0.70 2.00 C0.31 2.00 0.00 1.55 0.00 1.00 C0.00 0.45 0.31 0.00 0.70 0.00 L13.30 0.00 C13.69 0.00 14.00 0.45 14.00 1.00 C14.00 1.55 13.69 2.00 13.30 2.00 Z"
        ></Path>
      </Svg>
      <Svg viewBox="-0 -0 18 2" style={styles.path11}>
        <Path
          strokeWidth={0}
          fill="rgba(45,49,66,1)"
          d="M0.90 2.00 C0.40 2.00 0.00 1.55 0.00 1.00 C0.00 0.45 0.40 0.00 0.90 0.00 L17.10 0.00 C17.60 0.00 18.00 0.45 18.00 1.00 C18.00 1.55 17.60 2.00 17.10 2.00 Z"
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  path9: {
    width: 18,
    height: 2,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 5,
    marginLeft: 3
  },
  path10: {
    width: 14,
    height: 2,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 4,
    marginLeft: 3
  },
  path11: {
    width: 18,
    height: 2,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 4,
    marginLeft: 3
  }
});

export {IconMenu};
