import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";


  const IconHeartLight = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0 -0 15.00003 13.74999361218741" style={styles.icon4}>
        <Path
          strokeWidth={0}
          fill="rgba(214,217,224,1)"
          d="M7.61 13.75 C7.03 13.75 6.47 13.50 6.08 13.06 L1.31 7.65 C0.24 6.53 -0.62 4.43 0.59 2.26 C1.24 1.11 2.26 0.32 3.40 0.08 C4.61 -0.17 5.86 0.15 6.85 0.96 C7.13 1.19 7.38 1.46 7.59 1.74 C7.77 1.49 7.98 1.26 8.21 1.05 C9.06 0.28 10.16 -0.06 11.30 0.07 C13.15 0.29 14.65 1.81 14.94 3.78 C15.16 5.20 14.77 6.47 13.77 7.67 L9.14 13.05 C8.75 13.49 8.20 13.75 7.61 13.75 Z"
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  icon4: {
    width: 15,
    height: 14,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 5,
    marginLeft: 4
  }
});

export {IconHeartLight};
