import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";

  const IconExploreInactive = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0 -0 16.333333 19.39583283101879" style={styles.path36}>
        <Path
          strokeWidth={0}
          fill="rgba(214,217,224,1)"
          d="M8.17 0.00 C10.42 0.00 12.25 4.08 12.25 4.08 L12.25 11.23 C12.25 11.23 10.42 15.31 8.17 15.31 C5.91 15.31 4.08 11.23 4.08 11.23 L4.08 4.08 C4.08 4.08 5.91 0.00 8.17 0.00 Z M0.00 0.00 L0.00 0.00 Z"
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  path36: {
    width: 16,
    height: 19,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 2,
    marginLeft: 4
  }
});

export {IconExploreInactive};
