import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    FlatList,
    Dimensions,
    Image,
    KeyboardAvoidingView,
    BackHandler,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25)
import Slider from '@react-native-community/slider';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
import { CustomDialog, NoInternet } from './common';
let weight = '';

class TargetWeight extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sliderValue: 50,
            maxSliderValue: 0,
            minSliderValue: 0,
            arrSubCatList: [],
            noDataMsg: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            goalSubId: 0,
            isInternet: false,
        };
    }

    async componentDidMount() {
        try {

            allowFunction();
            StatusBar.setHidden(true);
            weight = await AsyncStorage.getItem('weight_value');
            var value = parseInt(weight);

            if (this.props.gCode === 'WL' || this.props.gCode === 'PP') {
                this.setState({ maxSliderValue: value, sliderValue: value });
            }
            else if (this.props.gCode === 'WG') {
                if (value < 150) {
                    this.setState({ minSliderValue: value, maxSliderValue: 150, sliderValue: value });
                }
                else {
                    this.setState({ minSliderValue: value, maxSliderValue: 200, sliderValue: value });
                }
            }

            if (this.props.gCode === 'RM') {
                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        this.getSubGoals();
                    }
                    else {
                        this.setState({ isInternet: true });
                    }
                });
            }

            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                if (this.props.isFocused) {
                    Actions.pop();
                } else {
                    this.props.navigation.goBack(null);
                }
                return true;
            });
        } catch (error) {
            console.log(error);
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async getSubGoals() {
        try {

            fetch(
                `${BASE_URL}/master/goalsubctg`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        // Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        g_id: await AsyncStorage.getItem('goalId'),
                    }),

                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    if (statusCode >= 200 && statusCode <= 300) {

                        if (data.data) {
                            this.setState({ loading: false, arrSubCatList: data.data });
                        }
                        else {
                            this.setState({ loading: false, noDataMsg: 'No data available' });
                        }
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
                });
        } catch (error) {
            console.log(error);
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onButtonPressed() {
        if (this.props.gCode === 'WL' || this.props.gCode === 'PP') {
            if (Math.floor(this.state.sliderValue) === Math.floor(weight)) {
                this.setState({ isAlert: true, alertMsg: 'Target weight cannot be equal to your weight.Please change your target weight' });
            } else {
                await AsyncStorage.setItem('trgt_weight', this.state.sliderValue.toString());
                await AsyncStorage.setItem('goalSubId', this.state.goalSubId.toString());

                LogUtils.firebaseEventLog('click', {
                    p_id: 106,
                    p_category: 'Registration',
                    p_name: 'TargetWeight',
                });
                Actions.actLevel();
            }
        }
        else if (this.props.gCode === 'WG') {
            if (Math.floor(this.state.sliderValue) === Math.floor(weight)) {
                this.setState({ isAlert: true, alertMsg: 'Desired weight cannot be equal to your weight.Please change your desired weight' });
            } else {
                await AsyncStorage.setItem('trgt_weight', this.state.sliderValue.toString());
                await AsyncStorage.setItem('goalSubId', this.state.goalSubId.toString());
                LogUtils.firebaseEventLog('click', {
                    p_id: 106,
                    p_category: 'Registration',
                    p_name: 'TargetWeight',
                });
                Actions.actLevel();
            }
        }
        else if (this.props.gCode === 'RM') {
            if (this.state.goalSubId !== 0) {
                await AsyncStorage.setItem('trgt_weight', '0');
                await AsyncStorage.setItem('goalSubId', this.state.goalSubId.toString());
                LogUtils.firebaseEventLog('click', {
                    p_id: 106,
                    p_category: 'Registration',
                    p_name: 'TargetWeight',
                });
                Actions.actLevel();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please select a marathon type' });
            }
        }
    }

    press = (hey) => {
        this.state.arrSubCatList.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalSubId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getSubGoals();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    renderSubGoalData() {
        if (this.props.gCode === 'WL' || this.props.gCode === 'PP') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your target weight?
                    </Text>
                    <Text style={styles.weighttextStyle}>
                        {Math.floor(this.state.sliderValue)} Kg
                    </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        thumbTintColor="#8c52ff"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                        What is your weight loss goal?
                </Text> */}
                </View>
            )
        }
        else if (this.props.gCode === 'WG') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your desired weight?
                    </Text>
                    <Text style={styles.weighttextStyle}>
                        {Math.floor(this.state.sliderValue)} Kg
                    </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={this.state.minSliderValue}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        thumbTintColor="#8c52ff"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                    What is your weight loss goal?
            </Text> */}
                </View>
            )
        }
        else if (this.props.gCode === 'RM') {
            return (
                <View>
                    <Text style={styles.subtextOneStyle}>
                        Select type of marathon you want to run?
                    </Text>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('45%') }}
                        data={this.state.arrSubCatList}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.press(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>

            )
        }
    }

    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
                <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textIndicator}>4 / 6</Text>
                    </View>

                    <View style={styles.optionInnerContainer}>
                        <Image
                            source={require('../res/ic_app.png')}
                            style={styles.appImageStyle}
                        />
                    </View>
                    <View style={styles.containericonStyle}>

                        {this.renderSubGoalData()}

                        {/* {this.props.gCode === 'WL' || this.props.gCode === 'PP'
                            ? (
                                <View>

                                    <Text style={styles.subtextOneStyle}>
                                        What is your target weight?
                                        </Text>
                                    <Text style={styles.weighttextStyle}>
                                        {Math.floor(this.state.sliderValue)} Kg
                                    </Text>
                                    <Slider
                                        style={styles.sliderImageStyle}
                                        minimumValue={0}
                                        maximumValue={this.state.maxSliderValue}
                                        minimumTrackTintColor="#8c52ff"
                                        maximumTrackTintColor="#000000"
                                        scrollEventThrottle={100}
                                        value={this.state.sliderValue}
                                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                                    />
                                    <Text style={styles.subtextOneStyle}>
                                        What is your weight loss goal?
                                    </Text>
                                </View>
                            )
                            : (
                                <Text style={styles.subtextOneStyle}>
                                    Select type of marathon you want to run?
                                </Text>
                            )
                        } */}

                        {/* <FlatList
                            contentContainerStyle={{ paddingBottom: hp('45%') }}
                            data={this.state.arrSubCatList}
                            keyExtractor={item => item.id}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                    this.press(item)
                                }}>
                                    <View >
                                        <Text style={styles.countryText}>{`${item.name}`}</Text>
                                    </View>
                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.press(item)
                                                }}>
                                                    <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.press(item)
                                                }}>
                                                    <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </TouchableOpacity>
                            }} /> */}
                    </View>




                    <View style={styles.viewBottom}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                onPress={() => this.onButtonPressed()}>
                                <Text style={styles.buttonText}>Next</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </ImageBackground>
            </KeyboardAvoidingView >
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        marginLeft: 15,
        marginRight: 15,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: 40,
    },
    textStyle: {
        fontSize: 18,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('3%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        marginBottom: hp('2%'),
        fontFamily: 'Rubik-Medium',
    },
    sliderImageStyle: {
        width: wp('90%'),
        height: hp('5%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    weighttextStyle: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'left',
        marginLeft: wp('10%'),
        alignSelf: 'flex-start',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: {
        width: 25,
        height: 25,
        position: 'relative',
        alignSelf: 'center',
        marginRight: 10
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    textIndicator: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
});

const mapStateToProps = state => {


    const { subGoals } = state.masters;
    const loading = state.masters.loading;
    const error = state.masters.error;
    return { loading, error, subGoals };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(TargetWeight),
);