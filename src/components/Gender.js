import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mobileNumberChanged, getOtp } from '../actions';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';

class Gender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      isMaleFemale: -1,
      isAlert: false,
      alertMsg: '',
    };
  }

  async componentDidMount() {
    allowFunction();
    StatusBar.setHidden(true);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.isFocused) {
        Actions.pop();
      } else {
        this.props.navigation.goBack(null);
      }
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onMobileNumberChanged(text) {
    this.props.mobileNumberChanged(text);
  }

  onBackPressed() {
    Actions.pop();
  }

  onGenderClicked() {

  }

  async onButtonPressed() {
    if (this.state.isMaleFemale === -1) {
      this.setState({ isAlert: true, alertMsg: 'Please select your gender' });
    } else {
      await AsyncStorage.setItem('genderId', this.state.isMaleFemale.toString());
      LogUtils.firebaseEventLog('click', {
        p_id: 103,
        p_category: 'Registration',
        p_name: 'Gender',
      });
      Actions.heightnew();
    }
  }

  renderGenderMaleFemale() {
    if (this.state.isMaleFemale === -1) {
      return (
        <View style={styles.containerGender}>
          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 1 })}>
            <View style={styles.containerMaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/uncheck.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/male.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Male</Text>
            </View>

          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 2 })}>

            <View style={styles.containerFemaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/uncheck.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/female.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Female</Text>
            </View>
          </TouchableOpacity>

        </View>
      );
    } else if (this.state.isMaleFemale === 1) {
      return (
        <View style={styles.containerGender}>
          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 1 })}>
            <View style={styles.containerMaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/check.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/male.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Male</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 2 })}>
            <View style={styles.containerFemaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/uncheck.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/female.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Female</Text>
            </View>
          </TouchableOpacity>

        </View>


      );
    } else if (this.state.isMaleFemale === 2) {
      return (
        <View style={styles.containerGender}>
          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 1 })}>
            <View style={styles.containerMaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/uncheck.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/male.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Male</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({ isMaleFemale: 2 })}>

            <View style={styles.containerFemaleStyle}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/check.png')}
                style={styles.mobileImageStyle}
              />
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/female.png')}
                style={styles.genImg}
              />
              <Text style={styles.countryText}>Female</Text>
            </View>
          </TouchableOpacity>

        </View>
      );
    }
  }

  async onAccept() {
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
          <Loader loading={this.props.loading} />
          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <Text style={styles.textIndicator}>2 / 4</Text>
          </View>

          <View style={styles.optionInnerContainer}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>Which one are you?</Text>

            {this.renderGenderMaleFemale()}

            <Text style={styles.desc}>To give you a better experience we need to know your gender</Text>

          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onButtonPressed()}>
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    mobileNumber: state.auth.mobileNumber,
    error: state.auth.error,
    loading: state.auth.loading,
  };
};

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  subtextOneStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  containerGender: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: hp('4%'),
  },
  containerMaleStyle: {
    width: wp('40%'),
    height: hp('25%'),
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  mobileImageStyle: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 20,
    right: 0,
    marginRight: 20
  },
  genImg: {
    height: 40,
    width: 35,
    alignSelf: 'center',
    marginTop: hp('3%'),
  },
  countryText: {
    alignSelf: 'center',
    fontSize: 14,
    marginTop: hp('2%'),
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: 0.26,
    fontFamily: 'Rubik-Medium',
  },
  containerFemaleStyle: {
    width: wp('40%'),
    height: hp('25%'),
    left: wp('5%'),
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('3%'),
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 10,
  },
  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },

});

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { mobileNumberChanged, getOtp },
  )(Gender),
);
