import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { getTraineeGoals } from '../actions';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

class Trainee_Help_Success extends Component {

    constructor(props) {
        super(props);
       
        this.state = {
            visible: false,
        };
    }

    async componentDidMount() {
        allowFunction();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.appointments();
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <View style={styles.mainView}>
                    {/* title, weight  and chart details */}
                    <TouchableOpacity
                        style={{ width: 40, height: 40, }}
                        onPress={() => this.onBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={require('../res/img_customer_help.png')}
                        style={styles.imgNoSuscribe}
                    />
                    <Text style={styles.textNodataTitle}>{this.props.title}</Text>

                    <Text style={styles.textNodata}>{this.props.infoText}</Text>

                    <View style={styles.bottom}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                onPress={() => Actions.appointments()}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>Go to Appointments</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>

                </View>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    mainView: {
        flexDirection: 'column',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 20,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },


});

const mapStateToProps = state => {
    // const { goals, trainerCodes } = state.procre;
    // const loading = state.procre.loading;
    // const error = state.procre.error;
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(Trainee_Help_Success),
);

// export default ProfileCreation;