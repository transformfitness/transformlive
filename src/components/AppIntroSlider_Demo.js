import React,{Component,useState,useEffect} from 'react';
import {
  SafeAreaView, StyleSheet, View, Text, Image,Platform,PixelRatio
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import AppIntroSlider from 'react-native-app-intro-slider';
import LogUtils from '../utils/LogUtils.js';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign';


class AppIntroSlider_Demo extends Component {
  constructor(props){
    super(props);
    this.state= {
      showRealApp:true,
      sliderImages:[],
      pixelRatio:1,
      slideArray:[]
    }
  }

  async componentDidMount(){
    try{
      var info_android = await AsyncStorage.getItem('info_android');
      var info_ios = await AsyncStorage.getItem('info_ios');
      var pixelRatio = PixelRatio.get();
      if(Platform.OS=='android'){
        this.setState({
          sliderImages:JSON.parse(info_android),
          showRealApp:false
        })
      }else{
        this.setState({
          sliderImages:JSON.parse(info_ios),
          showRealApp:false
        })
      }

      //Know the pixel ration of device 
      if(pixelRatio < 2){
        this.setState({pixelRatio:0})
      }else if(pixelRatio > 2 && pixelRatio < 3 ){
        this.setState({pixelRatio:1})
      }else if(pixelRatio > 3){
        this.setState({pixelRatio:2})
      }

      var slides = [];
      Object.values(this.state.sliderImages[this.state.pixelRatio]).map((img,index)=>{
        if(img!=null){
          var data = {
            key:index,
            image:img
          }
          slides.push(data);
        }
      })

      this.setState({
        slideArray:slides
      })
      
    }catch(error){
      console.log(error);
    }
  }


  onDone = () => {
    LogUtils.firebaseEventLog('click', {
      p_id: 115,
      p_category: 'Registration',
      p_name: 'UserGoalDetails',
  });
    Actions.thome({ type: 'reset' });
  }

  renderItem = ({item}) => {
    return (
      <View>
        <Image
          style={styles.introImageStyle}
          source={{ uri: item.image }}
        />
      </View>
    )
  }

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="right"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="right"
          color="rgba(255, 255, 255, .9)"
          size={24}
        />
      </View>
    );
  };

  render(){
    return (
      <>
      {
        this.state.showRealApp ? 
        (
         <View />
        ) : (
          <AppIntroSlider 
            slides={this.state.slideArray}
            renderItem={this.renderItem}
            onDone={this.onDone}
            // onSkip={onSkip}
            // showSkipButton={true}
            dotStyle={{ backgroundColor: '#8c52ff' }}
            activeDotStyle={{ backgroundColor: 'white' }}
            bounces={true}
            buttonTextStyle={{fontFamily: 'Rubik-Regular',}}
            renderDoneButton={this._renderDoneButton}
            renderNextButton={this._renderNextButton}
          />
        )
      }
        
      </>
    );
  }
}

export default AppIntroSlider_Demo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  titleStyle: {
    padding: 10,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  introTitleStyle:{
    fontSize:5,
    color:'white',
    textAlign:'center',
    marginBottom:16,
    fontWeight:'bold'
  },
  introImageStyle:{
    width: wp('100%'),
    height: hp('100%'),
  },
  introTextStyle:{
    fontSize:18,
    color:'white',
    textAlign:'center',
    paddingVertical:30
  },
  buttonCircle: {
    width: 38,
    height: 38,
    backgroundColor: 'rgba(0, 0, 0, .2)',//rgba(0, 0, 0, .2)
    borderRadius: 19,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:6
  },
});
