import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Alert,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
} from 'react-native';
import { connect } from 'react-redux';

import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
} from 'react-native-popup-dialog';
import { Calendar, CalendarList, Arrow, Agenda } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
LocaleConfig.locales['en'] = {
    monthNames: ['January', 'February ', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'en';
import moment from "moment";
import DeviceInfo from 'react-native-device-info';

import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { allowFunction } from '../utils/ScreenshotUtils.js';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getToDate() {
    let now = new Date()
    //console.log('Today: ' + now.toUTCString())
    let next60days = new Date(now.setDate(now.getDate() + 13))
    //console.log('Next: ' + next60days)

    return next60days;
}

class Trainee_Help_Book extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            noDataMsg: '',
            msgSelectDate: 'Please select date...',
            arrAllTrainers: [],
            selected: '',
            visible: false,
            day: '',
            month: '',
            year: '',
            confirmPop: false,
            isInternet: false,
            dateConString: '',
            arrTrainerSlots: [],
            selTrID: 0,
            selSlotObj: {},
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {

                this.setState({ selTrID: this.props.selItem.hq_id, arrTrainerSlots: [], selSlotObj: {} });
                var date = moment(new Date()).format("YYYY-MM-DD");
                var today = new Date();
                let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                this.setState({ selected: date });
                this.setState({ day: today.getDate() });
                this.setState({ month: parseInt(today.getMonth() + 1) });
                this.setState({ year: today.getFullYear() });
                var gsDayNames = [
                    'Sunday',
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday'
                ];
                var gsMonNames = [
                    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ];
                var d = new Date(date);
                var dayName = gsDayNames[d.getDay()];
                var monName = gsMonNames[d.getMonth()];
                this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });

                this.getAllTrainerSlots();
            }
            else {
                this.setState({ isInternet: true });

            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
        // Actions.pop();
    }


    async getAllTrainerSlots() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getcallcenterslots`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    // tr_id: this.state.selTrID,
                    seldate: this.state.selected,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        })
                        this.setState({ loading: false, arrTrainerSlots: data.data });
                    }
                    else {
                        this.setState({ loading: false, arrTrainerSlots: [], msgSelectDate: 'No slots available for the selected date' });
                    }

                    // console.log('Res Obj', data.data)
                } else {
                    this.setState({ arrTrainerSlots: [] });

                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async bookAppointment() {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        // console.log(JSON.stringify({
        //     seldate: this.state.selected,
        //     from_time: this.state.selSlotObj.from_time,
        //     to_time: this.state.selSlotObj.to_time,
        //     hq_id: this.state.selTrID,
        // }));
        // {"seldate":"2020-06-02","from_time":"16:20:00","to_time":"16:40:00"}
        fetch(
            `${BASE_URL}/trainee/savecallcenterslot`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    seldate: this.state.selected,
                    from_time: this.state.selSlotObj.from_time,
                    to_time: this.state.selSlotObj.to_time,
                    hq_id: this.state.selTrID,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    // console.log('Res Obj', data.data)
                    Actions.traHelpSucc({ title: data.title, infoText: data.message });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    ListEmptyView = () => {
        return (
            <View style={styles.viewNoData}>
                <Text style={styles.textNoData}>{this.state.noDataMsg}</Text>
            </View>

        );
    }

    ListSlotsEmptyView = () => {
        return (
            <View style={{
                padding: 10,
                marginTop: 10,
                height: 40,
            }}>
                <Text style={styles.textNoData}></Text>
                {/* <Text style={styles.textNoData}>{this.state.msgSelectDate}</Text> */}
            </View>
        );
    }

    onBackPressed() {
        if (this.state.confirmPop) {
            this.setState({ confirmPop: false });
        }
        else {
            Actions.pop();
        }

    }

    press = (hey) => {
        this.state.arrTrainerSlots.map((item) => {
            if (item.tas_id === hey.tas_id) {
                item.check = true;
                this.setState({ selSlotObj: item });
            }
            else {
                item.check = false;
            }
        })
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, visible: false, confirmPop: false });
                this.getAllTrainerSlots();
            }
            else {
                this.setState({ isInternet: true, visible: false, confirmPop: false });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Choose date and time</Text>
                    </View>
                    <Calendar
                        // Initially visible month. Default = Date()
                        current={this.state.selected}
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={new Date()}
                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                        maxDate={getToDate()}
                        // Handler which gets executed on day press. Default = undefined
                        onDayPress={(day) => {
                            //console.log('selected day', day);
                            this.setState({ selected: day.dateString });
                            this.setState({ day: day.day });
                            this.setState({ month: day.month });
                            this.setState({ year: day.year });
                            // const date = new Date(2019, 12, 10);  // 2009-11-10
                            // const month = date.toLocaleString('default', { month: 'short' });
                            // console.log(month);
                            var gsDayNames = [
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ];
                            var gsMonNames = [
                                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ];

                            var d = new Date(day.dateString);
                            var dayName = gsDayNames[d.getDay()];
                            var monName = gsMonNames[d.getMonth()];

                            // console.log(dayName, monName);
                            this.setState({ dateConString: dayName + ', ' + monName + ' ' + day.day + ', ' + day.year });

                            this.getAllTrainerSlots();
                        }}

                        // Handler which gets executed on day long press. Default = undefined
                        // onDayLongPress={(day) => { console.log('selected day', day) }}
                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                        monthFormat={'MMM yyyy'}
                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                        onMonthChange={(month) => { console.log('month changed', month) }}
                        // Hide month navigation arrows. Default = false
                        //hideArrows={true}
                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                        // renderArrow={(direction) => (<Arrow />)}
                        // Do not show days of other months in month page. Default = false
                        //hideExtraDays={true}
                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                        // day from another month that is visible in calendar page. Default = false
                        //    disableMonthChange={true}
                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                        firstDay={1}
                        // Hide day names. Default = false
                        //    hideDayNames={true}
                        // Show week numbers to the left. Default = false
                        //    showWeekNumbers={true}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                        // onPressArrowLeft={substractMonth => substractMonth()}
                        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                        //onPressArrowRight={addMonth => addMonth()}
                        // Disable left arrow. Default = false
                        //disableArrowLeft={true}
                        // Disable right arrow. Default = false
                        //disableArrowRight={true}
                        markedDates={{ [this.state.selected]: { selected: true, disableTouchEvent: true, selectedDotColor: '#8c52ff' } }}
                        theme={{
                            // calendarBackground: '#333248',
                            // textSectionTitleColor: 'white',
                            // dayTextColor: 'red',
                            todayTextColor: '#8c52ff',
                            selectedDayTextColor: 'white',
                            // monthTextColor: 'white',
                            // indicatorColor: 'white',
                            textDayFontSize: 12,
                            textMonthFontSize: 12,
                            textMonthFontFamily: 'Rubik-Regular',
                            textDayHeaderFontFamily: 'Rubik-Regular',
                            textDayHeaderFontSize: 12,
                            selectedDayBackgroundColor: '#8c52ff',
                            arrowColor: '#8c52ff',
                            // textDisabledColor: 'red',
                            'stylesheet.calendar.header': {
                                week: {
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }
                            }
                        }}
                    />

                    <Text style={styles.selDateText}>{this.state.dateConString}</Text>
                    <FlatList
                        // style={{ alignSelf:'center'}}
                        contentContainerStyle={{ paddingBottom: hp('15%'), justifyContent: 'center', alignSelf: 'center', marginTop: 10 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrTrainerSlots}
                        keyExtractor={item => item.tas_id}
                        ItemSeparatorComponent={this.FlatListItemSeparator1}
                        ListEmptyComponent={this.ListSlotsEmptyView}
                        numColumns={2}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={{
                                width: wp('45%'),
                                marginRight: 0,
                                marginLeft: wp('2.5%'),
                                marginBottom: 10,
                                alignSelf: 'flex-start',

                            }} onPress={() => { this.press(item) }}>
                                {item.check
                                    ? (
                                        <View style={{
                                            borderColor: '#8c52ff', borderRadius: 5,
                                            borderWidth: 1,
                                            height: 40,
                                            justifyContent: 'center',
                                            backgroundColor: '#8c52ff'
                                        }}>
                                            <Text style={styles.textWorkNameChecked}>{`${item.schedule_time}`}</Text>

                                        </View>

                                    )
                                    : (

                                        <View style={{
                                            borderColor: '#ddd', borderRadius: 5,
                                            borderWidth: 1,
                                            height: 40,
                                            justifyContent: 'center',
                                            backgroundColor: '#ffffff'

                                        }}>
                                            <Text style={styles.textWorkName}>{`${item.schedule_time}`}</Text>

                                        </View>

                                    )}
                            </TouchableOpacity>



                            // <TouchableOpacity style={styles.containerMaleStyle} onPress={() => {
                            //     this.press(item)
                            // }}>
                            //     <View >
                            //         <Text style={styles.countryText}>{`${item.schedule_time}`}</Text>
                            //     </View>
                            //     <View style={styles.mobileImageStyle}>
                            //         {item.check
                            //             ? (
                            //                 <TouchableOpacity onPress={() => {
                            //                     this.press(item)
                            //                 }}>
                            //                     <Image
                            //                         progressiveRenderingEnabled={true}
                            //                         resizeMethod="resize"
                            //                         source={require('../res/ic_rect_check.png')}
                            //                         style={styles.mobileImageStyle} />
                            //                 </TouchableOpacity>
                            //             )
                            //             : (
                            //                 <TouchableOpacity onPress={() => {
                            //                     this.press(item)
                            //                 }}>
                            //                     <Image
                            //                         progressiveRenderingEnabled={true}
                            //                         resizeMethod="resize"
                            //                         source={require('../res/ic_rect_uncheck.png')}
                            //                         style={styles.mobileImageStyle} />
                            //                 </TouchableOpacity>
                            //             )}
                            //     </View>
                            // </TouchableOpacity>
                        }} />

                </View>


                {/* confirm popup */}
                <Dialog
                    onDismiss={() => {
                        this.setState({ confirmPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ confirmPop: false });
                    }}
                    width={0.8}
                    // height={0.9}
                    // dialogStyle={{marginTop: 30,}}
                    visible={this.state.confirmPop}
                    // rounded
                    // actionsBordered
                    // dialogTitle={
                    //     <DialogTitle
                    //         title="Default Animation Dialog Simple"
                    //         style={{
                    //             backgroundColor: '#8c52ff',
                    //         }}
                    //         hasTitleBar={false}
                    //         align="left"
                    //     />
                    // }
                    footer={
                        <DialogFooter style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                            alignSelf: 'center',
                        }}>
                            {/* <View style={{
                                backgroundColor: '#ffffff',
                                alignItems: 'center',
                                alignSelf: 'center',
                                justifyContent: 'center',
                            }}>
                                
                            </View> */}

                            {/* <Text style={styles.textSave}>Save Changes</Text> */}

                            <DialogButton
                                text="Cancel"
                                bordered
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                }}
                                textStyle={{
                                    fontSize: 13, color: '#6d819c', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="Confirm"
                                bordered
                                textStyle={{
                                    fontSize: 13, color: '#8c52ff', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                    this.bookAppointment();
                                    // Actions.appSuccess();
                                }}
                                key="button-2"
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>
                                <Text style={{
                                    alignSelf: 'center', color: '#282c37', fontSize: 16,
                                    fontWeight: '500',
                                    textAlign: 'center',
                                    marginTop: 15,
                                    fontFamily: 'Rubik-Medium',
                                    letterSpacing: 0.49,
                                }}>Please confirm your appointment</Text>
                                <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    {/* <Image
                                        source={{ uri: this.state.selTraImg }}
                                        style={styles.profileImage1}>
                                    </Image> */}

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_app.png')}
                                        style={styles.profileImage1}
                                    />

                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 13,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{moment(this.state.selected, 'YYYY-MM-DD').format("DD-MM-YYYY")}</Text>
                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 5,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{this.state.selSlotObj.schedule_time}</Text>
                                </View>


                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <View style={styles.bootom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={() => {
                            if (this.state.selected) {
                                if (this.state.selSlotObj.tas_id) {
                                    // this.setState({ visible: false });
                                    this.setState({ confirmPop: true });
                                }
                                else {
                                    this.setState({ isAlert: true, alertMsg: 'Please select time slot to book appointment' });
                                }

                            }
                            else {
                                this.setState({ isAlert: true, alertMsg: 'Please select appointment data and time' });
                            }
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>BOOK</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>


                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    loadingBar: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        height: hp('10%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        padding: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    profileImage: {
        width: 70,
        height: 70,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 5,
        // width: wp('15%'),
        // height: hp('15%'),
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    profileImage: {
        width: 70,
        height: 70,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 5,
        // width: wp('15%'),
        // height: hp('15%'),
        alignSelf: 'center',
        resizeMode: "cover",
        justifyContent: 'center',
    },
    textWorkNameChecked: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#ffffff',
        padding: 10,
    },

    textWorkName: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
        padding: 10,
    },
    textTrainerName: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        alignSelf: 'center',
    },
    textStatus1: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#8c52ff',
        alignSelf: 'center',
    },
    textStatus: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#ffa66d',
        alignSelf: 'center',
    },

    textPrice: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        marginTop: 5,
        color: '#282c37',
    },
    viewChallDate: {
        flexDirection: 'column',
        alignContent: 'center',
        backgroundColor: '#F4F6FA',
        position: 'absolute',
        borderRadius: 5,
        bottom: -5,
        right: -5,
        margin: 3,
        alignItems: 'center',
    },
    textMonth: {
        fontSize: 6,
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        backgroundColor: '#8c52ff',
        textAlign: 'center',
        color: '#ffffff',
        borderRadius: 5,
        textAlign: 'center',
        alignSelf: 'center',
        paddingLeft: 5,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 5,
        letterSpacing: 0.23,
    },
    textDate: {
        fontSize: 17,
        width: wp('10%'),
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        textAlign: 'center',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        padding: 8,
        letterSpacing: 0.23,
    },
    viewFollowAround: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        marginTop: 5,
        justifyContent: 'center'
    },
    calTitle: {
        fontSize: 11,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        color: '#6d819c',
    },
    calDesc: {
        fontSize: 9,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
    },
    bootom: {
        width: '100%',
        flexDirection: 'row',
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        position: 'absolute',
        alignSelf: 'center',

    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textSelTime: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#1E1F20',
    },
    textDone: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#8c52ff',
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('5%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    countryText: {
        width: wp('80%'),
        alignSelf: 'center',
        fontSize: 11,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.5,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: { width: 15, height: 15, position: 'relative', alignSelf: 'center', marginRight: 10 },
    selDateText: {
        alignSelf: 'flex-start',
        fontSize: 11,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 15,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
    },
    profileImage1: {
        width: 200,
        height: 60,
        marginTop: 10,
        marginBottom: 10,
        // aspectRatio: 1,
        // backgroundColor: "#D8D8D8",
        // borderWidth: StyleSheet.hairlineWidth,
        // borderColor: "#979797",
        // borderRadius: 90 / 2,
        // width: wp('15%'),
        // height: hp('15%'),
        resizeMode: "cover",
        justifyContent: 'center',
    },
    viewNoData: {
        padding: 10,
        marginTop: Dimensions.get('window').height / 3,
        height: 40,
    },
    textNoData: {
        textAlign: 'center',
        marginLeft: 20,
        marginRight: 20,
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 22,
        letterSpacing: 0.1,
    },
    viewBlueDot: {
        width: 13,
        height: 13,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 6.5,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    linearGradient1: {
        width: '85%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 15,
    },

});

const mapStateToProps = state => {

    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(Trainee_Help_Book);
