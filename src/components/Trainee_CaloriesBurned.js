import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, CustomDialog, NoInternet } from './common';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import moment from "moment";
import { BarChart, LineChart, Grid, YAxis, XAxis } from 'react-native-svg-charts';
import LogUtils from '../utils/LogUtils.js';
import Dialog, {
    DialogContent,
    DialogFooter,
} from 'react-native-popup-dialog';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_CaloriesBurned extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isInternet: false,
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            resObj: {},
            fill: 0,
            firstName: '',
            stepArr: [],
            dialyGoalVisible: false,
            sucTitle: '',
            CaloriesValue: 0,
            min_range: 0,
            max_range: 0,
        };
        this.callService = this.callService.bind(this);
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.callService();

                const interval = setInterval(() => {
                    if (!isEmpty(this.state.resObj)) {
                        this.setState({ fill: this.state.resObj.cal_per })
                        var weights = [];
                        this.state.resObj.lw_cal_arr.map((item) => {
                            weights.push(item.cal_burn);
                        })
                        var conf = {
                            chart: {
                                type: 'column',
                                height: 150,
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            colors: ['#8c52ff'],
                            xAxis: {
                                visible: false,
                                // categories: [
                                //     '*',
                                //     '',
                                //     '6AM',
                                //     '',
                                //     '',
                                //     '',
                                //     '',
                                //     '12PM',
                                //     '',
                                //     '',
                                //     '',
                                //     '6PM',
                                //     '',
                                //     '',
                                //     '*'
                                // ],
                                crosshair: true,
                                lineWidth: 0,
                                minorGridLineWidth: 0,
                                lineColor: 'transparent',
                            },
                            yAxis: {
                                visible: false,
                                title: false,
                                labels: {
                                    enabled: false,
                                },
                            },
                            tooltip: {
                                // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                // footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.1,
                                    borderWidth: 0,
                                    borderRadius: 3,
                                    showInLegend: false,
                                },
                                allowPointSelect: false,
                            },
                            series: [{
                                enableMouseTracking: false,
                                name: '',
                                data: weights
                                // data: [150, 110, 28, 250, 98, 100, 230]

                            }]

                        }
                        LogUtils.infoLog(weights)
                        this.setState({ chartOptions: conf });
                        this.setState({ stepArr: weights });
                        clearInterval(interval);
                    }
                }, 100);
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.dialyGoalVisible) {
            this.setState({ dialyGoalVisible: false })
        } else {
            // Actions.popTo('traineeHome');
            Actions.pop();
        }
    }

    async callService() {
        this.setState({ loading: true })
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/usercaldetail`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data });
                    this.setState({ CaloriesValue: parseInt(this.state.resObj.cal_goal), min_range: parseInt(this.state.resObj.min_range), max_range: parseInt(this.state.resObj.max_range), });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: 'Something went wrong please try again later' });
            });
    }

    async updateDailyGoal() {
        this.setState({ dialyGoalVisible: false });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/updatecalintakelimit`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    cal_limit: this.state.CaloriesValue,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, sucMsg: data.message, sucTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.callService();

            }
            else {
                this.setState({ isInternet: true });
            }
        });

    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    jewelStyle = function (height) {
        if (height) {
            height = parseInt(height / 100);
            LogUtils.infoLog1("height", height);
            var val = height + '%';
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#8c52ff', height: val, position: 'absolute', bottom: 0,
            }
        }
        else {
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#ddd', height: 0, position: 'absolute', bottom: 0,
            }
        }

    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }
    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', sucTitle: '' });
        this.callService();
    }

    renderChart() {
        if (!isEmpty(this.state.resObj)) {
            const axesSvg = { fontSize: 10, fill: "#2d3142" };
            const verticalContentInset = { top: 10, bottom: 10 };
            const xAxisHeight = 30;

            return (
                <View style={{ height: 200, flexDirection: "row", marginTop: 20 }}>
                    <YAxis
                        data={this.state.stepArr}
                        style={{ flex: 0.1, marginBottom: xAxisHeight }}
                        contentInset={verticalContentInset}
                        svg={axesSvg}
                        numberOfTicks={5}
                    />
                    <View style={{ flex: 0.9, }}>
                        <BarChart
                            style={{ flex: 1 }}
                            data={this.state.stepArr}
                            contentInset={verticalContentInset}
                            spacingInner={0.5}
                            // spacing={0.2}
                            svg={{ strokeWidth: 1, fill: '#8c52ff', }}
                            numberOfTicks={5}
                            animate
                            animationDuration={5000}
                        >
                            <Grid direction={Grid.Direction.HORIZONTAL} />

                        </BarChart>
                    </View>
                </View>
            );
        }
        else {
            return (
                <View style={{
                    alignItems: 'center',
                    width: wp('90%'),
                    height: 150,
                    alignSelf: 'center',
                }}></View>
            );
        }
    }


    render() {

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <Loader loading={this.state.loading} />
                <ScrollView contentContainerStyle={{ paddingBottom: hp('5%') }}>

                    {/* arc chat and details    */}
                    <View style={styles.viewWalkChart}>
                        <TouchableOpacity
                            style={{
                                width: 40,
                                height: 40,
                                // marginTop: 40,
                                marginLeft: 20,
                                // marginBottom: 20,
                            }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                        <Text style={styles.textStyle}>You have burned</Text>
                        <View style={styles.row}>
                            <Text style={styles.textColorStyle}>{this.state.resObj.cal_burn} cal</Text>
                            <Text style={styles.textStyle}>today</Text>
                        </View>

                        <View style={styles.calViewDet}>
                            <View style={{
                                width: 130, height: 130,
                                position: 'absolute',
                                top: 30,
                                borderWidth: 3,
                                borderRadius: 65,
                                borderColor: '#e9e9e9',
                                borderStyle: 'dotted',
                            }}>

                            </View>
                            <AnimatedCircularProgress
                                size={160}
                                width={7}
                                fill={this.state.fill}
                                tintColor="#8c52ff"
                                arcSweepAngle={290}
                                rotation={215}
                                lineCap="round"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <View>
                                            <Image
                                                resizeMethod="resize"
                                                source={require('../res/ic_cal_burn.png')}
                                                style={{
                                                    width: 32,
                                                    height: 32,
                                                    alignSelf: 'center',
                                                }}
                                            />
                                            <Text style={styles.calTitleBig}> {this.state.fill}%</Text>
                                            <Text style={styles.calDesc}>of daily goals</Text>
                                        </View>

                                    )
                                }
                            </AnimatedCircularProgress>

                            <TouchableOpacity style={styles.calViewDetails}>
                                <Text style={styles.calDetail}></Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewCal}>
                            <View style={styles.column}>
                                <Text style={styles.mltextStyle}>
                                    {this.state.resObj.cal_burn} cal
                                </Text>
                                <Text style={styles.waterdranktextStyle}>
                                    Cal Burned
                                </Text>

                            </View>
                            <View style={styles.verLineView}>
                            </View>
                            <TouchableOpacity style={styles.column}
                                onPress={() => this.setState({ CaloriesValue: parseInt(this.state.resObj.cal_goal), dialyGoalVisible: true })}>
                                <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                    <Text style={styles.dialyGoaltextStyle}>
                                        {this.state.resObj.cal_goal}
                                    </Text>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_edit_food.png')}
                                        style={{ width: 15, height: 15, margin: 5, }}
                                    />
                                </View>

                                <Text style={styles.waterdranktextStyle}>
                                    Daily Goal
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>
                    {/* bar chat and details    */}
                    <View style={styles.viewChartBack}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={styles.statisticTextStyle}>Weekly Calories Burned</Text>
                        </View>
                        {this.renderChart()}
                        <View style={{ flexDirection: "row" }}>
                            <View

                                style={{ flex: 0.08, }}

                            />

                            <View style={{ flexDirection: 'row', marginTop: 10, flex: 1, alignSelf: 'flex-end', justifyContent: 'flex-end', backgroundColor: 'ddd' }}>
                                <FlatList
                                    data={this.state.resObj.lw_cal_arr}
                                    numColumns={7}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => index.toString()}
                                    style={{ alignSelf: 'flex-end' }}
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    ItemSeparatorComponent={this.FlatListItemSeparator}
                                    renderItem={({ item }) => {
                                        const date = item.cdate;
                                        return <View key={date} style={{ flexDirection: 'column', width: wp('12%'), marginLeft: 1, marginRight: 1, }}>

                                            <Text style={styles.chartextStyle}>
                                                {moment(date, 'DD-MM-YYYY').format("MMM D")}
                                            </Text>
                                        </View>
                                    }} />

                            </View>
                        </View>
                    </View>
                    <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>

                    <Text style={styles.insighttextStyle}>
                        Daily Calories Burned
                        </Text>
                    <View style={styles.horLineView}>
                    </View>
                    <FlatList
                        data={this.state.resObj.lw_cal_arr}
                        extraData={this.state}
                        keyExtractor={item => item.id}
                        // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <View >
                                <View style={styles.performanceRow}>

                                    <Image
                                        source={require('../res/ic_blue_dot.png')}
                                        style={{
                                            width: 10,
                                            height: 10,
                                            marginLeft: 10,
                                            marginTop: 5,
                                            alignSelf: 'flex-start',
                                        }}
                                    />

                                    <View style={styles.performancecolumn}>
                                        <Text style={styles.performancetextStyle}>
                                            {item.cdate}
                                        </Text>
                                        <Text style={styles.waterdranktextStyle}>
                                            {item.full_day_name}
                                        </Text>

                                    </View>

                                    <Text style={styles.performancePercenttextStyle}>
                                        {item.cal_burn} cal
                                    </Text>

                                </View>
                                <View style={styles.horLineView}>
                                </View>
                            </View>
                        }} />

                    <Dialog
                        onDismiss={() => {
                            this.setState({ dialyGoalVisible: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ dialyGoalVisible: false });
                        }}
                        width={0.8}
                        visible={this.state.dialyGoalVisible}
                        // rounded
                        // actionsBordered
                        // dialogTitle={
                        //     <DialogTitle
                        //         title="Default Animation Dialog Simple"
                        //         style={{
                        //             backgroundColor: '#8c52ff',
                        //         }}
                        //         hasTitleBar={false}
                        //         align="left"
                        //     />
                        // }
                        footer={
                            <DialogFooter style={{
                                backgroundColor: '#ffffff',
                                alignItems: 'center',
                                alignSelf: 'center',
                                borderRadius: 15,
                            }}>
                                <TouchableOpacity
                                    onPress={() => this.updateDailyGoal()}>

                                    <Text style={styles.textSave}>Make this my goal</Text>


                                </TouchableOpacity>

                            </DialogFooter>
                        }
                    >
                        <DialogContent
                            style={{
                                // backgroundColor: '#8c52ff'
                            }}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={{ flexDirection: 'column', padding: 15 }}>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_cal_burn.png')}
                                        style={{
                                            width: 80,
                                            height: 80,
                                            alignSelf: 'center',
                                            marginTop: 25,
                                            marginBottom: 25,
                                        }}
                                    />

                                    <Text style={styles.weighttextStyle}>
                                        Let's set your calorie goal for the day.
                                        </Text>
                                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>

                                        <TouchableOpacity onPress={() => {
                                            if (this.state.CaloriesValue > this.state.min_range) {
                                                this.setState({ CaloriesValue: Math.floor(this.state.CaloriesValue - 50, 1) });
                                            }

                                        }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_minus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>
                                        <View style={{ flexDirection: 'column' }}>
                                            <Text style={styles.textWeight}>{this.state.CaloriesValue}</Text>
                                            <Text style={styles.waterdranktextStyle}> calories per day </Text>
                                        </View>

                                        <TouchableOpacity
                                            onPress={() => {
                                                if (this.state.CaloriesValue < this.state.max_range) {
                                                    this.setState({ CaloriesValue: Math.floor(this.state.CaloriesValue + 50, 1) });
                                                }
                                            }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_plus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>



                                    </View>

                                </View>

                            </View>

                        </DialogContent>
                    </Dialog>

                </ScrollView>

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
                <CustomDialog
                    visible={this.state.isSuccess}
                    title={this.state.sucTitle}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    viewWalkChart: {
        width: '100%',
        flexDirection: 'column',
        padding: 20,
        backgroundColor: '#ffffff',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textColorStyle: {
        fontSize: 20,
        marginRight: 5,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    calViewDet: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 15,
        justifyContent: 'center'
    },
    calTitle: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calDesc: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
    },
    calViewDetails: {
        width: 65,
        borderRadius: 100,
        borderStyle: 'solid',
        padding: 5,
        marginTop: -35,
        backgroundColor: '#ffffff',
    },
    calTitleBig: {
        fontSize: 28,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
        marginTop: 8,
    },
    viewCal: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    mltextStyle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    verLineView: {
        borderWidth: 1,
        borderColor: '#ddd',
    },
    viewChartBack: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },
    container: {
        width: wp('90%'),
        height: 150,
        borderColor: '#000000',
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    statisticTextStyle: {
        fontSize: 18,
        fontWeight: '500',
        color: '#2d3142',
        alignSelf: 'flex-start',
        flex: 1,
        fontFamily: 'Rubik-Medium',
    },
    insighttextStyle: {
        fontSize: 18,
        marginTop: 5,
        fontWeight: '500',
        color: '#2d3142',
        padding: 20,
        fontFamily: 'Rubik-Medium',
    },
    chartextStyle: {
        fontSize: 10.7,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
    },
    performanceRow: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-start',
        marginLeft: 5,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 20,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    performancePercenttextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        marginRight: 30,
    },
    horLineView: {
        borderWidth: .5,
        borderColor: '#ddd',
        marginTop: 15,
        marginBottom: 15,

    },
    dialyGoaltextStyle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textSave: {
        width: '100%',
        fontSize: 11,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#8c52ff',
        marginBottom: 15,
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        overflow: 'hidden',
    },
    imgAddDelete: {
        width: 25,
        height: 25,
        marginRight: 15,
        marginLeft: 15,
        alignSelf: "center",
        tintColor: '#8c52ff',
    },
    textWeight: {
        alignSelf: 'center',
        color: '#2d3142',
        fontSize: 24,
        fontWeight: '400',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    weighttextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
    },
});

export default Trainee_CaloriesBurned;

// export default ProfileCreation;