import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mobileNumberChanged, getOtp } from '../actions';
import { NoInternet, Loader, CustomDialog } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  BackHandler,
  PermissionsAndroid,
  ImageBackground,
  StatusBar,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import RNExitApp from 'react-native-exit-app';

async function requestReadSmsPermission() {
  try {
    if (Platform.OS === 'android') {
      PermissionsAndroid.requestMultiple(
        [PermissionsAndroid.PERMISSIONS.CAMERA,
        // PermissionsAndroid.PERMISSIONS.READ_SMS,
        // PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          // PermissionsAndroid.PERMISSIONS.MANAGE_EXTERNAL_STORAGE
        ]
      ).then((result) => {
        if (result['android.permission.CAMERA']
          // && result['android.permission.READ_SMS']
          // && result['android.permission.RECEIVE_SMS']
          && result['android.permission.WRITE_EXTERNAL_STORAGE']
          && result['android.permission.READ_EXTERNAL_STORAGE']
          //&& result['android.permission.MANAGE_EXTERNAL_STORAGE'] 
          === 'granted') {
          // this.setState({
          //   permissionsGranted: true
          // });
        } else if (result['android.permission.CAMERA']
          // || result['android.permission.READ_SMS']
          // || result['android.permission.RECEIVE_SMS'] 
          || result['android.permission.WRITE_EXTERNAL_STORAGE']
          || result['android.permission.READ_EXTERNAL_STORAGE']
          // || result['android.permission.MANAGE_EXTERNAL_STORAGE'] 
          === 'never_ask_again') {
          // this.refs.toast.show('Please Go into Settings -> Applications -> APP_NAME -> Permissions and Allow permissions to continue');
        }
      });
    }
  } catch (err) {
    console.warn(err);
  }
}

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      isAlert: false,
      alertMsg: '',
      clickBack: false,
      isInternet: false,
      permissionsGranted: false,
      isShowContinue:true
    };
  }

  async componentDidMount() {
    try{
      
      allowFunction();
      StatusBar.setHidden(true);
      // LogUtils.firebaseEventLog('openScreen', {
      //   screenName: 'LoginForm',
      // });
      requestReadSmsPermission();
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.isFocused) {
          this.setState({ clickBack: true });
        } else {
          this.props.navigation.goBack(null);
        }
        return true;
      });
    }catch(error){
      console.log(error);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onMobileNumberChanged(text) {
    this.props.mobileNumberChanged(text);
  }

  onBackPressed() { }

  async onButtonPressed() {
    if (this.mobilevalidate(this.props.mobileNumber)) {

      LogUtils.firebaseEventLog('click', {
        p_id: 1,
        p_category: 'Login',
        p_name: 'OTP Requested',
      });

      NetInfo.fetch().then(state => {
        if (state.isConnected) {

          this.props.getOtp(this.state.mobile, "1");
        }
        else {
          this.setState({ isInternet: true });
        }
      });

    }
    else {
      this.setState({ alertMsg: 'Please enter a valid mobile number' });
      this.setState({ isAlert: true });
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  mobilevalidate(text) {
    const reg = /^[0]?[6789]\d{9}$/;
    if (reg.test(text) === false) {
      this.setState({
        mobilevalidate: false,
        telephone: text,
      });
      return false;
    } else {
      this.setState({
        mobilevalidate: true,
        telephone: text,
        message: '',
      });
      return true;
    }
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  async onAccept() {
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  async onExit() {
    this.setState({ clickBack: false });
    // BackHandler.exitApp();
    RNExitApp.exitApp();
  }

  async onNotExit() {
    this.setState({ clickBack: false });
  }

  onChanged(text) {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      }
      else {
        this.setState({ alertMsg: 'Please enter numbers only' });
        this.setState({ isAlert: true });
      }
    }
    this.setState({ mobile: newText });
    this.onMobileNumberChanged(newText);
  }

  render() {
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.containerStyle}
        onKeyboardDidShow={(frames)=>this.setState({isShowContinue:false})}
        onKeyboardDidHide={(frames)=>this.setState({isShowContinue:true})}
        enableOnAndroid={false}
        scrollEnabled={false}>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
          <Loader loading={this.props.loading} />
          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />

          </View>
          <TouchableOpacity
            onPress={() => this.onBackPressed()}>
            <Image
              source={require('../res/ic_back.png')}
              style={styles.backImageStyle}
            />
          </TouchableOpacity>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>Let’s start with your mobile number</Text>

            <View style={styles.containerMobileStyle}>
              <Image
                source={require('../res/india_flag.png')}
                style={styles.mobileImageStyle}
              />
              <Text style={styles.countryText}> +91 </Text>
            
                <Image
                  source={require('../res/ic_dropdown.png')}
                  style={styles.countryImageDropStyle}
                />

              <TextInput
                style={styles.textInputStyle}
                keyboardType="numeric"
                placeholder="Mobile Number"
                placeholderTextColor="grey"
                maxLength={10}
                value={this.props.mobileNumber}
                returnKeyType='done'
                onChangeText={(text) => this.onChanged(text)}
              />
            </View>
          </View>
          {
            this.state.isShowContinue &&
              <View style={styles.viewBottom}>
                <LinearGradient 
                colors={['#8c52ff', '#8c52ff']} 
                style={styles.linearGradient}
                >
                  <TouchableOpacity
                    onPress={() => this.onButtonPressed()}>
                    <Text style={styles.buttonText}>Continue</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
          }

          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

          <CustomDialog
            visible={this.state.clickBack}
            title='Alert'
            desc='Do you really want to exit?'
            onAccept={this.onExit.bind(this)}
            onDecline={this.onNotExit.bind(this)}
            no='No'
            yes='Yes' />
        </ImageBackground>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    mobileNumber: state.auth.mobileNumber,
    error: state.auth.error,
    loading: state.auth.loading,
  };
};

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  iconsView: {
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: 45,
    justifyContent: 'center',
  },
  back: {
    width: 25,
    height: 18,
    marginTop: 45,
    marginLeft: 20,
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  logo: {
    width: 70,
    height: 38,
    alignSelf: 'flex-end',
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerMobileStyle: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    alignSelf: 'center',
    height: 50,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  deleteImageStyle: {
    width: 20,
    height: 20,
    position: 'relative',
    alignSelf: 'center',
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    marginLeft: 10,
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: .5,
    fontFamily: 'Rubik-Regular',
  },
  subTextStyle: {
    alignSelf: 'center',
    fontSize: 12,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    color: '#ffffff',
    textAlign: 'center',
  },
  subtextOneStyle: {
    fontSize: 24,
    marginTop: hp('10%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },

  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  mobileImageStyle: { width: 25, height: 20, alignSelf: 'center', marginLeft: 20 },
  countryImageDropStyle: { width: 10, height: 7, alignSelf: 'center', marginTop: 3, },
  imageIconStyle: { width: 180, height: 98, alignSelf: 'center' },

  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: 15,
  },

  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  countryText: {
    padding: 5,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('7%'),
    marginLeft: 20,
    marginBottom: 20,
    opacity: 0,
    alignSelf: 'flex-start',
  },
  appImageStyle: {
    width: 300,
    height: 90,
    // marginTop: -45,
    marginBottom: 20,
  },
  topBarImageStyle: {
    width: wp('50%'),
    height: 8,
    marginBottom: 20,
    marginRight: 1,

  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    flex:1,
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 10,
  },
  content: {
    backgroundColor: 'white',
    padding: 20,
    width: wp('90%'),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 16,
    textAlign: 'left',
    color: '#1c1c1c',
    alignSelf: 'flex-start',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  contentDesc: {
    fontSize: 13,
    textAlign: 'left',
    color: '#9b9a9f',
    marginTop: 5,
    alignSelf: 'flex-start',
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
  },
  linearGradient1: {
    width: '20%',
    height: 30,
    borderRadius: 4,
    justifyContent: 'center',
    position: 'relative',
    right: 0,
    alignSelf: 'flex-end',
    marginTop: 10,
    marginBottom: 15,
  },
});

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { mobileNumberChanged, getOtp },
  )(LoginForm),
);
