import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    Platform,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { WebView } from 'react-native-webview';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

class Settings_Detail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
        };
    }

    async componentDidMount() {
        allowFunction();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }
    goBack = () => {
        this.webview.goBack();
    };

    renderData() {
        return (
            <WebView
                style={{ width: wp('100%'), height: hp('100%'), position: 'absolute', top: 0 }}
                onNavigationStateChange={this.log}
                source={{ uri: this.props.url }} />
        )
    }

    render() {
        return (
            <View style={styles.containerStyle}>


                <TouchableOpacity style={styles.viewTop}
                    onPress={() => this.onBackPressed()}>
                    <Image
                        source={require('../res/ic_back.png')}
                        style={styles.backImageStyle}
                    />
                </TouchableOpacity>

                {this.renderData()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 20,
        alignItems: 'center',
        padding: 5,
        justifyContent: 'center',
        width: 34,
        height: 34,
        borderRadius: 17,
        backgroundColor: 'white',
        zIndex: 100,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#8c52ff',
        alignSelf: 'center',
    },

});

const mapStateToProps = state => {

    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Settings_Detail),
);

// export default ProfileCreation;