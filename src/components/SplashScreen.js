/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, ImageBackground, TouchableOpacity, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { allowFunction } from '../utils/ScreenshotUtils.js';

class SplashScreen extends Component {
  async componentDidMount() {
    allowFunction();
    StatusBar.setHidden(true);
  }

  onButtonPressed() {
    Actions.auth({ type: 'reset' });
  }

  render() {
    return (
      <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
        <View style={styles.viewMain}>
          <Image
            source={require('../res/ic_app.png')}
            style={styles.imageIconStyle} />

          <Text style={styles.desc}>Push harder than yesterday if you want a different tomorrow</Text>
          <Image
            resizeMode="contain"
            source={require('../res/img_info.png')}
            style={styles.imageInfo} />
        </View>
        <View style={styles.viewBottom}>
          <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
            <TouchableOpacity
              onPress={() => this.onButtonPressed()}>
              <Text style={styles.buttonText}>Get Started</Text>
            </TouchableOpacity>
          </LinearGradient>

        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',

  },
  viewMain: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 20,
  },
  viewBottom: {
    width: wp('90%'),
    height: wp('30%'),
    flexDirection: 'column',
    alignContent: 'center',
    alignSelf: 'center',
  },
  imageIconStyle: {
    marginTop: 70, width: 300,
    height: 90, alignSelf: 'center'
  },
  desc: {
    fontSize: 14,
    marginTop: 30,
    marginLeft: 30,
    marginRight: 30,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 18,
    letterSpacing: 0.15,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
  },
  imageInfo: {
    // width: "100%",
    height: hp('40%'), // 70% of height device screen
    width: wp('100%'),
  },
  linearGradient: {
    width: '100%',
    height: 50,
    borderRadius: 24,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
});

export default SplashScreen;
