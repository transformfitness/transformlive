import React, {Component} from 'react';
import {connect} from 'react-redux';
import {mobileNumberChanged,getOtp} from '../actions';
import {Spinner, Loader} from './common';
import {withNavigationFocus} from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  BackHandler,
  Alert,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {getTraineeGoals} from '../actions';
import AsyncStorage from '@react-native-community/async-storage';

class DailyActivityLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      goals: [],
      goalId: 0,
      fakeContact: [],
      SelectedFakeContactList: [],
    };
  }

  async componentDidMount() {
    StatusBar.setHidden(true);
   // console.log('isMaleFemale_goal', this.props.isMaleFemale);
    this.props.getTraineeGoals();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      //console.log('LoginForm', this.props.isFocused);
      
      if (this.props.isFocused) {
        Actions.pop();
      } else {
        this.props.navigation.goBack(null);
      }
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onMobileNumberChanged(text) {
    this.props.mobileNumberChanged(text);
  }

  onBackPressed() {
    Actions.pop();
  }

  async onButtonPressed() {
    await AsyncStorage.setItem('goalId', "1");
    if (this.state.SelectedFakeContactList.length === 0) {
      Alert.alert('Alert', 'Please select your goal');
    }else {
      Actions.fitness();
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  render() {
    
    return (
      <KeyboardAvoidingView  keyboardVerticalOffset={Platform.select({ios: 0, android: -20})} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>
        {/* <LinearGradient colors={['#f4f6fa', '#ffffff']}> */}
          <Loader loading={this.props.loading} />
          <View style={styles.optionInnerContainer}>
              <Image
                source={require('../res/ic_ver_line.png')}
                style={styles.topBarImageStyle}
              />
              <Image
                source={require('../res/ic_ver_line.png')}
                style={styles.topBarImageStyle}
              />
              <Image
                source={require('../res/ic_ver_line.png')}
                style={styles.topBarImageStyle}
              />
              <Image
                source={require('../res/ic_ver_line.png')}
                style={styles.topBarImageStyle}
              />
              <Image
                source={require('../res/ic_ver_shade.png')}
                style={styles.topBarImageStyle}
              />
              <Image
                source={require('../res/ic_ver_shade.png')}
                style={styles.topBarImageStyle}
              />
              
            </View>  

            <TouchableOpacity
                onPress={() => this.onBackPressed()}>
               <Image
                source={require('../res/ic_back.png')}
                style={styles.backImageStyle}
              />
              </TouchableOpacity>
            
            <View style={styles.optionInnerContainer}>
              <Image
                source={require('../res/ic_app.png')}
                style={styles.appImageStyle}
              />
            </View>  
          
          <View style={styles.containericonStyle}>
          
            <Text style={styles.subtextOneStyle}>Let us know how we can help you</Text>
            <Text style={styles.desc}>You always can change this later</Text>

            <FlatList
            contentContainerStyle={{ paddingBottom: hp('35%')}}
            data={this.props.goals} 
            keyExtractor={item => item.id} 
            extraData={this.state} 
            // ListHeaderComponent={this.renderHeader} 
            renderItem={({item}) => {
              return <TouchableOpacity style={styles.containerMaleStyle} onPress={() => {
                //Actions.workDet({item});
              }}>
                <View >
                  <Text style={styles.countryText}>{`${item.name}`}</Text>
                  {/* <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 12, color: '#858C94',}}>Category            : </Text>
                    <Text style={{ fontSize: 12, color: '#ffffff',}}>{item.ctg}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 12, color: '#858C94',}}>Muscle Groups : </Text>
                    <Text style={{ fontSize: 12, color: '#ffffff',}}>{item.subctg}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 12, color: '#858C94',}}>Type of Sets      : </Text>
                    <Text style={{ fontSize: 12, color: '#ffffff',}}>{item.rpname}</Text>
                  </View> */}
                  {/* {item.check
                    ? (
                      <Text style={{
                        fontWeight: 'bold',
                        color: '#1aba9a',
                      }}>{`${item.name} ${item.name} ${item.check}`}</Text>
                    )
                    : (
                      <Text style={{
                        fontWeight: 'bold',
                        color: '#1aba9a',
                      }}>{`${item.name} ${item.name} ${item.check}`}</Text>
                    )} */}
                </View>
                <View style={styles.mobileImageStyle}>
                  {item.check
                    ? (
                      <TouchableOpacity  onPress={() => {
                        this.press(item)
                      }}>
                        <Image source={require('../res/check.png')} style={styles.mobileImageStyle}/>
                      </TouchableOpacity>
                    )
                    : (
                    <TouchableOpacity  onPress={() => {
                      this.press(item)
                    }}>
                        <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle}/>
                      </TouchableOpacity>
                    )}
                </View>
              </TouchableOpacity>
            }}/>

          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                <TouchableOpacity
                  onPress={() => this.onButtonPressed()}>
                  <Text style={styles.buttonText}>Continue</Text>
                </TouchableOpacity>
            </LinearGradient>
         </View>
          
        {/* </LinearGradient> */}
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

  press = (hey) => {
    this.props.goals.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.SelectedFakeContactList.push(item);
          //console.log('selected:' + item.name + ' Size : '+ this.state.SelectedFakeContactList.length);
        } else if (item.check === false) {
          const i = this.state.SelectedFakeContactList.indexOf(item)
          if (1 != -1) {
            this.state.SelectedFakeContactList.splice(i, 1)
            //console.log('unselect:' + item.name + ' Size : '+ this.state.SelectedFakeContactList.length)
            return this.state.SelectedFakeContactList
          }
        }
      }
    })
    this.setState({fakeContact: this.props.goals})
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  img: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  iconsView: {
    flexDirection: 'row',
    alignContent: 'center',
    marginTop: 45,
    justifyContent: 'center',
  },
  back: {
    width: 25, 
    height: 18, 
    marginTop: 45,
    marginLeft: 20,
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  logo: {
    width: 70, 
    height: 38, 
    alignSelf: 'flex-end',
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerGender: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('9%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  deleteImageStyle: {
    width: 20,
    height: 20,
    position: 'relative',
    alignSelf: 'center',
  },
  subTextStyle: {
    alignSelf: 'center',
    fontSize: 12,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    color: '#ffffff',
    textAlign: 'center',
  },
  subtextOneStyle: {
    fontSize: 24,
    marginTop: hp('10%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  desc: {
    fontSize: 16,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',  
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  
  textSubmitStyle: {fontSize: 14, color: '#ffffff', fontWeight: 'bold'},
  mobileImageStyle: {width: 25, height: 25, position: 'relative', alignSelf: 'center',marginRight: 10},
  genImg: {height: 40,width: 35, alignSelf: 'center',marginTop: hp('3%'),},
  imageIconStyle: {width: 180, height: 98, alignSelf: 'center'},

  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    
    marginBottom:15,
  },
  
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('7%'),
    marginLeft:20,
    marginBottom:20,
    alignSelf: 'flex-start',
  },
  appImageStyle: {
    width: 100,
    height: 49,
    marginTop: -45,
    marginBottom:20,
  },
  topBarImageStyle: {
    width: wp('16.7%'),
    height: 8,
    marginBottom:20,
    marginRight:1,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position:'absolute',
    bottom:0,
    backgroundColor: '#fff',
  },
});

const mapStateToProps = state => {

  const {goals} = state.procre;
  const loading = state.procre.loading;
  const error = state.procre.error;
  return { loading, error,goals};
  
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    {getTraineeGoals},
  )(DailyActivityLevel),
);
