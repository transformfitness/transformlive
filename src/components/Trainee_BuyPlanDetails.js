import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    FlatList,
    TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { NoInternet, HomeDialog, Loader } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, RZP_KEY } from '../actions/types';
import RazorpayCheckout from 'react-native-razorpay';
import DeviceInfo from 'react-native-device-info';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();

    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function is3Months(array) {
    let three = false;
    array.map((item, i) => {
        if (item.month_duration === 3) {
            three = true;
        }
    });
    return three;
}


class Trainee_BuyPlanDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrAllSubPlans: [],
            noDataMsg: '',
            selSubObj: {},
            subCreateObj: {},
            payRequest: {},
            payResponse: {},
            applyCouponResponse: {},
            modalVisible: false,
            isSuccess: false,
            isFail: false,
            couponCode: '',
            isApplyCoupon: false,
        };
        // this.getAllSubPlanDetails = this.getAllSubPlanDetails.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllSubPlanDetails();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    onBackPressed() {
        if (this.state.modalVisible) {
            if (this.state.isSuccess) {
                this.setState({ isSuccess: false, modalVisible: false });
            }
            else if (this.state.isFail) {
                this.setState({ isFail: false });
            }
            else {
                this.setState({ modalVisible: false });
            }
        }
        else {
            Actions.pop();
        }

    }

    async getAllSubPlanDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            plan_id: this.props.selPlan.plan_id,
        }));
        fetch(
            `${BASE_URL}/trainee/planbenifits`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length) {
                        LogUtils.infoLog1(is3Months(data.data));
                        if (is3Months(data.data)) {
                            data.data.map((item, i) => {
                                if (item.month_duration === 3) {
                                    item.check = true;
                                    this.setState({ selSubObj: item });
                                }
                                else {
                                    item.check = false;
                                }
                                return data.data;
                            });
                        }
                        else {
                            data.data.map((item, i) => {
                                if (i === 0) {
                                    item.check = true;
                                    this.setState({ selSubObj: item });
                                }
                                else {
                                    item.check = false;
                                }
                                return data.data;
                            });
                        }


                        this.setState({ loading: false, arrAllSubPlans: data.data });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
    }


    async getSubscriptionDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        let data;
        if (this.state.isApplyCoupon) {
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                pp_id: this.state.selSubObj.pp_id,
                amount: this.state.selSubObj.new_total_price,
                plan_code: this.state.selSubObj.plan_code,
                modify: 0,
                c_id: this.state.applyCouponResponse.c_id,
                c_disc: this.state.selSubObj.cpn_discount,
            });
        }
        else {
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                pp_id: this.state.selSubObj.pp_id,
                amount: this.state.selSubObj.new_total_price,
                plan_code: this.state.selSubObj.plan_code,
                modify: 0,
                c_id: 0,
                c_disc: 0,
            });
        }

        // }
        LogUtils.infoLog1(token, data);
        fetch(
            `${BASE_URL}/trainee/create_subscription`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, subCreateObj: data });
                    LogUtils.infoLog(data);

                    LogUtils.firebaseEventLog('click', {
                        p_id: this.props.selPlan.plan_id,
                        p_category: 'Plans',
                        p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Started`,
                    });

                    var options = {
                        key: RZP_KEY,
                        subscription_id: data.sub_code,
                        // subscription_card_change: 1,
                        name: 'Transform',
                        description: this.props.selPlan.title,
                        // notes: {
                        //   note_key_1: 'Tea. Earl Grey. Hot',
                        //   note_key_2: 'Make it so.'
                        // },
                        // prefill: {
                        //     email: 'venkatesh@appoids.in',
                        //     contact: '8861512641',
                        //     name: 'Venkatesh A'
                        // },
                        theme: { color: '#8c52ff' }
                    };

                    this.setState({ payRequest: options });
                    LogUtils.infoLog1('Payment Request : ', options);
                    RazorpayCheckout.open(options).then((data) => {
                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Success`,
                        });
                        // handle success
                        LogUtils.infoLog1('Payment Response : ', data);
                        this.setState({ payResponse: data });
                        // alert(`Success: ${data.razorpay_payment_id}`);
                        this.savePlanSubscription();

                    }).catch((error) => {
                        // handle failure
                        // alert(`Error: ${error.code} | ${error.description}`);
                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment fail`,
                        });
                        this.setState({ isAlert: true, alertMsg: `${error.description}`, alertTitle: 'Declined!' });
                    });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }


    async getOrderDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        let data;
        if (this.state.isApplyCoupon) {
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                pp_id: this.state.selSubObj.pp_id,
                amount: this.state.selSubObj.new_total_price,
                c_id: this.state.applyCouponResponse.c_id,
                c_disc: this.state.selSubObj.cpn_discount,
            });
        }
        else {
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                pp_id: this.state.selSubObj.pp_id,
                amount: this.state.selSubObj.new_total_price,
                c_id: 0,
                c_disc: 0,
            });
        }

        LogUtils.infoLog1(token, data);
        fetch(
            `${BASE_URL}/trainee/create_order`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, subCreateObj: data });
                    LogUtils.infoLog(data);

                    LogUtils.firebaseEventLog('click', {
                        p_id: this.props.selPlan.plan_id,
                        p_category: 'Plans',
                        p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Started`,
                    });

                    var options = {
                        description: this.props.selPlan.title,
                        // image: 'http://fitness.whamzone.com/assets/images/logo.png',
                        // currency: 'INR',
                        key: RZP_KEY,
                        // amount: '2999',
                        name: 'Transform',
                        order_id: data.ord_code,//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                        // prefill: {
                        //     email: 'venkatesh@appoids.in',
                        //     contact: '861512641',
                        //     name: 'Venkatesh Annavaram'
                        // },
                        theme: { color: '#8c52ff' }
                    }

                    this.setState({ payRequest: options });
                    LogUtils.infoLog1('Payment Request : ', options);
                    RazorpayCheckout.open(options).then((data) => {
                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Success`,
                        });
                        // handle success
                        LogUtils.infoLog1('Payment Response : ', data);
                        this.setState({ payResponse: data });
                        // alert(`Success: ${data.razorpay_payment_id}`);
                        this.savePlanSubscription();

                    }).catch((error) => {
                        // handle failure
                        // alert(`Error: ${error.code} | ${error.description}`);
                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Fail`,
                        });
                        this.setState({ isAlert: true, alertMsg: `${error.description}`, alertTitle: 'Declined!' });
                    });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }

    async savePlanSubscription() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        let data;
        if (this.state.selSubObj.can_subscribe === 1) {
            if (this.state.isApplyCoupon) {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    rzp_s_id: this.state.subCreateObj.sub_id,
                    payment_id: this.state.payResponse.razorpay_payment_id,
                    subscription_id: this.state.payResponse.razorpay_subscription_id,
                    signature: this.state.payResponse.razorpay_signature,
                    request: this.state.payRequest,
                    uc_id: this.state.subCreateObj.uc_id,
                    // rzp_o_id: '',
                    // order_id: '',
                    modify: 0,
                    up_id: 0,
                    credits: 0,
                });
            }
            else {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    rzp_s_id: this.state.subCreateObj.sub_id,
                    payment_id: this.state.payResponse.razorpay_payment_id,
                    subscription_id: this.state.payResponse.razorpay_subscription_id,
                    signature: this.state.payResponse.razorpay_signature,
                    request: this.state.payRequest,
                    uc_id: 0,
                    // rzp_o_id: '',
                    // order_id: '',
                    modify: 0,
                    up_id: 0,
                    credits: 0,
                });
            }

        }
        else {
            if (this.state.isApplyCoupon) {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    // rzp_s_id: '',
                    payment_id: this.state.payResponse.razorpay_payment_id,
                    // subscription_id: '',
                    signature: this.state.payResponse.razorpay_signature,
                    request: this.state.payRequest,
                    rzp_o_id: this.state.subCreateObj.ord_id,
                    order_id: this.state.subCreateObj.ord_code,
                    uc_id: this.state.subCreateObj.uc_id,
                    modify: 0,
                    up_id: 0,
                    credits: 0,
                });
            }
            else {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    // rzp_s_id: '',
                    payment_id: this.state.payResponse.razorpay_payment_id,
                    // subscription_id: '',
                    signature: this.state.payResponse.razorpay_signature,
                    request: this.state.payRequest,
                    rzp_o_id: this.state.subCreateObj.ord_id,
                    order_id: this.state.subCreateObj.ord_code,
                    uc_id: 0,
                    modify: 0,
                    up_id: 0,
                    credits: 0,
                });
            }

        }
        LogUtils.infoLog1(token, data);

        fetch(
            `${BASE_URL}/trainee/save_subscription`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    // this.setState({ loading: false, isSuccess: true, sucMsg: data.message, titMsg: data.title });
                    this.setState({ loading: false });
                    LogUtils.firebaseEventLog('click', {
                        p_id: this.props.selPlan.plan_id,
                        p_category: 'Plans',
                        p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Subscribed Successfully`,
                    });
                    Actions.traBuySuccess({ from: 'payment', sucResponse: data });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }

    async callApplyCouponsService() {
        if (this.state.couponCode) {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            let data;
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                // amount: this.state.selSubObj.new_total_price,
                coupon: this.state.couponCode,
            });
            LogUtils.infoLog1(token, data);
            fetch(
                `${BASE_URL}/trainee/apply_coupon2`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isSuccess: true, isApplyCoupon: true, applyCouponResponse: data });

                        this.isUpdateList();
                        
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isFail: true });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please enter coupon code', alertTitle: 'Alert' });
        }

    }

    isUpdateList() {
        this.state.applyCouponResponse.pp_list.map((item, i) => {
            this.state.arrAllSubPlans.map((selItem, j) => {
                if (item.pp_id === selItem.pp_id) {
                    selItem.new_price_text = item.net_price_text;
                    selItem.offer_price_text = item.net_discount_text;
                    selItem.new_total_price = item.tot_price;
                    selItem.cpn_discount = item.cpn_discount * item.months;
                }
            });
        });

        this.state.arrAllSubPlans.map((item, i) => {
            if (item.check === true) {
                this.setState({ selSubObj: item});
            }
        });
    }

    // async callApplyCouponsService() {
    //     if (this.state.couponCode) {
    //         this.setState({ loading: true });
    //         let token = await AsyncStorage.getItem('token');
    //         let data;
    //         data = JSON.stringify({
    //             pp_id: this.state.selSubObj.pp_id,
    //             amount: this.state.selSubObj.new_total_price,
    //             coupon: this.state.couponCode,
    //         });
    //         LogUtils.infoLog1(token, data);
    //         fetch(
    //             `${BASE_URL}/trainee/apply_coupon`,
    //             {
    //                 method: 'POST',
    //                 headers: {
    //                     Accept: 'application/json',
    //                     'Content-Type': 'application/json',
    //                     Authorization: `Bearer ${token}`,
    //                 },
    //                 body: data,
    //             },
    //         )
    //             .then(processResponse)
    //             .then(res => {
    //                 const { statusCode, data } = res;
    //                 LogUtils.infoLog1('statusCode', statusCode);
    //                 LogUtils.infoLog1('data', data);

    //                 if (statusCode >= 200 && statusCode <= 300) {
    //                     this.setState({ loading: false, isSuccess: true, isApplyCoupon: true, applyCouponResponse: data });
    //                 } else {
    //                     if (data.message === 'You are not authenticated!') {
    //                         this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
    //                     } else {
    //                         this.setState({ loading: false, isFail: true });
    //                     }
    //                 }
    //             })
    //             .catch(function (error) {
    //                 this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
    //             });
    //     }
    //     else {
    //         this.setState({ isAlert: true, alertMsg: 'Please enter coupon code', alertTitle: 'Alert' });
    //     }

    // }


    async callPaymentMethod() {
        // normal paymet
        // var options = {
        //     description: this.props.selPlan.title,
        //     // image: 'http://fitness.whamzone.com/assets/images/logo.png',
        //     currency: 'INR',
        //     key: 'rzp_test_3NfxiVvKdFdl3o',
        //     amount: '19900',
        //     // external: {
        //     //     wallets: ['paytm']
        //     // },
        //     name: 'Transform',
        //     prefill: {
        //         email: 'venkatesh@appoids.in',
        //         contact: '9491457727',
        //         name: 'Venkatesh Annavaram'
        //     },
        //     // theme: { color: '#8c52ff' }
        // }

        //   paymet with Order ID
        // var options = {
        //     description: this.props.selPlan.title,
        //     image: 'http://fitness.whamzone.com/assets/images/logo.png',
        //     // currency: 'INR',
        //     key: 'rzp_test_3NfxiVvKdFdl3o',
        //     // amount: '2999',
        //     name: 'Transform',
        //     order_id: 'order_EydFty36GIjYxn',//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
        //     prefill: {
        //         email: 'venkatesh@appoids.in',
        //         contact: '861512641',
        //         name: 'Venkatesh Annavaram'
        //     },
        //     theme: { color: '#8c52ff' }
        // }

        // subscription payment
        var options = {
            key: 'rzp_test_3NfxiVvKdFdl3o',
            subscription_id: 'sub_Exm4WCuEZyTcAt',
            // subscription_card_change: 1,
            name: 'Transform',
            description: this.props.selPlan.title,
            // image: 'http://fitness.whamzone.com/assets/images/logo.png',
            // handler: function(response) {
            //   alert(response.razorpay_payment_id),
            //   alert(response.razorpay_subscription_id),
            //   alert(response.razorpay_signature);
            // },
            notes: {
                note_key_1: 'Tea. Earl Grey. Hot',
                note_key_2: 'Make it so.'
            },
            prefill: {
                email: 'venkatesh@appoids.in',
                contact: '8861512641',
                name: 'Venkatesh A'
            },
            theme: { color: '#8c52ff' }
        };

        RazorpayCheckout.open(options).then((data) => {
            // handle success
            LogUtils.infoLog1('Payment Response : ', data);
            // alert(`Success: ${data.razorpay_payment_id}`);
            this.savePlanSubscription();

        }).catch((error) => {
            // handle failure
            alert(`${error.description}`);
        });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllSubPlanDetails();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    press = (hey) => {
        if (this.state.arrAllSubPlans.length > 1 && !hey.check) {
            this.state.arrAllSubPlans.map((item, i) => {
                if (item.pp_id === hey.pp_id) {
                    item.check = true;
                    this.setState({ selSubObj: item});
                    // this.setState({ selSubObj: item, isApplyCoupon: false, applyCouponResponse: {} });
                }
                else {
                    item.check = false;
                }
            });

            // LogUtils.infoLog(this.state.applyCouponResponse);
            this.setState({ arrAllSubPlans: this.state.arrAllSubPlans });
        }

    }

    renderBenfitsInclude() {
        if (!isEmpty(this.state.selSubObj)) {
            let arrBenfitIncludes = this.state.selSubObj.benifit_arr.map((item1, i) => {
                return <View key={i} style={styles.viewBenInclu}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: item1.icon_img }}
                        style={{
                            width: 35,
                            height: 35,
                            marginLeft: 5,
                            alignSelf: 'center',
                        }}
                    />
                    <View style={styles.viewBenIncluInner}>
                        {/* <Text style={styles.txt14}>{item1.title}</Text> */}
                        <Text style={styles.txtDesc}>{item1.sub_title}</Text>
                    </View>
                </View>
            });
            return (
                <View>
                    {arrBenfitIncludes}
                </View>
            );
        }
    }

    renderPriceWhite = (item) => {
        if (item.is_flat === 'y') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtWhite}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtWhite}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        } else if (item.is_flat === 'n') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtWhite}>Rs.{`${item.month_price}`}  Only</Text>
                    <Text style={styles.txtOffAmtWhite}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        }
        else {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtOffAmtWhite}>Rs.{`${item.month_price}`} Only</Text>
                </View>
            );
        }
    }

    renderPriceBlack = (item) => {
        if (item.is_flat === 'y') {

            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtBlack}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        } else if (item.is_flat === 'n') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtBlack}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        }
        else {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtOffAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                </View>
            );
        }
    }

    renderAllSubPlansList() {
        if (Array.isArray(this.state.arrAllSubPlans) && this.state.arrAllSubPlans.length) {
            return (
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={this.state.arrAllSubPlans}
                    keyExtractor={item => item.id}
                    contentContainerStyle={{ paddingLeft: 5, paddingRight: 5 }}
                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                    // ListEmptyComponent={this.ListEmptyView}
                    renderItem={({ item, index }) => {
                        return <View key={item.pp_id} >
                            {item.check
                                ? (
                                    <TouchableOpacity style={styles.bgSelect}
                                        onPress={() => {
                                            this.press(item);
                                        }}>
                                        <View style={{
                                            flexDirection: 'column',
                                            padding: 10,
                                            width: wp('45%'),
                                            height: hp('25%'),
                                        }}>
                                            <View style={{ marginLeft: -10, height: 60, alignSelf: 'flex-start', }}>
                                                {item.cashbak_img
                                                    ? (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={{ uri: item.cashbak_img }}
                                                            style={{
                                                                width: 60,
                                                                height: 60,
                                                                resizeMode: 'cover',
                                                                alignSelf: 'flex-start',
                                                            }}
                                                        />
                                                    )
                                                    : (
                                                        <Text style={styles.txtOffPerBlack}>{`${item.offer_text}`}</Text>
                                                    )
                                                }
                                            </View>

                                            <Text style={styles.txtOfferWhite} numberOfLines={1}>{item.duration}</Text>
                                            {item.new_price_text
                                                ? (
                                                    <View style={{ flexDirection: 'column', marginTop: 15, }}>
                                                        <Text style={styles.txtActAmtWhite}>{`${item.price_text}`}</Text>
                                                        <View style={{ height: 5, }}></View>

                                                        {/* {this.state.isApplyCoupon
                                                            ? (
                                                                <Text style={styles.txtOffAmtWhite}>@Rs{`${this.state.applyCouponResponse.net_amt}`} only</Text>
                                                            )
                                                            : ( */}
                                                                <Text style={styles.txtOffAmtWhite}>{`${item.new_price_text}`}</Text>
                                                            {/* )
                                                        } */}
                                                    </View>
                                                )
                                                : (
                                                    <View style={{ flexDirection: 'column', marginTop: 15, }}>
                                                        <Text style={styles.txtActAmtWhite}></Text>
                                                        <View style={{ height: 5, }}></View>
                                                        {/* {this.state.isApplyCoupon
                                                            ? (
                                                                <Text style={styles.txtOffAmtWhite}>@Rs{`${this.state.applyCouponResponse.net_amt}`} only</Text>
                                                            )
                                                            : ( */}
                                                                <Text style={styles.txtOffAmtWhite}>{`${item.price_text}`}</Text>
                                                            {/* )
                                                        } */}

                                                    </View>
                                                )
                                            }
                                            <Text style={styles.txtSaveAmtBlack}>{`${item.offer_price_text}`}</Text>
                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            position: 'absolute',
                                            width: wp('45%'),
                                            height: hp('25%'),
                                        }}>
                                            {item.sale_img
                                                ? (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={{ uri: item.sale_img }}
                                                        style={{
                                                            width: 60,
                                                            height: 60,
                                                            top: -30,
                                                            marginLeft: wp('28%'),
                                                            position: 'absolute',
                                                            resizeMode: 'cover',
                                                            alignSelf: 'flex-start',
                                                        }}
                                                    />
                                                )
                                                : (
                                                    <View></View>
                                                )
                                            }

                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.main_img }}
                                                style={{
                                                    width: 130,
                                                    height: 130,
                                                    marginTop: 40,
                                                    position: 'absolute',
                                                    right: -30,
                                                    resizeMode: 'cover',
                                                    alignSelf: 'flex-end',
                                                }}
                                            />
                                        </View>
                                    </TouchableOpacity>

                                )
                                : (
                                    <TouchableOpacity style={styles.bgUnSelect}
                                        onPress={() => {
                                            this.press(item);
                                        }}>
                                        <View style={{
                                            flexDirection: 'column',
                                            padding: 10,
                                            width: wp('45%'),
                                            height: hp('25%'),

                                        }}>
                                            <View style={{ marginLeft: -10, height: 60, alignSelf: 'flex-start', }}>
                                                {item.cashbak_img
                                                    ? (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={{ uri: item.cashbak_img }}
                                                            style={{
                                                                width: 60,
                                                                height: 60,
                                                                resizeMode: 'cover',
                                                                alignSelf: 'flex-start',
                                                            }}
                                                        />
                                                    )
                                                    : (
                                                        <Text style={styles.txtOffPerBlack}>{`${item.offer_text}`}</Text>
                                                    )
                                                }
                                            </View>
                                            <Text style={styles.txtOfferBlack} numberOfLines={1}>{`${item.duration}`}</Text>
                                            {item.new_price_text
                                                ? (
                                                    <View style={{ flexDirection: 'column', marginTop: 15, }}>
                                                        <Text style={styles.txtActAmtBlack}>{`${item.price_text}`}</Text>
                                                        <View style={{ height: 5, }}></View>
                                                        <Text style={styles.txtOffAmtBlack}>{`${item.new_price_text}`}</Text>
                                                    </View>
                                                )
                                                : (
                                                    <View style={{ flexDirection: 'column', marginTop: 15, }}>
                                                        <Text style={styles.txtActAmtBlack}></Text>
                                                        <View style={{ height: 5, }}></View>
                                                        <Text style={styles.txtOffAmtBlack}>@Rs.{`${item.price_text}`} Only</Text>
                                                    </View>
                                                )
                                            }
                                            <Text style={styles.txtSaveAmtBlack}>{`${item.offer_price_text}`}</Text>
                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            position: 'absolute',
                                            width: wp('45%'),
                                            height: hp('25%'),
                                        }}>
                                            {item.sale_img
                                                ? (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={{ uri: item.sale_img }}
                                                        style={{
                                                            width: 60,
                                                            height: 60,
                                                            top: -30,
                                                            marginLeft: wp('28%'),
                                                            position: 'absolute',
                                                            resizeMode: 'cover',
                                                            alignSelf: 'flex-start',
                                                        }}
                                                    />
                                                )
                                                : (
                                                    <View></View>
                                                )
                                            }


                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.main_img }}
                                                style={{
                                                    width: 130,
                                                    height: 130,
                                                    marginTop: 40,
                                                    position: 'absolute',
                                                    right: -30,
                                                    resizeMode: 'cover',
                                                    alignSelf: 'flex-end',
                                                }}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                )
                            }


                        </View>

                    }} />
            );
        }
        else {
            return (

                <View style={{ flexDirection: 'row', flex: 1, }}>
                    <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                </View>
            );
        }

    }

    renderIaApplyCoupons() {
        if (this.state.selSubObj.is_coupon === 1) {
            if (this.state.isApplyCoupon) {
                return (

                    <View style={styles.viewApplyCoupon}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/img_apply_coupon.png')}
                            style={{ width: 23, height: 23 }}
                        />
                        <Text style={styles.txtApplyCoupon}>{this.state.applyCouponResponse.coupon} coupon applied</Text>
                    </View>
                );
            }
            else {
                return (
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ modalVisible: true, });
                        }}>
                        <View style={styles.viewApplyCoupon}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/img_apply_coupon.png')}
                                style={{ width: 23, height: 23 }}
                            />
                            <Text style={styles.txtApplyCoupon}>Apply Coupon</Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible });
    }


    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <Loader loading={this.state.loading} />

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
                    <View style={styles.containerStyle}>
                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={styles.mainView}>
                            <Text style={styles.txtPlanName}>{this.props.selPlan.name}</Text>
                            <Text style={styles.txtPlanDesc}>{this.props.selPlan.description}</Text>

                            <Text style={styles.txtBenfitIncludes}>Benfits includes:</Text>

                            {this.renderBenfitsInclude()}


                        </View>
                        {/*  sub plans */}
                        <View style={{ marginTop: 20, }}></View>
                        {this.renderAllSubPlansList()}

                        {this.renderIaApplyCoupons()}

                    </View>

                </ScrollView>

                <View style={styles.bottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {
                                // LogUtils.infoLog(this.state.selSubObj);

                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration}`,
                                });

                                if (this.state.selSubObj.can_subscribe === 1) {
                                    this.getSubscriptionDetails();
                                }
                                else {
                                    this.getOrderDetails();
                                }
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Buy Now</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ modalVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ modalVisible: false });
                    }}
                    // width={0.9}
                    // height={0.9}
                    dialogStyle={{ backgroundColor: '#ffffff', }}
                    visible={this.state.modalVisible}>
                    <DialogContent
                        style={{
                            // backgroundColor: '#b0b0b0',
                            flex: 1,

                        }}>
                        <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', width: wp('100%'), }}>

                            <View style={{
                                flexDirection: 'row',
                                margin: 20,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111 }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.toggleModal(!this.state.modalVisible)}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle1}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <Text style={styles.textHeadTitle}>Apply Coupons</Text>
                            </View>

                            <View style={{ flexDirection: 'column', alignSelf: 'center', alignItems: 'center', justifyContent: 'flex-start' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/img_apply_coupon.png')}
                                    style={styles.imgNoSuscribe}
                                />
                                <Text style={styles.textNodataTitle}>Please enter coupon details to avail offer</Text>
                            </View>
                        </View>
                        <View style={styles.bottom}>

                            <View style={styles.containerMobileStyle}>
                                <TextInput
                                    ref={input => { this.addressTextInput = input; }}
                                    style={styles.textInputStyle}
                                    placeholder="Enter Coupon Code"
                                    maxLength={20}
                                    placeholderTextColor="grey"
                                    value={this.state.couponCode}
                                    autoCapitalize='words'
                                    returnKeyType='done'
                                    onChangeText={text => {
                                        if (text) {
                                            this.setState({ couponCode: text });
                                        }
                                        else {
                                            this.setState({ couponCode: '' });
                                        }
                                    }}
                                />
                            </View>

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.callApplyCouponsService();
                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>Submit</Text>
                                </TouchableOpacity>

                            </LinearGradient>
                        </View>
                    </DialogContent>
                </Dialog>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isSuccess: false, modalVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isSuccess: false, modalVisible: false });
                    }}
                    width={0.75}
                    visible={this.state.isSuccess}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 10 }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/img_apply_coupon_success.png')}
                                        style={styles.imgpopup}
                                    />
                                    <Text style={styles.textPopTitle}>Congratulations</Text>
                                    <Text style={styles.textPopDesc}>You coupon has been applied successfully. Proceed to check updated pricing.</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isSuccess: false, modalVisible: false });
                                            // this.cancelSubscriptions();
                                        }}>
                                            <Text style={styles.textSave}>OK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isFail: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isFail: false });
                    }}
                    width={0.75}
                    visible={this.state.isFail}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 10 }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/img_apply_coupon_fail.png')}
                                        style={styles.imgpopup}
                                    />
                                    <Text style={styles.textPopTitle}>Failed</Text>
                                    <Text style={styles.textPopDesc}>The coupon code you have applied is invalid. Please check and try again or check with our customer care.</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isFail: false });
                                        }}>
                                            <Text style={styles.textSave}>OK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <HomeDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    backImageStyle1: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    mainView: {
        flexDirection: 'column',
        margin: 20,
    },
    txtPlanName: {
        fontSize: 20,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtPlanDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#6D819C',
        marginTop: 15,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    txtBenfitIncludes: {
        fontSize: 12,
        fontWeight: '400',
        color: '#8c52ff',
        marginTop: 20,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.4,
    },
    viewBenInclu: {
        flexDirection: 'row',
        marginTop: 20,
    },
    viewBenIncluInner: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 15,
    },
    txt14: {
        fontSize: 16,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        marginRight: 30,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    bgSelect: {
        width: 190,
        height: 200,
        backgroundColor: '#8c52ff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 10,
        marginLeft: 5,
        marginRight: 35,
        marginBottom: 10,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 30,
        zIndex: 111,
    },
    bgUnSelect: {
        width: 190,
        height: 200,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 10,
        marginLeft: 5,
        marginRight: 35,
        marginBottom: 10,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 30,
        zIndex: 111,
    },
    txtOfferWhite: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        marginTop: 20,
    },
    txtOfferBlack: {
        fontSize: 14,
        fontWeight: '500',
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        marginTop: 20,
    },
    txtActAmtWhite: {
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
    },
    txtActAmtBlack: {
        fontSize: 10,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
    },
    txtOffAmtWhite: {
        fontSize: 10,
        fontWeight: '700',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtOffAmtBlack: {
        fontSize: 10,
        fontWeight: '700',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtOffPerBlack: {
        fontSize: 10,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'center',
        marginTop: 5,
        backgroundColor: '#49C997',
        paddingLeft: 7,
        paddingRight: 7,
        paddingTop: 3,
        paddingBottom: 3,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    },
    txtSaveAmtBlack: {
        fontSize: 9,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        marginTop: 15,
        marginRight: 30,
        backgroundColor: '#FAD541',
        padding: 3,
        borderRadius: 3,
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 20,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewApplyCoupon: {
        margin: 25,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    txtApplyCoupon: {
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        marginLeft: 3,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        paddingBottom: 3,
        backgroundColor: '#8c52ff',
        letterSpacing: 0.3,
    },
    modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        marginTop: 50,
        alignSelf: 'center',
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    containerMobileStyle: {
        width: '90%',
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        padding: 8,
        marginBottom: 50,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    imgpopup: {
        width: 200,
        height: 200,
        alignSelf: 'center',
    },
    textPopTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textPopDesc: {
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        padding: 5,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 25,
    },

});

const mapStateToProps = state => {
    // const { goals, trainerCodes } = state.procre;
    // const loading = state.procre.loading;
    // const error = state.procre.error;
    return {};
};

export default Trainee_BuyPlanDetails;
