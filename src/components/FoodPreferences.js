import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFoodPreferences } from '../actions';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

class FoodPreferences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      foodPrefId: 0,
      isVeg: 1,
      fakeContact: [],
      SelectedFakeContactList: [],
      isAlert: false,
      alertMsg: '',
      isInternet: false,
    };
  }

  async componentDidMount() {
    StatusBar.setHidden(true);
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.props.getFoodPreferences();
      }
      else {
        this.setState({ isInternet: true });
      }
    });
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Actions.pop();
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    Actions.pop();
  }

  async onButtonPressed() {
    if (this.state.SelectedFakeContactList.length === 0) {
      this.setState({ isAlert: true, alertMsg: 'Please select your food preference' });
    }
    else {
      let wIds = '';
      this.state.SelectedFakeContactList.map((item) => {
        if (item.check) {
          if (wIds.length === 0) {
            wIds = wIds + item.id;
          }
          else {
            wIds = wIds + ',' + item.id;
          }
        }
      })
      await AsyncStorage.setItem('foodPrefId', wIds);
      await AsyncStorage.setItem('isVeg', this.state.isVeg.toString());
      Actions.goal();
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  renderVegOrNonVeg() {
    if (this.state.isVeg === 1) {
      return (
        <View style={styles.containerIsVeg}>
          <TouchableOpacity
            onPress={() => this.setState({ isVeg: 1 })}>
            <View style={styles.containerVegStyle}>
              <Image
                source={require('../res/veg.png')}
                style={styles.imgVeg}
              />
              <Text style={styles.vegText}>Veg</Text>
              <Image
                source={require('../res/check.png')}
                style={styles.vegSelect}
              />
            </View>
          </TouchableOpacity>

          <View style={{ width: 6, height: 4 }}></View>

          <TouchableOpacity
            onPress={() => this.setState({ isVeg: 2 })}>
            <View style={styles.containerVegStyle}>
              <Image
                source={require('../res/nonveg.png')}
                style={styles.imgVeg}
              />
              <Text style={styles.vegText}>Non-Veg</Text>
              <Image
                source={require('../res/uncheck.png')}
                style={styles.vegSelect}
              />
            </View>
          </TouchableOpacity>
        </View>
      );
    } else if (this.state.isVeg === 2) {
      return (
        <View style={styles.containerIsVeg}>
          <TouchableOpacity
            onPress={() => this.setState({ isVeg: 1 })}>
            <View style={styles.containerVegStyle}>
              <Image
                source={require('../res/veg.png')}
                style={styles.imgVeg}
              />
              <Text style={styles.vegText}>Veg</Text>
              <Image
                source={require('../res/uncheck.png')}
                style={styles.vegSelect}
              />
            </View>
          </TouchableOpacity>
          <View style={{ width: 6, height: 4 }}></View>
          <TouchableOpacity
            onPress={() => this.setState({ isVeg: 2 })}>

            <View style={styles.containerVegStyle}>
              <Image
                source={require('../res/nonveg.png')}
                style={styles.imgVeg}
              />
              <Text style={styles.vegText}>Non-Veg</Text>
              <Image
                source={require('../res/check.png')}
                style={styles.vegSelect}
              />
            </View>
          </TouchableOpacity>

        </View>
      );
    }
  }

  async onAccept() {
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
        this.props.getFoodPreferences();
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  render() {

    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>

          <Loader loading={this.props.loading} />
          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_shade.png')}
              style={styles.topBarImageStyle}
            />

          </View>

          <TouchableOpacity
            style={{ width: 40, height: 40, marginBottom: 40, }}
            onPress={() => this.onBackPressed()}>
            <Image
              source={require('../res/ic_back.png')}
              style={styles.backImageStyle}
            />
          </TouchableOpacity>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>Food Preference</Text>
            <Text style={styles.desc}>Please select your choice of food</Text>

            {this.renderVegOrNonVeg()}

            <FlatList
              contentContainerStyle={{ paddingBottom: hp('45%') }}
              showsVerticalScrollIndicator={false}
              data={this.props.foodPreferences}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => {
                return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                  this.press(item)
                }}>
                  <View >
                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                  </View>
                  <View style={styles.mobileImageStyle}>
                    {item.check
                      ? (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )
                      : (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )}
                  </View>
                </TouchableOpacity>
              }} />
          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onButtonPressed()}>
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

  press = (hey) => {
    this.props.foodPreferences.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.SelectedFakeContactList.push(item);
        } else if (item.check === false) {
          const i = this.state.SelectedFakeContactList.indexOf(item)
          if (1 != -1) {
            this.state.SelectedFakeContactList.splice(i, 1)
            return this.state.SelectedFakeContactList
          }
        }
      }
    })
    this.setState({ fakeContact: this.props.foodPreferences })
  }
}


const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  img: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  back: {
    width: 25,
    height: 18,
    marginTop: 45,
    marginLeft: 20,
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerIsVeg: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  containerVegStyle: {
    width: wp('45%'),
    height: hp('10%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('9%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  subtextOneStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
  imgVeg: { width: 40, height: 40, alignSelf: 'center', marginLeft: 10, position: 'relative' },
  vegText: {
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '600',
    marginLeft: 5,
    flex: 1,
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  vegSelect: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 10 },
  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('7%'),
    marginLeft: 20,
    marginBottom: 20,
    alignSelf: 'flex-start',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: -45,
  },
  topBarImageStyle: {
    width: wp('8.2%'),
    height: 8,
    marginBottom: 20,
    marginRight: 1,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
  },
});

const mapStateToProps = state => {

  const { foodPreferences } = state.masters;
  const loading = state.masters.loading;
  const error = state.masters.error;
  return { loading, error, foodPreferences };

};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { getFoodPreferences },
  )(FoodPreferences),
);
