import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Text,
  Dimensions,
  Modal,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import {
  getTraineeFeed,
  uploadLike,
  uploadPoll,
  getTraineeFeedLoadMore,
  uploadFollow,
} from '../actions';
import { Loader, CustomDialog, NoInternet } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
let pageNo = 1;
const profileImageSize = 38;
const padding = 10;
let username = "", userId = '', token = '', width = '50%', feedUserId = '', shopping_url = '';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
import Orientation from 'react-native-orientation';
import NetInfo from "@react-native-community/netinfo";
import LinearGradient from 'react-native-linear-gradient';
import { BASE_URL, SWR } from '../actions/types';
import Image1 from 'react-native-scalable-image';
import LogUtils from '../utils/LogUtils.js';
import ActionButton from 'react-native-circular-action-menu';
import moment from "moment";
import { allowFunction } from '../utils/ScreenshotUtils.js';
import RBSheet from "react-native-raw-bottom-sheet";

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

class Trainee_Wall extends Component {

  constructor(props) {
    super(props);
    LogUtils.showSlowLog(this);
    this.state = {
      likesCount: 0,
      is_poll: 0,
      is_like: 0,
      fakeContact: [],
      SelectedFakeContactList: [],
      feeds: [],
      isAlert: false,
      alertMsg: '',
      isInternet: false,
      loading: false,
      isSuccess: false,
      refreshing: false,
      noData: '',
      modalVisible: false,
      imageUri: '',
      loadmore: true,
      isData: '',
      textShown: -1,
      resObj: {},
    };
    this.getTraineeFeed = this.getTraineeFeed.bind(this);
  }

  async componentDidMount() {
    allowFunction();
    username = await AsyncStorage.getItem('userName');
    userId = await AsyncStorage.getItem('userId');
    token = await AsyncStorage.getItem('token');
    shopping_url = await AsyncStorage.getItem('shopping_url');
    LogUtils.infoLog1("userId", userId);
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loading: true });
        pageNo = 1;
        this.getTraineeFeed();
      }
      else {
        this.setState({ isInternet: true });
      }
    });

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }
  componentWillMount() {
    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      // do something
      LogUtils.infoLog(`Changed Device Orientation: ${initial}`);
      Orientation.lockToPortrait();
      LogUtils.infoLog1("orientation", initial.toString())
    } else {
      // do something else
      LogUtils.infoLog(`Changed Device Orientation: ${initial}`);
      Orientation.lockToPortrait();
    }
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }

  async loadMore() {
    let token = await AsyncStorage.getItem('token')
    this.props.getTraineeFeedLoadMore({ token, pageNo: this.props.pageNo });
  }

  handleLoadMore = () => {
    if (!this.state.loading && this.state.loadmore) {
      this.setState({ loading: false, loadmore: false });
      pageNo = pageNo + 1; // increase page by 1
      this.getTraineeFeed(); // method for API call 
    }
  };

  async getTraineeFeed() {
    LogUtils.infoLog1("request", JSON.stringify({
      width: screenWidth,
      pagesize: 5,
      page: pageNo,
    }));

    fetch(
      `${BASE_URL}/trainee/getfeed`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          width: screenWidth,
          pagesize: 5,
          page: pageNo,
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1("data", data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ resObj: data })
          if (data.data) {

            this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message, isData: '0' });

            this.setState({
              feeds: [...this.state.feeds, ...data.data],
              loadMore: true,
              isData: '1',
              //adding the new data with old one available in Data Source of the List
            });
            if (this.state.feeds.length === 0) {
              this.setState({ noData: 'Currently no feeds are posted.Be the first to post one.' });
            } else {
              data.data.map((item) => {

                if (item.is_follow === 1) {
                  item.followcheck = true;
                } else {
                  item.followcheck = false;
                }
                if (item.is_poll === 1) {
                  item.pollcheck = true;
                } else {
                  item.pollcheck = false;
                }
                if (item.is_like === 1) {
                  item.check = true;
                } else {
                  item.check = false;
                }

                return data.data;
              })
            }

          } else {
            this.setState({ loadmore: false })
          }


        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: SWR });
      });
  }

  async uploadLike(body, token) {
    fetch(`${BASE_URL}/trainee/like`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
      });
  }

  async uploadPoll(body, token) {
    fetch(`${BASE_URL}/trainee/Feedpolloptn`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
      });
  }

  async uploadFollow(body, token) {
    fetch(`${BASE_URL}/trainee/saveUserFollow`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          // this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message });

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
      });
  }



  Item({ title }) {
    return (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }

  onBackPressed() {
    if (this.props.from === 'rewards') {
      Actions.pop();
    }
    else {
      // Actions.pop();
      Actions.popTo('traineeHome');
    }

  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  press = (hey) => {
    this.state.feeds.map((item) => {
      if (item.feed_id === hey.feed_id) {
        item.check = !item.check
        if (item.check === true) {
          item.like_cnt = item.like_cnt + 1;
          this.state.SelectedFakeContactList.push(item);
        } else if (item.check === false) {
          const i = this.state.SelectedFakeContactList.indexOf(item)
          if (1 != -1) {
            this.state.SelectedFakeContactList.splice(i, 1)
            return this.state.SelectedFakeContactList
          }
        }
      }
    })
    this.setState({ fakeContact: this.state.feeds })
    let data = JSON.stringify({
      fId: hey.feed_id,
      userId: userId,
    });
    LogUtils.infoLog(data)
    this.uploadLike(data, token);
  }



  onLikePressed = (hey) => {
    this.state.feeds.map((item) => {
      if (item.feed_id === hey.feed_id) {
        LogUtils.infoLog1("feedItem.is_like", item.is_like);
        item.is_like = 1;
        LogUtils.infoLog1("feedItem.is_like", item.is_like);
        item.like_cnt = item.like_cnt + 1
        this.setState({ is_like: item.is_like });
      }
      else {
        item.is_like = 0;
      }
    })

    let data = JSON.stringify({
      fId: hey.feed_id,
      userId: userId,
    });
    LogUtils.infoLog(data)
    this.props.uploadLike(data, token);

  }
  onPollPressed = (feedItem, item) => {
    this.state.feeds.map((item) => {
      if (item.feed_id === feedItem.feed_id) {
        item.pollcheck = !item.pollcheck
        if (item.pollcheck === true) {
          this.state.SelectedFakeContactList.push(item);
          this.setState({ refreshing: true });
        } else if (item.pollcheck === false) {
          const i = this.state.SelectedFakeContactList.indexOf(item)
          if (1 != -1) {
            this.state.SelectedFakeContactList.splice(i, 1)
            return this.state.SelectedFakeContactList
          }
        }
      }
    })
    let data = JSON.stringify({
      fId: feedItem.feed_id,
      foId: item.option_id,
    });
    LogUtils.infoLog(data)
    this.uploadPoll(data, token);

  }

  onFollowPressed = (hey) => {
    this.state.feeds.map((item) => {
      if (hey.user_id > 0) {
        if (item.user_id === hey.user_id) {
          LogUtils.infoLog1("Item userId", item.user_id);
          item.followcheck = true
        }

      } else {

        if (item.tr_id === hey.tr_id) {
          item.followcheck = true
        }

      }
    })
    this.setState({ fakeContact: this.state.feeds })
    let data = JSON.stringify({
      flwngId: hey.user_id,
      tr_id: hey.tr_id,
      userId: userId,
    });
    LogUtils.infoLog(data)
    this.uploadFollow(data, token);
  }

  componentWillReceiveProps() {
    LogUtils.infoLog('Will Receive is called');
  }

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{ marginTop: 5 }} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{ marginTop: 5 }} onPress={handlePress}>
        Show less
      </Text>
    );
  }

  _handleTextReady = () => {
    // ...
  }

  toggleNumberOfLines = index => {
    this.setState({
      textShown: this.state.textShown === index ? -1 : index,
    });
  };

  renderReadMore = (feedItem, index) => {
    if (feedItem.description.length >= 130) {
      return (
        <View>
          <Text
            onPress={() => this.toggleNumberOfLines(index)}
            style={{ color: '#8c52ff', fontSize: 12, fontFamily: 'Rubik-Medium', paddingTop: 5, }}>
            {this.state.textShown === index ? 'read less' : 'read more'}
          </Text>

        </View>
      );
    }


  }


  renderReadMoreTitle = (feedItem, index) => {
    if (feedItem.title.length >= 130) {
      return (
        <View>
          <Text
            onPress={() => this.toggleNumberOfLines(index)}
            style={{ color: '#8c52ff', fontSize: 12, fontFamily: 'Rubik-Medium', marginLeft: 10, marginBottom: 10, marginTop: -5, zIndex: 1 }}>
            {this.state.textShown === index ? 'read less' : 'read more'}
          </Text>

        </View>
      );
    }


  }

  renderfileType = (feeditem, index) => {
    switch (feeditem.file_type) {
      case 1:
        return (
          <View>
            {feeditem.title
              ? (
                <View>
                  <Text
                    numberOfLines={this.state.textShown === index ? undefined : 3}
                    style={styles.titleTextStyle}>
                    {feeditem.title}
                  </Text>
                  {this.renderReadMoreTitle(feeditem, index)}
                </View>
                // <Text style={styles.titleTextStyle}>{feeditem.title}</Text>
              )
              : (
                <View>
                </View>
              )}

            <View>
              <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', }}>
                <Image1
                  style={{ borderRadius: 10 }}
                  width={Dimensions.get('window').width - 20} // height will be calculated automatically
                  source={{ uri: feeditem.file_url }}
                  onPress={() => {
                    this.toggleModal(!this.state.modalVisible);
                    this.setState({ imageUri: feeditem.file_url });
                  }}
                />

              </View>
            </View>

          </View>

        );
      case 2:
        return (

          <View style={styles.container}>
            {feeditem.title
              ? (
                <View>
                  <Text
                    numberOfLines={this.state.textShown === index ? undefined : 3}
                    style={styles.titleTextStyle}>
                    {feeditem.title}
                  </Text>
                  {this.renderReadMoreTitle(feeditem, index)}
                </View>
              )
              : (
                <View>
                </View>
              )}

            <View style={styles.containerVideo}>

              <Image1
                style={{ borderRadius: 10, }}
                height={215}
                width={Dimensions.get('window').width - 20} // height will be calculated automatically
                source={{ uri: feeditem.thumbnail_img }}

              />


              <TouchableOpacity
                style={styles.playButton}
                onPress={() => {
                  if (Platform.OS === 'android') {
                    Actions.newvideoplay({ item: feeditem, from: 'feed' });
                    // Actions.customPlayer({ item: feeditem, from: 'feed' });
                  } else {
                    Actions.afPlayer({ item: feeditem, from: 'feed' });
                  }
                }}>
                <Image
                  source={require('../res/ic_play.png')}
                  style={styles.playImageStyle}
                />
              </TouchableOpacity>
            </View>
          </View>

        );
      case 0:
        return (
          <View >
            <View style={{ margin: 10 }}>
              <Text
                numberOfLines={this.state.textShown === index ? undefined : 3}
                style={styles.descriptionTextStyle}>
                {feeditem.description}
              </Text>
              {this.renderReadMore(feeditem, index)}
            </View>
          </View>
        );
      case 3:
        return (
          <View >
            <View style={{ margin: 10 }}>
              <Text
                numberOfLines={this.state.textShown === index ? undefined : 3}
                style={styles.descriptionTextStyle}>
                {feeditem.description}
              </Text>
              {this.renderReadMore(feeditem, index)}
            </View>
            <FlatList
              data={feeditem.poll_arr}
              keyExtractor={item => item.option_id}
              refreshing={this.state.refreshing}
              extraData={this.state}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => {
                return (
                  <View key={item.option_id}>
                    {feeditem.pollcheck
                      ? (
                        <View style={styles.containerPercentageStyle}>
                          <View style={{
                            width: `${item.poll_result} %`, height: 50, position: 'absolute', backgroundColor: '#D6D9E5', borderTopLeftRadius: 8,
                            borderBottomLeftRadius: 8,
                          }}>
                          </View>
                          <View >
                            <Text style={styles.optnText}>{`${item.option_name}`}</Text>
                          </View>
                          <Text style={styles.percentageText}>{`${item.poll_result} %`}</Text>
                        </View>
                      )
                      : (
                        <TouchableOpacity style={styles.containerMaleStyle} onPress={() => { this.onPollPressed(feeditem, item) }}>
                          <View >
                            <Text style={styles.countryText}>{`${item.option_name}`}</Text>
                          </View>
                          <View style={styles.mobileImageStyle}>
                            <Image source={require('../res/ic_right_arrow.png')} style={styles.mobileImageStyle} />
                          </View>
                        </TouchableOpacity>
                      )}
                  </View>
                );
              }} />

          </View>
        );
      case 4:
        return (
          <View>
            {feeditem.title
              ? (
                <View>
                  <Text
                    numberOfLines={this.state.textShown === index ? undefined : 3}
                    style={styles.titleTextStyle}>
                    {feeditem.title}
                  </Text>
                  {this.renderReadMoreTitle(feeditem, index)}
                </View>
              )
              : (
                <View>
                </View>
              )}

            <View style={{ margin: 10 }}>
              <Text
                numberOfLines={this.state.textShown === index ? undefined : 3}
                style={styles.descriptionTextStyle}>
                {feeditem.description}
              </Text>
              {this.renderReadMore(feeditem, index)}
            </View>
            <Text style={styles.articleTextStyle}
              onPress={() => Actions.webview({ url: feeditem.aricle_link })}>
              {feeditem.aricle_link}
            </Text>

            {feeditem.file_url
              ? (
                <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', }}>

                  <Image1
                    style={{ borderRadius: 10, }}
                    width={Dimensions.get('window').width - 20} // height will be calculated automatically
                    source={{ uri: feeditem.file_url }}
                  />

                </View>
              )
              : (
                <View>
                </View>
              )}



          </View>
        );
      case 5:
        return (
          <View>

            {feeditem.title
              ? (
                <View>
                  <Text
                    numberOfLines={this.state.textShown === index ? undefined : 3}
                    style={styles.titleTextStyle}>
                    {feeditem.title}
                  </Text>
                  {this.renderReadMoreTitle(feeditem, index)}
                </View>
              )
              : (
                <View>
                </View>
              )}



            <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', marginTop: 10, flexDirection: 'row', alignSelf: 'center' }}>

              <View
                style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, }}>

                <Image1
                  style={{ borderTopLeftRadius: 6, borderBottomLeftRadius: 6, }}
                  width={(Dimensions.get('window').width / 2) - 10} // height will be calculated automatically
                  source={{ uri: feeditem.file_url }}
                />
              </View>

              <View style={{ width: '50%', borderTopRightRadius: 6, borderBottomRightRadius: 6, }}>


                <Image1
                  style={{ borderTopRightRadius: 6, borderBottomRightRadius: 6, }}
                  width={(Dimensions.get('window').width / 2) - 10} // height will be calculated automatically
                  source={{ uri: feeditem.filename_url1 }}
                />

              </View>

              <View style={{ position: 'absolute', top: 0, width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '50%', alignSelf: 'center' }}>
                  <Text style={styles.desc1}>Before</Text>
                </View>
                <View style={{ width: '50%', alignSelf: 'center' }}>
                  <Text style={styles.desc1}>After</Text>
                </View>
              </View>
            </View>
          </View>

        );

      default:
        break;
    }
  };
  renderFollow = feeditem => {
    if (!(feeditem.user_id === 0 && feeditem.tr_id === 0)) {
      if ((feeditem.user_id != userId)) {

        return (
          <View>
            {feeditem.followcheck
              ? (
                <Text style={styles.followingtextStyle}>Following</Text>
              )
              : (
                <TouchableOpacity
                  onPress={() => {
                    feedUserId = feeditem.user_id;
                    this.onFollowPressed(feeditem);
                  }}>
                  <Text style={styles.followtextStyle}>Follow</Text>
                </TouchableOpacity>
              )}
          </View>

        )

      }

    }

  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false, loading: true });
        this.getTraineeFeed();

      }
      else {
        LogUtils.infoLog1("Is connected?", state.isConnected);
        this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
      }
    });

  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  renderFlatList() {
    if (this.state.isData) {
      if (Array.isArray(this.state.feeds) && this.state.feeds.length) {
        return (

          <FlatList
            contentContainerStyle={{ paddingBottom: hp('12%') }}
            extraData={this.state}
            data={this.state.feeds}
            keyExtractor={item => item.feed_id}
            onEndReached={this.handleLoadMore.bind(this)}
            onEndReachedThreshold={0.4}
            // windowSize={21}
            initialNumToRender={10}
            removeClippedSubviews={true}
            scrollEventThrottle={16}
            renderItem={({ item, index }) => (
              <View key={item.feed_id} style={styles.viewStyle}>
                <View style={[styles.row, styles.padding]}>
                  <View style={styles.row}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      style={styles.avatar}
                      source={{ uri: item.profile_url + '?w=100' }} />
                    <View style={styles.column}>
                      <View style={styles.row}>
                        <Text style={styles.textStyle}>{item.user_name}</Text>
                        {this.renderFollow(item)}
                      </View>
                      <Text style={styles.text}>{item.cdate}</Text>
                    </View>
                  </View>
                </View>

                {this.renderfileType(item, index)}

                <View style={styles.likeRow}>

                  {item.check
                    ? (
                      <Image
                        source={require('../res/ic_liked.png')}
                        style={styles.likeImageStyle}
                      />
                    )
                    : (
                      <TouchableOpacity
                        onPress={() => {
                          this.press(item);
                        }}>
                        <Image
                          source={require('../res/ic_like.png')}
                          style={styles.likeImageStyle}
                        />
                      </TouchableOpacity>
                    )}

                  <Text style={styles.likeTextStyle}>{item.like_cnt}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.feeddetails({ feedId: item.feed_id });
                    }}>
                    <Image
                      source={require('../res/ic_comment.png')}
                      style={styles.commentImageStyle}
                    />
                  </TouchableOpacity>
                  <Text style={styles.commentTextStyle}>{item.comment_cnt}</Text>
                </View>

              </View>
            )}
            keyExtractor={item => item.feed_id}
            initialNumToRender={5}
            showsVerticalScrollIndicator={false}
            refreshing={this.props.refreshing}
          />

        );
      }
      else {
        return (

          <View style={{
            height: hp('80%'),
            flexDirection: 'column',
            padding: 30,
            justifyContent: 'center',
            alignContent: 'center',
            alignSelf: 'center',
          }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/wall_nodata.png')}
              style={styles.imgNoFeed}
            />
            <Text style={styles.textNodata}>{this.state.noData}</Text>
          </View>
        );
      }
    }
  }

  onProgramsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 201,
      p_category: 'Feed',
      p_name: 'Feed->Programs',
    });
    Actions.traineeWorkoutsHome({ from: 'feed' });
  }

  onRewardsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 203,
      p_category: 'Feed',
      p_name: 'Feed->Rewards',
    });
    Actions.rewardHome({ from: 'feed' });
  }

  onTrendWorkoutsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 232,
      p_category: 'Feed',
      p_name: 'Feed->Workouts Home',
    });
    Actions.WorkoutsHome({ from: 'feed' });
  }

  async callReferAFriend() {
    LogUtils.firebaseEventLog('click', {
      p_id: 226,
      p_category: 'Feed',
      p_name: 'Feed->ReferAFriend',
    });
    Actions.refer();
  }

  onDietClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 204,
      p_category: 'Feed',
      p_name: 'Feed->Diet',
    });
    Actions.dietHome1({ from: 'feed' });
  }

  renderDietBottomMenuIcon() {
    if (!isEmpty(this.state.resObj)) {
      if (this.state.resObj.prmdietuser === 1) {
        return (
          <TouchableOpacity
            style={styles.bottomClick}
            onPress={() => this.onDietClicked()}>
            <View style={{ flexDirection: 'column', }}>
              <View style={styles.vBtmImgHeight}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_diet.png')}
                  style={{
                    width: 20,
                    height: 20,
                    alignSelf: 'center',
                    tintColor: '#d6d9e0',
                  }}
                />
              </View>
              <Text style={styles.imgBottomMenuText1}>Diet</Text>
            </View>
          </TouchableOpacity>
        );
      }
      else {
        return (
          <TouchableOpacity
            style={styles.bottomClick}
            onPress={() => this.onRewardsClicked()}>
            <View style={{ flexDirection: 'column' }}>
              <View style={styles.vBtmImgHeight}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_rewards.png')}
                  style={{
                    width: 20,
                    height: 20,
                    alignSelf: 'center',
                    tintColor: '#d6d9e0'
                  }}
                />
              </View>
              <Text style={styles.imgBottomMenuText1}>Rewards</Text>
            </View>
          </TouchableOpacity>
        );
      }
    }
    else {
      return (
        <TouchableOpacity
          style={styles.bottomClick}
          onPress={() => this.onRewardsClicked()}>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.vBtmImgHeight}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_rewards.png')}
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: 'center',
                  tintColor: '#d6d9e0'
                }}
              />
            </View>
            <Text style={styles.imgBottomMenuText1}>Rewards</Text>
          </View>
        </TouchableOpacity>
      );
    }

  }


  render() {
    return (
      <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
        <View style={styles.containerTop}>
          <TouchableOpacity
            onPress={() => this.onBackPressed()}>
            <Image
              source={require('../res/ic_back.png')}
              style={styles.backImageStyle}
            />
          </TouchableOpacity>

          <View style={{ flexDirection: 'row', marginTop: 20, }}>
            <Text style={styles.communityTextStyle}>Community</Text>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => {
                  Actions.feedpost();
                }} >
                <Text style={styles.buttonText}>ADD</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>

        {/* <SafeAreaView > */}
        <Loader loading={this.state.loading} />
        {this.renderFlatList()}

        {/* </SafeAreaView> */}
        <NoInternet
          image={require('../res/img_nointernet.png')}
          loading={this.state.isInternet}
          onRetry={this.onRetry.bind(this)} />

        <CustomDialog
          visible={this.state.isAlert}
          title='Alert'
          desc={this.state.alertMsg}
          onAccept={this.onAccept.bind(this)}
          no=''
          yes='Ok' />

        <View style={styles.mainContainer} >
          <Modal
            backdropColor="#B4B3DB"
            backdropOpacity={0.8}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={600}
            animationOutTiming={600}
            backdropTransitionInTiming={600}
            backdropTransitionOutTiming={600}
            visible={this.state.modalVisible}
            onRequestClose={() => { this.toggleModal(!this.state.modalVisible) }}>

            <View style={styles.modal}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
                source={{ uri: this.state.imageUri }}
              />


            </View>
            <TouchableOpacity style={{ position: 'absolute', top: 0, marginTop: hp('2%'), marginLeft: '2%', padding: 10 }}
              onPress={() => { this.toggleModal(!this.state.modalVisible) }}>
              <Image
                style={{
                  width: 19,
                  height: 16,
                  alignSelf: 'flex-start'
                }}
                source={require('../res/ic_back.png')}
              />
            </TouchableOpacity>
          </Modal>

        </View>

        {/* bottom nav bar */}
        {/* <View style={styles.bottom}>
          <View style={styles.bottomHome}>
            <View style={styles.bottomViewContainer}>
              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() =>  Actions.popTo('traineeHome')}>
                <View style={{ flexDirection: 'column' }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_home_gray.png')}
                    style={{
                      width: 17,
                      height: 17,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.imgBottomMenuText1}>Home</Text>
                </View>

              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.onProgramsClicked()}>
                <View style={{ flexDirection: 'column' }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_workout_gray.png')}
                    style={{ height: 23, width: 23, alignSelf: 'center', }}
                  />
                  <Text style={styles.imgBottomMenuText1}>Programs</Text>
                </View>

              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomClick}>
                <View style={{ flexDirection: 'column' }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_feed_gray.png')}
                    style={{
                      width: 17,
                      height: 17,
                      alignSelf: 'center',
                      tintColor:'#2d3142',
                    }}
                  />
                  <Text style={styles.imgBottomMenuText}>Feed</Text>
                </View>


              </TouchableOpacity>
              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.onRewardsClicked()}
              >
                <View style={{ flexDirection: 'column' }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_rewards.png')}
                    style={{
                      width: 17,
                      height: 17,
                      alignSelf: 'center',
                      tintColor: '#d6d9e0',
                    }}
                  />
                  <Text style={styles.imgBottomMenuText1}>Rewards</Text>
                </View>

              </TouchableOpacity>
            </View>
          </View>
        </View> */}

        <RBSheet
          ref={ref => {
            this.rbMenuSheet = ref;
          }}
          height={190}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: 'center',
              // alignItems: 'center',
              alignSelf: 'center',
              width: wp('95%'),
              marginVertical: 10,
              borderRadius: 10,
            }
          }}
        >
          <View style={{ flexDirection: 'column', }}>
            <View style={styles.vRBMenuBottom}>
              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close();
                  Actions.traFoodPhotos({ from: 'feed' });
                }}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_foodphoto.png')}
                      style={{
                        width: 23,
                        height: 23,
                        tintColor: '#2d3142',
                        alignSelf: 'flex-start',
                      }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>Food{'\n'}Photos</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  LogUtils.firebaseEventLog('click', {
                    p_id: 231,
                    p_category: 'Home',
                    p_name: 'Home->ChatList',
                  });
                  Actions.chatlist({ from: 'feed' });
                }}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_bm_oneononechat.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142', }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>One on one{'\n'}Chat</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  LogUtils.firebaseEventLog('click', {
                    p_id: 222,
                    p_category: 'Home',
                    p_name: 'Home->Select a Plan',
                  });
                  Actions.traAllBuyPlans1({screen:"Trainee Wall"});
                }}>

                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_bm_viewplans.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>View{'\n'}Plans</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  this.callReferAFriend();
                }}>

                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_refer_new.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>Refer a{'\n'}Friend</Text>
                </View>
              </TouchableOpacity>

              {this.state.resObj.prmdietuser === 1
                ?
                (
                  <TouchableOpacity
                    style={styles.bottomrbMenuClick}
                    onPress={() => {
                      this.rbMenuSheet.close()
                      this.onRewardsClicked()
                    }}>
                    <View style={{ flexDirection: 'column' }}>
                      <View style={styles.vBtmImgHeight}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_rewards1.png')}
                          style={{
                            width: 23,
                            height: 23,
                            alignSelf: 'center',
                            tintColor: '#2d3142'
                          }}
                        />
                      </View>
                      <Text style={styles.imgBottomMenuText}>Rewards{'\n'}</Text>
                    </View>
                  </TouchableOpacity>
                )
                :
                (
                  <View></View>
                )
              }

            </View>

            <View style={{ height: 15, }}></View>

            <View style={styles.vRBMenuBottom}>
              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  LogUtils.firebaseEventLog('click', {
                    p_id: 214,
                    p_category: 'Home',
                    p_name: 'Home->TopSellers',
                  });
                  Actions.webview({ url: shopping_url });
                }}>
                <View style={{ flexDirection: 'column', }}>

                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_store.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>Store</Text>
                </View>
              </TouchableOpacity>



              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  Actions.appointments({ from: 'feed' });
                }}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_bm_appointments.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>Appointments</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  //this.onFeedPressed()
                }}>
                <View style={{ flexDirection: 'column', }}>

                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_bm_feed.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#8c52ff' }}
                    />
                  </View>
                  <Text style={[styles.imgBottomMenuText, { color: '#8c52ff' }]}>Feed</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => {
                  this.rbMenuSheet.close()
                  LogUtils.firebaseEventLog('click', {
                    p_id: 230,
                    p_category: 'Home',
                    p_name: 'Home->PCFDetails',
                  });
                  Actions.pcfdetails({ from: 'feed' });
                }}>

                <View style={{ flexDirection: 'column', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_macros.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>Macros</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomrbMenuClick}
                onPress={() => this.rbMenuSheet.close()}>
                <View style={{ flexDirection: 'column', alignSelf: 'center', }}>
                  <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_more_new.png')}
                      style={{
                        width: 23,
                        height: 23,
                        tintColor: '#ffffff',
                        alignSelf: 'center',
                        tintColor: '#8c52ff'
                      }}
                    />
                  </View>
                  <Text style={[styles.imgBottomMenuText, { color: '#8c52ff' }]}>More</Text>
                </View>
              </TouchableOpacity>

            </View>
          </View>
        </RBSheet>

        {/* bottom nav bar */}
        <View style={styles.bottom}>
          <View style={styles.bottomHome}>
            <View style={styles.bottomViewContainer}>
              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => Actions.popTo('traineeHome')}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_home_gray.png')}
                      style={styles.imgBottomMenu}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Home</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.onProgramsClicked()}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_workout_gray.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Programs</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.onTrendWorkoutsClicked()}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_workout.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Workouts</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => Actions.trackershome({ from: 'feed' })}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_tracker.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0' }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Tracker</Text>
                </View>
              </TouchableOpacity>

              {this.renderDietBottomMenuIcon()}

              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.rbMenuSheet.open()}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_more_new.png')}
                      style={{
                        width: 23,
                        height: 23,
                        tintColor: '#ffffff',
                        alignSelf: 'center',
                        tintColor: '#2d3142'
                      }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText}>More</Text>
                </View>
              </TouchableOpacity>

            </View>
          </View>
        </View>

        {/* <ActionButton size={65} radius={110} position="center" bgColor="rgba(0, 0, 0, 0.7)" buttonColor="#8c52ff">
          <ActionButton.Item
            buttonColor={'transparent'}
            size={70}
            title="New Task"
            onPress={() => {
              LogUtils.firebaseEventLog('click', {
                p_id: 218,
                p_category: 'Home',
                p_name: 'Home->Breakfast',
              });
              Actions.traFoodList({ mFrom: "home", title: "Breakfast", foodType: 2, mdate: moment(new Date()).format("YYYY-MM-DD") });
            }}>
            <View style={{ width: 70, height: 70, flexDirection: 'column', }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_breakfast_blue.png')}
                style={styles.arcImg}
              />
              <Text style={styles.arcTitle}>Breakfast</Text>
            </View>
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor={'transparent'}
            size={70}
            title="Notifications"
            onPress={() => {
              LogUtils.firebaseEventLog('click', {
                p_id: 219,
                p_category: 'Home',
                p_name: 'Home->Lunch',
              });
              Actions.traFoodList({ mFrom: "home", title: "Lunch", foodType: 4, mdate: moment(new Date()).format("YYYY-MM-DD") });
            }}>
            <View style={{ width: 70, height: 70, flexDirection: 'column', }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_lunch_blue.png')}
                style={styles.arcImg}
              />
              <Text style={styles.arcTitle}>Lunch</Text>
            </View>
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor={'transparent'}
            size={70}
            title="All Tasks"
            onPress={() => {
              LogUtils.firebaseEventLog('click', {
                p_id: 220,
                p_category: 'Home',
                p_name: 'Home->Dinner',
              });
              Actions.traFoodList({ mFrom: "home", title: "Dinner", foodType: 6, mdate: moment(new Date()).format("YYYY-MM-DD") });
            }}>
            <View style={{ width: 70, height: 70, flexDirection: 'column' }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_dinner_blue.png')}
                style={styles.arcImg}
              />
              <Text style={styles.arcTitle}>Dinner</Text>
            </View>
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor={'transparent'}
            size={70}
            title="All Tasks"
            onPress={() => {
              LogUtils.firebaseEventLog('click', {
                p_id: 221,
                p_category: 'Home',
                p_name: 'Home->Snack',
              });
              Actions.traFoodList({ mFrom: "home", title: "Snack", foodType: 5, mdate: moment(new Date()).format("YYYY-MM-DD") });
            }}>
            <View style={{ width: 70, height: 70, flexDirection: 'column' }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_snacks_blue.png')}
                style={styles.arcImg}
              />
              <Text style={styles.arcTitle}>Snack</Text>
            </View>
          </ActionButton.Item>
        </ActionButton> */}

      </ImageBackground>
      // </View>
    );
  }
  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#f4fcfc'
  },
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  containerVideo: {
    flex: 1,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  containerTop: {
    flexDirection: 'column',
    margin: '6%'
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('1%'),
    alignSelf: 'flex-start',
  },
  communityTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(24, screenHeight),
    fontWeight: '500',
    flex: 1,
  },
  item: {
    width: 375,
    height: 355,
    backgroundColor: '#ffffff',
    margin: 15,
  },
  title: {
    fontSize: 32,
  },

  row: {
    flexDirection: 'row',
    alignItems: "center",

  },
  column: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignSelf: 'center',
  },
  padding: {
    padding: 5,
  },
  avatar: {
    width: 40,
    height: 40,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: profileImageSize / 2,
    width: profileImageSize,
    height: profileImageSize,
    resizeMode: "cover",
    marginRight: padding
  },
  text: {
    height: 25,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(12, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.17,
    marginTop: 2,
  },
  textStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(16, screenHeight),
    fontWeight: '500',
    letterSpacing: 0.23,
    marginTop: 10,
  },
  followtextStyle: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(12, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    marginLeft: 20,
    marginTop: 15,
  },
  followingtextStyle: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(12, screenHeight),
    fontWeight: '300',
    letterSpacing: 0.23,
    marginLeft: 15,
    marginTop: 10,
  },
  titleTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(16, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    margin: 10,
  },
  descriptionTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    margin: 10,
  },
  articleTextStyle: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(16, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    marginLeft: 10,
    marginBottom: 10,
  },
  likeImageStyle: {
    width: 21,
    height: 19,
    marginLeft: 10,
    alignSelf: 'center'
  },
  commentImageStyle: {
    width: 20,
    height: 19,
    marginLeft: 10,
  },
  likeTextStyle: {
    width: 18,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
    textAlign: 'center',
    alignSelf: 'center'
  },
  commentTextStyle: {
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
  },
  likeRow: {
    flexDirection: 'row',
    alignItems: "center",
    marginTop: 15,
    marginBottom: 15,
  },
  viewStyle: {
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#d6d9e0',
    backgroundColor: 'white',
    marginBottom: 7,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  playButton: {
    padding: 10,
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  playImageStyle: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ImageStyle: {
    position: 'absolute',
    right: 20,
    alignSelf: 'center'
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('7%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 1,
  },
  containerPercentageStyle: {
    width: wp('90%'),
    height: 50,
    borderColor: '#ddd',
    borderRadius: 10,
    borderWidth: .5,
    marginLeft: 10,
    justifyContent: 'center',
    backgroundColor: '#EFF2F8',
    marginBottom: 10,
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  percentageText: {
    color: 'black',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '500',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'right',
  },
  optnText: {
    color: '#686C7B',
    fontFamily: 'Rubik-Regular',
    fontSize: RFValue(16, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'left',
    position: 'absolute',
  },
  mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
  linearGradient: {
    width: '17%',
    height: 32,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'relative',
    alignSelf: 'flex-end',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 6,
  },
  buttonText: {
    fontSize: 12,
    textAlign: 'center',
    marginRight: 5,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  textNodata: {
    // width: wp('80%'),
    fontSize: 14,
    fontWeight: '400',
    // letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 20,
    margin: 20,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
    justifyContent: 'center',
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  desc1: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    marginTop: 15,
  },
  // bottom: {
  //   flex: 1,
  //   justifyContent: 'flex-end',
  // },
  // bottomHome: {
  //   width: wp('100%'),
  //   height: hp('7%'),
  //   backgroundColor: '#ffffff',
  //   shadowColor: 'rgba(0, 0, 0, 0.08)',
  //   shadowOffset: { width: 5, height: 0 },
  //   shadowOpacity: 0.25,
  //   shadowRadius: 10,
  //   elevation: 6,
  //   borderColor: '#ddd',
  //   borderTopWidth: .2,
  // },
  // bottomHome1: {
  //   width: wp('90%'),
  //   height: 40,
  // },
  // bottomViewContainer: {
  //   flexDirection: 'row',
  //   flex: 1,
  //   alignItems: 'center',
  //   width: wp('100%'),
  // },
  // bottomViewContainer1: {
  //   flexDirection: 'row',
  //   flex: 1,
  //   alignItems: 'center',
  //   width: wp('90%'),
  //   marginBottom: 8,
  // },
  // bottomClick: {
  //   width: wp('25%'),
  //   alignItems: 'center',
  // },
  // imgBottomMenu: {
  //   width: 17,
  //   height: 17,
  //   alignSelf: 'center',
  // },
  // imgBottomMenuText: {
  //   alignSelf: 'center',
  //   fontSize: 9,
  //   fontWeight: '500',
  //   color: '#2d3142',
  //   lineHeight: 14,
  //   textAlign: 'center',
  //   fontFamily: 'Rubik-Medium',
  // },
  // imgBottomMenuText1: {
  //   alignSelf: 'center',
  //   fontSize: 9,
  //   fontWeight: '500',
  //   color: '#d6d5dd',
  //   lineHeight: 14,
  //   textAlign: 'center',
  //   fontFamily: 'Rubik-Medium',
  // },


  bottom: {
    // flex: 1,
    justifyContent: 'flex-end',
    height: 40,
    backgroundColor: '#ffffff',
  },
  bottomHome: {
    width: wp('100%'),
    height: hp('7%'),
    backgroundColor: '#ffffff',
  },
  bottomHome1: {
    width: wp('90%'),
    height: 40,
  },
  bottomViewContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    width: wp('100%'),
    backgroundColor: '#ffffff',
  },
  vRBMenuBottom: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp('95%'),
    marginBottom: 10,
    marginTop: 10,
  },
  bottomrbMenuClick: {
    width: wp('19%'),
    // width: wp('23.75%'),
    alignItems: 'center',
  },
  bottomViewContainer1: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    width: wp('90%'),
    marginBottom: 8,
  },
  bottomClick: {
    width: wp('16.66%'),
    bottom: -5,
    alignItems: 'center',
  },
  imgBottomMenu: {
    width: 17,
    height: 17,
    alignSelf: 'center',
  },
  vBtmImgHeight: {
    height: 23,
    width: 23,
    flexDirection: 'column',
    alignSelf: 'center',
  },
  imgBottomMenuText: {
    alignSelf: 'center',
    fontSize: 9,
    fontWeight: '500',
    color: '#2d3142',
    // lineHeight: 14,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  imgBottomMenuText1: {
    alignSelf: 'center',
    fontSize: 9,
    fontWeight: '500',
    color: '#d6d5dd',
    lineHeight: 14,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  arcImg: {
    width: 55,
    height: 55,
    alignSelf: 'center',
    zIndex: -2,
  },
  arcTitle: {
    alignSelf: 'center',
    fontSize: 11,
    fontWeight: '500',
    color: '#ffffff',
    lineHeight: 14,
    marginTop: 5,
    zIndex: -2,
    marginBottom: 50,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    alignSelf: 'center',
  },
  imgNoFeed: {
    width: 250,
    height: 153,
    alignSelf: 'center',
  },

});

const mapStateToProps = state => {
  const { feeds, uploadLike, poll } = state.feed;
  const loading = state.feed.loading;
  const refreshing = state.feed.refreshing;
  const pageNo = state.feed.pageNo;
  const error = state.feed.error;
  return { feeds, loading, refreshing, pageNo, error, uploadLike, poll };
};

export default connect(
  mapStateToProps,
  {
    getTraineeFeed,
    uploadLike,
    uploadPoll,
    getTraineeFeedLoadMore,
    uploadFollow,
  },
)(Trainee_Wall);
