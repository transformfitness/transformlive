import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  FlatList,
  TouchableWithoutFeedback,
  RefreshControl,
  ScrollView,
  Linking,
  Platform,
  Dimensions,
  TextInput,
  Modal,
  Animated,
  ActivityIndicator,
  Alert
  //SafeAreaView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
//import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { ProgressBar, NoInternet, HomeDialog, Loader, PopupDialog, VersionCheckInfo, ProgressBar1, ImagePreview } from './common';
import AppIntroSlider from 'react-native-app-intro-slider';
import GoogleFit, { Scopes } from 'react-native-google-fit';
import AppleHealthKit from 'rn-apple-healthkit';
import Share1 from 'react-native-share';
import { RFValue } from "react-native-responsive-fontsize";
const screenHeight = Math.round(Dimensions.get('window').height);
import Slidemenu from './Slidemenu';
import DeviceInfo from 'react-native-device-info';
import LinearGradient from 'react-native-linear-gradient';
import firebase from 'react-native-firebase';
import RNFetchBlob from 'rn-fetch-blob'
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, VINFO_ANDROID, VINFO_IOS } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import Orientation from 'react-native-orientation';
import StarRating from './Starrating';
import Dialog, {
  DialogContent,
  //SlideAnimation
} from 'react-native-popup-dialog';
import { AirbnbRating } from 'react-native-ratings';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
//import FastImage from 'react-native-fast-image';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { copilot, walkthroughable, CopilotStep } from 'react-native-copilot';
import RNExitApp from 'react-native-exit-app';
import PropTypes from 'prop-types';
import { RNS3 } from 'react-native-aws3';
import RBSheet from "react-native-raw-bottom-sheet";
//import { FlatListSlider } from 'react-native-flatlist-slider';
import FitnessProducts from './Home/FitnessProducts';
import NewAppointments from './Home/NewAppointments';
import Challenges from './Home/Challenges';
import Macros from './Home/Macros';
import { getProductsAction, getHomepTrendingWorkoutsAction, getHomepageDetAction,GetGoalsubctgAction } from '../actions/traineeActions'

//const WalkthroughableText = walkthroughable(Text);
//const WalkthroughableImage = walkthroughable(Image);
const WalkthroughableView = walkthroughable(View);

let popupHeight = screenHeight / 2, covidPopupHeight = screenHeight / 2.5;
// import appsFlyer from 'react-native-appsflyer';

// const futch = (url, opts = {}, onProgress) => {
//   LogUtils.infoLog1(url, opts)
//   return new Promise((res, rej) => {
//     var xhr = new XMLHttpRequest();
//     xhr.open(opts.method || 'get', url);
//     for (var k in opts.headers || {})
//       xhr.setRequestHeader(k, opts.headers[k]);
//     xhr.onload = e => res(e.target);
//     xhr.onerror = rej;
//     if (xhr.upload && onProgress)
//       xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
//     xhr.send(opts.body);
//   });
// }

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

let currentDate, token, fitbitToken, trackId, fitbitUserId = '', fitbitUserWeight = '';;
let stepsArray = [], caloriesArray = [], distanceArray = [];
let isServiceCalled = false;
let bigImage, type = '', typeid, uc_id;
let notific_id = '';

// import Database from '../Database';
// const db1 = new Database();

function getData(access_token, user_id) {
  try {
    // fetch('https://api.fitbit.com/1/user/-/activities/date/today.json', {
    fetch('https://api.fitbit.com/1/user/-/activities/tracker/steps/date/today/7d.json', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      // body: `root=auto&path=${Math.random()}`
    })
      .then(res => res.json())
      .then(res => {

        LogUtils.infoLog1(`res:`, res);
        stepsArray = [];
        caloriesArray = [];
        res["activities-tracker-steps"].map((item) => {

          stepsArray.push({ 'act': 'summary', 'from': item.dateTime, 'to': item.dateTime, 'steps': item.value, 'cals': 0, 'dist': 0 });
          // stepsArray.push({ 'act': 'summary', 'c_date': item.dateTime, 'steps': item.value,'cals' : 0 });

        })
        LogUtils.infoLog1("Steps Array", stepsArray);
        if (Array.isArray(stepsArray) && stepsArray.length) {

          LogUtils.infoLog1("body", stepsArray);
          // sendStepsData(data);

          getFitbitCaloriesData(fitbitToken, stepsArray);
        }
      })
      .catch(err => {
        console.error('Error: ', err);
      });
  } catch (error) {
    console.log(error)
  }
}

function getFitbitCaloriesData(access_token, data) {
  try {
    fetch('https://api.fitbit.com/1/user/-/activities/tracker/activityCalories/date/today/7d.json', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      // body: `root=auto&path=${Math.random()}`
    })
      .then(res => res.json())
      .then(res => {

        LogUtils.infoLog1(`res:`, res);
        if (data) {
          data.map((dataItem) => {
            res["activities-tracker-activityCalories"].map((item) => {

              if (dataItem.from === item.dateTime) {
                dataItem.cals = item.value
              }
            })
          })
        }

        LogUtils.infoLog1(" Calories data", data);

        getFitbitDistanceData(fitbitToken, data);

      })
      .catch(err => {
        console.error('Error: ', err);
      });
  } catch (error) {
    console.log(error)
  }
}

function getFitbitDistanceData(access_token, data) {
  try {
    fetch('https://api.fitbit.com/1/user/-/activities/tracker/distance/date/today/7d.json', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      // body: `root=auto&path=${Math.random()}`
    })
      .then(res => res.json())
      .then(res => {

        LogUtils.infoLog1(`res:`, res);

        if (data) {
          data.map((dataItem) => {

            res["activities-tracker-distance"].map((item) => {

              if (dataItem.from === item.dateTime) {
                dataItem.dist = item.value * 1000
                dataItem.from = item.dateTime + ' 00:00:00'
                dataItem.to = item.dateTime + ' 23:59:59'
              }
            })
          })
        }

        LogUtils.infoLog1("Final data", data);

        getFitbitActivitiesData(fitbitToken, data);

      })
      .catch(err => {
        console.error('Error: ', err);
      });
  } catch (error) {
    console.log(error)
  }
}

function getFitbitActivitiesData(access_token, data) {
  try {
    fetch('https://api.fitbit.com/1/user/-/activities/list.json?afterDate=' + moment().subtract(7, 'days').format("YYYY-MM-DD") + '&sort=asc&offset=0&limit=100', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      // body: `root=auto&path=${Math.random()}`
    })
      .then(res => res.json())
      .then(res => {

        LogUtils.infoLog1(`res activities:`, res);

        if (res) {
          if (res.activities.length > 0) {

            res.activities.map((item) => {

              if (item.logType != 'manual') {
                var fromDate = item.originalStartTime.split('T')[0] + ' ' + item.originalStartTime.split('T')[1].split('.')[0];
                LogUtils.infoLog1("fromDate", fromDate);
                var milliseconds = Date.parse(item.originalStartTime); // some mock date
                LogUtils.infoLog1("milliseconds", milliseconds);

                var totalMillis = 0;
                if (milliseconds) {
                  totalMillis = milliseconds + item.originalDuration;
                  LogUtils.infoLog1("totalMillis", totalMillis);
                }
                data.push({ 'act': item.activityName, 'from': fromDate, 'to': moment(totalMillis).format("YYYY-MM-DD HH:mm:ss"), 'steps': item.steps, 'cals': item.calories, 'dist': item.distance * 1000 });
              }
            })
          }
        }

        LogUtils.infoLog1("Final With Activity Data", data);

        if (Array.isArray(data) && data.length) {

          let body = JSON.stringify({
            td_id: 1,
            td_unq_id: fitbitUserId,
            weight: fitbitUserWeight,
            fitbit_token: access_token,
            act_arr: data,
            // cal_arr: caloriesArray,

          });
          LogUtils.infoLog1("body", body);
          saveuseractivitytran(body);
        }
      })
      .catch(err => {
        console.error('Error: ', err);
      });
  } catch (error) {
    console.log(error)
  }
}

function getUserProfile(access_token) {
  try {
    fetch('https://api.fitbit.com/1/user/-/profile.json', {
      // fetch('https://api.fitbit.com/1/user/-/body/log/weight/date/2020-11-24.json', {

      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
      // body: `root=auto&path=${Math.random()}`
    })
      .then(res => res.json())
      .then(res => {

        LogUtils.infoLog1(`res:`, res);
        if (res.user) {
          fitbitUserWeight = res.user.weight;
        }

      })
      .catch(err => {
        console.error('Error: ', err);
      });
  } catch (error) {
    console.log(error)
  }
}

function saveuseractivitytran(body) {
  try {
    LogUtils.infoLog1('Url', `${BASE_URL}/trainee/saveuseractivitytran`);
    LogUtils.infoLog1("body", body);
    fetch(
      `${BASE_URL}/trainee/saveuseractivitytran`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: body,
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {

        } else {
          if (data.message === 'You are not authenticated!') {
            // this.setState({ isAlert: true, alertMsg: data.message });
          } else {
            // this.setState({ isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        // this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
      });
  } catch (error) {
    console.log(error)
  }
}

// function sendStepsData(body) {
//   try {
//     fetch(
//       `${BASE_URL}/trainee/saveuserstepsnew`,
//       {
//         method: 'POST',
//         headers: {
//           Accept: 'application/json',
//           'Content-Type': 'application/json',
//           Authorization: `Bearer ${token}`,
//         },
//         body: body,
//       },
//     )
//       .then(processResponse)
//       .then(res => {
//         const { statusCode, data } = res;
//         LogUtils.infoLog1('statusCode', statusCode);
//         LogUtils.infoLog1('data', data);
//         if (statusCode >= 200 && statusCode <= 300) {
//           if (!isServiceCalled && trackId === '1') {
//             isServiceCalled = true;
//             getFitbitCaloriesData(fitbitToken);
//           }
//         } else {
//           if (data.message === 'You are not authenticated!') {
//           } else {
//           }
//         }
//       })
//       .catch(function (error) {
//         // this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
//       });
//   } catch (error) {
//     console.log(error)
//   }
// }

const HKPERMS = AppleHealthKit.Constants.Permissions;
const HKOPTIONS = {
  permissions: {
    read: [
      HKPERMS.StepCount,
      HKPERMS.ActiveEnergyBurned,
      HKPERMS.DistanceWalkingRunning,
      HKPERMS.Workout,
    ],
  }
};

function isEmpty(obj) {
  try {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  } catch (error) {
    console.log(error)
  }
}

class TraineeHome extends Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };

  constructor(props) {
    super(props);

    LogUtils.showSlowLog(this);
    // this.trace = null;

    this.state = {
      fill: 80,
      userName: '',
      goalName: '',
      weight: '',
      profImg: '',
      isOpen: false,
      //selectedItem: 'About',//1
      open: false,
      currentMenu: 'slide',
      loading: false,
      isAlert: false,
      alertTitle: '',
      alertMsg: '',
      isSuccess: false,
      sucMsg: '',
      titMsg: '',
      resObj: {},
      glsArray: [],
      noDataWorkouts: '',
      noDataChallenges: '',
      //slideindex: 0,
      slideHeadText: '',
      isInternet: false,
      isSaveWater: false,
      refreshing: false,

      isAppUpdatePop: false,
      isAppUpdateTit: '',
      isAppUpdateMsg: '',
      isAppCancel: '',
      isAppUpdate: '',
      //osId: 0,
      //appversion: '',
      versionResObj: {},
      isAcceptChallenge: false,

      alertobj: {},
      isShowPopup: false,
      isPopupImage: '',
      isPopupCancel: '',
      isPopupOk: '',
      isPopupNavigate: '',
      isPopupNavigateId: '',
      popupId: 0,
      isPopupOpened: 0,
      web_url: '',
      poppupImageArr: [],
      thumbnailUrl: '',
      mediaType: '',

      //Diet
      foodType: 1,
      mainOption: 1,
      isSubListVisible: false,
      headerTitle: 'How does it work',
      //noPrefCarb: '',
      products: [],
      isShowDiet: 0,
      isShowAppt: 0,
      isShowFreeTrail: 0,

      disabled: false,
      showAppointmentFeedback: false,
      feedbackTitle: '',
      feedbackDesc: '',
      rating: 5,
      comments: '',
      isSaveApntFeedback: false,
      // filepath: {
      //   data: '',
      //   uri: '',
      // },
      fileData: '',
      fileUri: '',
      fileName: '',
      fileType: '',
      isProgress: false,
      progress: 10,
      aimtId: 0,
      isRewardComplete: false,
      //taskType: '',
      //taskName: '',
      isAddFoodImgPop: false,
      isShowInfoPopup: false,
      foodPhotos: [],
      freeAppointmentPopup: false,
      //freeAppointTitle: '',
      //freeAppointDesc: '',
      dietItems: {},
      isLogoutApp: false,
      first_time_home: '0',

      bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
      confirmSuccess: false,
      freeTrialObj: {},
      //buttonState: '',
      freeTrialSuccessTitle: '',
      freeTrialSuccessMessage: '',
      onConfirmClick: false,
      //freeTrialConfirmImg: '',
      //freeTrialConfirmTitle: '',
      //freeTrialConfirmText: '',
      popuploading: false,

      email: '',
      freeTrailProcess: 1,
      emailOtp: '',
      emailSuccessTitle: '',
      emailSuccessDesc: '',

      freeTrialInitialObj: {},
      //activeSlide1: 0,
      arrTrendingWorkouts: [],
      resObjAwsS3: {},

      covidDietProcess: 1,
      covid_imp: {},
      covidPlanSucObj: {},
      covid_diet_sent: 0,

      alertService:{},
      updateService:{}
    };
    this.toggle = this.toggle.bind(this);
    this.getHomepageDet = this.getHomepageDet.bind(this);
    // this.getHomepTrendingWorkouts  = this.getHomepTrendingWorkouts.bind(this);
    // this.getProducts = this.getProducts.bind(this);
  }

  async componentDidMount() {
    try {

      Orientation.lockToPortrait();
      allowFunction();

      if (popupHeight < 370) {
        popupHeight = 370;
      }
      let show_apptour = await AsyncStorage.getItem('show_apptour');
      let firsttimehome = await AsyncStorage.getItem('first_time_home');
      token = await AsyncStorage.getItem('token');
      fitbitToken = await AsyncStorage.getItem('fitbitaccess_token');
      fitbitUserId = await AsyncStorage.getItem('td_unq_id');
      trackId = await AsyncStorage.getItem('trackId');

      LogUtils.infoLog1("Did mount called", trackId);
      currentDate = this.ShowCurrentDate();
      isServiceCalled = false;
      this.getNotiId(); //Notification ID

      this.setState({
        userName: await AsyncStorage.getItem('userName'),
        goalName: await AsyncStorage.getItem('goalName'),
        weight: await AsyncStorage.getItem('weight'),
        profImg: await AsyncStorage.getItem('profileImg'),
        refreshing: false,
        first_time_home: firsttimehome,
      });

      Linking
        .getInitialURL()
        .then(url => {
          this._handleOpenURL({ url });
        }).catch(error => console.error(error));

      NetInfo.fetch().then(state => {
        if (state.isConnected) {

          this.createNotificationChannel();

          if (trackId === '1' && fitbitToken) {
            getUserProfile(fitbitToken);
            getData(fitbitToken);
          }
          else if (trackId === '2' && Platform.OS === 'android') {
            this.sendGoogleFitData();
          } else if (trackId === '2' && Platform.OS === 'ios') {
            this.appleAuth();
          }

          this.getHomepageDet();

          let params = { "g_id": "0" };
          this.props.GetGoalsubctgAction(token, params);
 
          if (this.state.first_time_home == '1') { // '1'

            setTimeout(() => {
              this.checkVersion()
            }, 1000);
          }

          this.createNotificationListeners();

        }
        else {
          this.setState({ isInternet: true });
        }
      });

      //App tour
      // const interval = setInterval(() => {
      //   if (!isEmpty(this.state.resObj)) {
      //     if (show_apptour === '0') {//1//
      //       if (this.props.walkThrough) {
      //         if (this.props.walkThrough === 1) {
      //           //this.props.copilotEvents.on('stepChange', this.handleStepChange);
      //           // this.props.start(true, this.scrollView);
      //           this.saveShowcaseViewFlag();
      //           //this.props.start();
      //         }
      //       }
      //     }
      //     clearInterval(interval);
      //   }
      // }, 100);

      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.onBackPressed();
        return true;
      });

    } catch (error) {
      console.log(error)
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  onBackPressed() {
    try {
      if (this.state.isRewardComplete) {
        this.setState({ isRewardComplete: false });
      }
      else if (this.state.showAppointmentFeedback) {
        this.setState({ showAppointmentFeedback: false });
      }
      else if (this.state.freeAppointmentPopup) {
        this.setState({ freeAppointmentPopup: false });
      }
      else if (this.state.isAddFoodImgPop) {
        this.setState({ isAddFoodImgPop: false });
      }
      else {
        this.setState({ isSuccess: true, titMsg: 'Are you sure!', sucMsg: 'Do you really want to exit?' });
      }
    } catch (error) {
      console.log(error)
    }
  }

  ShowCurrentDate = () => {
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    currentDate = year + '-' + month + '-' + date;
  }

  handleStepChange = (step) => {
    console.log(`Current step is: ${step.name}`);
  }

  async getNotiId() {
    let notiId = '';
    notiId = await AsyncStorage.getItem('notificationID');
    notific_id = notiId;
    LogUtils.infoLog1("NOTIID", notiId);
    return notiId;
  }

  _handleOpenURL = (url) => {
    //LogUtils.infoLog1("URL", url);
    if (url.url != null && (trackId === null || trackId === '0')) {
      Actions.trackdevices();
    }
  };

  createNotificationChannel = () => {
    try {
      // Build a android notification channel
      const channel = new firebase.notifications.Android.Channel(
        "reminder", // channelId
        "Reminders Channel", // channel name
        firebase.notifications.Android.Importance.High // channel importance
      ).setDescription("Used for getting reminder notification"); // channel description
      // Create the android notification channel
      firebase.notifications().android.createChannel(channel);
    } catch (error) {
      console.log(error)
    }
  };

  async checkVersion() {
    try {
      if (this.props.trainee.traineeDetails.alert != ""  && !isEmpty(this.props.trainee.traineeDetails.alert) ) {
        this.alertServiceCall();
      }else if (this.props.trainee.traineeDetails.app_version != "" && !isEmpty(this.props.trainee.traineeDetails.app_version) ) {
        LogUtils.infoLog1('this.state.versionResObj', this.state.versionResObj);
        if (this.state.versionResObj.is_optional === 1) {
          this.setState({
            isAppUpdatePop: true,
            isAppUpdateTit: this.state.versionResObj.vrsn_title,
            isAppUpdateMsg: this.state.versionResObj.vrsn_text,
            isAppCancel: '',
            isAppUpdate: 'UPDATE',
          });
        }
        else if (this.state.versionResObj.is_optional === 2) {
          this.setState({
            isAppUpdatePop: true,
            isAppUpdateTit: this.state.versionResObj.vrsn_title,
            isAppUpdateMsg: this.state.versionResObj.vrsn_text,
            isAppCancel: 'CANCEL',
            isAppUpdate: 'UPDATE',
          });
        } 
        // else {
        //   setTimeout(() => {
        //     this.alertServiceCall();
        //   }, 100);
        // }
      } 
      // else {
      //   setTimeout(() => {
      //     this.alertServiceCall();
      //   }, 100);
      // }

    } catch (error) {
      console.log("Error occured at check version , Error : ",error);
    }
  }

  
  //Alert
  async alertServiceCall() {
    try {
      if (this.props.trainee.traineeDetails.alert != "" && !isEmpty(this.props.trainee.traineeDetails.alert)) {
        LogUtils.infoLog1('data.alert.img_list', this.props.trainee.traineeDetails.alert);
        if (this.state.alertobj.navigate_to === "") {
          LogUtils.infoLog("EMPTY");
          this.setState({
            isShowPopup: true,
            isPopupImage: this.state.alertobj.img_url,
            isPopupCancel: 'Close',
            isPopupOk: 'Ok.Got it',
            web_url: '',
            isPopupNavigate: '',
            isPopupNavigateId: 0,
            popupId: this.state.alertobj.id,
            poppupImageArr: this.state.alertobj.img_list,
            thumbnailUrl: this.state.alertobj.thumbnail_url,
            mediaType: this.state.alertobj.media_type,
          });

        } else {
          this.setState({
            isShowPopup: true,
            isPopupImage: this.state.alertobj.img_url,
            web_url: this.state.alertobj.web_url,
            isPopupCancel: 'Close',
            isPopupOk: 'Open',
            isPopupNavigate: this.state.alertobj.navigate_to,
            isPopupNavigateId: this.state.alertobj.navigate_id,
            popupId: this.state.alertobj.id,
            poppupImageArr: this.state.alertobj.img_list,
            thumbnailUrl: this.state.alertobj.thumbnail_url,
            mediaType: this.state.alertobj.media_type,
          });
        }
      } else {
        this.setState({
          isShowPopup: false,
        })
      }

    } catch (error) {
      console.log(error)
    }
  }

  //Alert Update
  async alertServiceUpdate() {
    try {
      let token = await AsyncStorage.getItem('token');
      LogUtils.infoLog(JSON.stringify({
        id: this.state.popupId,
        opened: this.state.isPopupOpened,
      }));

      fetch(
        `${BASE_URL}/trainee/alertlog`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            id: this.state.popupId,
            opened: this.state.isPopupOpened,
          }),

        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('Alert log statusCode', statusCode);
          LogUtils.infoLog1('Alert log data', data);

          if (statusCode >= 200 && statusCode <= 300) {

          } else {

          }
        })
        .catch(function (error) {
        });
    } catch (error) {
      console.log(error)
    }
  }

  //Create notifiication
  async createNotificationListeners() {
    try {
      /*
      * Triggered when a particular notification has been received in foreground
      * */
      this.notificationListener = firebase.notifications().onNotification((notification) => {

        if (Platform.OS === 'ios') {
          bigImage = notification._data.fcm_options.image;
        } else {
          bigImage = notification._android._largeIcon;
        }
        const { title, body, data, notificationId } = notification;
        LogUtils.infoLog('ForeGround');
        LogUtils.infoLog1('Notification', notification);
        if (title !== undefined && body !== undefined && bigImage !== undefined) {
          firebase.notifications().displayNotification(this.buildNotification(title, body, bigImage, notification._data));
        } else if ((title !== undefined && body !== undefined) && bigImage === undefined) {
          firebase.notifications().displayNotification(this.buildNotification(title, body, "", notification._data));
        } else if ((title !== undefined && bigImage !== undefined) && body === undefined) {
          firebase.notifications().displayNotification(this.buildNotification(title, "", bigImage, notification._data));
        } else if (body === undefined && bigImage === undefined) {
          firebase.notifications().displayNotification(this.buildNotification(title, "", "", notification._data));
        } else if (title === undefined && bigImage === undefined) {
          firebase.notifications().displayNotification(this.buildNotification("", body, "", notification._data));
        } else if ((body !== undefined && bigImage !== undefined) && title === undefined) {
          firebase.notifications().displayNotification(this.buildNotification("", body, bigImage, notification._data));
        }

        if (data !== undefined && data.type !== undefined && data.key !== undefined) {
          type = data.type;
          typeid = data.id;
          if (data.uc_id) {
            uc_id = data.uc_id;
          }
        }
        LogUtils.infoLog1('DB NotoficationId', notificationId)
        // if (notificationId && title !== undefined) {
        //   this.saveNotificationToDB(notificationId, title, body, bigImage, type, typeid, data.key);
        // }
      });

      /*
      * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
      * */
      this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {

        if (notificationOpen) {
          const { title, body, data } = notificationOpen;
          if (notific_id === null || notific_id === '') {
            if (type) {
              this.storeNotiId('100');
              if (type === 'p') {
                if (typeid && typeid > 0) {
                  //Program Details Page
                  Actions.traineePlanNew({ pId: typeid });
                } else {
                  //Program Home Page
                  Actions.traineeWorkoutsHome();
                }
              } else if (type === 'w') {
                type = '';
                if (typeid && typeid > 0) {
                } else {
                  // Water Details Page
                  Actions.traWaterDet();
                }
              } else if (type === 'dl') {
                if (typeid && typeid > 0) {
                } else {
                  // Food Home Page
                  Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
                }
              } else if (type === 's') {
                if (typeid && typeid > 0) {
                } else {
                  // Steps Home Page
                  Actions.traWalkDet()
                }
              } else if (type === 'a') {
                if (typeid && typeid > 0) {
                } else {
                  // Appointments Home Page
                  Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
                }
              } else if (type === 'c') {
                if (typeid && typeid > 0) {
                  Actions.traChallenges({ cid: typeid });
                  // Actions.accChallenge({ cid: typeid });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (type === 'cvu') {
                if (typeid && typeid > 0) {
                  Actions.traChallenges({ cid: typeid });
                  // Actions.accChallenge({ cid: typeid, uc_id: uc_id });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (type === 'f') {
                if (typeid && typeid > 0) {
                  // Feed Details Page
                  Actions.feeddetails({ feedId: typeid });
                } else {
                  // Feed Page
                  Actions.wall();
                }
              } else if (type === 'cwd') {
                if (typeid && typeid > 0) {
                  Actions.traChallenges({ cid: typeid });
                  // Actions.accChallenge({ cid: typeid, flag: 'Leaderboard' });
                  // Actions.traineechallengWinners({ cid: typeid });
                } else {
                  Actions.traChallenges();
                }
              } else if (type === 'dr') {
                if (typeid && typeid > 0) {
                  Actions.dietHome1();
                } else {
                  Actions.dietHome1();
                }
              } else if (type === 'onc') {
                if (typeid && typeid > 0) {
                  // Actions.oneononechat({trainerId : typeid});
                  Actions.chatlist();
                } else {
                  Actions.chatlist();
                }
              }
            }
            else if (notificationOpen.notification._data.type) {
              this.storeNotiId(notificationOpen.notification._notificationId);
              if (notificationOpen.notification._data.type === 'p') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  //Program Details Page
                  Actions.traineePlanNew({ pId: notificationOpen.notification._data.id });
                } else {
                  //Program Home Page
                  Actions.traineeWorkoutsHome();
                }

              } else if (notificationOpen.notification._data.type === 'w') {
                notificationOpen.notification._data.type = '';
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Water Details Page
                  Actions.traWaterDet();
                }
              } else if (notificationOpen.notification._data.type === 'dl') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Food Home Page
                  Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
                }
              } else if (notificationOpen.notification._data.type === 's') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Steps Home Page
                  Actions.traWalkDet()
                }
              } else if (notificationOpen.notification._data.type === 'a') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Appointments Home Page
                  Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
                }
              } else if (notificationOpen.notification._data.type === 'c') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id });

                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (notificationOpen.notification._data.type === 'cvu') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id, uc_id: notificationOpen.notification._data.uc_id });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (notificationOpen.notification._data.type === 'f') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Feed Details Page
                  Actions.feeddetails({ feedId: notificationOpen.notification._data.id });
                } else {
                  // Feed Page
                  Actions.wall();
                }
              } else if (notificationOpen.notification._data.type === 'cwd') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id, flag: 'Leaderboard' });
                } else {

                }
              } else if (notificationOpen.notification._data.type === 'dr') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  Actions.dietHome1();
                }
              }
              else if (notificationOpen.notification._data.type === 'onc') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Actions.oneononechat({trainerId : notificationOpen.notification._data.id});
                  Actions.chatlist();
                } else {
                  Actions.chatlist();
                }
              }
            }
          } else if (notific_id != notificationOpen.notification._notificationId) {
            if (type) {
              this.storeNotiId(notificationOpen.notification._notificationId);
              if (type === 'p') {
                if (typeid && typeid > 0) {
                  //Program Details Page
                  Actions.traineePlanNew({ pId: typeid });

                } else {
                  //Program Home Page
                  Actions.traineeWorkoutsHome();
                }
              } else if (type === 'w') {
                type = '';
                if (typeid && typeid > 0) {

                } else {
                  // Water Details Page
                  Actions.traWaterDet();
                }
              } else if (type === 'dl') {
                if (typeid && typeid > 0) {

                } else {
                  // Food Home Page
                  Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
                }
              } else if (type === 's') {
                if (typeid && typeid > 0) {

                } else {
                  // Steps Home Page
                  Actions.traWalkDet()
                }
              } else if (type === 'a') {
                if (typeid && typeid > 0) {

                } else {
                  // Appointments Home Page
                  Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
                }
              } else if (type === 'c') {
                if (typeid && typeid > 0) {
                  // Actions.accChallenge({ cid: typeid });
                  Actions.traChallenges({ cid: typeid });

                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (type === 'cvu') {
                if (typeid && typeid > 0) {
                  // Actions.accChallenge({ cid: typeid, uc_id: uc_id });
                  Actions.traChallenges({ cid: typeid });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (type === 'f') {
                if (typeid && typeid > 0) {
                  // Feed Details Page
                  Actions.feeddetails({ feedId: typeid });
                } else {
                  // Feed Page
                  Actions.wall();
                }
              } else if (type === 'cwd') {
                if (typeid && typeid > 0) {
                  // Actions.accChallenge({ cid: typeid, flag: 'Leaderboard' });
                  Actions.traChallenges({ cid: typeid });

                } else {

                }
              } else if (type === 'dr') {
                if (typeid && typeid > 0) {

                } else {
                  Actions.dietHome1();
                }
              } else if (type === 'onc') {
                if (typeid && typeid > 0) {
                  // Actions.oneononechat({trainerId : typeid});
                  Actions.chatlist();
                } else {
                  Actions.chatlist();
                }
              }
            }
            else if (notificationOpen.notification._data.type) {
              this.storeNotiId(notificationOpen.notification._notificationId);
              if (notificationOpen.notification._data.type === 'p') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  //Program Details Page
                  Actions.traineePlanNew({ pId: notificationOpen.notification._data.id });
                } else {
                  //Program Home Page
                  Actions.traineeWorkoutsHome();
                }
              } else if (notificationOpen.notification._data.type === 'w') {
                notificationOpen.notification._data.type = '';
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Water Details Page
                  Actions.traWaterDet();
                }
              } else if (notificationOpen.notification._data.type === 'dl') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  // Food Home Page
                  Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
                }
              } else if (notificationOpen.notification._data.type === 's') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                } else {
                  // Steps Home Page
                  Actions.traWalkDet()
                }
              } else if (notificationOpen.notification._data.type === 'a') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                } else {
                  // Appointments Home Page
                  Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
                }
              } else if (notificationOpen.notification._data.type === 'c') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (notificationOpen.notification._data.type === 'cvu') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id, uc_id: notificationOpen.notification._data.uc_id });
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                } else {
                  //Challenge Home Page
                  Actions.traChallenges()
                }
              } else if (notificationOpen.notification._data.type === 'f') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Feed Details Page
                  Actions.feeddetails({ feedId: notificationOpen.notification._data.id });

                } else {
                  // Feed Page
                  Actions.wall();
                }
              } else if (notificationOpen.notification._data.type === 'cwd') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Actions.accChallenge({ cid: notificationOpen.notification._data.id, flag: 'Leaderboard' });
                  Actions.traChallenges({ cid: notificationOpen.notification._data.id });
                } else {
                }
              } else if (notificationOpen.notification._data.type === 'dr') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

                } else {
                  Actions.dietHome1();
                }
              } else if (notificationOpen.notification._data.type === 'onc') {
                if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                  // Actions.oneononechat({trainerId : notificationOpen.notification._data.id});
                  Actions.chatlist();
                } else {
                  Actions.chatlist();
                }
              }
            }
          }
          firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
        }
      });

      /*
      * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
      * */
      const notificationOpen = await firebase.notifications().getInitialNotification();

      if (notificationOpen) {
        // LogUtils.infoLog1('notificationOpen', notificationOpen);
        // LogUtils.infoLog1('notificationOpen.notification', notificationOpen.notification);
        // LogUtils.infoLog1('App Closed');
        const { title, body, image, id, key } = notificationOpen.notification._data;
        // if (notificationOpen.notification._notificationId && title !== undefined) {
        //   this.saveNotificationToDB(notificationOpen.notification._notificationId, title, body, image, notificationOpen.notification._data.type, id, key);
        // }
        // LogUtils.infoLog1('type', notificationOpen.notification._data.type);
        // LogUtils.infoLog1('Type', type);
        // LogUtils.infoLog1('NotificationId', notificationOpen.notification._notificationId);
        // LogUtils.infoLog1('notific_id', notific_id);
        if (notific_id === null || notific_id === '') {

          if (type) {
            this.storeNotiId('100');
            if (type === 'p') {
              if (typeid && typeid > 0) {
                //Program Details Page
                Actions.traineePlanNew({ pId: typeid });
              } else {
                //Program Home Page
                Actions.traineeWorkoutsHome();
              }
            } else if (type === 'w') {
              type = '';
              if (typeid && typeid > 0) {

              } else {
                // Water Details Page
                Actions.traWaterDet();
              }
            } else if (type === 'dl') {
              if (typeid && typeid > 0) {

              } else {
                // Food Home Page
                Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
              }
            } else if (type === 's') {
              if (typeid && typeid > 0) {

              } else {
                // Steps Home Page
                Actions.traWalkDet()
              }

            } else if (type === 'a') {
              if (typeid && typeid > 0) {

              } else {
                // Appointments Home Page
                Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
              }
            } else if (type === 'c') {
              if (typeid && typeid > 0) {
                // Actions.accChallenge({ cid: typeid });
                Actions.traChallenges({ cid: typeid });

              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }

            } else if (type === 'cvu') {
              if (typeid && typeid > 0) {
                Actions.traChallenges({ cid: typeid });
                // Actions.accChallenge({ cid: typeid, uc_id: uc_id });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (type === 'f') {
              if (typeid && typeid > 0) {
                // Feed Details Page
                Actions.feeddetails({ feedId: typeid });
              } else {
                // Feed Page
                Actions.wall();
              }
            } else if (type === 'cwd') {
              if (typeid && typeid > 0) {
                // Actions.accChallenge({ cid: typeid, flag: 'Leaderboard' });
                Actions.traChallenges({ cid: typeid });

              } else {

              }
            } else if (type === 'dr') {
              if (typeid && typeid > 0) {


              } else {
                Actions.dietHome1();
              }
            } else if (type === 'onc') {
              if (typeid && typeid > 0) {
                // Actions.oneononechat({trainerId : typeid});
                Actions.chatlist();
              } else {
                Actions.chatlist();
              }
            }

          }
          else if (notificationOpen.notification._data.type) {
            this.storeNotiId(notificationOpen.notification._notificationId);
            if (notificationOpen.notification._data.type === 'p') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                //Program Details Page
                Actions.traineePlanNew({ pId: notificationOpen.notification._data.id });
              } else {
                //Program Home Page
                Actions.traineeWorkoutsHome();
              }
            } else if (notificationOpen.notification._data.type === 'w') {
              notificationOpen.notification._data.type = '';
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Water Details Page
                Actions.traWaterDet();
              }
            } else if (notificationOpen.notification._data.type === 'dl') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Food Home Page
                Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
              }
            } else if (notificationOpen.notification._data.type === 's') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Steps Home Page
                Actions.traWalkDet()
              }
            } else if (notificationOpen.notification._data.type === 'a') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Appointments Home Page
                Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
              }

            } else if (notificationOpen.notification._data.type === 'c') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });

              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (notificationOpen.notification._data.type === 'cvu') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id, uc_id: notificationOpen.notification._data.uc_id });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (notificationOpen.notification._data.type === 'f') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Feed Details Page
                Actions.feeddetails({ feedId: notificationOpen.notification._data.id });
              } else {
                // Feed Page
                Actions.wall();
              }
            } else if (notificationOpen.notification._data.type === 'cwd') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id, flag: 'Leaderboard' });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });

              } else {
              }
            } else if (notificationOpen.notification._data.type === 'dr') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {


              } else {
                Actions.dietHome1();
              }
            } else if (notificationOpen.notification._data.type === 'onc') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.oneononechat({trainerId : notificationOpen.notification._data.id});
                Actions.chatlist();
              } else {
                Actions.chatlist();
              }
            }
          }

        } else if (notific_id != notificationOpen.notification._notificationId) {

          if (type) {
            this.storeNotiId(notificationOpen.notification._notificationId);
            if (type === 'p') {
              if (typeid && typeid > 0) {
                //Program Details Page
                Actions.traineePlanNew({ pId: typeid });
              } else {
                //Program Home Page
                Actions.traineeWorkoutsHome();
              }

            } else if (type === 'w') {
              type = '';
              if (typeid && typeid > 0) {

              } else {
                // Water Details Page
                Actions.traWaterDet();
              }
            } else if (type === 'dl') {
              if (typeid && typeid > 0) {

              } else {
                // Food Home Page
                Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
              }
            } else if (type === 's') {
              if (typeid && typeid > 0) {

              } else {
                // Steps Home Page
                Actions.traWalkDet()
              }
            } else if (type === 'a') {
              if (typeid && typeid > 0) {

              } else {
                // Appointments Home Page
                Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
              }
            } else if (type === 'c') {
              if (typeid && typeid > 0) {
                // Actions.accChallenge({ cid: typeid });
                Actions.traChallenges({ cid: typeid });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (type === 'cvu') {
              if (typeid && typeid > 0) {
                // Actions.accChallenge({ cid: typeid, uc_id: uc_id });
                Actions.traChallenges({ cid: typeid });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (type === 'f') {
              if (typeid && typeid > 0) {
                // Feed Details Page
                Actions.feeddetails({ feedId: typeid });
              } else {
                // Feed Page
                Actions.wall();
              }
            } else if (type === 'cwd') {
              if (typeid && typeid > 0) {
                // Actions.accChallenge({ cid: typeid, flag: 'Leaderboard' });
                Actions.traChallenges({ cid: typeid });
              } else {

              }
            } else if (type === 'dr') {
              if (typeid && typeid > 0) {


              } else {
                Actions.dietHome1();
              }
            } else if (type === 'onc') {
              if (typeid && typeid > 0) {
                // Actions.oneononechat({trainerId : typeid});
                Actions.chatlist();
              } else {
                Actions.chatlist();
              }
            }
          }
          else if (notificationOpen.notification._data.type) {
            this.storeNotiId(notificationOpen.notification._notificationId);
            if (notificationOpen.notification._data.type === 'p') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                //Program Details Page
                Actions.traineePlanNew({ pId: notificationOpen.notification._data.id });
              } else {
                //Program Home Page
                Actions.traineeWorkoutsHome();
              }

            } else if (notificationOpen.notification._data.type === 'w') {
              notificationOpen.notification._data.type = '';
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Water Details Page
                Actions.traWaterDet();
              }

            } else if (notificationOpen.notification._data.type === 'dl') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Food Home Page
                Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
              }
            } else if (notificationOpen.notification._data.type === 's') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Steps Home Page
                Actions.traWalkDet()
              }
            } else if (notificationOpen.notification._data.type === 'a') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {

              } else {
                // Appointments Home Page
                Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
              }
            } else if (notificationOpen.notification._data.type === 'c') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (notificationOpen.notification._data.type === 'cvu') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id, uc_id: notificationOpen.notification._data.uc_id });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });
              } else {
                //Challenge Home Page
                Actions.traChallenges()
              }
            } else if (notificationOpen.notification._data.type === 'f') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Feed Details Page
                Actions.feeddetails({ feedId: notificationOpen.notification._data.id });
              } else {
                // Feed Page
                Actions.wall();
              }
            } else if (notificationOpen.notification._data.type === 'cwd') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.accChallenge({ cid: notificationOpen.notification._data.id, flag: 'Leaderboard' });
                Actions.traChallenges({ cid: notificationOpen.notification._data.id });
              } else {

              }
            } else if (notificationOpen.notification._data.type === 'dr') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
              } else {
                Actions.dietHome1();
              }
            } else if (notificationOpen.notification._data.type === 'onc') {
              if (notificationOpen.notification._data.id && notificationOpen.notification._data.id > 0) {
                // Actions.oneononechat({trainerId : notificationOpen.notification._data.id});
                Actions.chatlist();
              } else {
                Actions.chatlist();
              }
            }
          }
        }
        firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
      }
      /*
      * Triggered for data only payload in foreground
      * */
      this.messageListener = firebase.messaging().onMessage((message) => {
        //process data message
        // LogUtils.infoLog('data only payload in foreground');
        // LogUtils.infoLog(message);
        const { title, body, image, id } = message._data;
        const navtype = message._data.type;
        // LogUtils.infoLog1("navtype", navtype);
        // if (message) {
        if (title != '' && body != '' && image != '') {
          firebase.notifications().displayNotification(this.buildNotification(title, body, image, message._data));
        } else if ((title != '' && body != '') && image === '') {
          firebase.notifications().displayNotification(this.buildNotification(title, body, "", message._data));
        } else if ((title != '' && image != '') && body === '') {
          firebase.notifications().displayNotification(this.buildNotification(title, "", image, message._data));
        } else if (body === '' && image === '') {
          firebase.notifications().displayNotification(this.buildNotification(title, "", "", message._data));
        }
        if (navtype !== undefined && id !== undefined) {
          type = navtype;
          typeid = id;
        }
        this.storeNotiId('100');

        LogUtils.infoLog1('DB NotoficationId', message._messageId);
        // if (message._messageId) {
        //   this.saveNotificationToDB(message._messageId, title, body, image, type, typeid);
        // }

        // }
      });
    } catch (error) {
      console.log(error)
    }
  }

  buildNotification = (title, body, bigImage, data) => {
    try {
      if (bigImage) {
        const notification = new firebase.notifications.Notification({
          sound: 'default',
          show_in_foreground: true,
        })
          .setNotificationId("1") // Any random ID
          .setTitle(title) // Title of the notification
          .setBody(body) // body of notification
          .setSound("500")
          // .android.setSmallIcon('../res/ic_profile_gray.png')
          .android.setBigText(body)
          .android.setLargeIcon(bigImage)
          .android.setBigPicture(bigImage)
          .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
          .android.setChannelId("reminder") // should be the same when creating channel for Android
          .android.setAutoCancel(true); // To remove notification when tapped on it
        return notification;
      } else {
        const notification = new firebase.notifications.Notification()
          .setNotificationId("1") // Any random ID
          .setTitle(title) // Title of the notification
          .setBody(body) // body of notification
          .setSound("500")
          // .android.setSmallIcon('../res/ic_profile_gray.png')
          .android.setBigText(body)
          .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
          .android.setChannelId("reminder") // should be the same when creating channel for Android
          .android.setAutoCancel(true); // To remove notification when tapped on it
        return notification;
      }
      //return notification;
    } catch (error) {
      console.log(error)
    }
  };

  async storeNotiId(notiId) {
    await AsyncStorage.setItem('notificationID', notiId);
    notific_id = await AsyncStorage.getItem('notificationID');
    LogUtils.infoLog1("NotificationId", await AsyncStorage.getItem('notificationID'));
  }

  async appleAuth() {
    try {
      AppleHealthKit.isAvailable((err, available) => {
        if (available) {
          AppleHealthKit.initHealthKit(HKOPTIONS, (err, results) => {
            if (err) {
              //console.log("error initializing Healthkit: ", err);
              return;
            }
            stepsArray = [];
            caloriesArray = [];
            this._fetchStepsToday_Apple();
            // this._fetchCalories_Apple();
          });
        }
      });
    } catch (error) {
      console.log(error)
    }
  }

  _fetchStepsToday_Apple() {
    try {
      const options = {
        startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
        endDate: new Date().toISOString(), // required ISO8601Timestamp
        // type: 'Workout',
      }

      // AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
      AppleHealthKit.getDailyStepCountSamples(options, (err, res) => {

        if (err) {
          LogUtils.infoLog(err);
          return;
        }
        // LogUtils.infoLog(res);

        if (res && Array.isArray(res) && res.length) {
          LogUtils.infoLog1("Health res steps data", res);
          let dummyStepsArray = [];

          res.map((item) => {
            dummyStepsArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': item.value, 'cals': 0, 'dist': 0 });
          });

          if (Array.isArray(dummyStepsArray) && dummyStepsArray.length) {
            let cDate = '';
            let stepCount = 0;
            let prevDate = '';
            for (let i = 0; i < dummyStepsArray.length; ++i) {
              prevDate = dummyStepsArray[i].from;

              if (cDate != '' && cDate != prevDate) {
                // LogUtils.infoLog1("CDate", cDate);
                // LogUtils.infoLog1("to", dummyStepsArray[i].to);
                stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });
                cDate = '';
                stepCount = 0;
              }
              cDate = prevDate;
              prevDate = '';
              stepCount = stepCount + dummyStepsArray[i].steps;
              if (i === dummyStepsArray.length - 1) {
                stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });
              }
            }
          }
        }
        this._fetchDistance_Apple(stepsArray);
      });
    } catch (error) {
      console.log(error)
    }
  }

  _fetchDistance_Apple(data) {
    try {
      const options = {
        startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
        endDate: new Date().toISOString(), // required ISO8601Timestamp
        // type: 'Workout',
      }

      AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
        if (err) {
          LogUtils.infoLog(err);
          return;
        }
        // LogUtils.infoLog(res);
        if (res && Array.isArray(res) && res.length) {
          let dummyDistanceArray = [];
          res.map((item) => {
            dummyDistanceArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': 0, 'dist': item.value });
            // dummyDistanceArray.push(item);
          });

          if (Array.isArray(dummyDistanceArray) && dummyDistanceArray.length) {
            let cDate = '';
            let distance = 0;
            let prevDate = '';
            for (let i = 0; i < dummyDistanceArray.length; ++i) {
              prevDate = dummyDistanceArray[i].from;

              if (cDate != '' && cDate != prevDate) {
                // LogUtils.infoLog1("CDate", cDate);
                // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });
                cDate = '';
                distance = 0;
              }
              cDate = prevDate;
              prevDate = '';
              distance = distance + dummyDistanceArray[i].dist;
              if (i === dummyDistanceArray.length - 1) {
                distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });
              }
            }
          }

          data.map((dataItem) => {
            distanceArray.map((item) => {
              if (dataItem.from === item.from) {
                dataItem.dist = item.dist
              }
            })
          })
          this._fetchCalories_Apple(data);
        }
      });
    } catch (error) {
      console.log(error)
    }
  }

  _fetchCalories_Apple(data) {
    try {
      const opt = {
        // startDate: "2020-03-01T00:00:17.971Z", // required
        startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T00:00:17.971Z', // required ISO8601Timestamp
        endDate: new Date().toISOString(), // required
        // type: 'walking',

      };
      AppleHealthKit.getActiveEnergyBurned(opt, (err, res) => {
        if (err) {
          LogUtils.infoLog(err);
          return;
        }

        if (res && Array.isArray(res) && res.length) {
          LogUtils.infoLog1("Health res calories data", res);
          let dummyCaloriesArray = [];

          res.map((item) => {
            if (!item.sourceName.includes('Health')) {
              dummyCaloriesArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': item.value, 'dist': 0 });
            }
          });

          if (Array.isArray(dummyCaloriesArray) && dummyCaloriesArray.length) {
            let cDate = '';
            let calories = 0;
            let prevDate = '';
            for (let i = 0; i < dummyCaloriesArray.length; ++i) {
              prevDate = dummyCaloriesArray[i].from;

              if (cDate != '' && cDate != prevDate) {
                // LogUtils.infoLog1("CDate", cDate);
                // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });

                cDate = '';
                calories = 0;

              }
              cDate = prevDate;
              prevDate = '';
              calories = calories + dummyCaloriesArray[i].cals;
              if (i === dummyCaloriesArray.length - 1) {
                caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });
              }
            }
          }
          data.map((dataItem) => {
            caloriesArray.map((item) => {
              if (dataItem.from === item.from) {
                dataItem.cals = item.cals
              }
            })
          })
        }
        // LogUtils.infoLog1("Health data", data);
        this._fetchApple_Workouts(data);
      });
    } catch (error) {
      console.log(error)
    }
  }

  _fetchApple_Workouts(data) {
    try {
      const options = {
        startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
        endDate: new Date().toISOString(), // required ISO8601Timestamp
        type: 'Workout',
      }

      AppleHealthKit.getSamples(options, (err, res) => {

        if (err) {
          LogUtils.infoLog1(err);
          return;
        }

        if (res && Array.isArray(res) && res.length) {
          LogUtils.infoLog1("Health res workout data", res);
          res.map((item) => {
            if (item.tracked === true) {
              data.push({ 'act': item.activityName, 'from': item.start.split('T')[0] + ' ' + item.start.split('T')[1].split('.')[0], 'to': item.end.split('T')[0] + ' ' + item.end.split('T')[1].split('.')[0], 'steps': 0, 'cals': item.calories, 'dist': item.distance });
            }
          });
        }
        LogUtils.infoLog1("Health Final data", data);
        let body = JSON.stringify({
          td_id: 3,
          td_unq_id: '',
          fitbit_token: '',
          act_arr: data,
        });
        saveuseractivitytran(body);
      });
    } catch (error) {
      console.log(error)
    }
  }

  async getHomepageDet() {
    try {

      this.setState({
        showAppointmentFeedback: false,
        isRewardComplete: false,
        isShowPopup: false,
        freeAppointmentPopup: false,
      });

      this.setState({ loading: true });
      let token = await AsyncStorage.getItem('token');
      let version = DeviceInfo.getVersion();
      let params;

      if (Platform.OS === 'android') {
        params = {
          os_id: 1,
          curnt_vrsn: version,
          vrsn_info: VINFO_ANDROID,
        };
      }
      else if (Platform.OS === 'ios') {
        params = {
          os_id: 2,
          curnt_vrsn: version,
          vrsn_info: VINFO_IOS,
        };
      }

      LogUtils.infoLog(`${BASE_URL}/trainee/homedet`);
      await this.props.getHomepageDetAction(token,params).then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data) {
            this.setState({
              loading: false,
              refreshing: false,
              resObj: this.props.trainee.traineeDetails,
              covid_imp: this.props.trainee.traineeDetails.covid_imp,
              covid_diet_sent: this.props.trainee.traineeDetails.covid_diet_sent,
              email: this.props.trainee.traineeDetails.email,
              alertobj:this.props.trainee.traineeDetails.alert,
              versionResObj:this.props.trainee.traineeDetails.app_version
            });

            this.getHomepTrendingWorkouts();
            this.getProducts();

            if (this.state.resObj) {

              this.savePlanCount(this.state.resObj.plan_cnt);
              this.saveFileSizeLimitations();
              this.setState({ isShowDiet: this.state.resObj.show_diet, isShowAppt: this.state.resObj.show_appt, isShowFreeTrail: this.state.resObj.show_free_trial });

              var myloop = [];
              for (let i = 0; i < 8; i++) {
                if (i < this.state.resObj.glass_drink) {
                  myloop.push({ id: i + 1, isFill: true });
                }
                else {
                  myloop.push({ id: i + 1, isFill: false });
                }
              }

              this.setState({ glsArray: myloop })

              if (this.state.resObj.diet_name) {
                this.setState({ dietItems: this.state.resObj.rcmd_food })
              }

              if (this.state.resObj.food) {
                this.setState({ slideHeadText: this.state.resObj.food[0].date_name });
              }

              if (this.state.resObj.workout.length === 0) {
                this.setState({ noDataWorkouts: `Your haven't enrolled in any workout program` });
              }

              if (this.state.resObj.challenge.length === 0) {
                this.setState({ noDataChallenges: `You haven't accepted any challenges yet` });
              }

              if (this.state.first_time_home === '1') {
                if (this.state.resObj.consult_rating) {
                  if (!isEmpty(this.state.resObj.consult_rating)) {
                    this.setState({ showAppointmentFeedback: true, feedbackTitle: this.state.resObj.consult_rating.title, feedbackDesc: this.state.resObj.consult_rating.desc });
                  }
                }
                else if (this.state.resObj.ftr_code != "" && this.state.resObj.ftr_code) {
                  //this.bottomMultiPopup.open();
                } else {
                  //this.alertServiceCall();
                }

              } else {
                setTimeout(() => {
                  this.alertServiceCall();
                  this.saveFirstTimeHome();
                }, 100);
              }
            }
          }
          else {
            this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: 'No data available' });
          }
        } else {
          if (data.message === 'You are not authenticated!' || statusCode === 420) {
            this.setState({ isLogoutApp: true, loading: false, refreshing: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert', alertobj:{},versionResObj:"" });
          } else {
            this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert',alertobj:{},versionResObj:"" });
          }
        }
      }).catch(function (error) {
        this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert',alertobj:{} });
      });
    } catch (error) {
      console.log("Error in getHomepageDet : ",error);
    }
  }

  //used to get Trending workouts
  async getHomepTrendingWorkouts() {
    try {
      let token = await AsyncStorage.getItem('token');
      await this.props.getHomepTrendingWorkoutsAction(token).then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('Code', statusCode);
        LogUtils.infoLog1('TrendingWorkouts ', data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, arrTrendingWorkouts: this.props.trainee.trendingWorkouts });
        } else {
          if (data.message === 'You are not authenticated!') {
          } else {
          }
        }
      }).catch(function (error) {
      });
    } catch (error) {
      console.log(error)
    }
  }

  //used to get Fitness products
  async getProducts() {
    try {
      this.setState({ loading: true });
      let token = await AsyncStorage.getItem('token');
      await this.props.getProductsAction(token).then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, products: this.props.trainee.eComProducrs });
        } else {
          if (data.message === 'You are not authenticated!') {
            // this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
          } else {
            // this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
          }
        }
      }).catch(error => {
        this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
      })
    } catch (error) {
      this.setState({ loading: false });
      console.log(error)
    }
  };

  async saveFirstTimeHome() {
    await AsyncStorage.setItem('first_time_home', '1');
    this.setState({
      first_time_home: '1',
    });
    LogUtils.infoLog1("first_time_home", await AsyncStorage.getItem('first_time_home'));
  }

  async saveShowcaseViewFlag() {
    await AsyncStorage.setItem('show_apptour', '1');
    //LogUtils.infoLog1("show_apptour", await AsyncStorage.getItem('show_apptour'));
  }

  async savePlanCount(planCount) {
    await AsyncStorage.setItem('plan_cnt', planCount.toString());
    LogUtils.infoLog1("plan_cnt", await AsyncStorage.getItem('plan_cnt'));
  }

  async saveFileSizeLimitations() {
    try {
      // file sizes
      await AsyncStorage.setItem('max_img_size', this.state.resObj.max_img_size);
      await AsyncStorage.setItem('max_pdf_size', this.state.resObj.max_pdf_size);
      await AsyncStorage.setItem('max_video_size', this.state.resObj.max_video_size);
      await AsyncStorage.setItem('shopping_url', this.state.resObj.shopping_url);

      // aws security keys
      await AsyncStorage.setItem('s3_bucket', this.state.resObj.s3_bucket);
      await AsyncStorage.setItem('s3_chlng_path', this.state.resObj.s3_chlng_path);
      await AsyncStorage.setItem('s3_feed_path', this.state.resObj.s3_feed_path);
      await AsyncStorage.setItem('s3_key', this.state.resObj.s3_key);
      await AsyncStorage.setItem('s3_region', this.state.resObj.s3_region);
      await AsyncStorage.setItem('s3_secret', this.state.resObj.s3_secret);
      await AsyncStorage.setItem('s3_userpics_path', this.state.resObj.s3_userpics_path);
      await AsyncStorage.setItem('s3_userfoodimg_path', this.state.resObj.s3_userfoodimg_path);

      //Storing free trial initial values
      this.setState({
        freeTrialInitialObj: {
          ftc_image: this.state.resObj.ftc_image,
          ftc_title: this.state.resObj.ftc_title,
          ftc_text: this.state.resObj.ftc_text,
          email_verified: this.state.resObj.email_verified,
          ev_title: this.state.resObj.ev_title,
          ev_descp: this.state.resObj.ev_descp,
          ftc_btn: this.state.resObj.ftc_btn,
          fcc_btn: this.state.resObj.fcc_btn,
          fcc_image: this.state.resObj.fcc_image,
          fcc_text: this.state.resObj.fcc_text,
          fcc_title: this.state.resObj.fcc_title,
          show_free_trial: this.state.resObj.show_free_trial,
          show_free_call: this.state.resObj.show_free_call,
          email: this.state.resObj.email,
          is_paiduser: this.state.resObj.is_paiduser,
          epf_text: this.state.resObj.epf_text,
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  async sendGoogleFitData() {
    try {
      const options = {
        scopes: [
          Scopes.FITNESS_ACTIVITY_READ,
          // Scopes.FITNESS_ACTIVITY_READ_WRITE,
          Scopes.FITNESS_BODY_READ_WRITE,
          Scopes.FITNESS_LOCATION_READ,
        ],
      }

      GoogleFit.authorize(options)
        .then((res) => {
          if (res.success) {
            let opt = {
              startDate: new Date(moment().subtract(7, 'days').format("YYYY-MM-DD")).valueOf(), // simply outputs the number of milliseconds since the Unix Epoch
              endDate: new Date().valueOf()
            };

            GoogleFit.getActivitySamples(opt, (err, res) => {
              LogUtils.infoLog1("Googlefit org data", res);

              if (res && Array.isArray(res) && res.length) {
                stepsArray = [];
                res.map((item) => {
                  if (item.tracked === true) {
                    var element = { 'act': item.activityName, 'from': moment.unix(item.start / 1000).format("YYYY-MM-DD HH:mm:ss"), 'to': moment.unix(item.end / 1000).format("YYYY-MM-DD HH:mm:ss"), 'steps': 0, 'cals': 0, 'dist': 0 };
                    if (item.quantity) {
                      element["steps"] = item.quantity;
                    } else {
                      element["steps"] = 0;
                    }

                    if (item.calories) {
                      element["cals"] = item.calories;
                    } else {
                      element["cals"] = 0;
                    }

                    if (item.distance) {
                      element["dist"] = item.distance;
                    } else {
                      element["dist"] = 0;
                    }

                    stepsArray.push(element);
                  }
                });

                LogUtils.infoLog1("Googlefit data", stepsArray);

                let body = JSON.stringify({
                  td_id: 2,
                  td_unq_id: '',
                  fitbit_token: '',
                  act_arr: stepsArray,
                });
                saveuseractivitytran(body);
              }
            });
          }
        })
        .catch((err) => {
          LogUtils.infoLog1('err >>> ', err)
        })
    } catch (error) {
      console.log(error)
    }
  }

  //Un used code region

  // async getRequest(url) {
  //   try {
  //     //this.setState({ loading: true });
  //     let token = await AsyncStorage.getItem('token');
  //     LogUtils.infoLog1(token, url);
  //     // Define the network metric
  //     const metric = await perf().newHttpMetric(url, 'GET').setResponsePayloadSize(null);
  //     await metric.start();

  //     // Perform a HTTP request and provide response information
  //     const response = await fetch(url, {
  //       method: 'GET',
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //         Authorization: `Bearer ${token}`,
  //       },
  //     });
  //     metric.setHttpResponseCode(response.status);
  //     metric.setResponseContentType(response.headers.get('Content-Type'));
  //     metric.setResponsePayloadSize(response.headers.get('Content-Length'));

  //     // Stop the metric
  //     await metric.stop();
  //     return response.json();
  //   } catch (error) {
  //     console.log(error)
  //   }
  // }

  // onWaterPressed() {
  //   LogUtils.firebaseEventLog('click', {
  //     p_id: 216,
  //     p_category: 'Home',
  //     p_name: 'Home->WaterHome',
  //   });
  //   Actions.traWaterDet();
  // }


  renderDietHeader() {
    if (!isEmpty(this.state.resObj)) {
      if (this.state.resObj.show_diet === 1) {
        if (this.state.resObj.is_diet_plan === 1) {
          // if (this.state.resObj.diet_name) {
          //   return (
          //     <View style={styles.vHeaderTitMore}>
          //       <Text style={styles.textPlan}>{'Diet Plan'.toUpperCase()}</Text>

          //       <TouchableOpacity
          //         onPress={() => this.onDietClicked()}>
          //         <Text style={styles.textAdd}>{'+ More'.toUpperCase()}</Text>
          //       </TouchableOpacity>
          //     </View>
          //   );
          // }
          // else {
          //   return (
          //     <View style={styles.vHeaderTitMore}>

          //       <Text style={styles.textPlan}>{'Diet Plan'.toUpperCase()}</Text>

          //       <TouchableOpacity
          //         onPress={() => this.onDietClicked()}>
          //         <Text style={styles.textAdd}>{'+ More'.toUpperCase()}</Text>
          //       </TouchableOpacity>
          //     </View>
          //   );
          // }
        }
        else {
          return (
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', width: wp('94%'), }}>
              <TouchableOpacity
                onPress={() => this.onDietClicked()}>
                {this.state.resObj.diet_banner_img
                  ? (
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={{ uri: this.state.resObj.diet_banner_img }}
                      style={{
                        width: wp('94%'),
                        height: undefined,
                        borderRadius: 6,
                        alignSelf: 'center',
                        resizeMode: 'cover',
                        aspectRatio: 2 / 1,
                      }}
                    />
                  )
                  : (
                    <Image
                      source={require('../res/ic_noimage.png')}
                      style={{
                        width: wp('94%'),
                        height: hp('25%'),
                        borderRadius: 6,
                        alignSelf: 'center',
                        resizeMode: 'cover',
                      }}
                    />
                  )
                }
              </TouchableOpacity>


              {/* <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan1}>
                  {'Diet Plan'.toUpperCase()}
                </Text>

                <View style={{ flex: 1 }}></View>
                <TouchableOpacity
                  onPress={() => this.onDietClicked()}>
                  <Text style={styles.textAdd}>{'+ More'.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
              <View style={{ flexDirection: 'row' }}>

                {this.state.resObj.diet_work_video
                  ?
                  (
                    <TouchableOpacity
                      activeOpacity={0.7}
                      onPress={() => {
                        if (Platform.OS === 'android') {
                          Actions.newvideoplay({ videoURL: this.state.resObj.diet_work_video, from: 'diet video', label: `End - Diet Video` });
                        } else {
                          Actions.afPlayer({ videoURL: this.state.resObj.diet_work_video, from: 'diet video', label: `End - Diet Video` });
                        }
                      }}>
                      <Text style={styles.textNodata}>
                        <Text style={styles.textNodata}>{this.state.resObj.diet_sub_msg_new}</Text>
                        <Text style={styles.textClickHere}>Click Here</Text>
                      </Text>
                    </TouchableOpacity>
                  )
                  :
                  (
                    <Text style={styles.textNodata}>{this.state.resObj.diet_sub_msg}</Text>
                  )
                }
              </View> */}
            </View>
          );
        }
      }
    }
  }

  renderDietPlan() {
    if (!isEmpty(this.state.resObj)) {
      if (this.state.resObj.show_diet === 1) {
        if (this.state.resObj.is_diet_plan === 1) {
          if (this.state.resObj.diet_name) {
            return (
              <View>
                <ScrollView
                  nestedScrollEnabled={true}
                  keyboardShouldPersistTaps='always'
                  contentContainerStyle={{ paddingBottom: hp('1%') }}
                  style={{}}>
                  <TouchableWithoutFeedback>
                    <ImageBackground source={require('../res/ic_diet_bg1.jpg')}
                      resizeMode='cover'
                      imageStyle={{ borderRadius: 6 }}
                      style={{
                        flex: 1,
                        width: '100%',
                        height: '100%',
                        borderRadius: 6,
                      }}>
                      <View style={styles.mainView}>

                        {this.renderPlanFoodList()}

                      </View>
                    </ImageBackground>
                  </TouchableWithoutFeedback>
                </ScrollView>
                {this.renderDietBottom()}
              </View>
            );

          } else {
            return (
              <View>
                {/* <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View> */}
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textNodata}>{this.state.resObj.diet_msg}</Text>
                </View>
              </View>
            );
          }
        }
      }
    }
  }

  renderDietBottom() {
    try {
      if (!isEmpty(this.state.resObj)) {
        // if (this.state.resObj.is_dietitian === 0) {
        return (
          <View style={styles.bottom}>

            <View source={require('../res/ic_home_bottom1.png')} style={styles.bottomHome1}>
              <View style={styles.bottomViewContainer1}>
                <TouchableOpacity
                  style={styles.bottomClick1}
                  onPress={() => this.setState({ mainOption: 3, foodType: 1, isSubListVisible: false, headerTitle: 'Plan - Breakfast' })}>
                  {this.state.foodType === 1
                    ? (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_breakfast_black.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText}>Breakfast</Text>
                      </View>
                    )
                    : (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_breakfast_gray.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText1}>Breakfast</Text>
                      </View>
                    )
                  }
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.bottomClick1}
                  onPress={() => this.setState({ mainOption: 3, foodType: 2, isSubListVisible: false, headerTitle: 'Plan - Lunch' })}>
                  {this.state.foodType === 2
                    ? (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_lunch_black.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText}>Lunch</Text>
                      </View>
                    )
                    : (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_lunch_gray.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText1}>Lunch</Text>
                      </View>
                    )
                  }
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.bottomClick1}
                  onPress={() => this.setState({ mainOption: 3, foodType: 3, isSubListVisible: false, headerTitle: 'Plan - Dinner' })}>
                  {this.state.foodType === 3
                    ? (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_dinner_black.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText}>Dinner</Text>
                      </View>
                    )
                    : (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_dinner_gray.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText1}>Dinner</Text>
                      </View>
                    )
                  }
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.bottomClick1}
                  onPress={() => {
                    this.setState({ mainOption: 3, foodType: 4, isSubListVisible: false, headerTitle: 'Plan - Snack' })
                  }}>
                  {this.state.foodType === 4
                    ? (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_snacks_black.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText}>Snack</Text>
                      </View>
                    )
                    : (
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_diet_snacks_gray.png')}
                          style={styles.imgBottomMenu1}
                        />
                        <Text style={styles.imgBottomMenuText1}>Snack</Text>
                      </View>
                    )
                  }
                </TouchableOpacity>
              </View>
            </View>
          </View>
        );
      }
    } catch (error) {
      console.log("Error in renderDietBottom", error);
    }
  }

  FlatListItemSeparatorCallenges = () => {
    return (
      <View style={{ height: 3, }}></View>
    );
  }

  renderPlanFoodList() {
    try {
      if (this.state.foodType === 1) {
        if (Array.isArray(this.state.dietItems.breakfast) && this.state.dietItems.breakfast.length) {
          return (
            <FlatList
              data={this.state.dietItems.breakfast}
              extraData={this.state}
              style={{}}
              // keyExtractor={item => item.c_id}
              keyExtractor={(item, index) => "breakfast" + index}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
              ItemSeparatorComponent={this.FlatListItemSeparatorCallenges}
              renderItem={({ item }) => {
                return <View style={{ flexDirection: 'column', }}>
                  <View style={styles.viewPrefFoodDet}>
                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', width: wp('80%'), }}>

                      {item.section
                        ?
                        (
                          <Text style={styles.txtDietSecName}>{`${item.section}`}</Text>
                        )
                        :
                        (
                          <View></View>
                        )}

                      <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <View style={styles.viewBlackDot}>
                        </View>
                        <Text style={styles.txtDietItemName}>{`${item.items}`}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              }} />
          );
        }
      }
      else if (this.state.foodType === 2) {
        if (Array.isArray(this.state.dietItems.lunch) && this.state.dietItems.lunch.length) {
          return (
            <FlatList
              data={this.state.dietItems.lunch}
              extraData={this.state}
              style={{}}
              // keyExtractor={item => item.c_id}
              keyExtractor={(item, index) => "lunch" + index}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
              ItemSeparatorComponent={this.FlatListItemSeparatorCallenges}
              renderItem={({ item }) => {
                return <View style={{ flexDirection: 'column', }}>
                  <View style={styles.viewPrefFoodDet}>
                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', width: wp('80%'), }}>

                      {item.section
                        ?
                        (
                          <Text style={styles.txtDietSecName}>{`${item.section}`}</Text>
                        )
                        :
                        (
                          <View></View>
                        )}

                      <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <View style={styles.viewBlackDot}>
                        </View>
                        <Text style={styles.txtDietItemName}>{`${item.items}`}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              }} />
          );
        }
      }
      else if (this.state.foodType === 3) {
        if (Array.isArray(this.state.dietItems.dinner) && this.state.dietItems.dinner.length) {
          return (
            <FlatList
              data={this.state.dietItems.dinner}
              extraData={this.state}
              style={{}}
              // keyExtractor={item => item.c_id}
              keyExtractor={(item, index) => "dinner" + index}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
              ItemSeparatorComponent={this.FlatListItemSeparatorCallenges}
              renderItem={({ item }) => {
                return <View style={{ flexDirection: 'column', }}>
                  <View style={styles.viewPrefFoodDet}>
                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', width: wp('80%'), }}>

                      {item.section
                        ?
                        (
                          <Text style={styles.txtDietSecName}>{`${item.section}`}</Text>
                        )
                        :
                        (
                          <View></View>
                        )}

                      <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <View style={styles.viewBlackDot}>
                        </View>
                        <Text style={styles.txtDietItemName}>{`${item.items}`}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              }} />
          );
        }
      }
      else if (this.state.foodType === 4) {
        if (Array.isArray(this.state.dietItems.snack) && this.state.dietItems.snack.length) {
          return (
            <FlatList
              data={this.state.dietItems.snack}
              extraData={this.state}
              style={{}}
              // keyExtractor={item => item.c_id}
              keyExtractor={(item, index) => "snack" + index}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
              ItemSeparatorComponent={this.FlatListItemSeparatorCallenges}
              renderItem={({ item }) => {
                return <View style={{ flexDirection: 'column', }}>
                  <View style={styles.viewPrefFoodDet}>
                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', width: wp('80%'), }}>
                      {item.section
                        ?
                        (
                          <Text style={styles.txtDietSecName}>{`${item.section}`}</Text>
                        )
                        :
                        (
                          <View></View>
                        )}

                      <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <View style={styles.viewBlackDot}>
                        </View>
                        <Text style={styles.txtDietItemName}>{`${item.items}`}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              }} />
          );
        }
      }
    } catch (error) {
      console.log("Error in renderEcommerceList", error);
    }
  }

  myStyle = function (dyLeft) {
    if (dyLeft) {
      var v1 = parseInt(dyLeft);
      var v2 = parseInt(this.state.resObj.max_steps);

      var val1 = parseFloat(v1 / v2);
      var val2 = parseFloat(val1 * 65);
      // LogUtils.infoLog1("Val 2: ", val2);

      var val3 = Math.round(val2);

      if (val3 > 64) {
        val3 = 64;
      }

      // LogUtils.infoLog1("Val 3: ", val3);

      return {
        width: 2,
        height: 30,
        alignSelf: 'center',
        position: 'absolute',
        left: 0,
        marginLeft: wp(`${val3}%`),

      }
    }
    else {
      return {
        width: 2,
        height: 30,
        alignSelf: 'center',
        position: 'absolute',
        marginLeft: wp('1%'),
        left: 0,
      }
    }
  }

  chooseImage = () => {
    let options = {
      title: 'Select Image',

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        LogUtils.infoLog1('response', JSON.stringify(response));
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileType: response.type,
        });
        this.validateImage(response.fileSize);
      }
    });
  };

  createFormData = (image, body) => {
    const data = new FormData();
    if (image) {
      data.append('file', {
        uri: image,
        name: image
          .toString()
          .replace(
            'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
            '',
          ),
        type: 'image/jpeg', // or photo.type
      });
    }
    data.append('data', body);
    return data;
  };

  bodyFlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0,
          width: 0,
          backgroundColor: "transparent",
        }}
      />
    );
  }

  renderBreakfatInnerList(innerArray) {
    try {
      let arrBreakfastInner = innerArray.map((item, j) => {
        return <View>
          <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
            <Text style={styles.textWorkName}>{`${item.section} - ${item.items}`}</Text>

          </View>
        </View>
      });
      return (
        <View>
          {arrBreakfastInner}
        </View >
      );
    } catch (error) {
      console.log("Error in renderBreakfatInnerList", error);
    }
  }

  shareImage = (img_url, message) => {
    try {
      const fs = RNFetchBlob.fs;
      let imagePath = null;
      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", img_url)
        // the image is now dowloaded to device's storage
        .then(resp => {
          // the image path you can use it directly with Image component
          imagePath = resp.path();
          return resp.readFile("base64");
        })
        .then(base64Data => {
          // here's base64 encoded image
          //console.log(base64Data);
          Share1.open({
            title: 'Share Via',
            message: message,
            // social: Share1.Social.WHATSAPP,
            url: `data:image/png;base64,${base64Data}`,
            // url: "file://" + img_url,

          })
            .then(res => {
              LogUtils.infoLog(res);
            })
            .catch(err => {
              err && console.log(err);
            });
          // remove the file from storage
          return fs.unlink(imagePath);
        });
    } catch (error) {
      console.log("Error in shareImage", error);
    }
  };

  renderWoroutAddButton() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.workout) && this.state.resObj.workout.length) {
          return (
            <View style={styles.vHeaderTitMore}>
              <Text style={styles.textPlan}>{'Your Program Schedule'.toUpperCase()}</Text>
              <TouchableOpacity
                onPress={() => this.onWorkoutsClicked()}>
                <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
              </TouchableOpacity>

            </View>
          );
        }
        else {
          return (
            <View>
              <View style={styles.vHeaderTitMore}>
                {/* <Text style={styles.textPlan}>{'Enroll in a Program'.toUpperCase()}</Text> */}
                <Text style={styles.textPlan}>{'Recommended Programs'.toUpperCase()}</Text>

                <TouchableOpacity
                  onPress={() => this.onWorkoutsClicked()}>
                  <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                </TouchableOpacity>

              </View>
              {/* <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderWoroutAddButton : ", error);
    }
  }

  renderMostPopular() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.popular_workout) && this.state.resObj.popular_workout.length) {
          return (
            <View>
              <Text style={styles.txtTitProType}>Most Popular</Text>

              <FlatList
                // contentContainerStyle={{ paddingBottom: hp('1%') }}
                contentContainerStyle={{ paddingLeft: 3, }}
                data={this.state.resObj.popular_workout}
                keyExtractor={(item, index) => "popular_workout" + index}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                ItemSeparatorComponent={this.FlatListItemSeparator1}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.p_id} style={{ flexDirection: 'column', height: 95, paddingLeft: 5, paddingBottom: 10, }}
                    onPress={() => {
                      LogUtils.firebaseEventLog('click', {
                        p_id: item.p_id,
                        p_category: 'Programs',
                        p_name: `Program - ${item.name}`,
                      });

                      Actions.traineePlanNew({ pId: item.p_id });
                    }}>
                    <View style={{ flexDirection: 'row', flex: 1, }}>
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={{ uri: item.workoutimg }}
                          style={{
                            width: 140,
                            height: 88,
                            borderRadius: 6,
                            alignSelf: 'center',
                            alignContent: 'center',
                            resizeMode: 'cover',
                          }} />
                        <View style={{ flexDirection: 'column', bottom: 0, position: 'absolute' }}>
                          <Text style={styles.textRecTrainer}>{`${item.trainername}`}</Text>
                        </View>
                      </View>

                      <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', marginLeft: 5, padding: 5 }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', }}>

                          <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.name}</Text>
                          <Text style={styles.textPopuTitle} numberOfLines={1}>{`${item.durtnlvl}`}</Text>
                          <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form}`}</Text>

                          {
                            item.user_images.length ?
                              (<View style={{ marginTop: 2, }}>
                                {this.renderPopularProfiles(item)}
                              </View>) : (<View style={{ marginTop: 10, height: 23 }}></View>)
                          }

                          <View style={{ flexDirection: 'column', alignSelf: 'flex-end', paddingRight: 3, bottom: 0, right: 0, position: 'absolute' }}>
                            <StarRating ratings={item.pgm_rating} />
                          </View>

                        </View>

                      </View>
                    </View>

                  </TouchableOpacity>
                }
                } />
            </View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderMostPopular : ", error);
    }
  }

  renderPopularProfiles(item) {
    if (item) {
      if (item.user_images.length === 1) {
        let arrProfiles = item.user_images.map((item, i) => {
          return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: item.profile_img }}
              style={styles.chaProfile1} />
          </View>
        });
        return (
          <View>
            {arrProfiles}
          </View>
        );
      }
      else if (item.user_images.length === 2) {
        let arrProfiles = item.user_images.map((item, i) => {
          return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: item.profile_img }}
              style={styles.chaProfile1} />
          </View>
        });
        return (
          <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
            {arrProfiles}
          </View>
        );
      }
      else {
        let arrProfiles = item.user_images.map((item, i) => {
          if (i < 2) {
            return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={{ uri: item.profile_img }}
                style={styles.chaProfile1} />
            </View>
          }
        });
        return (
          <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center', }}>
            {arrProfiles}
            <View style={styles.chaMore}>
              <Text style={styles.chaMoreText}>+{item.user_images.length - 2}</Text>
            </View>
          </View>
        );
      }
    }
    else {
      return (
        <View>
        </View>
      );
    }
  }

  FlatListItemSeparator1 = () => {
    return (
      <View
        style={{
          height: 1,
          width: 1,
          backgroundColor: "transparent",
        }}
      />
    );
  }

  trainWithThebestSuperator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: '#b0b0b0',
        }}
      />
    );
  }

  renderFoodCalDetails() {
    if (this.state.resObj) {
      if (this.state.resObj.food) {
        return (
          <View style={{ flexDirection: 'row', width: wp('100%'), justifyContent: 'center' }}>
            <AppIntroSlider
              ref={ref => this.AppIntroSlider = ref}
              style={{ width: wp('100%'), alignSelf: 'center', }}
              slides={this.state.resObj.food}
              // onDone={this._onDone}
              showSkipButton={false}
              showDoneButton={false}
              showNextButton={false}
              // hidePagination={false}
              bottomButton={false}
              dotStyle={{ backgroundColor: 'transparent' }}
              activeDotStyle={{ backgroundColor: 'transparent' }}
              // onSkip={this._onSkip}
              hidePagination={true}
              extraData={this.state}
              // onSlideChange={this.slideChange}
              scrollEnabled={false}
              renderItem={({ item }) => {
                return (
                  <View style={{ flexDirection: 'row', flex: 1, width: wp('90%'), justifyContent: 'center', }}>
                    <View style={styles.calViewDet}>
                      <Text style={styles.calTitle}>{item.cal_food}</Text>
                      <Text style={styles.calDesc}>Consumed</Text>
                    </View>
                    <View style={styles.calViewDet}>
                      <AnimatedCircularProgress
                        size={140}
                        width={7}
                        fill={item.cal_remain_per}
                        tintColor="#8c52ff"
                        arcSweepAngle={290}
                        rotation={215}
                        lineCap="round"
                        backgroundColor="#e9e9e9">
                        {
                          (fill) => (
                            <View>
                              <Text style={styles.calTitleBig}>{item.cal_remain}</Text>
                              <Text style={styles.calDesc}>Calories left</Text>
                            </View>

                          )
                        }
                      </AnimatedCircularProgress>

                      <TouchableOpacity onPress={() => {
                        LogUtils.firebaseEventLog('click', {
                          p_id: 213,
                          p_category: 'Home',
                          p_name: 'Home->FoodHome',
                        });
                        Actions.traHomeFoodNew({ mdate: item.date_food });
                      }} style={styles.calViewDetails}>
                        <Text style={styles.calDetail}>DETAIL</Text>
                      </TouchableOpacity>

                    </View>
                    <View style={{
                      flexDirection: 'column',
                      width: wp('30%'),
                      alignItems: 'center',
                      padding: 15,
                      justifyContent: 'center',
                      marginTop: 20,
                      marginLeft: 2,
                    }}>
                      <Text style={styles.calTitle}>{item.cal_burn}</Text>
                      <Text style={styles.calDesc}>Active calories burned</Text>
                      {this.state.resObj.info_source_url != ''
                        ? (
                          <TouchableOpacity onPress={() => {
                            Actions.setDetail({ from: '', url: this.state.resObj.info_source_url })
                          }} style={{
                            position: 'absolute',
                            bottom: 15,
                          }}>
                            <Text style={styles.sourceText}>Source</Text>
                          </TouchableOpacity>

                        ) : (
                          <View></View>

                        )}
                    </View>

                  </View>
                );
              }}
            />
          </View>

        );
      }
      else {
        return (
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.textNodata}>{this.state.noDataWorkouts}</Text>
          </View>
        );
      }
    }
  }
  //End UW

  //Bottom nav
  renderDietBottomMenuIcon() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (this.state.resObj.is_paiduser === 1) { //Paid user
          if ((this.state.resObj.is_diet_plan === 1) ||
            (this.state.resObj.is_diet_plan === 1 && this.state.resObj.is_fitness_plan === 1)
          ) { // only diet and both
            return (
              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => {
                  this.onDietClicked()
                }}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_diet.png')}
                      style={{
                        width: 23,
                        height: 23,
                        alignSelf: 'center',
                        tintColor: '#d6d9e0',
                      }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Diet</Text>
                </View>
              </TouchableOpacity>
            );
          } else if (this.state.resObj.is_fitness_plan === 1) { // only fitnesss
            return (
              <TouchableOpacity
                style={styles.bottomClick}
                onPress={() => this.onTrendWorkoutsClicked()}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={styles.vBtmImgHeight}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_workout.png')}
                      style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                    />
                  </View>
                  <Text style={styles.imgBottomMenuText1}>Workouts</Text>
                </View>
              </TouchableOpacity>
            )
          }
        } else { // New User
          return (
            <TouchableOpacity
              style={[styles.bottomClick]}
              onPress={() => {
                this.rbMenuSheet.close()
                LogUtils.firebaseEventLog('click', {
                  p_id: 222,
                  p_category: 'Home',
                  p_name: 'Home->Select a Plan',
                });
                Actions.traAllBuyPlans1({ isFreeTrial: this.state.resObj.show_free_trial, screen: "Bottom memu view plans" });
              }}
            >
              <View style={{ flexDirection: 'column' }}>
                {
                  this.state.resObj.plans_disc_per != "" &&
                  (<View style={[styles.tabBadge, { backgroundColor: this.state.resObj.home_bg_color, right: -33, borderRadius: 4, fontSize: 6 }]}>
                    <Text style={styles.tabBadgeText}>{`${this.state.resObj.plans_disc_per}`}</Text>
                  </View>)
                }
                <View style={styles.vBtmImgHeight}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_bm_viewplans.png')}
                    style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                  />
                </View>
                <Text style={styles.imgBottomMenuText1}>Plans</Text>
              </View>
            </TouchableOpacity>
          )
        }
      } else { // When object empty
        return (
          <TouchableOpacity
            style={styles.bottomClick}
            onPress={() => this.onTrendWorkoutsClicked()}>
            <View style={{ flexDirection: 'column', }}>
              <View style={styles.vBtmImgHeight}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_workout.png')}
                  style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                />
              </View>
              <Text style={styles.imgBottomMenuText1}>Workouts</Text>
            </View>
          </TouchableOpacity>
        )
      }
    } catch (error) {
      console.log(error);
    }
  }
  //End Bottom

  //Add Photos region
  captureImage = () => {
    let options = {
      title: 'Select Image',
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 500,
      maxWidth: 500,

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.7,
    };
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileType: response.type,
        });
        this.validateImage(response.fileSize);
      }
    });
  };

  async pickImagesFromGallery() {
    // Pick a single file
    try {
      const response = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });

      this.setState({
        filePath: response,
        fileData: response.data,
        fileUri: response.uri,
        fileType: response.type,
        fileName: response.name,
      });

      LogUtils.infoLog1('response', response);

      this.validateImage(response.size);

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  async pickImagesFromGalleryIOS() {
    try {
      // Pick a single file
      let options = {
        title: 'Select Image',
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 500,
        maxWidth: 500,

        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
        quality: 0.7,
      };
      ImagePicker.launchImageLibrary(options, response => {
        if (response.didCancel) {
          LogUtils.infoLog('User cancelled image picker');
        } else if (response.error) {
          LogUtils.infoLog1('ImagePicker Error: ', response.error);
        } else {
          // LogUtils.infoLog1('response', response);
          LogUtils.infoLog1('fileUri', response.uri);

          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
          this.validateImage(response.fileSize);
        }
      });
    } catch (error) {
      console.log(error)
    }
  }

  async validateImage(fileSize) {
    let max_img_size = await AsyncStorage.getItem('max_img_size');
    var imgSize = parseInt(max_img_size) / 1048576;
    if (fileSize < parseInt(max_img_size)) {
      // this.callPhotoUpload();
      this.callFoodImagesToAWSS3();
    }
    else {
      this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `Please make sure your file size should not exceed ${imgSize}MB` });
    }
  }

  async callFoodImagesToAWSS3() {
    try {
      this.setState({ progress: 0, isProgress: true });
      let userId = await AsyncStorage.getItem('userId');
      let type = '', extension = '';

      if (this.state.fileUri.includes('.png') || this.state.fileUri.includes('.PNG')) {
        type = 'image/png';
        extension = '.png';
      } else if (this.state.fileUri.includes('.jpg') || this.state.fileUri.includes('.JPG')) {
        type = 'image/jpg';
        extension = '.jpg';
      }
      else {
        if (this.state.fileName) {
          const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
          type = this.state.fileType;
          extension = '.' + fileExtension.toLowerCase();
        }
      }

      const file = {
        uri: this.state.fileUri,
        name: userId + '_' + new Date().getTime() + extension,
        type: type,
      }

      const options = {
        keyPrefix: await AsyncStorage.getItem('s3_userfoodimg_path'),
        bucket: await AsyncStorage.getItem('s3_bucket'),
        region: await AsyncStorage.getItem('s3_region'),
        accessKey: await AsyncStorage.getItem('s3_key'),
        secretKey: await AsyncStorage.getItem('s3_secret'),
        successActionStatus: 201
      }
      LogUtils.infoLog1('file : ', file);
      LogUtils.infoLog1('options : ', options);

      RNS3.put(file, options)
        .progress((e) => {
          const progress = Math.floor((e.loaded / e.total) * 100);
          this.setState({ progress: progress });
        }).then(response => {
          if (response.status === 201) {
            this.setState({ resObjAwsS3: response.body.postResponse });
            LogUtils.infoLog1('response', response.body);
            LogUtils.infoLog1('response1', response.body.postResponse);

            this.callPhotoUpload();
          }
          else {
            LogUtils.infoLog('Failed to upload image to S3');
            LogUtils.infoLog1('response', response.body);
            this.setState({ isProgress: false });
          }
        });
    } catch (error) {
      console.log(error)
    }
  }

  async callPhotoUpload() {
    try {
      if (this.state.fileUri) {
        // this.setState({ isProgress: true });

        let token = await AsyncStorage.getItem('token');
        let body = JSON.stringify({
          aimt_id: this.state.aimtId,
          img_url: this.state.resObjAwsS3.location
        });
        LogUtils.infoLog1("body", body);
        fetch(
          `${BASE_URL}/trainee/userfoodimages3`,
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
            body: body,
          },
        )
          .then(processResponse)
          .then(res => {
            const { statusCode, data } = res;
            LogUtils.infoLog1("statusCode", statusCode);
            LogUtils.infoLog1("data", data);
            if (statusCode >= 200 && statusCode <= 300) {
              this.setState({ isProgress: false });
              this.getHomepageDet();
              this.getProducts();
            } else {
              if (data.message === 'You are not authenticated!') {
                this.setState({ isProgress: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
              } else {
                this.setState({ isProgress: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
              }
            }
          })
          .catch(function (error) {
            this.setState({ isProgress: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
          });
        // futch(`${BASE_URL}/trainee/userfoodimage`, {
        //   method: 'POST',
        //   body: this.createFormData(this.state.fileUri, body),
        //   headers: {
        //     'Content-Type': 'multipart/form-data',
        //     Authorization: `Bearer ${token}`,
        //   },

        // }, (progressEvent) => {
        //   const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
        //   this.setState({ progress: progress });
        //   LogUtils.infoLog(progress);
        // })
        //   .then(res => {
        //     const statusCode = res.status;
        //     LogUtils.infoLog1('statusCode', statusCode);
        //     const data = res.response;
        //     var obj = JSON.parse(res.response)
        //     LogUtils.infoLog1('data', data);
        //     LogUtils.infoLog1('message', data.message);
        //     if (statusCode >= 200 && statusCode <= 300) {
        //       this.setState({ isProgress: false });
        //       this.getHomepageDet();
        //     } else {
        //       if (data.message === 'You are not authenticated!') {
        //         this.setState({ isAlert: true, alertTitle: obj.title, alertMsg: obj.message, isProgress: false });
        //       } else {
        //         this.setState({ isAlert: true, alertTitle: obj.title, alertMsg: obj.message, isProgress: false });
        //       }
        //     }
        //   })
        //   .catch(function (error) {
        //     this.setState({ isProgress: false });
        //     LogUtils.infoLog1('upload error', error);
        //     this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: JSON.stringify(error), isProgress: false });
        //   });
      }
      else {
        this.setState({ isAlert: true, alertMsg: 'Please select/capture image' });
      }
    } catch (error) {
      console.log(error)
    }
  }

  //End region

  //Start Home page region 
  //render
  afterAnimation() {
    LogUtils.infoLog('animated')
  }

  handleChangeMenu(currentMenu) {
    try {
      this.setState({ currentMenu })
      // LogUtils.infoLog(currentMenu);
      if (currentMenu === 'START 7 DAYS FREE TRIAL') {
        this.bottompopup.open();
        // this.bottomMultiPopup.open();
      } else if (currentMenu === 'EXPLORE PREMIUM FEATURES') {
        Actions.traAllBuyPlans1({ screen: "Side menu Explore" });
        // this.bottomMultiPopup.open();
      }
      else if (currentMenu === 'HOME') {
        // Actions.chaRewards({mFrom: 'product'});
      }
      else if (currentMenu === 'MY PROFILE') {
        LogUtils.firebaseEventLog('click', {
          p_id: 205,
          p_category: 'Home',
          p_name: 'Home->MyProfile',
        });
        Actions.myProfile();
      }
      else if (currentMenu === 'VIEW PLANS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 222,
          p_category: 'Home',
          p_name: 'Home->Select a Plan',
        });
        Actions.traAllBuyPlans1({ isFreeTrial: this.state.resObj.show_free_trial, screen: "Side menu view plans" });
      }
      else if (currentMenu === 'MY SUBSCRIPTIONS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 222,
          p_category: 'Home',
          p_name: 'Home->MySubscriptions',
        });
        Actions.myOrders();
      }
      else if (currentMenu === 'DIET') {
        this.onDietClicked();
      }
      else if (currentMenu === 'MY MEDICAL RECORDS') {
        Actions.myMdlReds();
      }
      else if (currentMenu === 'BODY MEASUREMENTS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 299,
          p_category: 'Home',
          p_name: 'Home->BodyMeasurenemts',
        });
        Actions.bodymeasure();
      }
      else if (currentMenu === 'APPOINTMENTS') {
        this.callAppointments();
      }
      else if (currentMenu === 'MY PHOTOS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 223,
          p_category: 'Home',
          p_name: 'Home->MyPhotos',
        });
        Actions.traPhotos();
      }
      else if (currentMenu === 'LINK MY TRACKER') {
        LogUtils.firebaseEventLog('click', {
          p_id: 224,
          p_category: 'Home',
          p_name: 'Home->LinkMyTracker',
        });
        Actions.trackdevices();
      }else if (currentMenu ==='SETUP CALORIE COUNTER'){
        LogUtils.firebaseEventLog('click', {
          p_id: 224,
          p_category: 'Home',
          p_name: 'Home->side menu->setupcaloriecounter',
        });
        Actions.ageCnf();
        //calorieCounterSetup
        //ageCnf
      }
      else if (currentMenu === 'NOTIFICATIONS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 225,
          p_category: 'Home',
          p_name: 'Home->Notifications',
        });
        Actions.notifi();
      }
      else if (currentMenu === 'CUSTOM WORKOUT') {

      }
      else if (currentMenu === 'REFER A FRIEND') {
        this.callReferAFriend();
      }
      else if (currentMenu === 'LIVE CHAT') {
        Actions.liveChat();
      }
      else if (currentMenu === 'ONE ON ONE CHAT') {
        LogUtils.firebaseEventLog('click', {
          p_id: 231,
          p_category: 'Home',
          p_name: 'Home->ChatList',
        });
        Actions.chatlist();
      }
      else if (currentMenu === 'HELP') {
        LogUtils.firebaseEventLog('click', {
          p_id: 227,
          p_category: 'Home',
          p_name: 'Home->Help',
        });
        Actions.traHelp();
      }
      else if (currentMenu === 'SETTINGS') {
        LogUtils.firebaseEventLog('click', {
          p_id: 228,
          p_category: 'Home',
          p_name: 'Home->Settings',
        });
        Actions.settings();
      }
      else if (currentMenu === 'LOGOUT') {
        LogUtils.firebaseEventLog('click', {
          p_id: 229,
          p_category: 'Home',
          p_name: 'Home->Logout',
        });
        this.setState({ isSuccess: true, titMsg: 'Are you sure !', sucMsg: 'Do you want to logout ?' });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async callReferAFriend() {
    LogUtils.firebaseEventLog('click', {
      p_id: 226,
      p_category: 'Home',
      p_name: 'Home->ReferAFriend',
    });
    Actions.refer();
  }

  async callAppointments() {
    if (this.state.resObj.is_paiduser === 1) {
      LogUtils.firebaseEventLog('click', {
        p_id: 212,
        p_category: 'Home',
        p_name: 'Home->Appointments',
      });

      Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj });

    } else {
      LogUtils.firebaseEventLog('click', {
        p_id: 212,
        p_category: 'Home',
        p_name: 'Home->Appointments',
      });
      // Actions.myOrders({from : 'appointments'});
      Actions.appointments({ isPaid: 0, freeTrial: this.state.resObj.show_free_trial, plantype: this.state.resObj.plan_type, homeObj: this.state.freeTrialInitialObj });
    }
  }

  handleToggleMenu() {
    try {
      this.setState({ open: !this.state.open })
    } catch (error) {
      console.log(error);
    }
  }

  renderProgressBar() {
    if (Platform.OS === 'android') {
      return (
        <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
      );
    } else {
      return (
        <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
      );
    }
  }

  async onRetry() {
    try {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ isInternet: false });
          if (trackId === '1' && fitbitToken) {
            getUserProfile(fitbitToken);
            getData(fitbitToken);
          }
          else if (trackId === '2' && Platform.OS === 'android') {
            this.sendGoogleFitData();
          } else if (trackId === '2' && Platform.OS === 'ios') {
            this.appleAuth();
          }
          this.getHomepageDet();
          // this.getProducts();
          // this.getHomepTrendingWorkouts();
        }
        else {
          this.setState({ isInternet: true });
        }
      });
    } catch (error) {
      console.log(error)
    }
  }

  onRefresh() {
    try {

      this.setState({
        refreshing: true,
        foodType: 1,
        noDataWorkouts: '',
        noDataChallenges: '',
      });

      this.getHomepageDet();
      // this.getProducts();
      // this.getHomepTrendingWorkouts();

      if (trackId === '1' && fitbitToken) {
        getUserProfile(fitbitToken);
        getData(fitbitToken);

      }
      else if (trackId === '2' && Platform.OS === 'android') {
        this.sendGoogleFitData();
      } else if (trackId === '2' && Platform.OS === 'ios') {
        this.appleAuth();
      }
    } catch (error) {
      console.log(error);
    }
  };

  closeMenu() {
    if (this.state.open) {
      this.setState({ open: false });
    }
  }

  gotoProfile() {
    LogUtils.firebaseEventLog('click', {
      p_id: 205,
      p_category: 'Home',
      p_name: 'Home->ProfilePictire',
    });
    Actions.myProfile();
  }

  renderProfileImg() {
    if (this.state.resObj.img_url) {
      return (
        <Image
          resizeMethod="resize"
          source={{ uri: this.state.resObj.img_url }}
          style={styles.profileImage}
          progressiveRenderingEnabled={true}
        />
      );
    }
    else {
      return (
        <Image
          resizeMethod="resize"
          source={require('../res/ic_profile_gray.png')}
          style={styles.profileImage}
          progressiveRenderingEnabled={true}
        />
      );
    }
  }

  renderWelcomeMsg = (currentTime = new Date()) => {
    const currentHour = currentTime.getHours()
    const splitAfternoon = 12; // 24hr time to split the afternoon
    const splitEvening = 17; // 24hr time to split the evening

    if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
      // Between 12 PM and 5PM
      return 'Good Afternoon';
    } else if (currentHour >= splitEvening) {
      // Between 5PM and Midnight
      return 'Good Evening';
    }
    // Between dawn and noon
    return 'Good Morning';
  }

  renderWorkoutsList() {
    try {
      if (!isEmpty(this.state.resObj)) {
        // LogUtils.infoLog1('Workout : ', this.state.resObj.workout)
        if (Array.isArray(this.state.resObj.workout) && this.state.resObj.workout.length) {
          return (
            <View style={styles.workOutBgNew}>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan}>{'Your Program Schedule'.toUpperCase()}</Text>
                <TouchableOpacity
                  onPress={() => this.onWorkoutsClicked()}>
                  <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.workout}
                // keyExtractor={item => item.pg_id}
                keyExtractor={(item, index) => "workout" + index}
                contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.pg_id} style={{ flexDirection: 'column', }} onPress={() => {
                    // console.log(item)
                    if (this.state.open) {
                      this.setState({ open: false });
                    }
                    else {
                      if (item.rest_day === 0) {
                        Actions.traPlanIntro({ pdObj: item, from: 'home' });
                      }
                    }

                  }}>
                    <View style={{ flexDirection: 'column', }}>
                      <Image
                        source={{ uri: item.home_img }}
                        resizeMethod="resize"
                        progressiveRenderingEnabled={true}
                        style={{
                          width: 150,
                          height: 268,
                          borderRadius: 8,
                          alignSelf: 'center',
                          resizeMode: 'cover',
                        }}
                      />

                      <View style={styles.viewRecDetails}>
                        <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.name}`}</Text>
                        <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.title}`}</Text>
                        <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.duration}`}</Text>
                      </View>

                    </View>
                  </TouchableOpacity>
                }} />
            </View>
          );
        }
        else {
          return (
            <View />
          );
        }
      }
    } catch (error) {
      console.log("Error in renderWorkoutsList : ", error);
    }
  }

  renderRecommendedForYou() {
    try {
      if (!isEmpty(this.state.resObj)) {
        // LogUtils.infoLog1('Top Workout : ', this.state.resObj.top_workout);
        if (Array.isArray(this.state.resObj.top_workout) && this.state.resObj.top_workout.length) {
          return (
            <View>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan}>{'Recommended Programs'.toUpperCase()}</Text>

                <TouchableOpacity
                  onPress={() => this.onWorkoutsClicked()}>
                  <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                </TouchableOpacity>

              </View>
              {/* <View style={{ height: 15, }}></View> */}
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.top_workout}
                // keyExtractor={item => item.pg_id}
                keyExtractor={(item, index) => "top_workout" + index}
                contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                renderItem={({ item }) => {
                  return <View>
                    {item.is_comingsoon === 1
                      ?
                      (
                        <TouchableOpacity key={item.pg_id} activeOpacity={0.9} style={{ flexDirection: 'column', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.home_img }}
                            style={{
                              width: 150,
                              height: 268,
                              borderRadius: 8,
                              alignSelf: 'center',
                              resizeMode: 'cover',
                              // aspectRatio: 0.55 / 1,
                            }}
                          />
                          <View style={styles.viewRecDetails}>
                            <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.name}`}</Text>
                            <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.duration}`}</Text>
                            <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.fitnessform}`}</Text>
                          </View>
                        </TouchableOpacity>
                      )
                      :
                      (
                        <TouchableOpacity key={item.pg_id} activeOpacity={0.5} style={{ flexDirection: 'column', }} onPress={() => {
                          if (this.state.open) {
                            this.setState({ open: false });
                          }
                          else {
                            if (this.state.resObj.is_paiduser === 1) {

                              if (this.state.resObj.workout_imp_confirm === 0) {
                                if (this.state.resObj.plan_type === 2 || this.state.resObj.plan_type === 3) {
                                  Actions.traBuySuccess({ from: 'homepage', homeQType: this.state.resObj.plan_type });
                                } else {
                                  Actions.traineePlanNew({ pId: item.pg_id });
                                }
                              }
                              else {
                                Actions.traineePlanNew({ pId: item.pg_id });
                              }

                            }
                            else {
                              Actions.traineePlanNew({ pId: item.pg_id });

                            }
                          }

                        }}>
                          <View style={{ flexDirection: 'column', }}>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={{ uri: item.home_img }}
                              style={{
                                width: 150,
                                height: 268,
                                borderRadius: 8,
                                alignSelf: 'center',
                                resizeMode: 'cover',
                                // aspectRatio: 0.55 / 1,
                              }}
                            />
                            <View style={styles.viewRecDetails}>
                              <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.name}`}</Text>
                              <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.duration}`}</Text>
                              <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.fitnessform}`}</Text>
                            </View>
                          </View>
                        </TouchableOpacity>
                      )
                    }
                  </View>
                }} />
            </View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderRecommendedForYou : ", error);
    }
  }

  FlatListItemSeparatorWorkouts = () => {
    return (
      <View
        style={{
          height: 1,
          width: 10,
          backgroundColor: "transparent",
        }}
      />
    );
  }

  renderTodayWorouts() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.today_workouts) && this.state.resObj.today_workouts.length) {
          return (
            <View style={styles.workOutBgNew}>
              <View style={{ height: 10 }}></View>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan}>{'Free Workout Of The Day'.toUpperCase()}</Text>
                <TouchableOpacity>
                  <Text style={styles.textAdd}></Text>
                </TouchableOpacity>
              </View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.today_workouts}
                // keyExtractor={item => item.pg_id}
                keyExtractor={(item, index) => "today_workouts" + index}
                contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.pg_id} activeOpacity={0.8} style={{ flexDirection: 'column', }} onPress={() => {
                    if (this.state.open) {
                      this.setState({ open: false });
                    }
                    else {
                      if (item.pd_id !== 0) {
                        Actions.traPlanIntro({ pdObj: item, from: 'workouts', pId: item.p_id });
                      }
                    }
                  }}>
                    <View style={{ flexDirection: 'column', }}>
                      {item.pd_id !== 0 ?
                        (
                          <View>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={{ uri: item.img_url }}
                              style={{
                                width: 300,
                                height: undefined,
                                borderRadius: 7,
                                resizeMode: 'cover',
                                aspectRatio: 16 / 9,
                              }}
                            />
                            <View style={{
                              flexDirection: 'column',
                              alignItems: 'center',
                              // alignContent: 'flex-end',
                              // margin: 10,
                              top: 30,
                              right: 0,
                              alignSelf: 'center',
                              position: 'absolute',
                              justifyContent: 'flex-end',
                            }}>
                              <View style={styles.vWorkoutTrainer}>
                                <Text style={styles.textWorkTrainer}>{`${item.tr_f_name.toUpperCase()}`}</Text>
                                <Text style={styles.textWorkTrainer}>{`${item.tr_l_name.toUpperCase()}`}</Text>
                              </View>
                              <Text style={styles.textWorkSpecial}>{`${item.fitness_form}`}</Text>

                              <Text style={styles.textWorkOftheDay}>{`${item.duration}`}</Text>
                              <Text style={styles.textWorkOftheDay}>{`${item.focus_area}`}</Text>
                            </View>
                          </View>

                        )
                        :
                        (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.img_url }}
                            style={{
                              width: 300,
                              height: undefined,
                              borderRadius: 7,
                              resizeMode: 'cover',
                              aspectRatio: 16 / 9,
                            }}
                          />
                        )}
                    </View>
                  </TouchableOpacity>
                }} />

            </View >
          );
        }
        else {
          return (
            <View>
            </View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderTodayWorouts : ", error);
    }
  }

  renderBestTrainersList() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.trainer) && this.state.resObj.trainer.length) {
          // LogUtils.infoLog1('trainers : ', this.state.resObj.trainer);
          return (
            <View>
              {/* <View style={{ height: 15 }}></View> */}
              <View style={styles.workOutBgNew}>
                <View style={styles.vHeaderTitMore}>
                  <Text style={styles.textPlan1}>
                    {'Train With The Best'.toUpperCase()}
                  </Text>
                  <View style={{ flex: 1 }}></View>
                  <TouchableOpacity
                    onPress={() => Actions.trainersList()}>
                    <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </View>

                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.resObj.trainer}
                  style={{ alignSelf: 'flex-start' }}
                  // numColumns={3}
                  // keyExtractor={item => item.pg_id}
                  keyExtractor={(item, index) => "trainer" + index}
                  // contentContainerStyle={{ margin: 10, paddingRight: 30 }}
                  ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                  renderItem={({ item }) => {
                    return <TouchableOpacity key={item.id} style={styles.vBestTrainer} onPress={() => {
                      // Actions.traDiscover({ mFrom: 'trainer', title: item.tr_name, wType: item.tr_id, ffId: 0 });
                      Actions.disHomePage({ title: item.tr_name, ffId: 0, tr_id: item.tr_id });
                    }}>
                      <View style={{ flexDirection: 'column', }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={{ uri: item.img }}
                          // source={`${item.image}`}
                          style={styles.imgTrainer}
                        />
                        <View style={{ paddingTop: 3, paddingBottom: 3, alignItems: 'center', alignSelf: 'flex-start', justifyContent: 'center' }}>
                          <Text numberOfLines={1} style={styles.textTrName}>{`${item.tr_name}`}</Text>
                          <Text numberOfLines={1} style={styles.textSpecial}>{`${item.special_in}`}</Text>
                          <Text numberOfLines={1} style={styles.textSpecial}>{`${item.fitness_form}`}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  }} />
              </View>
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderTrendingWorkouts() {
    try {
      if (!isEmpty(this.state.resObj)) {
        // LogUtils.infoLog1('Trending Workouts: ', this.state.arrTrendingWorkouts);
        if (Array.isArray(this.state.arrTrendingWorkouts) && this.state.arrTrendingWorkouts.length) {
          return (
            <View style={styles.workOutBgNew}>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan1}>
                  {'Trending Workouts'.toUpperCase()}
                </Text>
                <View style={{ flex: 1 }}></View>
                <TouchableOpacity
                  onPress={() => {
                    this.onTrendWorkoutsClicked();
                  }}>
                  <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                </TouchableOpacity>
              </View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.arrTrendingWorkouts}
                // keyExtractor={item => item.pg_id}
                keyExtractor={(item, index) => "arrTrendingWorkouts" + index}
                contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                renderItem={({ item }) => {
                  return <View>
                    <TouchableOpacity key={item.pg_id} activeOpacity={0.5} style={{ flexDirection: 'column', }} onPress={() => {
                      Actions.traPlanIntro({ pdObj: item, from: 'workouts', pId: item.p_id });
                    }}>
                      <View style={{ flexDirection: 'column', }}>
                        <View>
                          {item.pd_img
                            ? (
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.pd_img }}
                                style={{
                                  width: 250,
                                  height: 150,
                                  borderRadius: 7,
                                  aspectRatio: 16 / 9,
                                }}
                              />
                            )
                            : (
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                  width: 250,
                                  height: 150,
                                  borderRadius: 7,
                                  resizeMode: 'cover',
                                }}
                              />
                            )
                          }
                          {this.renderFreeorPremium(item)}
                        </View>
                        <View style={styles.viewTrendDetails}>
                          <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.title}`}</Text>
                          <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.tr_name}`}</Text>
                          <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.fitness_form} - ${item.duration}`}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                }} />
            </View>
          );
        }
        else {
          return (
            <View style={{ height: 1 }}></View>
          );
        }
      }
      else {
        return (
          <View style={{ height: 1 }}></View>
        );
      }
    } catch (error) {
      console.log("Error in renderTrendingWorkouts : ", error);
    }
  }

  renderFreeorPremium(item) {
    try {
      if (item.pd_type === 1) {
        return (
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            alignContent: 'flex-end',
            margin: 1,
            right: 0,
            position: 'absolute',
            justifyContent: 'center',
          }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/ic_free.png')}
              style={{
                width: 35,
                height: 33,
                alignSelf: 'center',
                // tintColor: '#ffffff'
              }}
            />
          </View>
        );
      }
      else if (item.pd_type === 2) {
        return (
          <View style={{
            flexDirection: 'column',
            alignItems: 'center',
            alignContent: 'flex-end',
            margin: 1,
            right: 0,
            position: 'absolute',
            justifyContent: 'center',
          }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/ic_premium.png')}
              style={{
                width: 35,
                height: 33,
                alignSelf: 'center',
                // tintColor: '#ffffff'
              }}
            />
          </View>
        );
      }
      else {
        return (
          <View>
          </View>
        );
      }
    } catch (error) {
      console.log("Error in renderFreeorPremium : ", error);
    }
  }

  renderMentalFitnessList() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.mental_pgm) && this.state.resObj.mental_pgm.length) {
          return (
            <View>
              {/* <View style={{ height: 15 }}></View> */}
              <View style={styles.workOutBgNew}>
                <View style={styles.vHeaderTitMore}>
                  <Text style={styles.textPlan}>{'Mental Health '.toUpperCase()}</Text>
                  <TouchableOpacity
                    onPress={() => Actions.mentList()}>
                    <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ height: 1, marginTop: -10 }} />

                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.resObj.mental_pgm}
                  // keyExtractor={item => item.pg_id}
                  keyExtractor={(item, index) => "mental_pgm" + index}
                  contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                  ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                  renderItem={({ item }) => {
                    return <View>
                      <TouchableOpacity key={item.pg_id} activeOpacity={0.5} style={{ flexDirection: 'column', }}
                        onPress={() => Actions.mentDetails({ pId: item.pg_id })}
                      //  onPress={item => {
                      //   Actions.mentDetails({ pId: item.pg_id });
                      // }}
                      >
                        <View style={{ flexDirection: 'column', }}>
                          <View>
                            {item.img
                              ? (
                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  source={{ uri: item.img }}
                                  style={{
                                    width: 250,
                                    height: 150,
                                    borderRadius: 7,
                                    aspectRatio: 16 / 9,
                                  }}
                                />
                              )
                              : (
                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  source={require('../res/ic_noimage.png')}
                                  style={{
                                    width: 250,
                                    height: 150,
                                    borderRadius: 7,
                                    resizeMode: 'cover',
                                  }}
                                />
                              )
                            }
                          </View>
                          {/* <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          alignContent: 'flex-start',
                          margin: 10,
                          bottom: 0,
                          left: 10,
                          position: 'absolute',
                          justifyContent: 'flex-start',
                        }}>
                          <Text style={styles.traineerDesc}>{item.tr_name}</Text>
                        </View> */}
                          <View style={styles.viewTrendDetails}>
                            <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.tr_name}`}</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  }} />


                {/* <FlatListSlider
                  width={wp('94%')}
                  height={230}
                  data={this.state.resObj.mental_pgm}
                  autoscroll={false}
                  loop={false}
                  separator={0}
                  component={<ImagePreview />}
                  onPress={item => {
                    Actions.mentDetails({ pId: item.pg_id });
                  }}
                  indicatorContainerStyle={{
                    top: -10,
                  }}
                  indicatorStyle={{
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                  }}
                  // contentContainerStyle={{paddingHorizontal: 15, paddingVertical: 15}}
                  // indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
                  indicatorActiveColor={'#8c52ff'}
                  indicatorInActiveColor={'#b0b0b0'}
                  indicatorActiveWidth={15}
                  animation
                /> */}
                <View style={{ height: 1, marginTop: 5 }}></View>
              </View>
            </View>
          );
        }
        else {
          return (
            <View />
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderDiscover() {
    try {
      if (!isEmpty(this.state.resObj)) {
        // LogUtils.infoLog1('discover: ', this.state.resObj.discover);
        if (Array.isArray(this.state.resObj.discover) && this.state.resObj.discover.length) {
          return (
            <View style={styles.workOutBgNew}>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan1}>
                  {'Discover'.toUpperCase()}
                </Text>
                <View style={{ flex: 1 }}></View>
                <TouchableOpacity
                  onPress={() => {
                    // this.onTrendWorkoutsClicked();
                  }}>
                  <Text style={styles.textAdd}></Text>
                </TouchableOpacity>
              </View>
              <FlatList
                contentContainerStyle={{
                  // width: wp('96%'),
                  paddingBottom: hp('2%'),
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}
                data={this.state.resObj.discover}
                style={styles.disFlatList}
                numColumns={3}
                keyExtractor={(item, index) => "discover" + index}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.ff_id}
                    style={styles.containerFitnessform}
                    onPress={() => {
                      Actions.disHomePage({ title: item.name, ffId: item.ff_id, tr_id: 0 });
                    }}>
                    <View>
                      {item.img_url
                        ? (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.img_url }}
                            style={{
                              width: wp('30%'),
                              height: undefined,
                              borderRadius: 7,
                              resizeMode: 'cover',
                              aspectRatio: 1 / 1,
                            }}
                          />
                        )
                        : (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_noimage.png')}
                            style={{
                              width: wp('30%'),
                              height: undefined,
                              borderRadius: 7,
                              resizeMode: 'cover',
                              aspectRatio: 1 / 1,
                            }}
                          />
                        )
                      }

                      <View style={{ paddingTop: 3, paddingBottom: 3, alignItems: 'center', alignSelf: 'flex-start', justifyContent: 'center' }}>
                        <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.name}`}</Text>
                      </View>
                    </View>

                  </TouchableOpacity>
                }} />
            </View>
          );
        }
        else {
          return (
            <View />
          );
        }
      }
    } catch (error) {
      console.log("Error in renderDiscover : ", error);
    }
  }

  //Home Banner
  renderHomeBanners() {
    try {
      // LogUtils.infoLog1('this.state.resObj.navgt', this.state.resObj.navgt);
      if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.navgt) && this.state.resObj.navgt.length) {
          return (
            <FlatList
              data={this.state.resObj.navgt}
              extraData={this.state}
              style={{ width: wp('94%'), alignSelf: 'center' }}
              // keyExtractor={item => item.c_id}
              keyExtractor={(item, index) => "navgt" + index}
              showsVerticalScrollIndicator={false}
              scrollEnabled={false}
              contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
              ItemSeparatorComponent={this.FlatListItemSeparatorBanners}
              renderItem={({ item }) => {
                return <View style={{ width: wp('94%'), flexDirection: 'column', alignSelf: 'center', }}>
                  <View style={styles.vHeaderTitMore}>
                    <Text style={styles.textPlan}>{item.navgt_header.toUpperCase()}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.onBannerImageClicked(item)}>
                    {item.img_url
                      ? (
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={{ uri: item.img_url }}
                          style={{
                            width: wp('94%'),
                            height: undefined,
                            borderRadius: 6,
                            alignSelf: 'center',
                            // resizeMode: 'cover',
                            aspectRatio: 2 / 1,
                          }}
                        />
                      )
                      : (
                        <Image
                          source={require('../res/ic_noimage.png')}
                          style={{
                            width: wp('94%'),
                            height: hp('25%'),
                            borderRadius: 6,
                            alignSelf: 'center',
                            resizeMode: 'cover',
                            // aspectRatio: 2 / 1,
                          }}
                        />
                      )
                    }
                  </TouchableOpacity>
                </View>
              }} />
          );
        }
        else {
          return (
            <View></View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderHomeBanners: ", error);
    }
  }

  //Home banner separator
  FlatListItemSeparatorBanners = () => {
    return (
      <View style={{ height: 10, }}></View>
    );
  }

  //Click on Image Home banner
  async onBannerImageClicked(item) {
    try {
      if (item.navgt_to !== '') {
        if (item.navgt_to === 'w') {
          this.onTrendWorkoutsClicked();
        }
        else if (item.navgt_to === 't') {
          this.onTrackersClicked();
        }
        else if (item.navgt_to === 'ps') {
          if (Platform.OS === 'android') {
            Linking.openURL("market://details?id=com.transformfitness");
          }
          else if (Platform.OS === 'ios') {
            Linking.openURL('itms-apps://itunes.apple.com/us/app/apple-store/id1545532803?mt=8')
          }
        }
        // else if (item.navgt_to === 'pd') {

        // }
        else if (item.navgt_to === 'p') {
          if (item.navgt_id && item.navgt_id > 0) {
            Actions.traineePlanNew({ pId: item.navgt_id });
          } else {
            Actions.traineeWorkoutsHome();
          }
        }
        else if (item.navgt_to === 'c') {
          if (item.navgt_id && item.navgt_id > 0) {
            // Actions.accChallenge({ cid: item.navgt_id });
            Actions.traChallenges({ cid: item.navgt_id });
          } else {
            Actions.traChallenges()
          }
        }
        else if (item.navgt_to === 'd') {
          if (item.navgt_id && item.navgt_id > 0) {
          } else {
            Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") });
          }
        }
        else if (item.navgt_to === 'wv') {
          if (item.url != '') {
            Actions.webview({ url: item.url });
          }
        }
        else if (item.navgt_to === 'vp') {
          Actions.traAllBuyPlans1({ screen: "Home screen view plans" });
        }
        else if (item.navgt_to === 'of') {
          LogUtils.firebaseEventLog('click', {
            p_id: 298,
            p_category: 'Home',
            p_name: 'Home->OrderFoodList',
          });
          Actions.orderFood();
        } else if (item.navgt_to === 'ewl') {
          if (item.url != '') {
            Linking.openURL(item.url).catch((err) => console.error('An error occurred', err));
          }
        } else if (item.navgt_to === 'fc') {
          this.bottompopup.open();
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderCovidBanner() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (this.state.covid_diet_sent > 0) {

          return (
            <View></View>
          );
        } else {

          if (!isEmpty(this.state.covid_imp) && this.state.covid_imp.is_coviddietavl === 1) {
            return (

              <View style={{ width: wp('94%'), flexDirection: 'column', alignSelf: 'center', }}>
                <View style={styles.vHeaderTitMore}>
                  <Text style={styles.textPlan}>{this.state.covid_imp.diet_title.toUpperCase()}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => { this.covidbottompopup.open() }}
                >
                  {this.state.covid_imp.diet_img
                    ? (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.covid_imp.diet_img }}
                        style={{
                          width: wp('94%'),
                          height: undefined,
                          borderRadius: 6,
                          alignSelf: 'center',
                          // resizeMode: 'cover',
                          aspectRatio: 2 / 1,
                        }}
                      />
                    )
                    : (
                      <Image
                        source={require('../res/ic_noimage.png')}
                        style={{
                          width: wp('94%'),
                          height: hp('25%'),
                          borderRadius: 6,
                          alignSelf: 'center',
                          resizeMode: 'cover',
                          // aspectRatio: 2 / 1,
                        }}
                      />
                    )
                  }
                </TouchableOpacity>
              </View>
            );
          }
          else {
            return (
              <View />
            );
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderCovidBreathingTechniqueBanner() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (!isEmpty(this.state.covid_imp) && this.state.covid_imp.is_breathtechavl === 1) {
          return (
            <View style={{ width: wp('94%'), flexDirection: 'column', alignSelf: 'center', }}>
              <View style={styles.vHeaderTitMore}>
                <Text style={styles.textPlan}>{this.state.covid_imp.breath_title.toUpperCase()}</Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  LogUtils.infoLog1("breath_tech", this.state.resObj.breath_tech);
                  Actions.covidprogramme({ covidObj: this.state.covid_imp, breath_tech: this.state.resObj.breath_tech });
                }}
              >
                {this.state.covid_imp.breath_img
                  ? (
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={{ uri: this.state.covid_imp.breath_img }}
                      style={{
                        width: wp('94%'),
                        height: undefined,
                        borderRadius: 6,
                        alignSelf: 'center',
                        // resizeMode: 'cover',
                        aspectRatio: 2 / 1,
                      }}
                    />
                  )
                  : (
                    <Image
                      source={require('../res/ic_noimage.png')}
                      style={{
                        width: wp('94%'),
                        height: hp('25%'),
                        borderRadius: 6,
                        alignSelf: 'center',
                        resizeMode: 'cover',
                        // aspectRatio: 2 / 1,
                      }}
                    />
                  )
                }
              </TouchableOpacity>
            </View>
          );
        }
        else {
          return (
            <View></View>
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderDietPortionImages() {
    try {
      if (!isEmpty(this.state.resObj)) {
        if (this.state.resObj.show_diet === 1) {
          if (this.state.resObj.user_diet === 1 && this.state.resObj.trail_user === 0) {
            return (
              <View>
                <View style={styles.workOutBgNew}>
                  <View style={styles.vHeaderTitMore}>
                    <View style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      flex: 1,
                      alignSelf: 'flex-start'
                    }}>
                      <Text style={styles.textFoodPhotos}>{'Food Photos'.toUpperCase()}</Text>
                      <Text style={styles.textFoodPhotos1}> (Upload your daily portions)</Text>
                    </View>

                    <TouchableOpacity
                      onPress={() => Actions.traFoodPhotos()}>
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_info_blueround.png')}
                        style={styles.infoAppBlue}
                      />
                    </TouchableOpacity>
                  </View>
                  <FlatList
                    data={this.state.resObj.diet_img}
                    numColumns={5}
                    // extraData={this.state}
                    style={{ marginTop: 5, marginBottom: 5, }}
                    // keyExtractor={item => item.id}
                    keyExtractor={(item, index) => "diet_img" + index}
                    showsVerticalScrollIndicator={false}
                    // ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                      return <View key={item.id} style={{ flexDirection: 'column', flex: 1, padding: 5 }}>
                        {item.img
                          ? (
                            <TouchableOpacity
                              onPress={() => {
                                this.state.foodPhotos.push(item);
                                this.setState({ isShowInfoPopup: true });
                              }}>
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.img }}
                                style={{
                                  width: wp('10%'),
                                  height: wp('10%'),
                                  borderRadius: 8,
                                  alignSelf: 'center',
                                  resizeMode: 'cover',
                                }}
                              />
                            </TouchableOpacity>
                          )
                          : (
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({ aimtId: item.aimt_id, isAddFoodImgPop: true });
                              }}>
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_add_foodimg.png')}
                                style={{
                                  width: wp('10%'),
                                  height: wp('10%'),
                                  // borderRadius: 8,
                                  tintColor: '#d6d5dd',
                                  alignSelf: 'center',
                                  resizeMode: 'cover',
                                }}
                              />
                            </TouchableOpacity>
                          )
                        }
                        <Text style={styles.textAddFoodName}>{`${item.name}`}</Text>
                      </View>
                    }} />

                </View>
              </View>
            );
          }
          else if (this.state.resObj.user_diet === 0 || this.state.resObj.user_diet === 2) {
            return (
              <View>
                {/* <View style={{ height: 15 }}></View>
                <View style={styles.workOutBgNew}>
  
                  <View style={styles.vHeaderTitMore}>
                    <Text style={styles.textPlan}>{'Food Photos'.toUpperCase()}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', padding: 15, width: wp('90%'), alignItems: 'flex-start', justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'column', flex: 1 }}>
                      <Text style={styles.textPhotosInfo}>{this.state.resObj.user_diet_text}</Text>
                    </View>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={{ uri: this.state.resObj.user_dietimg }}
                      style={{ width: 60, height: 60, marginLeft: 5, alignSelf: 'flex-start' }}
                    />
                  </View>
                </View> */}
              </View>
            );
          }
          else {
            return (
              <View>
              </View>
            );
          }
        }
        else {
          return (
            <View></View>
          );
        }
      }
    } catch (error) {
      console.log("Error in renderDietPortionImages:", error);
    }
  }

  closeChallenge(result) {
    this.setState({ open: result })
  }
  //End home page

  async componentWillUnmount() {
    // Stop the trace
    // await this.trace.stop();
    // Don't forget to disable event handlers to prevent errors
    this.props.copilotEvents.off("stop");
    this.backHandler.remove();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  //Popup actions

  async popupUpdate() {
    try {
      this.alertServiceUpdate();
      this.setState({ isShowPopup: false });
      LogUtils.infoLog1("Navigate To", this.state.isPopupNavigate);

      if (this.state.isPopupNavigate !== '') {
        this.setState({ isPopupOpened: 1 });

        if (this.state.isPopupNavigate === 'p') {
          if (this.state.isPopupNavigateId && this.state.isPopupNavigateId > 0) {
            //Program Details Page
            Actions.traineePlanNew({ pId: this.state.isPopupNavigateId });
          } else {
            //Program Home Page
            Actions.traineeWorkoutsHome();
          }
        } else if (this.state.isPopupNavigate === 'c') {
          if (this.state.isPopupNavigateId && this.state.isPopupNavigateId > 0) {
            // Actions.accChallenge({ cid: this.state.isPopupNavigateId });
            Actions.traChallenges({ cid: this.state.isPopupNavigateId });
          } else {
            //Challenge Home Page
            Actions.traChallenges()
          }

        } else if (this.state.isPopupNavigate === 'd') {
          if (this.state.isPopupNavigateId && this.state.isPopupNavigateId > 0) {

          } else {
            //Diet Home Page
            Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })
          }
        } else if (this.state.isPopupNavigate === 'wv') {
          if (this.state.web_url != '') {
            Actions.webview({ url: this.state.web_url });
          }

        } else if (this.state.isPopupNavigate === 'vp') {
          if (this.state.isPopupNavigateId && this.state.isPopupNavigateId > 0) {

          } else {
            //View plans
            Actions.traAllBuyPlans1({ screen: "Popup update" });
          }

        } else if (this.state.isPopupNavigate === 'ewl') {
          if (this.state.web_url != '') {
            Linking.openURL(this.state.web_url).catch((err) => console.error('An error occurred', err));
          }

        } else if (this.state.isPopupNavigate === 'fc') {
          this.bottompopup.open();
        }
      } else {
        this.setState({ isPopupOpened: 0 });
      }
    } catch (error) {
      console.log(error)
    }
  }

  async popupCancel() {
    // if (this.state.thumbnailUrl !== '') {
    //   this.setState({ isShowPopup: false});
    //   setTimeout(() => { Actions.newvideoplay({ videoURL: this.state.isPopupImage }) }, 100);
    // }else {
    this.setState({ isShowPopup: false, isPopupOpened: 0 });
    this.alertServiceUpdate();
    // }
  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!' || this.state.isLogoutApp) {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }

    this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });

    if (this.state.isSaveWater) {
      this.state.resObj.glass_drink = this.state.resObj.glass_drink + 1;

      let waterDrank = parseFloat(this.state.resObj.glass_drink * 0.25);
      this.state.resObj.water_drank = `${waterDrank} Ltrs`;

      var myloop = [];
      for (let i = 0; i < 8; i++) {
        if (i < this.state.resObj.glass_drink) {
          myloop.push({ id: i + 1, isFill: true });
        }
        else {
          myloop.push({ id: i + 1, isFill: false });
        }
      }
      this.setState({ glsArray: myloop })

      this.setState({ isSaveWater: false, isAlert: false, alertMsg: '', alertTitle: '' });

    }

    if (this.state.isAcceptChallenge) {
      this.setState({ isAcceptChallenge: false });
      this.getHomepageDet();
      // this.getProducts();
      // this.getHomepTrendingWorkouts();
    }

    if (this.state.isSaveApntFeedback) {
      this.setState({ isSaveApntFeedback: false });
      this.getHomepageDet();
      // this.getProducts();
      // this.getHomepTrendingWorkouts();
    }
  }

  async onSuccess() {
    if (this.state.sucMsg === 'Do you want to logout ?') {
      try {
        this.logOut('token');
      } catch (error) {
      }
    }
    else {
      // BackHandler.exitApp();
      RNExitApp.exitApp();
    }
    this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
  }

  async onDeny() {
    this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
  }

  async versionUpdateLog(is_upd_type){
    try{
      let token = await AsyncStorage.getItem('token');
      let version = DeviceInfo.getVersion();
      let os_id_type = Platform.OS === 'android' ? 1 : 2
      fetch(`${BASE_URL}/trainee/appvrsnlog`,{
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            os_id:os_id_type,
            vrsn_no: version,
            is_upd: is_upd_type,
          })
        },
      ).then(processResponse).then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('update log statusCode', statusCode);
          LogUtils.infoLog1('update log data', data);
          if (statusCode >= 200 && statusCode <= 300) {

          } else {

          }
        })
        .catch(function (error) {
        });

    }catch(error){
      console.log(error);
    }
  }

  async onUpdate() {
    this.versionUpdateLog(1);
    this.setState({ isAppUpdatePop: false });
    if (Platform.OS === 'android') {
      Linking.openURL("market://details?id=com.transformfitness");
    }
    else if (Platform.OS === 'ios') {
      Linking.openURL('itms-apps://itunes.apple.com/us/app/apple-store/id1545532803?mt=8')
    }
  }

  async onCancel() {
    this.setState({ isAppUpdatePop: false });
    this.versionUpdateLog(0);
    if (this.state.versionResObj.is_optional === 1) {
      // BackHandler.exitApp();
      RNExitApp.exitApp();
    }
  }

  async logOut(key) {
    try {
      // await AsyncStorage.removeItem(key).then(() =>
      //   Actions.splash({ type: 'reset' }),
      // );
      // firebase.analytics().resetAnalyticsData();
      await AsyncStorage.clear().then(() =>
        Actions.splash({ type: 'reset' }),
      );
    } catch (error) {
      // LogUtils.infoLog(error.message);
    }
  }
  //End popup

  // Start RB Sheeet region (bottomMultiPopup)
  renderCovidDietProcess() {
    try {
      if (this.state.covidDietProcess === 1) {

        return (

          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

            {/* <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}> */}

            {/* <Text style={{
                  width: wp('85%'),
                  fontSize: 14,
                  fontWeight: '500',
                  letterSpacing: 0.2,
                  fontFamily: 'Rubik-Medium',
                  textAlign: 'center',
                  alignSelf: 'center',
                  color: '#282c37',
                  lineHeight: 18,
                  marginBottom: 10
                }}>{'Please enter your email ID to get your free COVID Diet plan'}</Text> */}

            <Text style={{
              fontSize: 12,
              fontWeight: '400',
              letterSpacing: 0.2,
              fontFamily: 'Rubik-Regular',
              textAlign: 'center',
              alignSelf: 'center',
              color: '#6d819c',
              lineHeight: 18,
              marginTop: 5,
              marginLeft: 5,
              marginRight: 5,
              marginBottom: 25,
            }}> {this.state.covid_imp.breath_img_hdr}</Text>

            <View style={styles.containerMobileStyle1}>
              <TextInput
                ref={input => { this.emailTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
                maxLength={100}
                value={this.state.email}
                returnKeyType='done'
                onChangeText={text => this.setState({ email: text })}
              />
            </View>

            {this.state.popuploading
              ?
              (
                <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
              )
              :
              (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                  <TouchableOpacity onPress={() => {
                    this.validateVerifyEmail(this.state.email);
                    // this.setState({covidDietProcess : 2});
                  }}>
                    <Text style={styles.textSave}>Continue</Text>
                  </TouchableOpacity>
                </LinearGradient>
              )
            }
            {/* </Animated.View> */}
          </View>
        );
      } else if (this.state.covidDietProcess === 2) {
        return (
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: this.state.covidPlanSucObj.img_url }}
              style={{
                width: 150,
                height: 150,
                alignSelf: 'center',
              }}
            />
            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>
              <Text style={styles.freeTrialPopupTitle}>{this.state.covidPlanSucObj.title}</Text>
              <Text style={styles.freeTrialPopupText}>{this.state.covidPlanSucObj.message}</Text>
              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                <TouchableOpacity onPress={() => {
                  this.covidbottompopup.close();
                }}>
                  <Text style={styles.textSave}>Ok. Thanks</Text>
                </TouchableOpacity>
              </LinearGradient>
            </Animated.View>
          </View>
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderFreeTrailProcess() {
    try{
      if (this.state.freeTrailProcess === 1) {
        return (
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
  
            {this.state.resObj.show_free_call === 1 ?
              (
                <View>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.fcc_image }}
                    style={{
                      width: 150,
                      height: 150,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.fcc_title}</Text>
                  <Text style={styles.freeTrialPopupText}>{this.state.resObj.fcc_text}</Text>
                </View>
              )
              :
              (
                <View>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.ftc_image }}
                    style={{
                      width: 150,
                      height: 150,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftc_title}</Text>
                  <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftc_text}</Text>
                </View>
              )
            }
  
            {/* <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: this.state.resObj.ftc_image }}
              style={{
                width: 150,
                height: 150,
                alignSelf: 'center',
              }}
            />
  
            <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftc_title}</Text>
  
            <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftc_text}</Text> */}
  
            {this.state.popuploading
              ?
              (
                <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
              )
              :
              (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                  <TouchableOpacity onPress={() => {
  
                    if (this.state.resObj.email_verified === 1) {
  
                      if (this.state.resObj.show_free_call === 1 || this.state.resObj.show_free_trial === 1) {
                        this.setState({ popuploading: true, onConfirmClick: true });
                        this.saveFreeTrial();
                      }
                      else {
  
                      }
                    } else {
                      this._toggleSubview();
                      this.setState({ freeTrailProcess: 2 });
                    }
                  }}>
                    <Text style={styles.textSave}>Confirm</Text>
                  </TouchableOpacity>
                </LinearGradient>
              )
            }
          </View>
        );
      } else if (this.state.freeTrailProcess === 2) {
  
        return (
  
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
  
            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>
  
              <Text style={{
                width: wp('85%'),
                fontSize: 14,
                fontWeight: '500',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Medium',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#282c37',
                lineHeight: 18,
                marginBottom: 10
              }}>{this.state.resObj.ev_title}</Text>
  
              <Text style={{
                fontSize: 12,
                fontWeight: '400',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Regular',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#6d819c',
                lineHeight: 18,
                marginTop: 5,
                marginLeft: 5,
                marginRight: 5,
                marginBottom: 25,
              }}> {this.state.resObj.ev_descp} </Text>
  
              <View style={styles.containerMobileStyle1}>
                <TextInput
                  ref={input => { this.emailTextInput = input; }}
                  style={styles.textInputStyle}
                  placeholder="Enter Email"
                  placeholderTextColor="grey"
                  keyboardType="email-address"
                  maxLength={100}
                  value={this.state.email}
                  returnKeyType='done'
                  onChangeText={text => this.setState({ email: text })}
                />
              </View>
  
              {this.state.popuploading
                ?
                (
                  <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                )
                :
                (
  
                  <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                    <TouchableOpacity onPress={() => {
  
                      this.validateEmail(this.state.email);
  
                      // setTimeout(() => {
                      //   this._toggleSubview();
                      //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                      // }, 5000)
  
                    }}>
                      <Text style={styles.textSave}>Continue</Text>
                    </TouchableOpacity>
                  </LinearGradient>
                )
              }
            </Animated.View>
          </View>
        );
      } else if (this.state.freeTrailProcess === 3) {
        return (
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
  
            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>
  
              <Text style={{
                width: wp('85%'),
                fontSize: 14,
                fontWeight: '500',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Medium',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#282c37',
                lineHeight: 18,
                marginBottom: 10
              }}> {this.state.emailSuccessTitle} </Text>
  
              <Text style={{
                fontSize: 12,
                fontWeight: '400',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Regular',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#6d819c',
                lineHeight: 18,
                marginTop: 5,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 25,
              }}> {this.state.emailSuccessDesc} </Text>
  
              <View style={styles.containerMobileStyle1}>
                <TextInput
                  ref={input => { this.emailTextInput = input; }}
                  style={styles.textInputStyle}
                  placeholder="Enter code"
                  placeholderTextColor="grey"
                  keyboardType="number-pad"
                  maxLength={4}
                  value={this.state.emailOtp}
                  returnKeyType='done'
                  onChangeText={text => this.setState({ emailOtp: text })}
                />
              </View>
  
              {this.state.popuploading
                ?
                (
                  <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                )
                :
                (
  
                  <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                    <TouchableOpacity onPress={() => {
  
                      if (this.state.emailOtp) {
                        this.setState({ popuploading: true, onConfirmClick: true });
                        this.saveFreeTrial();
                      } else {
                        this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                      }
                      // setTimeout(() => {
                      //   this._toggleSubview();
                      //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                      // }, 5000)
                    }}>
                      <Text style={styles.textSave}>Continue</Text>
                    </TouchableOpacity>
                  </LinearGradient>
                )
              }
            </Animated.View>
          </View>
        );
      } else if (this.state.freeTrailProcess === 4) {
  
        return (
  
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
  
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: this.state.freeTrialObj.image }}
              style={{
                width: 150,
                height: 150,
                alignSelf: 'center',
              }}
            />
  
            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>
              <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>
              <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
  
  
              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                <TouchableOpacity onPress={() => {
                  this.bottompopup.close();
                  // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });
                  if (this.state.freeTrialObj.qst_type !== 0) {
                    if (this.state.freeTrialObj.qst_type === 1) {
                      if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                        Actions.aidietques();
                      } else {
                        Actions.dietHome1({ isRefresh: 'yes' });
                      }
                    } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                      if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                      } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                      } else {
                        Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                      }
                    }
                  }
  
                }}>
                  <Text style={styles.textSave}>Ok. Thanks</Text>
                </TouchableOpacity>
  
              </LinearGradient>
            </Animated.View>
          </View>
        );
      }
    }catch(error){
      console.log(error);
    }
  }

  async reminderlogUpdate() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/reminderlog`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('Reminder statusCode', statusCode);
          LogUtils.infoLog1('Reminder data', data);

          if (statusCode >= 200 && statusCode <= 300) {
          } else {
          }
        })
        .catch(function (error) {
        });
    } catch (error) {
      console.log(error)
    }
  }

  async validateVerifyEmail(email) {
    try {

      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

      if (!email) {
        this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
      }
      else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
        this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
      }
      else {
        this.setState({ popuploading: true });
        this.getcoviddietplan();
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getcoviddietplan() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/getcoviddietplan`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            email: this.state.email,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('covid statusCode', statusCode);
          LogUtils.infoLog1('covid data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this._toggleSubview();
            this.setState({ popuploading: false, covidDietProcess: 2, covidPlanSucObj: data, covid_diet_sent: 1 });

          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            } else {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
        });
    } catch (error) {
      console.log(error)
    }
  }

  async validateEmail(email) {
    try{
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  
      if (!email) {
        this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
      }
      else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
        this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
      }
      else {
        this.setState({ popuploading: true, onConfirmClick: true });
        this.verifyEmailOtp();
      }
    }catch(error){
      console.log(error);
    }
  }

  async verifyEmailOtp() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/verifyemailotp`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            email: this.state.email,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this._toggleSubview();
            this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });

          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            } else {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
        });
    } catch (error) {
      console.log(error)
    }
  }

  async saveFreeTrial() {
    try {
      let token = await AsyncStorage.getItem('token');
      let urlType = '';
      if (this.state.resObj.show_free_call === 1) {
        urlType = 'save_freetrial2';
      }
      else if (this.state.resObj.show_free_trial === 1) {
        urlType = 'save_freetrial';
      }

      LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

      fetch(
        `${BASE_URL}/trainee/${urlType}`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            otp: this.state.emailOtp,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this._toggleSubview();
            this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
            if (this.state.resObj.show_free_call === 1) {
              LogUtils.appsFlyerEventLog('freeconsultationstarted', {
                desc: 'Free Consultation Started',
              });
            }
            else {
              LogUtils.appsFlyerEventLog('freetrialstarted', {
                desc: 'Free Trial Started',
              });
            }
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            } else {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
        });
    } catch (error) {
      console.log(error)
    }
  }

  _toggleSubview() {
    Animated.timing(
      this.state.bounceValue,
      {
        toValue: 0,
        duration: 1000
      }
    ).start();
  }

  onFeedPressed() {
    LogUtils.firebaseEventLog('click', {
      p_id: 202,
      p_category: 'Home',
      p_name: 'Home->Feed',
    });
    Actions.wall();
  }

  //RBEnd region

  //Dialog
  async saveAppointmentFeedback() {
    try {
      this.setState({ loading: true });
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/saveappcallrating`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            ua_id: this.state.resObj.consult_rating.ua_id,
            rating: this.state.rating,
            comments: this.state.comments,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this.setState({
              loading: false,
              isSaveApntFeedback: true,
              isAlert: true,
              alertMsg: data.message,
              alertTitle: data.title,
              rating: 3,
              comments: ''
            });
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
            } else {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
            }
          }
        }).catch(function (error) {
          this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
        });
    } catch (error) {
      this.setState({ loading: false });
      console.log(error)
    }
  }

  ratingCompleted(rating) {
    this.setState({ rating: rating })
    switch (rating) {
      case 1:
        this.setState({ ratingText: 'Bad experience !' })
        break;
      case 2:
        this.setState({ ratingText: 'Not that great !' })
        break;
      case 3:
        this.setState({ ratingText: 'It was good !' })
        break;
      case 4:
        this.setState({ ratingText: 'Had a great experience !' })
        break;
      case 5:
        this.setState({ ratingText: 'Absolutely superb !' })
        break;
      default:
        this.setState({ ratingText: 'Give rating !' })
        break
    }
  }
  //End Dialog

  render() {
    return (
      <View style={styles.mainContainer}>
        <Slidemenu
          active={this.state.open}
          isShowAppt={this.state.isShowAppt}
          isShowDiet={this.state.isShowDiet}
          freeTrialObj={this.state.freeTrialInitialObj}
          afterAnimation={() => this.afterAnimation()}
          handleChangeMenu={(d) => this.handleChangeMenu(d)}
          handleToggleMenu={() => this.handleToggleMenu()}>
          <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>

            <View style={styles.containerStyle}>
              <Loader loading={this.state.loading} />
              {this.renderProgressBar()}
              <NoInternet
                image={require('../res/img_nointernet.png')}
                loading={this.state.isInternet}
                onRetry={this.onRetry.bind(this)} />

              <ScrollView
                ref={ref => (this.scrollView = ref)}
                nestedScrollEnabled={true}
                contentContainerStyle={{ paddingBottom: hp('3%') }}
                style={{ marginBottom: hp('6%'), position: 'relative', zIndex: -1 }}
                refreshControl={
                  <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.onRefresh()} />}>
                <TouchableWithoutFeedback onPress={() => this.closeMenu()}>
                  <View style={{ flexDirection: 'column', alignSelf: 'flex-start', }}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_bg_homeblue.png')}
                      resizeMode="stretch"
                      style={{
                        width: wp('100%'),
                        height: hp('35%'),
                        position: 'absolute',
                        zIndex: 1,
                      }}
                    />

                    <View style={{ flexDirection: 'column', alignSelf: 'flex-start', zIndex: 10, }}>

                      <View style={styles.containerTop}>
                        <View style={styles.profileView}>
                          <TouchableOpacity
                            onPress={() =>
                              this.handleToggleMenu()
                            } >
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={require('../res/ic_menu.png')}
                              style={styles.backImageStyle}
                            />
                          </TouchableOpacity>
                          <View style={{ flex: 1 }}></View>
                          {/* <CopilotStep text="Access your profile anytime here" order={1} name="openApp">
                        <WalkthroughableView> */}
                          <TouchableOpacity onPress={() => this.gotoProfile()}>
                            {this.renderProfileImg()}
                          </TouchableOpacity>
                          {/* </WalkthroughableView>
                      </CopilotStep> */}

                        </View>

                      </View>

                      <View style={{ marginLeft: wp('3%'), marginRight: wp('3%'), marginBottom: wp('6%') }}>
                        <Text style={styles.textHello}>{this.renderWelcomeMsg()}</Text>
                        <Text style={styles.textHello}>{this.state.resObj.user_name}</Text>
                      </View>


                      {/* goal and weight details */}
                      <View style={styles.goalBg}>
                        <View style={styles.goalDet}>
                          <Text style={styles.textTitle}>Your goal</Text>
                          <Text style={styles.textValue}>{this.state.resObj.goal_name}</Text>
                        </View>
                        <View style={{ width: 1, backgroundColor: '#e9e9e9', }}></View>
                        <View style={styles.goalDet}>
                          <Text style={styles.textTitle}>Latest weight</Text>
                          <TouchableOpacity onPress={() => {
                            LogUtils.firebaseEventLog('click', {
                              p_id: 206,
                              p_category: 'Home',
                              p_name: 'Home->Latest Weight',
                            });
                            Actions.traHomeWeight();
                          }} style={{ flexDirection: 'row' }}>
                            <Text style={styles.textWeight}>{this.state.resObj.user_weight}</Text>
                            <Text style={styles.textKgs}> kg </Text>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={require('../res/img_goal_level.png')}
                              style={styles.imgGoal}
                            />
                          </TouchableOpacity>

                        </View>
                      </View>

                      {/* Program Schedule */}
                      {/* {this.renderWoroutAddButton()} */}
                      {this.renderWorkoutsList()}

                      {/* Recommended Programs */}
                      {/* <CopilotStep text="Please enroll into our fitness programs to achieve your fitness goals"  order={1} name="openApp"> */}
                      <WalkthroughableView style={styles.workOutBgNew}>
                        {this.renderRecommendedForYou()}
                      </WalkthroughableView>
                      {/* </CopilotStep> */}

                      {/* workout plan details */}
                      {this.renderTodayWorouts()}

                      {/* best trainers */}
                      {this.renderBestTrainersList()}

                      {/* Trending workouts */}
                      {this.renderTrendingWorkouts()}

                      {/* Mental Fitness details */}
                      {this.renderMentalFitnessList()}

                      {/* Discover  */}
                      {this.renderDiscover()}

                      {/* Diet Plan */}
                      {/* <View style={{ height: 15 }}></View> */}
                      {/* <CopilotStep text="You can access your diet plan here. You can have a call with our dietician anytime to personalize it according to your preferences." order={2} name="secondText">
                      <WalkthroughableView style={styles.workOutBgNew}>
                        {this.renderDietHeader()}
                        {/* {this.renderDietPlan()} */}
                      {/* </WalkthroughableView>
                    </CopilotStep> */}

                      {/* Home Banners */}
                      {this.renderHomeBanners()}

                      {/* Covid Diet Banner */}
                      {this.renderCovidBanner()}

                      {/* Covid Breathing Technique Banner */}
                      {this.renderCovidBreathingTechniqueBanner()}

                      {/* Macros */}
                      {/* <Macros macros={this.state.resObj} /> */}

                      {/* Diet Portion Images */}
                      {this.renderDietPortionImages()}

                      {/* challenges plan details */}
                      <View style={styles.workOutBgNew}>
                        <Challenges trChnlg={this.state.resObj} loading={this.state.loading} refreshing={this.state.refreshing} open={this.state.open} closeChallenge={this.closeChallenge.bind(this)} />
                      </View>

                      {/* appointment details */}
                      <View style={{ height: 15 }}></View>
                      <View style={styles.workOutBgNew}>
                        <NewAppointments appointments={this.state.resObj} isShowAppt={this.state.isShowAppt} freeTrialInitial={this.state.freeTrialInitialObj} />
                      </View>

                      {/* Fitness products details */}
                      <View style={styles.workOutBgNew}>
                        <FitnessProducts products={this.state.products} shoopingURL={this.state.resObj.shopping_url} />
                      </View>

                    </View>
                  </View>
                </TouchableWithoutFeedback>

                <PopupDialog
                  visible={this.state.isShowPopup}
                  image={this.state.isPopupImage}
                  imageArr={this.state.poppupImageArr}
                  thumbnail={this.state.thumbnailUrl}
                  mediaType={this.state.mediaType}
                  onAccept={this.popupUpdate.bind(this)}
                  onDecline={this.popupCancel.bind(this)}
                  no={this.state.isPopupCancel}
                  yes={this.state.isPopupOk} 
                  />
              </ScrollView>

              <HomeDialog
                visible={this.state.isAlert}
                title={this.state.alertTitle}
                desc={this.state.alertMsg}
                onAccept={this.onAccept.bind(this)}
                no=''
                yes='Ok' />

              <HomeDialog
                visible={this.state.isSuccess}
                title={this.state.titMsg}
                desc={this.state.sucMsg}
                onAccept={this.onSuccess.bind(this)}
                onDecline={this.onDeny.bind(this)}
                no='No'
                yes='Yes' />

              <VersionCheckInfo
                visible={this.state.isAppUpdatePop}
                title={this.state.isAppUpdateTit}
                desc={this.state.isAppUpdateMsg}
                onAccept={this.onUpdate.bind(this)}
                onDecline={this.onCancel.bind(this)}
                no={this.state.isAppCancel}
                yes={this.state.isAppUpdate}
                image={require('../res/ic_app_download.png')} 
              />

              {/* <Dialog
                width={0.7}
                // height={0.5}
                visible={this.state.isAddFoodImgPop}
                onDismiss={() => {
                  this.setState({ isAddFoodImgPop: false });
                }}
                onTouchOutside={() => {
                  this.setState({ isAddFoodImgPop: false });
                }}>
                <DialogContent
                  style={{
                    backgroundColor: '#ffffff',
                  }}>
                  <View style={{ flexDirection: 'column', padding: 15 }}>
                    <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                      <View style={{ marginTop: 10, }}></View>
                      <Text style={styles.textAddFoodImgTit}>Add Food Photo</Text>

                      <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                        <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                          <TouchableWithoutFeedback
                            onPress={() => {
                              this.setState({ isAddFoodImgPop: false });
                              this.captureImage();
                            }}>
                            <View style={styles.containerMaleStyle2}>

                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                source={require('../res/photo.png')}>
                              </Image>

                            </View>
                          </TouchableWithoutFeedback>
                          <Text style={styles.desc}>Camera</Text>
                        </View>
                        <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                          <TouchableWithoutFeedback
                            onPress={() => {
                              this.setState({ isAddFoodImgPop: false });
                              if (Platform.OS === 'android') {
                                this.pickImagesFromGallery();
                              } else {
                                this.pickImagesFromGalleryIOS();
                              }
                            }}>
                            <View style={styles.containerMaleStyle1}>

                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                source={require('../res/transformation.png')}>
                              </Image>

                            </View>
                          </TouchableWithoutFeedback>
                          <Text style={styles.desc}>Gallery</Text>
                        </View>

                      </View>
                    </View>
                  </View>
                </DialogContent>
              </Dialog> */}

              {/* Add Food Photos */}
              <Modal
                transparent={true}
                animationType={'none'}
                visible={this.state.isAddFoodImgPop}
                onRequestClose={() => {
                  //console.log('close modal');
                }}>
                <View style={{
                  backgroundColor: '#ffffff',
                  // position: 'relative',
                  flex: 1,
                  justifyContent: 'center',
                }}>
                  <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.75)', justifyContent: 'center' }}>
                    <View style={{ width: wp('80%'), alignSelf: 'center', flexDirection: 'column', padding: 15, borderRadius: 15, backgroundColor: '#ffffff' }}>
                      <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                        <View style={{ marginTop: 10, }}></View>
                        <Text style={styles.textAddFoodImgTit}>Add Food Photo</Text>

                        <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                          <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                            <TouchableWithoutFeedback
                              onPress={() => {
                                this.setState({ isAddFoodImgPop: false });
                                setTimeout(() => { this.captureImage() }, 100);

                              }}>
                              <View style={styles.containerMaleStyle2}>

                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                  source={require('../res/photo.png')}>
                                </Image>

                              </View>
                            </TouchableWithoutFeedback>
                            <Text style={styles.desc}>Camera</Text>
                          </View>
                          <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                            <TouchableWithoutFeedback
                              onPress={() => {
                                this.setState({ isAddFoodImgPop: false });
                                if (Platform.OS === 'android') {
                                  this.pickImagesFromGallery();
                                } else {
                                  setTimeout(() => { this.pickImagesFromGalleryIOS() }, 100);
                                }
                              }}>
                              <View style={styles.containerMaleStyle1}>

                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                  source={require('../res/transformation.png')}>
                                </Image>

                              </View>
                            </TouchableWithoutFeedback>
                            <Text style={styles.desc}>Gallery</Text>
                          </View>

                        </View>
                      </View>

                      <TouchableOpacity style={{
                        position: 'absolute',
                        right: 10,
                        top: 10,
                        // padding: 10
                      }}
                        onPress={() => {
                          this.setState({ isAddFoodImgPop: false });
                        }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{
                            width: 19,
                            height: 19,
                            alignSelf: 'center',
                            tintColor: 'black',
                          }}
                          source={require('../res/ic_cross_black.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
              {/* End Add Food Photos */}

              <Dialog
                onDismiss={() => {
                  this.setState({ showAppointmentFeedback: false });
                }}
                onTouchOutside={() => {
                  this.setState({ showAppointmentFeedback: false });
                }}
                width={0.8}
                visible={this.state.showAppointmentFeedback}
              >
                <DialogContent
                  style={{
                    backgroundColor: '#ffffff'
                  }}>
                  <View style={{ flexDirection: 'column', }}>
                    <View style={{ flexDirection: 'column', padding: 15 }}>

                      <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                        {/* <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/img_feedback.png')}
                          style={styles.imgpopup}
                        /> */}
                        <Text style={styles.fbTitle}>{this.state.feedbackTitle}</Text>
                        <Text style={styles.fbDesc}>{this.state.feedbackDesc}</Text>
                        <AirbnbRating
                          // showRating
                          type='star'
                          ratingCount={5}
                          reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                          imageSize={20}
                          startingValue={this.state.rating}
                          readonly={false}
                          isDisabled={false}
                          reviewSize={20}
                          defaultRating={5}
                          onFinishRating={this.ratingCompleted.bind(this)}
                          style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                        />

                        <View style={styles.containerMobileStyle}>
                          <TextInput
                            ref={input => { this.addressTextInput = input; }}
                            style={styles.textInputStyle1}
                            placeholder='Comments'
                            multiline={true}
                            maxLength={300}
                            placeholderTextColor='grey'
                            value={this.state.comments}
                            autoCapitalize='words'
                            returnKeyType='next'
                            onSubmitEditing={() => {
                            }}
                            onChangeText={text => {
                              if (text) {
                                this.setState({ comments: text });
                              }
                              else {
                                this.setState({ comments: '' });
                              }
                            }}
                          />
                        </View>

                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                          <TouchableOpacity onPress={() => {
                            this.setState({ showAppointmentFeedback: false });
                            this.saveAppointmentFeedback();
                          }}>
                            <Text style={styles.textSave}>Submit</Text>
                          </TouchableOpacity>
                        </LinearGradient>

                      </View>
                    </View>
                  </View>
                </DialogContent>
              </Dialog>

              {/* Srart Show Added Food Photos */}
              <Modal
                transparent={true}
                animationType={'none'}
                visible={this.state.isShowInfoPopup}
                onRequestClose={() => {
                  //('close modal');
                }}>
                <View style={{
                  backgroundColor: '#ffffff',
                  // position: 'relative',
                  flex: 1,
                  justifyContent: 'center',
                }}>

                  <View style={{ flex: 1, backgroundColor: '#000000', justifyContent: 'center' }}>
                    <AppIntroSlider
                      slides={this.state.foodPhotos}
                      // onDone={this._onDone}
                      showSkipButton={false}
                      showDoneButton={false}
                      showNextButton={false}
                      dotStyle={{ backgroundColor: '#ffffff' }}
                      activeDotStyle={{ backgroundColor: '#8c52ff' }}
                      renderItem={({ item }) => {
                        return <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={{ uri: item.img }}
                          style={{ width: wp('90%'), height: hp('80%'), flex: 1, alignSelf: 'center', resizeMode: 'contain' }}
                        />
                        // <FastImage
                        //   style={{ width: wp('90%'), height: hp('80%'), flex: 1, alignSelf: 'center' }}
                        //   source={{
                        //     uri: item.img,
                        //     headers: { Authorization: '12345' },
                        //     priority: FastImage.priority.high,
                        //   }}
                        //   resizeMode={FastImage.resizeMode.contain}
                        // />
                      }}
                    />
                    <TouchableOpacity style={styles.viewTop}
                      onPress={() => {
                        this.setState({ foodPhotos: [] });
                        this.setState({ isShowInfoPopup: false, selItem: '' });
                      }
                      }>
                      <Image
                        source={require('../res/ic_back.png')}
                        style={styles.popBackImageStyle}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
              {/* End Show Added Food Photos */}

              {/* Start RB Sheets */}
              {/* Covid */}
              <RBSheet
                ref={ref => {
                  this.covidbottompopup = ref;
                }}
                height={covidPopupHeight}
                closeOnDragDown={false}
                closeOnPressMask={false}
                // closeOnPressBack={this.covidbottompopup.close()}
                onClose={() => {
                  this.setState({ covidDietProcess: 1 })
                }}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10
                  }
                }}
              >
                <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                  onPress={() => { this.covidbottompopup.close() }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{
                      width: 19,
                      height: 19,
                      alignSelf: 'flex-start',
                      tintColor: 'black',
                    }}
                    source={require('../res/ic_cross_black.png')}
                  />
                </TouchableOpacity>

                {/* {this.state.onConfirmClick
                  ?
                  (
                    <View></View>
                  )
                  :
                  (
                    <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                      onPress={() => { this.covidbottompopup.close() }}>
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={{
                          width: 19,
                          height: 19,
                          alignSelf: 'flex-start',
                          tintColor: 'black',
                        }}
                        source={require('../res/ic_cross_black.png')}
                      />
                    </TouchableOpacity>

                  )} */}

                {this.renderCovidDietProcess()}


                <HomeDialog
                  visible={this.state.isAlert}
                  title='Alert'
                  desc={this.state.alertMsg}
                  onAccept={this.onAccept.bind(this)}
                  no=''
                  yes='Ok' />

              </RBSheet>

              {/* Free trail process */}
              <RBSheet
                ref={ref => {
                  this.bottompopup = ref;
                }}
                height={popupHeight}
                closeOnDragDown={false}
                closeOnPressMask={false}
                closeOnPressBack={!this.state.onConfirmClick}
                onClose={() => {
                  this.setState({ freeTrailProcess: 1 })
                }}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10
                  }
                }}
              >
                {this.state.onConfirmClick
                  ?
                  (
                    <View />
                  )
                  :
                  (
                    <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                      onPress={() => { this.bottompopup.close() }}>
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={{
                          width: 19,
                          height: 19,
                          alignSelf: 'flex-start',
                          tintColor: 'black',
                        }}
                        source={require('../res/ic_cross_black.png')}
                      />
                    </TouchableOpacity>

                  )}

                {this.renderFreeTrailProcess()}

                <HomeDialog
                  visible={this.state.isAlert}
                  title='Alert'
                  desc={this.state.alertMsg}
                  onAccept={this.onAccept.bind(this)}
                  no=''
                  yes='Ok' />

              </RBSheet>

              {/* bottomMultiPopup */}
              <RBSheet
                ref={ref => {
                  this.bottomMultiPopup = ref;
                }}
                height={popupHeight}
                closeOnDragDown={false}
                closeOnPressMask={false}
                closeOnPressBack={!this.state.onConfirmClick}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10
                  }
                }}
              >
                <TouchableOpacity style={{ position: 'absolute', right: 0, marginTop: hp('2%'), marginLeft: '2%', padding: 10 }}
                  onPress={() => {
                    this.reminderlogUpdate()
                    this.bottomMultiPopup.close()
                  }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{
                      width: 19,
                      height: 19,
                      alignSelf: 'flex-start',
                      tintColor: 'black',
                    }}
                    source={require('../res/ic_cross_black.png')}
                  />
                </TouchableOpacity>

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.ftr_image }}
                    style={{
                      width: 150,
                      height: 150,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftr_title}</Text>
                  <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftr_text}</Text>
                  <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientFreeTrial1}>
                      <TouchableOpacity onPress={() => {
                        this.reminderlogUpdate();
                        this.bottomMultiPopup.close();
                      }}>
                        <Text style={styles.textFreeTrialBottomButton}>
                          Not now
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>

                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientFreeTrial2}>
                      <TouchableOpacity onPress={() => {
                        this.reminderlogUpdate();
                        this.bottomMultiPopup.close();

                        if (this.state.resObj.ftr_code === 'vp') {
                          Actions.traAllBuyPlans1({ isFreeTrial: this.state.resObj.show_free_trial, screen: "Popup" });
                        } else if (this.state.resObj.ftr_code === 'ms') {
                          Actions.myOrders({ from: 'home' });
                        } else if (this.state.resObj.ftr_code === 'diet') {
                          Actions.dietHome1();
                        } else if (this.state.resObj.ftr_code === 'apt') {
                          Actions.appointments({ plantype: this.state.resObj.plan_type, premiumuser: this.state.resObj.premium_user, freeTrial: this.state.resObj.show_free_trial, homeObj: this.state.freeTrialInitialObj })
                        }
                      }}>
                        <Text style={styles.textFreeTrialBottomButton}>
                          {this.state.resObj.ftr_btntxt}
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </View>
                </View>
              </RBSheet>

              {/* More */}
              <RBSheet
                ref={ref => {
                  this.rbMenuSheet = ref;
                }}
                height={190}
                openDuration={250}
                customStyles={{
                  container: {
                    justifyContent: 'center',
                    // alignItems: 'center',
                    alignSelf: 'center',
                    width: wp('95%'),
                    marginVertical: 10,
                    borderRadius: 10,
                  }
                }}
              >
                <View style={{ flexDirection: 'column', }}>

                  <View style={styles.vRBMenuBottom}>

                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close();
                        Actions.traFoodPhotos();
                      }}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_foodphoto.png')}
                            style={{
                              width: 23,
                              height: 23,
                              tintColor: '#2d3142',
                              alignSelf: 'flex-start',
                            }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Food{'\n'}Photos</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        LogUtils.firebaseEventLog('click', {
                          p_id: 231,
                          p_category: 'Home',
                          p_name: 'Home->ChatList',
                        });
                        Actions.chatlist();
                      }}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_bm_oneononechat.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142', }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>One on one{'\n'}Chat</Text>
                      </View>
                    </TouchableOpacity>

                    {/* phase1 */}

                    {
                      (this.state.resObj.is_paiduser === 0 || this.state.resObj.is_diet_plan === 1) ?
                        (
                          <TouchableOpacity
                            style={styles.bottomrbMenuClick}
                            onPress={() => {
                              this.rbMenuSheet.close()
                              this.onTrendWorkoutsClicked()
                            }
                            }
                          >
                            <View style={{ flexDirection: 'column', }}>
                              <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  source={require('../res/ic_workout.png')}
                                  style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                />
                              </View>
                              <Text style={styles.imgBottomMenuText}>Workouts{'\n'}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                        :
                        (<TouchableOpacity
                          style={styles.bottomrbMenuClick}
                          onPress={() => {
                            this.rbMenuSheet.close()
                            LogUtils.firebaseEventLog('click', {
                              p_id: 222,
                              p_category: 'Home',
                              p_name: 'Home->Select a Plan',
                            });
                            Actions.traAllBuyPlans1({ isFreeTrial: this.state.resObj.show_free_trial, screen: "Bottom memu view plans" });
                          }}>
                          <View style={{ flexDirection: 'column', }}>
                            {
                              this.props.trainee.traineeDetails.plans_disc_per != "" &&
                              (<View style={[styles.tabBadge, { backgroundColor: this.props.trainee.traineeDetails.home_bg_color, right: -33, borderRadius: 4, fontSize: 6 }]}>
                                <Text style={styles.tabBadgeText}>{`${this.props.trainee.traineeDetails.plans_disc_per}`}</Text>
                              </View>)
                            }
                            <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_bm_viewplans.png')}
                                style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                              />
                            </View>
                            <Text style={styles.imgBottomMenuText}>Plans</Text>
                          </View>
                        </TouchableOpacity>)
                    }

                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        this.callReferAFriend();
                      }}>

                      <View style={{ flexDirection: 'column', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_refer_new.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Refer a{'\n'}Friend</Text>
                      </View>
                    </TouchableOpacity>

                    {/* {this.state.resObj.show_diet === 1 && this.state.resObj.is_diet_plan === 1
                      ?
                      (
                        <TouchableOpacity
                          style={styles.bottomrbMenuClick}
                          onPress={() => {
                            this.rbMenuSheet.close()
                            this.onRewardsClicked()
                          }}>
                          <View style={{ flexDirection: 'column' }}>
                            {
                              this.state.resObj.activity_cnt ?
                                (<View style={styles.tabBadge}>
                                  <Text style={styles.tabBadgeText}>{`${this.state.resObj.activity_cnt}`}</Text>
                                </View>) :
                                (<View>

                                </View>)
                            }

                            <View style={styles.vBtmImgHeight}>
                              <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_rewards1.png')}
                                style={{
                                  width: 23,
                                  height: 23,
                                  alignSelf: 'center',
                                  tintColor: '#2d3142'
                                }}
                              />
                            </View>
                            <Text style={styles.imgBottomMenuText}>Rewards{'\n'}</Text>
                          </View>
                        </TouchableOpacity>
                      )
                      :
                      (
                        <View></View>
                      )
                    } */}
                  </View>

                  <View style={{ height: 15, }}></View>

                  <View style={styles.vRBMenuBottom}>
                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        LogUtils.firebaseEventLog('click', {
                          p_id: 214,
                          p_category: 'Home',
                          p_name: 'Home->TopSellers',
                        });
                        Actions.webview({ url: this.state.resObj.shopping_url });
                      }}>
                      <View style={{ flexDirection: 'column', }}>

                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_store.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Store</Text>
                      </View>
                    </TouchableOpacity>

                    {/* Appointments */}
                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        this.callAppointments()
                      }}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_bm_appointments.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Appointments</Text>
                      </View>
                    </TouchableOpacity>

                    {/* Feed */}
                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        this.onFeedPressed()
                      }}>
                      <View style={{ flexDirection: 'column', }}>

                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_bm_feed.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Feed</Text>
                      </View>
                    </TouchableOpacity>

                    {/* Macros */}
                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => {
                        this.rbMenuSheet.close()
                        LogUtils.firebaseEventLog('click', {
                          p_id: 230,
                          p_category: 'Home',
                          p_name: 'Home->PCFDetails',
                        });
                        Actions.pcfdetails({ from: 'traineeHome' });
                      }}>

                      <View style={{ flexDirection: 'column', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_macros.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Macros</Text>
                      </View>
                    </TouchableOpacity>

                    {/* modal More */}
                    <TouchableOpacity
                      style={styles.bottomrbMenuClick}
                      onPress={() => this.rbMenuSheet.close()}>
                      <View style={{ flexDirection: 'column', alignSelf: 'center', }}>
                        <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_more_new.png')}
                            style={{
                              width: 20,
                              height: 20,
                              tintColor: '#ffffff',
                              alignSelf: 'center',
                              tintColor: '#8c52ff'
                            }}
                          />
                        </View>
                        <Text style={[styles.imgBottomMenuText, { color: '#8c52ff' }]}>More</Text>
                      </View>
                    </TouchableOpacity>

                  </View>
                </View>
              </RBSheet>
              {/* End RB Sheets */}

              {/* Startbottom nav bar */}
              <View style={styles.bottom}>
                <View style={styles.bottomHome}>
                  <View style={styles.bottomViewContainer}>
                    <TouchableOpacity
                      style={styles.bottomClick}>
                      <View style={{ flexDirection: 'column' }}>
                        <View style={styles.vBtmImgHeight}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_home_black.png')}
                            style={styles.imgBottomMenu}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText}>Home</Text>
                      </View>

                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.bottomClick}
                      onPress={() => this.onProgramsClicked()}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={styles.vBtmImgHeight}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_workout_gray.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText1}>Programs</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.bottomClick}
                      onPress={() => this.onTrackersClicked()}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={styles.vBtmImgHeight}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_tracker.png')}
                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0' }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText1}>Tracker</Text>
                      </View>
                    </TouchableOpacity>

                    {this.renderDietBottomMenuIcon()}

                    <TouchableOpacity
                      style={styles.bottomClick}
                      onPress={() => this.onRewardsClicked()}>
                      <View style={{ flexDirection: 'column' }}>
                        {
                          this.state.resObj.activity_cnt ?
                            (<View style={styles.tabBadge}>
                              <Text style={styles.tabBadgeText}>{`${this.state.resObj.activity_cnt}`}</Text>
                            </View>) : (<View></View>)
                        }

                        <View style={styles.vBtmImgHeight}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_rewards.png')}
                            style={{
                              width: 20,
                              height: 20,
                              alignSelf: 'center',
                              tintColor: '#d6d9e0',
                            }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText1}>Rewards</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.bottomClick}
                      onPress={() => this.rbMenuSheet.open()}>
                      <View style={{ flexDirection: 'column', }}>
                        <View style={styles.vBtmImgHeight}>
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_more_new.png')}
                            style={{
                              width: 23,
                              height: 23,
                              tintColor: '#ffffff',
                              alignSelf: 'center',
                              tintColor: '#d6d9e0'
                            }}
                          />
                        </View>
                        <Text style={styles.imgBottomMenuText1}>More</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              {/* End bottom nav bar  */}

            </View >
          </ImageBackground>
        </Slidemenu>
      </View>
    );
  }

  onProgramsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 201,
      p_category: 'Home',
      p_name: 'Home->Programs',
    });

    if (!isEmpty(this.state.resObj)) {
      if (this.state.resObj.is_paiduser === 1) {

        if (this.state.resObj.workout_imp_confirm === 0) {
          if (this.state.resObj.plan_type === 2 || this.state.resObj.plan_type === 3) {
            Actions.traBuySuccess({ from: 'homepage', homeQType: this.state.resObj.plan_type });
          } else {
            Actions.traineeWorkoutsHome();
          }
        } else {
          Actions.traineeWorkoutsHome();
        }
      }
      else {
        Actions.traineeWorkoutsHome();
      }
    }
  }

  onWorkoutsClicked() {
    try {
      if (!isEmpty(this.state.resObj)) {
        LogUtils.firebaseEventLog('click', {
          p_id: 208,
          p_category: 'Home',
          p_name: 'Home->Programs',
        });

        if (this.state.resObj.is_paiduser === 1) {

          if (this.state.resObj.workout_imp_confirm === 0) {
            if (this.state.resObj.plan_type === 2 || this.state.resObj.plan_type === 3) {
              Actions.traBuySuccess({ from: 'homepage', homeQType: this.state.resObj.plan_type });
            } else {
              Actions.traineeWorkoutsHome();
            }
          } else {
            // if (this.state.resObj.pgm_enrol === 1) {
            //   Actions.traineePlanNew({ pId: this.state.resObj.pgm_enrolid });
            // } else {
            //   Actions.traineeWorkoutsHome();
            // }
            Actions.traineeWorkoutsHome();
          }
        }
        else {
          Actions.traineeWorkoutsHome();
        }
      }
    } catch (error) {
      console.log('Error in onWorkoutsClicked : ', error);
    }
  }

  onDietClicked() {
    try {

      LogUtils.firebaseEventLog('click', {
        p_id: 204,
        p_category: 'Home',
        p_name: 'Home->Diet',
      });

      if (!isEmpty(this.state.resObj)) {
        if (this.state.resObj.is_paiduser === 1 && this.state.resObj.plan_type != 0) {
          if (this.state.resObj.diet_imp_confirm === 1) {
            if (this.state.resObj.plan_type === 1 || this.state.resObj.plan_type === 3) {
              Actions.dietHome1();
            }
            else {
              Actions.myOrders({ from: 'home' });
            }
          }
          else {
            if (this.state.resObj.plan_type === 1 || this.state.resObj.plan_type === 3) {
              Actions.traBuySuccess({ from: 'homepage', homeQType: this.state.resObj.plan_type });
            }
            else {
              Actions.myOrders({ from: 'home' });
            }
          }
        }
        else {
          Actions.dietHome1();
        }
      }
    } catch (error) {

    }
  }

  onRewardsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 203,
      p_category: 'Home',
      p_name: 'Home->Rewards',
    });
    Actions.rewardHome();
  }

  onTrendWorkoutsClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 232,
      p_category: 'Home',
      p_name: 'Home->Workouts Home',
    });
    Actions.WorkoutsHome();
  }

  onTrackersClicked() {
    LogUtils.firebaseEventLog('click', {
      p_id: 233,
      p_category: 'Home',
      p_name: 'Home->Trackers Home',
    });
    Actions.trackershome();
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  containerStyle: {
    flexDirection: 'column',
    flex: 1,
    // zIndex: 10,
  },
  containerTop: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    marginTop: hp('2%'),
  },
  profileView: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  backImageStyle: {
    width: wp('5%'),
    height: hp('4%'),
    tintColor: '#ffffff',
    alignSelf: 'flex-start',
  },
  profileImage: {
    width: 55,
    height: 55,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 55 / 2,
    resizeMode: "cover",
    justifyContent: 'flex-end',
  },
  textHello: {
    fontSize: 25,
    fontWeight: '500',
    color: '#ffffff',
    lineHeight: 30,
    fontFamily: 'Rubik-Medium',
  },
  goalBg: {
    width: wp('95%'),
    marginLeft: wp('2.5%'),
    marginRight: wp('2.5%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 6,
    position: 'relative',
    alignSelf: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 6,
    marginTop: 3.5,
  },
  goalDet: {
    width: wp('47.5%'),
    marginLeft: 10,
    paddingTop: 15,
    paddingBottom: 15,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'flex-start',
  },
  textTitle: {
    fontSize: 11,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    lineHeight: 18,
  },
  textCertification: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
    lineHeight: 18,
  },
  imgGoal: {
    width: 50,
    height: 26,
    marginLeft: 10,
    alignSelf: 'flex-end',
  },
  textValue: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    marginTop: 15,
    color: '#282c37',
  },
  textWeight: {
    fontSize: 20,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    marginTop: 10,
    color: '#282c37',
  },
  textKgs: {
    fontFamily: 'Rubik-Medium',
    marginTop: 10,
    color: '#282c37',
    fontSize: 14,
    alignSelf: 'flex-end',
    fontWeight: '400',
    lineHeight: 24,
  },
  workOutBgNew: {
    width: wp('94%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    // backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    //position: 'relative',
    alignSelf: 'center',
  },
  textPlan: {
    lineHeight: 18,
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    flex: 1,
    alignSelf: "center",
    fontWeight: '500',
  },
  textPlan1: {
    lineHeight: 18,
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    textAlign: 'left',
    alignSelf: "center",
    fontWeight: '500',
  },
  textAdd: {
    color: '#8c52ff',//#8c52ff
    fontFamily: 'Rubik-Medium',
    alignSelf: "flex-end",
    fontSize: 11,
    fontWeight: '500',
    lineHeight: 18,
  },
  calViewDet: {
    flexDirection: 'column',
    width: wp('30%'),
    alignItems: 'center',
    padding: 15,
    justifyContent: 'center'
  },
  calTitle: {
    fontSize: 20,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    color: '#282c37',
  },
  calDesc: {
    fontSize: 11,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    textAlign: 'center',
    lineHeight: 15,
  },
  calTitleBig: {
    fontSize: 30,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    color: '#282c37',
  },
  calViewDetails: {
    width: 60,
    borderRadius: 100,
    borderColor: '#e9e9e9',
    borderStyle: 'solid',
    padding: 5,
    marginTop: -25,
    borderWidth: 1,
    backgroundColor: '#ffffff',
  },
  calDetail: {
    color: '#6d819c',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    alignSelf: 'center',
    fontWeight: '800',
  },
  nutrition: {
    flexDirection: 'column',
    width: wp('90%'),
    padding: wp('7%'),
    height: hp('19%'),
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    height: 40,
    backgroundColor: '#ffffff',
    position: 'absolute',
    zIndex: 111,
    bottom: 10,
  },
  bottomHome: {
    width: wp('100%'),
    height: hp('6%'),
    backgroundColor: '#ffffff',
  },
  bottomHome1: {
    width: wp('90%'),
    height: 40,
  },
  bottomViewContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    width: wp('100%'),
    backgroundColor: '#ffffff',
  },
  bottomViewContainer1: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    width: wp('90%'),
    marginBottom: 8,
  },
  vRBMenuBottom: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp('95%'),
    marginBottom: 10,
    marginTop: 10,
  },
  bottomClick: {
    width: wp('16.66%'),
    bottom: -10,
    alignItems: 'center',
  },
  bottomrbMenuClick: {
    width: wp('19%'),
    // width: wp('23.75%'),
    alignItems: 'center',
  },
  imgBottomMenu: {
    width: 17,
    height: 17,
    alignSelf: 'center',
  },
  imgBottomMenuText: {
    alignSelf: 'center',
    fontSize: 9,
    fontWeight: '500',
    color: '#2d3142',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  imgBottomMenuText1: {
    alignSelf: 'center',
    fontSize: 9,
    fontWeight: '500',
    color: '#d6d5dd',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },

  chaProfile1: {
    width: 23,
    height: 23,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 23 / 2,
    marginRight: 2,
    resizeMode: "cover",
    justifyContent: 'center',
  },
  chaMore: {
    flexDirection: 'row',
    backgroundColor: '#979797',
    borderRadius: 15,
    paddingLeft: 8,
    paddingTop: 4,
    marginTop: 5,
    alignSelf: 'center',
    paddingBottom: 4,
    paddingRight: 8,
  },
  chaMoreText: {
    color: '#ffffff',
    fontSize: 12,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
  },
  textNodata: {
    width: wp('90%'),
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 10,
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
  },
  textChalName: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
    marginRight: 15,
  },
  textWorkName: {
    fontSize: 14,
    width: wp('80%'),
    fontWeight: '500',
    paddingTop: 10,
    paddingRight: 10,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  viewBlueDot: {
    width: 10,
    height: 10,
    backgroundColor: '#8c52ff',
    alignSelf: 'flex-start',
    marginRight: 10,
    marginTop: 5,
    borderRadius: 5,
  },
  viewBlackDot: {
    width: 8,
    height: 8,
    backgroundColor: '#282c37',
    alignSelf: 'flex-start',
    // marginRight: 5,
    marginTop: 4,
    borderRadius: 4,
  },
  chaEnrol: {
    bottom: 0,
    right: 0,
    borderRadius: 20,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  percentageText: {
    color: 'white',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(10, screenHeight),
    fontWeight: '500',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'right',
  },
  optnText: {
    color: 'white',
    fontFamily: 'Rubik-Medium',
    fontSize: RFValue(14, screenHeight),
    fontWeight: '400',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'left',
    position: 'absolute',
  },
  bottomClick1: {
    width: wp('22%'),
    alignItems: 'center',
  },
  imgBottomMenu1: {
    width: 25,
    height: 25,
    alignSelf: 'center',
  },
  mainView: {
    flexDirection: 'column',
    marginBottom: 10,
  },
  viewPrefFoodDet: {
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'center',
  },
  txtTitProType: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
    margin: 10,
  },
  textRecTrainer: {
    fontSize: 8,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    backgroundColor: '#8c52ff',
    borderRadius: 8,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    paddingRight: 5,
    margin: 5,
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: '#ffffff',
  },
  textWorkNamePop: {
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  textPopuTitle: {
    fontSize: 11,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
    marginTop: 1,
  },
  textEqpNeed: {
    fontSize: 11,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    marginTop: 1,
  },
  fbDesc: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 5,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
  },
  fbTitle: {
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 5,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  linearGradientEnroll: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginTop: 25,
  },
  linearGradientFreeTrial: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
  },
  imgpopup: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
  textSave: {
    width: wp('40%'),
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 8,
    paddingBottom: 8,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff',
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    width: wp('75%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    paddingTop: 8,
    paddingBottom: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle1: {
    height: 100,
    flex: 1,
    fontSize: 12,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
    textAlignVertical: 'top',
  },
  tabBadge: {
    position: 'absolute',
    top: -10,
    right: -7,
    backgroundColor: '#8c52ff',
    borderRadius: 16,
    paddingHorizontal: 6,
    paddingVertical: 2,
    zIndex: 2,
  },
  tabBadgeText: {
    color: 'white',
    fontSize: 9,
    fontWeight: '600',
  },
  textPhotosInfo: {
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: '#6d819c',
    lineHeight: 18,
  },
  textAddFoodImgTit: {
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  containerMaleStyle1: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#fd9c93',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerMaleStyle2: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#b79ef7',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  desc: {
    fontSize: 12,
    // marginLeft: 20,
    // marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    marginTop: 5,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    // marginBottom: hp('2%'),
  },
  textAddFoodName: {
    fontSize: 9,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 5,
    paddingTop: 2,
    paddingBottom: 2,
    margin: 3,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
  },
  viewTop: {
    flexDirection: 'column',
    position: 'absolute',
    margin: 15,
    alignItems: 'flex-start',
    padding: 5,
    justifyContent: 'center',
    width: 34,
    height: 34,
    borderRadius: 17,
    backgroundColor: 'white',
    // zIndex: 100,
    top: 1,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 6,
  },
  popBackImageStyle: {
    width: 19,
    height: 16,
    tintColor: '#8c52ff',
    alignSelf: 'center',
  },
  txtDietSecName: {
    fontSize: 14,
    fontWeight: '500',
    marginTop: 10,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  txtDietItemName: {
    fontSize: 12,
    fontWeight: '400',
    paddingLeft: 5,
    paddingRight: 5,
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
  },
  vBestTrainer: {
    width: wp('40%'),
    // backgroundColor: '#ffffff',
    flexDirection: 'row',
    marginTop: hp('0.1%'),
    marginBottom: hp('1.1%'),
    marginRight: wp('1.3%'),
    borderColor: '#ddd',
    borderRadius: 5,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 1
  },
  imgTrainer: {
    width: wp('40%'),
    borderRadius: 5,
    height: undefined,
    resizeMode: 'contain',
    aspectRatio: 5 / 6,
  },
  textTrName: {
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#2d3142',
    textAlign: 'left',
    alignSelf: 'flex-start',
  },
  textSpecial: {
    fontSize: 10,
    fontWeight: '500',
    color: '#2d3142',
    letterSpacing: 0.23,
    // marginTop: 2,
    textAlign: 'left',
    fontFamily: 'Rubik-Regular',
    alignSelf: 'flex-start',
  },
  infoAppBlue: {
    width: 25,
    height: 25,
    alignSelf: 'center',
  },
  textClickHere: {
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#8c52ff',
    lineHeight: 18,
  },
  freeTrialPopupTitle: {
    width: wp('85%'),
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
    marginTop: 10
  },
  freeTrialPopupText: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 15,
  },
  linearGradientFreeTrial1: {
    flex: 1,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  linearGradientFreeTrial2: {
    flex: 1,
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginLeft: 10,
  },
  textFreeTrialBottomButton: {
    fontSize: 11,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 8,
    paddingBottom: 8,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff',
  },
  sourceText: {
    fontSize: 12,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#8c52ff',
    textAlign: 'center',
    lineHeight: 18,
  },
  containerMobileStyle1: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 20,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
    //overflow: 'hidden',
  },
  vBtmImgHeight: {
    height: 23,
    width: 23,
    flexDirection: 'column',
    alignSelf: 'center',
  },
  viewRecDetails: {
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    width: 150,
    alignSelf: 'flex-start',
    flexDirection: 'column',
  },
  viewTrendDetails: {
    marginRight: 5,
    marginTop: 5,
    marginBottom: 5,
    width: 250,
    alignSelf: 'flex-start',
    flexDirection: 'column',
  },
  txtRecProName: {
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: '#2d3142',
  },
  txtRecProDuration: {
    fontSize: 11,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
    textAlign: 'left',
  },
  txtRecProfitForm: {
    fontSize: 11,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
    textAlign: 'left',
  },
  vHeaderTitMore: {
    width: wp('94%'),
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 15,
  },
  disFlatList: {
    marginVertical: 5,
    alignSelf: 'center',
  },
  containerFitnessform: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginRight: wp('2%'),
    marginBottom: wp('2%')
  },
  vWorkoutTrainer: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    backgroundColor: '#697282',
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'flex-end',
    marginBottom: 3,
  },
  textWorkTrainer: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    alignSelf: 'flex-end',
    textAlign: 'center',
    fontWeight: '700',
    letterSpacing: 0.5,
  },
  textWorkOftheDay: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    backgroundColor: '#444B58',
    fontSize: 9,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'flex-end',
    textAlign: 'center',
    fontWeight: '500',
    marginBottom: 5,
  },
  textWorkSpecial: {
    color: '#ff9028',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    paddingTop: 3,
    paddingBottom: 1,
    marginRight: 10,
    alignSelf: 'flex-end',
    textAlign: 'center',
    fontWeight: '500',
    borderBottomWidth: 1,
    borderColor: '#ffffff',
    marginBottom: 15,
  },
  textFoodPhotos: {
    lineHeight: 18,
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    alignSelf: "center",
    fontWeight: '500',
  },
  textFoodPhotos1: {
    fontSize: 9,
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    textAlign: 'left',
    alignSelf: 'center'
  },
  imgVeg: {
    width: wp('27%'),
    height: 100,
    alignSelf: 'center',
    position: 'relative',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  vegText: {
    fontSize: 10,
    fontWeight: '500',
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  priceText: {
    fontSize: 11,
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Medium',
  },
  containerFoodStyle: {
    width: wp('27%'),
    marginTop: hp('1%'),
    marginBottom: hp('2%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    // borderColor: '#ddd',
    borderRadius: 5,
    position: 'relative',
    alignSelf: 'center',
    // shadowColor: "#ddd",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    //shadowOpacity: 0.25,
    //shadowRadius: 3.84,
    elevation: 4,
    marginLeft: 0.5,
    marginRight: 0.5,
  },
  videoContainer: {
    width: wp('94%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoPreview: {
    width: wp('92%'),
    height: 230,
    borderRadius: 8,
    marginTop: hp('1%'),
    // marginBottom: hp('0.5%'),
    aspectRatio: 16 / 9,
  },
  traineerDesc: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 9,
    paddingTop: 1,
    paddingBottom: 1,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 15,
    marginLeft: 2,
    backgroundColor: '#8c52ff',
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 18,
  },
  imageContainer: {
    width: wp('94%'),
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});

const mapStateToProps = state => {
  const trainee = state.traineeFood
  return { trainee };
};

export default copilot({
  animated: true, // Can be true or false
  overlay: 'svg', // Can be either view or svg
  verticalOffset: 30,
  // arrowColor: '#FF00FF',
  // tooltipStyle: {
  //   width: WIDTH,
  //   maxWidth: WIDTH,
  //   top: 100,
  // },
  // tooltipComponent: TooltipComponent,
  // stepNumberComponent: StepNumberComponent
})(connect(mapStateToProps,
  { getProductsAction, getHomepTrendingWorkoutsAction, getHomepageDetAction,GetGoalsubctgAction })(TraineeHome));
