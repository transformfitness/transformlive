import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
    Animated,
    ActivityIndicator,
    TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { NoInternet, HomeDialog, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
    DialogContent,
    DialogFooter,
    DialogButton,
} from 'react-native-popup-dialog';
import { Calendar } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
LocaleConfig.locales['en'] = {
    monthNames: ['January', 'February ', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'en';
import moment from "moment";
import DeviceInfo from 'react-native-device-info';

import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';

import RBSheet from "react-native-raw-bottom-sheet";
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getToDate() {
    let now = new Date()
    let next14days = new Date(now.setDate(now.getDate() + 13))

    return next14days;
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


class Appointments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            isInternet: false,
            alertMsg: '',
            noDataMsg: '',
            arrAppointments: '',
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            selAppId: 0,
            visible: false,
            day: '',
            month: '',
            year: '',
            confirmPop: false,
            dateConString: '',
            arrTrainerSlots: [],
            selTrID: 0,
            selSlotObj: {},
            selTraImg: '',
            msgSelectDate: 'Please select date...',
            selTasId: 0,
            selUaId: 0,
            buttonText: 'Book Appointment',
            arrAllPlans: [],

            bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
            confirmSuccess: false,
            freeTrialObj: {},
            buttonState: '',
            freeTrialSuccessTitle: '',
            freeTrialSuccessMessage: '',
            onConfirmClick: false,
            popuploading: false,

            email: '',
            freeTrailProcess: 1,
            emailOtp: '',
            emailSuccessTitle: '',
            emailSuccessDesc: '',
            resObj: '',

            isShowModify:0,

        };
        this.getAllAppointments = this.getAllAppointments.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        if (popupHeight < 370) {
            popupHeight = 370;
        }

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllAppointments();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });


        LogUtils.infoLog1('this.props.premiumuser', this.props.premiumuser);
        if (this.props.isPaid) {
            if (this.props.isPaid === 0) {
                LogUtils.infoLog1('this.props.isPaid', this.props.isPaid);
                this.setState({ buttonText: 'View Plans' });
            }
        }
        if (this.props.sccFlag) {
            if (this.props.sccFlag === 0) {
                this.setState({ buttonText: 'View Plans' });
            }
        }

        // if (this.props.freeTrial === 1) {
        //     LogUtils.infoLog1('this.props.freeTrial', this.props.freeTrial);
        //     this.setState({ buttonText: 'Start 7 Days Free Trial' });
        // }

        if (this.props.homeObj) {
            LogUtils.infoLog1('email', this.props.homeObj);
            if (this.props.homeObj.email) {
                this.setState({ email: this.props.homeObj.email })
            }
            if (this.props.homeObj.show_free_call === 1) {
                this.setState({ buttonText: this.props.homeObj.fcc_btn });

            } else if (this.props.homeObj.show_free_trial === 1) {
                this.setState({ buttonText: this.props.homeObj.ftc_btn });

            }
        }




    }
    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getAllAppointments() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/appointmentlist`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ alertMsg: data.message, titMsg: data.title,resObj:data });
                    if (data.data.length) {
                        this.setState({ loading: false, arrAppointments: data.data });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: `You haven't booked any appointments yet` });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async cancelAppointment() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/cancelappointment`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    ua_id: this.state.selAppId,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.getAllAppointments();
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), });
            });
    }

    async getAllTrainerSlots() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('body', JSON.stringify({
            // tr_id: this.state.selTrID,
            ua_id: this.state.selUaId,
            seldate: this.state.selected,
        }));
        fetch(
            `${BASE_URL}/trainee/appointmentslots`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    // tr_id: this.state.selTrID,
                    at_id: 0,
                    ua_id: this.state.selUaId,
                    seldate: this.state.selected,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            if (item.tas_id === this.state.selTasId) {
                                item.check = true;
                                this.setState({ selSlotObj: item });
                            }
                            else {
                                item.check = false;
                            }
                        })
                        this.setState({ loading: false, arrTrainerSlots: data.data });
                    }
                    else {
                        this.setState({ loading: false, arrTrainerSlots: [], msgSelectDate: 'No slots available for this trainer' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                    this.setState({ arrTrainerSlots: []});
                }
                this.setState({ isShowModify:data.show_modify,visible:data.show_modify == 1 ? false:true });
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async bookAppointment() {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            at_id: 0,
            seldate: this.state.selected,
            from_time: this.state.selSlotObj.from_time,
            to_time: this.state.selSlotObj.to_time,
            ua_id: this.state.selUaId,
        }));
        fetch(
            `${BASE_URL}/trainee/saveappointmentslot`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    at_id: 0,
                    seldate: this.state.selected,
                    from_time: this.state.selSlotObj.from_time,
                    to_time: this.state.selSlotObj.to_time,
                    ua_id: this.state.selUaId,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    LogUtils.infoLog1(this.props.plantype);
                    Actions.appSuccess({ infoText: data.message, plantype: this.props.plantype, premiumuser: this.props.premiumuser });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async saveFreeTrial() {
        let token = await AsyncStorage.getItem('token');
        let urlType = '';
        if (this.props.homeObj.show_free_call === 1) {
            urlType = 'save_freetrial2';
        }
        else {
            urlType = 'save_freetrial';
        }

        LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

        fetch(
            `${BASE_URL}/trainee/${urlType}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    otp: this.state.emailOtp,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
                    if (this.props.homeObj.show_free_call === 1) {
                        LogUtils.appsFlyerEventLog('freeconsultationstarted', {
                            desc: 'Free Consultation Started',
                        });
                    }
                    else {
                        LogUtils.appsFlyerEventLog('freetrialstarted', {
                            desc: 'Free Trial Started',
                        });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }

    async verifyEmailOtp() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/verifyemailotp`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    email: this.state.email,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });
                    // this.setState({ popuploading: false, freeTrialObj: data.data,confirmSuccess: true, freeTrialSuccessTitle:  data.data.title, freeTrialSuccessMessage:  data.data.message, });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }

    // async getAllPlans() {
    //     this.setState({ loading: true });
    //     let token = await AsyncStorage.getItem('token');

    //     fetch(
    //         `${BASE_URL}/trainee/plans2`,
    //         {
    //             method: 'GET',
    //             headers: {
    //                 Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 Authorization: `Bearer ${token}`,
    //             },
    //         },
    //     )
    //         .then(processResponse)
    //         .then(res => {
    //             const { statusCode, data } = res;
    //             LogUtils.infoLog1('statusCode', statusCode);
    //             LogUtils.infoLog1('data', data);

    //             if (statusCode >= 200 && statusCode <= 300) {
    //                 if (data.data.length) {
    //                     this.setState({ loading: false, arrAllPlans: data.data });
    //                     var mySelItem = _.cloneDeep(this.state.arrAllPlans[0].data[0]);
    //                     Actions.traBuyPlanDetNew({ selPlan: mySelItem });
    //                 }
    //                 else {
    //                     this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
    //                 }

    //             } else {
    //                 if (data.message === 'You are not authenticated!') {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
    //                 } else {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
    //                 }
    //             }
    //         })
    //         .catch(function (error) {
    //             this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
    //         });
    // }


    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    ListEmptyView = () => {
        return (
            <View style={styles.viewNoData}>
                <Text style={styles.textNoData}>{this.state.noDataMsg}</Text>
            </View>

        );
    }

    onBackPressed() {
        if (this.state.confirmPop) {
            this.setState({ confirmPop: false });
        }
        else if (this.state.visible) {
            this.setState({ visible: false });
        }
        else {
            if (this.props.isRefresh === 'yes') {
                Actions.traineeHome();
            }
            else if (this.props.from === 'rewards' || this.props.from === 'feed') {
                Actions.pop();
            }
            else {
                Actions.popTo('traineeHome');
            }
        }
    }

    press = (hey) => {
        this.state.arrTrainerSlots.map((item) => {
            if (item.tas_id === hey.tas_id) {
                item.check = true;
                this.setState({ selSlotObj: item });
            }
            else {
                item.check = false;
            }
        })
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        } 
       
        if(this.state.isShowModify==1){
            this.setState({ visible:false });
            Actions.myOrders();
        }
        //this.setState({ alertMsg: '' });
        this.setState({ isAlert: false});
    }

    
    async onDecline() {
        if(this.state.isShowModify==1){
            this.setState({ visible:false });
        }
        this.setState({ isAlert: false });
    }

    renderCancelStatus(item) {

        if (item.is_cancel === 1) {
            return (

                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.optionBtns}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ selAppId: item.ua_id })
                            this.setState({ isSuccess: true, titMsg: 'Are you sure', sucMsg: 'Do you really want to cancel the appointment?' });
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    <View style={{ width: 1 }}></View>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.optionBtns}>
                        <TouchableOpacity onPress={() => {

                            this.setState({ visible: this.state.isShowModify==1 ? false : true, selTrID: item.tr_id, selTraImg: item.trainer_url, arrTrainerSlots: [], selSlotObj: {}, selTasId: item.tas_id, selUaId: item.ua_id });
                            LogUtils.infoLog(moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD"));
                            var date = moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD");
                            LogUtils.infoLog1('Converted Date', date);
                            var today = new Date(date);
                            let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                            LogUtils.infoLog(newdate);
                            this.setState({ selected: date });
                            this.setState({ day: today.getDate() });
                            this.setState({ month: parseInt(today.getMonth() + 1) });
                            this.setState({ year: today.getFullYear() });
                            var gsDayNames = [
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ];
                            var gsMonNames = [
                                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ];
                            var d = new Date(date);
                            var dayName = gsDayNames[d.getDay()];
                            var monName = gsMonNames[d.getMonth()];
                            LogUtils.infoLog1(dayName, monName);
                            this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                            this.getAllTrainerSlots();
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Reschedule</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

            );
        } else if (item.is_cancel === 2) {
            return (

                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>

                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        borderColor: '#ddd',
                        borderBottomRightRadius: 8,
                        borderBottomLeftRadius: 8,
                        position: 'relative',
                        flex: 1,
                    }}>
                        <TouchableOpacity onPress={() => {

                            this.setState({ visible: this.state.isShowModify==1 ? false : true, selTrID: item.tr_id, selTraImg: item.trainer_url, arrTrainerSlots: [], selSlotObj: {}, selTasId: item.tas_id, selUaId: item.ua_id });
                            LogUtils.infoLog(moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD"));
                            var date = moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD");
                            LogUtils.infoLog1('Converted Date', date);
                            var today = new Date(date);
                            let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                            LogUtils.infoLog(newdate);
                            this.setState({ selected: date });
                            this.setState({ day: today.getDate() });
                            this.setState({ month: parseInt(today.getMonth() + 1) });
                            this.setState({ year: today.getFullYear() });
                            var gsDayNames = [
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ];
                            var gsMonNames = [
                                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ];
                            var d = new Date(date);
                            var dayName = gsDayNames[d.getDay()];
                            var monName = gsMonNames[d.getMonth()];
                            LogUtils.infoLog1(dayName, monName);
                            this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                            this.getAllTrainerSlots();
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Reschedule</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

            );
        } else {
            return (
                <View></View>
            );
        }

    }

    renderProgramsList() {

        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.arrAppointments) && this.state.arrAppointments.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.arrAppointments}
                            keyExtractor={(item,index) => "arrAppointments"+index}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            // ListEmptyComponent={this.ListEmptyView}
                            renderItem={({ item }) => {
                                return <View key={item.ua_id} style={styles.aroundListStyle} >
                                    <View style={{
                                        flexDirection: 'row',
                                        padding: 15,
                                    }}>

                                        <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                            <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', }}>
                                                <View style={styles.viewBlueDot}></View>
                                                <Text style={styles.textWorkName}>{item.trainer_name}</Text>
                                            </View>

                                            <View style={{ flexDirection: 'column', marginTop: 3 }}>
                                                <Text style={styles.textTitle}>{item.sch_date}, {item.sch_time}</Text>
                                                {/* {item.is_traineeapmnt === 1
                                                    ? ( */}
                                                <Text style={styles.textExp}>{item.certification}</Text>
                                                {/* //     )
                                                //     : (
                                                //         <Text style={styles.textExp}>{`${item.experience}`}</Text>
                                                //     )
                                                // } */}
                                            </View>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', marginTop: 5 }}>
                                                <Text style={styles.textTrainerName}>Status : </Text>
                                                <Text style={styles.textStatus1}>{`${item.status}`}</Text>
                                            </View>

                                        </View>
                                    </View>

                                    {this.renderCancelStatus(item)}

                                </View>
                            }} />
                    </View>

                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/img_no_subscription.png')}
                            style={styles.imgNoSuscribe}
                        />
                        <Text style={styles.textNodataTitle}>{this.state.titMsg}</Text>

                        <Text style={styles.textNodata}>{this.state.alertMsg}</Text>
                    </View>

                );
            }
        }
        // else {
        //     if (this.props.isPaid === 0 || this.props.sccFlag === 0) {
        //         return (
        //             <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        //                 <Image
        //                     progressiveRenderingEnabled={true}
        //                     resizeMethod="resize"
        //                     source={require('../res/img_no_subscription.png')}
        //                     style={styles.imgNoSuscribe}
        //                 />
        //                 <Text style={styles.textNodataTitle}>{this.state.titMsg}</Text>

        //                 <Text style={styles.textNodata}>{this.state.alertMsg}</Text>
        //             </View>

        //         );
        //     }
        //     else {
        //         return (
        //             <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        //                 <Image
        //                     progressiveRenderingEnabled={true}
        //                     resizeMethod="resize"
        //                     source={require('../res/img_no_subscription.png')}
        //                     style={styles.imgNoSuscribe}
        //                 />
        //                 <Text style={styles.textNodataTitle}>{this.state.titMsg}</Text>

        //                 <Text style={styles.textNodata}>{this.state.alertMsg}</Text>
        //             </View>

        //         );
        //     }


        // }

    }

    renderBottom() {
        if (this.props.homeObj) {
            if (this.props.homeObj.show_free_call === 1 || this.props.homeObj.show_free_trial === 1) {
                return (
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity onPress={() => {
                            // Actions.traAllBuyPlans1({ isFreeTrial: 1 });
                            this.bottompopup.open()
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>{this.state.buttonText}</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                );
            }
        }
        // else {
        //     return (
        //         <LinearGradient colors={['#8b63e6', '#8c52ff']} style={styles.linearGradientViewPlans}>
        //             <TouchableOpacity
        //                 onPress={() => {
        //                     Actions.traAllBuyPlans1({ isFreeTrial: 0 });
        //                 }}
        //                 style={styles.buttonTuch}>
        //                 <Text style={styles.buttonText}>VIEW PLANS</Text>
        //             </TouchableOpacity>
        //         </LinearGradient>
        //     );
        // }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllAppointments();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    async onDeny() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
        this.cancelAppointment();
    }

    async onViewPlansClicked() {
        // let plan_cnt = await AsyncStorage.getItem('plan_cnt');
        // if (plan_cnt === '1') {
        //     this.getAllPlans();
        // }
        // else {
        Actions.traAllBuyPlans1({screen:"Appointments"});
        // }
    }

    _toggleSubview() {


        Animated.timing(
            this.state.bounceValue,
            {
                toValue: 0,
                duration: 1000
            }
        ).start();

    }

    renderFreeTrailProcess() {

        if (this.state.freeTrailProcess === 1) {

            return (
                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>


                    {this.props.homeObj.show_free_call === 1 ?
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.props.homeObj.fcc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.props.homeObj.fcc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.props.homeObj.fcc_text}</Text>
                            </View>
                        )
                        :
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.props.homeObj.ftc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.props.homeObj.ftc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.props.homeObj.ftc_text}</Text>
                            </View>
                        )
                    }

                    {/* <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.props.homeObj.ftc_image }}
                        style={{
                            width: 150,
                            height: 150,
                            alignSelf: 'center',
                        }}
                    />


                    <Text style={styles.freeTrialPopupTitle}>{this.props.homeObj.ftc_title}</Text>

                    <Text style={styles.freeTrialPopupText}>{this.props.homeObj.ftc_text}</Text> */}

                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.props.homeObj.email_verified === 1) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();

                                    } else {
                                        this._toggleSubview();
                                        this.setState({ freeTrailProcess: 2 });
                                    }


                                }}>
                                    <Text style={styles.textSave}>Confirm</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }


                </View>
            );

        } else if (this.state.freeTrailProcess === 2) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}>{this.props.homeObj.ev_title}</Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 25,
                        }}> {this.props.homeObj.ev_descp} </Text>
                    </Animated.View>



                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter Email"
                            placeholderTextColor="grey"
                            keyboardType="email-address"
                            maxLength={100}
                            value={this.state.email}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ email: text })}
                        />
                    </View>



                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    this.validateEmail(this.state.email);




                                    // setTimeout(() => {
                                    //   this._toggleSubview();
                                    //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                                    // }, 5000)



                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 3) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}> {this.state.emailSuccessTitle} </Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 20,
                            marginRight: 20,
                            marginBottom: 25,
                        }}> {this.state.emailSuccessDesc} </Text>
                    </Animated.View>



                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter code"
                            placeholderTextColor="grey"
                            keyboardType="number-pad"
                            maxLength={4}
                            value={this.state.emailOtp}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ emailOtp: text })}
                        />
                    </View>



                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.state.emailOtp) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();
                                    } else {
                                        this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                                    }


                                    // setTimeout(() => {
                                    //   this._toggleSubview();
                                    //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                                    // }, 5000)



                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 4) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.freeTrialObj.image }}
                        style={{
                            width: 150,
                            height: 150,
                            alignSelf: 'center',
                        }}
                    />
                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

                        <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
                    </Animated.View>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                        <TouchableOpacity onPress={() => {
                            this.bottompopup.close();
                            // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });

                            if (this.state.freeTrialObj.qst_type !== 0) {
                                if (this.state.freeTrialObj.qst_type === 1) {
                                    if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.aidietques();
                                    } else {
                                        Actions.dietHome1({ isRefresh: 'yes' });
                                    }

                                } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                                    if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else {
                                        Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                                    }

                                }
                            }

                        }}>
                            <Text style={styles.textSave}>Ok. Thanks</Text>
                        </TouchableOpacity>
                    </LinearGradient>


                </View>

            );




        }
    }


    async validateEmail(email) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!email) {
            this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
        }
        else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
            this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
        }
        else {

            this.setState({ popuploading: true, onConfirmClick: true });
            this.verifyEmailOtp();

        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Appointments</Text>
                    </View>

                    {this.renderProgramsList()}

                </View>

                {this.props.premiumuser === 1
                    ? (
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity onPress={() => {
                                // if (this.state.buttonText === 'Start 7 Days Free Trial') {
                                //     // Actions.traAllBuyPlans1({ isFreeTrial: 1 });
                                //     this.bottompopup.open()
                                // }
                                if (this.state.buttonText === 'View Plans') {
                                    this.onViewPlansClicked();
                                } else {

                                    if (this.props.plantype) {
                                        LogUtils.infoLog(this.props.plantype);
                                        if (this.props.plantype === 3) {
                                            Actions.appBook({ plantype: this.props.plantype, premiumuser: this.props.premiumuser });
                                        } else {
                                            // Actions.appBook({flag : 'Appoint'})
                                            // LogUtils.infoLog(moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD"));
                                            this.setState({ visible: this.state.isShowModify==1 ? false : true });
                                            var date = moment(new Date()).format("YYYY-MM-DD");
                                            LogUtils.infoLog1('Converted Date', date);
                                            var today = new Date(date);
                                            let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                                            LogUtils.infoLog(newdate);
                                            this.setState({ selected: date });
                                            this.setState({ day: today.getDate() });
                                            this.setState({ month: parseInt(today.getMonth() + 1) });
                                            this.setState({ year: today.getFullYear() });
                                            var gsDayNames = [
                                                'Sunday',
                                                'Monday',
                                                'Tuesday',
                                                'Wednesday',
                                                'Thursday',
                                                'Friday',
                                                'Saturday'
                                            ];
                                            var gsMonNames = [
                                                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                            ];
                                            var d = new Date(date);
                                            var dayName = gsDayNames[d.getDay()];
                                            var monName = gsMonNames[d.getMonth()];
                                            LogUtils.infoLog1(dayName, monName);
                                            this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                                            this.getAllTrainerSlots();
                                        }
                                    } else {
                                        // Actions.appBook({flag : 'Appoint'})
                                        // LogUtils.infoLog(moment(item.cdate, 'DD-MM-YYYY').format("YYYY-MM-DD"));
                                        this.setState({ visible: this.state.isShowModify==1 ? false : true });
                                        var date = moment(new Date()).format("YYYY-MM-DD");
                                        LogUtils.infoLog1('Converted Date', date);
                                        var today = new Date(date);
                                        let newdate = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
                                        LogUtils.infoLog(newdate);
                                        this.setState({ selected: date });
                                        this.setState({ day: today.getDate() });
                                        this.setState({ month: parseInt(today.getMonth() + 1) });
                                        this.setState({ year: today.getFullYear() });
                                        var gsDayNames = [
                                            'Sunday',
                                            'Monday',
                                            'Tuesday',
                                            'Wednesday',
                                            'Thursday',
                                            'Friday',
                                            'Saturday'
                                        ];
                                        var gsMonNames = [
                                            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                        ];
                                        var d = new Date(date);
                                        var dayName = gsDayNames[d.getDay()];
                                        var monName = gsMonNames[d.getMonth()];
                                        LogUtils.infoLog1(dayName, monName);
                                        this.setState({ dateConString: dayName + ', ' + monName + ' ' + today.getDate() + ', ' + today.getFullYear() });
                                        this.getAllTrainerSlots();
                                    }

                                    // if (this.props.sccFlag) {
                                    //     if (this.props.sccFlag === 1) {
                                    //         Actions.appBook()
                                    //     } else {
                                    //         Actions.traAllBuyPlans1();
                                    //     }

                                    // } else {
                                    //     Actions.appBook()
                                    // }
                                }

                            }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>{this.state.buttonText}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    )
                    : (
                        <View>
                            {this.renderBottom()}
                        </View>
                    )
                }



                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDecline.bind(this)}
                    no={this.state.isShowModify == 1 ? 'Ok' : ''} 
                    yes={this.state.isShowModify == 1 ? 'Modify' : 'Ok'} />

                <HomeDialog
                    visible={this.state.isSuccess}
                    title={this.state.titMsg}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    onDecline={this.onDeny.bind(this)}
                    no='No'
                    yes='Yes' />

                <Dialog
                    onDismiss={() => {
                        this.setState({ visible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ visible: false });
                    }}
                    width={1}
                    height={0.93}
                    dialogStyle={{ backgroundColor: '#fafafa', borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, position: 'absolute', bottom: 0, left: 0, width: wp('100%') }}
                    visible={this.state.visible}
                // rounded
                // actionsBordered
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff',
                        }}>
                        <View style={{ flexDirection: 'column', backgroundColor: '#ffffff' }}>
                            <View style={{
                                flexDirection: 'row',
                                margin: 15,
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    marginRight: 15,
                                    marginLeft: -15,
                                }}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ visible: false })}>
                                        <Image
                                            source={require('../res/ic_back.png')}
                                            style={[styles.backImageStyle, { marginTop: 0, }]}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <Text style={styles.textSelTime}>Choose visit time</Text>
                                <TouchableOpacity onPress={() => {
                                    if (this.state.selected) {
                                        if (this.state.selSlotObj.tas_id) {
                                            this.setState({ visible: false });
                                            this.setState({ confirmPop: true });
                                        }
                                        else {
                                            this.setState({ isAlert: true, alertMsg: 'Please select time slot to book appointment' });
                                        }

                                    }
                                    else {
                                        this.setState({ isAlert: true, alertMsg: 'Please select appointment data and time' });
                                    }

                                    // Actions.appSuccess();
                                }}>
                                    <Text style={styles.textDone}>DONE</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ marginLeft: -20, height: 1.5, width: wp('100%'), backgroundColor: '#e9e9e9' }}></View>
                            <Calendar
                                // Initially visible month. Default = Date()
                                current={this.state.selected}
                                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                minDate={new Date()}
                                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                maxDate={getToDate()}
                                // Handler which gets executed on day press. Default = undefined
                                onDayPress={(day) => {
                                    LogUtils.infoLog1('selected day', day);
                                    this.setState({ selected: day.dateString });
                                    this.setState({ day: day.day });
                                    this.setState({ month: day.month });
                                    this.setState({ year: day.year });
                                    // const date = new Date(2019, 12, 10);  // 2009-11-10
                                    // const month = date.toLocaleString('default', { month: 'short' });
                                    // LogUtils.infoLog(month);
                                    var gsDayNames = [
                                        'Sunday',
                                        'Monday',
                                        'Tuesday',
                                        'Wednesday',
                                        'Thursday',
                                        'Friday',
                                        'Saturday'
                                    ];
                                    var gsMonNames = [
                                        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                    ];

                                    var d = new Date(day.dateString);
                                    var dayName = gsDayNames[d.getDay()];
                                    var monName = gsMonNames[d.getMonth()];

                                    LogUtils.infoLog1(dayName, monName);
                                    this.setState({ dateConString: dayName + ', ' + monName + ' ' + day.day + ', ' + day.year });

                                    this.getAllTrainerSlots();
                                }}

                                // Handler which gets executed on day long press. Default = undefined
                                // onDayLongPress={(day) => { LogUtils.infoLog1('selected day', day) }}
                                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                                monthFormat={'MMM yyyy'}
                                // Handler which gets executed when visible month changes in calendar. Default = undefined
                                onMonthChange={(month) => { LogUtils.infoLog1('month changed', month) }}
                                // Hide month navigation arrows. Default = false
                                //hideArrows={true}
                                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                                // renderArrow={(direction) => (<Arrow />)}
                                // Do not show days of other months in month page. Default = false
                                //hideExtraDays={true}
                                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                                // day from another month that is visible in calendar page. Default = false
                                //    disableMonthChange={true}
                                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                                firstDay={1}
                                // Hide day names. Default = false
                                //    hideDayNames={true}
                                // Show week numbers to the left. Default = false
                                //    showWeekNumbers={true}
                                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                                // onPressArrowLeft={substractMonth => substractMonth()}
                                // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                                //onPressArrowRight={addMonth => addMonth()}
                                // Disable left arrow. Default = false
                                //disableArrowLeft={true}
                                // Disable right arrow. Default = false
                                //disableArrowRight={true}
                                markedDates={{ [this.state.selected]: { selected: true, disableTouchEvent: true, selectedDotColor: '#8c52ff' } }}
                                theme={{
                                    // calendarBackground: '#333248',
                                    // textSectionTitleColor: 'white',
                                    // dayTextColor: 'red',
                                    todayTextColor: '#8c52ff',
                                    selectedDayTextColor: 'white',
                                    // monthTextColor: 'white',
                                    // indicatorColor: 'white',
                                    textDayFontSize: 12,
                                    textMonthFontSize: 12,
                                    textMonthFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontSize: 12,
                                    selectedDayBackgroundColor: '#8c52ff',
                                    arrowColor: '#8c52ff',
                                    // textDisabledColor: 'red',
                                    'stylesheet.calendar.header': {
                                        week: {
                                            marginTop: 5,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }
                                    }
                                }}
                            />

                        </View>
                        <View style={{ backgroundColor: '#fafafa', marginLeft: -20, width: wp('100%'), }}>
                            <Text style={styles.selDateText}>{this.state.dateConString}</Text>

                            <FlatList
                                style={{ width: wp('100%'), height: hp('45%') }}
                                contentContainerStyle={{ paddingBottom: hp('5%'), justifyContent: 'center', alignSelf: 'center', marginTop: 10 }}
                                numColumns={2}
                                data={this.state.arrTrainerSlots}
                                keyExtractor={(item,index) => "arrTrainerSlots"+index}
                                ItemSeparatorComponent={this.FlatListItemSeparator1}
                                ListEmptyComponent={this.ListSlotsEmptyView}
                                renderItem={({ item }) => {
                                    return <TouchableOpacity style={{
                                        width: wp('45%'),
                                        marginRight: 5,
                                        marginLeft: 7,
                                        marginBottom: 7,

                                    }} onPress={() => { this.press(item) }}>
                                        {item.check
                                            ? (
                                                <View style={{
                                                    borderColor: '#8c52ff', borderRadius: 5,
                                                    borderWidth: 1,
                                                    height: 40,
                                                    width: wp('45%'),
                                                    justifyContent: 'center',
                                                    backgroundColor: '#8c52ff'
                                                }}>
                                                    <Text style={styles.textWorkNameChecked}>{`${item.schedule_time}`}</Text>

                                                </View>
                                            )
                                            : (
                                                <View style={{
                                                    borderColor: '#ddd', borderRadius: 5,
                                                    borderWidth: 1,
                                                    height: 40,
                                                    width: wp('45%'),
                                                    justifyContent: 'center',
                                                    backgroundColor: '#ffffff'
                                                }}>
                                                    <Text style={styles.textWorkName1}>{`${item.schedule_time}`}</Text>

                                                </View>

                                            )}
                                    </TouchableOpacity>

                                }} />
                        </View>

                    </DialogContent>
                </Dialog>

                {/* confirm popup */}
                <Dialog
                    onDismiss={() => {
                        this.setState({ confirmPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ confirmPop: false });
                    }}
                    width={0.8}
                    // height={0.9}
                    // dialogStyle={{marginTop: 30,}}
                    visible={this.state.confirmPop}
                    // rounded
                    // actionsBordered
                    footer={
                        <DialogFooter style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                            alignSelf: 'center',
                        }}>
                            <DialogButton
                                text="Cancel"
                                bordered
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                }}
                                textStyle={{
                                    fontSize: 13, color: '#6d819c', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="Confirm"
                                bordered
                                textStyle={{
                                    fontSize: 13, color: '#8c52ff', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                    this.bookAppointment();
                                }}
                                key="button-2"
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>
                                <Text style={{
                                    alignSelf: 'center', color: '#282c37', fontSize: 16,
                                    fontWeight: '500',
                                    textAlign: 'center',
                                    marginTop: 15,
                                    fontFamily: 'Rubik-Medium',
                                    letterSpacing: 0.49,
                                }}>Please confirm your appointment</Text>
                                <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        source={require('../res/ic_app.png')}
                                        style={styles.profileImage1}>
                                    </Image>

                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 13,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{moment(this.state.selected, 'YYYY-MM-DD').format("DD-MM-YYYY")}</Text>
                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 5,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{this.state.selSlotObj.schedule_time}</Text>
                                </View>


                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <RBSheet
                    ref={ref => {
                        this.bottompopup = ref;
                    }}
                    height={popupHeight}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    closeOnPressBack={!this.state.onConfirmClick}
                    onClose={() => {
                        this.setState({ freeTrailProcess: 1 })
                    }}
                    customStyles={{
                        container: {
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }
                    }}
                >


                    {this.state.onConfirmClick
                        ?
                        (
                            <View></View>
                        )
                        :
                        (
                            <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                                onPress={() => { this.bottompopup.close() }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={{
                                        width: 19,
                                        height: 19,
                                        alignSelf: 'flex-start',
                                        tintColor: 'black',
                                    }}
                                    source={require('../res/ic_cross_black.png')}
                                />
                            </TouchableOpacity>

                        )}

                    {this.props.homeObj
                        ?
                        (
                            <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                {this.renderFreeTrailProcess()}
                            </View>

                        )
                        :
                        (
                            <View></View>

                        )}



                    {/* {this.state.confirmSuccess
                            ?
                            (

                                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{ uri: this.state.freeTrialObj.image }}
                                        style={{
                                            width: 150,
                                            height: 150,
                                            alignSelf: 'center',
                                        }}
                                    />
                                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                                        <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

                                        <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
                                    </Animated.View>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.bottompopup.close();
                                            // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });

                                            if (this.state.freeTrialObj.qst_type !== 0) {
                                                if (this.state.freeTrialObj.qst_type === 1) {
                                                    if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                                        Actions.aidietques();
                                                    } else {
                                                        Actions.dietHome1({ isRefresh: 'yes' });
                                                    }

                                                } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                                                    if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                                    } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                                    } else {
                                                        Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                                                    }

                                                }
                                            }


                                        }}>
                                            <Text style={styles.textSave}>Ok. Thanks</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>


                                </View>

                            )
                            :
                            (
                                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{ uri: this.props.freeTrialConfirmImg }}
                                        style={{
                                            width: 150,
                                            height: 150,
                                            alignSelf: 'center',
                                        }}
                                    />


                                    <Text style={styles.freeTrialPopupTitle}>{this.props.freeTrialConfirmTitle}</Text>

                                    <Text style={styles.freeTrialPopupText}>{this.props.freeTrialConfirmText}</Text>

                                    {this.state.popuploading
                                        ?
                                        (
                                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
                                        )
                                        :
                                        (

                                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                                <TouchableOpacity onPress={() => {

                                                    this.setState({ popuploading: true, onConfirmClick: true });
                                                    this.saveFreeTrial();

                                                    // setTimeout(() => {
                                                    //   this._toggleSubview();
                                                    //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                                                    // }, 5000)



                                                }}>
                                                    <Text style={styles.textSave}>Confirm</Text>
                                                </TouchableOpacity>
                                            </LinearGradient>

                                        )
                                    }


                                </View>
                            )
                        } */}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </RBSheet>


            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    profileImage: {
        width: 100,
        height: 100,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 8,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    textWorkNameChecked: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#ffffff',
        padding: 10,
    },
    textWorkName1: {
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
        padding: 10,
    },
    textWorkName: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        marginRight: 15,
    },
    textTrainerName: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        alignSelf: 'center',
    },
    textStatus1: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#8c52ff',
        alignSelf: 'center',
    },
    textStatus: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#ffa66d',
        alignSelf: 'center',
    },
    textMonth: {
        fontSize: 8,
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        backgroundColor: '#8c52ff',
        textAlign: 'center',
        color: '#ffffff',
        borderRadius: 5,
        textAlign: 'center',
        alignSelf: 'center',
        paddingLeft: 5,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 5,
        letterSpacing: 0.23,
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewNoData: {
        padding: 10,
        marginTop: Dimensions.get('window').height / 3,
        height: 40,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        margin: 20,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    // lgCancel: {
    //     justifyContent: 'center',
    //     flexDirection: 'row',
    //     borderColor: '#ddd',
    //     borderBottomLeftRadius: 8,
    //     position: 'relative',
    //     flex: 1,
    // },
    // lgReschedule: {
    //     justifyContent: 'center',
    //     flexDirection: 'row',
    //     borderColor: '#ddd',
    //     borderBottomRightRadius: 8,
    //     position: 'relative',
    //     flex: 1,
    // },
    optionBtns: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomRightRadius: 8,
        position: 'relative',
        flex: 1,
    },
    textSelTime: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#1E1F20',
    },
    textDone: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#8c52ff',
    },
    selDateText: {
        alignSelf: 'flex-start',
        fontSize: 11,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 15,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
    },
    profileImage1: {
        width: 200,
        height: 60,
        marginTop: 10,
        marginBottom: 10,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    textExp: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
        lineHeight: 18,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 5,
    },
    textTitle: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        // position:'absolute',
        // bottom:0,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    freeTrialPopupTitle: {
        width: wp('85%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 10
    },
    freeTrialPopupText: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 15,
    },

    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 20,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
});

const mapStateToProps = state => {

    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(Appointments);
