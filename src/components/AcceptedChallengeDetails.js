import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
    Dimensions,
    FlatList,
    Platform,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { ProgressBar, Loader, CustomDialog, ProgressBar1 } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { BASE_URL, SWR } from '../actions/types';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import Video from 'react-native-video';
import NetInfo from "@react-native-community/netinfo";
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
import { RFValue } from "react-native-responsive-fontsize";
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import Orientation from 'react-native-orientation';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { RNS3 } from 'react-native-aws3';
import RNFetchBlob from 'rn-fetch-blob';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// let token = '', userId = '';
const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

const createFormData = (video, body) => {
    const data = new FormData();
    LogUtils.infoLog1("video url", video);
    if (video) {
        data.append('file', {
            uri: video,
            name: video
                .toString()
                .replace(
                    'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
                    '',
                ),
            type: 'video/mp4', // or photo.type
        });
    }
    data.append('data', body);
    return data;
};


class AcceptedChallengeDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            challenges: [],
            paused: true,
            isSuccess: false,
            sucMsg: '',
            isAlert: false,
            alertMsg: '',
            alertTitle: '',
            loading: false,
            pageNo: 1,
            isTabClicked: 2,
            filepath: {
                data: '',
                uri: '',
            },
            fileData: '',
            fileUri: '',
            fileName: '',
            fileType: '',
            progress: 0,
            isProgress: false,
            resMyChalObj: '',
            refresh: false,
            challengeDetails: {},
            noData: '',
            post: '',

            challengeWinners: {},
            arrChallengeWinners: [],
            isAddVideoPop: false,
            isVideoRejPop: false,
            videoRejStatus: '',
            videoRejComments: '',
        };
    }
    async componentDidMount() {
        allowFunction();
        // token = await AsyncStorage.getItem('token');
        // userId = await AsyncStorage.getItem('userId');
        Orientation.lockToPortrait();

        if (this.props.flag && this.props.flag === 'Leaderboard') {
            this.setState({ isTabClicked: 3 })
        }
        if (this.props.flag && this.props.flag === 'MyVideos') {
            this.setState({ isTabClicked: 2 })
        }

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                this.getUserMyChallengesVideos();
                this.getAllWinners();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });


    }


    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isVideoRejPop) {
            this.setState({ isVideoRejPop: false });
        }
        else if (this.state.isAddVideoPop) {
            this.setState({ isAddVideoPop: false });
        }
        else {
            Actions.pop();
        }

    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
        this.setState({ fileUri: "" });

        if (this.state.isSuccess) {
            this.setState({ loading: true });
            this.getUserMyChallengesVideos();
            this.setState({ isSuccess: false });
        }



    }

    async getUserMyChallengesVideos() {
        this.setState({ refresh: !this.state.refresh });

        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/getchlngvideos`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    c_id: this.props.cid,
                    page: 1,
                    pagesize: 20,
                    width: screenWidth,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                LogUtils.infoLog1('data', res);
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.setState({ resMyChalObj: data.data });
                    // LogUtils.infoLog1('All Videos : ', data.data.all_video);
                    LogUtils.infoLog1('My Videos : ', data.data.my_video);
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTitle: 'Alert', alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTitle: 'Alert', alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTitle: 'Alert', alertMsg: JSON.stringify(error) });
            });
    }

    async getAllWinners() {
        // this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('Url', `${BASE_URL}/trainee/challengeranking`);
        LogUtils.infoLog1('Body', JSON.stringify({
            c_id: this.props.cid,
            page: 1,
            pagesize: 10
        }));
        fetch(
            `${BASE_URL}/trainee/challengeranking`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    c_id: this.props.cid,
                    page: 1,
                    pagesize: 10
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    this.setState({ loading: false, challengeWinners: data.data });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        // this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                //this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    renderItem(item) {
        return (
            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>

                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={styles.image}
                    source={item.thumbnail} />
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_play.png')}
                    style={styles.playImageStyle}
                />
            </View>
        );
    }



    renderItem = ({ item, index }) => {
        return (
            <View style={{ felx: 1, flexDirection: 'column', justifyContent: 'center' }}>
                <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center' }}
                    onPress={() => {
                        if (Platform.OS === 'android') {
                            Actions.newvideoplay({ item: item, from: 'challengedetails' });
                            // Actions.customPlayer({ item: item, from: 'challengedetails' });
                        } else {
                            Actions.afPlayer({ item: item, from: 'challengedetails' });
                        }
                    }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={styles.item}
                        source={{ uri: item.thumbnail }} />
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_play.png')}
                        style={styles.playImageStyle}
                    />
                </TouchableOpacity>
            </View>
        );
    }

    renderTabBar() {
        if (this.state.loading === false) {
            if (this.state.isTabClicked === 1) {

                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        {/* <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>ALL</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>MY VIDEOS</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>LEADERBOARD</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>

                    </ImageBackground>
                );
            } else if (this.state.isTabClicked === 2) {
                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        {/* <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>ALL</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>MY VIDEOS</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>LEADERBOARD</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                );

            } else if (this.state.isTabClicked === 3) {
                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        {/* <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>ALL</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>MY VIDEOS</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>LEADERBOARD</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                );

            }
        }


    }
    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }
    renderClaimed() {
        if (this.state.challengeWinners.is_detgiven === 1) {
            return (
                <View style={styles.claimReward}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <View
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Reward Claimed </Text>
                        </View>
                    </LinearGradient>

                </View>
            );

        } else {

            return (
                <View style={styles.claimReward}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {
                                LogUtils.infoLog1("this.state.challengeWinners:", this.state.challengeWinners);
                                // if (this.state.challengeWinners.is_coupon === 1) {
                                //     Actions.chaRewards({ mFrom: 'coupon', cid: this.props.cid });
                                // } else {
                                //     Actions.chaRewards({ mFrom: 'product', cid: this.props.cid, weight: this.state.challengeWinners.weight, pincode: this.state.challengeWinners.pincode });
                                // }
                                Actions.chaRewards({ mFrom: 'product', cid: this.props.cid, weight: this.state.challengeWinners.weight, pincode: this.state.challengeWinners.pincode });
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Claim Reward</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </View>
            );
        }
    }

    renderMyRank() {

        if (this.state.challengeWinners.myrank) {
            return (
                <View style={styles.goalBg}>
                    <View
                        style={styles.goalDet}>
                        <Text style={styles.textWeight}>Your Rank</Text>
                    </View>
                    <View style={{ width: 1, backgroundColor: '#e9e9e9' }}></View>
                    <View style={styles.goalDet}>
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={styles.textWeight}>{this.state.challengeWinners.myrank}</Text>

                        </View>

                    </View>
                </View>
            );
        } else {
            return (
                <View></View>
            );

        }


    }

    renderWinnersList() {
        if (this.state.challengeWinners) {
            if (Array.isArray(this.state.challengeWinners.winner_arr) && this.state.challengeWinners.winner_arr.length) {
                return (
                    <View style={{ justifyContent: 'flex-start', }}>
                        {this.renderMyRank()}

                        {this.state.challengeWinners.is_winner === 1
                            ? (
                                this.renderClaimed()

                            )
                            : (

                                <View></View>

                            )}

                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('10%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.challengeWinners.winner_arr}
                            keyExtractor={(item, index) => "winner_arr"+index}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                return <View style={styles.aroundListStyle} >

                                    <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                                        <ImageBackground source={require('../res/ic_rank_bg.png')} style={{ width: 25, height: 28, alignSelf: 'center', marginRight: 15, alignItems: 'center', }}>
                                            <View style={styles.viewOraDot}>
                                                <Text style={styles.textrank}>{`${item.rank}`}</Text>
                                            </View>
                                        </ImageBackground>
                                    </View>

                                    <View style={{ width: '11%', alignItems: 'center', justifyContent: 'center' }}>
                                        {item.user_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.user_img }}
                                                    style={styles.profileImage}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={styles.profileImage}
                                                />
                                            )
                                        }

                                    </View>
                                    <View style={{ width: wp('50%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${item.user_name}`}</Text>

                                    </View>

                                    <View style={{ width: wp('20%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'center', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${item.rating}`}</Text>
                                        <Text style={styles.textWorkName}>Points</Text>

                                    </View>



                                </View>
                            }} />
                    </View>

                );


            }
            else {
                return (
                    <View style={styles.viewTitleCal}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_nowinners_data.png')}
                            style={styles.imgUnSuscribe}
                        />
                        <Text style={styles.textCreditCard}>Coming soon</Text>

                        <Text style={styles.textDesc}>Leaderboard will be visible soon. Thank you</Text>

                    </View>
                );

            }
        }
    }

    renderUploadVideoButton() {
        try {
            if (!isEmpty(this.state.challengeWinners)) {
                if (this.state.challengeWinners.is_expired === 0) {
                    return (
                        <View style={styles.bottom}>
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        // this.chooseImage();
                                        if (this.state.resMyChalObj.chlng_cnt < this.state.resMyChalObj.max_limit) {
                                            // this.setState({ isAddVideoPop: true });
                                            if (Platform.OS === 'android') {
                                                this.pickImagesFromGallery();
                                            } else {
                                                this.pickImagesFromGalleryIOS();
                                            }
                                        }
                                        else {
                                            this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `You can upload upto ${this.state.resMyChalObj.max_limit} videos per day` });
                                        }

                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>Upload Video</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        </View>
                    );
                }
                else {
                    return (
                        <View style={styles.bottom}>
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <View
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>Challenge Completed</Text>
                                </View>
                            </LinearGradient>
                        </View>
                    )
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    chooseImage = () => {
        let options = {
            title: 'Video Picker',
            mediaType: 'video',
            takePhotoButtonTitle: 'Take Video',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // alert(JSON.stringify(response));s
                LogUtils.infoLog1('response', JSON.stringify(response));
                LogUtils.infoLog1('fileUri', response.uri);
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.setState({ isProgress: true })
                this.saveChallengeVideo();
            }
        });
    };


    captureImage = () => {
        let options = {
            title: 'Select Video',
            mediaType: 'video',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.setState({ isProgress: true })
                this.saveChallengeVideo();
            }
        });
    };


    async pickImagesFromGallery() {
        // Pick a single file
        try {
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.video],
            });

            LogUtils.infoLog1('response', response);

            this.setState({
                filePath: response,
                fileData: response.data,
                fileName: response.name,
                fileUri: response.uri,
                fileType: response.type,
            });

            this.validateVideoSize(response.size);

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    async pickImagesFromGalleryIOS() {
        // Pick a single file
        let options = {
            title: 'Select Video',
            mediaType: 'video',
            videoQuality: 'medium',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
                if (response.fileSize) {

                    this.validateVideoSize(response.fileSize);
                } else {
                    RNFetchBlob.fs.stat(response.origURL)
                        .then((stats) => {
                            LogUtils.infoLog1('stats.size', stats.size);
                            this.validateVideoSize(stats.size);

                        })
                        .catch((err) => { LogUtils.infoLog1('err', err); })
                }
            }
        });
    }


    async validateVideoSize(fileSize) {
        let max_video_size = await AsyncStorage.getItem('max_video_size');
        var videoSize = parseInt(max_video_size) / 1048576;
        if (fileSize < parseInt(max_video_size)) {
            //    this.setState({ isProgress: true })
            //     this.saveChallengeVideo();

            this.uploadVideoToAWS();
        }
        else {
            this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `Please make sure your file size should not exceed ${videoSize}MB` });
        }
    }

    async uploadVideoToAWS() {
        try{

            this.setState({ progress: 0, isProgress: true });
            let userId = await AsyncStorage.getItem('userId');
            LogUtils.infoLog1('this.state.fileUri : ', this.state.fileUri);
            let type = '', extension ='';
            if ((this.state.fileUri.includes('.mp4') || this.state.fileUri.includes('.MP4')) ) {
                type = "video/mp4";
                extension = '.mp4';
            } else if ((this.state.fileUri.includes('.3gp') || this.state.fileUri.includes('.3GP'))) {
                type = "video/3gpp";
                extension = '.3gp';
            } else if ((this.state.fileUri.includes('.mov') || this.state.fileUri.includes('.MOV'))) {
                type = "video/quicktime";
                extension = '.mov';
            } else if ((this.state.fileUri.includes('.avi') || this.state.fileUri.includes('.AVI'))) {
                type = "video/x-msvideo";
                extension = '.avi';
            } else if ((this.state.fileUri.includes('.wmv') || this.state.fileUri.includes('.WMV'))) {
                type = "video/x-ms-wmv";
                extension = '.wmv';
            }else {
                 // LogUtils.infoLog1('file : ', this.state.fileName);
                 if (this.state.fileName) {
                    const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
                  //   LogUtils.infoLog1(pluginName, fileExtension);
                    type = this.state.fileType;
                    extension = '.'+fileExtension.toLowerCase();
                }
            }
           
    
            const file = {
                // `uri` can also be a file system path (i.e. file://)
                uri: this.state.fileUri,
                name: userId + '_' + new Date().getTime() + extension,
                type: type
            }
    
            const options = {
                keyPrefix: await AsyncStorage.getItem('s3_chlng_path'),
                bucket: await AsyncStorage.getItem('s3_bucket'),
                region: await AsyncStorage.getItem('s3_region'),
                accessKey: await AsyncStorage.getItem('s3_key'),
                secretKey: await AsyncStorage.getItem('s3_secret'),
                successActionStatus: 201
            }
            LogUtils.infoLog1('file : ', file);
            LogUtils.infoLog1('options : ', options);
    
            RNS3.put(file, options)
                .progress((e) => {
                    const progress = Math.floor((e.loaded / e.total) * 100);
                    this.setState({ progress: progress });      
                }).then(response => {
                    if (response.status === 201) {
                        this.setState({ resObjAwsS3: response.body.postResponse });
                        LogUtils.infoLog1('response', response.body);
                        LogUtils.infoLog1('response1', response.body.postResponse);
                        // this.setState({ isProgress: false });
                        // this.setState({ resObjAwsS3: response.body.postResponse });
                        this.saveChallengeVideoAwsS3();
                    }
                    else {
                        LogUtils.infoLog('Failed to upload image to S3');
                        LogUtils.infoLog1('response', response.body);
                        this.setState({ isProgress: false });
                    }
                });
        }catch(error){
            console.log(error);
        }
    }

    async saveChallengeVideoAwsS3() {
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('request', JSON.stringify({
            uc_id: this.props.uc_id,
            c_id: this.props.cid,
            comments: this.state.post,
            bucket: this.state.resObjAwsS3.bucket,
            etag: this.state.resObjAwsS3.etag,
            key: this.state.resObjAwsS3.key,
            location: this.state.resObjAwsS3.location
        }));
        fetch(
            `${BASE_URL}/trainee/saveuserchlngvideoS3`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    uc_id: this.props.uc_id,
                    c_id: this.props.cid,
                    comments: this.state.post,
                    bucket: this.state.resObjAwsS3.bucket,
                    etag: this.state.resObjAwsS3.etag,
                    key: this.state.resObjAwsS3.key,
                    location: this.state.resObjAwsS3.location
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1("statusCode", statusCode);
                LogUtils.infoLog1("data", data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isProgress: false, isTabClicked: 2 });
                    this.setState({ isSuccess:true,isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    // this.setState({ loading: true });
                    // this.getUserMyChallengesVideos();
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                    } else {
                        this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isProgress: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }

    async saveChallengeVideo() {
        let token = await AsyncStorage.getItem('token');
        futch(`${BASE_URL}/trainee/saveuserchlngvideo`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${token}`,
            },
            body: createFormData(this.state.fileUri, JSON.stringify({
                uc_id: this.props.uc_id,
                c_id: this.props.cid,
                comments: this.state.post,
            })),
        }, (progressEvent) => {
            const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            this.setState({ progress: progress });
        })
            .then(res => {
                LogUtils.infoLog1('data', res);
                this.setState({ isProgress: false })
                const statusCode = res.status;
                LogUtils.infoLog1('statusCode', statusCode);

                const data = JSON.parse(res.response);
                LogUtils.infoLog1('data', data);
                LogUtils.infoLog1('data.message', data.message);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: true });
                    this.getUserMyChallengesVideos();

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
                this.setState({ isProgress: false })
            })
            .catch(function (error) {
                LogUtils.infoLog1('upload error', error);
                feedPostError(dispatch, error);
            });
    }



    renderPlayButton() {
        if (this.state.paused) {
            return (
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_play_white.png')}
                    style={styles.postPlayImageStyle}
                />
            );
        }
    }

    onEnd = () => this.setState({ paused: true });

    renderVideoData() {
        if (this.state.fileUri) {
            return (
                <TouchableOpacity onPress={() => {
                    this.setState({ paused: !this.state.paused });
                }}>
                    <Video
                        onEnd={this.onEnd}
                        onLoad={this.onLoad}
                        onLoadStart={this.onLoadStart}
                        onProgress={this.onProgress}
                        paused={this.state.paused}
                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                        resizeMode={this.state.screenType}
                        source={{ uri: this.state.fileUri }}
                        style={styles.mediaPlayer}
                        onEnd={this.onEnd}
                        volume={10}
                    />
                    <TouchableOpacity
                        style={styles.playButton}
                        onPress={() => {
                            this.setState({ paused: !this.state.paused });
                        }}>
                        {this.renderPlayButton()}
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    }

    renderItemNew = ({ item, index }) => {
        return (

            <View key={item.ucv_id} style={{ width: '50%', justifyContent: 'flex-start' }}>
                <View>
                    <TouchableOpacity style={styles.item}
                        onPress={() => {
                            if (Platform.OS === 'android') {
                                Actions.newvideoplay({ item: item, from: 'challengedetails' });
                                // Actions.customPlayer({ item: item, from: 'challengedetails' });
                            } else {
                                Actions.afPlayer({ item: item, from: 'challengedetails' });
                            }

                        }}>
                        {item.thumbnail
                            ?
                            (
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={styles.image}
                                    source={{ uri: item.thumbnail + `?w=${screenWidth}` }} />
                            )
                            :
                            (
                                <View
                                    style={{
                                        width: wp('49%'),
                                        height: hp('20%'),
                                        borderRadius: 5,
                                        alignSelf: 'center',
                                        backgroundColor: '#000000',
                                    }}>

                                </View>
                            )
                        }



                    </TouchableOpacity>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 0, position: 'absolute', right: 0, alignItems: 'center' }}>
                        <Text style={styles.textStyle}>{item.user_name}</Text>
                    </LinearGradient>
                </View>

            </View>


        );



    };

    renderMyVideosItem = ({ item, index }) => {
        return (

            <View key={item.ucv_id} style={{ width: '50%', justifyContent: 'flex-start' }}>
                <View>
                    <TouchableOpacity style={styles.item}
                        onPress={() => {
                            if (Platform.OS === 'android') {
                                Actions.newvideoplay({ item: item, from: 'challengedetails' });
                                // Actions.customPlayer({ item: item, from: 'challengedetails' });
                            } else {
                                Actions.afPlayer({ item: item, from: 'challengedetails' });
                            }

                        }}>

                        {item.thumbnail
                            ?
                            (
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={styles.image}
                                    source={{ uri: item.thumbnail + `?w=${screenWidth}` }} />
                            )
                            :
                            (
                                <View
                                    style={{
                                        width: wp('49%'),
                                        height: hp('20%'),
                                        borderRadius: 5,
                                        alignSelf: 'center',
                                        backgroundColor: '#000000',
                                    }}>

                                </View>
                            )
                        }

                    </TouchableOpacity>

                    {item.video_status === 'Rejected'
                        ?
                        (
                            <View style={{
                                flexDirection: 'row',
                                alignSelf: 'flex-end',
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                                position: 'absolute',
                                padding: 15,
                                top: 0,
                                right: 0
                            }}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ videoRejStatus: item.video_status, videoRejComments: item.video_comments, isVideoRejPop: true, })}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_info_blueround.png')}
                                        style={styles.infoAppBlue}
                                    />
                                </TouchableOpacity>

                            </View>
                        )
                        :
                        (<View></View>)

                    }

                </View>

            </View>
        );
    }

    renderVideoListOrNot() {
        if (this.state.resMyChalObj !== null && this.state.loading === false) {
            if (Array.isArray(this.state.resMyChalObj.all_video) && this.state.resMyChalObj.all_video.length) {
                return (
                    <FlatList
                        extraData={this.state}
                        contentContainerStyle={{ paddingBottom: hp('20%') }}
                        data={this.state.resMyChalObj.all_video}
                        numColumns={2}
                        keyExtractor={(item, index) => "all_video"+index}
                        showsVerticalScrollIndicator={false}
                        //keyExtractor={(item,index) => item.ucv_id}
                        renderItem={(this.renderItemNew)} />
                );

            }
            else {
                if (this.state.loading === false) {
                    if (!isEmpty(this.state.challengeWinners) && this.state.challengeWinners.is_expired === 0) {
                        return (
                            <View style={styles.viewTitleCal}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_novideos.png')}
                                    style={styles.imgUnSuscribe}
                                />
                                <Text style={styles.textCreditCard}></Text>

                                <Text style={styles.textDesc}>Be the first to participate</Text>


                            </View>
                        );
                    }
                    else {
                        return (
                            <View style={styles.viewTitleCal}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_novideos.png')}
                                    style={styles.imgUnSuscribe}
                                />
                                <Text style={styles.textCreditCard}></Text>

                                <Text style={styles.textDesc}>This challenge is completed you cannot upload anymore</Text>

                            </View>
                        );
                    }


                }

            }
        }
    }
    renderVideoListOrNotMyVideos() {
        if (this.state.resMyChalObj !== null && this.state.loading === false) {

            if (Array.isArray(this.state.resMyChalObj.my_video) && this.state.resMyChalObj.my_video.length) {
                return (
                    <FlatList
                        extraData={this.state}
                        contentContainerStyle={{ paddingBottom: hp('20%') }}
                        data={this.state.resMyChalObj.my_video}
                        keyExtractor={(item, index) => index}
                        showsVerticalScrollIndicator={false}
                        numColumns={2}
                        renderItem={(this.renderMyVideosItem)} />
                );
            }
            else {
                if (!isEmpty(this.state.challengeWinners) && this.state.challengeWinners.is_expired === 0) {
                    return (
                        <View style={styles.viewTitleCal}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_novideos.png')}
                                style={styles.imgUnSuscribe}
                            />
                            <Text style={styles.textCreditCard}></Text>

                            <Text style={styles.textDesc}>Start uploading your videos to win rewards</Text>

                        </View>
                    );
                }
                else {
                    return (
                        <View style={styles.viewTitleCal}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_novideos.png')}
                                style={styles.imgUnSuscribe}
                            />
                            <Text style={styles.textCreditCard}></Text>

                            <Text style={styles.textDesc}>This challenge is completed you cannot upload anymore</Text>

                        </View>
                    );
                }


            }
        }
    }

    renderTabData() {
        if (this.state.loading === false) {
            if (this.state.isTabClicked === 1) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                        }}>
                            {this.renderVideoListOrNot()}
                        </View>
                    </View>
                );
            }
            else if (this.state.isTabClicked === 2) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                        }}>
                            {this.renderVideoListOrNotMyVideos()}
                        </View>
                    </View>
                );
            }
            else if (this.state.isTabClicked === 3) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                        }}>
                            {this.renderWinnersList()}
                        </View>
                    </View>
                );
            }
        }

    }

    renderProgressBar() {

        if (Platform.OS === 'android') {
            return (
                <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
            );

        } else {
            return (
                <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
            );
        }

    }



    render() {

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                {this.renderProgressBar()}
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111, }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Challenge Details</Text>

                        <View style={{ position: 'absolute', zIndex: 111, right: 0 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => Actions.challengedetails({ cid: this.props.cid })}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_info_blueround.png')}
                                    style={styles.infoImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {this.renderTabBar()}
                    {this.renderTabData()}
                </View>

                {this.renderUploadVideoButton()}

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />


                <Dialog
                    onDismiss={() => {
                        this.setState({ isVideoRejPop: false, videoRejStatus: '', videoRejComments: '' });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isVideoRejPop: false, videoRejStatus: '', videoRejComments: '' });
                    }}
                    width={0.75}
                    visible={this.state.isVideoRejPop}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                    <Text style={styles.textPopTitle}>Video Information</Text>
                                    <View style={{ marginTop: 15 }}></View>
                                    <View style={{ flexDirection: 'column', width: wp('70%'), }}>
                                        <Text style={styles.textPopDesc}>Status</Text>
                                        <Text style={styles.textPopDescBlack}>{this.state.videoRejStatus}</Text>
                                        <View style={{ marginTop: 10 }}></View>

                                        {this.state.videoRejComments
                                            ?
                                            (
                                                <View>
                                                    <Text style={styles.textPopDesc}>Comments</Text>
                                                    <Text style={styles.textPopDescBlack}>{this.state.videoRejComments}</Text>
                                                </View>

                                            )
                                            :
                                            (
                                                <View></View>
                                            )
                                        }

                                    </View>



                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientTaskInfo}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isVideoRejPop: false, videoRejStatus: '', videoRejComments: '' });
                                        }}>
                                            <Text style={styles.textTaskInfoSave}>OK</Text>
                                        </TouchableOpacity>

                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>


                <Dialog
                    onDismiss={() => {
                        this.setState({ isAddVideoPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isAddVideoPop: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isAddVideoPop}>
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15 }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <View style={{ marginTop: 10, }}></View>
                                <Text style={styles.textAddImgTit}>Upload Video</Text>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddVideoPop: false });
                                                this.captureImage();
                                            }}>
                                            <View style={styles.containerMaleStyle2}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/photo.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Camera</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddVideoPop: false });
                                                if (Platform.OS === 'android') {
                                                    this.pickImagesFromGallery();
                                                } else {
                                                    this.pickImagesFromGalleryIOS();
                                                }
                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Gallery</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },

    textIntro: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },

    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    infoImageStyle: {
        width: 25,
        height: 25,
        marginTop: 10,
        alignSelf: 'center',
    },
    viewTitleCal: {
        height: '85%',
        flexDirection: 'column',
        padding: 30,
        justifyContent: 'center',
        alignContent: 'center',

    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    image: {
        width: wp('49%'),
        height: hp('20%'),
        borderRadius: 5,
        alignSelf: 'center',
    },

    mainView: {
        flex: 1,
        flexDirection: 'column',
    },
    header: {
        width: wp('100%'),
        height: 60,
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    headerInner: {
        width: wp('50%'),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },

    headerSel: {
        width: wp('50%'),
        height: 3,
        marginTop: 10,
    },

    playImageStyle: {
        width: 20,
        height: 20,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 10,
        left: 20,
    },
    postPlayImageStyle: {
        width: 60,
        height: 60,
        justifyContent: 'center',

    },
    textWorkName: {
        fontSize: 13,
        fontWeight: '500',
        lineHeight: 18,
        fontFamily: 'Rubik-Medium',
        color: '#6D819C',
    },

    textrank: {
        fontSize: 13,
        fontWeight: '500',
        lineHeight: 18,
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    viewOraDot: {
        alignSelf: 'center',
    },
    profileImage: {
        width: 40,
        height: 40,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 45 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start'
    },

    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        flexDirection: 'column',
        marginBottom: 10,
    },
    claimReward: {
        width: '90%',
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        marginBottom: 10,
        marginTop: 10,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },

    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },

    mediaPlayer: {
        width: '100%',
        height: 215,
        position: 'relative',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        borderWidth: .1,
        borderRadius: 10,
        backgroundColor: 'black',
        marginTop: '15%',
    },

    playButton: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 70,

    },
    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginTop: 5,// approximate a square
    },
    column: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 10,
    },
    textStyle: {
        color: 'white',
        fontFamily: 'Rubik-Medium',
        fontSize: RFValue(10, screenHeight),
        fontWeight: '500',
        letterSpacing: 0.23,
        textAlign: 'right',
        paddingLeft: 7,
        paddingRight: 7,
        paddingBottom: 3,
        paddingTop: 3,
    },

    aroundListStyle: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        padding: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    goalBg: {
        width: wp('95%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
        marginBottom: 10,
    },
    goalDet: {
        width: wp('45%'),
        padding: 15,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'flex-start',
    },

    textWeight: {
        fontSize: 18,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        alignSelf: 'center'
    },

    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,

    },
    imgUnSuscribe: {
        width: 250,
        height: 250,
        alignSelf: 'center',
    },
    textAddImgTit: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    containerMaleStyle1: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle2: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    infoAppBlue: {
        width: 25,
        height: 25,
        alignSelf: 'center',
    },
    textPopTitle: {
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 5,
    },
    textPopDesc: {

        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        alignSelf: 'flex-start',
        color: '#6d819c',
    },
    textPopDescBlack: {

        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        alignSelf: 'flex-start',
        color: '#2d3142',
    },
    linearGradientTaskInfo: {
        width: wp('25%'),
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
        borderRadius: 15,
    },
    textTaskInfoSave: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
});

const mapStateToProps = state => {
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(AcceptedChallengeDetails),
);
