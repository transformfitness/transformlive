import React, { Component } from 'react';
import {
    FlatList,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    Platform,
    Dimensions,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals } from '../actions';
import { ProgressBar, Loader, ProgressBar1 } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const sliderWidth = wp('100%');
const itemWidth = wp('90%');
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
const mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
import { CustomDialog } from './common';
import ImagePicker from 'react-native-image-picker';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import DocumentPicker from 'react-native-document-picker';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { RNS3 } from 'react-native-aws3';
import RNFetchBlob from 'rn-fetch-blob';
import RBSheet from "react-native-raw-bottom-sheet";

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

const createFormData = (video, body) => {
    const data = new FormData();
    LogUtils.infoLog1("video url", video);
    if (video) {
        data.append('file', {
            uri: video,
            name: video
                .toString()
                .replace(
                    'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
                    '',
                ),
            type: 'video/mp4', // or photo.type
        });
    }
    data.append('data', body);
    return data;
};

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 1.15;

class Trainee_Challenges extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            tab: 1,
            currentIndex: 0,
            viewIndex: 0,
            resObj: '',
            resMyChalObj: '',
            isSuccess: false,
            refresh: false,
            isAlert:false,
            alertTitle: '',
            isAcceptShow: 0,
            isProgress: false,
            progress: 0,
            selitem: '',
            isAccChallenge: false,
            isAddVideoPop: false,
            post: '',
            filepath: {
                data: '',
                uri: '',
            },
            fileData: '',
            fileUri: '',
            fileName: '',
            fileType: '',
            isMale: 1,
            activeSlide1: 0,
            isConfirm: false, 
            confirmMsg: '' ,
            confirmTitle:''
        };
        this.getChallengesDetails = this.getChallengesDetails.bind(this);
        this.getUserMyChallenges = this.getUserMyChallenges.bind(this);
    }

    renderMonth = (date) => {
        var res = date.split("-", 2);
        var n = (res[1].replace(/0/, ""));
        return <Text
            style={styles.textMonth}>{mlist[n - 1]}</Text>;
    }

    renderdate = (date) => {
        var res = date.split("-", 1);
        return <Text style={styles.textDate}>{res}</Text>;
    }

    async componentDidMount() {
        try {
            allowFunction();

            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.setState({ loading: true });
                    this.getChallengesDetails();
                    this.getUserMyChallenges();
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
                }
            });

            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {
            console.log(error);
        }
    }


    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isAddVideoPop) {
            this.setState({ isAddVideoPop: false });
        }
        else if (this.state.isAccChallenge) {
            Actions.traineeHome();
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    async getChallengesDetails() {
        try {
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/getchallenge`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        page: 1,
                        pagesize: 20
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1("data", data);
                    LogUtils.infoLog1("Challenge Data", data.data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ resObj: data });
                        // LogUtils.infoLog1('User_videos', data.data[0].user_videos);
                        if (data.data.length > 0) {
                            const interval = setInterval(() => {
                                data.data.map((item, index) => {
                                    if (this.props.cid) {
                                        if (item.c_id === this.props.cid) {
                                            this.setState({ currentIndex: index });
                                            LogUtils.infoLog1("index", index);
                                            if (data.data.length > 1) {
                                                setTimeout(() => 
                                                this._carousel1.snapToItem(index, true)
                                                ,100)
                                                this.setState({ loading: false });
                                                //this._carousel1.snapToItem(index, true);
                                            }
                                            // setTimeout(() => this.carousel.snapToItem(this.state.currentIndex), 250)
                                        }else{
                                             this.setState({ loading: false });
                                        }
                                        clearInterval(interval);
                                    }else{
                                             this.setState({ loading: false });
                                        }
                                })
                            }, 1000);

                            if (data.data[this.state.currentIndex].is_accept === 1) {
                                this.setState({ isAcceptShow: 1, selitem: data.data[this.state.currentIndex] });
                            }
                            else {
                                this.setState({ isAcceptShow: 0 });
                            }
                        }else{
                            this.setState({ loading: false });
                        }
                        // if (data.data[this.state.currentIndex].is_accept === 1)
                        //     this.setState({ isAcceptShow: 1 });
                        // else
                        //     this.setState({ isAcceptShow: 0 });

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        }
        catch (error) {
            console.log(error);
        }
    }

    async getUserMyChallenges() {
        try {
            this.setState({ refresh: !this.state.refresh });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/getmychallenges`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        page: 1,
                        pagesize: 20
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1("data", data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, resMyChalObj: data.data });
                        // LogUtils.infoLog1('this.state.currentIndex',this.state.currentIndex);
                        // if (data.data.length > 0) {
                        //     LogUtils.infoLog1('this.state.currentIndex',this.state.currentIndex);
                        //     if (data.data[this.state.currentIndex].is_accept === 1) {
                        //         this.setState({ isAcceptShow: 1 });
                        //         // this._carousel1.snapToItem(this.state.currentIndex,true)
                        //     }
                        //     else
                        //         this.setState({ isAcceptShow: 0 });
                        // }

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async saveChallengeVideo() {
        try{
        let token = await AsyncStorage.getItem('token');
        futch(`${BASE_URL}/trainee/saveuserchlngvideo`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${token}`,
            },
            body: createFormData(this.state.fileUri, JSON.stringify({
                uc_id: this.state.selitem.uc_id,
                c_id: this.state.selitem.c_id,
                comments: this.state.post,
            })),
        }, (progressEvent) => {
            const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            this.setState({ progress: progress });
        })
            .then(res => {
                this.setState({ isProgress: false })
                const statusCode = res.status;
                LogUtils.infoLog1('statusCode', statusCode);
                const data = JSON.parse(res.response);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: true, isSuccess: true, alertMsg: data.message, alertTitle: 'Success' });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
                this.setState({ isProgress: false })
            })
            .catch(function (error) {
                LogUtils.infoLog1('upload error', error);
                feedPostError(dispatch, error);
            });
        } catch (error) {
            console.log(error);
        }
    }

    changeIndex = (currentIndex) => {
        LogUtils.infoLog1('currentIndex', currentIndex);
        this.setState({ currentIndex });
        this.setState({ viewIndex: currentIndex });

        if (this.state.resObj.data[currentIndex].is_accept === 1) {
            this.setState({ isAcceptShow: 1, selitem: this.state.resObj.data[currentIndex] });
        }
        else {
            this.setState({ isAcceptShow: 0 });
        }
    }

    async onAccept() {
        try {
            if (this.state.alertMsg === 'You are not authenticated!') {
                AsyncStorage.clear().then(() => {
                    Actions.auth({ type: 'reset' });
                });
            }

            this.setState({ isAlert: false, alertMsg: '' });

            if (this.state.isSuccess) {
                this.setState({ loading: true });
                this.getChallengesDetails();
                this.getUserMyChallenges();
                this.setState({ isSuccess: false })
            }
        } catch (error) {
            console.log(error);
        }
    }

    async saveUserChallenge() {
        try{
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/saveuserchallenge`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    c_id: this.state.resObj.data[this.state.currentIndex].c_id
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: true, alertMsg: data.message, isSuccess: true, alertTitle: data.title, isAccChallenge: true });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
        } catch (error) {
            console.log(error);
        }
    }

    chooseImage = () => {
        let options = {
            title: 'Video Picker',
            takePhotoButtonTitle: 'Record Video...',
            mediaType: 'video',
            videoQuality: 'medium',
            durationLimit: 120,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                LogUtils.infoLog1('response', JSON.stringify(response));
                LogUtils.infoLog1('fileUri', response.uri);
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.setState({ isProgress: true })
                this.saveChallengeVideo();
            }
        });
    };

    launchCamera = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                LogUtils.infoLog1('User tapped custom button: ', response.customButton);
            } else {
                LogUtils.infoLog1('launchCamera fileUri', response.uri);
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
            }
        });
    };

    launchImageLibrary = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            // LogUtils.infoLog1('Response = ', response);

            if (response.didCancel) {
                // LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                // LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // LogUtils.infoLog1('User tapped custom button: ', response.customButton);
            } else {
                // LogUtils.infoLog1('launchImageLibrary fileUri', response.uri);
                this.props.videoUri = '';
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

            }
        });
    };

    renderChallengesProfiles(item) {
        try{
            if (Array.isArray(item.user_videos) && item.user_videos.length) {
                if (item.user_videos.length === 1) {
                    let arrProfiles = item.user_videos.map((item, i) => {
                        return <View key={i+"videos"} style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.profile_img }}
                                style={styles.chaProfile} />
                        </View>
                    });
                    return (
                        <View>
                            {arrProfiles}
                        </View>
                    );
                }
                else if (item.user_videos.length === 2) {
                    let arrProfiles = item.user_videos.map((item, i) => {
                        return <View key={i+"userVideos"} style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.profile_img }}
                                style={styles.chaProfile} />
                        </View>
                    });
                    return (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {arrProfiles}
    
                            {item.chlng_usr_cnt !== 0
                                ?
                                (
                                    <View style={styles.chaMore}>
                                        <Text style={styles.chaMoreText}>+{`${item.chlng_usr_cnt}`}</Text>
                                    </View>
                                )
                                :
                                (
                                    <View></View>
                                )
                            }
    
                        </View>
                    );
                }
                // else {
                //     let arrProfiles = item.user_videos.map((item, i) => {
                //         if (i < 2) {
                //             return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                //                 <Image
                //                     progressiveRenderingEnabled={true}
                //                     resizeMethod="resize"
                //                     source={{ uri: item.profile_img }}
                //                     style={styles.chaProfile} />
                //             </View>
                //         }
                //     });
                //     return (
                //         <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center', }}>
                //             {arrProfiles}
                //             <View style={styles.chaMore}>
                //                 <Text style={styles.chaMoreText}>+{item.user_videos.length - 2}</Text>
                //             </View>
                //         </View>
                //     );
                // }
            }
            else {
                return (
                    <View style={{
                        flexDirection: 'row', marginTop: 7, alignItems: 'center', width: 30,
                        height: 30,
                    }}>

                    </View>
                );
            }
        }catch(error){
            console.log(error)
        }
    }

    renderListorText() {
        if (this.state.resMyChalObj.length > 0) {
            return (
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.resMyChalObj}
                    keyExtractor={(item, index) => "resMyChalObj"+index}
                    extraData={this.state.refresh}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        return <View style={styles.flatcontainerListStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.image }}
                                style={{
                                    width: wp('90%'),
                                    height: 180,
                                    borderTopLeftRadius: 8,
                                    borderTopRightRadius: 8,
                                    resizeMode: 'cover',
                                }}
                            />
                            <View style={styles.viewChallNames}>
                                <View style={styles.viewChallDate}>
                                    {this.renderMonth(item.cdate)}
                                    {this.renderdate(item.cdate)}
                                </View>
                                <View style={{ flexDirection: 'column', alignSelf: 'flex-start', paddingLeft: 15, width: wp('65%'), }}>
                                    <Text style={styles.textValue}>{item.title}</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                        <Text style={styles.textTitle}>{item.from_date}</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_arr_right.png')}
                                            style={{
                                                width: 14,
                                                alignSelf: 'center',
                                                marginRight: 10,
                                                marginLeft: 10,
                                                tintColor: '#6d819c',
                                                height: 12,
                                            }} />
                                        <Text style={styles.textTitle}>{item.to_date}</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', margin: 5, }}>
                                <View style={styles.viewDetailsMain1}>
                                    <TouchableOpacity style={styles.viewDetailsLayout}
                                        onPress={() => Actions.accChallenge({ cid: item.c_id, uc_id: item.uc_id })}>
                                        <Text style={styles.textDetailsVideo}>View Details</Text>
                                    </TouchableOpacity>
                                </View>

                                {item.is_expired === 0
                                    ?
                                    (
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <View style={{ width: 5, }}></View>
                                            <View style={styles.viewDetailsMain1}>
                                                <TouchableOpacity style={styles.viewDetailsLayout}
                                                    onPress={() => {
                                                        if (item.chlng_cnt < item.max_limit) {
                                                            this.setState({ selitem: item });
                                                            // this.setState({ isAddVideoPop: true });

                                                            if (Platform.OS === 'android') {
                                                                this.pickImagesFromGallery();
                                                            } else {
                                                                this.pickImagesFromGalleryIOS();
                                                            }
                                                        }
                                                        else {
                                                            this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `You can upload upto ${item.max_limit} videos per day` });
                                                        }
                                                    }}>
                                                    <Text style={styles.textDetailsVideo}>Upload Video</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    )
                                    :
                                    (
                                        <View>
                                        </View>
                                    )
                                }

                                <View style={{ width: 5, }}></View>
                                <View style={styles.viewDetailsMain1}>
                                    <TouchableOpacity style={styles.viewDetailsLayout}
                                        onPress={() => Actions.accChallenge({ cid: item.c_id, uc_id: item.uc_id, flag: 'Leaderboard' })}>
                                        <Text style={styles.textDetailsVideo}>Leaderboard</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }} />
            );
        }
        else {
            if (this.state.loading === false) {
                return (
                    <View style={styles.viewTitleCal1}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/img_no_challenges.png')}
                            style={styles.imgUnSuscribe}
                        />
                        <Text style={styles.textCreditCard}></Text>
                        <Text style={styles.textDesc}>You haven't accepted any challenges yet</Text>
                    </View>
                );
            }
        }
    }

    get pagination1() {
        // const { entries, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={this.state.resObj.data.length}
                activeDotIndex={this.state.viewIndex}
                containerStyle={{ paddingVertical: 10 }}
                dotContainerStyle={{
                    // backgroundColor: '#ffffff',
                    width: 10,
                    height: 10,
                    // marginTop: -20,
                    // marginBottom: -20
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#8c52ff'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#b0b0b0',
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={0.8}
            />
        );
    }

    renderTabs() {
        if (this.state.tab === 1) {
            return (
                <View style={styles.introDerails}>
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({ tab: 1 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>CHALLENGES</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ tab: 2 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>MY CHALLENGES</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
            )
        }
        else if (this.state.tab === 2) {
            return (
                <View style={styles.introDerails}>
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({ tab: 1 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>CHALLENGES</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ tab: 2 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>MY CHALLENGES</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                    {this.renderListorText()}
                </View>
            );
        }
    }

    onItemClicked(item) {
        if (item.is_accept === 1) {
            Actions.accChallenge({ cid: item.c_id, uc_id: item.uc_id })
        } else {
            Actions.challengedetails({ cid: item.c_id })
        }
    }

    renderLeaderBorad(item) {
        try {
            if ((Array.isArray(item.male_ranks) && item.male_ranks.length > 0) && (Array.isArray(item.female_ranks) && item.female_ranks.length > 0)) {
                return (
                    <View>

                        <View style={{ flexDirection: 'row', alignSelf: 'center', padding: 15 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'center', width: '40%', justifyContent: 'center' }}
                                onPress={() => {
                                    this.setState({ isMale: 1 });
                                }}>

                                {this.state.isMale === 1
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_box_check.png')}
                                            style={{ width: 17, height: 17, alignSelf: 'center', }} />

                                    )
                                    : (

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_box_uncheck.png')}
                                            style={{ width: 17, height: 17, alignSelf: 'center', }} />

                                    )}

                                <Text style={styles.textMale}>Male</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'center', width: '40%', justifyContent: 'center' }}
                                onPress={() => {
                                    this.setState({ isMale: 0 });
                                }}>
                                {this.state.isMale === 1
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_box_uncheck.png')}
                                            style={{ width: 17, height: 17, alignSelf: 'center', }} />

                                    )
                                    : (

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_box_check.png')}
                                            style={{ width: 17, height: 17, alignSelf: 'center', }} />

                                    )}
                                <Text style={styles.textMale}>Female</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.popupHeadTitle}>Leaderboard</Text>
                        {/* {this.renderMyRank(item)} */}

                        {item.is_winner === 1
                            ? (
                                this.renderClaimed(item)

                            )
                            : (

                                <View></View>

                            )}
                        {this.state.isMale === 1
                            ? (
                                item.male_ranks.map((tipDesc, index) => (
                                    <ScrollView>

                                        <View style={styles.aroundListStyle} >

                                            <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                                                <ImageBackground key={tipDesc.rank} source={require('../res/ic_rank_bg.png')} style={{ width: 23, height: 26, alignSelf: 'center', marginRight: 15, alignItems: 'center', }}>
                                                    <View style={styles.viewOraDot}>
                                                        <Text style={styles.textrank}>{`${tipDesc.rank}`}</Text>
                                                    </View>
                                                </ImageBackground>
                                            </View>

                                            <View style={{ width: '11%', alignItems: 'center', justifyContent: 'center' }}>
                                                {tipDesc.user_img
                                                    ? (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={{ uri: tipDesc.user_img }}
                                                            style={styles.profileImage}
                                                        />
                                                    )
                                                    : (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_noimage.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    )
                                                }

                                            </View>
                                            <View style={{ width: wp('50%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.textWorkName}>{`${tipDesc.user_name}`}</Text>

                                            </View>

                                            <View style={{ width: wp('20%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'center', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.textWorkName}>{`${tipDesc.rating}`}</Text>
                                                <Text style={styles.textWorkName}>Points</Text>

                                            </View>
                                        </View>
                                    </ScrollView>
                                ))

                            )
                            : (

                                item.female_ranks.map((tipDesc, index) => (
                                    <ScrollView>

                                        <View style={styles.aroundListStyle} >

                                            <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                                                <ImageBackground key={tipDesc.rank} source={require('../res/ic_rank_bg.png')} style={{ width: 23, height: 26, alignSelf: 'center', marginRight: 15, alignItems: 'center', }}>
                                                    <View style={styles.viewOraDot}>
                                                        <Text style={styles.textrank}>{`${tipDesc.rank}`}</Text>
                                                    </View>
                                                </ImageBackground>
                                            </View>

                                            <View style={{ width: '11%', alignItems: 'center', justifyContent: 'center' }}>
                                                {tipDesc.user_img
                                                    ? (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={{ uri: tipDesc.user_img }}
                                                            style={styles.profileImage}
                                                        />
                                                    )
                                                    : (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_noimage.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    )
                                                }

                                            </View>
                                            <View style={{ width: wp('55%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.textWorkName}>{`${tipDesc.user_name}`}</Text>

                                            </View>

                                            <View style={{ width: wp('20%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'center', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.textWorkName}>{`${tipDesc.rating}`}</Text>
                                                <Text style={styles.textWorkName}>Points</Text>

                                            </View>



                                        </View>
                                    </ScrollView>
                                ))

                            )}


                    </View>

                );
            } else if (Array.isArray(item.male_ranks) && item.male_ranks.length > 0) {
                return (
                    <View>


                        <Text style={styles.popupHeadTitle}>Leaderboard</Text>
                        {/* {this.renderMyRank(item)} */}

                        {item.is_winner === 1
                            ? (
                                this.renderClaimed(item)

                            )
                            : (

                                <View></View>

                            )}

                        {item.male_ranks.map((tipDesc, index) => (
                            <ScrollView>

                                <View style={styles.aroundListStyle} >

                                    <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                                        <ImageBackground source={require('../res/ic_rank_bg.png')} style={{ width: 23, height: 26, alignSelf: 'center', marginRight: 15, alignItems: 'center', }}>
                                            <View style={styles.viewOraDot}>
                                                <Text style={styles.textrank}>{`${tipDesc.rank}`}</Text>
                                            </View>
                                        </ImageBackground>
                                    </View>

                                    <View style={{ width: '11%', alignItems: 'center', justifyContent: 'center' }}>
                                        {tipDesc.user_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: tipDesc.user_img }}
                                                    style={styles.profileImage}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={styles.profileImage}
                                                />
                                            )
                                        }

                                    </View>
                                    <View style={{ width: wp('55%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${tipDesc.user_name}`}</Text>

                                    </View>

                                    <View style={{ width: wp('20%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'center', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${tipDesc.rating}`}</Text>
                                        <Text style={styles.textWorkName}>Points</Text>

                                    </View>



                                </View>
                            </ScrollView>
                        ))}

                    </View>

                );
            } else if (Array.isArray(item.female_ranks) && item.female_ranks.length > 0) {
                return (
                    <View>


                        <Text style={styles.popupHeadTitle}>Leaderboard</Text>
                        {/* {this.renderMyRank(item)} */}

                        {item.is_winner === 1
                            ? (
                                this.renderClaimed(item)

                            )
                            : (

                                <View></View>

                            )}

                        {item.female_ranks.map((tipDesc, index) => (
                            <ScrollView>

                                <View style={styles.aroundListStyle} >

                                    <View style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
                                        <ImageBackground source={require('../res/ic_rank_bg.png')} style={{ width: 23, height: 26, alignSelf: 'center', marginRight: 15, alignItems: 'center', }}>
                                            <View style={styles.viewOraDot}>
                                                <Text style={styles.textrank}>{`${tipDesc.rank}`}</Text>
                                            </View>
                                        </ImageBackground>
                                    </View>

                                    <View style={{ width: '11%', alignItems: 'center', justifyContent: 'center' }}>
                                        {tipDesc.user_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: tipDesc.user_img }}
                                                    style={styles.profileImage}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={styles.profileImage}
                                                />
                                            )
                                        }

                                    </View>
                                    <View style={{ width: wp('55%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${tipDesc.user_name}`}</Text>

                                    </View>

                                    <View style={{ width: wp('20%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'center', alignContent: 'center', justifyContent: 'center', }}>

                                        <Text style={styles.textWorkName}>{`${tipDesc.rating}`}</Text>
                                        <Text style={styles.textWorkName}>Points</Text>

                                    </View>



                                </View>
                            </ScrollView>
                        ))}

                    </View>

                );
            }
            else {
                <View></View>
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderClaimed(item) {
        try {
            if (item.is_detgiven === 1) {
                return (
                    <View style={styles.claimReward}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <View
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>Reward Claimed </Text>
                            </View>
                        </LinearGradient>

                    </View>
                );

            } else {
                return (
                    <View style={styles.claimReward}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <TouchableOpacity
                                onPress={() => {
                                    LogUtils.infoLog1("this.state.challengeWinners:", item);
                                    // if (this.state.challengeWinners.is_coupon === 1) {
                                    //     Actions.chaRewards({ mFrom: 'coupon', cid: this.props.cid });
                                    // } else {
                                    //     Actions.chaRewards({ mFrom: 'product', cid: this.props.cid, weight: this.state.challengeWinners.weight, pincode: this.state.challengeWinners.pincode });
                                    // }
                                    this.bottompopup.close();
                                    setTimeout(() => Actions.chaRewards({ mFrom: 'product', cid: item.c_id, weight: item.weight, pincode: item.pincode }), 250)

                                }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>Claim Reward</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                    </View>
                );
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderMyRank(item) {
        try {
            if (item.myrank) {
                return (
                    <View style={styles.goalBg}>
                        <View
                            style={styles.goalDet}>
                            <Text style={styles.textWeight}>Your Rank</Text>
                        </View>
                        <View style={{ width: 1, backgroundColor: '#e9e9e9' }}></View>
                        <View style={styles.goalDet}>
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                <Text style={styles.textWeight}>{item.myrank}</Text>

                            </View>

                        </View>
                    </View>
                );
            } else {
                return (
                    <View></View>
                );

            }
        } catch (error) {
            console.log(error);
        }
    }

    renderTabData() {
        try {
            if (this.state.tab === 1) {
                if (!isEmpty(this.state.resObj)) {
                    if (Array.isArray(this.state.resObj.data) && this.state.resObj.data.length) {
                        return (
                            <View>
                                {this.pagination1}
                                <Carousel style={{ width: wp('90%'), }}
                                    data={this.state.resObj.data}
                                    renderItem={({ item,index }) => {
                                        return <ScrollView>
                                            <View style={styles.containerListStyle}>

                                                <TouchableOpacity activeOpacity={0.8}
                                                    key={"data"+index}
                                                    onPress={() => {
                                                        if (item.chlng_video) {
                                                            LogUtils.firebaseEventLog('video', {
                                                                p_id: item.c_id,
                                                                p_category: 'Challenges',
                                                                p_name: `Start - ${item.title} Video`,
                                                            });

                                                            if (Platform.OS === 'android') {
                                                                Actions.newvideoplay({ videoURL: item.chlng_video, from: 'challenges', label: `End - ${item.title} Video` });
                                                                // Actions.customPlayer({ videoURL: item.chlng_video, from: 'plandetails', label: `End - ${item.title} Video` });
                                                            } else {
                                                                Actions.afPlayer({ videoURL: item.chlng_video, from: 'plandetails', label: `End - ${item.title} Video` });
                                                            }
                                                        }
                                                    }
                                                    }>

                                                    <View>
                                                        {item.image
                                                            ? (
                                                                <Image
                                                                    progressiveRenderingEnabled={true}
                                                                    resizeMethod="resize"
                                                                    source={{ uri: item.image }}
                                                                    style={{
                                                                        width: wp('90%'),
                                                                        height: hp('27%'),
                                                                        borderTopLeftRadius: 8,
                                                                        borderTopRightRadius: 8,
                                                                        resizeMode: 'cover',
                                                                    }}
                                                                />
                                                            )
                                                            : (
                                                                <Image
                                                                    progressiveRenderingEnabled={true}
                                                                    resizeMethod="resize"
                                                                    source={require('../res/ic_noimage.png')}
                                                                    style={{
                                                                        width: wp('90%'),
                                                                        height: hp('27%'),
                                                                        borderTopLeftRadius: 8,
                                                                        borderTopRightRadius: 8,
                                                                        resizeMode: 'cover',
                                                                    }}
                                                                />
                                                            )
                                                        }

                                                        {item.chlng_video
                                                            ?
                                                            (
                                                                <View style={styles.playContainerProgram}>
                                                                    <TouchableOpacity
                                                                        style={{ flexDirection: 'row', }}
                                                                        onPress={() => {
                                                                            // this.onItemClicked(item)
                                                                        }}>

                                                                    </TouchableOpacity>

                                                                    <View style={{ flex: 1 }}>
                                                                    </View>

                                                                    <TouchableOpacity
                                                                        style={{ flexDirection: 'row' }}
                                                                        activeOpacity={0.8}
                                                                        onPress={() => {

                                                                            LogUtils.firebaseEventLog('video', {
                                                                                p_id: item.c_id,
                                                                                p_category: 'Challenges',
                                                                                p_name: `Start - ${item.title} Video`,
                                                                            });

                                                                            if (Platform.OS === 'android') {
                                                                                Actions.newvideoplay({ videoURL: item.chlng_video, from: 'challenges', label: `End - ${item.title} Video` });
                                                                                // Actions.customPlayer({ videoURL: item.chlng_video, from: 'plandetails', label: `End - ${item.title} Video` });
                                                                            } else {
                                                                                Actions.afPlayer({ videoURL: item.chlng_video, from: 'plandetails', label: `End - ${item.title} Video` });
                                                                            }
                                                                        }}>
                                                                        <Image
                                                                            progressiveRenderingEnabled={true}
                                                                            resizeMethod="resize"
                                                                            source={require('../res/ic_play_purple.png')}
                                                                            style={styles.playIcon} />
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                            :
                                                            (
                                                                <View></View>
                                                            )
                                                        }
                                                    </View>

                                                </TouchableOpacity>


                                                <View style={styles.viewChallNames}>
                                                    <View style={styles.viewChallDate}>
                                                        {this.renderMonth(item.cdate)}
                                                        {this.renderdate(item.cdate)}
                                                    </View>
                                                    <View style={{ flexDirection: 'column', alignSelf: 'flex-start', paddingLeft: 15, width: wp('65%'), }}>
                                                        <Text style={styles.textValue}>{item.title}</Text>
                                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                            <Text style={styles.textTitle}>{item.from_date}</Text>
                                                            <Image
                                                                progressiveRenderingEnabled={true}
                                                                resizeMethod="resize"
                                                                source={require('../res/ic_arr_right.png')}
                                                                style={{
                                                                    width: 14,
                                                                    alignSelf: 'center',
                                                                    marginRight: 10,
                                                                    marginLeft: 10,
                                                                    tintColor: '#6d819c',
                                                                    height: 12,
                                                                }} />
                                                            <Text style={styles.textTitle}>{item.to_date}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{ width: wp('90%'), height: 1, backgroundColor: '#e9e9e9', }}></View>
                                                {/* <ScrollView style={styles.scrollChaDesc}> */}
                                                <Text style={styles.textChaDesc}>{item.description}</Text>
                                                {item.rewards
                                                    ?
                                                    (
                                                        <View>
                                                            <Text style={styles.textTitleHead}>Rewards :</Text>
                                                            {item.rewards.split('|').map((tipDesc, index) => (
                                                                <View style={styles.viewListAllInner} key={index+"Rewards"}>
                                                                    <View style={styles.viewBlackDot}></View>
                                                                    <Text style={styles.textDescRules}>{tipDesc}</Text>
                                                                </View>
                                                            ))}
                                                        </View>
                                                    )
                                                    :
                                                    (
                                                        <View></View>
                                                    )
                                                }
                                                <View style={{ width: wp('90%'), height: 10, }}></View>
                                                {item.remarks
                                                    ?
                                                    (
                                                        <View>
                                                            <Text style={styles.textTitleHead}>Rules :</Text>
                                                            {item.remarks.split('|').map((tipDesc, index) => (
                                                                <View style={styles.viewListAllInner} key={"rules"+index}>
                                                                    <View style={styles.viewBlackDot}></View>
                                                                    <Text style={styles.textDescRules}>{tipDesc}</Text>
                                                                </View>
                                                            ))}
                                                        </View>
                                                    )
                                                    :
                                                    (
                                                        <View>
                                                        </View>
                                                    )
                                                }
                                                <View style={{ width: wp('90%'), height: 10, }}></View>

                                                <View style={{ width: wp('80%'), flexDirection: 'row', alignSelf: 'flex-start', alignItems: 'center', justifyContent: 'flex-start', padding: 10, backgroundColor: '#ffffff', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, }}>

                                                    {this.renderChallengesProfiles(item)}
                                                    <View style={styles.viewIndicator}>
                                                        <Text style={styles.textIndicator}>{item.sno} / {this.state.resObj.data.length}</Text>
                                                    </View>
                                                </View>

                                            </View>
                                        </ScrollView>
                                    }}
                                    itemWidth={itemWidth}
                                    sliderWidth={sliderWidth}
                                    onSnapToItem={this.changeIndex.bind(this)}
                                    separatorWidth={0}
                                    inActiveScale={0.95}
                                    ref={(c1) => {
                                        this._carousel1 = c1;
                                    }}
                                //pagingEnable={false}
                                />

                            </View>

                        );
                    }
                    else {
                        if (this.state.loading === false) {
                            return (
                                <View style={styles.viewTitleCal}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/img_no_challenges.png')}
                                        style={styles.imgUnSuscribe}
                                    />
                                    <Text style={styles.textCreditCard}>{this.state.resObj.title}</Text>

                                    <Text style={styles.textDesc}>{this.state.resObj.message}</Text>

                                </View>
                            );
                        }
                    }
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderBottom() {
        try {
            if (this.state.loading === false) {
                if (!isEmpty(this.state.resObj)) {
                    if (Array.isArray(this.state.resObj.data) && this.state.resObj.data.length) {
                        if (this.state.tab === 1 && this.state.isAcceptShow === 0) {
                            return (
                                <View style={styles.bottom}>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                this.saveUserChallenge()}
                                            style={styles.buttonTuch}>
                                            <Text style={styles.buttonText}>Accept Challenge</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                </View>
                            );
                        } else if (this.state.tab === 1 && this.state.isAcceptShow === 1) {
                            return (

                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{
                                    width: '89%',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    bottom: 0,
                                    alignSelf: 'center',
                                    borderWidth: 1,
                                    borderColor: '#eee',
                                    marginBottom: 15,
                                    borderRadius: 15,
                                }}>
                                    <View style={styles.viewDetailsMain}>
                                        <TouchableOpacity style={styles.viewDetailsLayout1}
                                            onPress={() => {
                                                if (this.state.selitem.is_expired === 0) {
                                                    if (this.state.selitem.chlng_cnt < this.state.selitem.max_limit) {
                                                        if (Platform.OS === 'android') {
                                                            this.pickImagesFromGallery();
                                                        } else {
                                                            this.pickImagesFromGalleryIOS();
                                                        }
                                                    } else {
                                                        this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `You can upload upto ${this.state.selitem.max_limit} videos per day` });

                                                    }
                                                } else {
                                                    this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: this.state.selitem.expire_text });

                                                }

                                                // Actions.accChallenge({ cid: item.c_id, uc_id: item.uc_id })
                                            }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_ch_upldvideo1.png')}
                                                style={styles.chaInfoIcon} />
                                            <Text style={styles.textDetailsVideo1}>Upload Video</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.viewDetailsMain}>
                                        <TouchableOpacity style={styles.viewDetailsLayout1}
                                            onPress={() => {
                                                if ((Array.isArray(this.state.selitem.male_ranks) && this.state.selitem.male_ranks.length > 0) || (Array.isArray(this.state.selitem.female_ranks) && this.state.selitem.female_ranks.length > 0)) {
                                                    this.bottompopup.open()
                                                } else {
                                                    this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: this.state.selitem.leaderbrdmsg });
                                                }
                                            }
                                            }>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_ch_leaderborad.png')}
                                                style={styles.chaInfoIcon} />
                                            <Text style={styles.textDetailsVideo1}>Leaderboard</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.viewDetailsMain}>
                                        <TouchableOpacity style={styles.viewDetailsLayout1}
                                            onPress={() => {
                                                if (this.state.selitem.my_video) {
                                                    // if (Platform.OS === 'android') {
                                                    Actions.newvideoplay({ item: this.state.selitem, from: 'challengedetails' });
                                                    // Actions.customPlayer({ item: item, from: 'challengedetails' });
                                                    // } else {
                                                    //     Actions.afPlayer({ item: this.state.selitem, from: 'challengedetails' });
                                                    // }
                                                } else {
                                                    this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `You haven't uploaded any videos in this challenge` });
                                                }

                                            }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_ch_myvideos.png')}
                                                style={styles.chaInfoIcon} />
                                            <Text style={styles.textDetailsVideo1}>My Video</Text>
                                        </TouchableOpacity>
                                    </View>
                                </LinearGradient>

                                // <View style={styles.bottom}>
                                //     <Text style={styles.acceptedText}>Accepted</Text>
                                // </View>
                            );
                            // return (
                            //     <View style={styles.bottom}>
                            //         <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            //             <TouchableOpacity
                            //                 onPress={() =>
                            //                     this.saveUserChallenge()}
                            //                 style={styles.buttonTuch}>
                            //                 <Text style={styles.buttonText}>Accept Challenge</Text>
                            //             </TouchableOpacity>
                            //         </LinearGradient>
                            //     </View>
                            // );
                        }
                    }

                } else {
                    return (
                        <View />
                    );
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    captureImage = () => {
        let options = {
            title: 'Select Video',
            mediaType: 'video',
            storageOptions: {
                skipBackup: true,
                path: 'video',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.setState({ isProgress: true })
                this.saveChallengeVideo();
            }
        });
    };

    async validateVideoSize(fileSize) {
        let max_video_size = await AsyncStorage.getItem('max_video_size');
        var videoSize = parseInt(max_video_size) / 1048576;
        if (fileSize < parseInt(max_video_size)) {
            // this.setState({ isProgress: true })
            // this.saveChallengeVideo();

            this.uploadVideoToAWS();
        }
        else {
            this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `Please make sure your file size should not exceed ${videoSize}MB` });
        }
    }

    async uploadVideoToAWS() {
        try {
            this.setState({ progress: 0, isProgress: true });
            let userId = await AsyncStorage.getItem('userId');
            let type = '', extension = '';
            if ((this.state.fileUri.includes('.mp4') || this.state.fileUri.includes('.MP4'))) {
                type = "video/mp4";
                extension = '.mp4';
            } else if ((this.state.fileUri.includes('.3gp') || this.state.fileUri.includes('.3GP'))) {
                type = "video/3gpp";
                extension = '.3gp';
            } else if ((this.state.fileUri.includes('.mov') || this.state.fileUri.includes('.MOV'))) {
                type = "video/quicktime";
                extension = '.mov';
            } else if ((this.state.fileUri.includes('.avi') || this.state.fileUri.includes('.AVI'))) {
                type = "video/x-msvideo";
                extension = '.avi';
            } else if ((this.state.fileUri.includes('.wmv') || this.state.fileUri.includes('.WMV'))) {
                type = "video/x-ms-wmv";
                extension = '.wmv';
            }
            else {
                // LogUtils.infoLog1('file : ', this.state.fileName);
                if (this.state.fileName) {
                    const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
                    //   LogUtils.infoLog1(pluginName, fileExtension);
                    type = this.state.fileType;
                    extension = '.' + fileExtension.toLowerCase();
                }
            }

            const file = {
                // `uri` can also be a file system path (i.e. file://)
                uri: this.state.fileUri,
                name: userId + '_' + new Date().getTime() + extension,
                type: type
            }

            const options = {
                keyPrefix: await AsyncStorage.getItem('s3_chlng_path'),
                bucket: await AsyncStorage.getItem('s3_bucket'),
                region: await AsyncStorage.getItem('s3_region'),
                accessKey: await AsyncStorage.getItem('s3_key'),
                secretKey: await AsyncStorage.getItem('s3_secret'),
                successActionStatus: 201
            }
            LogUtils.infoLog1('file : ', file);
            LogUtils.infoLog1('options : ', options);

            RNS3.put(file, options)
                .progress((e) => {
                    const progress = Math.floor((e.loaded / e.total) * 100);
                    this.setState({ progress: progress });
                }).then(response => {
                    if (response.status === 201) {
                        this.setState({ resObjAwsS3: response.body.postResponse });
                        LogUtils.infoLog1('response', response.body);
                        LogUtils.infoLog1('response1', response.body.postResponse);
                        // this.setState({ isProgress: false });
                        // this.setState({ resObjAwsS3: response.body.postResponse });
                        this.saveChallengeVideoAwsS3();
                    }
                    else {
                        LogUtils.infoLog('Failed to upload image to S3');
                        LogUtils.infoLog1('response', response.body);
                        this.setState({ isProgress: false });
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }

    async saveChallengeVideoAwsS3() {
        try {
            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog1('request', JSON.stringify({
                uc_id: this.state.selitem.uc_id,
                c_id: this.state.selitem.c_id,
                comments: this.state.post,
                bucket: this.state.resObjAwsS3.bucket,
                etag: this.state.resObjAwsS3.etag,
                key: this.state.resObjAwsS3.key,
                location: this.state.resObjAwsS3.location
            }));
            fetch(
                `${BASE_URL}/trainee/saveuserchlngvideoS3`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        uc_id: this.state.selitem.uc_id,
                        c_id: this.state.selitem.c_id,
                        comments: this.state.post,
                        bucket: this.state.resObjAwsS3.bucket,
                        etag: this.state.resObjAwsS3.etag,
                        key: this.state.resObjAwsS3.key,
                        location: this.state.resObjAwsS3.location
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1("statusCode", statusCode);
                    LogUtils.infoLog1("data", data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ isProgress: false, isAlert: true, isSuccess: true, alertMsg: data.message, alertTitle: 'Success' });

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                        } else {
                            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ isProgress: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async pickImagesFromGallery() {
        // Pick a single file
        try {
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.video],
            });

            LogUtils.infoLog1('response', response);
            LogUtils.infoLog1('fileUri', response.uri);

            this.setState({
                filePath: response,
                fileData: response.data,
                fileName: response.name,
                fileUri: response.uri,
                fileType: response.type,
            });

            this.validateVideoSize(response.size);

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    async pickImagesFromGalleryIOS() {
        try {
            // Pick a single file
            let options = {
                title: 'Select Video',
                mediaType: 'video',
                storageOptions: {
                    skipBackup: true,
                    path: 'video',
                },
            };
            ImagePicker.launchImageLibrary(options, response => {
                if (response.didCancel) {
                    LogUtils.infoLog('User cancelled image picker');
                } else if (response.error) {
                    LogUtils.infoLog1('ImagePicker Error: ', response.error);
                } else {
                    LogUtils.infoLog1('response', response);
                    LogUtils.infoLog1('fileUri', response.uri);

                    this.setState({
                        filePath: response,
                        fileData: response.data,
                        fileUri: response.uri,
                        fileType: response.type,
                    });

                    if (response.fileSize) {

                        this.validateVideoSize(response.fileSize);
                    } else {
                        RNFetchBlob.fs.stat(response.origURL)
                            .then((stats) => {
                                LogUtils.infoLog1('stats', stats);
                                LogUtils.infoLog1('stats.size', stats.size);
                                this.validateVideoSize(stats.size);

                            })
                            .catch((err) => { LogUtils.infoLog1('err', err); })
                    }
                    // this.setState({ isProgress: true })
                    // this.saveChallengeVideo();
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

    renderProgressBar() {
        if (Platform.OS === 'android') {
            return (
                <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
            );

        } else {
            return (
                <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
            );
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                {this.renderProgressBar()}
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Challenges</Text>
                </View>

                {/* <ScrollView>
                    {this.renderTabs()}
                </ScrollView> */}

                <View style={{ paddingBottom: hp('10%'), }}>

                    {this.renderTabData()}
                </View>

                {this.renderBottom()}

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <RBSheet
                    ref={ref => {
                        this.bottompopup = ref;
                    }}
                    height={popupHeight}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    closeOnPressBack={!this.state.onConfirmClick}
                    onClose={() => {
                        // this.setState({ freeTrailProcess: 1 })
                    }}
                    customStyles={{
                        container: {
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }
                    }}
                >
                    <TouchableOpacity style={{ position: 'absolute', right: 10, top: 5, padding: 5, zIndex: 1 }}
                        onPress={() => {
                            this.bottompopup.close()
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            style={{
                                width: 15,
                                height: 15,
                                alignSelf: 'flex-start',
                                tintColor: 'black',
                            }}
                            source={require('../res/ic_cross_black.png')}
                        />
                    </TouchableOpacity>

                    {this.renderLeaderBorad(this.state.selitem)}

                </RBSheet>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isAddVideoPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isAddVideoPop: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isAddVideoPop}>
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15 }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <View style={{ marginTop: 10, }}></View>
                                <Text style={styles.textAddImgTit}>Upload Video</Text>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddVideoPop: false });
                                                this.captureImage();
                                            }}>
                                            <View style={styles.containerMaleStyle2}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/photo.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Camera</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddVideoPop: false });
                                                if (Platform.OS === 'android') {
                                                    this.pickImagesFromGallery();
                                                } else {
                                                    this.pickImagesFromGalleryIOS();
                                                }
                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Gallery</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>

            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    viewTitleCal: {
        height: '100%',
        flexDirection: 'column',
        padding: 30,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },
    viewTitleCal1: {
        // height: '100%',
        flexDirection: 'column',
        padding: 30,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },

    containerListStyle: {
        width: wp('90%'),
        // height: hp('85%'),
        backgroundColor: '#ffffff',
        alignItems: 'center',
        alignSelf: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 12,
        marginBottom: hp('20%'),
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderWidth: 1, borderColor: '#eee'
    },
    flatcontainerListStyle: {
        width: wp('90%'),
        backgroundColor: '#ffffff',
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 12,
        marginBottom: hp('4%'),
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    viewChallNames: {
        width: wp('90%'),
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        paddingBottom: 5,
        alignContent: 'center',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        borderLeftWidth: 1, borderRightWidth: 1, borderColor: '#eee'
    },
    viewChallDate: {
        width: wp('16%'),
        flexDirection: 'column',
        alignContent: 'center',
        backgroundColor: '#F4F6FA',
        borderRadius: 5,
        alignItems: 'center',
    },
    viewDetailsMain: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        borderRadius: 5,
        alignItems: 'center',
        margin: 1
    },
    viewDetailsMain1: {
        flex: 1,
        flexDirection: 'row',
        alignContent: 'center',
        backgroundColor: '#FFF',
        borderRadius: 5,
        alignItems: 'center',
        margin: 1
    },
    viewDetailsLayout: {
        flex: 1,
        // margin: 5,
        flexDirection: 'column',
        alignContent: 'center',
        backgroundColor: '#8c52ff',
        borderRadius: 5,
        alignItems: 'center',
    },
    viewDetailsLayout1: {
        flex: 1,
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
        paddingTop: 5,
        paddingBottom: 5,
    },

    textMonth: {
        fontSize: 12,
        width: wp('16%'),
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        backgroundColor: '#8c52ff',
        textAlign: 'center',
        color: '#ffffff',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        textAlign: 'center',
        alignSelf: 'center',
        padding: 5,
        letterSpacing: 0.23,
    },
    textDate: {
        fontSize: 18,
        width: wp('16%'),
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        textAlign: 'center',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        padding: 8,
        letterSpacing: 0.23,
    },
    textValue: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 10,
        color: '#282c37',
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textTitleHead: {
        // width: wp('88%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        lineHeight: 18,
    },

    textDetailsVideo: {
        margin: 7,
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        lineHeight: 18,
    },
    textDetailsVideo1: {
        fontSize: 9,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 3,
    },
    textChaDesc: {
        fontSize: 14,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        color: '#2d3142',
        backgroundColor: '#ffffff',
        lineHeight: 20,
        alignSelf: 'flex-start',
        flexWrap: 'wrap',
        paddingBottom: 20,
    },
    scrollChaDesc: {
        // height: hp('30%'),
        fontSize: 14,
        fontWeight: '400',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        color: '#2d3142',
        backgroundColor: '#ffffff',
        lineHeight: 20,
        alignSelf: 'flex-start',
        flexWrap: 'wrap',
    },
    chaProfile: {
        width: 30,
        height: 30,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 30 / 2,
        marginRight: 4,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    chaMore: {
        flexDirection: 'row',
        backgroundColor: '#979797',
        borderRadius: 15,
        paddingLeft: 8,
        paddingTop: 4,
        marginTop: 5,
        paddingBottom: 4,
        paddingRight: 8,
    },
    chaMoreText: {
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewIndicator: {
        flexDirection: 'row',
        backgroundColor: '#E1DDF5',
        borderRadius: 15,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 2,
        paddingTop: 2,
        alignItems: 'center',
        position: 'absolute',
        right: -10,
        alignSelf: 'center',
    },
    textIndicator: {
        fontSize: 11,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        color: '#8c52ff',
        fontWeight: '500',
        lineHeight: 18,
    },

    textIntro: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 10,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 40,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    acceptedText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    introDerails: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
    },
    header: {
        flex: 1,
        width: wp('100%'),
        height: 60,
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    headerInner: {
        flex: 1,
        width: wp('50%'),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    headerSel: {
        width: wp('50%'),
        height: 3,
        marginTop: 10,
    },
    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    imgUnSuscribe: {
        width: 250,
        height: 250,
        alignSelf: 'center',
    },

    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,

    },
    playContainerProgram: {
        height: hp('27%'),
        flexDirection: 'column',
        alignContent: 'center',
        position: 'absolute',
        right: 20,
        // top: 20,
        // bottom: 20,
        // margin: 20,
        // marginBottom: 20,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        marginBottom: 20,
    },
    chaInfoIcon: {
        width: 20,
        height: 20,
        alignSelf: 'center',
        tintColor: '#ffffff'
    },
    textAddImgTit: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    containerMaleStyle1: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle2: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    infoAppBlue: {
        width: 25,
        height: 25,
        marginRight: 10,
        marginTop: 10,
        alignSelf: 'center',
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 2,
        marginBottom: 2,
    },
    viewBlackDot: {
        width: 8,
        height: 8,
        backgroundColor: '#282c37',
        alignSelf: 'flex-start',
        marginTop: 5,
        marginRight: 5,
        borderRadius: 4,
    },
    textDescRules: {
        width: wp('80%'),
        fontSize: 12,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 2,
    },
    aroundListStyle: {
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        // borderColor: '#ddd',
        // borderRadius: 15,
        padding: 5,
        // borderRadius: 8,
        position: 'relative',
        // marginLeft: 10,
        // marginRight: 10,
        marginTop: 10,
        marginBottom: 1,
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textrank: {
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    viewOraDot: {
        alignSelf: 'center',
    },
    profileImage: {
        width: 30,
        height: 30,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 45 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start'
    },
    textWorkName: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#6D819C',
    },
    claimReward: {
        width: '100%',
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        marginBottom: 10,
        marginTop: 10,
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        padding: 2,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    linearGradient1: {
        width: '90%',
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
    },

    goalBg: {
        width: wp('80%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 5,
        elevation: 3,
        marginTop: 25,
        marginBottom: 15,
    },
    goalDet: {
        width: wp('40%'),
        padding: 10,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'flex-start',
    },

    textWeight: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        alignSelf: 'center'
    },

    textMale: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        alignSelf: 'center',
        marginLeft: 10,
    },
    popupHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        margin: 15,
    },
});

const mapStateToProps = state => {
    const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals, trainerCodes };
};

// export default withNavigationFocus(
//     connect(
//         mapStateToProps,
//         { getTraineeGoals },
//     )(Trainee_Challenges),
// );


export default connect(mapStateToProps, { getTraineeGoals })(Trainee_Challenges);