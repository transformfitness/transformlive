import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    ScrollView,
    Dimensions,
    Image,
    FlatList,
    TextInput,
    PanResponder,
    BackHandler,
} from 'react-native';
import times from 'lodash.times';
import PropTypes from 'prop-types';
import DeviceInfo from 'react-native-device-info';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25)
const INTERVAL_WIDTH = 13
import Slider from '@react-native-community/slider';
const scale1 = (v, inputMin, inputMax, outputMin, outputMax) => {
    return Math.round(((v - inputMin) / (inputMax - inputMin)) * (outputMax - outputMin) + outputMin)
}
import { Loader, CustomDialog, HomeDialog } from './common';
import { getProfileDet } from '../actions';

import { BASE_URL, SWR } from '../actions/types';
import FastImage from 'react-native-fast-image';
import LogUtils from '../utils/LogUtils.js';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import Autocomplete from 'react-native-autocomplete-input';
import AppIntroSlider from 'react-native-app-intro-slider';
import { allowFunction } from '../utils/ScreenshotUtils.js';
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getTotCal(val, val2) {
    var v1 = parseFloat(val);
    var v2 = parseFloat(val2);
    var value = parseInt((v1 * v2).toFixed());

    if (value < 0) {
        value = 0;
    }
    return value;
}

function getCalories(_text) {
    let lines = _text.split('|');
    let elements = [];
    for (let i = 0; i < lines.length; i++) {
        elements.push(lines[i]);
        if (i < lines.length - 1) {
            elements.push(<br key={i} />);
        }
    }
    let myText = elements[0].split('-');
    return myText[1].replace(' Calories: ', '');
}

function getPeace(_text) {
    let lines = _text.split('|');
    let elements = [];
    for (let i = 0; i < lines.length; i++) {
        elements.push(lines[i]);
        if (i < lines.length - 1) {
            elements.push(<br key={i} />);
        }
    }
    let myText = elements[0].split('-');
    return myText[0];
}

function getPeace1(_text) {
    let lines = _text.split('|');
    let elements = [];
    for (let i = 0; i < lines.length; i++) {
        elements.push(lines[i]);
        if (i < lines.length - 1) {
            elements.push(<br key={i} />);
        }
    }
    let myText = elements[0].split('-');
    return myText[0].replace('Per ', '');
}

function isSelected(selArray) {
    let selected = false;
    selArray.map((item) => {
        if (item.check) {
            selected = true;
        }
    })
    return selected;
}

function isMaxFive(selArray) {
    let selected = 0;
    selArray.map((item) => {
        if (item.check) {
            selected = selected + 1;;
        }
    })
    return selected;
}

function isOthersSelected(selArray) {
    let selected = false;
    selArray.map((item) => {
        if (item.id === 99) {
            selected = true;
        }
    })
    return selected;
}

function getLbs(val) {
    var value = parseFloat((val * 2.2046).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

function getInches(val) {
    var value = parseFloat((val / 2.54).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

const convertedCentoFeet = (values) => {
    var realFeet = ((values * 0.393700) / 12);
    var feet = Math.floor(realFeet);
    var inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + inches;
}


let subCatIds = '', subcatVals = '';

class TraineeDietWorkQuestions extends Component {
    constructor(props) {
        super(props);
        this._handleScroll = this._handleScroll.bind(this)
        this._handleScrollEnd = this._handleScrollEnd.bind(this)
        this._handleContentSizeChange = this._handleContentSizeChange.bind(this)

        this.scrollMin = 0
        this.scrollMax = this._getScrollMax(props)
        this._scrollQueue = null
        this._value = Math.floor(this.props.profdet.height)

        this.state = {
            contentOffset: this._scaleValue(this._value),
            value_w: Math.floor(this.props.profdet.height),
            sliderValue: 50,
            isAlert: false,
            loading: false,
            alertTit: '',
            alertMsg: '',

            qType: 1,
            age: '',
            isMaleFemale: -1,
            goalId: 0,
            goalCode: '',
            goalSubId: 0,
            goalName: '',
            goals: [],
            tarSliderValue: 0,
            maxSliderValue: 0,
            minSliderValue: 0,
            arrSubCatList: [],
            activeLevels: [],
            activeId: 0,
            workoutTimes: [],
            wTimeId: 0,
            arrProfessions: [],
            professionId: 0,
            workoutlocations: [],
            workLocSelectedFakeContactList: [],
            workLocfakeContact: [],
            workLocId: 0,
            equipments: [],
            eqpSelectedFakeContactList: [],
            eqpfakeContact: [],
            fitnessForms: [],
            formSelectedFakeContactList: [],
            formfakeContact: [],
            healthConditions: [],
            healthSelectedFakeContactList: [],
            healthfakeContact: [],
            isSuccess: false,
            qstType: 0,
            quesTotal: 0,
            isVeg: 1,
            foodPreferences: [],
            foodpreferencearray: [],
            SelectedFoodPreferenceArray: [],

            foodAlergies: [],
            foodAlergiesarray: [],
            SelectedfoodAlergiesArray: [],

            foodLikes: [],
            foodLikesarray: [],
            SelectedfoodLikesArray: [],

            modalAddFoodVisible: false,
            modalTitle: '',
            searchText: '',
            noDataMsg: 'Please search for items you want to add',
            films: [],
            resObj: {},
            foodType: 0,
            selBreakfastArray: [],
            selLunchArray: [],
            selDinnerArray: [],
            selSnacksArray: [],
            isQtyPopVisible: false,
            selSubObj: {},
            qty: '',
            qtyMeasurement: '',
            profesion_other: '',
            hlthprblm_other: '',
            allergies_other: '',
            nextSubmitText: 'Next',

            isDeleteItem: false,
            deleteMsg: '',
            delFoodId: 0,
            selDelType: 0,
            showAppointments: 0,
            appoitText: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        // this.scrollMax = this._getScrollMax(nextProps)

        // if (nextProps.value !== this._value) {
        //     this._setScrollQueue({
        //         x: this._scaleValue(nextProps.value, nextProps),
        //         animate: true,
        //     })

        //     if (!this._contentSizeWillChange(nextProps)) {
        //         this._resolveScrollQueue()
        //     }
        // }

    }

    async componentDidMount() {
        allowFunction();
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
                return Math.abs(gestureState.dy) > 10;  // can adjust this num
            },
            onPanResponderGrant: (e, gestureState) => {
                this.fScroll.setNativeProps({ scrollEnabled: false })
            },
            onPanResponderMove: () => { },
            onPanResponderTerminationRequest: () => true,
        });

        if (this.props.qst_type === 2) {
            this.setState({ qstType: this.props.qst_type, quesTotal: 2 });
            // this.setState({ qstType: this.props.qst_type, quesTotal: 11 });
        }
        else if (this.props.qst_type === 3) {
            this.setState({ qstType: this.props.qst_type, quesTotal: 2 });
            // this.setState({ qstType: this.props.qst_type, quesTotal: 15 });
        }

        let token = await AsyncStorage.getItem('token');
        this.props.getProfileDet({ token });

        const interval = setInterval(() => {
            if (!isEmpty(this.props.profdet)) {

                if (this.props.profdet.is_veg !== 0) {
                    this.setState({
                        isVeg: this.props.profdet.is_veg,
                    });
                }

                this.setState({
                    age: this.props.profdet.age.toString(),
                    sliderValue: Math.floor(this.props.profdet.weight),
                    tarSliderValue: Math.floor(this.props.profdet.weight),
                    maxSliderValue: Math.floor(this.props.profdet.weight),
                    value_w: Math.floor(this.props.profdet.height),
                    isMaleFemale: this.props.profdet.gender_id,
                    profesion_other: this.props.profdet.profesion_other,
                    hlthprblm_other: this.props.profdet.hlthprblm_other,
                    allergies_other: this.props.profdet.allergies_other,

                });
                this._value = Math.floor(this.props.profdet.height);


                this.getFitnessForms();
                this.getFoodPreferences();

                // this.getGoals();
                // this.getActiveLevels();
                // this.getProfessions();
                // this.getWorkLocations();
                // this.getEquipmentAccess();
                // this.gethealthConditions();
                // this.getFoodAlergies();

                //this.getWorkoutTimes();
                // this.getFoodLikes();
                // this.getFoodLikesDislikes();


                clearInterval(interval);
            }
        }, 100);

        setTimeout(() => {
            this._scrollView && this._scrollView.scrollTo({ x: this.state.contentOffset, y: 0, animated: true })
        }, 100);

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isQtyPopVisible) {
            this.setState({ isQtyPopVisible: false });
        }
        else if (this.state.modalAddFoodVisible) {
            this.setState({ modalAddFoodVisible: false });
        }
        else {

            this.setState({ nextSubmitText: 'Next' });

            if (this.state.qType === 1) {
                // Actions.popTo('traineeHome');
                Actions.traineeHome();
            } else if (this.state.qType === 2) {
                this.setState({ qType: 1 })
            } else if (this.state.qType === 3) {
                // setTimeout(() => {
                //     this._scrollView && this._scrollView.scrollTo({ x: this.state.contentOffset, y: 0, animated: true })
                // }, 100);
                this.setState({ qType: 2 })
            } else if (this.state.qType === 4) {
                this.setState({ qType: 3 })
            } else if (this.state.qType === 5) {
                if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP' || this.state.goalCode === 'RM' || this.state.goalCode === 'WG') {
                    this.setState({ qType: 4 });
                }
                else {
                    this.setState({ qType: 3 });
                }
            } else if (this.state.qType === 6) {

                this.setState({ qType: 5 })

            } else if (this.state.qType === 7) {
                this.setState({ qType: 6 })
            } else if (this.state.qType === 8) {
                this.setState({ qType: 7 })
            }
            else if (this.state.qType === 9) {
                this.setState({ qType: 8 })
            }
            else if (this.state.qType === 10) {
                this.setState({ qType: 9 })
            }
            else if (this.state.qType === 11) {
                this.setState({ qType: 10 })
            }
            else if (this.state.qType === 12) {
                this.setState({ qType: 11 })
            }
            else if (this.state.qType === 13) {
                this.setState({ qType: 12 })
            }
            else if (this.state.qType === 14) {
                this.setState({ qType: 13 })
            }
            else if (this.state.qType === 15) {
                this.setState({ qType: 14 })
            }
            // else if (this.state.qType === 16) {
            //     this.setState({ qType: 15 })
            // }
        }
    }

    async getGoals() {
        fetch(
            `${BASE_URL}/master/gendergoal?gender_id=${this.state.isMaleFemale}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            if (item.id === this.props.profdet.goal_id) {
                                item.check = true;
                                this.setState({ goalId: item.id, goalCode: item.code });
                            }
                            else {
                                item.check = false;
                            }
                        })
                        this.setState({ goals: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getSubGoals() {
        fetch(
            `${BASE_URL}/master/goalsubctg`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    // Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    g_id: this.state.goalId,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data) {
                        data.data.map((item) => {
                            if (item.id === this.props.profdet.gt_id) {
                                item.check = true;
                                this.setState({ goalSubId: item.id });
                            } else {
                                item.check = false;
                            }
                            return data.data;
                        })

                        this.setState({ loading: false, arrSubCatList: data.data });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    // if (data.message === 'You are not authenticated!') {
                    //     this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    // } else {
                    //     this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    // }
                }
            })
            .catch(function (error) {
                // this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async getActiveLevels() {
        fetch(
            `${BASE_URL}/master/activelevels`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            if (item.id === this.props.profdet.active_level_id) {
                                item.check = true;
                                this.setState({ activeId: item.id });
                            }
                            else {
                                item.check = false;
                            }
                        })
                        this.setState({ activeLevels: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getWorkoutTimes() {

        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/workouttimes`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            if (item.id === this.props.profdet.wt_id) {
                                item.check = true;
                                this.setState({ wTimeId: item.id });
                            }
                            else {
                                item.check = false;
                            }
                        })
                        this.setState({ workoutTimes: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getProfessions() {
        fetch(
            `${BASE_URL}/master/profession`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            if (item.id === this.props.profdet.pf_id) {
                                item.check = true;
                                this.setState({ professionId: item.id });
                            }
                            else {
                                item.check = false;
                            }
                        })
                        this.setState({ arrProfessions: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getWorkLocations() {
        fetch(
            `${BASE_URL}/master/workoutlocation`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });
                        data.data.map((item) => {
                            this.props.profdet.workout_location.map((item1) => {
                                if (item.id === item1.id) {
                                    item.check = true;
                                    this.state.workLocSelectedFakeContactList.push(item);
                                }
                            })

                        })
                        this.setState({ workoutlocations: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getEquipmentAccess() {
        fetch(
            `${BASE_URL}/master/equipment`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });

                        data.data.map((item) => {
                            this.props.profdet.equipment_access.map((item1) => {
                                if (item.id === item1.id) {
                                    item.check = true;
                                    this.state.eqpSelectedFakeContactList.push(item);
                                }
                            })

                        })
                        this.setState({ equipments: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getFitnessForms() {
        fetch(
            `${BASE_URL}/master/fitnessform`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });

                        data.data.map((item) => {
                            this.props.profdet.fitness_form.map((item1) => {
                                if (item.ff_id === item1.id) {
                                    item.check = true;
                                    this.state.formSelectedFakeContactList.push(item);
                                }
                            })

                        })
                        this.setState({ fitnessForms: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async gethealthConditions() {
        fetch(
            `${BASE_URL}/master/medicalcndt`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });

                        data.data.map((item) => {
                            this.props.profdet.health_problems.map((item1) => {
                                if (item.id === item1.id) {
                                    item.check = true;
                                    this.state.healthSelectedFakeContactList.push(item);
                                }
                            })

                        })
                        this.setState({ healthConditions: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getFoodPreferences() {
        fetch(
            `${BASE_URL}/master/foodpreference`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });

                        data.data.map((item) => {
                            this.props.profdet.food_preference.map((item1) => {
                                if (item.id === item1.id) {
                                    item.check = true;

                                    this.state.SelectedFoodPreferenceArray.push(item);
                                }
                            })

                        })
                        this.setState({ foodPreferences: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getFoodAlergies() {
        fetch(
            `${BASE_URL}/master/foodallergies`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });

                        data.data.map((item) => {
                            this.props.profdet.food_allergies.map((item1) => {
                                if (item.id === item1.id) {
                                    item.check = true;

                                    this.state.SelectedfoodAlergiesArray.push(item);
                                }
                            })

                        })
                        this.setState({ foodAlergies: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }

    async getFoodLikedItems() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/foodliked`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_type: this.state.isVeg,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.food_icons.map((item1) => {
                                item1.check = false;
                            });
                        });

                        data.data.map((item) => {
                            item.food_icons.map((item1) => {
                                this.props.profdet.liked_food.map((item2) => {
                                    if (item1.fi_id === item2.fi_id) {
                                        item1.check = true;
                                    }
                                })
                            });

                        })

                        this.setState({ foodLikes: data.data });
                    }
                } else {
                }
            })
            .catch(function (error) {
            });
    }


    async getFoodLikes() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/foodicons`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        });
                        this.setState({ foodLikes: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }


    async getFoodLikesDislikes() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/healthyfood`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.like = false;
                            item.dislike = false;
                        });
                        this.setState({ foodLikes: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }


    _contentSizeWillChange(nextProps) {
        let { min, max } = nextProps
        if (min !== this.props.min || max !== this.props.max) {
            return true
        }

        return false
    }
    _getScrollMax(props = this.props) {
        return (props.max - props.min) * INTERVAL_WIDTH
    }

    _scaleScroll(x, props = this.props) {
        let { min, max } = props
        return scale1(x, this.scrollMin, this.scrollMax, min, max)
    }

    _scaleValue(v, props = this.props) {
        let { min, max } = props
        return scale1(v, min, max, this.scrollMin, this.scrollMax)
    }

    _setScrollQueue(scrollTo) {
        this._scrollQueue = scrollTo
    }

    _resolveScrollQueue() {
        if (this._scrollQueue !== null) {
            this._scrollView && this._scrollView.scrollTo(this._scrollQueue)
            this._handleScrollEnd()
        }
    }

    _handleContentSizeChange() {
        this._resolveScrollQueue()
    }

    async _handleScroll(event) {
        if (this._scrollQueue) return

        let offset = event.nativeEvent.contentOffset.x
        let { min, max } = this.props

        let val = this._scaleScroll(offset)

        if (val !== this._value) {
            this._value = val
            this.props.onChange(val)
            this.setState({ value_w: val });
        }
    }

    _handleScrollEnd() {
        this._value = this.props.value
        this._scrollQueue = null
    }

    _getIntervalSize(val) {
        let { largeInterval, mediumInterval } = this.props

        if (val % largeInterval == 0) return 'large'
        if (val % mediumInterval == 0) return 'medium'
        return 'small'
    }

    _renderIntervals() {
        let { min, max } = this.props
        let range = max - min + 1

        let values = times(range, (i) => i + min)

        return values.map((val, i) => {
            let intervalSize = this._getIntervalSize(val)

            return (
                <View key={`val-${i}`} style={styles.intervalContainer}>
                    {intervalSize === 'large' && (
                        <Text style={[styles.intervalValue, this.props.styles.intervalValue]}>{val}</Text>
                    )}

                    <View style={[styles.interval, styles[intervalSize], this.props.styles.interval, this.props.styles[intervalSize]]} />
                </View>
            )
        })
    }



    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderGenderMaleFemale() {
        if (this.state.isMaleFemale === -1) {
            return (
                <View style={styles.containerGender}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 1 })}>
                        <View style={styles.containerMaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/male.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Male</Text>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 2 })}>

                        <View style={styles.containerFemaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/female.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Female</Text>
                        </View>
                    </TouchableOpacity>

                </View>


            );
        } else if (this.state.isMaleFemale === 1) {
            return (
                <View style={styles.containerGender}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 1 })}>
                        <View style={styles.containerMaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/male.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Male</Text>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 2 })}>
                        <View style={styles.containerFemaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/female.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Female</Text>
                        </View>
                    </TouchableOpacity>

                </View>


            );
        } else if (this.state.isMaleFemale === 2) {
            return (
                <View style={styles.containerGender}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 1 })}>
                        <View style={styles.containerMaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/male.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Male</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.setState({ isMaleFemale: 2 })}>

                        <View style={styles.containerFemaleStyle}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.genCheckImgStyle}
                            />
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/female.png')}
                                style={styles.genImg}
                            />
                            <Text style={styles.maleFemaleText}>Female</Text>
                        </View>
                    </TouchableOpacity>

                </View>


            );
        }
    }


    renderVegOrNonVeg() {
        if (this.state.isVeg === 1) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>

                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            );
        } else if (this.state.isVeg === 2) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            );
        } else if (this.state.isVeg === 3) {
            return (
                <View style={styles.containerIsVeg}>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 1 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 3 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a Vegetarian who eats egg</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/check.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>
                    <View style={{ width: 6, height: 6 }}></View>
                    <TouchableOpacity style={styles.containerVegStyle}
                        onPress={() => this.setState({ isVeg: 2 })}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.countryText}>I am a non Vegetarian</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/uncheck.png')}
                                style={styles.vegSelect}
                            />
                        </View>
                    </TouchableOpacity>

                </View>
            );
        }
    }

    renderSubGoalData() {
        if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your target weight?
                        </Text>
                    <Text style={styles.tarweighttextStyle}>
                        {Math.floor(this.state.tarSliderValue)} Kg
                    </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.tarSliderValue}
                        onValueChange={(sliderValue) => this.setState({ tarSliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                        What is your weight loss goal?
                    </Text> */}
                </View>
            )
        }
        else if (this.state.goalCode === 'WG') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your desired weight?
                </Text>
                    <Text style={styles.tarweighttextStyle}>
                        {Math.floor(this.state.tarSliderValue)} Kg
            </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={this.state.minSliderValue}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.tarSliderValue}
                        onValueChange={(sliderValue) => this.setState({ tarSliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                    What is your weight loss goal?
            </Text> */}
                </View>
            )
        }
        else if (this.state.goalCode === 'RM') {
            return (
                <View>
                    <Text style={styles.subtextOneStyle}>
                        Select type of marathon you want to run?
                </Text>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        data={this.state.arrSubCatList}
                        keyExtractor={(item,index) =>"arrSubCatList"+index }
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressSubGoal(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>

            )
        }
    }


    renderQuestionTypeNew() {
        if (this.state.qType === 1) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Fitness Form</Text>
                    <Text style={styles.desc}>Please select fitness form of your preference</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.fitnessForms}
                        keyExtractor={(item,index) => "fitnessForms"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.viewListItem} onPress={() => {
                                this.pressFitnessForm(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 2) {
            return (
                <ScrollView contentContainerStyle={{ paddingBottom: hp('25%') }}>
                    <View style={styles.containericonStyle}>
                        <Text style={styles.subtextOneStyle}>Food Preference</Text>
                        <Text style={styles.desc}>Please select your choice of food</Text>

                        {this.renderVegOrNonVeg()}

                        <Text style={styles.desc}>Please select your cuisine preference</Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('2%'), alignSelf: 'center' }}
                            data={this.state.foodPreferences}
                            keyExtractor={(item,index) => "foodPreferences"+index}
                            extraData={this.state}
                            numColumns={3}
                            style={{ width: wp('100%') }}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.containerFoodStyle} onPress={() => {
                                    this.pressFoodPref(item)
                                }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.img }}
                                            style={styles.imgVeg}
                                        />
                                        <Text style={styles.vegText}>{`${item.name}`}</Text>

                                        {item.check
                                            ? (
                                                <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 4, top: 0, right: 1, }}
                                                    onPress={() => {
                                                        this.pressFoodPref(item)
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity style={{ width: 15, height: 15, position: 'absolute', alignSelf: 'flex-end', margin: 4, top: 0, }}
                                                    onPress={() => {
                                                        this.pressFoodPref(item)
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </TouchableOpacity>
                                            )}

                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>
                </ScrollView>
            );
        }
    }


    renderQuestionType2() {
        if (this.state.qType === 1) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        What is your Age?
                    </Text>
                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.ageTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter your age"
                            maxLength={3}
                            placeholderTextColor="grey"
                            keyboardType="numeric"
                            returnKeyType='done'
                            value={this.state.age}
                            onChangeText={text => {
                                if (text) {
                                    let newText = '';
                                    let numbers = '0123456789';
                                    for (var i = 0; i < text.length; i++) {
                                        if (numbers.indexOf(text[i]) > -1) {
                                            newText = newText + text[i];
                                        }
                                        else {
                                            this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter numbers only' });
                                        }
                                    }
                                    if (newText) {
                                        this.setState({ age: newText });

                                    }

                                }
                                else {
                                    this.setState({ age: '' });
                                }

                            }}
                        />
                    </View>
                </ScrollView>
            );
        }
        else if (this.state.qType === 2) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        Which one are you?
                    </Text>

                    {this.renderGenderMaleFemale()}

                    <Text style={styles.genderDesc}>To give you a better experience we need to know your gender</Text>

                </ScrollView>
            );
        }
        else if (this.state.qType === 3) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        What is your weight and height?
                    </Text>

                    <View style={{ flexDirection: 'column', marginTop: 20 }}>
                        {this.renderValue()}
                        {this.renderWeightValue()}
                    </View>

                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={150}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                </ScrollView>
            );

        }
        else if (this.state.qType === 4) {
            return (

                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Fitness Goal</Text>
                    <Text style={styles.desc}>Please select your fitness goal</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.goals}
                        keyExtractor={(item,index) => "goals"+index}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.press(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />

                </View>
            );
        }
        else if (this.state.qType === 5) {
            return (
                <View style={styles.containericonStyle}>
                    {this.renderSubGoalData()}
                    {/* {this.state.goalCode === 'WL' || this.state.goalCode === 'PP'
                        ? (
                            <View>

                                <Text style={styles.subtextOneStyle}>
                                    What is your target weight?
                                        </Text>
                                <Text style={styles.tarweighttextStyle}>
                                    {Math.floor(this.state.tarSliderValue)} Kg
                                    </Text>
                                <Slider
                                    style={styles.sliderImageStyle}
                                    minimumValue={0}
                                    maximumValue={this.state.maxSliderValue}
                                    minimumTrackTintColor="#8c52ff"
                                    maximumTrackTintColor="#000000"
                                    scrollEventThrottle={100}
                                    value={this.state.tarSliderValue}
                                    onValueChange={(sliderValue) => this.setState({ tarSliderValue: Math.floor(sliderValue) })}
                                />
                                <Text style={styles.subtextOneStyle}>
                                    What is your weight loss goal?
                                    </Text>
                            </View>
                        )
                        : (
                            <Text style={styles.subtextOneStyle}>
                                Select type of marathon you want to run?
                            </Text>
                        )
                    }

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        data={this.state.arrSubCatList}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressSubGoal(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} /> */}

                </View>
            );
        }

        else if (this.state.qType === 6) {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Active level</Text>
                    <Text style={styles.desc}>How active are you?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.activeLevels}
                        keyExtractor={(item,index) => "activeLevels"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressActLevl(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressActLevl(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressActLevl(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }

        else if (this.state.qType === 7) {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Profession</Text>
                    <Text style={styles.desc}>What is your profession?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrProfessions}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "arrProfessions"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerOthersStyle} onPress={() => {
                                this.pressProfession(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressProfession(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressProfession(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>
                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter other profession'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.profesion_other}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ profesion_other: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 8) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Workout Location</Text>
                    <Text style={styles.desc}>What is your location preference for working out?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.workoutlocations}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "workoutlocations"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressWorkLoc(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressWorkLoc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressWorkLoc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 9) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Equipment Access</Text>
                    <Text style={styles.desc}>Please select the equipment you have access to</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.equipments}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "equipments"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.viewListItem} onPress={() => {
                                this.pressEqpAcc(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressEqpAcc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressEqpAcc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 10) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Fitness Form</Text>
                    <Text style={styles.desc}>Please select fitness form of your preference</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.fitnessForms}
                        //keyExtractor={item => item.ff_id}
                        keyExtractor={(item,index) => "fitnessForms"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.viewListItem} onPress={() => {
                                this.pressFitnessForm(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 11) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Health Problems</Text>
                    <Text style={styles.desc}>Please select any current health problems we should be aware of</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.healthConditions}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "healthConditions"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.containerOthersStyle} onPress={() => {
                                this.pressHealthCondition(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressHealthCondition(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressHealthCondition(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>
                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter your health problems'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.hlthprblm_other}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ hlthprblm_other: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        // else if (this.state.qType === 12) {
        //     return (
        //         <View style={styles.containericonStyle}>
        //             <Text style={styles.subtextOneStyle}>Food Allergies</Text>
        //             <Text style={styles.desc}>Please select any food you are allergic to</Text>


        //             <FlatList
        //                 contentContainerStyle={{ paddingBottom: hp('35%') }}
        //                 showsVerticalScrollIndicator={false}
        //                 data={this.state.foodAlergies}
        //                 keyExtractor={item => item.id}
        //                 ItemSeparatorComponent={this.FlatListItemSeparator}
        //                 renderItem={({ item }) => {
        //                     return <TouchableOpacity style={styles.viewListItem} onPress={() => {
        //                         this.pressAlergies(item)
        //                     }}>
        //                         <View >
        //                             <Text style={styles.countryText}>{`${item.name}`}</Text>
        //                         </View>
        //                         <View style={styles.mobileImageStyle}>
        //                             {item.check
        //                                 ? (
        //                                     <TouchableOpacity onPress={() => {
        //                                         this.pressAlergies(item)
        //                                     }}>
        //                                         <Image
        //                                             progressiveRenderingEnabled={true}
        //                                             resizeMethod="resize"
        //                                             source={require('../res/ic_box_check.png')}
        //                                             style={styles.mobileImageStyle} />
        //                                     </TouchableOpacity>
        //                                 )
        //                                 : (
        //                                     <TouchableOpacity onPress={() => {
        //                                         this.pressAlergies(item)
        //                                     }}>
        //                                         <Image
        //                                             progressiveRenderingEnabled={true}
        //                                             resizeMethod="resize"
        //                                             source={require('../res/ic_box_uncheck.png')}
        //                                             style={styles.mobileImageStyle} />
        //                                     </TouchableOpacity>
        //                                 )}
        //                         </View>
        //                     </TouchableOpacity>
        //                 }} />
        //         </View>
        //     );
        // }
    }

    renderQuestionType3() {
        if (this.state.qType === 1) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        What is your Age?
                    </Text>
                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.ageTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter your age"
                            maxLength={3}
                            placeholderTextColor="grey"
                            keyboardType="numeric"
                            returnKeyType='done'
                            value={this.state.age}
                            onChangeText={text => {
                                if (text) {
                                    let newText = '';
                                    let numbers = '0123456789';
                                    for (var i = 0; i < text.length; i++) {
                                        if (numbers.indexOf(text[i]) > -1) {
                                            newText = newText + text[i];
                                        }
                                        else {
                                            this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter numbers only' });
                                        }
                                    }
                                    if (newText) {
                                        this.setState({ age: newText });

                                    }

                                }
                                else {
                                    this.setState({ age: '' });
                                }

                            }}
                        />
                    </View>
                </ScrollView>
            );
        }
        else if (this.state.qType === 2) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        Which one are you?
                    </Text>

                    {this.renderGenderMaleFemale()}

                    <Text style={styles.genderDesc}>To give you a better experience we need to know your gender</Text>

                </ScrollView>
            );
        }
        else if (this.state.qType === 3) {
            return (
                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>
                    <Text style={styles.subtextOneStyle}>
                        What is your weight and height?
                    </Text>

                    <View style={{ flexDirection: 'column', marginTop: 20 }}>
                        {this.renderValue()}
                        {this.renderWeightValue()}
                    </View>

                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={150}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                </ScrollView>
            );

        }
        else if (this.state.qType === 4) {
            return (

                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Fitness Goal</Text>
                    <Text style={styles.desc}>Please select your fitness goal</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('40%') }}
                        data={this.state.goals}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "goals2"+index}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.press(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />

                </View>
            );
        }
        else if (this.state.qType === 5) {
            return (
                <View style={styles.containericonStyle}>
                    {this.renderSubGoalData()}
                    {/* {this.state.goalCode === 'WL' || this.state.goalCode === 'PP'
                        ? (
                            <View>

                                <Text style={styles.subtextOneStyle}>
                                    What is your target weight?
                                        </Text>
                                <Text style={styles.tarweighttextStyle}>
                                    {Math.floor(this.state.tarSliderValue)} Kg
                                    </Text>
                                <Slider
                                    style={styles.sliderImageStyle}
                                    minimumValue={0}
                                    maximumValue={this.state.maxSliderValue}
                                    minimumTrackTintColor="#8c52ff"
                                    maximumTrackTintColor="#000000"
                                    scrollEventThrottle={100}
                                    value={this.state.tarSliderValue}
                                    onValueChange={(sliderValue) => this.setState({ tarSliderValue: Math.floor(sliderValue) })}
                                />
                                <Text style={styles.subtextOneStyle}>
                                    What is your weight loss goal?
                                    </Text>
                            </View>
                        )
                        : (
                            <Text style={styles.subtextOneStyle}>
                                Select type of marathon you want to run?
                            </Text>
                        )
                    }

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        data={this.state.arrSubCatList}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressSubGoal(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressSubGoal(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} /> */}

                </View>
            );
        }

        else if (this.state.qType === 6) {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Active level</Text>
                    <Text style={styles.desc}>How active are you?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.activeLevels}
                        //keyExtractor={item => item.id}
                        keyExtractor={(item,index) => "activeLev"+index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressActLevl(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressActLevl(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressActLevl(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        // else if (this.state.qType === 7) {

        //     return (
        //         <View style={styles.containericonStyle}>

        //             <Text style={styles.subtextOneStyle}>Workout Time</Text>
        //             <Text style={styles.desc}>Please select at what time do you regularly workout?</Text>

        //             <FlatList
        //                 contentContainerStyle={{ paddingBottom: hp('35%') }}
        //                 showsVerticalScrollIndicator={false}
        //                 data={this.state.workoutTimes}
        //                 keyExtractor={item => item.wt_id}
        //                 ItemSeparatorComponent={this.FlatListItemSeparator}
        //                 renderItem={({ item }) => {
        //                     return <TouchableOpacity key={item.wt_id} style={styles.viewListItem} onPress={() => {
        //                         this.pressWorkoutTimes(item)
        //                     }}>
        //                         <View >
        //                             <Text style={styles.countryText}>{`${item.name}`}</Text>
        //                         </View>
        //                         <View style={styles.mobileImageStyle}>
        //                             {item.check
        //                                 ? (
        //                                     <TouchableOpacity onPress={() => {
        //                                         this.pressWorkoutTimes(item)
        //                                     }}>
        //                                         <Image
        //                                             progressiveRenderingEnabled={true}
        //                                             resizeMethod="resize"
        //                                             source={require('../res/check.png')}
        //                                             style={styles.mobileImageStyle} />
        //                                     </TouchableOpacity>
        //                                 )
        //                                 : (
        //                                     <TouchableOpacity onPress={() => {
        //                                         this.pressWorkoutTimes(item)
        //                                     }}>
        //                                         <Image
        //                                             progressiveRenderingEnabled={true}
        //                                             resizeMethod="resize"
        //                                             source={require('../res/uncheck.png')}
        //                                             style={styles.mobileImageStyle} />
        //                                     </TouchableOpacity>
        //                                 )}
        //                         </View>
        //                     </TouchableOpacity>
        //                 }} />
        //         </View>
        //     );
        // }
        else if (this.state.qType === 7) {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.subtextOneStyle}>Profession</Text>
                    <Text style={styles.desc}>What is your profession?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrProfessions}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerOthersStyle} onPress={() => {
                                this.pressProfession(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressProfession(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressProfession(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>
                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter other profession'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.profesion_other}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ profesion_other: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 8) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Workout Location</Text>
                    <Text style={styles.desc}>What is your location preference for working out?</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.workoutlocations}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.viewListItem} onPress={() => {
                                this.pressWorkLoc(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressWorkLoc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressWorkLoc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 9) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Equipment Access</Text>
                    <Text style={styles.desc}>Please select the equipment you have access to</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.equipments}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.viewListItem} onPress={() => {
                                this.pressEqpAcc(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressEqpAcc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressEqpAcc(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 10) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Fitness Form</Text>
                    <Text style={styles.desc}>Please select fitness form of your preference</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.fitnessForms}
                        keyExtractor={item => item.ff_id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.viewListItem} onPress={() => {
                                this.pressFitnessForm(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFitnessForm(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_box_uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 11) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Health Problems</Text>
                    <Text style={styles.desc}>Please select any current health problems we should be aware of</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.healthConditions}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.containerOthersStyle} onPress={() => {
                                this.pressHealthCondition(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressHealthCondition(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressHealthCondition(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>

                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter your health problems'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.hlthprblm_other}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ hlthprblm_other: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 12) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Food Allergies</Text>
                    <Text style={styles.desc}>Please select any food you are allergic to</Text>


                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.foodAlergies}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.containerOthersStyle} onPress={() => {
                                this.pressAlergies(item)
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>

                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressAlergies(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.pressAlergies(item)
                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </View>
                                {item.id === 99 && item.check
                                    ? (
                                        <View style={styles.tiViewBack}>
                                            <TextInput
                                                style={styles.textInputStyle}
                                                placeholder='Enter your food allergies'
                                                maxLength={100}
                                                placeholderTextColor="grey"
                                                value={this.state.allergies_other}
                                                autoCapitalize='sentences'
                                                returnKeyType={'done'}
                                                onChangeText={text => this.setState({ allergies_other: text })}
                                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                            />
                                        </View>
                                    )
                                    : (
                                        <View>
                                        </View>
                                    )}
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else if (this.state.qType === 13) {
            return (
                <ScrollView contentContainerStyle={{ paddingBottom: hp('20%') }}>
                    <View style={styles.containericonStyle}>
                        <Text style={styles.subtextOneStyle}>Food Preference</Text>
                        <Text style={styles.desc}>Please select your choice of food</Text>
                        {this.renderVegOrNonVeg()}
                        <Text style={styles.desc}>Please select your cuisine preference</Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%'), alignSelf: 'center' }}
                            data={this.state.foodPreferences}
                            keyExtractor={item => item.id}
                            extraData={this.state}
                            numColumns={3}
                            scrollEnabled={false}
                            style={{ width: wp('100%') }}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.containerFoodStyle} onPress={() => {
                                    this.pressFoodPref(item)
                                }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.img }}
                                            style={styles.imgVeg}
                                        />
                                        <Text style={styles.vegText}>{`${item.name}`}</Text>

                                        {item.check
                                            ? (
                                                <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 4, top: 0, right: 1, }}
                                                    onPress={() => {
                                                        this.pressFoodPref(item)
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_check.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity style={{ width: 15, height: 15, position: 'absolute', alignSelf: 'flex-end', margin: 4, top: 0, }}
                                                    onPress={() => {
                                                        this.pressFoodPref(item)
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_box_uncheck.png')}
                                                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', top: 0, right: 1, }} />
                                                </TouchableOpacity>
                                            )}

                                    </View>

                                </TouchableOpacity>
                            }} />

                    </View>
                </ScrollView>
            );
        }
        else if (this.state.qType === 14) {
            return (
                <View style={styles.containericonStyle}>
                    <Text style={styles.subtextOneStyle}>Food Likes</Text>
                    <Text style={styles.desc}>Please select any five foods which you like from below</Text>


                    <AppIntroSlider
                        slides={this.state.foodLikes}
                        // onDone={this._onDone}
                        // showSkipButton={false}
                        showPrevButton={false}
                        showDoneButton={false}
                        showNextButton={false}
                        // renderDoneButton={this._renderDoneButton}
                        dotStyle={{ backgroundColor: '#2d3142', elevation: 2, width: 8, height: 8, borderRadius: 4 }}
                        activeDotStyle={{ backgroundColor: '#8c52ff', elevation: 5, width: 10, height: 10, borderRadius: 5 }}
                        paginationStyle={{ top: -25, height: 20 }}
                        renderItem={({ item }) => {
                            return <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row', marginLeft: 10, marginRight: 10, marginTop: 10 }}>

                                    <Text style={styles.txtLikeFoodTit}>{item.name}</Text>

                                </View>

                                <FlatList
                                    //    data={formatData(data, numColumns)}
                                    contentContainerStyle={{ paddingBottom: hp('35%') }}
                                    style={{ marginTop: 10, marginLeft: 5, marginRight: 5, }}
                                    data={item.food_icons}
                                    numColumns={3}
                                    extraData={this.state}
                                    renderItem={this.renderListItem}
                                    keyExtractor={this.keyExtractor}
                                    showsVerticalScrollIndicator={false}
                                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                                />
                            </View>
                        }}
                    />

                    {/* <FlatList
                        contentContainerStyle={{ paddingBottom: hp('30%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.foodLikes}
                        extraData={this.state}
                        keyExtractor={item => item.f_id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <View style={styles.viewListItem}>
                                <View >
                                    <Text style={styles.tvFoodLikeText}>{`${item.food_name}`}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    {item.like
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFoodLike(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_like_blue.png')}
                                                    style={styles.mobileImageStyle2} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFoodLike(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_like_grey.png')}
                                                    style={styles.mobileImageStyle1} />
                                            </TouchableOpacity>
                                        )}

                                    {item.dislike
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFoodDisLike(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_dislike_blue.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.pressFoodDisLike(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_dislike_grey.png')}
                                                    style={styles.mobileImageStyle3} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </View>
                        }} /> */}

                    {/* <Text style={styles.subtextOneStyle}>Pick the food you like</Text>
                    <FlatList
                        //    data={formatData(data, numColumns)}
                        contentContainerStyle={{ paddingBottom: hp('50%') }}
                        style={{ marginTop: 20, marginLeft: 5, marginRight: 5, }}
                        data={this.state.foodLikes}
                        numColumns={3}
                        renderItem={this.renderListItem}
                        keyExtractor={this.keyExtractor}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator1}
                    /> */}
                </View>
            );
        }
        else if (this.state.qType === 15) {
            return (
                <ScrollView contentContainerStyle={{ paddingBottom: hp('30%') }}>
                    <View style={styles.containericonStyle}>
                        <Text style={styles.subtextOneStyle}>Add Food</Text>
                        <Text style={styles.desc}>Please add the foods that you regularly eat on a normal day</Text>
                        <View style={{ height: 9, marginTop: 10, backgroundColor: '#f4f6fa', }}></View>
                        {/* breakfast */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Breakfast</Text>

                                <TouchableOpacity onPress={() => {
                                    this.setState({ modalAddFoodVisible: true, modalTitle: 'Breakfast', foodType: 1 });
                                }}>
                                    <Text style={styles.textAdd}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(1)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Lunch */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Lunch</Text>
                                <TouchableOpacity onPress={() => {
                                    this.setState({ modalAddFoodVisible: true, modalTitle: 'Lunch', foodType: 2 });
                                }}>
                                    <Text style={styles.textAdd}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(2)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Dinner */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Dinner</Text>
                                <TouchableOpacity onPress={() => {
                                    this.setState({ modalAddFoodVisible: true, modalTitle: 'Dinner', foodType: 3 });
                                }}>
                                    <Text style={styles.textAdd}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(3)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Snacks */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Snacks</Text>
                                <TouchableOpacity onPress={() => {
                                    this.setState({ modalAddFoodVisible: true, modalTitle: 'Snacks', foodType: 4 });
                                }}>
                                    <Text style={styles.textAdd}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(4)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>
                    </View>
                </ScrollView>
            );
        }
    }

    renderMealData = (item) => {

        switch (item) {
            case 1:
                let arrBreakfast = this.state.selBreakfastArray.map((item1, i) => {
                    return <TouchableOpacity key={i}
                        style={styles.nutrition1}
                        onPress={() => {
                            this.setState({ delFoodId: item1.food_id, selDelType: 1, isDeleteItem: true, deleteMsg: 'Are you sure? You want to delete this item.' });
                        }}>

                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_blue_dot.png')}
                            style={{
                                width: 10,
                                height: 10,
                                marginLeft: 15,
                                marginTop: 4,
                                alignSelf: 'flex-start',
                            }}
                        />
                        <View style={styles.nutritionInnerView}>
                            <View style={{ flexDirection: 'row' }}>

                                <Text style={styles.textNutTitle}>{item1.food_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>

                                {this.renderFoodType(item1)}
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal.png')}
                                    style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}>{`${getTotCal(getCalories(item1.food_description), item1.qty)}`} cal</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_serving.png')}
                                    style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}> {`${item1.qty}`} Qty of {`${getPeace1(item1.food_description)}`}</Text>
                            </View>

                            <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                        </View>
                    </TouchableOpacity>

                });
                return (
                    <View>
                        {arrBreakfast}
                    </View>
                );

            case 2:
                let ArrLunch = this.state.selLunchArray.map((item1, i) => {
                    return <TouchableOpacity key={i} style={styles.nutrition1}
                        onPress={() => {
                            this.setState({ delFoodId: item1.food_id, selDelType: 2, isDeleteItem: true, deleteMsg: 'Are you sure? You want to delete this item.' });
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_blue_dot.png')}
                            style={{
                                width: 10,
                                height: 10,
                                marginLeft: 15,
                                marginTop: 4,
                                alignSelf: 'flex-start',
                            }}
                        />
                        <View style={styles.nutritionInnerView}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.textNutTitle}>{item1.food_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                {this.renderFoodType(item1)}
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal.png')}
                                    style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}>{`${getTotCal(getCalories(item1.food_description), item1.qty)}`} cal</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_serving.png')}
                                    style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}> {`${item1.qty}`} Qty of {`${getPeace1(item1.food_description)}`}</Text>
                            </View>

                            <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                        </View>
                    </TouchableOpacity>

                });
                return (
                    <View>
                        {ArrLunch}
                    </View>
                );

            case 3:
                let arrDinner = this.state.selDinnerArray.map((item1, i) => {
                    return <TouchableOpacity key={i} style={styles.nutrition1}
                        onPress={() => {
                            this.setState({ delFoodId: item1.food_id, selDelType: 3, isDeleteItem: true, deleteMsg: 'Are you sure? You want to delete this item.' });
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_blue_dot.png')}
                            style={{
                                width: 10,
                                height: 10,
                                marginLeft: 15,
                                marginTop: 4,
                                alignSelf: 'flex-start',
                            }}
                        />
                        <View style={styles.nutritionInnerView}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.textNutTitle}>{item1.food_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                {this.renderFoodType(item1)}
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal.png')}
                                    style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}>{`${getTotCal(getCalories(item1.food_description), item1.qty)}`} cal</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_serving.png')}
                                    style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}> {`${item1.qty}`} Qty of {`${getPeace1(item1.food_description)}`}</Text>
                            </View>

                            <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                        </View>
                    </TouchableOpacity>

                });
                return (
                    <View>
                        {arrDinner}
                    </View>
                );
            case 4:
                let arrSnacks = this.state.selSnacksArray.map((item1, i) => {
                    return <TouchableOpacity key={i} style={styles.nutrition1}
                        onPress={() => {
                            this.setState({ delFoodId: item1.food_id, selDelType: 4, isDeleteItem: true, deleteMsg: 'Are you sure? You want to delete this item.' });
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_blue_dot.png')}
                            style={{
                                width: 10,
                                height: 10,
                                marginLeft: 15,
                                marginTop: 4,
                                alignSelf: 'flex-start',
                            }}
                        />
                        <View style={styles.nutritionInnerView}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.textNutTitle}>{item1.food_name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                {this.renderFoodType(item1)}
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal.png')}
                                    style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}>{`${getTotCal(getCalories(item1.food_description), item1.qty)}`} cal</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_serving.png')}
                                    style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                />
                                <Text style={styles.textNutDesc}> {`${item1.qty}`} Qty of {`${getPeace1(item1.food_description)}`}</Text>
                            </View>

                            <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                        </View>
                    </TouchableOpacity>

                });
                return (
                    <View>
                        {arrSnacks}
                    </View>
                );
            default:
                break;
        }

    };

    renderFoodType = (item) => {
        if (item.food_type) {
            if (item.food_type.length > 20) {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType}>{`${item.food_type}`}</Text>
                    </View>

                );
            }
            else {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType1}>{`${item.food_type}`}</Text>
                    </View>

                );
            }

        } else {
            return (
                <View>
                </View>
            )
        }
    }


    keyExtractor = (item) => {
        return item.fi_id
    }

    renderListItem = ({ item }) => {
        return (
            <TouchableOpacity key={item.fi_id} style={styles.containerLikeStyle} onPress={() => {
                this.pressLikes(item)
            }}>
                <View
                    style={styles.item}>
                    {item.img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.img }}
                                style={{ width: 100, height: 100, alignSelf: 'center', }} />
                        )
                        : (

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{ width: 100, height: 100, borderRadius: 50, alignSelf: 'center', }} />

                        )}

                    <Text
                        numberOfLines={2}
                        style={styles.vegText}>{`${item.name}`}</Text>
                    {item.check
                        ? (
                            <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }}
                                onPress={() => {
                                    this.pressLikes(item)
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_box_check.png')}
                                    style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                            </TouchableOpacity>
                        )
                        : (
                            <TouchableOpacity style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, }}
                                onPress={() => {
                                    this.pressLikes(item)
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_box_uncheck.png')}
                                    style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                            </TouchableOpacity>
                        )}

                </View>
            </TouchableOpacity>
        )
    }

    renderBottom() {
        return (
            <View style={styles.viewBottom}>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                    <TouchableOpacity
                        onPress={() => {
                            this.onButtonPressedNew();
                            // if (this.props.qst_type === 2) {
                            //     this.onButtonPressed2()
                            // }
                            // else {
                            //     this.onButtonPressed3()
                            // }
                        }}>
                        <Text style={styles.buttonText}>{this.state.nextSubmitText}</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </View>

        );

    }

    renderWeightValue() {
        return (
            <View style={styles.personViewStyle}>
                <View style={{ width: '90%', alignSelf: 'center', position: 'relative' }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_person_height.png')}
                        style={styles.personImageStyle}>

                    </Image>

                </View>
                <View style={[styles.container, this.props.styles.container]}>
                    <View
                        style={styles.triangleshape}>
                    </View>
                    <ScrollView
                        {...this._panResponder.panHandlers}
                        onScrollEndDrag={() => this.fScroll.setNativeProps({ scrollEnabled: true })}
                        onScrollBeginDrag={() => this.fScroll.setNativeProps({ scrollEnabled: true })}
                        nestedScrollEnabled={true}
                        ref={r => this._scrollView = r}
                        automaticallyAdjustInsets={false}
                        horizontal={true}
                        decelerationRate={0}
                        snapToInterval={INTERVAL_WIDTH}
                        snapToAlignment="start"
                        showsHorizontalScrollIndicator={false}
                        onScroll={this._handleScroll}
                        onMomentumScrollEnd={this._handleScrollEnd}
                        onContentSizeChange={this._handleContentSizeChange}
                        scrollEventThrottle={100}
                        contentOffset={{ x: this.state.contentOffset }}>

                        <View style={[styles.intervals, this.props.styles.intervals]}>
                            {this._renderIntervals()}
                        </View>
                    </ScrollView>
                </View>

                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    left: wp('12.5%'),
                }}>
                    <Text style={styles.weighttextStyle}>
                        {this.state.sliderValue} Kg
                </Text>

                    <Text style={styles.weighttextStyle1}>
                        {getLbs(this.state.sliderValue)} lbs
                </Text>
                </View>



            </View>
        );

    }

    renderValue() {
        return (
            <View style={{
                position: 'absolute',
                top: 0,
                right: wp('12%'),
            }}>
                <Text style={styles.HeighttextStyle}>
                    {this.state.value_w} cm
            </Text>
                <Text style={styles.HeighttextStyle1}>
                    {/* {getInches(this.state.value_w)} inch */}
                    {convertedCentoFeet(this.state.value_w)}"
        </Text>

            </View>

            // <View>
            //     <Text style={styles.HeighttextStyle}>
            //         {this.state.value_w.toString()} cm
            // </Text>

            // </View>

        );

    }

    async saveWorkoutPlanQuestions(data) {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/updateplanqst`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTit: 'Alert', alertMsg: SWR });
            });

    }

    async onButtonPressedNew() {

        if (this.state.qType === 1) {

            if (this.state.formSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select fitness form of your preference' });
            }
            else {
                this.setState({ qType: 2 });
                this.setState({ nextSubmitText: 'Submit' });
            }

        }
        else if (this.state.qType === 2) {

            if (this.state.SelectedFoodPreferenceArray.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your cuisine preference' });
            }
            else {

                let foodIds = '';
                this.state.SelectedFoodPreferenceArray.map((item) => {
                    if (item.check) {
                        if (foodIds.length === 0) {
                            foodIds = foodIds + item.id;
                        }
                        else {
                            foodIds = foodIds + ',' + item.id;
                        }
                    }
                });

                let ffIds = '';
                this.state.formSelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (ffIds.length === 0) {
                            ffIds = ffIds + item.ff_id;
                        }
                        else {
                            ffIds = ffIds + ',' + item.ff_id;
                        }
                    }
                });

                let data = JSON.stringify({
                    is_veg: this.state.isVeg,
                    cuisine_id: foodIds,
                    ff_id: ffIds,
                });
                LogUtils.infoLog(data);

                this.saveAiDietPlanQuestionsNew(data);
            }
        }
    }

    async saveAiDietPlanQuestionsNew(data) {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/updateqst`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message, showAppointments: data.show_app, appoitText:data.fta_qstn });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTit: 'Alert' });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTit: 'Alert' });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTit: 'Alert', alertMsg: SWR });
            });
    }

    async onButtonPressed2() {


        if (this.state.qType === 1) {
            if (!this.state.age) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter your age' });
            } else {
                if (this.state.age >= 15 && this.state.age <= 80) {
                    this.setState({ qType: 2 });
                } else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Persons with age between 15 and 80 can only register' });
                    this.setState({ age: '' });
                }
            }
        }
        else if (this.state.qType === 2) {
            this.setState({ qType: 3 });
            setTimeout(() => {
                this._scrollView && this._scrollView.scrollTo({ x: this.state.contentOffset, y: 0, animated: true })
            }, 100);
        }
        else if (this.state.qType === 3) {
            this.setState({
                tarSliderValue: Math.floor(this.state.sliderValue),
                maxSliderValue: Math.floor(this.state.sliderValue),
            });
            this.setState({ qType: 4 });
            this.getGoals();
        }
        else if (this.state.qType === 4) {
            if (isSelected(this.state.goals)) {
                if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP' || this.state.goalCode === 'RM' || this.state.goalCode === 'WG') {
                    this.setState({ qType: 5 });
                    if (this.state.goalCode === 'RM') {
                        this.getSubGoals();
                    }
                    else {
                        if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
                            this.setState({ maxSliderValue: this.state.sliderValue, tarSliderValue: this.state.sliderValue });
                        }
                        else if (this.state.goalCode === 'WG') {
                            // this.setState({ minSliderValue: this.state.sliderValue, maxSliderValue: 120, tarSliderValue: this.state.sliderValue });
                            if (this.state.sliderValue < 150) {
                                this.setState({ minSliderValue: this.state.sliderValue, maxSliderValue: 150, tarSliderValue: this.state.sliderValue });
                            }
                            else {
                                this.setState({ minSliderValue: this.state.sliderValue, maxSliderValue: 200, tarSliderValue: this.state.sliderValue });
                            }
                        }
                    }
                }
                else {
                    this.setState({ qType: 6 });
                }
            }
            else {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your fitness goal' });
            }
        }
        else if (this.state.qType === 5) {
            if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
                if (Math.floor(this.state.tarSliderValue) === Math.floor(this.state.sliderValue)) {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Target weight cannot be equal to your weight.Please change your target weight' });
                } else {
                    this.setState({ qType: 6 });
                }
            }
            else if (this.state.goalCode === 'WG') {
                if (Math.floor(this.state.tarSliderValue) === Math.floor(this.state.sliderValue)) {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Desired weight cannot be equal to your weight.Please change your desired weight' });
                } else {
                    this.setState({ qType: 6 });
                }
            }
            else if (this.state.goalCode === 'RM') {
                if (this.state.goalSubId !== 0 && isSelected(this.state.arrSubCatList)) {
                    this.setState({ qType: 6 });
                }
                else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select a marathon type' });
                }
            }

            // if (this.state.goalSubId !== 0) {
            //     this.setState({ qType: 6 });
            // }
            // else {
            //     if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
            //         this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your weight loss goal' });
            //     }
            //     else {
            //         this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select a marathon type' });
            //     }
            // }
        }
        else if (this.state.qType === 6) {
            this.setState({ qType: 7 });
        }
        else if (this.state.qType === 7) {
            if (this.state.professionId !== 0) {
                if (this.state.professionId === 99) {
                    if (this.state.profesion_other) {
                        this.setState({ qType: 8 });
                    }
                    else {
                        this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter your profession' });
                    }
                }
                else {
                    this.setState({ qType: 8, profesion_other: '' });
                }
            }
            else {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your profession' });
            }
        }
        else if (this.state.qType === 8) {

            if (this.state.workLocSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your location preference for working out' });
            }
            else {
                this.setState({ qType: 9 });
            }

        }
        else if (this.state.qType === 9) {
            if (this.state.eqpSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select the equipment you have access to' });
            }
            else {
                this.setState({ qType: 10 });
            }
        }
        else if (this.state.qType === 10) {
            if (this.state.formSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select fitness form of your preference' });
            }
            else {
                this.setState({ qType: 11 });
                this.setState({ nextSubmitText: 'Submit' });
            }
        }
        else if (this.state.qType === 11) {
            if (this.state.healthSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select any current health problems we should be aware of' });
            }
            else {

                let eqpIds = '';
                this.state.eqpSelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (eqpIds.length === 0) {
                            eqpIds = eqpIds + item.id;
                        }
                        else {
                            eqpIds = eqpIds + ',' + item.id;
                        }
                    }
                });


                let workLocIds = '';
                this.state.workLocSelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (workLocIds.length === 0) {
                            workLocIds = workLocIds + item.id;
                        }
                        else {
                            workLocIds = workLocIds + ',' + item.id;
                        }
                    }
                });

                let ffIds = '';
                this.state.formSelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (ffIds.length === 0) {
                            ffIds = ffIds + item.ff_id;
                        }
                        else {
                            ffIds = ffIds + ',' + item.ff_id;
                        }
                    }
                });

                let healthIds = '';
                this.state.healthSelectedFakeContactList.map((item) => {
                    if (item.check) {
                        if (healthIds.length === 0) {
                            healthIds = healthIds + item.id;
                        }
                        else {
                            healthIds = healthIds + ',' + item.id;
                        }
                    }
                });

                let allids = '';
                this.state.SelectedfoodAlergiesArray.map((item) => {
                    if (item.check) {
                        if (allids.length === 0) {
                            allids = allids + item.id;
                        }
                        else {
                            allids = allids + ',' + item.id;
                        }
                    }
                });


                if (isOthersSelected(this.state.healthSelectedFakeContactList)) {
                    if (this.state.hlthprblm_other) {

                        let data = JSON.stringify({
                            type_id: 2,
                            age: this.state.age,
                            gender_id: this.state.isMaleFemale,
                            height: this.state.value_w.toString(),
                            weight: this.state.sliderValue.toString(),
                            g_id: this.state.goalId,
                            trgt_weight: this.state.tarSliderValue.toString(),
                            gt_id: this.state.goalSubId,
                            fitness_level: this.state.activeId,
                            wt_id: this.state.wTimeId,
                            pf_id: this.state.professionId,
                            location_id: workLocIds,
                            equipment_id: eqpIds,
                            ff_id: ffIds,
                            health_id: healthIds,
                            allergy_id: allids,
                            is_veg: 0,
                            cuisine_id: '',
                            likes_id: '',
                            mg_id: subCatIds,
                            mg_val: subcatVals,
                            profesion_other: this.state.profesion_other,
                            hlthprblm_other: this.state.hlthprblm_other,
                            allergies_other: this.state.allergies_other,
                        });
                        LogUtils.infoLog(data);

                        this.saveWorkoutPlanQuestions(data);
                    }
                    else {
                        this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter any current health problems we should be aware of' });
                    }
                }
                else {
                    this.setState({ hlthprblm_other: '' });

                    let data = JSON.stringify({
                        type_id: 2,
                        age: this.state.age,
                        gender_id: this.state.isMaleFemale,
                        height: this.state.value_w.toString(),
                        weight: this.state.sliderValue.toString(),
                        g_id: this.state.goalId,
                        trgt_weight: this.state.tarSliderValue.toString(),
                        gt_id: this.state.goalSubId,
                        fitness_level: this.state.activeId,
                        wt_id: this.state.wTimeId,
                        pf_id: this.state.professionId,
                        location_id: workLocIds,
                        equipment_id: eqpIds,
                        ff_id: ffIds,
                        health_id: healthIds,
                        allergy_id: allids,
                        is_veg: 0,
                        cuisine_id: '',
                        likes_id: '',
                        mg_id: subCatIds,
                        mg_val: subcatVals,
                        profesion_other: this.state.profesion_other,
                        hlthprblm_other: this.state.hlthprblm_other,
                        allergies_other: this.state.allergies_other,
                    });
                    LogUtils.infoLog(data);

                    this.saveWorkoutPlanQuestions(data);
                }
            }
        }

    }

    async onButtonPressed3() {


        if (this.state.qType === 1) {
            if (!this.state.age) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter your age' });
            } else {
                if (this.state.age >= 15 && this.state.age <= 80) {
                    this.setState({ qType: 2 });
                } else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Persons with age between 15 and 80 can only register' });
                    this.setState({ age: '' });
                }
            }
        }
        else if (this.state.qType === 2) {
            this.setState({ qType: 3 });
            setTimeout(() => {
                this._scrollView && this._scrollView.scrollTo({ x: this.state.contentOffset, y: 0, animated: true })
            }, 100);
        }
        else if (this.state.qType === 3) {
            this.setState({
                tarSliderValue: Math.floor(this.state.sliderValue),
                maxSliderValue: Math.floor(this.state.sliderValue),
            });
            this.setState({ qType: 4 });
            this.getGoals();
        }
        else if (this.state.qType === 4) {
            if (isSelected(this.state.goals)) {
                if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP' || this.state.goalCode === 'RM' || this.state.goalCode === 'WG') {
                    this.setState({ qType: 5 });
                    if (this.state.goalCode === 'RM') {
                        this.getSubGoals();
                    }
                    else {
                        if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
                            this.setState({ maxSliderValue: this.state.sliderValue, tarSliderValue: this.state.sliderValue });
                        }
                        else if (this.state.goalCode === 'WG') {
                            this.setState({ minSliderValue: this.state.sliderValue, maxSliderValue: 120, tarSliderValue: this.state.sliderValue });
                        }
                    }
                }
                else {
                    this.setState({ qType: 6 });
                }
            }
            else {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your fitness goal' });
            }
        }
        else if (this.state.qType === 5) {
            if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
                if (Math.floor(this.state.tarSliderValue) === Math.floor(this.state.sliderValue)) {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Target weight cannot be equal to your weight.Please change your target weight' });
                } else {
                    this.setState({ qType: 6 });
                }
            }
            else if (this.state.goalCode === 'WG') {
                if (Math.floor(this.state.tarSliderValue) === Math.floor(this.state.sliderValue)) {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Desired weight cannot be equal to your weight.Please change your desired weight' });
                } else {
                    this.setState({ qType: 6 });
                }
            }
            else if (this.state.goalCode === 'RM') {
                if (this.state.goalSubId !== 0 && isSelected(this.state.arrSubCatList)) {
                    this.setState({ qType: 6 });
                }
                else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select a marathon type' });
                }
            }

            // if (this.state.goalSubId !== 0) {
            //     this.setState({ qType: 6 });
            // }
            // else {
            //     if (this.state.goalCode === 'WL' || this.state.goalCode === 'PP') {
            //         this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your weight loss goal' });
            //     }
            //     else {
            //         this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select a marathon type' });
            //     }
            // }
        }
        else if (this.state.qType === 6) {
            this.setState({ qType: 7 });
        }
        // else if (this.state.qType === 7) {
        //     if (this.state.wTimeId === 0) {
        //         this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select at what time do you regularly workout?' });
        //     }
        //     else {
        //         this.setState({ qType: 8 });
        //     }
        // }
        else if (this.state.qType === 7) {
            if (this.state.professionId !== 0) {
                if (this.state.professionId === 99) {
                    if (this.state.profesion_other) {
                        this.setState({ qType: 8 });
                    }
                    else {
                        this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter your profession' });
                    }
                }
                else {
                    this.setState({ qType: 8, profesion_other: '' });
                }
            }
            else {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your profession' });
            }
        }
        else if (this.state.qType === 8) {

            if (this.state.workLocSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your location preference for working out' });
            }
            else {
                this.setState({ qType: 9 });
            }

        }
        else if (this.state.qType === 9) {
            if (this.state.eqpSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select the equipment you have access to' });
            }
            else {
                this.setState({ qType: 10 });
            }
        }
        else if (this.state.qType === 10) {
            if (this.state.formSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select fitness form of your preference' });
            }
            else {
                this.setState({ qType: 11 });
            }
        }
        else if (this.state.qType === 11) {
            if (this.state.healthSelectedFakeContactList.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select any current health problems we should be aware of' });
            }
            else {
                if (isOthersSelected(this.state.healthSelectedFakeContactList)) {
                    if (this.state.hlthprblm_other) {
                        this.setState({ qType: 12 });
                    }
                    else {
                        this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter any current health problems we should be aware of' });
                    }
                }
                else {
                    this.setState({ qType: 12, hlthprblm_other: '' });
                }

            }
        }
        else if (this.state.qType === 12) {
            if (this.state.SelectedfoodAlergiesArray.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select any food which you are allergic to' });
            }
            else {
                if (isOthersSelected(this.state.SelectedfoodAlergiesArray)) {
                    if (this.state.allergies_other) {
                        this.setState({ qType: 13 });
                    }
                    else {
                        this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter any food which you are allergic to' });
                    }
                }
                else {
                    this.setState({ qType: 13, allergies_other: '' });
                }
            }
        }
        else if (this.state.qType === 13) {
            if (this.state.SelectedFoodPreferenceArray.length === 0) {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your cuisine preference' });
            }
            else {
                this.setState({ qType: 14 });
                this.getFoodLikedItems();
            }
        }
        else if (this.state.qType === 14) {
            this.setState({ qType: 15 });
            this.setState({ nextSubmitText: 'Submit' });
        }
        else if (this.state.qType === 15) {

            let eqpIds = '';
            this.state.eqpSelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (eqpIds.length === 0) {
                        eqpIds = eqpIds + item.id;
                    }
                    else {
                        eqpIds = eqpIds + ',' + item.id;
                    }
                }
            });

            let healthIds = '';
            this.state.healthSelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (healthIds.length === 0) {
                        healthIds = healthIds + item.id;
                    }
                    else {
                        healthIds = healthIds + ',' + item.id;
                    }
                }
            });

            let foodIds = '';
            this.state.SelectedFoodPreferenceArray.map((item) => {
                if (item.check) {
                    if (foodIds.length === 0) {
                        foodIds = foodIds + item.id;
                    }
                    else {
                        foodIds = foodIds + ',' + item.id;
                    }
                }
            });

            let allids = '';
            this.state.SelectedfoodAlergiesArray.map((item) => {
                if (item.check) {
                    if (allids.length === 0) {
                        allids = allids + item.id;
                    }
                    else {
                        allids = allids + ',' + item.id;
                    }
                }
            });

            // let wIds = '';
            // this.state.SelectedfoodLikesArray.map((item) => {
            //     if (item.check) {
            //         if (wIds.length === 0) {
            //             wIds = wIds + item.fi_id;
            //         }
            //         else {
            //             wIds = wIds + ',' + item.fi_id;
            //         }
            //     }
            // })

            let workLocIds = '';
            this.state.workLocSelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (workLocIds.length === 0) {
                        workLocIds = workLocIds + item.id;
                    }
                    else {
                        workLocIds = workLocIds + ',' + item.id;
                    }
                }
            });

            let ffIds = '';
            this.state.formSelectedFakeContactList.map((item) => {
                if (item.check) {
                    if (ffIds.length === 0) {
                        ffIds = ffIds + item.ff_id;
                    }
                    else {
                        ffIds = ffIds + ',' + item.ff_id;
                    }
                }
            });

            let likeIds = '';
            this.state.foodLikes.map((item) => {
                item.food_icons.map((item1) => {
                    if (item1.check) {
                        if (likeIds.length === 0) {
                            likeIds = likeIds + item1.fi_id;
                        }
                        else {
                            likeIds = likeIds + ',' + item1.fi_id;
                        }
                    }

                });
            })

            // let disLikeIds = '';
            // this.state.foodLikes.map((item) => {
            //     if (item.dislike) {
            //         if (disLikeIds.length === 0) {
            //             disLikeIds = disLikeIds + item.f_id;
            //         }
            //         else {
            //             disLikeIds = disLikeIds + ',' + item.f_id;
            //         }
            //     }
            // })

            let foodArray = [];

            this.state.selBreakfastArray.map((item) => {
                const obj = {
                    'mt_id': 2,
                    'food_id': item.food_id,
                    'fs_id': item.fs_id,
                    'qnty': item.qty,
                    'mesurement': getPeace(item.food_description).trim(),
                    'cal': getCalories(item.food_description).trim(),
                };
                foodArray.push(obj);
            });

            this.state.selLunchArray.map((item) => {
                const obj = {
                    'mt_id': 4,
                    'food_id': item.food_id,
                    'fs_id': item.fs_id,
                    'qnty': item.qty,
                    'mesurement': getPeace(item.food_description).trim(),
                    'cal': getCalories(item.food_description).trim(),
                };
                foodArray.push(obj);
            });

            this.state.selDinnerArray.map((item) => {
                const obj = {
                    'mt_id': 6,
                    'food_id': item.food_id,
                    'fs_id': item.fs_id,
                    'qnty': item.qty,
                    'mesurement': getPeace(item.food_description).trim(),
                    'cal': getCalories(item.food_description).trim(),
                };
                foodArray.push(obj);
            });

            this.state.selSnacksArray.map((item) => {
                const obj = {
                    'mt_id': 5,
                    'food_id': item.food_id,
                    'fs_id': item.fs_id,
                    'qnty': item.qty,
                    'mesurement': getPeace(item.food_description).trim(),
                    'cal': getCalories(item.food_description).trim(),
                };
                foodArray.push(obj);
            });

            let data = JSON.stringify({

                type_id: 3,
                age: this.state.age,
                gender_id: this.state.isMaleFemale,
                height: this.state.value_w.toString(),
                weight: this.state.sliderValue.toString(),
                g_id: this.state.goalId,
                trgt_weight: this.state.tarSliderValue.toString(),
                gt_id: this.state.goalSubId,
                fitness_level: this.state.activeId,
                wt_id: this.state.wTimeId,
                pf_id: this.state.professionId,
                location_id: workLocIds,
                equipment_id: eqpIds,
                ff_id: ffIds,
                health_id: healthIds,
                allergy_id: allids,
                is_veg: this.state.isVeg,
                cuisine_id: foodIds,
                likes_id: likeIds,
                mg_id: subCatIds,
                mg_val: subcatVals,
                food_like: '',
                food_dislike: '',
                food_daily_intake: foodArray,
                profesion_other: this.state.profesion_other,
                hlthprblm_other: this.state.hlthprblm_other,
                allergies_other: this.state.allergies_other,
            });
            LogUtils.infoLog(data);
            this.saveWorkoutPlanQuestions(data);

        }

    }

    async onAccept() {

        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertTit: '', alertMsg: '' });

        if (this.state.isSuccess) {
            if (this.state.showAppointments === 1) {
                Actions.appFreeBook({appText: this.state.appoitText});
            }
            else {
                Actions.traineeWorkoutsHome({ isRefresh: 'yes', mFrom: 'dwques' });
            }
        }
    }

    async onDelete() {
        if (this.state.selDelType === 1) {
            let filteredArray = this.state.selBreakfastArray.filter(item => item.food_id !== this.state.delFoodId)
            this.setState({ selBreakfastArray: filteredArray });
        }
        else if (this.state.selDelType === 2) {
            let filteredArray = this.state.selLunchArray.filter(item => item.food_id !== this.state.delFoodId)
            this.setState({ selLunchArray: filteredArray });
        }
        else if (this.state.selDelType === 3) {
            let filteredArray = this.state.selDinnerArray.filter(item => item.food_id !== this.state.delFoodId)
            this.setState({ selDinnerArray: filteredArray });
        }
        else if (this.state.selDelType === 4) {
            let filteredArray = this.state.selSnacksArray.filter(item => item.food_id !== this.state.delFoodId)
            this.setState({ selSnacksArray: filteredArray });
        }

        this.setState({ isDeleteItem: false, deleteMsg: '', delFoodId: 0, selDelType: 0, });
    }

    async onDeny() {
        this.setState({ isDeleteItem: false, deleteMsg: '', delFoodId: 0, selDelType: 0, });
    }


    press = (hey) => {
        this.state.goals.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalId: item.id, goalCode: item.code, goalSubId: 0 });
            }
            else {
                item.check = false;
            }
        })
    }

    pressSubGoal = (hey) => {
        this.state.arrSubCatList.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalSubId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }

    pressActLevl = (hey) => {
        this.state.activeLevels.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ activeId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }

    pressWorkoutTimes = (hey) => {
        this.state.workoutTimes.map((item) => {
            if (item.wt_id === hey.wt_id) {
                item.check = true;
                this.setState({ wTimeId: item.wt_id });
            }
            else {
                item.check = false;
            }
        })

    }

    pressProfession = (hey) => {
        this.state.arrProfessions.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ professionId: item.id });
            }
            else {
                item.check = false;
            }
        })
    }

    pressWorkLoc = (hey) => {
        this.state.workoutlocations.map((item) => {
            if (item.id === hey.id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.workLocSelectedFakeContactList.push(item);
                } else if (item.check === false) {
                    const i = this.state.workLocSelectedFakeContactList.indexOf(item)
                    if (1 != -1) {
                        this.state.workLocSelectedFakeContactList.splice(i, 1)
                        return this.state.workLocSelectedFakeContactList
                    }
                }
            }
        })
        this.setState({ workLocfakeContact: this.state.workoutlocations })
    }

    pressEqpAcc = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearArray(hey);
        }
        else {
            this.state.equipments.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.eqpSelectedFakeContactList.push(item);
                    } else if (item.check === false) {
                        const i = this.state.eqpSelectedFakeContactList.indexOf(item)
                        if (1 != -1) {
                            this.state.eqpSelectedFakeContactList.splice(i, 1)
                            return this.state.eqpSelectedFakeContactList
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })
            this.removeNone();
            this.setState({ eqpfakeContact: this.state.equipments })
        }
    }

    onClearArray(hey) {
        const interval = setInterval(() => {
            this.setState({ eqpSelectedFakeContactList: [] });
            this.state.equipments.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check;
                    if (item.check === true) {
                        this.state.eqpSelectedFakeContactList.push(item);
                    }
                }
                else {
                    item.check = false;
                }
            });
            this.setState({ eqpfakeContact: this.state.equipments });
            clearInterval(interval);
        }, 100);
    };

    removeNone() {
        this.state.eqpSelectedFakeContactList.map((item) => {
            if (item.check) {

            }
            else {
                const i = this.state.eqpSelectedFakeContactList.indexOf(item)
                if (1 != -1) {
                    this.state.eqpSelectedFakeContactList.splice(i, 1)
                    return this.state.eqpSelectedFakeContactList
                }
            }
        })
    }

    pressFitnessForm = (hey) => {
        this.state.fitnessForms.map((item) => {
            if (item.ff_id === hey.ff_id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.formSelectedFakeContactList.push(item);
                } else if (item.check === false) {
                    const i = this.state.formSelectedFakeContactList.indexOf(item)
                    if (1 != -1) {
                        this.state.formSelectedFakeContactList.splice(i, 1)
                        return this.state.formSelectedFakeContactList
                    }
                }
            }
        })
        this.setState({ formfakeContact: this.props.fitnessform });

    }

    pressHealthCondition = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearHealthArray(hey);
        }
        else {
            this.state.healthConditions.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.healthSelectedFakeContactList.push(item);
                    } else if (item.check === false) {
                        const i = this.state.healthSelectedFakeContactList.indexOf(item)
                        if (1 != -1) {
                            this.state.healthSelectedFakeContactList.splice(i, 1)
                            return this.state.healthSelectedFakeContactList
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })

            this.removeHealthNone();
            this.setState({ healthfakeContact: this.state.healthConditions })
        }
    }

    onClearHealthArray(hey) {
        const interval = setInterval(() => {
            this.setState({ healthSelectedFakeContactList: [] });
            this.state.healthConditions.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check;
                    if (item.check === true) {
                        this.state.healthSelectedFakeContactList.push(item);
                    }
                }
                else {
                    item.check = false;
                }
            });
            this.setState({ fakeContact: this.state.healthConditions });
            clearInterval(interval);
        }, 100);
    };

    removeHealthNone() {
        this.state.healthSelectedFakeContactList.map((item) => {
            if (item.check) {

            }
            else {
                const i = this.state.healthSelectedFakeContactList.indexOf(item)
                if (1 != -1) {
                    this.state.healthSelectedFakeContactList.splice(i, 1)
                    return this.state.healthSelectedFakeContactList
                }
            }
        })
    }

    pressFoodPref = (hey) => {
        this.state.foodPreferences.map((item) => {
            if (item.id === hey.id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.SelectedFoodPreferenceArray.push(item);
                } else if (item.check === false) {
                    const i = this.state.SelectedFoodPreferenceArray.indexOf(item)
                    if (1 != -1) {
                        this.state.SelectedFoodPreferenceArray.splice(i, 1)
                        return this.state.SelectedFoodPreferenceArray
                    }
                }
            }
        })
        this.setState({ foodpreferencearray: this.state.foodPreferences })

    }

    pressAlergies = (hey) => {
        if (hey.name.toLowerCase() === 'none') {
            this.onClearAlergiesArray(hey);
        }
        else {
            this.state.foodAlergies.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check
                    if (item.check === true) {
                        this.state.SelectedfoodAlergiesArray.push(item);
                    } else if (item.check === false) {
                        const i = this.state.SelectedfoodAlergiesArray.indexOf(item)
                        if (1 != -1) {
                            this.state.SelectedfoodAlergiesArray.splice(i, 1)
                            return this.state.SelectedfoodAlergiesArray
                        }
                    }
                }
                else {
                    if (item.name.toLowerCase() === 'none') {
                        item.check = false
                    }
                }
            })
            this.removeAlergiesNone();
            this.setState({ foodAlergiesarray: this.state.foodAlergies })
        }
    }

    onClearAlergiesArray(hey) {
        const interval = setInterval(() => {
            this.setState({ SelectedfoodAlergiesArray: [] });
            this.state.foodAlergies.map((item) => {
                if (item.id === hey.id) {
                    item.check = !item.check;
                    if (item.check === true) {
                        this.state.SelectedfoodAlergiesArray.push(item);
                    }
                }
                else {
                    item.check = false;
                }
            });
            this.setState({ foodAlergiesarray: this.state.foodAlergies });
            clearInterval(interval);
        }, 100);
    };

    removeAlergiesNone() {
        this.state.SelectedfoodAlergiesArray.map((item) => {
            if (item.check) {

            }
            else {
                const i = this.state.SelectedfoodAlergiesArray.indexOf(item)
                if (1 != -1) {
                    this.state.SelectedfoodAlergiesArray.splice(i, 1)
                    return this.state.SelectedfoodAlergiesArray
                }
            }
        })
    }

    pressLikes = (hey) => {

        let newArray = this.state.foodLikes.slice();
        newArray.map((item) => {
            item.food_icons.map((item1) => {
                if (item1.fi_id === hey.fi_id) {
                    if (item1.check) {
                        item1.check = !item1.check
                    }
                    else {
                        if (isMaxFive(item.food_icons) >= 5) {
                            this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'You can select max five foods from each section ' });
                        }
                        else {
                            item1.check = !item1.check
                        }
                    }
                }
            });
        })
        this.setState({ foodLikes: newArray })

    }

    pressFoodLike = (hey) => {
        this.state.foodLikes.map((item) => {
            if (item.f_id === hey.f_id) {
                item.like = !item.like

                if (item.like === true) {
                    item.dislike = false;
                }
            }
        })
        this.setState({ foodLikesarray: this.state.foodLikes })
    }

    pressFoodDisLike = (hey) => {
        this.state.foodLikes.map((item) => {
            if (item.f_id === hey.f_id) {
                item.dislike = !item.dislike

                if (item.dislike === true) {
                    item.like = false;
                }
            }
        })
        this.setState({ foodLikesarray: this.state.foodLikes })
    }

    async searchItem(text) {
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/foodautocomplete`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_name: text,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (!this.state.isSearch) {
                        this.setState({ loading: false, films: data.data });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTit: 'Alert', alertMsg: SWR });
            });
    }

    async searchSubmit() {
        this.setState({ loading: true, isSearch: true, resObj: {} });
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/getfooditem`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_name: this.state.searchText,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    data.data.food.map((item) => {
                        item.qty = '1';
                        return data.data;
                    })

                    this.setState({ loading: false, isSearch: false, resObj: data.data });
                    //LogUtils.infoLog1('data 1',  this.state.resObj.food);
                    if (Array.isArray(this.state.resObj.food) && this.state.resObj.food.length) {

                    }
                    else {
                        this.setState({ loading: false, isSearch: false, noDataMsg: 'No data availabe for your search item' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: 'Alert', alertMsg: SWR });
            });
    }

    addItemToArray = (type, item) => {
        // LogUtils.infoLog1('type: ', type);
        LogUtils.infoLog1('Sel Item: ', item)
        if (type === 1) {
            let itemFound = false;
            if (this.state.selBreakfastArray.length > 0) {
                this.state.selBreakfastArray.map((breakitem, index) => {
                    if (item.food_id === breakitem.food_id) {
                        // this.state.selBreakfastArray[index].qty = item.qty;
                        itemFound = true;
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: 'Alert', alertMsg: 'You have already added this item!' });
                    }

                });
                if (!itemFound) {
                    this.setState({ selBreakfastArray: [...this.state.selBreakfastArray, item] })
                }


            } else {
                this.state.selBreakfastArray.push(item);
            }

        }
        else if (type === 2) {
            let itemFound = false;
            if (this.state.selLunchArray.length > 0) {
                this.state.selLunchArray.map((breakitem) => {
                    if (item.food_id === breakitem.food_id) {
                        itemFound = true;
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: 'Alert', alertMsg: 'You have already added this item!' });
                    }

                });

                if (!itemFound) {
                    this.setState({ selLunchArray: [...this.state.selLunchArray, item] })
                }


            } else {
                this.state.selLunchArray.push(item);
            }
            // this.state.selLunchArray.push(item);
        }
        else if (type === 3) {
            let itemFound = false;
            if (this.state.selDinnerArray.length > 0) {
                this.state.selDinnerArray.map((breakitem) => {
                    if (item.food_id === breakitem.food_id) {
                        itemFound = true;
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: 'Alert', alertMsg: 'You have already added this item!' });
                    }

                });

                if (!itemFound) {
                    this.setState({ selDinnerArray: [...this.state.selDinnerArray, item] })
                }


            } else {
                this.state.selDinnerArray.push(item);
            }
            // this.state.selDinnerArray.push(item);
        }
        else if (type === 4) {
            let itemFound = false;
            if (this.state.selSnacksArray.length > 0) {
                this.state.selSnacksArray.map((breakitem) => {
                    if (item.food_id === breakitem.food_id) {
                        itemFound = true;
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertTit: 'Alert', alertMsg: 'You have already added this item!' });
                    }

                });
                if (!itemFound) {
                    this.setState({ selSnacksArray: [...this.state.selSnacksArray, item] })
                }


            } else {
                this.state.selSnacksArray.push(item);
            }
            // this.state.selSnacksArray.push(item);
        }
    }

    renderFoodItemsList() {

        if (this.state.resObj && Array.isArray(this.state.resObj.food) && this.state.resObj.food.length) {
            return (
                <View style={{ position: 'absolute', marginTop: 100, zIndex: 1, height: hp('100%'), paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0, }}>
                    <FlatList
                        style={{ marginTop: 10, }}
                        contentContainerStyle={{ paddingBottom: hp('25%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.resObj.food}
                        // extraData={this.state}
                        keyExtractor={item => item.food_id}
                        ItemSeparatorComponent={this.FlatListItemSeparatorFoodList}
                        renderItem={({ item }) => {
                            return <TouchableOpacity
                                key={item.food_id}
                                onPress={() => {
                                    const result = Object.assign({}, item);
                                    this.setState({ isQtyPopVisible: true, selSubObj: result, qty: result.qty, qtyMeasurement: getPeace(result.food_description) });
                                }}
                                style={styles.containerListStyle}>

                                <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                                    <Text style={styles.textWorkName}>{`${item.food_name}`}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                        {this.renderFoodType(item)}

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textTitle}>{`${getCalories(item.food_description)}`}</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_serving.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textMesgment}>{`${getPeace(item.food_description)}`}</Text>
                                    </View>

                                </View>

                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_add_feed.png')}
                                    style={styles.imgAdd} />

                            </TouchableOpacity>
                        }} />
                </View>

            );
        }
        else {
            return (

                <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                    <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                </View>
            );
        }
    }

    findFilm(query) {
        if (query === '') {
            return [];
        }

        const { films } = this.state;
        const regex = new RegExp(`${query.trim()}`, 'i');
        return films.filter(film => film.name.search(regex) >= 0);
    }

    render() {
        const { searchText } = this.state;
        const films = this.findFilm(searchText);
        const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <Loader loading={this.state.loading} />
                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                    paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 40 : 0,
                    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textIndicator}>{this.state.qType} / {this.state.quesTotal}</Text>
                </View>

                <View style={styles.optionInnerContainer}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_app.png')}
                        style={styles.appImageStyle}
                    />
                </View>

                {this.renderQuestionTypeNew()}

                {/* {this.props.qst_type === 2
                    ? (
                        <View>
                            {this.renderQuestionType2()}
                        </View>
                    )
                    : (
                        <View>
                            {this.renderQuestionType3()}
                        </View>
                    )} */}

                {this.renderBottom()}

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTit}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <HomeDialog
                    visible={this.state.isDeleteItem}
                    title='Alert'
                    desc={this.state.deleteMsg}
                    onAccept={this.onDelete.bind(this)}
                    onDecline={this.onDeny.bind(this)}
                    no='No'
                    yes='Yes' />

                <Dialog
                    onDismiss={() => {
                        this.setState({ modalAddFoodVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ modalAddFoodVisible: false });
                    }}
                    // width={0.9}
                    // height={0.9}
                    dialogStyle={{ backgroundColor: '#ffffff', }}
                    visible={this.state.modalAddFoodVisible}>
                    <DialogContent
                        style={{
                            // backgroundColor: '#b0b0b0',
                            flex: 1,

                        }}>
                        <View style={styles.mainView}>

                            {/* title and cal details */}
                            <View style={{
                                flexDirection: 'row',
                                margin: 10,
                            }}>
                                {/* <View style={{ position: 'absolute', zIndex: 111 }}> */}
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, }}
                                    onPress={() => this.setState({ modalAddFoodVisible: false })}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                                {/* </View> */}

                                <Text style={styles.textHeadTitle}>Add to {this.state.modalTitle}</Text>

                            </View>

                            <View style={styles.popContainer}>

                                <Autocomplete
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    style={{
                                        backgroundColor: 'transparent',
                                        borderRadius: 0,
                                        height: 40,
                                        fontSize: 13,
                                        padding: 10,
                                        textAlign: 'left',
                                        letterSpacing: 0.23,
                                        fontFamily: 'Rubik-Regular',
                                        color: '#282c37',
                                    }}
                                    inputContainerStyle={{
                                        borderRadius: 4,
                                        borderColor: '#cccccc',
                                        borderWidth: 1,
                                    }}
                                    containerStyle={styles.autocompleteContainer}
                                    data={films.length === 1 && comp(searchText, films[0].name) ? [] : films}
                                    defaultValue={searchText}
                                    onChangeText={text => {
                                        this.setState({ searchText: text });
                                        this.searchItem(text);
                                    }}
                                    placeholder="Search food"
                                    returnKeyType='search'
                                    onSubmitEditing={() => {
                                        this.searchSubmit()
                                        this.setState({ films: [] })
                                    }}
                                    renderItem={({ item }) => (
                                        <TouchableOpacity key={item.name} onPress={() => {
                                            this.setState({ searchText: item.name });
                                            this.searchSubmit();
                                            this.setState({ films: [] })
                                            this.searchItem({ text: '' })
                                        }
                                        }>
                                            <Text style={styles.itemText}>
                                                {item.name}
                                            </Text>
                                        </TouchableOpacity>
                                    )}
                                />

                            </View>
                            {this.renderFoodItemsList()}
                        </View>

                    </DialogContent>
                </Dialog>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isQtyPopVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isQtyPopVisible: false });
                    }}
                    width={0.8}
                    visible={this.state.isQtyPopVisible}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Text style={styles.textNodataTitle}>{`${this.state.selSubObj.food_name}`}</Text>
                                    {/* <Text style={styles.textPopQtyName}>{`${this.state.selSubObj.food_name}`}</Text> */}

                                    {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', paddingTop: 2, }}>
                                            {this.renderFoodType(this.state.selSubObj)}

                                             <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_cal.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}
                                            /> 
                                            <Text style={styles.textTitle}>{`${getCalories(this.state.selSubObj.food_description)}`}</Text>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_serving.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.textMesgment}>{`${getPeace(this.state.selSubObj.food_description)}`}</Text>
                                        </View> */}

                                    <View style={{ flexDirection: 'row', alignSelf: 'flex-start', alignItems: 'flex-start', paddingTop: 2, }}>
                                        <Text style={styles.textSelQty}>Quantity</Text>
                                        <Text style={styles.textSelMesgment}>({this.state.qtyMeasurement})</Text>
                                    </View>

                                    <View style={styles.spinnersBg}>
                                        <TextInput
                                            style={styles.textInputStyle1}
                                            keyboardType="numeric"
                                            placeholder="Qty"
                                            placeholderTextColor="grey"
                                            maxLength={3}
                                            value={this.state.qty}
                                            returnKeyType='done'
                                            onChangeText={text => {
                                                this.setState({ qty: text });
                                                this.state.selSubObj.qty = text;
                                            }}
                                        />
                                    </View>


                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            if (this.state.selSubObj.qty) {
                                                var value1 = parseInt(this.state.selSubObj.qty);

                                                if (value1 >= 1) {
                                                    this.setState({ modalAddFoodVisible: false, isQtyPopVisible: false });
                                                    this.addItemToArray(this.state.foodType, this.state.selSubObj);
                                                }
                                                else {
                                                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter valid quantity' });
                                                }
                                            }
                                            else {
                                                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter quantity' });
                                            }

                                        }}>
                                            <Text style={styles.textSave}>Submit</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

            </ImageBackground>
        );
    }
}

TraineeDietWorkQuestions.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    largeInterval: PropTypes.number,
    mediumInterval: PropTypes.number,
    value: PropTypes.number,
    onChange: PropTypes.func,
    styles: PropTypes.object,
}

TraineeDietWorkQuestions.defaultProps = {
    min: 130,
    max: 210,
    mediumInterval: 5,
    largeInterval: 10,
    onChange: () => { },
    styles: {},
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 30,
        padding: 6,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    triangleshape: {
        width: 0,
        height: 0,
        borderTopWidth: 15,
        borderWidth: 10,
        borderBottomWidth: 0,
        borderColor: 'transparent',
        borderTopColor: '#8c52ff',
        alignSelf: 'center',
        marginBottom: -12,
    },
    intervals: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: GAUGE_WIDTH / 2,
        marginHorizontal: -INTERVAL_WIDTH / 2,
    },
    intervalContainer: {
        width: INTERVAL_WIDTH,
        alignItems: 'center',
    },
    interval: {
        width: 1,
        backgroundColor: '#8c52ff',
    },
    intervalValue: {
        width: 35,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#8c52ff',
        position: 'absolute',
        bottom: -65,
    },
    small: {
        height: 20,
    },
    medium: {
        height: 25,
    },
    large: {
        backgroundColor: '#8c52ff',
        width: 3,
        height: 35,
        position: 'absolute',
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: 40,
    },
    personImageStyle: {
        alignSelf: 'flex-end',
        width: 65,
        height: 220,
        marginTop: 20,
        marginRight: -40,
    },
    textStyle: {
        fontSize: 18,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    desc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('2%'),
        marginBottom: hp('2%'),
    },
    sliderImageStyle: {
        width: wp('75%'),
        height: hp('10%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    personViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    weighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    weighttextStyle1: {
        fontSize: 10,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        marginTop: 5,
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    tarweighttextStyle: {
        fontSize: 22,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        marginTop: 10,
        fontFamily: 'Rubik-Medium',
        left: wp('12.5%'),
    },
    HeighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',

    },
    HeighttextStyle1: {
        fontSize: 12,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
        marginTop: 5,
    },
    textIndicator: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    tvFoodLikeText: {
        width: wp('68%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
    mobileImageStyle1: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 12, tintColor: '#d6d5dd', },
    mobileImageStyle2: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 12, },
    mobileImageStyle3: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10, tintColor: '#d6d5dd', },
    viewListItem: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    imgVeg: { width: wp('28%'), height: 100, alignSelf: 'center', position: 'relative', borderTopLeftRadius: 10, borderTopRightRadius: 10 },
    vegText: {
        alignSelf: 'center',
        fontSize: 12,
        fontWeight: '600',
        flex: 1,
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        padding: 7,
    },
    vegSelect: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginLeft: 5, marginRight: 10 },
    containerIsVeg: {
        width: wp('90%'),
        flexDirection: 'column',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: hp('1%'),
        marginBottom: hp('1%'),
    },
    containerVegStyle: {
        width: wp('90%'),
        height: 50,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    vegSelect: {
        width: 20,
        height: 20,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 10
    },
    containerLikeStyle: {
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        margin: 5,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    item: {
        margin: 1,
        width: wp('29%'),
        alignItems: 'center',
        marginTop: 3,
        justifyContent: 'center',
        height: 130, // approximate a square
    },
    containerFoodStyle: {
        width: wp('28%'),
        marginTop: hp('2%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        margin: 5,
    },
    containerGender: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: hp('4%'),
    },
    containerFemaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        left: wp('5%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
    },
    containerMaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    genCheckImgStyle: {
        width: 25,
        height: 25,
        position: 'absolute',
        top: 20,
        right: 0,
        marginRight: 20
    },
    genImg: {
        height: 40,
        width: 35,
        alignSelf: 'center',
        marginTop: hp('3%'),
    },
    maleFemaleText: {
        alignSelf: 'center',
        fontSize: 14,
        marginTop: hp('2%'),
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.26,
        fontFamily: 'Rubik-Medium',
    },
    genderDesc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('3%'),
    },
    countryText1: {
        width: wp('65%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },

    viewPlusMinus: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    txtAvgVal: {
        alignSelf: 'center',
        fontSize: 13,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        marginLeft: -5,
        marginRight: 5,
        fontFamily: 'Rubik-Medium',
    },
    containerMeasurementStyle: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    breakfastBg: {
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
    },
    textBreakfast: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        flex: 1,
        alignSelf: "flex-start",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
        marginTop: 5,
    },
    nutrition1: {
        flexDirection: 'row',
        marginTop: 10,
    },
    nutritionInnerView: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
        marginLeft: 10
    },
    textNutTitle: {
        fontSize: 13,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.23,
    },
    textNutDesc: {
        fontSize: 11,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 18,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    textHeadTitle: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textFoodType: {
        fontSize: 10,
        width: wp('25%'),
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textFoodType1: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    popContainer: {
        width: wp('90%'),
        backgroundColor: 'transparent',
        flexDirection: 'row',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        zIndex: 10,
    },
    autocompleteContainer: {
        width: wp('90%'),
        backgroundColor: 'transparent',
        flex: 1,
        alignSelf: 'flex-start',
        position: 'relative',
    },
    itemText: {
        fontSize: 15,
        margin: 10,
        fontFamily: 'Rubik-Regular',
    },
    textWorkName: {
        fontSize: 14,
        width: wp('80%'),
        fontWeight: '500',
        paddingRight: 10,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textMesgment: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        flex: 1,
        marginRight: 20,
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
    },
    textNodata: {
        width: wp('100%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 20,
        marginTop: 15,
        marginTop: hp('35%'),
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    imgAdd: { width: 25, height: 25, position: 'relative', alignSelf: 'center', right: 15 },
    containerListStyle: {
        width: wp('90%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 6,
        padding: 15,
        position: 'relative',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textNodataTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textPopQtyName: {
        fontSize: 14,
        width: wp('70%'),
        fontWeight: '500',
        padding: 10,
        textAlign: 'left',
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        marginTop: 10,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 30,
    },
    textSelQty: {
        color: '#8c52ff',
        marginLeft: wp('5%'),
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        fontSize: 12,
        marginBottom: 5,
        marginTop: 20,
        textAlign: 'left',
        fontWeight: '500',
    },
    textSelMesgment: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        marginRight: 10,
        marginLeft: 5,
        marginTop: 20,
        textAlign: 'left',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
    },
    spinnersBg: {
        width: wp('70%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 6,
        position: 'relative',
        alignContent: 'center',
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
    },
    textInputStyle1: {
        height: 45,
        width: wp('70%'),
        alignSelf: 'flex-start',
        fontSize: 14,
        padding: 10,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: .5,
        fontFamily: 'Rubik-Regular',
    },
    containerOthersStyle: {
        width: wp('90%'),
        // height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        marginLeft: 15,
        marginRight: 15,
        padding: 25,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    tiViewBack: {
        // width: wp('80%'),
        borderWidth: 1,
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        marginTop: 10,
        // padding: 5,
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        // padding: 8,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    txtLikeFoodTit: {
        fontSize: 15,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
});

const mapStateToProps = state => {
    const { goals, profdet } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals, profdet };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getProfileDet },
    )(TraineeDietWorkQuestions),
);
