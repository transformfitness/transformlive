import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Image, Dimensions, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { RFValue } from "react-native-responsive-fontsize";
import LogUtils from '../../utils/LogUtils';

const screenHeight = Math.round(Dimensions.get('window').height);

function isEmpty(obj) {
    try {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    } catch (error) {
        console.log(error)
    }
}

const Macros = (props) => {
    const renderMacrosData = (data) => {
    
        if (!isEmpty(data.macros)) {
          
            // if (data.macros.show_diet === "1") {
            //     if (data.macros.is_diet_plan === 1 ) {
            //         if (data.macros.trail_user === 0) {
                        return (
                            <View>
                                {/* <View style={{ height: 15 }}></View> */}
                                <View style={styles.workOutBgNew}>
                                    <View style={styles.vHeaderTitMore}>
                                        <Text style={styles.textPlan}>{'Macros'.toUpperCase()}</Text>

                                        <TouchableOpacity
                                            onPress={() => {
                                                LogUtils.firebaseEventLog('click', {
                                                    p_id: 230,
                                                    p_category: 'Home',
                                                    p_name: 'Home->PCFDetails',
                                                });
                                                Actions.pcfdetails();
                                            }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../../res/ic_info_blueround.png')}
                                                style={styles.infoAppBlue}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    {/* <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View> */}
                                    <View style={{
                                        borderBottomLeftRadius: 6,
                                        borderBottomRightRadius: 6,
                                        width: wp('89%'),
                                        marginTop: 10,
                                        alignSelf: 'center',
                                        flexDirection: 'column',
                                    }}>
                                        <View style={{
                                            width: wp('87%'),
                                            height: 50,
                                            borderColor: '#ddd',
                                            borderRadius: 10,
                                            borderWidth: .5,
                                            justifyContent: 'center',
                                            backgroundColor: '#4b9da2',
                                            marginBottom: 10,
                                            alignSelf: 'center'
                                        }}>
                                            {renderCarbs(data.macros.carbs)}

                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={styles.optnText}>Carbohydrates</Text>
                                            </View>
                                            <Text style={styles.percentageText}>{`${data.macros.carbs_text}`}</Text>
                                        </View>

                                        <View style={{
                                            width: wp('87%'),
                                            height: 50,
                                            borderColor: '#ddd',
                                            borderRadius: 10,
                                            borderWidth: .5,
                                            justifyContent: 'center',
                                            backgroundColor: '#e88eaa',
                                            marginBottom: 10,
                                            alignSelf: 'center'
                                        }}>
                                            {renderProtiens(data.macros.proteins)}
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={styles.optnText}>Proteins</Text>
                                            </View>
                                            <Text style={styles.percentageText}>{`${data.macros.proteins_text}`}</Text>
                                        </View>

                                        <View style={{
                                            width: wp('87%'),
                                            height: 50,
                                            borderColor: '#ddd',
                                            borderRadius: 10,
                                            borderWidth: .5,
                                            justifyContent: 'center',
                                            backgroundColor: '#d2b54e',
                                            marginBottom: 10,
                                            alignSelf: 'center'
                                        }}>
                                            {renderFat(data.macros.fat)}
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={styles.optnText}>Fats</Text>
                                            </View>
                                            <Text style={styles.percentageText}>{`${data.macros.fat_text}`}</Text>
                                        </View>

                                        <View style={{
                                            width: wp('87%'),
                                            height: 50,
                                            borderColor: '#ddd',
                                            borderRadius: 10,
                                            borderWidth: .5,
                                            justifyContent: 'center',
                                            backgroundColor: '#a889f5',
                                            marginBottom: 10,
                                            alignSelf: 'center'
                                        }}>
                                            {renderFiber(data.macros.fiber)}
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={styles.optnText}>Fiber</Text>
                                            </View>
                                            <Text style={styles.percentageText}>{`${data.macros.fiber_text}`}</Text>
                                        </View>

                                    </View >
                                </View >
                            </View >
                        );
            //         }
            //         else {
            //             return (
            //                 <View />
            //             );
            //         }
            //     }
            //     else {
            //         return (
            //             <View />
            //         );
            //     }
            // }
            // else {
            //     return (
            //         <View />
            //     );
            // }
        }
    }

    const renderCarbs = (carbs) => {
        if (carbs === 1) {
            return (
                <View style={{
                    width: `${carbs}%`, height: 42, position: 'absolute', backgroundColor: '#75c7cc',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else if (carbs === 2) {
            return (
                <View style={{
                    width: `${carbs}%`, height: 46, position: 'absolute', backgroundColor: '#75c7cc',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else {
            return (
                <View style={{
                    width: `${carbs}%`, height: 50, position: 'absolute', backgroundColor: '#75c7cc',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                    borderTopRightRadius: (carbs === 100) ? 10 : 0,
                    borderBottomRightRadius: (carbs === 100) ? 10 : 0,
                }}>
                </View>
            );
        }
    }

    const renderProtiens = (proteins) => {
        if (proteins === 1) {
            return (
                <View style={{
                    width: `${proteins}%`, height: 42, position: 'absolute', backgroundColor: '#fda3c0',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else if (proteins === 2) {
            return (
                <View style={{
                    width: `${proteins}%`, height: 46, position: 'absolute', backgroundColor: '#fda3c0',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else {
            return (
                <View style={{
                    width: `${proteins}%`, height: 50, position: 'absolute', backgroundColor: '#fda3c0',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                    borderTopRightRadius: (proteins === 100) ? 10 : 0,
                    borderBottomRightRadius: (proteins === 100) ? 10 : 0,
                }}>
                </View>
            );
        }
    }

    const renderFat = (fat) => {
        if (fat === 1) {
            return (
                <View style={{
                    width: `${fat}%`, height: 42, position: 'absolute', backgroundColor: '#efd266',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else if (fat === 2) {
            return (
                <View style={{
                    width: `${fat}%`, height: 46, position: 'absolute', backgroundColor: '#efd266',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else {
            return (
                <View style={{
                    width: `${fat}%`, height: 50, position: 'absolute', backgroundColor: '#efd266',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                    borderTopRightRadius: (fat === 100) ? 10 : 0,
                    borderBottomRightRadius: (fat === 100) ? 10 : 0,
                }}>
                </View>
            );
        }
    }

    const renderFiber = (fiber) => {
        if (fiber === 1) {
            return (
                <View style={{
                    width: `${fiber}%`, height: 42, position: 'absolute', backgroundColor: '#9671f4',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else if (fiber === 2) {
            return (
                <View style={{
                    width: `${fiber}%`, height: 46, position: 'absolute', backgroundColor: '#9671f4',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                }}>
                </View>
            );
        } else {
            return (
                <View style={{
                    width: `${fiber}%`, height: 50, position: 'absolute', backgroundColor: '#9671f4',
                    justifyContent: 'center',
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                    borderTopRightRadius: (fiber === 100) ? 10 : 0,
                    borderBottomRightRadius: (fiber === 100) ? 10 : 0,
                }}>
                </View>
            );
        }
    }

    return (
        <View>
            {renderMacrosData(props)}
        </View>
    );
}

const styles = StyleSheet.create({
    vHeaderTitMore: {
        width: wp('90%'),
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
        right:5
    },
    textPlan: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        fontWeight: '500',
        left:24
    },
    textAdd: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
    },
    containerFoodStyle: {
        width: wp('27%'),
        marginTop: hp('1%'),
        marginBottom: hp('2%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        // borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        // shadowColor: "#ddd",
        // shadowOffset: {
        //   width: 0,
        //   height: 2,
        // },
        //shadowOpacity: 0.25,
        //shadowRadius: 3.84,
        elevation: 3,
        marginLeft: 0.5,
        marginRight: 0.5,
    },
    imgVeg: {
        width: wp('27%'),
        height: 100,
        alignSelf: 'center',
        position: 'relative',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    vegText: {
        fontSize: 10,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    priceText: {
        fontSize: 11,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Medium',
    },
    workOutBgNew: {
        width: wp('94%'),
        marginLeft: wp('3%'),
        marginRight: wp('3%'),
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        //position: 'relative',
        alignSelf: 'center',
    },
    infoAppBlue: {
        width: 25,
        height: 25,
        alignSelf: 'center',
    },
    optnText: {
        color: 'white',
        fontFamily: 'Rubik-Medium',
        fontSize: RFValue(14, screenHeight),
        fontWeight: '400',
        letterSpacing: 0.23,
        margin: 10,
        textAlign: 'left',
        position: 'absolute',
    },
    percentageText: {
        color: 'white',
        fontFamily: 'Rubik-Medium',
        fontSize: RFValue(10, screenHeight),
        fontWeight: '500',
        letterSpacing: 0.23,
        margin: 10,
        textAlign: 'right',
      },
})

export default Macros;