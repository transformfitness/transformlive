/*Example of React Native Video*/
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, BackHandler } from 'react-native';
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { forbidFunction } from '../utils/ScreenshotUtils.js';

class PlayVideo extends Component {
  videoPlayer;

  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'contain',
      onBuffer: false,
    };
  }

  componentWillMount() {

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      // do something
      //console.log("orientation", initial.toString())
    } else {
      // do something else
    }
  }

  async componentDidMount() {
    forbidFunction();
    // this locks the view to Portrait Mode
    // Orientation.lockToPortrait();

    // this locks the view to Landscape Mode
    // Orientation.lockToLandscapeRight();

    // this unlocks any previous locks to all Orientations
    Orientation.unlockAllOrientations();

    Orientation.addOrientationListener(this._orientationDidChange);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });

  }

  async onBackPressed() {
    Orientation.lockToPortrait();
    Actions.pop();
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      //console.log(`Changed Device Orientation: ${orientation}`);
      // do something with landscape layout
      Orientation.lockToLandscape();
    } else {
      // do something with portrait layout
      //console.log(`Changed Device Orientation: ${orientation}`);
      Orientation.lockToPortrait();
    }
  }

  _orientationRemove = (orientation) => {
    orientation.lockToPortrait();
  }

  componentWillUnmount() {
    this.backHandler.remove();

    Orientation.getOrientation((err, orientation) => {
      //console.log(`Current Device Orientation: ${orientation}`);
    });


    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationRemove);
  }

  onSeek = seek => {
    //Handler for change in seekbar
    this.videoPlayer.seek(seek);
  };

  onPaused = playerState => {
    //Handler for Video Pause
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };

  onReplay = () => {
    //Handler for Replay
    this.setState({ playerState: PLAYER_STATES.PLAYING });
    this.videoPlayer.seek(0);
  };

  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };

  onLoad = data => {
    //console.log(data.duration); 
    this.setState({ duration: data.duration, isLoading: false });
  }

  onLoadStart = data => this.setState({ isLoading: true });

  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });

  onError = () => alert('Oh! ', error);

  exitFullScreen = () => {
    alert('Exit full screen');
  };

  enterFullScreen = () => { };

  onFullScreen = () => {
    if (this.state.screenType == 'content')
      this.setState({ screenType: 'cover' });
    else this.setState({ screenType: 'content' });
  };
  renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );
  onSeeking = currentTime => this.setState({ currentTime });

  render() {
    return (
      <View style={{ flex: 1 }}>

        <View style={styles.container}>

          <Video
            onEnd={this.onEnd}
            onLoad={this.onLoad}
            onLoadStart={this.onLoadStart}
            onProgress={this.onProgress}
            paused={this.state.paused}
            ref={videoPlayer => (this.videoPlayer = videoPlayer)}
            resizeMode={this.state.screenType}
            onFullScreen={this.state.isFullScreen}
            source={{ uri: this.props.videoURL }}
            style={styles.mediaPlayer}
            volume={10}
            hls={true}
          />
          <MediaControls
            duration={this.state.duration}
            isLoading={this.state.isLoading}
            mainColor="#333"
            //   onFullScreen={this.onFullScreen}
            onPaused={this.onPaused}
            onReplay={this.onReplay}
            onSeek={this.onSeek}
            onSeeking={this.onSeeking}
            playerState={this.state.playerState}
            progress={this.state.currentTime}
          //   toolbar={this.renderToolbar()}
          />
          <View style={styles.containerTop}>
            <TouchableOpacity
              onPress={() => this.onBackPressed()}>
              <Image
                source={require('../res/ic_back.png')}
                style={styles.backImageStyle}
              />
            </TouchableOpacity>
          </View>

        </View>

      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  containerTop: {
    flexDirection: 'column',
    position: 'absolute',
    top: 0,
  },
  backImageStyle: {
    width: 20,
    height: 16,
    margin: hp('3%'),
    alignSelf: 'flex-start',
    tintColor: 'white',
  },
});
export default PlayVideo;