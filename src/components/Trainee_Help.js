import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
    Linking,
    ToastAndroid,
    Platform,
    Alert
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import { connect } from 'react-redux';

import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
    DialogContent,
    DialogFooter,
    DialogButton,
} from 'react-native-popup-dialog';
import { Calendar, CalendarList, Arrow, Agenda } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
LocaleConfig.locales['en'] = {
    monthNames: ['January', 'February ', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'en';
import moment from "moment";
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getToDate() {
    let now = new Date()
    let next60days = new Date(now.setDate(now.getDate() + 13))


    return next60days;
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_Help extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            noDataMsg: '',
            msgSelectDate: 'Please select date...',
            arrAllTrainers: [],
            selected: '',
            visible: false,
            day: '',
            month: '',
            year: '',
            confirmPop: false,
            isInternet: false,
            dateConString: '',
            arrTrainerSlots: [],
            selTrID: 0,
            selSlotObj: {},
            isShowInfo: true,
            resObject: {}
        };
    }

    async componentDidMount() {

        allowFunction();

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllAppointmentTypes();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getAllAppointmentTypes() {
        this.setState({ loading: false });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/helpqst`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObject: data.data, });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    // async getAllTrainers() {
    //     this.setState({ loading: true });
    //     let token = await AsyncStorage.getItem('token');
    //     fetch(
    //         `${BASE_URL}/trainee/appointmenttrainer`,
    //         {
    //             method: 'GET',
    //             headers: {
    //                 Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 Authorization: `Bearer ${token}`,
    //             },
    //         },
    //     )
    //         .then(processResponse)
    //         .then(res => {
    //             const { statusCode, data } = res;
    //             LogUtils.infoLog1('statusCode', statusCode);
    //             LogUtils.infoLog1('data', data);
    //             if (statusCode >= 200 && statusCode <= 300) {
    //                 if (data.data.length > 0) {
    //                     data.data.map((item) => {
    //                         item.check = false;
    //                     })
    //                     this.setState({ loading: false, arrAllTrainers: data.data });
    //                 }
    //                 else {
    //                     this.setState({ loading: false, noDataMsg: 'No trainers available at this moment' });
    //                 }
    //             } else {
    //                 if (data.message === 'You are not authenticated!') {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message });
    //                 } else {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message });
    //                 }
    //             }
    //         })
    //         .catch(function (error) {
    //             this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
    //         });
    // }

    async getAllTrainerSlots() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/getcallcenterslots`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    // tr_id: this.state.selTrID,
                    seldate: this.state.selected,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.check = false;
                        })
                        this.setState({ loading: false, arrTrainerSlots: data.data });
                    }
                    else {
                        this.setState({ loading: false, arrTrainerSlots: [], msgSelectDate: 'No slots available for the selected date' });
                    }


                } else {
                    this.setState({ arrTrainerSlots: [] });

                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async bookAppointment() {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            seldate: this.state.selected,
            from_time: this.state.selSlotObj.from_time,
            to_time: this.state.selSlotObj.to_time,
            hq_id: this.state.selTrID,
        }));
        fetch(
            `${BASE_URL}/trainee/savecallcenterslot`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    seldate: this.state.selected,
                    from_time: this.state.selSlotObj.from_time,
                    to_time: this.state.selSlotObj.to_time,
                    hq_id: this.state.selTrID,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    Actions.traHelpSucc({ title: data.title, infoText: data.message });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    ListEmptyView = () => {
        return (
            <View style={styles.viewNoData}>
                <Text style={styles.textNoData}>{this.state.noDataMsg}</Text>
            </View>

        );
    }

    ListSlotsEmptyView = () => {
        return (
            <View style={{
                padding: 10,
                marginTop: 10,
                height: 40,
            }}>
                <Text style={styles.textNoData}></Text>
                {/* <Text style={styles.textNoData}>{this.state.msgSelectDate}</Text> */}
            </View>
        );
    }

    onBackPressed() {
        if (this.state.isShowInfo) {
            Actions.popTo('traineeHome');
        }
        else {
            this.setState({ isShowInfo: true });
        }

    }

    press = (hey) => {
        this.state.arrTrainerSlots.map((item) => {
            if (item.tas_id === hey.tas_id) {
                item.check = true;
                this.setState({ selSlotObj: item });
            }
            else {
                item.check = false;
            }
        })
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, visible: false, confirmPop: false });
                this.getAllAppointmentTypes();
            }
            else {
                this.setState({ isInternet: true, visible: false, confirmPop: false });
            }
        });
    }

    getDesc() {
        let descText = this.state.resObject.desc.split('+91-');
        return descText[0]
    }

    getMobile() {
        let descText = this.state.resObject.desc.split('+91-');
        return descText[1]
    }

    renderHelpLists() {
        if (!isEmpty(this.state.resObject)) {
            if (this.state.isShowInfo) {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/img_customer_help.png')}
                            style={styles.imgNoSuscribe}
                        />
                        <Text style={styles.textNodataTitle}>{this.state.resObject.title}</Text>

                        <TouchableOpacity
                            activeOpacity={0.7}
                            onLongPress={() => {
                                 Clipboard.setString(this.getMobile());
                                 if(Platform.OS === 'android') {
                                    ToastAndroid.show('Mobile number copied', ToastAndroid.SHORT);
                                 }
                                 else {
                                    Alert.alert('Mobile number copied');
                                 }
                                 
                            }}
                            onPress={() => {
                               
                                let url = "whatsapp://send?text= &phone=91" + this.getMobile();
                                Linking.openURL(url)
                                    .then(data => {
                                        LogUtils.infoLog1("WhatsApp Opened successfully ", data);
                                    })
                                    .catch(() => {
                                        this.setState({ isAlert: true, alertMsg: 'Make sure WhatsApp installed on your device' });
                                    });
                            }}>
                            <View>
                                <Text style={{
                                    marginTop: 15,
                                    width: wp('85%'),
                                    textAlign: 'center',
                                }}>
                                    <Text style={styles.textNodata}>{this.getDesc()}</Text>

                                    <Text style={styles.textMobile}>{this.getMobile()}</Text>

                                </Text>
                            </View>
                        </TouchableOpacity>





                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ isShowInfo: false });
                            }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>BOOK A SLOT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                );
            }
            else {
                return (
                    <View>
                        {/* <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Help</Text>
                    </View> */}
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('13%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.resObject.qst}
                            keyExtractor={item => item.hq_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            ListEmptyComponent={this.ListEmptyView}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.hq_id} style={styles.aroundListStyle} onPress={() => {
                                    Actions.traHelpBook({ selItem: item });
                                }}>
                                    <View style={{ flexDirection: 'column', paddingLeft: 10, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={styles.viewBlueDot}></View>
                                            <Text style={styles.textWorkName}>{`${item.title}`}</Text>
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View>
                );
            }
        }
    }


    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Help</Text>
                    </View>



                    {this.renderHelpLists()}

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ visible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ visible: false });
                    }}
                    width={1}
                    height={0.93}
                    dialogStyle={{ backgroundColor: '#fafafa', borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, position: 'absolute', bottom: 0, left: 0, width: wp('100%') }}
                    visible={this.state.visible}
                // rounded
                // actionsBordered
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff',
                        }}>
                        <View style={{ flexDirection: 'column', backgroundColor: '#ffffff' }}>
                            <View style={{
                                flexDirection: 'row',
                                margin: 15,
                            }}>
                                <Text style={styles.textSelTime}>Choose date and time</Text>

                            </View>
                            <View style={{ marginLeft: -20, height: 1.5, width: wp('100%'), backgroundColor: '#e9e9e9' }}></View>
                            <Calendar
                                // Initially visible month. Default = Date()
                                current={this.state.selected}
                                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                minDate={new Date()}
                                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                maxDate={getToDate()}
                                // Handler which gets executed on day press. Default = undefined
                                onDayPress={(day) => {
                                    //LogUtils.infoLog1('selected day', day);
                                    this.setState({ selected: day.dateString });
                                    this.setState({ day: day.day });
                                    this.setState({ month: day.month });
                                    this.setState({ year: day.year });
                                    // const date = new Date(2019, 12, 10);  // 2009-11-10
                                    // const month = date.toLocaleString('default', { month: 'short' });
                                    // LogUtils.infoLog(month);
                                    var gsDayNames = [
                                        'Sunday',
                                        'Monday',
                                        'Tuesday',
                                        'Wednesday',
                                        'Thursday',
                                        'Friday',
                                        'Saturday'
                                    ];
                                    var gsMonNames = [
                                        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July.', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                                    ];

                                    var d = new Date(day.dateString);
                                    var dayName = gsDayNames[d.getDay()];
                                    var monName = gsMonNames[d.getMonth()];

                                    // LogUtils.infoLog1(dayName, monName);
                                    this.setState({ dateConString: dayName + ', ' + monName + ' ' + day.day + ', ' + day.year });

                                    this.getAllTrainerSlots();
                                }}

                                // Handler which gets executed on day long press. Default = undefined
                                // onDayLongPress={(day) => { LogUtils.infoLog1('selected day', day) }}
                                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                                monthFormat={'MMM yyyy'}
                                // Handler which gets executed when visible month changes in calendar. Default = undefined
                                onMonthChange={(month) => { LogUtils.infoLog1('month changed', month) }}
                                firstDay={1}
                                markedDates={{ [this.state.selected]: { selected: true, disableTouchEvent: true, selectedDotColor: '#8c52ff' } }}
                                theme={{
                                    // calendarBackground: '#333248',
                                    // textSectionTitleColor: 'white',
                                    // dayTextColor: 'red',
                                    todayTextColor: '#8c52ff',
                                    selectedDayTextColor: 'white',
                                    // monthTextColor: 'white',
                                    // indicatorColor: 'white',
                                    textDayFontSize: 12,
                                    textMonthFontSize: 12,
                                    textMonthFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontFamily: 'Rubik-Regular',
                                    textDayHeaderFontSize: 12,
                                    selectedDayBackgroundColor: '#8c52ff',
                                    arrowColor: '#8c52ff',
                                    // textDisabledColor: 'red',
                                    'stylesheet.calendar.header': {
                                        week: {
                                            marginTop: 5,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }
                                    }
                                }}
                            />




                        </View>
                        <View style={{ backgroundColor: '#fafafa', marginLeft: -20, width: wp('100%'), }}>
                            {/* <View style={{ marginLeft: -20, height: 2, width: wp('100%'), backgroundColor: '#e9e9e9' }}></View> */}
                            <Text style={styles.selDateText}>{this.state.dateConString}</Text>

                            <FlatList
                                style={{ width: wp('100%'), height: hp('45%') }}
                                contentContainerStyle={{ paddingBottom: hp('15%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.arrTrainerSlots}
                                keyExtractor={item => item.tas_id}
                                ItemSeparatorComponent={this.FlatListItemSeparator1}
                                ListEmptyComponent={this.ListSlotsEmptyView}
                                renderItem={({ item }) => {
                                    return <TouchableOpacity style={styles.containerMaleStyle} onPress={() => {
                                        this.press(item)
                                    }}>
                                        <View >
                                            <Text style={styles.countryText}>{`${item.schedule_time}`}</Text>
                                        </View>
                                        <View style={styles.mobileImageStyle}>
                                            {item.check
                                                ? (
                                                    <TouchableOpacity onPress={() => {
                                                        this.press(item)
                                                    }}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_rect_check.png')}
                                                            style={styles.mobileImageStyle} />
                                                    </TouchableOpacity>
                                                )
                                                : (
                                                    <TouchableOpacity onPress={() => {
                                                        this.press(item)
                                                    }}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_rect_uncheck.png')}
                                                            style={styles.mobileImageStyle} />
                                                    </TouchableOpacity>
                                                )}
                                        </View>
                                    </TouchableOpacity>
                                }} />

                            <View style={{
                                backgroundColor: '#fafafa', justifyContent: 'center', bottom: 0,
                                width: wp('100%'),
                                position: 'absolute',
                                alignSelf: 'center',
                            }}>
                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                                    <TouchableOpacity onPress={() => {
                                        if (this.state.selected) {
                                            if (this.state.selSlotObj.tas_id) {
                                                // this.setState({ visible: false });
                                                this.setState({ confirmPop: true });
                                            }
                                            else {
                                                this.setState({ isAlert: true, alertMsg: 'Please select time slot to book appointment' });
                                            }

                                        }
                                        else {
                                            this.setState({ isAlert: true, alertMsg: 'Please select appointment data and time' });
                                        }
                                    }}
                                        style={styles.buttonTuch}>
                                        <Text style={styles.buttonText}>BOOK</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                        </View>


                    </DialogContent>
                </Dialog>

                {/* confirm popup */}
                <Dialog
                    onDismiss={() => {
                        this.setState({ confirmPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ confirmPop: false });
                    }}
                    width={0.8}
                    // height={0.9}
                    // dialogStyle={{marginTop: 30,}}
                    visible={this.state.confirmPop}
                    footer={
                        <DialogFooter style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                            alignSelf: 'center',
                        }}>
                            <DialogButton
                                text="Cancel"
                                bordered
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                }}
                                textStyle={{
                                    fontSize: 13, color: '#6d819c', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="Confirm"
                                bordered
                                textStyle={{
                                    fontSize: 13, color: '#8c52ff', fontWeight: '500',
                                    fontFamily: 'Rubik-Medium',
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    letterSpacing: 0.4,
                                }}
                                onPress={() => {
                                    this.setState({ confirmPop: false });
                                    this.bookAppointment();
                                }}
                                key="button-2"
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>
                                <Text style={{
                                    alignSelf: 'center', color: '#282c37', fontSize: 16,
                                    fontWeight: '500',
                                    textAlign: 'center',
                                    marginTop: 15,
                                    fontFamily: 'Rubik-Medium',
                                    letterSpacing: 0.49,
                                }}>Please confirm your appointment</Text>
                                <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_app.png')}
                                        style={styles.profileImage1}
                                    />

                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 13,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{moment(this.state.selected, 'YYYY-MM-DD').format("DD-MM-YYYY")}</Text>
                                    <Text style={{
                                        alignSelf: 'center', color: '#282c37', fontSize: 12,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 5,
                                        fontFamily: 'Rubik-Medium',

                                    }}>{this.state.selSlotObj.schedule_time}</Text>
                                </View>


                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        height: hp('10%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        padding: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textWorkName: {
        width: wp('80%'),
        fontSize: 14,
        fontWeight: '100',
        marginTop: 3,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textMonth: {
        fontSize: 6,
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        backgroundColor: '#8c52ff',
        textAlign: 'center',
        color: '#ffffff',
        borderRadius: 5,
        textAlign: 'center',
        alignSelf: 'center',
        paddingLeft: 5,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 5,
        letterSpacing: 0.23,
    },
    textDate: {
        fontSize: 17,
        width: wp('10%'),
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        textAlign: 'center',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        padding: 8,
        letterSpacing: 0.23,
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textSelTime: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#1E1F20',
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('5%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    countryText: {
        width: wp('80%'),
        alignSelf: 'center',
        fontSize: 11,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.5,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: { width: 15, height: 15, position: 'relative', alignSelf: 'center', marginRight: 10 },
    selDateText: {
        alignSelf: 'flex-start',
        fontSize: 11,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 25,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
    },
    profileImage1: {
        width: 200,
        height: 60,
        marginTop: 10,
        marginBottom: 10,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    viewNoData: {
        padding: 10,
        marginTop: Dimensions.get('window').height / 3,
        height: 40,
    },
    textNoData: {
        textAlign: 'center',
        marginLeft: 20,
        marginRight: 20,
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 22,
        letterSpacing: 0.1,
    },
    viewBlueDot: {
        width: 13,
        height: 13,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 6.5,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        // width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        // padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textMobile: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        // padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#8c52ff',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    linearGradient1: {
        width: '85%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 15,
    },

});

const mapStateToProps = state => {

    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(Trainee_Help);
