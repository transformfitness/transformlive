import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    TextInput,
    ImageBackground,
    KeyboardAvoidingView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals } from '../actions';
import { Loader, CustomDialog } from './common';
let token = '', userId = '', mobileNumber = '', GenderId = '', goalId = '', fitnessLevel = '';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
let isSocialMedia = '';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DeviceInfo from 'react-native-device-info';
import { BASE_URL } from '../actions/types';
import NetInfo from "@react-native-community/netinfo";
import { Dropdown } from 'react-native-material-dropdown';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
class ChallengeRewardRedemption extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            mobile: '',
            name: '',
            address: '',
            isAlert: false,
            alertMsg: '',
            loading: false,

            city: '',
            pincode: '',
            stateId: 0,
            stateName: '',
            stateCode: '',
            isShowAgeNext: true,

        };
    }
    async componentDidMount() {
        allowFunction();
        isSocialMedia = await AsyncStorage.getItem('isSocialMedia');
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.callService();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }


    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.callService();

            }
            else {
                LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

    }


    componentWillUnmount() {
        this.backHandler.remove();
    }


    onBackPressed() {
        Actions.pop();
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.setState({ alertMsg: '' });
        this.setState({ isSuccess: false });
        Actions.traChallenges();
    }


    async callService() {
        this.setState({ loading: true })
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('token', token);
        fetch(
            `${BASE_URL}/master/state`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, stateArray: data.data });
                    this.setState({ stateName: this.state.stateArray[0].value, stateCode: this.state.stateArray[0].code, stateId: this.state.stateArray[0].id })

                    // LogUtils.infoLog1('this.state.resObj.day_arr', this.state.resObj.day_arr);
                    // LogUtils.infoLog1('this.state.resObj.data_arr', this.state.resObj.data_arr);
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: 'Something went wrong please try again later' });
            });
    }




    async saveUserRedemptionDet(body) {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/savechlgwinnerdet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: body,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: false, alertMsg: data.message, isSuccess: true, alertTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
    }


    render() {
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                onKeyboardDidShow={(frames) => this.setState({ isShowAgeNext: false })}
                onKeyboardDidHide={(frames) => this.setState({ isShowAgeNext: true })}
                scrollEnabled={false}>
                <ImageBackground source={require('../res/app_bg.png')} style={{ width: '100%', height: '100%' }}>
                    <Loader loading={this.state.loading} />
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.textHeadTitle}>Reward Redemption</Text>
                        </View>

                        <View style={styles.optionInnerContainer}>
                            <Image
                                source={require('../res/ic_app.png')}
                                style={styles.appImageStyle}
                            />
                        </View>

                        <Text style={styles.textStyle}>
                            Please fill the below details
                        </Text>

                        {this.props.mFrom === 'product'
                            ? (
                                <View style={styles.containerMobileStyle}>
                                    <TextInput
                                        style={styles.textInputStyle}
                                        placeholder="Enter Name"
                                        maxLength={50}
                                        placeholderTextColor="grey"
                                        value={this.state.name}
                                        autoCapitalize='words'
                                        returnKeyType='next'
                                        onSubmitEditing={() => { this.emailTextInput.focus(); }}
                                        onChangeText={text => {
                                            if (text) {
                                                let newText = '';
                                                let numbers = 'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                                for (var i = 0; i < text.length; i++) {
                                                    if (numbers.indexOf(text[i]) > -1) {
                                                        newText = newText + text[i];
                                                    }
                                                    else {
                                                        this.setState({ alertMsg: 'Name does not contain special characters and numbers' });
                                                        this.setState({ isAlert: true });
                                                    }
                                                }

                                                if (newText) {
                                                    this.setState({ name: newText });
                                                }

                                            }
                                            else {
                                                this.setState({ name: '' });
                                            }

                                        }}
                                    />
                                </View>
                            )
                            : (
                                <View>
                                </View>
                            )}



                        <View style={styles.containerMobileStyle}>
                            <TextInput
                                ref={input => { this.emailTextInput = input; }}
                                style={styles.textInputStyle}
                                placeholder="Enter Email"
                                placeholderTextColor="grey"
                                keyboardType="email-address"
                                maxLength={100}
                                value={this.state.email}
                                returnKeyType='next'
                                onChangeText={text => this.setState({ email: text })}
                                onSubmitEditing={() => { this.mobileTextInput.focus(); }}
                            />
                        </View>
                        <View style={styles.containerMobileStyle}>
                            <TextInput
                                ref={input => { this.mobileTextInput = input; }}
                                style={styles.textInputStyle}
                                placeholder="Enter Mobile Number"
                                maxLength={10}
                                placeholderTextColor="grey"
                                keyboardType="numeric"
                                returnKeyType='done'
                                value={this.state.mobile}
                                onSubmitEditing={() => {
                                    if (this.props.mFrom === 'product') {
                                        this.cityTextInput.focus();
                                    }
                                }}
                                onChangeText={text => {
                                    if (text) {
                                        let newText = '';
                                        let numbers = '0123456789';
                                        for (var i = 0; i < text.length; i++) {
                                            if (numbers.indexOf(text[i]) > -1) {
                                                newText = newText + text[i];
                                            }
                                            else {
                                                this.setState({ isAlert: true, alertMsg: 'Please enter numbers only' });
                                            }
                                        }
                                        if (newText) {
                                            this.setState({ mobile: newText });
                                        }
                                    }
                                    else {
                                        this.setState({ mobile: '' });
                                    }
                                }}
                            />
                        </View>

                        {this.props.mFrom === 'product'
                            ? (
                                <View style={styles.containerMobileStyle}>
                                    <TextInput
                                        ref={input => { this.cityTextInput = input; }}
                                        style={styles.textInputStyle}
                                        placeholder="Enter Your City"
                                        placeholderTextColor="grey"
                                        maxLength={100}
                                        value={this.state.city}
                                        returnKeyType='next'
                                        onChangeText={text => this.setState({ city: text })}
                                        onSubmitEditing={() => { this.addressTextInput.focus(); }}
                                    />
                                </View>
                            ) :
                            (
                                < View >
                                </View>
                            )}


                        {this.props.mFrom === 'product'
                            ? (

                                <View
                                    style={styles.spinnersBg}>


                                    <Dropdown
                                        // label='Favorite Fruit'
                                        dropdownOffset={{ top: 15, }}
                                        lineWidth={0}
                                        value={this.state.stateName}
                                        data={this.state.stateArray}
                                        textColor={'#000000'}
                                        itemCount={5}

                                        onChangeText={(value, index) => {
                                            this.setState({ stateName: value, stateId: this.state.stateArray[index].id });
                                            LogUtils.infoLog1('this.state.stateArray[index].id', this.state.stateArray[index].id)
                                            LogUtils.infoLog1('value', value)


                                        }}
                                    />

                                </View>
                            ) :
                            (
                                < View >
                                </View>
                            )}

                        {this.props.mFrom === 'product'
                            ? (
                                <View style={styles.containerMobileStyle}>
                                    <TextInput
                                        ref={input => { this.addressTextInput = input; }}
                                        style={styles.textInputStyle1}
                                        placeholder="Enter Address"
                                        multiline={true}
                                        maxLength={200}
                                        placeholderTextColor="grey"
                                        value={this.state.address}
                                        autoCapitalize='words'
                                        returnKeyType='next'
                                        onSubmitEditing={() => {
                                            if (this.props.mFrom === 'product') {
                                                this.pincodeTextInput.focus();
                                            }
                                        }}
                                        onChangeText={text => {
                                            if (text) {
                                                this.setState({ address: text });
                                            }
                                            else {
                                                this.setState({ address: '' });
                                            }
                                        }}
                                    />
                                </View>
                            )
                            : (
                                <View>
                                </View>
                            )}

                        {this.props.mFrom === 'product'
                            ? (

                                <View style={styles.containerMobileStyle}>
                                    <TextInput
                                        ref={input => { this.pincodeTextInput = input; }}
                                        style={styles.textInputStyle}
                                        placeholder="Enter Pincode"
                                        maxLength={10}
                                        placeholderTextColor="grey"
                                        keyboardType="numeric"
                                        returnKeyType='done'
                                        value={this.state.pincode}

                                        onChangeText={text => {
                                            if (text) {
                                                let newText = '';
                                                let numbers = '0123456789';
                                                for (var i = 0; i < text.length; i++) {
                                                    if (numbers.indexOf(text[i]) > -1) {
                                                        newText = newText + text[i];
                                                    }
                                                    else {
                                                        this.setState({ isAlert: true, alertMsg: 'Please enter numbers only' });
                                                    }
                                                }
                                                if (newText) {
                                                    this.setState({ pincode: newText });
                                                }
                                            }
                                            else {
                                                this.setState({ pincode: '' });
                                            }
                                        }}
                                    />
                                </View>
                            )
                            : (
                                <View>
                                </View>
                            )}

                    </ScrollView>

                    {
                        this.state.isShowAgeNext &&
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                style={styles.buttonTuch}
                                onPress={() => {
                                    this.callSubmit();
                                }}>
                                <Text style={styles.buttonText}>Submit</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    }


                    <CustomDialog
                        visible={this.state.isAlert}
                        title={this.state.alertTitle}
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                    <CustomDialog
                        visible={this.state.isSuccess}
                        title={this.state.alertTitle}
                        desc={this.state.alertMsg}
                        onAccept={this.onSuccess.bind(this)}
                        no=''
                        yes='Ok' />

                </ImageBackground>
            </KeyboardAwareScrollView>
        )
    }

    async callSubmit() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.props.mFrom === 'coupon') {
            if (!this.state.email) {
                this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
            }
            else if (reg.test(this.state.email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
                this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
            }
            else if (!this.mobilevalidate(this.state.mobile)) {
                this.setState({ isAlert: true, alertMsg: 'Please enter a valid mobile number' });
            }
            else {
                let body = '';
                body = JSON.stringify({
                    c_id: this.props.cid,
                    phno: this.state.mobile,
                    email: this.state.email,
                    name: this.state.name,
                    address: this.state.address,
                });
                this.saveUserRedemptionDet(body);
            }
        }
        else if (this.props.mFrom === 'product') {
            if (!this.state.name) {
                this.setState({ isAlert: true, alertMsg: 'Please enter name' });
            }
            else if (!this.state.email) {
                this.setState({ isAlert: true, alertMsg: 'Please enter email ID' });
            }
            else if (reg.test(this.state.email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
                this.setState({ isAlert: true, alertMsg: 'Please enter valid email ID' });
            }
            else if (!this.mobilevalidate(this.state.mobile)) {
                this.setState({ isAlert: true, alertMsg: 'Please enter valid mobile number' });
            }
            else if (!this.state.city) {
                this.setState({ isAlert: true, alertMsg: 'Please enter your city' });
            }
            else if (!this.state.address) {
                this.setState({ isAlert: true, alertMsg: 'Please enter address' });
            }
            else if (!this.state.pincode) {
                this.setState({ isAlert: true, alertMsg: 'Please enter your pincode' });
            }

            else {
                let body = '';
                body = JSON.stringify({
                    c_id: this.props.cid,
                    phno: this.state.mobile,
                    email: this.state.email,
                    name: this.state.name,
                    address: this.state.address,
                    state_id: this.state.stateId,
                    pincode: this.state.pincode,
                    city: this.state.city,
                    weight: this.props.weight,
                    pickup_pincode: this.props.pincode,
                });
                LogUtils.infoLog1("Body", body);
                this.saveUserRedemptionDet(body);
                // this.setState({ isAlert: true, alertMsg: 'call service' });
            }
        }
    }

    mobilevalidate(text) {
        const reg = /^[0]?[6789]\d{9}$/;
        if (reg.test(text) === false) {
            this.setState({
                mobilevalidate: false,
                telephone: text,
            });
            return false;
        } else {
            this.setState({
                mobilevalidate: true,
                telephone: text,
                message: '',
            });
            return true;
        }
    }

}




const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: "flex-start"
    },
    headline: {
        alignSelf: "center",
        fontSize: 18,
        marginTop: 10,
        marginBottom: 30
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
    },

    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },

    backImageStyle: {

        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },

    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: 40,

    },

    container: {
        flex: 1,
        marginTop: 20,
    },

    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    textInputStyle1: {
        height: 100,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        textAlignVertical: 'top',
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },

    text: {
        fontSize: 14,
        color: '#000',
    },

    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },


    textStyle: {
        fontSize: 20,
        marginTop: hp('3%'),
        marginLeft: 40,
        marginRight: 40,
        marginBottom: hp('3%'),
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },

    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },

    spinnersBg: {
        width: wp('89%'),
        marginLeft: 25,
        marginRight: 25,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 10,
        position: 'relative',
        alignContent: 'center',
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 15,
    },
});

const mapStateToProps = state => {
    return {};
};

export default
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(ChallengeRewardRedemption);

