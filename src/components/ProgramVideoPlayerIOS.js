
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, TextInput, KeyboardAvoidingView, BackHandler, Dimensions, ImageBackground, ScrollView, Alert } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Orientation from 'react-native-orientation';
import Video from 'react-native-video';
const theme = {
    title: '#FFF',
    more: '#446984',
    center: '#7B8F99',
    fullscreen: '#446984',
    volume: '#A5957B',
    scrubberThumb: '#234458',
    scrubberBar: '#DBD5C7',
    seconds: '#DBD5C7',
    duration: '#DBD5C7',
    progress: '#446984',
    loading: '#DBD5C7'
}
const initial = Orientation.getInitialOrientation();
let orientationchange = 'PORTRAIT';

import { BASE_URL, SWR } from '../actions/types';
import { CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { AirbnbRating } from 'react-native-ratings';
import LinearGradient from 'react-native-linear-gradient';
import LogUtils from '../utils/LogUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

let url = '';

class ProgramVideoPlayerIOS extends Component {

    constructor(props) {
        super(props);
        // this.player && this.player.presentFullscreenPlayer()
        this.state = {
            currentTime: 0,
            duration: 0,
            isFullScreen: false,
            isLoading: true,
            paused: false,
            screenType: 'contain',

            isAlert: false,
            loading: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            isEndWorkout: false,
            isEndWithOutFeedback: false,
            post: '',
            rating: 3,
            isFeedbackSubmit: false,
            isBackPressed: false,
        };
    }

    async onBackPressed() {
        if (!this.state.isEndWorkout) {
            Orientation.lockToPortrait();
            // Actions.pop();
            this.setState({ isBackPressed: true });

        }

    }



    componentDidMount() {

        Orientation.unlockAllOrientations();

        Orientation.addOrientationListener(this._orientationDidChange);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

    }

    componentWillMount() {
        // The getOrientation method is async. It happens sometimes that
        // you need the orientation at the moment the JS runtime starts running on device.
        // `getInitialOrientation` returns directly because its a constant set at the
        // beginning of the JS runtime.


        if (initial === 'PORTRAIT') {
            // do something
            //console.log("orientation", initial.toString())
        } else {
            // do something else
        }
    }

    _orientationDidChange = (orientation) => {
        initial === orientation;
        orientationchange === orientation;
        if (orientation === 'LANDSCAPE') {
            //console.log(`Changed Device Orientation: ${initial}`);
            // do something with landscape layout
            Orientation.lockToLandscape();
        } else {
            // do something with portrait layout
            //console.log(`Changed Device Orientation: ${orientation}`);
            Orientation.lockToPortrait();
        }
    }

    _onFullscreenPlayerWillDismiss() {
        Actions.pop();
    }

    _onFullscreenPlayerWillPresent() {
        this.player && this.player.presentFullscreenPlayer();
    }

    _orientationRemove = (orientation) => {
        orientation.lockToPortrait();
    }

    componentWillUnmount() {
        this.backHandler.remove();

        Orientation.getOrientation((err, orientation) => {
            //console.log(`Current Device Orientation: ${orientation}`);
        });


        // Remember to remove listener
        Orientation.removeOrientationListener(this._orientationRemove);
    }
    // static navigationOptions = ({ navigation }) => {
    //   const { state } = navigation
    //   // Setup the header and tabBarVisible status
    //   const header = state.params && (state.params.fullscreen ? undefined : null)
    //   const tabBarVisible = state.params ? state.params.fullscreen : true
    //   return {
    //     // For stack navigators, you can hide the header bar like so
    //     header,
    //     // For the tab navigators, you can hide the tab bar like so
    //     tabBarVisible,
    //   }
    // }

    onFullScreen(status) {
        // Set the params to pass in fullscreen status to navigationOptions
        // this.props.navigation.setParams({
        //   fullscreen: !status
        // })
        this.setState({ isFullScreen: true });
    }

    onMorePress() {
        Actions.pop();
    }
    handleFullScreenToPortrait = () => {
        this.player.presentFullscreenPlayer();
    };

    handleFullScreenDismiss = () => {
        // this.player.dismissFullscreenPlayer();
        this.setState({ isBackPressed: true, paused: true });
        // this.saveEndWorkOut();
    };

    onEndWorkout() {
        this.setState({ isBackPressed: false, paused: true });
        this.saveEndWorkOut();
    }

    async saveEndWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/endworkout`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (this.props.planObj.fb_done === 0)
                        this.setState({ loading: false, isEndWorkout: true });
                    else
                        this.setState({ loading: false, isEndWithOutFeedback: true, isAlert: true, alertMsg: data.message, });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });

                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, isEndWorkout: true });
            });
    }

    async saveFeebackWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/savedayfdback`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                    rating: this.state.rating,
                    comments: this.state.post,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isFeedbackSubmit: true })
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });

                    } else {
                        this.setState({ loading: false, isFeedBackError: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isFeedBackError: true, alertMsg: SWR });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertMsg: '' });

        if (this.state.isEndWithOutFeedback) {
            this.setState({ isEndWithOutFeedback: false });

            if (this.props.from === 'home')
                // Actions.thome();
                Actions.popTo('traineeHome');
            else
                // Actions.traineeWorkoutsHome();
                Actions.traineePlanNew({ pId: this.props.p_id });
        }

    }

    async onFeedbackSubmit() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isFeedbackSubmit: false, alertMsg: '' });

        if (this.props.from === 'home')
            Actions.popTo('traineeHome');
        else
            Actions.traineePlanNew({ pId: this.props.p_id });
    }

    renderProfileImg() {
        if (this.props.planObj.tr_img) {
            return (
                <Image
                    source={{ uri: this.props.planObj.tr_img }}
                    style={styles.profileImage}
                />
            );
        }
        else {
            return (
                <Image
                    source={require('../res/ic_profile_gray.png')}
                    style={styles.profileImage}
                />
            );
        }
    }

    ratingCompleted(rating) {
        this.setState({ rating: rating })
        switch (rating) {
            case 1:
                this.setState({ ratingText: 'Bad experience !' })
                break;
            case 2:
                this.setState({ ratingText: 'Not that great !' })
                break;
            case 3:
                this.setState({ ratingText: 'It was good !' })
                break;
            case 4:
                this.setState({ ratingText: 'Had a great experience !' })
                break;
            case 5:
                this.setState({ ratingText: 'Absolutely superb !' })
                break;
            default:
                this.setState({ ratingText: 'Give rating !' })
                break
        }
    }

    onEnd = () => {
        this.saveEndWorkOut();
        if (this.props.label) {
            LogUtils.firebaseEventLog('video', {
                p_id: this.props.planObj.p_id,
                p_category: 'Programs',
                p_name: this.props.label,
            });
        }
    }



    render() {

        if (!this.state.isEndWorkout) {

            return (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Loader loading={this.state.loading} />
                    <View style={styles.container}>

                        <Video
                            ref={ref => this.player = ref}
                            source={{ uri: this.props.planObj.video_url }}
                            style={styles.video}
                            paused={this.state.paused}
                            onEnd={this.onEnd}
                            controls={false}
                            resizeMode="contain"
                            fullscreen={true}
                            onLoadStart={this.handleFullScreenToPortrait}
                            onFullscreenPlayerWillDismiss={() =>
                                this.handleFullScreenDismiss()
                            }
                        />


                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.state.isBackPressed}
                        title='Alert'
                        desc={'Do you want End Workout ?'}
                        onAccept={this.onEndWorkout.bind(this)}
                        onDecline={() => {
                            this.setState({ isBackPressed: false, paused: false })
                            this.player.presentFullscreenPlayer();
                        }
                        }
                        no='No'
                        yes='Yes' />


                </View >
            )
        } else {
            return (
                <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -70 })} style={styles.mainContainer} behavior="padding" enabled>
                    <ImageBackground source={require('../res/post_back.png')} style={styles.mainImageContainer}>
                        <View style={styles.postContents}>
                            <Text style={styles.createPostStyle}>RATE YOUR WORKOUT</Text>

                            {this.renderProfileImg()}

                            <Text style={styles.feedbackNameStyle}>{this.props.planObj.tr_name}</Text>
                            <Text style={styles.feedbackSubStyle}>{this.props.planObj.name.replace('-', ' ')} - {this.props.planObj.title}</Text>

                            <AirbnbRating
                                // showRating
                                type='star'
                                ratingCount={5}
                                reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                                imageSize={30}
                                startingValue={this.state.rating}
                                readonly={false}
                                isDisabled={false}
                                reviewSize={15}
                                onFinishRating={this.ratingCompleted.bind(this)}
                                style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                            />

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearSubmitGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.saveFeebackWorkOut();
                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>SUBMIT</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            <View style={styles.inputRow}>
                                <TextInput
                                    style={styles.textInputStyle}
                                    placeholder="Type your review..."
                                    placeholderTextColor="#9c9eb9"
                                    value={this.state.post}
                                    numberOfLines={2}
                                    multiline={true}
                                    onChangeText={text => this.setState({ post: text })}
                                />
                            </View>
                        </View>

                        <CustomDialog
                            visible={this.state.isFeedbackSubmit}
                            title='Success'
                            desc={'Feedback submitted successfully.'}
                            onAccept={this.onFeedbackSubmit.bind(this)}
                            no=''
                            yes='Ok' />

                    </ImageBackground>
                </KeyboardAvoidingView>
            );
        }
    }
}
const styles = StyleSheet.create({
    video: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width * (9 / 16),
        backgroundColor: 'black',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#000000'
    },
    mainImageContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    postContents: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 23,
        marginTop: 10,
    },
    createPostStyle: {
        height: 30,
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    profileImage: {
        width: 150,
        height: 150,
        aspectRatio: 1,
        alignSelf: 'center',
        backgroundColor: "#D8D8D8",
        marginTop: hp('2%'),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 150 / 2,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    feedbackNameStyle: {
        height: 30,
        alignSelf: 'center',
        marginTop: hp('2%'),
        color: '#5a5a5a',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    feedbackSubStyle: {
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        fontWeight: '500',
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "flex-start",
        height: '18%',
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        borderRadius: 10,
        backgroundColor: '#f3f4f6',
    },
    linearSubmitGradient: {
        width: '90%',
        height: 50,
        bottom: 10,
        borderRadius: 15,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle: {
        width: wp('75%'),
        color: '#2d3142',
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.23,
    },

});
export default ProgramVideoPlayerIOS;