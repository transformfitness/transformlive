import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { getTraineeGoals } from '../actions';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

class Appointment_Success extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        };
    }

    async componentDidMount() {
        allowFunction();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.appointments();
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <View style={styles.mainView}>
                    {/* title, weight  and chart details */}
                    <TouchableOpacity
                        style={{ width: 40, height: 40, }}
                        onPress={() => this.onBackPressed()}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>
                    <View style={styles.viewTitleCal}>

                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_tick.png')}
                            style={styles.tickStyle}
                        />
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/img_appoint_suc.png')}
                            style={styles.appsucimg}
                        />


                        <Text style={styles.textCreditCard}>{this.props.infoText}</Text>

                    </View>

                </View>


                <View style={styles.bottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {

                                Actions.appointments({ plantype: this.props.plantype, premiumuser : this.props.premiumuser })
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Go to Appointments</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        height: hp('70%'),
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 30,
    },
    textCreditCard: {
        fontSize: 18,
        marginTop: 30,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        marginLeft: 30,
        marginRight: 30,
        textAlign: 'center',
        lineHeight: 25,
        letterSpacing: 0.1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    tickStyle: {
        width: 70,
        height: 70,
        alignSelf: 'center',
    },
    appsucimg: {
        width: 300,
        height: 215,
        marginTop: 20,
        alignSelf: 'center',
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 20,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },

});

const mapStateToProps = state => {
    const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals, trainerCodes };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(Appointment_Success),
);

// export default ProfileCreation;