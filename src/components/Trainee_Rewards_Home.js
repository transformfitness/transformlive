import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
    ScrollView,
    Dimensions,
    FlatList,
} from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, CustomDialog, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { BASE_URL, SWR } from '../actions/types';
import LinearGradient from 'react-native-linear-gradient';
const screenHeight = Math.round(Dimensions.get('window').height);
let screenWidth = Math.round(Dimensions.get('window').width);
import { RFValue } from "react-native-responsive-fontsize";
import DeviceInfo from 'react-native-device-info';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import LogUtils from '../utils/LogUtils.js';
import NetInfo from "@react-native-community/netinfo";
import { allowFunction } from '../utils/ScreenshotUtils.js';
import RBSheet from "react-native-raw-bottom-sheet";


function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
import Dialog, {
    DialogContent,
    SlideAnimation
} from 'react-native-popup-dialog';
let token = '', userId = '', remainingTime = 0, shopping_url = '';
import CountDown from 'react-native-countdown-component';


const initialLayout = { width: Dimensions.get('window').width };

const progressCustomStyles = {
    backgroundColor: '#8c52ff',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    marginTop: 10,
    marginBottom: 20,
};
const barWidth = Dimensions.get('screen').width - 135;
const barWidth1 = Dimensions.get('screen').width - 70;

let rewardShown = false;
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
let rewardWidth = wp('77%');
let rewardHeight = (rewardWidth / 2) + 15;

class Trainee_Rewards_home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            setIndex: 0,
            routes: [
                { key: 'first', title: 'Home' },
                { key: 'second', title: 'Tasks' },
                { key: 'third', title: 'Rewards' },

            ],
            tab: 1,
            isSuccess: false,
            sucMsg: '',
            isAlert: false,
            alertMsg: '',
            alertTitle: '',
            loading: false,
            isTabClicked: 1,
            innerTabClicked: 1,
            isPopupVisible: false,
            isRewardComplete: false,
            imageUrl: '',
            rewardsData: {},
            taskType: '',
            taskName: '',
            title: '',
            description: '',
            tempArray: [],
            isTaskInfo: false,
            progressValue: 100,
            isInternet: false,
            untiltime: 0,
            responseTime: 0,
            taskSeconds: 0,
        };
    }
    async componentDidMount() {
        try {
            allowFunction();
            token = await AsyncStorage.getItem('token');
            userId = await AsyncStorage.getItem('userId');
            shopping_url = await AsyncStorage.getItem('shopping_url');
            screenWidth = screenWidth * 0.75;
            // LogUtils.infoLog1("screenWidth", screenWidth);
            // LogUtils.infoLog1("rewardWidth", rewardWidth);

            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });

            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.getRewardsData();
                }
                else {
                    this.setState({ isInternet: true });
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.getRewardsData();
            }
            else {
                this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: 'Please enable your internet connection and try again' });
            }
        });
    }


    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isPopupVisible) {
            this.setState({ isPopupVisible: false })
        }
        else if (this.state.isRewardComplete) {
            this.setState({ isRewardComplete: false });
        }
        else if (this.props.from === 'feed') {
            Actions.pop();
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    async getRewardsData() {
        try {
            this.setState({ loading: true })
            fetch(
                `${BASE_URL}/trainee/getapptasks`,
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1("data", data.data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message, rewardsData: data.data, untiltime: data.data.seconds, responseTime: Date.now(), taskSeconds: data.data.day_seconds });
                        this.renderCompletedTaskPopup()

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
        this.setState({ fileUri: "" });

        if (this.state.isSuccess) {
            this.setState({ loading: true });
            this.getUserMyChallengesVideos();
            this.setState({ isSuccess: false });
        }
    }

    async saveTaskCompleteFlag(up_id) {
        try {

            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog(JSON.stringify({
                up_id: up_id,
            }));
            fetch(
                `${BASE_URL}/trainee/updateactivityseen`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        up_id: up_id,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('version statusCode', statusCode);
                    LogUtils.infoLog1('version data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                    } else {
                    }
                })
                .catch(function (error) {
                });
        } catch (error) {
            console.log(error);
        }
    }

    renderCompletedTaskPopup() {
        try {
            rewardShown = false;
            // const interval = setInterval(() => {
            if (this.state.rewardsData.task.length > 0) {
                this.state.rewardsData.task.map((item) => {
                    item.data.map((item1, index) => {
                        if ((((item1.achived / item1.target_limit) * 100) >= 100) && item1.is_claimed === 0 && item1.is_popupshown === 0) {


                            if (rewardShown) {
                                LogUtils.infoLog1("rewardShown ", rewardShown);
                                const interval = setInterval(() => {
                                    this.setState({ isRewardComplete: true, taskType: item1.task_type, taskName: item1.title });
                                    this.saveTaskCompleteFlag(item1.up_id);

                                    clearInterval(interval);

                                }, 2000);

                                rewardShown = false;
                            } else {
                                LogUtils.infoLog1("rewardShown ", rewardShown);
                                const interval = setInterval(() => {
                                    this.setState({ isRewardComplete: true, taskType: item1.task_type, taskName: item1.title });
                                    this.saveTaskCompleteFlag(item1.up_id);
                                    clearInterval(interval);

                                }, 1000);
                                rewardShown = true;

                            }
                        }
                    })
                })
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderRewardsAllData() {
        if (!isEmpty(this.state.rewardsData)) {

        }
    }

    renderTabBar() {
        if (this.state.loading === false) {
            if (this.state.isTabClicked === 1) {

                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>Home</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })
                        } style={styles.headerInner}>
                            <Text style={styles.textPlan}>Tasks</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>Rewards</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>

                    </ImageBackground>
                );
            } else if (this.state.isTabClicked === 2) {
                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>Home</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>Tasks</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>Rewards</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                );

            } else if (this.state.isTabClicked === 3) {
                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>Home</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                            <Text style={styles.textPlan}>Tasks</Text>
                            <View
                                style={styles.headerSel} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ isTabClicked: 3 })} style={styles.headerInner}>
                            <Text style={styles.textIntro}>Rewards</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.headerSel} />
                        </TouchableOpacity>
                    </ImageBackground>
                );

            }
        }
    }

    selectTab1() {
        const interval = setInterval(() => {
            this.setState({ innerTabClicked: 1 })
            remainingTime = parseInt(Date.now() / 1000) - parseInt(this.state.responseTime / 1000);
            this.setState({ taskSeconds: this.state.rewardsData.day_seconds - remainingTime });
            clearInterval(interval);

        }, 100);
    }

    selectTab2() {
        const interval = setInterval(() => {
            this.setState({ innerTabClicked: 2 })
            remainingTime = parseInt(Date.now() / 1000) - parseInt(this.state.responseTime / 1000);
            this.setState({ taskSeconds: this.state.rewardsData.week_seconds - remainingTime });
            clearInterval(interval);

        }, 100);
    }

    selectTab3() {
        const interval = setInterval(() => {
            this.setState({ innerTabClicked: 3 })
            remainingTime = parseInt(Date.now() / 1000) - parseInt(this.state.responseTime / 1000);
            this.setState({ taskSeconds: this.state.rewardsData.seconds - remainingTime });
            clearInterval(interval);

        }, 100);
    }

    renderInnerTabBar() {
        if (this.state.loading === false) {
            if (this.state.innerTabClicked === 1) {

                return (
                    <View style={{ backgroundColor: 'transparent', width: '100%', alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 10, marginBottom: 5, }}>
                        <TouchableOpacity onPress={() => this.selectTab1()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 30,
                                paddingRight: 30,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#8c52ff',
                                color: '#ffffff',
                                // route.key === props.navigationState.routes[props.navigationState.index].key ?
                                //     '#ffffff' : '#6D819C'
                            }}>
                                Daily
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab2()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 25,
                                paddingRight: 25,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',

                            }}>
                                Weekly
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab3()}
                            style={{
                                width: wp('30%'),
                                justifyContent: 'center',
                                alignItems: 'center',
                                flexDirection: 'column',
                                alignSelf: 'center',
                            }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 20,
                                paddingRight: 20,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',

                            }}>
                                Monthly
                            </Text>
                        </TouchableOpacity>

                    </View>
                );
            } else if (this.state.innerTabClicked === 2) {
                return (
                    <View style={{ backgroundColor: 'transparent', width: '98%', alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 10, marginBottom: 5, }}>
                        <TouchableOpacity onPress={() => this.selectTab1()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 30,
                                paddingRight: 30,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',
                                // route.key === props.navigationState.routes[props.navigationState.index].key ?
                                //     '#ffffff' : '#6D819C'
                            }}>
                                Daily
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab2()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 25,
                                paddingRight: 25,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#8c52ff',
                                color: '#ffffff',

                            }}>
                                Weekly
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab3()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 20,
                                paddingRight: 20,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',

                            }}>
                                Monthly
                            </Text>
                        </TouchableOpacity>

                    </View>
                );

            } else if (this.state.innerTabClicked === 3) {
                return (
                    <View style={{ backgroundColor: 'transparent', width: '98%', alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 10, marginBottom: 5, }}>
                        <TouchableOpacity onPress={() => this.selectTab1()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 30,
                                paddingRight: 30,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',
                                // route.key === props.navigationState.routes[props.navigationState.index].key ?
                                //     '#ffffff' : '#6D819C'
                            }}>
                                Daily
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab2()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 25,
                                paddingRight: 25,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#ffffff',
                                color: '#6D819C',

                            }}>
                                Weekly
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.selectTab3()} style={{
                            width: wp('30%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            alignSelf: 'center',
                        }}>
                            <Text style={{
                                // width: '100%',
                                fontFamily: 'Rubik-Regular',
                                alignSelf: "center",
                                fontSize: 12,
                                fontWeight: '500',
                                lineHeight: 18,
                                paddingTop: 7,
                                paddingBottom: 7,
                                paddingLeft: 20,
                                paddingRight: 20,
                                borderRadius: 15,
                                overflow: 'hidden',
                                backgroundColor: '#8c52ff',
                                color: '#ffffff',

                            }}>
                                Monthly
                            </Text>
                        </TouchableOpacity>

                    </View>
                );

            }
        }


    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async saveClaimPoints(body) {
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('Body', body);
        fetch(
            `${BASE_URL}/trainee/savepointsclaim`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: body,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    // this.setState({ loading: false, isAlert: true, alertMsg: data.message, isSuccess: true, alertTitle: data.title });
                    this.getRewardsData();
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
    }

    renderTabData() {
        if (this.state.loading === false) {
            if (this.state.isTabClicked === 1) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                        }}>
                            {this.renderHomeTab()}
                        </View>
                    </View>
                );
            }
            else if (this.state.isTabClicked === 2) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            marginTop: -12,
                        }}>
                            {/* {this.renderTasks()} */}

                            <TabView
                                navigationState={this.state}

                                renderScene={({ route }) => {
                                    switch (route.key) {
                                        case 'first':
                                            return this.TaskFirstRoute();
                                        case 'second':
                                            return this.TaskSecondRoute();
                                        case 'third':
                                            return this.TaskThirdRoute();
                                        default:
                                            return null;
                                    }
                                }}

                                onIndexChange={index => this.setState({ index })}
                                initialLayout={initialLayout}
                                style={{ flex: 1 }}
                                renderTabBar={this._renderTabBar}
                            />
                        </View>
                    </View>
                );
            }
            else if (this.state.isTabClicked === 3) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                        }}>
                            {this.renderRewardsTab()}

                        </View>
                    </View>
                );
            }
        }

    }

    renderInnerTabData() {
        if (this.state.loading === false) {
            if (this.state.innerTabClicked === 1) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            marginTop: 5,
                        }}>
                            {this.renderTasks('Dialy')}
                        </View>
                    </View>
                );
            }
            else if (this.state.innerTabClicked === 2) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            marginTop: 5,
                        }}>
                            {this.renderTasks('Weekly')}
                        </View>
                    </View>
                );
            }
            else if (this.state.innerTabClicked === 3) {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                            marginTop: 5,
                        }}>
                            {this.renderTasks('Monthly')}
                        </View>
                    </View>
                );
            }
        }

    }

    FirstRoute = () => (
        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {this.renderHomeTab()}
            </View>
        </View>
    );

    SecondRoute = () => (
        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {/* {this.renderTasks()} */}
                <View style={styles.mainView}>
                    <View style={{
                        paddingBottom: 5, flex: 1,
                        flexDirection: 'column',
                    }}>
                        {/* {this.renderTasks()} */}
                        {this.renderInnerTabBar()}

                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                            <Text style={{
                                fontSize: 14,
                                fontWeight: '500',
                                letterSpacing: 0.2,
                                fontFamily: 'Rubik-Regular',
                                color: '#6d819c',
                                lineHeight: 18,
                                marginBottom: 15,
                                alignSelf: 'center'
                            }}>Refreshes in : </Text>
                            {this.renderTaskCountDown()}
                        </View>

                        {this.renderInnerTabData()}

                    </View>
                </View>
            </View>
        </View>

    );

    ThirdRoute = () => (

        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {this.renderRewardsTab()}
            </View>
        </View>
    );

    TaskFirstRoute = () => (
        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {this.renderTasks('Dialy')}
            </View>
        </View>
    );

    TaskSecondRoute = () => (
        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {this.renderTasks('Weekly')}
            </View>
        </View>
    );

    TaskThirdRoute = () => (
        <View style={styles.mainView}>
            <View style={{
                paddingBottom: 5, flex: 1,
                flexDirection: 'column',
            }}>
                {this.renderTasks('Monthly')}
            </View>
        </View>
    );

    renderTasksNew(flag) {

        LogUtils.infoLog1('flag', flag);
        if (Array.isArray(this.state.rewardsData.task) && this.state.rewardsData.task.length) {

            this.state.rewardsData.task.map((item1) => {

                if (item1.title === flag) {

                    LogUtils.infoLog1('item.data', item1.data);

                    return (
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('5%'), marginLeft: 10, paddingRight: hp('3%'), backgroundColor: '#000000' }}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.rewardsData.other_reward}
                            keyExtractor={(item, index) => index + "other_reward"}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <View style={styles.aroundListStyle} >

                                    <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                                        <Text style={styles.textWorkName}>{`${item.title}`}</Text>
                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                            <Text style={styles.buttonText}>{`+${item.points} Points`}</Text>
                                        </LinearGradient>

                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20, }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_refer.png')}
                                            style={styles.imgGoal}
                                        />
                                        <Text style={styles.textStatusTit}>{`${item.sub_title}`}
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_info_blueround.png')}
                                                style={{
                                                    width: 20,
                                                    height: 20,
                                                    marginLeft: 10,
                                                    alignSelf: 'flex-end',
                                                    backgroundColor: '#000000'
                                                }}
                                            />
                                        </Text>



                                    </View>

                                    {this.renderTaskCompleted(item)}

                                </View>

                            }} />
                    );
                }
            })
        }


        if (Array.isArray(this.state.tempArray) && this.state.tempArray.length) {
            return (
                <FlatList
                    contentContainerStyle={{ paddingBottom: hp('5%'), marginLeft: 10, paddingRight: hp('3%') }}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={this.state.tempArray}
                    keyExtractor={item => "tempArray" + item.at_id}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        <View style={styles.aroundListStyle} >

                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                                <Text style={styles.textWorkName}>{`${item.title}`}</Text>
                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                    <Text style={styles.buttonText}>{`+${item.points} Points`}</Text>
                                </LinearGradient>

                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20, }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_refer.png')}
                                    style={styles.imgGoal}
                                />
                                <Text style={styles.textStatusTit}>{`${item.sub_title}`}</Text>

                            </View>

                            {this.renderTaskCompleted(item)}

                        </View>

                    }} />
            );

        }

    }


    renderNextRewards() {

        if (Array.isArray(this.state.rewardsData.reward_points) && this.state.rewardsData.reward_points.length) {
            let nextReward = 0;
            let nextReward1, nextReward2, nextReward3 = 0;
            // let currentPnt = 9000;

            if (this.state.rewardsData.current_points >= this.state.rewardsData.reward_points[2].points) {
                nextReward = ((this.state.rewardsData.current_points) / (this.state.rewardsData.reward_points[2].points) * 100);
            } else if (this.state.rewardsData.current_points >= this.state.rewardsData.reward_points[1].points) {
                nextReward = ((this.state.rewardsData.current_points) / (this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);
            } else if (this.state.rewardsData.current_points >= this.state.rewardsData.reward_points[0].points) {
                nextReward = ((this.state.rewardsData.current_points) / (this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);
            }
            else {
                nextReward = ((this.state.rewardsData.current_points) / (this.state.rewardsData.reward_points[0].points + this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);
            }

            nextReward1 = ((this.state.rewardsData.reward_points[0].points) / (this.state.rewardsData.reward_points[0].points + this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);
            nextReward2 = ((this.state.rewardsData.reward_points[1].points) / (this.state.rewardsData.reward_points[0].points + this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);
            nextReward3 = ((this.state.rewardsData.reward_points[2].points) / (this.state.rewardsData.reward_points[0].points + this.state.rewardsData.reward_points[1].points + this.state.rewardsData.reward_points[2].points) * 100);

            if (nextReward > 100) {
                nextReward = 100;
            } else if (nextReward < 1 && nextReward > 0) {
                nextReward = 1;
            }
            return (

                <View style={{ flexDirection: 'column', alignItems: 'center', width: '100%', marginTop: 10, marginBottom: 20, }}>
                    <ProgressBarAnimated
                        {...progressCustomStyles}
                        width={barWidth1}
                        height={15}
                        style={styles.activityIndicatorWrapper}
                        value={nextReward}
                        onComplete={() => {
                            // Alert.alert('Hey!', 'onComplete event fired!');
                        }}
                    />
                    <View style={{ position: 'absolute', flexDirection: 'row', width: '100%', right: 0, marginTop: -5 }}>

                        <View style={{ width: nextReward1 + '%', flexDirection: 'column', }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: 25,
                                    height: 25,
                                    alignSelf: 'flex-end',
                                    resizeMode: 'contain',
                                    tintColor: 'orange',
                                }}
                                source={require('../res/ic-circle.png')}
                            />
                            <Text style={{
                                width: nextReward1 + 20,
                                fontSize: 12,
                                fontWeight: '400',
                                letterSpacing: 0.2,
                                fontFamily: 'Rubik-Regular',
                                color: '#6d819c',
                                lineHeight: 18,
                                textAlign: 'right',
                                alignSelf: 'flex-end',
                            }}>{`${this.state.rewardsData.reward_points[0].points}`}</Text>
                        </View>

                        <View style={{ width: nextReward2 + '%', }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: 25,
                                    height: 25,
                                    alignSelf: 'flex-end',
                                    resizeMode: 'contain',
                                }}
                                source={require('../res/ic-circle.png')}
                            />
                            <Text style={{
                                fontSize: 12,
                                fontWeight: '400',
                                letterSpacing: 0.2,
                                fontFamily: 'Rubik-Regular',
                                color: '#6d819c',
                                lineHeight: 18,
                                alignSelf: 'flex-end',
                            }}>{`${this.state.rewardsData.reward_points[1].points}`}</Text>
                        </View>

                        <View style={{ width: nextReward3 + '%', }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: 25,
                                    height: 25,
                                    alignSelf: 'flex-end',
                                    resizeMode: 'contain',
                                }}
                                source={require('../res/ic-circle.png')}
                            />
                            <Text style={{
                                fontSize: 12,
                                fontWeight: '400',
                                letterSpacing: 0.2,
                                fontFamily: 'Rubik-Regular',
                                color: '#6d819c',
                                lineHeight: 18,
                                alignSelf: 'flex-end',
                            }}>{`${this.state.rewardsData.reward_points[2].points}`}</Text>
                        </View>
                    </View>

                </View>

            );
        }
    }

    renderTopRewards() {
        if (Array.isArray(this.state.rewardsData.top_reward) && this.state.rewardsData.top_reward.length) {
            return (
                <View>
                    <Text style={styles.texttitle}>Top Rewards</Text>

                    <View style={{
                        marginBottom: 15,
                        shadowColor: '#4075cd',
                        elevation: 2, width: wp('94%'), alignSelf: 'center', justifyContent: 'center',
                        height: 185,
                    }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            style={{
                                width: wp('94%'),
                                height: undefined,
                                alignSelf: 'center',
                                // resizeMode: 'cover',
                                borderRadius: 5,
                                aspectRatio: 2 / 1,
                            }}
                            source={{ uri: this.state.rewardsData.top_reward[0].image }}
                        />
                    </View>
                </View>
            );
        }

    }

    renderLikedRewards() {
        if (Array.isArray(this.state.rewardsData.liked_reward) && this.state.rewardsData.liked_reward.length) {
            return (
                <View>
                    <Text style={styles.texttitle}>Rewards you would like</Text>

                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <View style={{
                            shadowColor: '#4075cd',
                            width: wp('45%'),
                            shadowRadius: 1,
                            elevation: 2, borderRadius: 8,
                            backgroundColor: '#ffffff', marginRight: 5,
                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: wp('45%'),
                                    height: 340,
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    resizeMode: 'cover',
                                    borderRadius: 8,
                                }}
                                source={{ uri: this.state.rewardsData.liked_reward[0].image }}
                            />


                        </View>


                        <View style={{
                            shadowColor: '#4075cd',
                            width: wp('45%'),
                            height: 340,
                            shadowRadius: 1,
                            elevation: 2, borderRadius: 8,
                            backgroundColor: '#ffffff', marginLeft: 5,
                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: wp('45%'),
                                    height: 340,
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    resizeMode: 'cover',
                                    borderRadius: 8,
                                }}
                                source={{ uri: this.state.rewardsData.liked_reward[1].image }}
                            />


                        </View>
                    </View>
                </View>
            );
        }

    }

    renderOtherRewards() {
        if (Array.isArray(this.state.rewardsData.other_reward) && this.state.rewardsData.other_reward.length) {
            return (
                <View>
                    <Text style={styles.texttitle}>Other Rewards</Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('5%'), marginLeft: 10, paddingRight: hp('3%'), }}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={this.state.rewardsData.other_reward}
                        keyExtractor={(item, index) => "other_reward" + index}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <View style={{
                                width: wp('30.5%'),
                                height: undefined,
                                marginBottom: 15, shadowColor: '#4075cd',
                                shadowRadius: 1,
                                elevation: 2, borderRadius: 5,
                                backgroundColor: '#ffffff',
                                marginRight: 8,
                                aspectRatio: 1 / 1,
                            }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: item.image }}
                                    style={{
                                        width: wp('30.5%'),
                                        height: undefined,
                                        borderRadius: 5,
                                        // resizeMode: 'cover',
                                        padding: 3,
                                        aspectRatio: 1 / 1,
                                    }}
                                />

                            </View>

                        }} />
                </View>
            );
        }

    }
    renderCountDown() {
        if (this.state.rewardsData.seconds) {
            if (this.state.rewardsData.seconds > 86400) {
                return <CountDown
                    style={{ alignSelf: 'center', }}
                    until={this.state.rewardsData.seconds}
                    size={14}
                    // onChange={(time) => { this.setState({ untiltime: time }) }}
                    onFinish={() => { this.getRewardsData() }}
                    digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                    digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                    timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                    separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                    timeToShow={['D', 'H', 'M', 'S']}
                    timeLabels={{ d: 'days', h: 'hours', m: 'min', s: 'sec' }}
                // showSeparator
                />

            } else {
                return <CountDown
                    style={{ alignSelf: 'center', }}
                    until={this.state.rewardsData.seconds}
                    size={14}
                    onFinish={() => { this.getRewardsData() }}
                    digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                    digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                    timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                    separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                    timeToShow={['H', 'M', 'S']}
                    timeLabels={{ d: 'days', h: 'hours', m: 'min', s: 'sec' }}
                // showSeparator
                />
            }

        }
    }


    renderTaskCountDown() {
        let seconds = 0;
        if (this.state.innerTabClicked === 1) {
            seconds = this.state.rewardsData.day_seconds - remainingTime;
            return <CountDown
                style={{ alignSelf: 'center', }}
                until={seconds}
                size={14}
                onFinish={() => { this.getRewardsData() }}
                digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                timeToShow={['H', 'M', 'S']}
                timeLabels={{ d: 'days', h: 'hours', m: 'min', s: 'sec' }}
            // showSeparator
            />

        }
        else if (this.state.innerTabClicked === 2) {
            seconds = this.state.rewardsData.week_seconds - remainingTime;
            return <CountDown
                style={{ alignSelf: 'center', }}
                until={seconds}
                size={14}
                // onChange={(time) => { this.setState({ untiltime: time }) }}
                onFinish={() => { this.getRewardsData() }}
                digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                timeToShow={['D', 'H', 'M', 'S']}
                timeLabels={{ d: 'days', h: 'hours', m: 'min', s: 'sec' }}
            // showSeparator
            />

        }
        else if (this.state.innerTabClicked === 3) {
            seconds = this.state.rewardsData.seconds - remainingTime;
            return <CountDown
                style={{ alignSelf: 'center', }}
                until={seconds}
                size={14}
                // onChange={(time) => { this.setState({ untiltime: time }) }}
                onFinish={() => { this.getRewardsData() }}
                digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                timeToShow={['D', 'H', 'M', 'S']}
                timeLabels={{ d: 'days', h: 'hours', m: 'min', s: 'sec' }}
            // showSeparator
            />
        }

        LogUtils.infoLog1("seconds", seconds);


    }
    renderHomeTab() {
        if (this.state.rewardsData) {
            return (
                <ScrollView style={{ flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                        <Text style={{
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginBottom: 15,
                            alignSelf: 'center'
                        }}>Ends in : </Text>
                        {this.renderCountDown()}
                    </View>

                    <View style={styles.aroundListStyle} >

                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                            <Text style={styles.textWorkName}>{`${this.state.rewardsData.title}`}</Text>
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <Text style={styles.buttonText}>{`${this.state.rewardsData.current_points} Points`}</Text>
                            </LinearGradient>

                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20, }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_refer.png')}
                                style={styles.imgGoal}
                            />
                            <Text style={styles.textStatusTit}>{`${this.state.rewardsData.description}`}</Text>

                        </View>
                        <Text style={{
                            fontSize: 13,
                            fontWeight: '300',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 20,
                            alignSelf: 'center'
                        }}>Next Reward</Text>

                        {this.renderNextRewards()}


                    </View>
                    {this.renderTopRewards()}
                    {this.renderLikedRewards()}
                    {this.renderOtherRewards()}
                </ScrollView>

            );
        }

    }
    renderClaim(item) {

        if (item.is_points_reached === 1) {

            if (item.is_claimed === 1) {
                return (
                    <View >
                        <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 5, }}
                        >
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_green_tick.png')}
                                style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                        </View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 0, width: '50%' }}>
                            <Text style={styles.textMainTrainer}>{`${item.points} Points`}</Text>
                        </LinearGradient>
                    </View>
                );
            }
            else {
                return (
                    <View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 0, width: '50%' }}>
                            <Text style={styles.textTrainerPrograms}>Claim</Text>
                        </LinearGradient>
                        <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 1, }}
                        >
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_unlock.png')}
                                style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                        </View>
                    </View>
                );
            }
        } else {
            return (
                <View>
                    <View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 0, width: '50%' }}>
                            <Text style={styles.textMainTrainer}>{`${item.points} Points`}</Text>
                        </LinearGradient>
                        <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 1, }}
                        >
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_lock.png')}
                                style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                        </View>
                    </View>
                </View>
            );
        }
    }

    renderLock(item) {
        if (item.can_redeem === 1) {
            return (
                <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 10, }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_unlock.png')}
                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                </View>
            );
        } else if (item.can_redeem === 0) {
            return (
                <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 10, }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_lock.png')}
                        style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                </View>
            );
        }
    }

    renderTrackLock(item, index) {
        // LogUtils.infoLog1("index",index);
        if (item.can_redeem === 1) {

            return (
                <View
                    key={item.r_id}
                    style={styles.textTrackUnlock}>
                    <View style={{ position: 'absolute', alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>

                        <Text style={styles.textMainTrainer1}>Reward unlocked</Text>
                    </View>
                    <View style={{ width: 25, height: 25, position: 'absolute', alignSelf: 'center', backgroundColor: '#8c52ff', borderRadius: 15, justifyContent: 'center' }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_unlock.png')}
                            style={{ width: 15, height: 15, position: 'absolute', alignSelf: 'center', }} />
                    </View>
                </View>
            );
        } else if (item.can_redeem === 0) {
            return (
                <View
                    key={item.r_id}
                    style={styles.textTracklock}>

                    <View style={{ position: 'absolute', alignSelf: 'center', justifyContent: 'center', }}
                    >

                        <Text style={styles.textMainTrainer2}>{`Reach ${item.points} points to unlock`}</Text>
                    </View>

                    <View style={{ width: 25, height: 25, position: 'absolute', alignSelf: 'center', backgroundColor: '#8c52ff', borderRadius: 15, justifyContent: 'center', flexDirection: 'column', marginTop: -1, }}
                    >
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_lock.png')}
                            style={{ width: 15, height: 15, position: 'absolute', alignSelf: 'center', }} />
                    </View>

                </View>

            );
        }

    }

    renderClaimed(item) {
        if (item.can_redeem === 1) {
            return (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 5, width: 80 }}>
                    <TouchableOpacity
                        onPress={() => {
                            // if (item.is_coupon === 1) {
                            //     Actions.rewardRedeem({ mFrom: 'coupon', item: item });
                            // } else {
                            //     Actions.rewardRedeem({ mFrom: 'product', item: item });
                            // }
                            Actions.rewardRedeem({ mFrom: 'product', item: item });
                        }}>
                        <Text style={styles.textTrainerPrograms}>Claim</Text>
                    </TouchableOpacity>
                </LinearGradient>

            );
        } else {
            return (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 5, }}>
                    <Text style={styles.textMainTrainer}>{`${item.points} Points`}</Text>
                </LinearGradient>


            );
        }

    }

    renderClaimedCount(item) {

        if (item.rw_cnt) {
            return (
                <View>
                    {item.rw_cnt.split('|').map((tipDesc, index) => (
                        <View style={styles.viewListAllInner}>

                            <Text style={styles.textMainTrainer}>{tipDesc}</Text>
                        </View>
                    ))}
                </View>

            );
        } else {
            return (
                <View></View>
            );
        }

    }

    renderRewardsTab() {
        if (this.state.rewardsData) {
            if (Array.isArray(this.state.rewardsData.sbnsb_rewards) && this.state.rewardsData.sbnsb_rewards.length) {
                return (
                    <View style={{ width: '100%', margin: 5, flexDirection: 'row', justfyContent: 'center', }}>
                        <View style={{ width: '100%', margin: 5, flexDirection: 'column', justfyContent: 'center', }}>

                            <Text style={styles.textInfo}>{this.state.rewardsData.info_text}</Text>

                            {/* <View style={{ width: wp('100%'), flexDirection: 'row' }}> */}

                            <FlatList
                                extraData={this.state}
                                contentContainerStyle={{ paddingBottom: hp('15%') }}
                                data={this.state.rewardsData.sbnsb_rewards}
                                keyExtractor={(item, index) => "sbnsb_rewards" + index}
                                // onEndReached={this.loadMore.bind(this)}
                                // onEndReachedThreshold={0.7}
                                // windowSize={21}
                                initialNumToRender={5}
                                // removeClippedSubviews={true}
                                // scrollEventThrottle={16}
                                renderItem={({ item, index }) => (

                                    <View style={{ width: wp('100%'), flexDirection: 'row' }}>
                                        <View
                                            key={"Reward" + index}
                                            style={{
                                                width: wp('77%'),
                                                height: undefined,
                                                marginBottom: 15,
                                                alignSelf: 'flex-start',
                                                justifyContent: 'center',
                                                // marginRight: 15,
                                                aspectRatio: 2 / 1,
                                            }}>
                                            <TouchableOpacity
                                                onPress={() => this.setState({ isPopupVisible: true, imageUrl: item.image, title: item.title, description: item.description })}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{
                                                        width: wp('77%'),
                                                        height: undefined,
                                                        justifyContent: 'center',
                                                        alignSelf: 'flex-start',
                                                        // resizeMode: 'stretch',
                                                        borderRadius: 5,
                                                        aspectRatio: 2 / 1,
                                                    }}
                                                    source={{ uri: item.image }}
                                                />
                                            </TouchableOpacity>


                                            {item.is_redeemed === 1
                                                ? (

                                                    <View style={{ width: 20, height: 20, position: 'absolute', alignSelf: 'flex-end', margin: 5, top: 5, right: 10, }}
                                                    >
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_green_tick.png')}
                                                            style={{ width: 17, height: 17, position: 'absolute', alignSelf: 'flex-end', margin: 3, top: 0, right: 1, }} />
                                                    </View>


                                                )
                                                : (

                                                    this.renderLock(item)

                                                )}

                                            {item.is_redeemed === 1
                                                ? (

                                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, bottom: 5, position: 'absolute', right: 5, width: 80, }}>
                                                        <Text style={styles.textTrainerPrograms}>Claimed</Text>
                                                    </LinearGradient>




                                                )
                                                : (

                                                    this.renderClaimed(item)

                                                )}

                                            {item.rw_cnt
                                                ?
                                                (
                                                    <View style={{ borderRadius: 15, bottom: 5, position: 'absolute', left: 5, }}>
                                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, margin: 10, paddingLeft: 10, paddingRight: 10, paddingTop: 2, paddingBottom: 2 }} >
                                                            {item.rw_cnt.split('|').map((tipDesc, index) => (
                                                                // <View style={styles.viewListAllInner}>
                                                                //     <View style={styles.viewBlackDot}></View>
                                                                <Text key={index} style={styles.textCountClaimed}>{tipDesc}</Text>
                                                                // </View>
                                                            ))}
                                                        </LinearGradient>

                                                    </View>

                                                )
                                                :
                                                (
                                                    <View>
                                                    </View>
                                                )
                                            }




                                        </View>

                                        {this.renderTrackLock(item, index)}


                                    </View>
                                )}
                                keyExtractor={item => item.r_id}
                                initialNumToRender={5}
                                showsVerticalScrollIndicator={false}

                            />


                        </View>




                        {/* </View> */}

                    </View>
                );
            }
        }

    }

    renderSectionHeader = ({ section }) => {
        return <Text style={{
            fontSize: 16,
            fontWeight: '500',
            lineHeight: 18,
            fontFamily: 'Rubik-Medium',
            color: '#282c37', marginLeft: 20,
            marginTop: 10,
        }}>{section.title}</Text>
    }

    renderSection = ({ item }) => {
        LogUtils.infoLog1("Item", item);
        return (
            <View style={styles.aroundListStyle} >

                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                    <Text style={styles.textWorkName}>{`${item.title}`}</Text>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <Text style={styles.buttonText}>{`+${item.points} Points`}</Text>
                    </LinearGradient>

                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20, }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_refer.png')}
                        style={styles.imgGoal}
                    />
                    <Text style={styles.textStatusTit}>{`${item.sub_title}`}</Text>

                </View>

                {this.renderTaskCompleted(item)}

            </View>

        )
    }
    onClaimPressed(item) {
        let body = '';
        body = JSON.stringify({
            a_id: item.a_id,
            at_id: item.at_id,
            is_claimed: 1,
        });
        this.saveClaimPoints(body);

    }

    renderRefreshCountDown(item) {
        if (item.refresh_in) {
            // LogUtils.infoLog1("item.refresh_in",item.refresh_in);

            if (item.refresh_in > 86400) {
                return <CountDown
                    style={{ alignSelf: 'center', }}
                    until={item.refresh_in - remainingTime}
                    size={14}
                    onFinish={() => { }}
                    digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                    digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                    timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                    separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                    timeToShow={['D', 'H',]}
                    timeLabels={{ d: 'days', h: 'hrs', m: 'mins', s: 'sec' }}
                // showSeparator
                />

            } else {
                return <CountDown
                    style={{ alignSelf: 'center', }}
                    until={item.refresh_in - remainingTime}
                    size={14}
                    onFinish={() => { }}
                    digitStyle={{ backgroundColor: '#8c52ff', borderWidth: 2, borderColor: '#8c52ff' }}
                    digitTxtStyle={{ color: '#ffffff', fontSize: 10 }}
                    timeLabelStyle={{ color: '#6d819c', fontSize: 10 }}
                    separatorStyle={{ color: '#6d819c', fontWeight: 'bold', fontSize: 12 }}
                    timeToShow={['H', 'M', 'S']}
                    timeLabels={{ d: 'days', h: 'hrs', m: 'mins', s: 'sec' }}
                // showSeparator
                />
            }

        }
    }
    renderProgress(item) {
        if (((item.achived / item.target_limit) * 100) >= 100) {
            // this.setState({ isRewardComplete: true, taskType: item.task_type, taskName: item.title });
            return (
                <ProgressBarAnimated
                    {...progressCustomStyles}
                    width={barWidth}
                    height={15}
                    style={styles.activityIndicatorWrapper}
                    value={100}
                    // maxValue={100}
                    onComplete={() => {
                        // Alert.alert('Hey!', 'onComplete event fired!');
                        LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                        this.setState({ isRewardComplete: true, taskType: item.task_type, taskName: item.title });
                        LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                    }}
                />

            );
        } else {
            return (
                <ProgressBarAnimated
                    {...progressCustomStyles}
                    width={barWidth}
                    height={15}
                    style={styles.activityIndicatorWrapper}
                    value={(item.achived / item.target_limit) * 100}
                    // maxValue={100}
                    onComplete={() => {
                        // Alert.alert('Hey!', 'onComplete event fired!');
                        LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                        this.setState({ isRewardComplete: true, taskType: item.task_type, taskName: item.title });
                        LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                    }}
                />

            );
        }

    }

    renderTaskCompleted = (item) => {

        if (item.achived >= item.target_limit) {

            if (item.is_claimed === 1) {
                return (
                    <View>
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={{
                                fontSize: 14,
                                fontWeight: '500',
                                letterSpacing: 0.2,
                                fontFamily: 'Rubik-Medium',
                                color: '#6d819c',
                                lineHeight: 18,
                                marginTop: 10,
                            }}>Refreshes in </Text>
                            {this.renderRefreshCountDown(item)}
                        </View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 15, width: '80%', alignSelf: 'center', marginTop: 10 }}>
                            <Text style={styles.textTrainerPrograms}>Completed</Text>
                        </LinearGradient>
                    </View>

                );

            } else {
                return (
                    <View>
                        <Text style={styles.textStatusVal}>{`${item.staus}`}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 10, }}>
                            {this.renderProgress(item)}

                            <TouchableOpacity
                                onPress={() => this.onClaimPressed(item)}>
                                <LinearGradient colors={['#228B22', '#228B22']} style={{ borderRadius: 15, marginRight: 5, marginLeft: 5, width: 60 }}>
                                    <Text style={styles.textTrainerPrograms}>Claim</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                        </View>
                    </View>

                );
            }

        } else {
            return (
                <View>
                    <Text style={styles.textStatusVal}>{`${item.staus}`}</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 10, }}>
                        <ProgressBarAnimated
                            {...progressCustomStyles}
                            width={barWidth}
                            height={15}
                            style={styles.activityIndicatorWrapper}
                            value={(item.achived / item.target_limit) * 100}
                            onComplete={() => {
                                // Alert.alert('Hey!', 'onComplete event fired!');
                            }}
                        />
                        <Text style={styles.textprogressVal}>{`${item.achived_text}`}</Text>
                    </View>
                </View>
            );
        }
    }

    renderTasks(flag) {
        let arrayIndex = 0;
        if (Array.isArray(this.state.rewardsData.task) && this.state.rewardsData.task.length) {

            this.state.rewardsData.task.map((item1, index) => {

                if (item1.title === flag) {
                    arrayIndex = index;
                }
            })
            return (
                <View style={{}}>


                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('5%'), }}

                        data={this.state.rewardsData.task[arrayIndex].data}
                        keyExtractor={item => "task" + item.a_id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <View style={styles.aroundListStyle} >

                                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                                    <Text style={styles.textWorkName}>{`${item.title}`}</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                        <Text style={styles.buttonText}>{`+${item.points} Points`}</Text>
                                    </LinearGradient>

                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20, }}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_refer.png')}
                                        style={styles.imgGoal}
                                    />
                                    <Text style={styles.textStatusTit}>{`${item.sub_title}`}</Text>
                                    <TouchableOpacity
                                        onPress={() =>

                                            this.setState({ isTaskInfo: true, description: item.description })
                                        }
                                        style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_info_blueround.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                                marginLeft: 5,
                                                alignSelf: 'flex-start',
                                            }}
                                        />
                                    </TouchableOpacity>

                                </View>

                                {this.renderTaskCompleted(item)}

                            </View>

                        }} />

                </View>
            );


        }


    }

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);


        return (
            <TabBar
                style={{ width: '100%', backgroundColor: 'transparent', width: '98%', alignSelf: 'center', justifyContent: 'center' }}
                labelStyle={styles.textPlan}
                {...props}
                indicatorStyle={{ backgroundColor: 'transparent' }}


                renderLabel={({ route }) => (
                    <View style={{ borderRadius: 5, }}>
                        <Text style={{
                            width: '100%',
                            fontFamily: 'Rubik-Regular',
                            alignSelf: "center",
                            fontSize: 12,
                            fontWeight: '500',
                            lineHeight: 18,
                            paddingTop: 7,
                            paddingBottom: 7,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 15,
                            overflow: 'hidden',
                            backgroundColor: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                '#8c52ff' : '#ffffff', elevation: 0, borderColor: '#e9e9e9',
                            color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                '#ffffff' : '#6D819C'
                        }}>
                            {route.title}
                        </Text>
                    </View>
                )}
            />
        );
    };

    _renderTabBar1 = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return (
            <TabBar
                style={{ backgroundColor: '#FFFFFF', elevation: 0, borderColor: '#fff', borderBottomWidth: 1, }}
                labelStyle={styles.textPlan}
                {...props}
                indicatorStyle={{ backgroundColor: '#8c52ff', height: 3 }}

                renderLabel={({ route }) => (
                    <View>
                        <Text style={{
                            fontFamily: 'Rubik-Medium',
                            alignSelf: "center",
                            fontSize: 12,
                            fontWeight: '500',
                            lineHeight: 18,
                            color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                '#8c52ff' : '#6D819C'
                        }}>
                            {route.title}
                        </Text>
                    </View>
                )}
            />
        );
    };

    onProgramsClicked() {
        LogUtils.firebaseEventLog('click', {
            p_id: 201,
            p_category: 'Rewards',
            p_name: 'Rewards->Programs',
        });

        Actions.traineeWorkoutsHome({ from: 'rewards' });
    }

    onFeedPressed() {
        LogUtils.firebaseEventLog('click', {
            p_id: 202,
            p_category: 'Rewards',
            p_name: 'Rewards->Feed',
        });

        Actions.wall({ from: 'rewards' });
    }

    onTrendWorkoutsClicked() {
        LogUtils.firebaseEventLog('click', {
            p_id: 232,
            p_category: 'Rewards',
            p_name: 'Rewards->Workouts Home',
        });
        Actions.WorkoutsHome({ from: 'rewards' });
    }

    renderAllRewardsData() {
        try {
            if (!isEmpty(this.state.rewardsData)) {

                if (this.state.rewardsData.is_reward_avl === 1) {

                    return (
                        <View style={styles.containerStyle}>
                            <View style={{
                                flexDirection: 'row',
                                margin: 20,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111, }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.textHeadTitle}>Rewards</Text>
                            </View>
                            <TabView
                                navigationState={this.state}
                                renderScene={({ route }) => {
                                    switch (route.key) {
                                        case 'first':
                                            return this.FirstRoute();
                                        case 'second':
                                            return this.SecondRoute();
                                        case 'third':
                                            return this.ThirdRoute();
                                        default:
                                            return null;
                                    }
                                }}
                                onIndexChange={index => this.setState({ index })}
                                initialLayout={initialLayout}
                                style={{ flex: 1 }}
                                renderTabBar={this._renderTabBar1}
                            />
                        </View>
                    )
                }
                else {
                    return (<View style={{
                        width: wp('100%'),
                        height: hp('100%'),
                        backgroundColor: '#ffffff',
                        flexDirection: 'column',
                        flex: 1,
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            margin: 30,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>


                            <Image
                                source={require('../res/img_no_rewardsprogram.png')}
                                style={styles.imgNoRewdProgram}
                            />

                            <Text style={styles.textNodataTitle}>{this.state.rewardsData.noreward_title}</Text>


                            <Text style={styles.textNodata}>{this.state.rewardsData.noreward_desc}</Text>


                        </View>
                    </View>)
                }

            }
            else {
                return (
                    <View style={{
                        width: '100%',
                        height: '100%',
                        backgroundColor: '#ffffff',
                        flex: 1,
                    }}></View>
                )
            }
        } catch (error) {
            console.log(error);
        }
    }

    async callReferAFriend() {
        LogUtils.firebaseEventLog('click', {
            p_id: 226,
            p_category: 'Feed',
            p_name: 'Feed->ReferAFriend',
        });
        Actions.refer();
    }

    renderDietBottomMenuIcon() {
        try {
            if (!isEmpty(this.state.rewardsData)) {
                if (this.state.rewardsData.is_paiduser === 1) { //Paid user
                    if ((this.state.rewardsData.is_diet_plan === 1) ||
                        (this.state.rewardsData.is_diet_plan === 1 && this.state.rewardsData.is_fitness_plan === 1)
                    ) { // only diet and both
                        return (
                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => this.onDietClicked()}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_diet.png')}
                                            style={{
                                                width: 23,
                                                height: 23,
                                                alignSelf: 'center',
                                                tintColor: '#d6d9e0',
                                            }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>Diet</Text>
                                </View>
                            </TouchableOpacity>
                        );
                    } else if (this.state.rewardsData.is_fitness_plan === 1) { // only fitnesss
                        return (
                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => this.onTrendWorkoutsClicked()}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_workout.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>Workouts</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }
                } else { // New User
                    return (
                        <TouchableOpacity
                            style={[styles.bottomClick]}
                            //onPress={() => this.onTrendWorkoutsClicked()}
                            onPress={() => {
                                this.rbMenuSheet.close()
                                LogUtils.firebaseEventLog('click', {
                                    p_id: 222,
                                    p_category: 'Home',
                                    p_name: 'Home->Select a Plan',
                                });
                                Actions.traAllBuyPlans1({ isFreeTrial: this.props.trainee.traineeDetails.show_free_trial, screen: "Bottom memu view plans" });
                            }}
                        >
                            <View style={{ flexDirection: 'column' }}>
                                {
                                    this.props.trainee.traineeDetails.plans_disc_per != "" &&
                                    (<View style={[styles.tabBadge, { backgroundColor: this.props.trainee.traineeDetails.home_bg_color, right: -33, borderRadius: 4, fontSize: 6 }]}>
                                        <Text style={styles.tabBadgeText}>{`${this.props.trainee.traineeDetails.plans_disc_per}`}</Text>
                                    </View>)
                                }
                                <View style={styles.vBtmImgHeight}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_bm_viewplans.png')}
                                        style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                                    />
                                </View>
                                <Text style={styles.imgBottomMenuText1}>Plans</Text>
                            </View>
                        </TouchableOpacity>
                    )
                }
            } else { // When object empty
                return (
                    <TouchableOpacity
                        style={styles.bottomClick}
                        onPress={() => this.onTrendWorkoutsClicked()}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={styles.vBtmImgHeight}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_workout.png')}
                                    style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0', }}
                                />
                            </View>
                            <Text style={styles.imgBottomMenuText1}>Workouts</Text>
                        </View>
                    </TouchableOpacity>
                )
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderMoreBottomMenuIcon() {
        try {
            if (!isEmpty(this.state.rewardsData)) {
                if (this.state.rewardsData.prmdietuser === 1) {
                    return (
                        <TouchableOpacity
                            style={styles.bottomClick}
                            onPress={() => this.rbMenuSheet.open()}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={styles.vBtmImgHeight}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_more_new.png')}
                                        style={{
                                            width: 23,
                                            height: 23,
                                            alignSelf: 'center',
                                            tintColor: '#d6d9e0',//#d6d9e0 #2d3142
                                        }}
                                    />
                                </View>
                                <Text style={styles.imgBottomMenuText1}>More</Text>
                            </View>
                        </TouchableOpacity>
                    );
                }
                else {
                    return (
                        <TouchableOpacity
                            style={styles.bottomClick}
                            onPress={() => this.rbMenuSheet.open()}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={styles.vBtmImgHeight}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_more_new.png')}
                                        style={{
                                            width: 23,
                                            height: 23,
                                            tintColor: '#ffffff',
                                            alignSelf: 'center',
                                            tintColor: '#d6d9e0'
                                        }}
                                    />
                                </View>
                                <Text style={styles.imgBottomMenuText1}>More</Text>
                            </View>
                        </TouchableOpacity>
                    );
                }
            }
            else {
                return (
                    <TouchableOpacity
                        style={styles.bottomClick}
                        onPress={() => this.rbMenuSheet.open()}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={styles.vBtmImgHeight}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_more_new.png')}
                                    style={{
                                        width: 23,
                                        height: 23,
                                        tintColor: '#ffffff',
                                        alignSelf: 'center',
                                        tintColor: '#d6d9e0'
                                    }}
                                />
                            </View>
                            <Text style={styles.imgBottomMenuText1}>More</Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        } catch (error) {
            console.log(error);
        }
    }

    onDietClicked() {
        LogUtils.firebaseEventLog('click', {
            p_id: 204,
            p_category: 'Feed',
            p_name: 'Feed->Diet',
        });
        Actions.dietHome1({ from: 'rewards' });
    }

    render() {
        return (
            <ImageBackground source={require('../res/ic_rewards_bg.jpg')} style={styles.mainContainer}>
                <Loader loading={this.state.loading} />
                {this.renderAllRewardsData()}
                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <Dialog
                    onDismiss={() => {
                        this.setState({ isPopupVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isPopupVisible: false });
                    }}
                    width={0.75}
                    visible={this.state.isPopupVisible}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        // resizeMode='cover'
                                        source={{ uri: this.state.imageUrl }}
                                        style={styles.imgpopup}
                                    />
                                    <Text style={styles.textPopTitle}>{this.state.title}</Text>
                                    <Text style={styles.textPopDesc}>{this.state.description}</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isPopupVisible: false });
                                        }}>
                                            <Text style={styles.textSave}>OK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>


                <Dialog
                    onDismiss={() => {
                        this.setState({ isTaskInfo: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isTaskInfo: false });
                    }}
                    width={0.75}
                    visible={this.state.isTaskInfo}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                    <Text style={styles.textPopTitle}> Task Information </Text>

                                    {this.state.description
                                        ?
                                        (
                                            <View>
                                                {this.state.description.split('||').map((tipDesc, index) => (
                                                    <View style={styles.viewListAllInner}>
                                                        <View style={styles.viewBlueDot}></View>
                                                        <Text style={styles.textPopDesc}>{tipDesc}</Text>
                                                    </View>
                                                ))}
                                            </View>
                                        )
                                        :
                                        (
                                            <View style={styles.viewListAllInner}>
                                                <View style={styles.viewBlueDot}></View>
                                                <Text style={styles.textPopDesc}>No data available</Text>
                                            </View>
                                        )}


                                    {/* <Text style={styles.textPopDesc}>{this.state.description}</Text> */}
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientTaskInfo}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isTaskInfo: false });
                                        }}>
                                            <Text style={styles.textTaskInfoSave}>OK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>


                <Dialog
                    onDismiss={() => {
                        this.setState({ isRewardComplete: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isRewardComplete: false });
                    }}
                    width={0.5}
                    height={65}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'right',
                        initialValue: -1,
                    })}
                    dialogStyle={{
                        alignSelf: 'flex-start',
                        alignItems: 'flex-start',
                        // marginRight: 10,
                        // marginBottom: 300,
                        position: 'absolute',
                        right: 10,
                        top: 60,
                        paddingLeft: 5,
                        paddingRight: 5,
                        paddingBottom: 10,
                        paddingTop: 10,
                        flexDirection: 'column',
                    }}
                    visible={this.state.isRewardComplete}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff', flexDirection: 'column', alignContent: 'flex-start', justifyContent: 'flex-start', alignSelf: 'flex-start'
                        }}>
                        <View style={{ flexDirection: 'column', }}>


                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>

                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_rewards.png')}
                                    style={{ width: 25, height: 25, tintColor: '#8c52ff', alignSelf: 'center', marginBottom: 5 }}
                                />
                                <View style={{ flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingBottom: 5, }}>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: '400',
                                        letterSpacing: 0.2,
                                        fontFamily: 'Rubik-Regular',
                                        textAlign: 'left',
                                        color: '#6d819c',
                                    }}>{this.state.taskType} Task</Text>
                                    <Text
                                        numberOfLines={2}
                                        style={{
                                            fontSize: 12,
                                            fontWeight: '500',
                                            letterSpacing: 0.2,
                                            fontFamily: 'Rubik-Medium',
                                            textAlign: 'left',
                                            color: '#8c52ff',

                                        }}>{this.state.taskName}</Text>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: '400',
                                        letterSpacing: 0.2,
                                        fontFamily: 'Rubik-Regular',
                                        textAlign: 'left',
                                        color: '#6d819c',
                                    }}>Task Complete!</Text>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <RBSheet
                    ref={ref => {
                        this.rbMenuSheet = ref;
                    }}
                    height={190}
                    openDuration={250}
                    customStyles={{
                        container: {
                            justifyContent: 'center',
                            // alignItems: 'center',
                            alignSelf: 'center',
                            width: wp('95%'),
                            marginVertical: 10,
                            borderRadius: 10,
                        }
                    }}
                >
                    <View style={{ flexDirection: 'column', }}>
                        <View style={styles.vRBMenuBottom}>
                            {/* Food Photos */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close();
                                    Actions.traFoodPhotos({ from: 'rewards' });
                                }}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_foodphoto.png')}
                                            style={{
                                                width: 23,
                                                height: 23,
                                                tintColor: '#2d3142',
                                                alignSelf: 'flex-start',
                                            }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Food{'\n'}Photos</Text>
                                </View>
                            </TouchableOpacity>

                            {/* One on one Chat */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 231,
                                        p_category: 'Home',
                                        p_name: 'Home->ChatList',
                                    });
                                    Actions.chatlist({ from: 'rewards' });
                                }}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_bm_oneononechat.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142', }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>One on one{'\n'}Chat</Text>
                                </View>
                            </TouchableOpacity>


                            {/* Workouts / plans */}
                            {
                                (this.state.rewardsData.is_paiduser === 0 || this.state.rewardsData.is_diet_plan === 1) ?
                                    (
                                        <TouchableOpacity
                                            style={styles.bottomrbMenuClick}
                                            onPress={() => {
                                                this.rbMenuSheet.close()
                                                this.onTrendWorkoutsClicked()
                                            }}
                                        >
                                            <View style={{ flexDirection: 'column', }}>
                                                <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_workout.png')}
                                                        style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                                    />
                                                </View>
                                                <Text style={styles.imgBottomMenuText}>Workouts{'\n'}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                    :
                                    (<TouchableOpacity
                                        style={styles.bottomrbMenuClick}
                                        onPress={() => {
                                            this.rbMenuSheet.close()
                                            LogUtils.firebaseEventLog('click', {
                                                p_id: 222,
                                                p_category: 'Home',
                                                p_name: 'Home->Select a Plan',
                                            });
                                            Actions.traAllBuyPlans1({ isFreeTrial: this.props.trainee.traineeDetails.show_free_trial, screen: "Bottom memu view plans" });
                                        }}>
                                        <View style={{ flexDirection: 'column', }}>
                                            {
                                                this.props.trainee.traineeDetails.plans_disc_per != "" &&
                                                (<View style={[styles.tabBadge, { backgroundColor: this.props.trainee.traineeDetails.home_bg_color, right: -33, borderRadius: 4, fontSize: 6 }]}>
                                                    <Text style={styles.tabBadgeText}>{`${this.props.trainee.traineeDetails.plans_disc_per}`}</Text>
                                                </View>)
                                            }
                                            <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_bm_viewplans.png')}
                                                    style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                                />
                                            </View>
                                            <Text style={styles.imgBottomMenuText}>Plans</Text>
                                        </View>
                                    </TouchableOpacity>)
                            }

                            {/* Refer a Friend */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    this.callReferAFriend();
                                }}>

                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_refer_new.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Refer a{'\n'}Friend</Text>
                                </View>
                            </TouchableOpacity>

                            {/* Rewards for prm diet user */}
                            {/* {this.state.rewardsData.prmdietuser === 1 &&
                                <TouchableOpacity
                                    style={styles.bottomrbMenuClick}
                                    onPress={() => {
                                        this.rbMenuSheet.close()
                                    }}>
                                    <View style={{ flexDirection: 'column' }}>
                                        {
                                            this.state.rewardsData.activity_cnt ? (
                                                <View style={styles.tabBadge}>
                                                    <Text style={styles.tabBadgeText}>{`${this.state.rewardsData.activity_cnt}`}</Text>
                                                </View>
                                            ) :
                                                (<View />)
                                        }
                                        <View style={styles.vBtmImgHeight}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_rewards1.png')}
                                                style={{
                                                    width: 23,
                                                    height: 23,
                                                    alignSelf: 'center',
                                                    tintColor: '#8c52ff'
                                                }}
                                            />
                                        </View>
                                        <Text style={[styles.imgBottomMenuText, { color: '#8c52ff' }]}>Rewards{'\n'}</Text>
                                    </View>
                                </TouchableOpacity>
                            } */}
                        </View>

                        <View style={{ height: 15, }}></View>

                        <View style={styles.vRBMenuBottom}>
                            {/* Store */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 214,
                                        p_category: 'Home',
                                        p_name: 'Home->TopSellers',
                                    });
                                    Actions.webview({ url: shopping_url });
                                }}>
                                <View style={{ flexDirection: 'column', }}>

                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_store.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Store</Text>
                                </View>
                            </TouchableOpacity>

                            {/* Appointments */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    Actions.appointments({ from: 'rewards' });
                                }}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_bm_appointments.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Appointments</Text>
                                </View>
                            </TouchableOpacity>

                            {/* Feed */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    this.onFeedPressed()
                                }}>
                                <View style={{ flexDirection: 'column', }}>

                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_bm_feed.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Feed</Text>
                                </View>
                            </TouchableOpacity>

                            {/* Macros */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => {
                                    this.rbMenuSheet.close()
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 230,
                                        p_category: 'Home',
                                        p_name: 'Home->PCFDetails',
                                    });
                                    Actions.pcfdetails({ from: 'rewards' });
                                }}>

                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_macros.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#2d3142' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Macros</Text>
                                </View>
                            </TouchableOpacity>
                            {/* More (popup) */}
                            <TouchableOpacity
                                style={styles.bottomrbMenuClick}
                                onPress={() => this.rbMenuSheet.close()}>
                                <View style={{ flexDirection: 'column', alignSelf: 'center', }}>
                                    <View style={{ height: 23, width: 23, alignSelf: 'center', }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_more_new.png')}
                                            style={{
                                                width: 23,
                                                height: 23,
                                                tintColor: '#ffffff',
                                                alignSelf: 'center',
                                                tintColor: '#8c52ff'
                                            }}
                                        />
                                    </View>
                                    <Text style={[styles.imgBottomMenuText, { color: '#8c52ff' }]}>More</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </RBSheet>

                {/* bottom nav bar */}
                <View style={styles.bottom}>
                    <View style={styles.bottomHome}>
                        <View style={styles.bottomViewContainer}>
                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => Actions.popTo('traineeHome')}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_home_gray.png')}
                                            style={styles.imgBottomMenu}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>Home</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => this.onProgramsClicked()}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_workout_gray.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>Programs</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => Actions.trackershome({ from: 'rewards' })}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_tracker.png')}
                                            style={{ height: 23, width: 23, alignSelf: 'center', tintColor: '#d6d9e0' }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>Tracker</Text>
                                </View>
                            </TouchableOpacity>

                            {this.renderDietBottomMenuIcon()}

                            <TouchableOpacity
                                style={styles.bottomClick}>
                                <View style={{ flexDirection: 'column' }}>
                                    {
                                        this.state.rewardsData.activity_cnt ? (
                                            <View style={styles.tabBadge}>
                                                <Text style={styles.tabBadgeText}>{`${this.state.rewardsData.activity_cnt}`}</Text>
                                            </View>
                                        ) :
                                            (<View />)
                                    }
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_rewards.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                                alignSelf: 'center',
                                                tintColor: '#2d3142',
                                            }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText}>Rewards</Text>
                                </View>
                            </TouchableOpacity>
                            {/* {this.renderMoreBottomMenuIcon()} */}
                            <TouchableOpacity
                                style={styles.bottomClick}
                                onPress={() => this.rbMenuSheet.open()}>
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={styles.vBtmImgHeight}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_more_new.png')}
                                            style={{
                                                width: 23,
                                                height: 23,
                                                alignSelf: 'center',
                                                tintColor: '#d6d9e0',//#d6d9e0 #2d3142
                                            }}
                                        />
                                    </View>
                                    <Text style={styles.imgBottomMenuText1}>More</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground >
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        height: 28,
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        right: -23,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    activityIndicatorWrapper: {
        backgroundColor: '#f1f1f1',
        width: '95%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 10,
        paddingBottom: 20,
    },
    textIntro: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    image: {
        width: wp('49%'),
        height: hp('20%'),
        borderRadius: 5,
        alignSelf: 'center',
    },

    mainView: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    header: {
        width: wp('100%'),
        height: 60,
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    headerInner: {
        width: wp('33.3%'),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    headerSel: {
        width: wp('33.3%'),
        height: 3,
        marginTop: 10,
    },
    textWorkName: {
        width: wp('66%'),
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 18,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    buttonText: {
        fontSize: 10,
        textAlign: 'center',
        padding: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },

    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginTop: 5,// approximate a square
    },

    column: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 10,
    },

    textStatusTit: {
        width: wp('55%'),
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
        marginLeft: 10,
    },
    texttitle: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        padding: 15,
        marginLeft: 5,
    },
    textStatusVal: {
        fontSize: 14,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 20,
    },
    textprogressVal: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#6d819c',
        lineHeight: 18,
        marginLeft: 5,
    },
    aroundListStyle: {
        width: '90%',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        padding: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        alignSelf: 'center',
    },
    imgGoal: {
        width: 60,
        height: 60,
        marginLeft: 10,
        alignSelf: 'flex-end',
    },
    textTrainerPrograms: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 11,
        padding: 7,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
    textMainTrainer: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 11,
        padding: 7,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
    textMainTrainer1: {
        width: rewardHeight,
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 11,
        padding: 7,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        transform: [{ rotate: '90deg' }],
        marginTop: 70,
        marginRight: wp('12%')
    },
    textMainTrainer2: {
        width: rewardHeight,
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 11,
        padding: 7,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        transform: [{ rotate: '90deg' }],
        marginTop: 60,
        marginRight: wp('12%'),
        lineHeight: 11,
    },

    imgpopup: {
        width: '115%',
        height: undefined,
        // alignSelf: 'center',
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        // margin: 5
        resizeMode: 'cover',
        aspectRatio: 2 / 1,
    },
    textPopTitle: {
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 5,
    },
    textPopDesc: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 16,
        marginLeft: 8,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    textTaskInfoSave: {
        width: wp('75%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },

    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
    },

    linearGradientTaskInfo: {
        width: '75%',
        borderBottomLeftRadius: 9,
        borderBottomRightRadius: 9,
        justifyContent: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        marginBottom: -25,
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 15,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },
    bottom: {
        justifyContent: 'flex-end',
        height: 40,
        backgroundColor: '#ffffff',
        // paddingBottom: 10,
    },
    bottomHome: {
        width: wp('100%'),
        height: hp('7%'),
        backgroundColor: '#ffffff',
    },
    bottomViewContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        width: wp('100%'),
        backgroundColor: '#ffffff',
    },

    bottomClick: {
        width: wp('16.66%'),
        bottom: -5,
        alignItems: 'center',
    },
    vBtmImgHeight: {
        height: 23,
        width: 23,
        flexDirection: 'column',
        alignSelf: 'center',
    },
    imgBottomMenu: {
        width: 17,
        height: 17,
        alignSelf: 'center',
    },
    imgBottomMenuText: {
        alignSelf: 'center',
        fontSize: 9,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    imgBottomMenuText1: {
        alignSelf: 'center',
        fontSize: 9,
        fontWeight: '500',
        color: '#d6d5dd',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    vRBMenuBottom: {
        flexDirection: 'row',
        alignItems: 'center',
        width: wp('95%'),
        marginBottom: 10,
        marginTop: 10,
    },
    bottomrbMenuClick: {
        width: wp('19%'),
        // width: wp('23.75%'),
        alignItems: 'center',
    },
    tabBadge: {
        position: 'absolute',
        top: -10,
        right: -7,
        backgroundColor: '#8c52ff',
        borderRadius: 16,
        paddingHorizontal: 6,
        paddingVertical: 2,
        zIndex: 2,
    },
    tabBadgeText: {
        color: 'white',
        fontSize: 9,
        fontWeight: '600',
    },
    textCountClaimed: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        paddingTop: 1,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
    imgNoRewdProgram: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textInfo: {
        width: wp('80%'),
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 12,
        // fontWeight: '400',
        lineHeight: 18,
        // margin: 10,
        // paddingRight: 10,
        paddingBottom: 10,

    },
    textTrackUnlock: {
        width: wp('2%'),
        height: rewardHeight,
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',
        position: 'absolute',
        right: 35,
        borderLeftWidth: 0.5,
        borderRightWidth: 0.5,
        borderColor: '#ffffff',
        backgroundColor: '#8c52ff',

    },
    textTracklock: {
        width: wp('2%'),
        height: rewardHeight,
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',
        position: 'absolute',
        right: 35,
        borderLeftWidth: 0.5,
        borderRightWidth: 0.5,
        borderColor: '#8c52ff',
        backgroundColor: '#ffffff',
    }
});

const mapStateToProps = state => {
    const trainee = state.traineeFood
    return { trainee };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_Rewards_home),
);
