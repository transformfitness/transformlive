import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    TouchableHighlight,
    ImageBackground,
    PanResponder,
    StyleSheet,
    Animated,
    SafeAreaView,
    Easing,
    Image,
    View,
    Text,
    KeyboardAvoidingView,
    StatusBar, Dimensions, BackHandler, Platform, TouchableOpacity, TextInput, ScrollView, FlatList
} from 'react-native';
import { CustomDialog, Loader, DataInput, WorkoutCustomDialog,HomeDialog } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DeviceInfo from 'react-native-device-info';
import AppIntroSlider from 'react-native-app-intro-slider';
import Video from 'react-native-video';
import NetInfo from "@react-native-community/netinfo";
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';
import padStart from 'lodash/padStart';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LogUtils from '../utils/LogUtils.js';

import AsyncStorage from '@react-native-community/async-storage';
import ShortVideoPlayer from '../components/ShortVideoPlayer';

import { BASE_URL, SWR, VINFO_ANDROID, VINFO_IOS } from '../actions/types';

const initial = Orientation.getInitialOrientation();

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
      statusCode: res[0],
      data: res[1],
    }));
  }

class GymVersion extends Component {

    static defaultProps = {
        toggleResizeModeOnFullscreen: true,
        controlAnimationTiming: 500,
        doubleTapTime: 130,
        playInBackground: true,
        playWhenInactive: false,
        resizeMode: 'contain',
        isFullscreen: false,
        showOnStart: true,
        paused: false,
        repeat: false,
        muted: false,
        volume: 10,
        title: '',
        rate: 1,
        //video_key:"",
    };

    constructor(props) {
        super(props);
        this.state = {
            // Video
            resizeMode: this.props.resizeMode,
            paused: this.props.paused,
            muted: this.props.muted,
            volume: this.props.volume,
            rate: this.props.rate,
            // Controls
            isFullscreen:
                this.props.isFullScreen || this.props.resizeMode === 'cover' || false,
            showTimeRemaining: true,
            volumeTrackWidth: 0,
            volumeFillWidth: 0,
            seekerFillWidth: 0,
            showControls: this.props.showOnStart,
            volumePosition: 0,
            seekerPosition: 0,
            volumeOffset: 0,
            seekerOffset: 0,
            seeking: false,
            originallyPaused: false,
            scrubbing: false,
            loading: false,
            currentTime: 0,
            error: false,
            duration: 0,

            isEndWorkout: false,
            isEndWithOutFeedback: false,
            post: '',
            rating: 5,
            isFeedbackSubmit: false,
            isBackPressed: false,
            fullscreen: false,
            isAlert: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            ignoreSilentSwitch: 'ignore',
            isCompleted: 0,

            isKey: 1,
            url: '',

            logError: {},
            bufferLoader: false,

            gymVersionObj: this.props.gymVersionObj,
            isShow: false,
            slideindex: 0,

            timer: null,
            hours_Counter: '00',
            minutes_Counter: '00',
            seconds_Counter: '00',

            isVisible: false,
            pd_id: 0,
            pdse_id: 0,
            pdses_id: 0,
            setValue: 0,
            setNum: 0,
            setReps: 0,
            alertTitle: "",
            setValueType: '',

            viewIndex: 0,
            video_url: "",
            video_key: 0,

            isShowAlert: false, 
            isShowAlertMsg: ""

        }
    }

    componentDidMount() {
        try {
            Orientation.lockToPortrait();
            this.setState({
                isShow: true,
            })

            let temp = this.state.gymVersionObj;
            temp.forEach((element, index) => {
                if (index == 0) {
                    element.pause = false;
                } else {
                    element.pause = true;
                }
                element.pdse_arr.forEach((pdse, pdseID) => {
                    pdse.pdses_arr.forEach((pdses, pdsesID) => {
                        pdses.set_val = pdses.set_val != "" ? pdses.set_val : "";
                        pdses.reps = pdses.reps != "" ? pdses.reps : "";
                    })
                })
            });
            setTimeout(() => {
                this.setState({ gymVersionObj: temp, });
            }, 1000);

            this.startInterval();
            Orientation.addOrientationListener(this._orientationDidChange);
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });

        } catch (error) {

        }
    }


    async onBackPressed() {
        Orientation.lockToPortrait();
        this.setState({ isBackPressed: true, paused: true });
        //Actions.pop();
        // Actions.popTo('traineePlanNew');
    }

    //Used to show video recording timer on screen
    startInterval() {
        try {
            if (!this.state.isShow) {
                this.timer = null
                this.timer = setInterval(() => {

                    var num = (Number(this.state.seconds_Counter) + 1).toString()
                    var count = this.state.minutes_Counter;
                    //var hcount = this.state.hours_Counter;

                    if (Number(this.state.seconds_Counter) == 59) {
                        count = (Number(this.state.minutes_Counter) + 1).toString();
                        num = '00';
                    }

                    // if (Number(this.state.minutes_Counter) == 60) {
                    //     hcount = (Number(this.state.hours_Counter) + 1).toString();
                    //     count = '00';
                    //     num = '00';
                    // }

                    this.setState({
                        //hours_Counter: hcount.length == 1 ? '0' + hcount : hcount,
                        minutes_Counter: count.length == 1 ? '0' + count : count,
                        seconds_Counter: num.length == 1 ? '0' + num : num
                    });
                }, 1000);
                this.setState({ timer: this.timer })
            }
        } catch (error) {
            console.warn(error)
        }
    }

    checkSetComplete(pdses_id, is_default, title, type) {
        try {
            this.setState({ pdses_id: pdses_id });
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                element.pdse_arr.forEach((pdse, pdseID) => {
                    pdse.pdses_arr.forEach((pdses, pdsesID) => {
                        if (pdses.pdses_id == pdses_id && pdses.is_default == 0) {
                            if (pdses.set_val == "" || pdses.reps == "") {
                                var Atitle = title != "" ? title + " : Set - " + pdses.set : " Set -" + pdses.set;
                                this.setState({
                                    setValue: pdses.set_val,
                                    setReps: pdses.reps,
                                    isAlert: true,
                                    alertMsg: type == 'w' ? "Enter your weight and rep'\s to complete the set." : "Enter your second'\s and rep'\s to complete the set.",
                                    alertTitle: Atitle,
                                    setValueType: type,
                                    //pdse:pdse,
                                    // pd_id:0,
                                    setNum: pdses.set,
                                    pdse_id: pdse.pdse_id,
                                })
                            } else {
                                pdses.is_default = 1;
                                this.setState({
                                    pdse_id: pdse.pdse_id,
                                    setNum: pdses.set,
                                    setValue: pdses.set_val,
                                    setReps: pdses.reps,
                                })
                                setTimeout(() => {
                                    this.saveSetsAndReps();
                                }, 1000);

                            }
                        } else if (pdses.pdses_id == pdses_id && pdses.is_default == 1) {
                            pdses.is_default = 0;
                            pdses.set_val = "";
                            pdses.reps = ""
                        }
                    })
                })
            });

            this.setState({ gymVersionObj: temp });
        } catch (error) {
            console.log(error)
        }
    }


    onChangeText(text, pdses_id, type) {
        try {
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                element.pdse_arr.forEach((pdse, pdseID) => {
                    pdse.pdses_arr.forEach((pdses, pdsesID) => {
                        if (pdses.pdses_id == pdses_id) {
                            if (type == 'w' || type == 'm') {
                                pdses.set_val = text
                            } else if (type == 'r') {
                                pdses.reps = text
                            }
                        }
                    })
                })
            });

            this.setState({ gymVersionObj: temp });
        } catch (error) {

        }
    }

    async saveSetsAndReps() {
        try {

            let params = {
                "pd_id": this.props.pdObj.pd_id,
                "pds_id": this.state.pdse_id,
                "pdses_id": this.state.pdses_id,
                "sets": this.state.setNum,
                "reps": this.state.setReps,
                "val": this.state.setValue
            }

            console.log(params, '---params');

            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/savepdsets`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        "pd_id":this.props.pdObj.pd_id,
                        "pds_id":this.state.pdse_id,
                        "pdses_id":this.state.pdses_id,
                        "sets":this.state.setNum,
                        "reps":this.state.setReps,
                        "val":this.state.setValue
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message,  });
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message, isEndWorkout: true });

                        } else {
                            this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message, isEndWorkout: true });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: SWR, isEndWorkout: true });
                });

        } catch (error) {
            console.log(error);
        }
    }

    async onAccept() {

        this.setState({ isAlert: false, alertMsg: '' });
        let temp = this.state.gymVersionObj;
        temp.forEach(element => {
            element.pdse_arr.forEach((pdse, pdseID) => {
                pdse.pdses_arr.forEach((pdses, pdsesID) => {
                    if (pdses.pdses_id == this.state.pdses_id) {
                        if (pdses.set_val == "" || pdses.reps == "") {
                            pdses.set_val = this.state.setValue
                            pdses.reps = this.state.setReps
                            pdses.is_default = 1
                        }

                    }
                })
            })
        });

        this.setState({ gymVersionObj: temp });
        this.saveSetsAndReps();

    }

    async onDecline() {
        this.setState({ isAlert: false, alertMsg: '' });
    }

    async onChangeWeight(value) {
        this.setState({
            setValue: value
        })
    }

    async onChangeReps(value) {
        this.setState({
            setReps: value
        })
    }

    onAlertAccept() {
        this.setState({ isShowAlert: false, isShowAlertMsg: '' });
    }

    showSetsAndReps(item) {
        try {
            var setAndReps = [];
            //console.log(item,'---item');
            item.pdse_arr.map((ele, index) => {
                //console.log(ele,'---ele');
                setAndReps.push(
                    <View key={ele.pdse_id + "pdse_arr"} style={{ flexDirection: 'column', marginTop: 5, }}>

                        {
                            ele.title != "" &&
                            (<View style={{ backgroundColor: '#8c52ff', height: 30, justifyContent: 'center', borderTopRightRadius: 4, borderTopLeftRadius: 4 }}>
                                <Text style={{ textAlign: 'center', fontFamily: 'Rubik-Regular', fontSize: 14, color: '#ffffff' }}>{ele.title}</Text>
                            </View>)
                        }
                        {/* <View
                            style={{
                                borderBottomColor: '#6d819c',
                                borderBottomWidth: 1,
                                marginTop: 0,
                                //backgroundColor:'red'
                            }}
                        /> */}
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <View style={{ flex: 0.1, alignItems: 'center' }}><Text></Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center' }}><Text style={{ fontFamily: 'Rubik-Regular', }}>Sets</Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center' }}><Text style={{ fontFamily: 'Rubik-Regular', }}>{ele.pdses_arr[index].type == 'w' ? "Kg" : 'Time(sec)'}</Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center' }}><Text style={{ fontFamily: 'Rubik-Regular', }}>Reps</Text></View>
                        </View>
                        <View
                            style={{
                                borderBottomColor: '#6d819c',
                                borderBottomWidth: 1,
                                // height:10,
                                //backgroundColor:'red'
                            }}
                        />

                        {
                            ele.pdses_arr.map((pdses, setID) => {
                                //console.log(pdses,'---pdses');
                                return (
                                    <View key={pdses.pdses_id + "pdses"} style={styles.containerListStyle}>

                                        <View style={{ flex: 0.1 }}>
                                            {
                                                pdses.is_default ? (
                                                    <TouchableOpacity
                                                        onPress={() => this.checkSetComplete(pdses.pdses_id, pdses.is_default, ele.title)}
                                                    >
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/check.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    </TouchableOpacity>

                                                ) : (
                                                    <TouchableOpacity
                                                        onPress={() => this.checkSetComplete(pdses.pdses_id, pdses.is_default, ele.title, pdses.type)}
                                                    >
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/uncheck.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    </TouchableOpacity>

                                                )
                                            }
                                        </View>

                                        <View style={{ flex: 0.3, alignItems: 'center' }}>
                                            <Text style={{ fontFamily: 'Rubik-Regular', }}>{pdses.set}</Text>
                                        </View>

                                        {
                                            pdses.type == 'w' ? (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <TextInput
                                                        ref={input => { this.weights = input; }}
                                                        style={[styles.textInputStyle, { marginLeft: 25, }]}
                                                        placeholder="Kg"
                                                        placeholderTextColor="grey"
                                                        keyboardType="numeric"
                                                        maxLength={3}
                                                        value={pdses.set_val}
                                                        returnKeyType='next'
                                                        onChangeText={text => this.onChangeText(text, pdses.pdses_id, "w")}
                                                    //onSubmitEditing={() => { this.mobileTextInput.focus(); }}
                                                    />
                                                </View>
                                            ) : (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <TextInput
                                                        ref={input => { this.weights = input; }}
                                                        style={styles.textInputStyle}
                                                        placeholder="Sec"
                                                        placeholderTextColor="grey"
                                                        keyboardType="numeric"
                                                        maxLength={3}
                                                        value={pdses.set_val}
                                                        returnKeyType='next'
                                                        onChangeText={text => this.onChangeText(text, pdses.pdses_id, "m")}
                                                    //onSubmitEditing={() => { this.mobileTextInput.focus(); }}
                                                    />

                                                </View>
                                            )
                                        }

                                        <View style={{ flex: 0.3, alignItems: 'center' }}>
                                            <TextInput
                                                ref={input => { this.reps = input; }}
                                                style={styles.textInputStyle}
                                                placeholder="Reps"
                                                placeholderTextColor="grey"
                                                keyboardType="numeric"
                                                maxLength={3}
                                                value={pdses.reps}
                                                returnKeyType='next'
                                                onChangeText={text => this.onChangeText(text, pdses.pdses_id, "r")}
                                            //onSubmitEditing={() => { this.mobileTextInput.focus(); }}
                                            />
                                        </View>

                                    </View>
                                )
                            })
                        }
                    </View>
                )
            })
            return (
                <View>
                    {setAndReps}
                </View>
            )
        } catch (error) {
            console.log(error);
        }
    }

    get pagination1() {
        // const { entries, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={this.state.gymVersionObj.length}
                activeDotIndex={this.state.viewIndex}
                containerStyle={{ paddingVertical: 10 }}
                dotContainerStyle={{
                    // backgroundColor: '#ffffff',
                    width: 10,
                    height: 10,
                    // marginTop: -20,
                    // marginBottom: -20
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#8c52ff',
                    //marginRight:30
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#b0b0b0',
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={0.8}
            />
        );
    }

    async onExitWorkout() {
        try {
            LogUtils.infoLog1("OnExit");
            this.setState({
                isCompleted: 0,
                isBackPressed: false,
                paused: true,
                isEndWorkout: true
            });
            // this.saveEndWorkOut();
            // if (this.props.from === 'home') {
            Actions.pop();
            // } else if (this.props.from === 'workouts') {
            //     Actions.WorkoutsHome();
            // } else {
            //     // Actions.traineeWorkoutsHome();
            //     Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
            // }
        } catch (error) {
            console.log(error);
        }
    }

    async onCompleteWorkout() {
        this.setState({ isCompleted: 1, isBackPressed: false, paused: true });
        Actions.pop();
    }

    _renderItem = ({ item, index }) => {
        //console.log(item,'---item');
        return (

            <View key={item.title + index.toString()} style={styles.containerStyle}>

                <View style={styles.mainView}>

                    {/* touch back */}
                    <View style={styles.viewTitleCal}>
                        <View style={{
                            flexDirection: 'row',
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ marginTop: 5, }} />
                    </View>

                    <View style={[styles.viewTitleCal, { height: hp("42%") }]}>

                        {/* title */}
                        <View style={{ justifyContent: 'center' }}>
                            <View>
                                <Text style={{ fontSize: 15, fontFamily: 'Rubik-Medium', textAlign: 'center' }} > {item.title}</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 13, fontFamily: 'Rubik-Medium', textAlign: 'center' }} > Exercise {index + 1} of {this.state.gymVersionObj.length} </Text>
                            </View>
                        </View>

                        {/* Video player */}
                        <ShortVideoPlayer
                            video_url={item.video_url}
                            video_key={item.pds_id}
                            pause={item.pause}
                        />

                    </View>

                    {/* <View style={{marginTop:10}}></View> */}
                    
                    {/* {this.pagination1} */}

                    {/* show sets and reps */}
                    <View style={[styles.viewTitleCal, { height: hp("45%") }]}>
                        <View style={{}}>
                            <ScrollView
                                key={index + "gymVersion"}
                                ref={ref => this.scrollView = ref}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ paddingBottom: hp('2%') }}
                            >
                                {this.showSetsAndReps(item)}
                            </ScrollView>
                        </View>
                    </View>

                    {/* timer */}
                    <View style={[styles.viewTitleCal, { height: hp("4%"), justifyContent: 'center', }]}>
                        <View style={{
                            alignSelf: 'center',
                            justifyContent: 'center',
                            //backgroundColor:'#8c52ff',
                            borderRadius: 4,
                            padding: 5,

                        }}>
                            <Text style={{ fontSize: 18, fontFamily: 'Rubik-Medium', fontWeight: '200' }}> {`${this.state.minutes_Counter} : ${this.state.seconds_Counter}  `}

                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/clock.png')}
                                    style={[styles.backImageStyle, { height: 20, width: 20 }]}
                                />
                            </Text>
                        </View>
                    </View>
                    {/* timer end */}
                   
                    <HomeDialog
                        visible={this.state.isShowAlert}
                        title="Success"
                        desc={this.state.isShowAlertMsg}
                        onAccept={this.onAlertAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <DataInput
                        visible={this.state.isAlert}
                        title={this.state.alertTitle}
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        onDecline={this.onDecline.bind(this)}
                        no='CANCEL'
                        yes='SAVE'
                        weight={this.state.setValue}
                        reps={this.state.setReps}
                        setValueType={this.state.setValueType}
                        onChangeWeight={this.onChangeWeight.bind(this)}
                        onChangeReps={this.onChangeReps.bind(this)}
                    />

                    <WorkoutCustomDialog
                        visible={this.state.isBackPressed}
                        title=''
                        desc={'Are you sure you want to exit this workout?'}
                        onAccept={this.onExitWorkout.bind(this)}
                        onDecline={() => { this.setState({ isBackPressed: false, paused: false }) }}
                        onComplete={this.onCompleteWorkout.bind(this)}
                        no='CANCEL'
                        yes='EXIT WORKOUT'
                        third='MY WORKOUT IS COMPLETED'
                    //third='COMPLETE WORKOUT' 
                    />

                </View>
            </View>
        );
    };


    slideChange = (index, lastIndex) => {
        try {
            this.setState({
                loading: true,
                viewIndex: index,
            });
            let temp = this.state.gymVersionObj;
            temp.forEach((element, i) => {
                if (index == i) {
                    element.pause = false
                } else {
                    element.pause = true
                }
            });
            this.setState({ loading: false, gymVersionObj: temp });

        } catch (error) {
            console.log(error);
        }
    }


    render() {
        return (
            <View style={styles.maincontainerStyle}>
                <Loader loading={this.state.loading} />
                {this.state.isShow
                    ? (
                        <AppIntroSlider
                            ref={ref => this.AppIntroSlider = ref}
                            style={{ width: wp('100%'), alignSelf: 'center', }}
                            slides={this.state.gymVersionObj}
                            showSkipButton={false}
                            showDoneButton={false}
                            showNextButton={false}
                            hidePagination={false}
                            bottomButton={true}
                            dotStyle={{ backgroundColor: 'transparent' }}
                            activeDotStyle={{ backgroundColor: 'transparent' }}
                            extraData={this.state}
                            onSlideChange={this.slideChange}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => item.pds_id + item.title}
                        />
                    )
                    : (
                        <View />
                    )
                }
            </View>
        )
    }
}

const styles = {
    player: StyleSheet.create({
        container: {
            overflow: 'hidden',
            backgroundColor: '#000',
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'space-between',
            borderRadius: 15,
            height: 280,
            //width:wp("90%")
        },
        video: {
            overflow: 'hidden',
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
        },
    }),
    error: StyleSheet.create({
        container: {
            backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            justifyContent: 'center',
            alignItems: 'center',
        },
        icon: {
            marginBottom: 16,
            width: 50,
            height: 50,
        },
        text: {
            backgroundColor: 'transparent',
            color: '#8c52ff',//#8c52ff #f27474
            fontFamily: 'Rubik-Medium',
            justifyContent: 'center',
            textAlign: 'center'
        },
    }),
    loader: StyleSheet.create({
        container: {
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            alignItems: 'center',
            justifyContent: 'center',
        },

        centerContainer: {
            // flex: 30,
            marginTop: 60,
            height: '60%',
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center',
        },
    }),
    controls: StyleSheet.create({
        backImageStyle: {
            width: 12,
            height: 19,
            alignSelf: 'center',
            marginLeft: 5,
        },

        backImageContainer: {
            width: 12,
            height: 19,
            alignSelf: 'center',
            padding: 10,
        },
        row: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: null,
            width: null,
        },
        centerRow: {
            flexDirection: 'row',
            alignItems: 'center',
            height: null,
            width: null,
            alignSelf: 'center',
        },
        column: {
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: null,
            width: null,
        },
        vignette: {
            resizeMode: 'stretch',
        },
        control: {
            padding: 16,
        },
        text: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 14,
            textAlign: 'center',
        },
        pullRight: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        top: {
            // flex: 1,
            alignItems: 'stretch',
            justifyContent: 'flex-start',
            height: '10%',
        },
        center: {
            flex: 1,
            alignItems: 'stretch',
            justifyContent: 'center',
        },
        bottom: {
            alignItems: 'stretch',
            // flex: 2,
            justifyContent: 'flex-end',
            height: '20%',
            marginTop: -20
        },
        topControlGroup: {
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            width: null,
            margin: 12,
            marginBottom: 18,
        },
        bottomControlGroup: {
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginLeft: 12,
            marginRight: 12,
            marginBottom: 0,
        },
        centerControlGroup: {
            alignSelf: 'center',
            alignItems: 'center',
            marginLeft: 12,
            marginRight: 12,
            marginBottom: 0,
        },
        volume: {
            flexDirection: 'row',
        },
        fullscreen: {
            flexDirection: 'row',
        },
        playPause: {
            // position: 'relative',
            justifyContent: 'center',
            alignSelf: 'center',
            tintColor: '#ffffff'
        },
        title: {
            alignItems: 'center',
            flex: 0.6,
            flexDirection: 'column',
            padding: 0,
        },
        titleText: {
            textAlign: 'center',
        },
        timer: {
            width: 80,

        },
        timerText: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 11,
            textAlign: 'left',
            marginTop: -20,
            marginLeft: -7,
        },
        durationText: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 11,
            textAlign: 'right',
            marginTop: -20,
            marginRight: -7,
        },
    }),
    volume: StyleSheet.create({
        container: {
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            height: 1,
            marginLeft: 20,
            marginRight: 20,
            width: 150,
        },
        track: {
            backgroundColor: '#333',
            height: 1,
            marginLeft: 7,
        },
        fill: {
            backgroundColor: '#FFF',
            height: 1,
        },
        handle: {
            position: 'absolute',
            marginTop: -24,
            marginLeft: -24,
            padding: 16,
        },
        icon: {
            marginLeft: 7,
        },
    }),
    seekbar: StyleSheet.create({
        container: {
            alignSelf: 'stretch',
            height: 28,
            marginLeft: 20,
            marginRight: 20,
        },
        track: {
            backgroundColor: '#333',
            height: 1,
            position: 'relative',
            top: 14,
            width: '100%',
        },
        fill: {
            backgroundColor: '#FFF',
            height: 1,
            width: '100%',
        },
        handle: {
            position: 'absolute',
            marginLeft: -7,
            height: 28,
            width: 28,
        },
        circle: {
            borderRadius: 12,
            position: 'relative',
            top: 8,
            left: 8,
            height: 12,
            width: 12,
        },
    }),
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
        //backgroundColor: 'red'
    },
    maincontainerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 10,
        marginRight: 20,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        // paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    todayCalView: {
        flexDirection: 'row',
        marginLeft: 40,
        alignSelf: 'center',
        marginRight: 40,
    },
    textCal: {
        fontSize: 14,
        //fontWeight: '500',
        //color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        padding: 4
        //marginBottom:15
    },
    // container: {
    //     flex: 1,
    //     justifyContent: "flex-start"
    // },
    profileImage: {
        width: 25,
        height: 25,
        resizeMode: 'cover',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textIndicator1: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        marginTop: 5
    },
    containerListStyle: {
        width: wp('90%'),
        //marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        //borderRadius: 6,
        //position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        //marginTop: 5,
        marginRight: 15,
        alignItems: 'center',
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        width: 50,
        //backgroundColor:'red'
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
        width: '100%',
        height: '100%'
    },
    container: {
        flex: 1,
        marginTop: 10,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
}

export default GymVersion;