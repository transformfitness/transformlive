import React, { Component } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    FlatList,
    Dimensions,
    KeyboardAvoidingView, StatusBar
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader, NoInternet } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
const sliderheight = Dimensions.get('window').height;
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import Dialog, {
    DialogContent,
    DialogFooter
} from 'react-native-popup-dialog';
import moment from "moment";
import { BarChart, LineChart, Grid, YAxis, XAxis } from 'react-native-svg-charts';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

const initialLayout = { width: Dimensions.get('window').width };
let colors = ['#CFD7E2', '#E0CBC8', '#DEE2D3', '#D4D3DB'];//, '#EADACB'

class Trainee_PCFDetails extends Component {
    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);
        this.state = {
            index: 0,
            setIndex: 0,
            routes: [
                { key: 'first', title: 'CARBS' },
                { key: 'second', title: 'PROTEINS' },
                { key: 'third', title: 'FATS' },
                { key: 'fourth', title: 'FIBER' },
            ],
            tab: 1,
            isAlert: false,
            loading: false,
            isSubTrue: false,
            alertMsg: '',
            alertTitle: '',
            resObj: '',
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            fill: 0,
            fillProtiens: 0,
            fillFats: 0,
            fillFiber: 0,
            stepArr: [],
            fatsArr: [],
            fibersArr: [],
            protiensArr: [],
            carbsObj: {},
            fatsObj: {},
            fibersObj: {},
            protiensObj: {},
            dialyGoalVisible: false,
            goalType: 0,
            carbsGoal: 0,
            proteinsGoal: 0,
            fatsGoal: 0,
            fiberGoal: 0,
            isSuccess: false,
            sucMsg: '',
            sucTitle: '',
            isInternet: false,
            premium_msg: '',
            premium_title: '',

        };

        this.onLayout = this.onLayout.bind(this);
        this.callService = this.callService.bind(this);
    }

    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        });
    }

    async callService() {
        this.setState({ loading: true })
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getpcfdata`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data, carbsObj: data.data.carbs, fatsObj: data.data.fats, fibersObj: data.data.fibers, protiensObj: data.data.proteins, premium_msg: data.premium_msg, premium_title: data.premium_title });
                    this.setState({ carbsGoal: parseInt(data.data.carbs.intake_goal), proteinsGoal: parseInt(data.data.proteins.intake_goal), fatsGoal: parseInt(data.data.fats.intake_goal), fiberGoal: parseInt(data.data.fibers.intake_goal) });
                    LogUtils.infoLog1("this.state.resObj.carbs.intake", this.state.resObj.fats)
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: 'Something went wrong please try again later' });
            });
    }


    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                this.callService();

                const interval = setInterval(() => {
                    if (!isEmpty(this.state.resObj)) {
                        this.setState({ fill: this.state.carbsObj.percentage, fillFats: this.state.fatsObj.percentage, fillFiber: this.state.fibersObj.percentage, fillProtiens: this.state.protiensObj.percentage })
                        var weights = [], fats = [], fibers = [], protiens = [];
                        if (!isEmpty(this.state.carbsObj)) {
                            this.state.carbsObj.carb_arr.map((item) => {
                                weights.push(item.carb_intake);
                            })
                        }
                        if (!isEmpty(this.state.fatsObj)) {
                            this.state.fatsObj.fat_arr.map((item) => {
                                fats.push(item.fat_intake);
                            })
                        }
                        if (!isEmpty(this.state.fibersObj)) {
                            this.state.fibersObj.fiber_arr.map((item) => {
                                fibers.push(item.protein_intake);
                            })
                        }
                        if (!isEmpty(this.state.protiensObj)) {
                            this.state.protiensObj.protein_arr.map((item) => {
                                protiens.push(item.protein_intake);
                            })
                        }
                        var conf = {
                            chart: {
                                type: 'column',
                                height: 150,
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            colors: ['#8c52ff'],
                            xAxis: {
                                visible: false,
                                // categories: [
                                //     '*',
                                //     '',
                                //     '6AM',
                                //     '',
                                //     '',
                                //     '',
                                //     '',
                                //     '12PM',
                                //     '',
                                //     '',
                                //     '',
                                //     '6PM',
                                //     '',
                                //     '',
                                //     '*'
                                // ],
                                crosshair: true,
                                lineWidth: 0,
                                minorGridLineWidth: 0,
                                lineColor: 'transparent',
                            },
                            yAxis: {
                                visible: false,
                                title: false,
                                labels: {
                                    enabled: false,
                                },
                            },
                            tooltip: {
                                // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                //     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                // footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.1,
                                    borderWidth: 0,
                                    borderRadius: 3,
                                    showInLegend: false,
                                },
                                allowPointSelect: false,
                            },
                            series: [{
                                enableMouseTracking: false,
                                name: '',
                                data: weights
                                // data: [150, 110, 28, 250, 98, 100, 230]

                            }]

                        }


                        LogUtils.infoLog(weights)
                        this.setState({ chartOptions: conf });
                        this.setState({ stepArr: weights, fatsArr: fats, fibersArr: fibers, protiensArr: protiens });
                        clearInterval(interval);
                    }
                }, 100);

            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }



    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', sucTitle: '' });
        this.callService();
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.callService();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

    }



    async onBackPressed() {
        try{
            if (this.state.dialyGoalVisible) {
                this.setState({ dialyGoalVisible: false })
            } else {
                if (this.props.from === 'rewards' || this.props.from === 'feed' ) {
                    Actions.pop();
                }
                else if (this.props.from === 'traineeHome' ) {
                    Actions.popTo('traineeHome');// //traineeHome //trackershome
                }else {
                    Actions.popTo('trackershome');
                }
            }
        }catch(error){
            console.log(error);
        }
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertMsg: '' });

    }

    renderPCFData() {
        if (!isEmpty(this.state.resObj)) {
            //if (this.state.premium_msg === '') {
                return (

                    <TabView
                        navigationState={this.state}
                        renderScene={({ route }) => {
                            switch (route.key) {
                                case 'first':
                                    return this.FirstRoute();
                                case 'second':
                                    return this.SecondRoute();
                                case 'third':
                                    return this.ThirdRoute();
                                case 'fourth':
                                    return this.FourthRoute();
                                default:
                                    return null;
                            }
                        }}
                        // renderScene={SceneMap({
                        //   first: this.FirstRoute.bind(this),
                        //   second: this.SecondRoute.bind(this),
                        //   third: this.ThirdRoute.bind(this),
                        // })}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={initialLayout}
                        style={styles.container}
                        renderTabBar={this._renderTabBar}
                    />

                );
            // } else {
            //     return (
            //         // <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>

            //         <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>


            //             <Image
            //                 source={require('../res/img_no_subscription.png')}
            //                 style={styles.imgNoSuscribe}
            //             />
            //             <Text style={styles.textNodataTitle}>{this.state.premium_title}</Text>


            //             <Text style={styles.textNodata}>{this.state.premium_msg}</Text>


            //         </View>
            //         // </View>

            //     );
            // }
        }

    }

    renderChart(num) {
        if (!isEmpty(this.state.resObj)) {
            const axesSvg = { fontSize: 10, fill: "#2d3142" };
            const verticalContentInset = { top: 10, bottom: 10 };
            const xAxisHeight = 30;

            if (num === 1) {

                return (
                    <View style={{ height: 200, flexDirection: "row", marginTop: 20 }}>
                        <YAxis
                            data={this.state.stepArr}
                            style={{ flex: 0.1, marginBottom: xAxisHeight }}
                            contentInset={verticalContentInset}
                            svg={axesSvg}
                            numberOfTicks={5}
                        />
                        <View style={{ flex: 0.9, }}>
                            <BarChart
                                style={{ flex: 1 }}
                                data={this.state.stepArr}
                                contentInset={verticalContentInset}
                                spacingInner={0.5}
                                // spacing={0.2}
                                svg={{ strokeWidth: 1, fill: '#8c52ff', }}
                                numberOfTicks={5}
                                animate
                                animationDuration={5000}
                            >
                                <Grid direction={Grid.Direction.HORIZONTAL} />

                            </BarChart>
                        </View>
                    </View>
                );

            } else if (num === 2) {

                return (
                    <View style={{ height: 200, flexDirection: "row", marginTop: 20 }}>
                        <YAxis
                            data={this.state.protiensArr}
                            style={{ flex: 0.1, marginBottom: xAxisHeight }}
                            contentInset={verticalContentInset}
                            svg={axesSvg}
                            numberOfTicks={5}
                        />
                        <View style={{ flex: 0.9, }}>
                            <BarChart
                                style={{ flex: 1 }}
                                data={this.state.protiensArr}
                                contentInset={verticalContentInset}
                                spacingInner={0.5}
                                // spacing={0.2}
                                svg={{ strokeWidth: 1, fill: '#8c52ff', }}
                                numberOfTicks={5}
                                animate
                                animationDuration={5000}
                            >
                                <Grid direction={Grid.Direction.HORIZONTAL} />

                            </BarChart>
                        </View>
                    </View>
                );

            } else if (num === 3) {

                return (
                    <View style={{ height: 200, flexDirection: "row", marginTop: 20 }}>
                        <YAxis
                            data={this.state.fatsArr}
                            style={{ flex: 0.1, marginBottom: xAxisHeight }}
                            contentInset={verticalContentInset}
                            svg={axesSvg}
                            numberOfTicks={5}
                        />
                        <View style={{ flex: 0.9, }}>
                            <BarChart
                                style={{ flex: 1 }}
                                data={this.state.fatsArr}
                                contentInset={verticalContentInset}
                                spacingInner={0.5}
                                // spacing={0.2}
                                svg={{ strokeWidth: 1, fill: '#8c52ff', }}
                                numberOfTicks={5}
                                animate
                                animationDuration={5000}
                            >
                                <Grid direction={Grid.Direction.HORIZONTAL} />

                            </BarChart>
                        </View>
                    </View>
                );

            } else if (num === 4) {

                return (
                    <View style={{ height: 200, flexDirection: "row", marginTop: 20 }}>
                        <YAxis
                            data={this.state.fibersArr}
                            style={{ flex: 0.1, marginBottom: xAxisHeight }}
                            contentInset={verticalContentInset}
                            svg={axesSvg}
                            numberOfTicks={5}
                        />
                        <View style={{ flex: 0.9, }}>
                            <BarChart
                                style={{ flex: 1 }}
                                data={this.state.fibersArr}
                                contentInset={verticalContentInset}
                                spacingInner={0.5}
                                // spacing={0.2}
                                svg={{ strokeWidth: 1, fill: '#8c52ff', }}
                                numberOfTicks={5}
                                animate
                                animationDuration={5000}
                            >
                                <Grid direction={Grid.Direction.HORIZONTAL} />

                            </BarChart>
                        </View>
                    </View>
                );

            }


        }
        else {
            return (
                <View style={{
                    alignItems: 'center',
                    width: wp('90%'),
                    height: 150,
                    alignSelf: 'center',
                }}></View>
            );
        }
    }

    FirstRoute = () => (
        <ScrollView 
        //overScrollMode={true}
        contentContainerStyle={{ paddingBottom: hp('12%'), backgroundColor: '#ffffff' }}
        >
            <View style={styles.viewWalkChart}>

                <View style={styles.calViewDet}>
                    <View style={{
                        width: 130, height: 130,
                        position: 'absolute',
                        top: 30,
                        borderWidth: 3,
                        borderRadius: 65,
                        borderColor: '#e9e9e9',
                        borderStyle: 'dotted',
                    }}>

                    </View>
                    <AnimatedCircularProgress
                        size={160}
                        width={7}
                        fill={this.state.fill}
                        tintColor="#8c52ff"
                        arcSweepAngle={290}
                        rotation={215}
                        lineCap="round"
                        backgroundColor="#e9e9e9">
                        {
                            (fill) => (
                                <View>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_carbs.png')}
                                        style={{
                                            width: 32,
                                            height: 32,
                                            alignSelf: 'center',
                                        }}
                                    />
                                    <Text style={styles.calTitleBig}> {this.state.fill}%</Text>
                                    <Text style={styles.calDesc}>of daily goals</Text>
                                </View>

                            )
                        }
                    </AnimatedCircularProgress>

                    <TouchableOpacity style={styles.calViewDetails}>
                        <Text style={styles.calDetail}></Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.carbsObj.intake} grams
                                </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Carbs Intake
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        {/* onPress={() => this.setState({ carbsGoal: parseInt(this.state.carbsObj.intake_goal), dialyGoalVisible: true, goalType: 1 })}> */}
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={styles.mltextStyle}>
                                {this.state.carbsObj.intake_goal} grams
                        </Text>
                            {/* <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_edit_food.png')}
                                style={{ width: 15, height: 15, margin: 5, }}
                            /> */}
                        </View>

                        <Text style={styles.waterdranktextStyle}>
                            Daily Goal
                                </Text>
                    </View>
                </View>

                <View style={{ height: 10, backgroundColor: '#e9e9e9', marginTop: 20, }}></View>
                {/* bar chat and details    */}
                <View style={styles.viewChartBack}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={styles.statisticTextStyle}>Weekly Carbs Intake</Text>
                    </View>
                    {this.renderChart(1)}
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 0.08, }}/>
                        <View style={{ flexDirection: 'row', marginTop: 10, flex: 1, alignSelf: 'flex-end', justifyContent: 'flex-end', backgroundColor: 'ddd' }}>
                            <FlatList
                                data={this.state.carbsObj.carb_arr}
                                numColumns={7}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                                style={{ alignSelf: 'flex-end' }}
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    const date = item.cdate;
                                    return <View key={date} style={{ flexDirection: 'column', width: wp('12%'), marginLeft: 1, marginRight: 1, }}>

                                        <Text style={styles.chartextStyle}>
                                            {moment(date, 'DD-MM-YYYY').format("MMM D")}
                                        </Text>
                                    </View>
                                }} />
                        </View>
                    </View>
                </View>
                <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>

                <Text style={styles.insighttextStyle}>
                    Daily Carbs Intake
                        </Text>
                <View style={styles.horLineView}>
                </View>
                <FlatList
                    data={this.state.carbsObj.lw_carb_arr}
                    extraData={this.state}
                    keyExtractor={(item,index) => "lw_carb_arr"+index}
                    // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        return <View >
                            <View style={styles.performanceRow}>

                                <Image
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 10,
                                        marginTop: 5,
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        {item.cdate}
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        {item.full_day_name}
                                    </Text>

                                </View>

                                <Text style={styles.performancePercenttextStyle}>
                                    {item.carb_intake} grams
                                    </Text>

                            </View>
                            <View style={styles.horLineView}>
                            </View>
                        </View>
                    }} />


            </View>


        </ScrollView>
    );



    SecondRoute = () => (
        <ScrollView contentContainerStyle={{ paddingBottom: hp('12%'), backgroundColor: '#ffffff' }}>
            <View style={styles.viewWalkChart}>

                <View style={styles.calViewDet}>
                    <View style={{
                        width: 130, height: 130,
                        position: 'absolute',
                        top: 30,
                        borderWidth: 3,
                        borderRadius: 65,
                        borderColor: '#e9e9e9',
                        borderStyle: 'dotted',
                    }}>

                    </View>
                    <AnimatedCircularProgress
                        size={160}
                        width={7}
                        fill={this.state.fillProtiens}
                        tintColor="#8c52ff"
                        arcSweepAngle={290}
                        rotation={215}
                        lineCap="round"
                        backgroundColor="#e9e9e9">
                        {
                            (fill) => (
                                <View>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_protein.png')}
                                        style={{
                                            width: 32,
                                            height: 32,
                                            alignSelf: 'center',
                                        }}
                                    />
                                    <Text style={styles.calTitleBig}> {this.state.fillProtiens}%</Text>
                                    <Text style={styles.calDesc}>of daily goals</Text>
                                </View>

                            )
                        }
                    </AnimatedCircularProgress>

                    <TouchableOpacity style={styles.calViewDetails}>
                        <Text style={styles.calDetail}></Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.protiensObj.intake} grams
                            </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Proteins Intake
                            </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        {/* onPress={() => this.setState({ proteinsGoal: parseInt(this.state.protiensObj.intake_goal), dialyGoalVisible: true, goalType: 2 })}> */}
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={styles.mltextStyle}>
                                {this.state.protiensObj.intake_goal} grams
                    </Text>
                            {/* <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_edit_food.png')}
                                style={{ width: 15, height: 15, margin: 5, }}
                            /> */}
                        </View>

                        <Text style={styles.waterdranktextStyle}>
                            Daily Goal
                            </Text>
                    </View>
                </View>

                <View style={{ height: 10, backgroundColor: '#e9e9e9', marginTop: 20, }}></View>
                {/* bar chat and details    */}
                <View style={styles.viewChartBack}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={styles.statisticTextStyle}>Weekly Proteins Intake</Text>
                    </View>
                    {this.renderChart(2)}
                    <View style={{ flexDirection: "row" }}>
                        <View

                            style={{ flex: 0.08, }}

                        />

                        <View style={{ flexDirection: 'row', marginTop: 10, flex: 1, alignSelf: 'flex-end', justifyContent: 'flex-end', backgroundColor: 'ddd' }}>
                            <FlatList
                                data={this.state.protiensObj.protein_arr}
                                numColumns={7}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                                style={{ alignSelf: 'flex-end' }}
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    const date = item.cdate;
                                    return <View key={date} style={{ flexDirection: 'column', width: wp('12%'), marginLeft: 1, marginRight: 1, }}>

                                        <Text style={styles.chartextStyle}>
                                            {moment(date, 'DD-MM-YYYY').format("MMM D")}
                                        </Text>
                                    </View>
                                }} />

                        </View>
                    </View>
                </View>
                <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>

                <Text style={styles.insighttextStyle}>
                    Daily Proteins Intake
                    </Text>
                <View style={styles.horLineView}>
                </View>
                <FlatList
                    data={this.state.protiensObj.lw_protein_arr}
                    extraData={this.state}
                    keyExtractor={(item,index) => "lw_protein_arr"+index}
                    // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        return <View >
                            <View style={styles.performanceRow}>

                                <Image
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 10,
                                        marginTop: 5,
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        {item.cdate}
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        {item.full_day_name}
                                    </Text>

                                </View>

                                <Text style={styles.performancePercenttextStyle}>
                                    {item.protein_intake} grams
                                </Text>

                            </View>
                            <View style={styles.horLineView}>
                            </View>
                        </View>
                    }} />


            </View>


        </ScrollView>
    );




    ThirdRoute = () => (
        <ScrollView contentContainerStyle={{ paddingBottom: hp('12%'), backgroundColor: '#ffffff' }}>
            <View style={styles.viewWalkChart}>

                <View style={styles.calViewDet}>
                    <View style={{
                        width: 130, height: 130,
                        position: 'absolute',
                        top: 30,
                        borderWidth: 3,
                        borderRadius: 65,
                        borderColor: '#e9e9e9',
                        borderStyle: 'dotted',
                    }}>

                    </View>
                    <AnimatedCircularProgress
                        size={160}
                        width={7}
                        fill={this.state.fillFats}
                        tintColor="#8c52ff"
                        arcSweepAngle={290}
                        rotation={215}
                        lineCap="round"
                        backgroundColor="#e9e9e9">
                        {
                            (fill) => (
                                <View>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_macros_fat.png')}
                                        style={{
                                            width: 32,
                                            height: 32,
                                            alignSelf: 'center',
                                        }}
                                    />
                                    <Text style={styles.calTitleBig}> {this.state.fillFats}%</Text>
                                    <Text style={styles.calDesc}>of daily goals</Text>
                                </View>

                            )
                        }
                    </AnimatedCircularProgress>

                    <TouchableOpacity style={styles.calViewDetails}>
                        <Text style={styles.calDetail}></Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.fatsObj.intake} grams
                                </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Fats Intake
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        {/* onPress={() => this.setState({ fatsGoal: parseInt(this.state.fatsObj.intake_goal), dialyGoalVisible: true, goalType: 3 })}> */}
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={styles.mltextStyle}>
                                {this.state.fatsObj.intake_goal} grams
                        </Text>
                            {/* <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_edit_food.png')}
                                style={{ width: 15, height: 15, margin: 5, }}
                            /> */}
                        </View>

                        <Text style={styles.waterdranktextStyle}>
                            Daily Goal
                                </Text>
                    </View>
                </View>

                <View style={{ height: 10, backgroundColor: '#e9e9e9', marginTop: 20, }}></View>
                {/* bar chat and details    */}
                <View style={styles.viewChartBack}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={styles.statisticTextStyle}>Weekly Fats Intake</Text>
                    </View>
                    {this.renderChart(3)}
                    <View style={{ flexDirection: "row" }}>
                        <View

                            style={{ flex: 0.08, }}

                        />

                        <View style={{ flexDirection: 'row', marginTop: 10, flex: 1, alignSelf: 'flex-end', justifyContent: 'flex-end', backgroundColor: 'ddd' }}>
                            <FlatList
                                data={this.state.fatsObj.fat_arr}
                                numColumns={7}
                                extraData={this.state}
                                keyExtractor={(item, index) => "fat_arr"+index.toString()}
                                style={{ alignSelf: 'flex-end' }}
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    const date = item.cdate;
                                    return <View key={date} style={{ flexDirection: 'column', width: wp('12%'), marginLeft: 1, marginRight: 1, }}>

                                        <Text style={styles.chartextStyle}>
                                            {moment(date, 'DD-MM-YYYY').format("MMM D")}
                                        </Text>
                                    </View>
                                }} />

                        </View>
                    </View>
                </View>
                <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>

                <Text style={styles.insighttextStyle}>
                    Daily Fats Intake
                        </Text>
                <View style={styles.horLineView}>
                </View>
                <FlatList
                    data={this.state.fatsObj.lw_fat_arr}
                    extraData={this.state}
                    keyExtractor={item =>item.full_day_name}
                    // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        return <View >
                            <View style={styles.performanceRow}>

                                <Image
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 10,
                                        marginTop: 5,
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        {item.cdate}
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        {item.full_day_name}
                                    </Text>

                                </View>

                                <Text style={styles.performancePercenttextStyle}>
                                    {item.fat_intake} grams
                                    </Text>

                            </View>
                            <View style={styles.horLineView}>
                            </View>
                        </View>
                    }} />


            </View>


        </ScrollView>
    );

    FourthRoute = () => (
        <ScrollView contentContainerStyle={{ paddingBottom: hp('12%'), backgroundColor: '#ffffff' }}>
            <View style={styles.viewWalkChart}>

                <View style={styles.calViewDet}>
                    <View style={{
                        width: 130, height: 130,
                        position: 'absolute',
                        top: 30,
                        borderWidth: 3,
                        borderRadius: 65,
                        borderColor: '#e9e9e9',
                        borderStyle: 'dotted',
                    }}>

                    </View>
                    <AnimatedCircularProgress
                        size={160}
                        width={7}
                        fill={this.state.fillFiber}
                        tintColor="#8c52ff"
                        arcSweepAngle={290}
                        rotation={215}
                        lineCap="round"
                        backgroundColor="#e9e9e9">
                        {
                            (fill) => (
                                <View>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_fiber.png')}
                                        style={{
                                            width: 32,
                                            height: 32,
                                            alignSelf: 'center',
                                        }}
                                    />
                                    <Text style={styles.calTitleBig}> {this.state.fillFiber}%</Text>
                                    <Text style={styles.calDesc}>of daily goals</Text>
                                </View>

                            )
                        }
                    </AnimatedCircularProgress>

                    <TouchableOpacity style={styles.calViewDetails}>
                        <Text style={styles.calDetail}></Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.fibersObj.intake} grams
                                </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Fiber Intake
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        {/* // onPress={() => this.setState({ fiberGoal: parseInt(this.state.fibersObj.intake_goal), dialyGoalVisible: true, goalType: 4 })}> */}
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={styles.mltextStyle}>
                                {this.state.fibersObj.intake_goal} grams
                        </Text>
                            {/* <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_edit_food.png')}
                                style={{ width: 15, height: 15, margin: 5, }}
                            /> */}
                        </View>

                        <Text style={styles.waterdranktextStyle}>
                            Daily Goal
                                </Text>
                    </View>
                </View>

                <View style={{ height: 10, backgroundColor: '#e9e9e9', marginTop: 20, }}></View>
                {/* bar chat and details    */}
                <View style={styles.viewChartBack}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={styles.statisticTextStyle}>Weekly Fiber Intake</Text>
                    </View>
                    {this.renderChart(4)}
                    <View style={{ flexDirection: "row" }}>
                        <View

                            style={{ flex: 0.08, }}

                        />

                        <View style={{ flexDirection: 'row', marginTop: 10, flex: 1, alignSelf: 'flex-end', justifyContent: 'flex-end', backgroundColor: 'ddd' }}>
                            <FlatList
                                data={this.state.fibersObj.fiber_arr}
                                numColumns={7}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                                style={{ alignSelf: 'flex-end' }}
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    const date = item.cdate;
                                    return <View key={date} style={{ flexDirection: 'column', width: wp('12%'), marginLeft: 1, marginRight: 1, }}>

                                        <Text style={styles.chartextStyle}>
                                            {moment(date, 'DD-MM-YYYY').format("MMM D")}
                                        </Text>
                                    </View>
                                }} />

                        </View>
                    </View>
                </View>
                <View style={{ height: 10, backgroundColor: '#e9e9e9', }}></View>

                <Text style={styles.insighttextStyle}>
                    Daily Fiber Intake
                        </Text>
                <View style={styles.horLineView}>
                </View>
                <FlatList
                    data={this.state.fibersObj.lw_fiber_arr}
                    extraData={this.state}
                    keyExtractor={item => item.id}
                    // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => {
                        return <View >
                            <View style={styles.performanceRow}>

                                <Image
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 10,
                                        marginTop: 5,
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        {item.cdate}
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        {item.full_day_name}
                                    </Text>

                                </View>

                                <Text style={styles.performancePercenttextStyle}>
                                    {item.protein_intake} grams
                                    </Text>

                            </View>
                            <View style={styles.horLineView}>
                            </View>
                        </View>
                    }} />


            </View>


        </ScrollView>
    );

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return (
            <TabBar
                style={{ backgroundColor: '#FFFFFF', elevation: 0, borderColor: '#fff', borderBottomWidth: 1, }}
                labelStyle={styles.textPlan}
                {...props}
                indicatorStyle={{ backgroundColor: '#8c52ff', height: 3 }}

                renderLabel={({ route }) => (
                    <View>
                        <Text style={{
                            fontFamily: 'Rubik-Medium',
                            alignSelf: "center",
                            fontSize: 12,
                            fontWeight: '500',
                            lineHeight: 18,
                            color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                '#8c52ff' : '#6D819C'
                        }}>
                            {route.title}
                        </Text>
                    </View>
                )}
            />
        );
    };

    _renderLabel = props => ({ route, index }) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        const outputRange = inputRange.map(
            inputIndex => (inputIndex === index ? '#D6356C' : '#222')
        );
        const color = props.position.interpolate({
            inputRange,
            outputRange,
        });

        return (
            <Animated.Text style={[styles.label, { color }]}>
                {route.title}
            </Animated.Text>
        );
    };

    async updateDailyGoal() {
        this.setState({ dialyGoalVisible: false });
        let goalValue = '', goalId = 0;
        if (this.state.goalType === 1) {
            goalValue = this.state.carbsGoal;
            goalId = 4;
        } else if (this.state.goalType === 2) {
            goalValue = this.state.proteinsGoal;
            goalId = 1;
        } else if (this.state.goalType === 3) {
            goalValue = this.state.fatsGoal;
            goalId = 2;
        } else if (this.state.goalType === 4) {
            goalValue = this.state.fiberGoal;
            goalId = 3;
        }
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/updatepcfdata`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

                body: JSON.stringify({
                    type_id: goalId,
                    intake_val: goalValue,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, sucMsg: data.message, sucTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    renderDailyGoalText() {

        if (this.state.goalType === 1) {
            return (
                <View>
                    <Image
                        resizeMethod="resize"
                        source={require('../res/ic_carbs.png')}
                        style={{
                            width: 100,
                            height: 100,
                            alignSelf: 'center',
                            marginTop: 25,
                            marginBottom: 25,
                        }}
                    />
                    <Text style={styles.weighttextStyle}>
                        Let's set your carbs intake goal for the day.
                    </Text>
                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>



                        <TouchableOpacity onPress={() => {
                            if (this.state.carbsGoal > this.state.carbsObj.min_range) {
                                this.setState({ carbsGoal: Math.floor(this.state.carbsGoal - 10, 1) });
                            }

                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_minus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={styles.textWeight}>{this.state.carbsGoal}</Text>
                            <Text style={styles.waterdranktextStyle}> carbs per day </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                if (this.state.carbsGoal < this.state.carbsObj.max_range) {
                                    this.setState({ carbsGoal: Math.floor(this.state.carbsGoal + 10, 1) });
                                }
                            }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>



                    </View>
                </View>

            );
        } 
        else if (this.state.goalType === 2) {
            return (
                <View>
                    <Image
                        resizeMethod="resize"
                        source={require('../res/ic_protein.png')}
                        style={{
                            width: 100,
                            height: 100,
                            alignSelf: 'center',
                            marginTop: 25,
                            marginBottom: 25,
                        }}
                    />
                    <Text style={styles.weighttextStyle}>
                        Let's set your proteins intake goal for the day.
                    </Text>
                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>



                        <TouchableOpacity onPress={() => {
                            if (this.state.proteinsGoal > this.state.protiensObj.min_range) {
                                this.setState({ proteinsGoal: Math.floor(this.state.proteinsGoal - 10, 1) });
                            }

                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_minus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={styles.textWeight}>{this.state.proteinsGoal}</Text>
                            <Text style={styles.waterdranktextStyle}> proteins per day </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                if (this.state.proteinsGoal < this.state.protiensObj.max_range) {
                                    this.setState({ proteinsGoal: Math.floor(this.state.proteinsGoal + 10, 1) });
                                }
                            }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>



                    </View>
                </View>

            );
        }
        else if (this.state.goalType === 3) {
            return (
                <View>
                    <Image
                        resizeMethod="resize"
                        source={require('../res/ic_macros_fat.png')}
                        style={{
                            width: 100,
                            height: 100,
                            alignSelf: 'center',
                            marginTop: 25,
                            marginBottom: 25,
                        }}
                    />
                    <Text style={styles.weighttextStyle}>
                        Let's set your fats intake goal for the day.
                    </Text>
                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>



                        <TouchableOpacity onPress={() => {
                            if (this.state.fatsGoal > this.state.fatsObj.min_range) {
                                this.setState({ fatsGoal: Math.floor(this.state.fatsGoal - 10, 1) });
                            }

                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_minus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={styles.textWeight}>{this.state.fatsGoal}</Text>
                            <Text style={styles.waterdranktextStyle}> fats per day </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                if (this.state.fatsGoal < this.state.fatsObj.max_range) {
                                    this.setState({ fatsGoal: Math.floor(this.state.fatsGoal + 10, 1) });
                                }
                            }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>



                    </View>
                </View>

            );
        }
        else if (this.state.goalType === 4) {
            return (
                <View>
                    <Image
                        resizeMethod="resize"
                        source={require('../res/ic_fiber.png')}
                        style={{
                            width: 100,
                            height: 100,
                            alignSelf: 'center',
                            marginTop: 25,
                            marginBottom: 25,
                        }}
                    />
                    <Text style={styles.weighttextStyle}>
                        Let's set your fiber intake goal for the day.
                    </Text>

                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>



                        <TouchableOpacity onPress={() => {
                            if (this.state.fiberGoal > this.state.fibersObj.min_range) {
                                this.setState({ fiberGoal: Math.floor(this.state.fiberGoal - 10, 1) });
                            }

                        }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_minus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={styles.textWeight}>{this.state.fiberGoal}</Text>
                            <Text style={styles.waterdranktextStyle}> fiber per day </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                if (this.state.fiberGoal < this.state.fibersObj.max_range) {
                                    this.setState({ fiberGoal: Math.floor(this.state.fiberGoal + 10, 1) });
                                }
                            }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={styles.imgAddDelete}
                            />
                        </TouchableOpacity>



                    </View>
                </View>

            );
        }

    }



    render() {
        return (
            <KeyboardAvoidingView onLayout={this.onLayout} keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Macros</Text>
                </View>

                {this.renderPCFData()}



                <Dialog
                    onDismiss={() => {
                        this.setState({ dialyGoalVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ dialyGoalVisible: false });
                    }}
                    width={0.8}
                    visible={this.state.dialyGoalVisible}
                    // rounded
                    // actionsBordered
                    // dialogTitle={
                    //     <DialogTitle
                    //         title="Default Animation Dialog Simple"
                    //         style={{
                    //             backgroundColor: '#8c52ff',
                    //         }}
                    //         hasTitleBar={false}
                    //         align="left"
                    //     />
                    // }
                    footer={
                        <DialogFooter style={{
                            backgroundColor: '#ffffff',
                            alignItems: 'center',
                            alignSelf: 'center',
                            borderRadius: 15,
                        }}>
                            <TouchableOpacity onPress={() => this.updateDailyGoal()}>
                                <Text style={styles.textSave}>Make this my goal</Text>
                            </TouchableOpacity>

                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            // backgroundColor: '#8c52ff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>


                                {this.renderDailyGoalText()}




                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
                <CustomDialog
                    visible={this.state.isSuccess}
                    title={this.state.sucTitle}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />


            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // marginTop: StatusBar.currentHeight,
        backgroundColor: '#fff'
    },
    scene: {
    },

    containerStyle: {
        width: '100%',
        height: sliderheight,
        backgroundColor: '#ffffff',
        flex: 1,
    },

    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },

    tabBar: {
        flexDirection: 'row',
    },
    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 11,
        fontWeight: '500',
    },



    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textColorStyle: {
        fontSize: 20,
        marginRight: 5,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    calViewDet: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 15,
        justifyContent: 'center'
    },
    calTitle: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calDesc: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
    },
    calViewDetails: {
        width: 65,
        borderRadius: 100,
        borderStyle: 'solid',
        padding: 5,
        marginTop: -35,
        backgroundColor: '#ffffff',
    },
    calTitleBig: {
        fontSize: 28,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
        marginTop: 8,
    },
    viewCal: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    mltextStyle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    dialyGoaltextStyle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    verLineView: {
        borderWidth: 1,
        borderColor: '#ddd',
    },
    viewChartBack: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 15,
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },

    statisticTextStyle: {
        fontSize: 18,
        fontWeight: '500',
        color: '#2d3142',
        alignSelf: 'flex-start',
        flex: 1,
        fontFamily: 'Rubik-Medium',
    },
    insighttextStyle: {
        fontSize: 18,
        marginTop: 5,
        fontWeight: '500',
        color: '#2d3142',
        padding: 20,
        fontFamily: 'Rubik-Medium',
    },
    chartextStyle: {
        fontSize: 10.7,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
    },
    performanceRow: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-start',
        marginLeft: 5,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 20,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    performancePercenttextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        marginRight: 30,
    },
    horLineView: {
        borderWidth: .5,
        borderColor: '#ddd',
        marginTop: 15,
        marginBottom: 15,

    },
    weighttextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
    },
    sliderImageStyle: {
        width: wp('85%'),
        height: hp('5%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },

    textSave: {
        width: '100%',
        fontSize: 11,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#8c52ff',
        marginBottom: 15,
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        overflow: 'hidden',
    },
    imgAddDelete: {
        width: 25,
        height: 25,
        marginRight: 15,
        marginLeft: 15,
        alignSelf: "center",
        tintColor: '#8c52ff',
    },
    textWeight: {
        alignSelf: 'center',
        color: '#2d3142',
        fontSize: 24,
        fontWeight: '400',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    viewWalkChart: {
        width: '100%',
        flexDirection: 'column',
        // padding: 20,
        backgroundColor: '#ffffff',
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        // padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },

});

export default (Trainee_PCFDetails);
