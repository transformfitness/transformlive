import React, { Component, PureComponent } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { HomeDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import AppIntroSlider from 'react-native-app-intro-slider';
import { BASE_URL } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import ProgressCircle from 'react-native-progress-circle';
import { getFoodDetailsAction, deleteFoodItemAction, getSelFoodItemDetailsAction,setSliderIndexAction } from '../actions/traineeActions';
import moment from "moment";

class Trainee_Home_Food_New extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            fill1: 0,
            fill2: 0,
            fill3: 0,
            loading: false,
            isAlert: false,
            alertMsg: '',
            nolabel: '',
            yeslabel: '',
            resObj: '',
            resArray: [],
            dataGot: false,
            slideindex: 0,
            uf_id: 0,
            selDate: '',

            selFoodTitle: '',
            selFoodType: 0,
            selFoodId: 0,
            selFoodDate: '',
            selFoodUfId: 0,
            selFoodQnty: '',
            selFoodMeasurement: '',
            selFoodFsId: 0,
            disableRightArrow: false,
            disableLeftArrow: false
        };
        this.currHeight = 0;
        this.prevHeight = 0;
        this.scrollHeight = 0;
        //this.getFoodDetails = this.getFoodDetails.bind(this);
    }

    async componentDidMount() {
        try {
            allowFunction();
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    //LogUtils.infoLog1("selDate", this.props.mdate);
                    this.setState({ selDate: this.props.mdate });
                    this.getFoodDetails();
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again', yeslabel: 'OK', nolabel: '' });
                }
            });
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {
            console.error('Error in Trainee_Home_Food_New componentDidMount : ', error)
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        // Actions.popTo('traineeHome');
        // Actions.pop();
        Actions.trackershome();
    }

   async goTOSlider(type) {
        try {
            if (type == "left" && this.state.slideindex < 6) {
                await this.props.setSliderIndexAction(this.state.slideindex + 1);
                var Obj = this.props.trainee.traineeLastWkFood.filter((dt,index )=> index == this.props.trainee.slideIndex);
                this.setState({ loading: false, resArray: Obj, dataGot: true });
                this.setState({ slideindex: this.state.slideindex + 1 });
                this.AppIntroSlider.goToSlide(this.state.slideindex + 1);
            } else if (type == "right" && this.state.slideindex != 0) {
                await this.props.setSliderIndexAction(this.state.slideindex - 1);
                var Obj = this.props.trainee.traineeLastWkFood.filter((dt,index )=> index == this.props.trainee.slideIndex);
                this.setState({ loading: false, resArray: Obj, dataGot: true });
                this.setState({ slideindex: this.state.slideindex - 1 });
                this.AppIntroSlider.goToSlide(this.state.slideindex - 1);
            }
        } catch (error) {
            console.log(error);
        }
    }

    async getFoodDetails() {
        try {
            //var date = this.getCurrentDate();
            var date = moment(new Date()).format("YYYY-MM-DD");
            let token = await AsyncStorage.getItem('token');
            let currDate = { "seldate": date };//this.state.selDate
            this.setState({ loading: true });
            await this.props.getFoodDetailsAction(token, currDate).then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    var Obj = this.props.trainee.traineeLastWkFood.filter(dt => dt.dbdate === this.props.mdate);
                    this.setState({ loading: false, resArray: Obj, dataGot: true });
                    var indx = this.props.trainee.traineeLastWkFood.findIndex(dt => dt.dbdate === this.props.mdate);
                    this.setState({ slideindex: this.state.slideindex + indx });
                    this.props.setSliderIndexAction(indx);
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, yeslabel: 'OK', nolabel: '' });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, yeslabel: 'OK', nolabel: '' });
                    }
                }
            }).catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), yeslabel: 'OK', nolabel: '' });
            });
        } catch (error) {
            console.log('Error while getting getFoodDetails : ', error);
        }
    }

    async deleteFoodItem() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            let params = { "uf_id": this.state.uf_id };
            LogUtils.infoLog1(token, JSON.stringify({
                uf_id: this.state.uf_id,
            }));
            await this.props.deleteFoodItemAction(token, params).then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, sucMsg: '', uf_id: 0, dataGot: false, });
                    this.getFoodDetails();

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, yeslabel: 'OK', nolabel: '' });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, yeslabel: 'OK', nolabel: '' });
                    }
                }
            }).catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), yeslabel: 'OK', nolabel: '' });
            });
        } catch (error) {
            console.log('Error in deleteFoodItem : ', error)
        }
    }


    async getSelFoodItemDetails() {
        try {
            this.setState({ loading: true, });
            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog(token, JSON.stringify({
                food_id: this.state.selFoodId,
                fs_id: this.state.selFoodFsId,
                is_update: 1,
            }));
            const params = {
                "food_id": this.state.selFoodId,
                "fs_id": this.state.selFoodFsId,
                "is_update": 1,
            }
            await this.props.getSelFoodItemDetailsAction(token, params).then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.goToFoodDetails({ response: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            }).catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
        } catch (error) {
            console.log('Error in getSelFoodItemDetails : ', error)
        }
    }

    goToFoodDetails({ response }) {
        Actions.traAddFood({ title: this.state.selFoodTitle, foodType: this.state.selFoodType, foodId: this.state.selFoodId, mdate: this.state.selFoodDate, uf_id: this.state.selFoodUfId, qnty: this.state.selFoodQnty, measurement: this.state.selFoodMeasurement, flag: 'Edit', fsId: this.state.selFoodFsId, isUpdate: 1, mainObject: this.props.trainee.selectedFoodDetails });
    }

    renderFoodType = (item1) => {
        if (item1.food_type) {
            if (item1.food_type.length > 20) {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType}>{`${item1.food_type}`}</Text>
                    </View>
                );
            }
            else {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType1}>{`${item1.food_type}`}</Text>
                    </View>
                );
            }

        } else {
            return (
                <View>
                </View>
            );
        }
    }

    renderMealData = (item, obj) => {
        try {
            if (obj) {
                switch (item) {
                    case 1:
                        let arrBreakfast = obj.breakfast.map((item1, i) => {
                            return <TouchableOpacity key={i + "breakfast" + item1.uf_id}

                                style={styles.nutrition1}
                                onLongPress={() => {
                                    this.setState({ isAlert: true, alertMsg: 'Are you sure? You want to delete this item.', yeslabel: 'Yes', nolabel: 'No', uf_id: item1.uf_id, selDate: obj.dbdate });
                                }}
                                onPress={() => {
                                    this.setState({
                                        selFoodTitle: 'Breakfast',
                                        selFoodType: 2,
                                        selFoodId: item1.food_id,
                                        selFoodDate: obj.dbdate,
                                        selFoodUfId: item1.uf_id,
                                        selFoodQnty: item1.qnty,
                                        selFoodMeasurement: item1.measurement,
                                        selFoodFsId: item1.fs_id,
                                    });

                                    this.getSelFoodItemDetails();
                                }}
                            >

                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 15,
                                        marginTop: 4,
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <View style={{ flexDirection: 'row' }}>

                                        <Text style={styles.textNutTitle}>{item1.name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>

                                        {this.renderFoodType(item1)}
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}>{`${item1.cal}`} cal</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_serving.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}> {`${item1.qnty}`} Qty of {`${item1.measurement}`}</Text>
                                    </View>

                                    <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                                </View>
                            </TouchableOpacity>

                        });
                        return (
                            <View>
                                {arrBreakfast}
                            </View>
                        );

                    case 2:
                        let ArrLunch = obj.lunch.map((item1, i) => {
                            return <TouchableOpacity key={i + "lunch" + item1.uf_id} style={styles.nutrition1}
                                onLongPress={() => {
                                    this.setState({ isAlert: true, alertMsg: 'Are you sure? You want to delete this item.', yeslabel: 'Yes', nolabel: 'No', uf_id: item1.uf_id, selDate: obj.dbdate });
                                }}
                                onPress={() => {
                                    this.setState({
                                        selFoodTitle: 'Lunch',
                                        selFoodType: 4,
                                        selFoodId: item1.food_id,
                                        selFoodDate: obj.dbdate,
                                        selFoodUfId: item1.uf_id,
                                        selFoodQnty: item1.qnty,
                                        selFoodMeasurement: item1.measurement,
                                        selFoodFsId: item1.fs_id,
                                    });

                                    this.getSelFoodItemDetails();
                                }}
                            >
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 15,
                                        marginTop: 4,
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.textNutTitle}>{item1.name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                        {this.renderFoodType(item1)}
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}>{`${item1.cal}`} cal</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_serving.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}> {`${item1.qnty}`} Qty of {`${item1.measurement}`}</Text>
                                    </View>

                                    <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                                </View>
                            </TouchableOpacity>

                        });
                        return (
                            <View>
                                {ArrLunch}
                            </View>
                        );

                    case 3:
                        let arrDinner = obj.dinner.map((item1, i) => {
                            return <TouchableOpacity key={i + "dinner" + item1.uf_id} style={styles.nutrition1}
                                onLongPress={() => {
                                    this.setState({ isAlert: true, alertMsg: 'Are you sure? You want to delete this item.', yeslabel: 'Yes', nolabel: 'No', uf_id: item1.uf_id, selDate: obj.dbdate });
                                }}
                                onPress={() => {
                                    this.setState({
                                        selFoodTitle: 'Dinner',
                                        selFoodType: 6,
                                        selFoodId: item1.food_id,
                                        selFoodDate: obj.dbdate,
                                        selFoodUfId: item1.uf_id,
                                        selFoodQnty: item1.qnty,
                                        selFoodMeasurement: item1.measurement,
                                        selFoodFsId: item1.fs_id,
                                    });

                                    this.getSelFoodItemDetails();
                                }}
                            >
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 15,
                                        marginTop: 4,
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.textNutTitle}>{item1.name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                        {this.renderFoodType(item1)}
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}>{`${item1.cal}`} cal</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_serving.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}> {`${item1.qnty}`} Qty of {`${item1.measurement}`}</Text>
                                    </View>

                                    <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                                </View>
                            </TouchableOpacity>
                        });
                        return (
                            <View>
                                {arrDinner}
                            </View>
                        );

                    case 4:
                        let arrSnacks = obj.snacks.map((item1, i) => {
                            return <TouchableOpacity key={i + "snacks" + item1.uf_id} style={styles.nutrition1}
                                onLongPress={() => {
                                    this.setState({ isAlert: true, alertMsg: 'Are you sure? You want to delete this item.', yeslabel: 'Yes', nolabel: 'No', uf_id: item1.uf_id, selDate: obj.dbdate });
                                }}
                                onPress={() => {
                                    this.setState({
                                        selFoodTitle: 'Snacks',
                                        selFoodType: 5,
                                        selFoodId: item1.food_id,
                                        selFoodDate: obj.dbdate,
                                        selFoodUfId: item1.uf_id,
                                        selFoodQnty: item1.qnty,
                                        selFoodMeasurement: item1.measurement,
                                        selFoodFsId: item1.fs_id,
                                    });
                                    this.getSelFoodItemDetails();
                                }}
                            >
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_blue_dot.png')}
                                    style={{
                                        width: 10,
                                        height: 10,
                                        marginLeft: 15,
                                        marginTop: 4,
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.textNutTitle}>{item1.name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                        {this.renderFoodType(item1)}
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginRight: 3, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}>{`${item1.cal}`} cal</Text>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_serving.png')}
                                            style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textNutDesc}> {`${item1.qnty}`} Qty of {`${item1.measurement}`}</Text>
                                    </View>

                                    <View style={{ width: wp('100%'), height: 1, marginTop: 10, marginLeft: -35, backgroundColor: '#f4f6fa', }}></View>
                                </View>
                            </TouchableOpacity>

                        });
                        return (
                            <View>
                                {arrSnacks}
                            </View>
                        );
                    default:
                        break;
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        } else if (this.state.alertMsg === 'Are you sure? You want to delete this item.') {
            this.deleteFoodItem();
        }
        this.setState({ alertMsg: '', nolabel: '', yeslabel: '' });
        this.setState({ isAlert: false });
    }

    async onDeny() {
        this.setState({ isAlert: false, alertMsg: '', nolabel: '', yeslabel: '' });
    }

    _renderItem = ({ item, index }) => {
        return (

            <View style={styles.containerStyle}>
                <ScrollView
                    key={index + "tfood"}
                    ref={ref => this.scrollView = ref}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}
                >
                <View style={styles.mainView}>

                    {/* title and cal details */}
                    <View style={styles.viewTitleCal}>
                        <View style={{
                            flexDirection: 'row',
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.textHeadTitle}> {item.c_date}</Text>
                        </View>

                        <View style={{ marginTop: 25, }} />
                    </View>

                        <View style={styles.viewTitleCal}>

                            {/* title */}
                            <Text style={styles.textStyle}>You have consumed</Text>
                            <View style={styles.todayCalView}>
                                <Text style={styles.textCal}>{item.cal} cal </Text>
                            </View>


                            {/* circular menus */}
                            <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', }}>
                                <View style={{ marginRight: hp('5%') }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.goTOSlider("left");
                                        }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_left.png')}
                                            style={{
                                                width: 40,
                                                height: 40,
                                                alignSelf: 'center',
                                                justifyContent: 'center',
                                                marginLeft: 10,
                                            }}
                                        />
                                    </TouchableOpacity>
                                </View>
                                {/* Circular view  */}
                                <View style={styles.calViewDet}>
                                    <View style={{ position: 'absolute', marginTop: 0 }}>
                                        <ProgressCircle
                                            percent={item.carb_per}
                                            radius={85}
                                            borderWidth={8}
                                            color="#ffa66d"
                                            shadowColor="#f4f6fa"
                                            bgColor="#ffffff"
                                        //containerStyle = {{borderBottomEndRadius:5}}
                                        />
                                    </View>
                                    <View style={{ position: 'absolute', marginTop: 0 }}>
                                        <ProgressCircle
                                            percent={item.protein_per}
                                            radius={72}
                                            borderWidth={7}
                                            color="#8c52ff"
                                            shadowColor="#f4f6fa"
                                            bgColor="#ffffff"
                                        />
                                    </View>
                                    <View style={{ position: 'absolute', marginTop: 0, alignSelf: 'center' }}>
                                        <ProgressCircle
                                            percent={item.fat_per}
                                            radius={60}
                                            borderWidth={6}
                                            color="#40c8bd"
                                            shadowColor="#f4f6fa"
                                            bgColor="#ffffff"
                                        >
                                            <Text style={styles.calTitleBig}>{item.day_cal_per}%</Text>
                                            <Text style={styles.calDesc}>of daily goals</Text>
                                        </ProgressCircle>
                                    </View>
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', marginLeft: hp('5%') }}>
                                    <TouchableOpacity onPress={() => {
                                        this.goTOSlider("right");
                                    }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_right.png')}
                                            style={{
                                                width: 40,
                                                height: 40,
                                                alignSelf: 'center',
                                                justifyContent: 'center',
                                                marginRight: 10,
                                            }}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* circular menus info*/}
                            <View style={styles.viewCalDetails}>
                                <View style={styles.viewCalDetailsInner}>
                                    <View style={styles.viewCrabDot}>
                                    </View>
                                    <Text style={styles.textCrab}>Carb</Text>
                                    <Text style={styles.textCrab1}>{item.crab}g</Text>
                                    <Text style={styles.textPercentage}>{item.carb_per}%</Text>
                                </View>
                                <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                                <View style={styles.viewCalDetailsInner}>
                                    <View style={styles.viewProteinDot}>
                                    </View>
                                    <Text style={styles.textCrab}>Protein</Text>
                                    <Text style={styles.textCrab1}>{item.protein}g</Text>
                                    <Text style={styles.textPercentage}>{item.protein_per}%</Text>
                                </View>
                                <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                                <View style={styles.viewCalDetailsInner}>
                                    <View style={styles.viewFatDot}>
                                    </View>
                                    <Text style={styles.textCrab}>Fat</Text>
                                    <Text style={styles.textCrab1}>{item.fat}g</Text>
                                    <Text style={styles.textPercentage}>{item.fat_per}%</Text>
                                </View>
                            </View>

                        </View>

                        <View style={{ height: 10, backgroundColor: '#f4f6fa', }}></View>

                        {/* breakfast */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 20, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Breakfast</Text>
                                <TouchableOpacity
                                    disabled={this.state.resArray["0"].breakfast.length > 9 ? true : false}
                                    onPress={() => Actions.traFoodList({ title: "Breakfast", foodType: 2, mdate: item.dbdate, })}>
                                    <Text style={[styles.textAdd,
                                    {
                                        color: this.state.resArray["0"].breakfast.length > 9 ? '#d3d3d3' : '#8c52ff'
                                    }]}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(1, item)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Lunch */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 20, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Lunch</Text>
                                <TouchableOpacity
                                    disabled={this.state.resArray["0"].lunch.length > 9 ? true : false}
                                    onPress={() => Actions.traFoodList({ title: "Lunch", foodType: 4, mdate: item.dbdate, })}>
                                    <Text
                                        style={[styles.textAdd, {
                                            color: this.state.resArray["0"].lunch.length > 9 ? '#d3d3d3' : '#8c52ff'
                                        }]}
                                    >{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(2, item)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Dinner */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 20, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Dinner</Text>
                                <TouchableOpacity
                                    disabled={this.state.resArray["0"].dinner.length > 9 ? true : false}
                                    onPress={() => Actions.traFoodList({ title: "Dinner", foodType: 6, mdate: item.dbdate, })}>
                                    <Text
                                        style={[styles.textAdd, {
                                            color: this.state.resArray["0"].dinner.length > 9 ? '#d3d3d3' : '#8c52ff'
                                        }]}
                                    >{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(3, item)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>

                        {/* Snacks */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', marginLeft: 20, marginBottom: 10, }}>
                                <Text style={styles.textBreakfast}>Snacks</Text>
                                <TouchableOpacity
                                    disabled={this.state.resArray["0"].snacks.length > 9 ? true : false}
                                    onPress={() => Actions.traFoodList({ title: "Snacks", foodType: 5, mdate: item.dbdate, })}>
                                    <Text
                                        style={[styles.textAdd, {
                                            color: this.state.resArray["0"].snacks.length > 9 ? '#d3d3d3' : '#8c52ff'
                                        }]}
                                    >{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.nutrition}>
                                {this.renderMealData(4, item)}
                            </View>
                        </View>
                        <View style={{ height: 9, backgroundColor: '#f4f6fa', }}></View>
                </View>
            </ScrollView>

                <HomeDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDeny.bind(this)}
                    no={this.state.nolabel}
                    yes={this.state.yeslabel} />
            </View>
        );
    };

    appIntro() {

    }

    render() {

        return (
            <View style={styles.maincontainerStyle}>
                <Loader loading={this.state.loading} />
                {this.state.dataGot
                    ? (
                        <AppIntroSlider
                            ref={ref => this.AppIntroSlider = ref}
                            style={{ width: wp('100%'), alignSelf: 'center', }}
                            slides={this.state.resArray}
                            // onDone={this._onDone}
                            showSkipButton={false}
                            showDoneButton={false}
                            showNextButton={false}
                            // hidePagination={false}
                            bottomButton={false}
                            dotStyle={{ backgroundColor: 'transparent' }}
                            activeDotStyle={{ backgroundColor: 'transparent' }}
                            // onSkip={this._onSkip}
                            hidePagination={true}
                            extraData={this.state}
                            // onSlideChange={this.slideChange}
                            scrollEnabled={false}
                            renderItem={this._renderItem}
                            //keyExtractor={(item, index) => index.toString()}
                        />
                    )
                    : (
                        <View />
                    )
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
        alignSelf: 'center',

    },
    maincontainerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 15,
        marginRight: 20,
    },
    textStyle: {
        fontSize: 22,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    todayCalView: {
        flexDirection: 'row',
        marginLeft: 40,
        alignSelf: 'center',
        marginRight: 40,
    },
    textCal: {
        fontSize: 22,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    calViewDet: {
        alignItems: 'center',
        marginTop: 15,
        width: 170,
        height: 170,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    calDesc: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
        lineHeight: 18,
    },
    calTitleBig: {
        fontSize: 30,
        fontWeight: '400',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    viewCalDetails: {
        flexDirection: 'column',
        marginTop: 20,
    },
    viewCalDetailsInner: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center',
        marginBottom: 10,
    },
    viewCrabDot: {
        width: 15,
        height: 15,
        backgroundColor: '#ffa66d',
        borderRadius: 4,
    },
    viewProteinDot: {
        width: 15,
        height: 15,
        backgroundColor: '#8c52ff',
        borderRadius: 4,
    },
    viewFatDot: {
        width: 15,
        height: 15,
        backgroundColor: '#40c8bd',
        borderRadius: 4,
    },
    textCrab: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        flex: 0.7,
        marginLeft: 10,
        color: '#282c37',
    },
    textCrab1: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        marginLeft: 10,
        flex: 1,
        color: '#282c37',
    },
    textPercentage: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'right',
        flex: 1,
        color: '#282c37',
    },
    breakfastBg: {
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        marginTop: 20,
        marginRight: 20,
    },
    textBreakfast: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        flex: 1,
        alignSelf: "flex-start",
        fontWeight: '500',
    },
    textAdd: {
        //color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
        marginTop: 5,
    },
    nutrition1: {
        flexDirection: 'row',
        marginTop: 10,
    },
    nutritionInnerView: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
        marginLeft: 10
    },
    textNutTitle: {
        fontSize: 13,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.23,
    },
    textNutDesc: {
        fontSize: 11,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 18,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    textHeadTitle: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textFoodType: {
        fontSize: 10,
        width: wp('25%'),
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textFoodType1: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
});

const mapStateToProps = state => {
    const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    const trainee = state.traineeFood
    return { loading, error, goals, trainerCodes, trainee };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getFoodDetailsAction, deleteFoodItemAction, getSelFoodItemDetailsAction,setSliderIndexAction },
    )(Trainee_Home_Food_New),
);

// export default ProfileCreation;


