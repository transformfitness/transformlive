import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSampleProfilePics, uploadProfile, profileRefresh } from '../actions';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Carousel from 'react-native-snap-carousel';
import ImagePicker from 'react-native-image-picker';
import NetInfo from "@react-native-community/netinfo";
import LogUtils from '../utils/LogUtils.js';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar,
  Dimensions,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import GetLocation from 'react-native-get-location';
import Dialog, {
  DialogContent,
} from 'react-native-popup-dialog';
import DocumentPicker from 'react-native-document-picker';
import { allowFunction } from '../utils/ScreenshotUtils.js';

const sliderWidth = Dimensions.get('window').width;
const itemWidth = 150;

let phno = '', fName = '', lName = '', email = '', gender = '',
  dob = '', height = '', weight = '', allergies = '',
  hProblems = '', actLevel = '', isVeg = '', foodPref = '',
  eqpAcc = '', age = '', goalId = '', trgt_weight = 0,
  goalSubId = '0', workLocId = "", professionId = '',
  fitnessFormId = '', fcmToken = '';

class ProfilePicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      isMaleFemale: -1,
      fileId: 0,
      filepath: {
        data: '',
        uri: '',
      },
      fileData: '',
      fileUri: '',
      fileType: '',
      index: 0,
      selId: 0,
      selPath: '',
      isAlert: false,
      alertMsg: '',
      isInternet: false,
      lat: 0,
      long: 0,
      isAddImgPop: false,
      selObj: {},
    };
  }

  async componentDidMount() {
    try{

      allowFunction();
      StatusBar.setHidden(true);
  
      this.getCurrentLocation();
  
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.props.getSampleProfilePics();
        }
        else {
          this.setState({ isInternet: true });
        }
      });
  
      phno = await AsyncStorage.getItem('phno');
      fName = await AsyncStorage.getItem('firstName');
      lName = await AsyncStorage.getItem('lastName');
      email = await AsyncStorage.getItem('email');
      gender = await AsyncStorage.getItem('genderId');
      dob = await AsyncStorage.getItem('dob');
      height = await AsyncStorage.getItem('height_value');
      weight = await AsyncStorage.getItem('weight_value');
      allergies = await AsyncStorage.getItem('allergyIds');
      hProblems = await AsyncStorage.getItem('medicaiIds');
      actLevel = await AsyncStorage.getItem('activeId');
      isVeg = await AsyncStorage.getItem('isVeg');
      foodPref = await AsyncStorage.getItem('foodPrefId');
      eqpAcc = await AsyncStorage.getItem('eqpIds');
      age = await AsyncStorage.getItem('age');
      goalId = await AsyncStorage.getItem('goalId');
      // languageId = await AsyncStorage.getItem('languageId');
      trgt_weight = await AsyncStorage.getItem('trgt_weight');
      goalSubId = await AsyncStorage.getItem('goalSubId');
      workLocId = await AsyncStorage.getItem('workLocId');
      professionId = await AsyncStorage.getItem('professionId');
      fitnessFormId = await AsyncStorage.getItem('fitnessFormId');
      fcmToken = await AsyncStorage.getItem('fcmToken');
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.isFocused) {
          this.onBackPressed();
        } else {
          this.props.navigation.goBack(null);
        }
        return true;
      });
    }catch(error){
      console.log(error)
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    if (this.state.isAddImgPop) {
      this.setState({ isAddImgPop: false });
    }
    else {
      Actions.pop();
    }
  }

  getCurrentLocation() {
    try {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
      })
        .then(location => {
          console.log(location);
          if (location) {
            this.setState({ lat: location.latitude, long: location.longitude })
          }
        })
        .catch(error => {
          const { code, message } = error;
          console.warn(code, message);
        })
    } catch (error) {
      console.log(error);
    }
  }

  async onButtonPressed() {
    try {
      LogUtils.firebaseEventLog('click', {
        p_id: 113,
        p_category: 'Registration',
        p_name: 'ProfilePicture',
      });

      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let data = JSON.stringify({
            mobileno: phno,
            fname: fName,
            lname: lName,
            email: email,
            gender_id: gender,
            height: height,
            weight: weight,
            dob: '1991-12-12',
            alergy_id: allergies,
            health_id: hProblems,
            activity_id: actLevel,
            is_veg: isVeg,
            food_id: foodPref,
            eqiupment_id: eqpAcc,
            g_id: goalId,
            age: age,
            profile_id: this.props.profilePics[this.state.index].id.toString(),
            trgt_weight: trgt_weight,
            lng_id: 1,
            gt_id: goalSubId,
            pf_id: professionId,
            wl_id: workLocId,
            ff_id: fitnessFormId,
            device_key: fcmToken,
            lat: this.state.lat,
            lng: this.state.long,
          });

          LogUtils.infoLog1('Signup Request', data);
          if (this.state.selId === 99) {
            if (this.props.profilePics[this.state.index].pr_url.includes('http')) {
              this.setState({ isAlert: true, alertMsg: 'capture profile image' });
            }
            else {
              console.log(data,'---data else pp');
              //this.props.uploadProfile(this.props.profilePics[this.state.index].pr_url, data, this.state.fileType);
            }
          }
          else {
            console.log(data,'---data else else pp');
            //this.props.uploadProfile('', data, '');
          }
        }
        else {
          this.setState({ isInternet: true });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  chooseImage = (hey) => {
    try {
      let options = {
        title: 'Select Image',
        // customButtons: [
        //   {
        //     name: 'customOptionKey',
        //     title: 'Choose Document',
        //   },
        // ],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.showImagePicker(options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          // console.log('ImagePicker Error: ', response.error);
        }
        // else if (response.customButton) {
        //   console.log('User tapped custom button: ', response.customButton);
        //   this.pickDocument();
        // } 
        else {

          // console.log('response', JSON.stringify(response));
          // console.log('fileUri', response.uri);
          this.props.profilePics.map((item) => {
            if (item.id === hey.id) {
              item.pr_url = response.uri;
            }
          })

          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  launchCamera = () => {
    try {

      let options = {
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.launchCamera(options, response => {
        console.log('Response = ', response);

        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          // console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          // console.log('User tapped custom button: ', response.customButton);
        } else {
          // console.log('launchCamera fileUri', response.uri);
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
        }
      });
    } catch (error) {
      console.log(error)
    }
  };

  launchImageLibrary = () => {
    try {
      let options = {
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.launchImageLibrary(options, response => {
        // console.log('Response = ', response);

        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          // console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          // console.log('User tapped custom button: ', response.customButton);
        } else {
          // console.log('launchImageLibrary fileUri', response.uri);
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
        }
      });
    } catch (error) {
      console.log(error)
    }
  };

  async onAccept() {
    this.setState({ isAlert: false,alertMsg: '' });
  }

  async onSuccess() {
    try {
      LogUtils.firebaseEventLog('click', {
        p_id: 114,
        p_category: 'Registration',
        p_name: 'Registration Completed Successfully',
      });

      LogUtils.appsFlyerEventLog('registrationsuccess', {
        desc: 'Registration Completed',
      });

      this.props.profileRefresh();
      Actions.traGoalCompl();
    } catch (error) {
      console.log(error);
    }
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
        this.props.getSampleProfilePics();
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  captureImage = () => {
    try {
      let options = {
        title: 'Select Image',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.launchCamera(options, response => {
        if (response.didCancel) {
          LogUtils.infoLog('User cancelled image picker');
        } else if (response.error) {
          LogUtils.infoLog1('ImagePicker Error: ', response.error);
        } else {
          // LogUtils.infoLog1('response', response);
          LogUtils.infoLog1('fileUri', response.uri);
          this.props.profilePics.map((item) => {
            if (item.id === this.state.selObj.id) {
              item.pr_url = response.uri;
            }
          })
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  async pickImagesFromGallery() {
    // Pick a single file
    try {
      const response = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });

      this.props.profilePics.map((item) => {
        if (item.id === this.state.selObj.id) {
          item.pr_url = response.uri;
        }
      })
      this.setState({
        filePath: response,
        fileData: response.data,
        fileUri: response.uri,
        fileType: response.type,
      });

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  async pickImagesFromGalleryIOS() {
    try {
      // Pick a single file
      let options = {
        title: 'Select Image',

        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      ImagePicker.launchImageLibrary(options, response => {
        if (response.didCancel) {
          LogUtils.infoLog('User cancelled image picker');
        } else if (response.error) {
          LogUtils.infoLog1('ImagePicker Error: ', response.error);
        } else {
          // LogUtils.infoLog1('response', response);
          LogUtils.infoLog1('fileUri', response.uri);

          this.props.profilePics.map((item) => {
            if (item.id === this.state.selObj.id) {
              item.pr_url = response.uri;
            }
          })
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
            fileType: response.type,
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentWillReceiveProps(nextProps){
    //console.log(nextProps,'---nextProps');
    // if(nextProps.someValue!==this.props.someValue){
    //   //Perform some operation
    //   this.setState({someState: someValue });
    //   this.classMethod();
    // }
  }


  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

          <Loader loading={this.props.loading} />

          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />

          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <Text style={styles.textIndicator}>6 / 6</Text>
          </View>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>
            <View style={styles.viewCarosel}>
              <Image
                source={require('../res/ic_bottom.png')}
                style={styles.imgTop}
              />
              <Carousel
                layout={'default'}
                ref={(c) => { this._carousel = c; }}
                data={this.props.profilePics}
                style={styles.crStyle}
                renderItem={({ item }) => {
                  if (item.id === 99) {
                    return (
                      <TouchableOpacity onPress={() => {
                        this.setState({ isAddImgPop: true, selObj: item });
                        // this.chooseImage(item)
                      }} style={styles.ovalCopy} >
                        <Image
                          source={{ uri: item.pr_url }}
                          style={styles.imgProfile}
                        />
                      </TouchableOpacity>
                    );
                  }
                  else {
                    return (
                      <View style={styles.ovalCopy}>
                        <Image
                          source={{ uri: item.pr_url }}
                          style={{ width: 60, height: 60, }}
                        />
                      </View>
                    );
                  }
                }
                }
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                //loop={true}
                // loopClonesPerSide={2}
                // useScrollView={true}
                inactiveSlideScale={0.75}
                inactiveSlideOpacity={1}
                activeSlideOffset={2}
                loopClonesPerSide={1}
                containerCustomStyle={styles.slider}
                contentContainerCustomStyle={styles.sliderContentContainer}
                snapToAlignment='center'
                firstItem={0}
                enableSnap={true}
                scrollEnabled={true}
                // activeAnimationType={'spring'}
                // activeAnimationOptions={{
                //     friction: 4,
                //     tension: 40
                // }}
                // swipeThreshold={1}
                onSnapToItem={(index) => {
                  const interval = setInterval(() => {
                    //console.log(" position ", index.toString())
                    //console.log(" sel_id  ", this.props.profilePics[index].id)
                    // console.log(" sel_path  ", this.props.profilePics[index].pr_url)

                    this.setState({ index: index });
                    this.setState({ selId: this.props.profilePics[index].id });
                    // this.setState({ selPath: this.props.profilePics[index].pr_url });
                    clearInterval(interval);
                  }, 100);

                }}
              // onBeforeSnapToItem={(index) => {
              //   const interval = setInterval(() => {
              //     console.log("B position ", index.toString())
              //     console.log(" B sel_id  ", this.props.profilePics[index].id)

              //     this.setState({ index: index });
              //     this.setState({ selId: this.props.profilePics[index].id });
              //     clearInterval(interval);
              //   }, 100);

              // }}
              />
              <Image
                source={require('../res/ic_top.png')}
                style={styles.imgBottom}
              />
            </View>

            <Text style={styles.subtextOneStyle}>Profile Picture</Text>
            <Text style={styles.desc}>You can select photo from one of this emoji or add your own photo as profile picture</Text>



          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onButtonPressed()}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

          <CustomDialog
            visible={this.props.isSuccess}
            title='Congratulations !'
            desc={this.props.sucMsg}
            onAccept={this.onSuccess.bind(this)}
            no=''
            yes='Ok' />

          <Dialog
            onDismiss={() => {
              this.setState({ isAddImgPop: false });
            }}
            onTouchOutside={() => {
              this.setState({ isAddImgPop: false });
            }}
            width={0.7}
            // height={0.5}
            visible={this.state.isAddImgPop}>
            <DialogContent
              style={{
                backgroundColor: '#ffffff'
              }}>
              <View style={{ flexDirection: 'column', padding: 15 }}>
                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                  <View style={{ marginTop: 10, }}></View>
                  <Text style={styles.textAddImgTit}>Add Photo</Text>

                  <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ isAddImgPop: false });
                          this.captureImage();
                        }}>
                        <View style={styles.containerMaleStyle2}>

                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                            source={require('../res/photo.png')}>
                          </Image>

                        </View>
                      </TouchableOpacity>
                      <Text style={styles.desc1}>Camera</Text>
                    </View>
                    <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ isAddImgPop: false });
                          if (Platform.OS === 'android') {
                            this.pickImagesFromGallery();
                          } else {
                            this.pickImagesFromGalleryIOS();
                          }

                        }}>
                        <View style={styles.containerMaleStyle1}>

                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                            source={require('../res/transformation.png')}>
                          </Image>

                        </View>
                      </TouchableOpacity>
                      <Text style={styles.desc1}>Gallery</Text>
                    </View>

                  </View>
                </View>
              </View>
            </DialogContent>
          </Dialog>

        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  back: {
    width: 25,
    height: 18,
    marginTop: 45,
    marginLeft: 20,
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  viewCarosel: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp('5%'),
    height: hp('23%'),
    width: wp('40%'),
    // marginLeft: wp('2%'),
    // marginRight: wp('2%'),
    backgroundColor: 'transparent',
  },
  ovalCopy: {
    width: 100,
    height: 100,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 50,
    alignSelf: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(64, 117, 205, 0.08)',
    shadowOffset: { width: 20, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 20,
  },
  imgProfile: {
    width: 60,
    height: 60,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 60 / 2,
    resizeMode: "cover",
    justifyContent: 'flex-end',
  },
  slider: {
    marginTop: 10,
    overflow: 'visible' // for custom animations
  },
  sliderContentContainer: {
    // paddingVertical: 10,
    paddingHorizontal: -5, // for custom animation
  },
  crStyle: {
    // backgroundColor: "#D8D8D8",
  },
  textMoreDet: {
    fontSize: 14,
    fontWeight: '500',
    color: '#8c52ff',
    marginTop: hp('2%'),
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  subtextOneStyle: {
    fontSize: 20,
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    marginTop: 15,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    marginTop: hp('4%'),
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 20,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('1%'),
  },
  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  imgTop: {
    width: 20,
    height: 13,
    alignSelf: 'center'
  },
  imgBottom: {
    width: 20,
    height: 13,
    alignSelf: 'center',
    marginBottom: -20,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 10,
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  textAddImgTit: {
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  containerMaleStyle1: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#fd9c93',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerMaleStyle2: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#8c52ff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  desc1: {
    fontSize: 12,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    marginTop: 5,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
});

const mapStateToProps = state => {
  const { profilePics } = state.masters;
  const loading = state.procre.loading;
  const error = state.procre.error;
  const sucMsg = state.procre.sucMsg;
  // const sucAlert = state.procre.sucAlert;
  const isSuccess = state.procre.isSuccess;
  return { loading, error, profilePics, sucMsg, isSuccess };
};

// export default withNavigationFocus(
//   connect(
//     mapStateToProps,
//     { getSampleProfilePics, uploadProfile, profileRefresh },
//   )(ProfilePicture),
// );



export default  connect(mapStateToProps,{ getSampleProfilePics, uploadProfile, profileRefresh })(ProfilePicture);