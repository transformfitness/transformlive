import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    FlatList,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { CustomDialog, NoInternet, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Dialog, {
    DialogContent,
    DialogFooter,
} from 'react-native-popup-dialog';
import { AreaChart, Grid, YAxis, XAxis } from 'react-native-svg-charts';
import { Circle, Path } from 'react-native-svg';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function round(value, precision) {
    if (Number.isInteger(precision)) {
        var shift = Math.pow(10, precision);
        // Limited preventing decimal issue
        return (Math.round(value * shift + 0.00000000000001) / shift);
    } else {
        return Math.round(value);
    }
}

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_Home_Weight extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            isSuccess: false,
            sucMsg: '',
            sucTitle: '',
            isAlert: false,
            isInternet: false,
            loading: false,
            refreshChart: false,
            alertMsg: '',
            state: '',
            resObj: {},
            curWeight: 0.0,
            chartMonths: [],
            chartWeights: [],
            targetWeight: false,
            weight_target: '',
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getHeightDetails();

                const interval = setInterval(() => {
                    if (!isEmpty(this.state.resObj)) {
                        var months = [];
                        var weights = [];

                        this.state.resObj.weight_chart.map((item) => {
                            months.push(item.month);
                            weights.push(Math.floor(item.weight));

                        })
                        this.setState({ chartMonths: months });
                        this.setState({ chartWeights: weights });
                        this.setState({ curWeight: parseFloat(this.state.resObj.weight_current) })
                        if (this.state.resObj.goal_code === 'WL' || this.state.resObj.goal_code === 'WG' || this.state.resObj.goal_code === 'PP') {
                            this.setState({ weight_target: parseFloat(this.state.resObj.weight_target) })
                        } else {
                            this.setState({ weight_target: this.state.resObj.weight_target })
                        }
                        clearInterval(interval);
                    }
                }, 100);

            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.visible) {
            this.setState({ visible: false });
        } else if (this.state.targetWeight) {
            this.setState({ targetWeight: false });
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async getHeightDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(token);
        fetch(
            `${BASE_URL}/trainee/weightdet`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data });
                    LogUtils.infoLog1('this.state.resObj.weight_chart', data.data.weight_chart);
                    if (this.state.refreshChart) {
                        if (this.state.resObj) {
                            if (this.state.resObj.weight_chart) {
                                const interval = setInterval(() => {
                                    var months = [];
                                    var weights = [];

                                    this.state.resObj.weight_chart.map((item) => {
                                        months.push(item.month);
                                        weights.push(Math.floor(item.weight));

                                    })
                                    this.setState({ chartMonths: months });
                                    this.setState({ chartWeights: weights });

                                    this.setState({ curWeight: parseFloat(this.state.resObj.weight_current), refreshChart: false })
                                    if (this.state.resObj.goal_code === 'WL' || this.state.resObj.goal_code === 'WG' || this.state.resObj.goal_code === 'PP') {
                                        this.setState({ weight_target: parseFloat(this.state.resObj.weight_target) })
                                    } else {
                                        this.setState({ weight_target: this.state.resObj.weight_target })
                                    }
                                    clearInterval(interval);
                                }, 1000);
                            }
                        }
                    }


                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })

            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });

            });

    };

    async callService() {
        this.setState({ visible: false });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/user/updateweight`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    weight: this.state.curWeight,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, sucMsg: data.message, sucTitle: data.title, refreshChart: true });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async updateTargetWeight() {
        this.setState({ targetWeight: false });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/updatetargetweight`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    target_weight: this.state.weight_target,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ isSuccess: true, sucMsg: data.message, sucTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }
    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', sucTitle: '' });
        this.getHeightDetails();
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, refreshChart: true });
                this.getHeightDetails();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    renderChart() {
        if (!isEmpty(this.state.resObj)) {

            const data = this.state.chartWeights;
            const data1 = this.state.chartMonths;
            const axesSvg = { fontSize: 10, fill: 'grey' };
            const verticalContentInset = { top: 10, bottom: 10 }
            const xAxisHeight = 30

            const Decorator = ({ x, y, data }) => {
                return data.map((value, index) => (
                    <Circle
                        key={index}
                        cx={x(index)}
                        cy={y(value)}
                        r={3}
                        stroke={'#8c52ff'}
                        fill={'#8c52ff'}
                    />
                ))
            }

            const Line = ({ line }) => (
                <Path
                    d={line}
                    stroke={'#8c52ff'}
                    fill={'none'}
                />
            )

            return (

                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <YAxis
                        data={data}
                        style={{ marginBottom: xAxisHeight }}
                        contentInset={verticalContentInset}
                        svg={axesSvg}
                        numberOfTicks={3}
                    />
                    <View style={{ flex: 1, marginLeft: 10 }}>
                        <AreaChart
                            style={{ height: 200 }}
                            data={data}
                            svg={{ fill: '#8c52ff' }} //rgba(88, 86, 214, 0.7)
                            contentInset={{ left: 10, top: 20, bottom: 30, right: 10 }}
                            numberOfTicks={3}
                            animate
                            animationDuration={300}
                        >
                            <Grid />
                            <Line />
                            <Decorator />
                        </AreaChart>
                        <XAxis
                            style={{ marginTop: 10, height: xAxisHeight }}
                            data={data1}
                            formatLabel={(value, index) => data1[index]}
                            contentInset={{ left: 15, right: 15 }}
                            svg={axesSvg}
                        />
                    </View>

                </View>
            );
        }
        else {
            return (
                <View style={{
                    alignItems: 'center',
                    marginTop: 25,
                    height: 180,
                    alignSelf: 'center',
                }}></View>
            );
        }
    }

    renderDynanmicInfo() {
        if (!isEmpty(this.state.resObj)) {
            if (this.state.resObj.weight_percnt !== '0' && this.state.resObj.weight_loss !== 'NA') {
                if (parseInt(this.state.resObj.weight_loss) > 0) {
                    return (
                        <View>
                            <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                            <View style={styles.viewWelldone}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_claps.png')}
                                    style={styles.imgClaps}
                                />
                                <View style={{ flexDirection: 'column', }}>
                                    <View style={{ flexDirection: 'row', }}>
                                        <Text style={styles.textGoodJob}>Good job you have a</Text>
                                        <Text style={styles.textAchieve}> {this.state.resObj.weight_percnt}% {this.state.resObj.weight_gainloss}</Text>
                                    </View>
                                    <Text style={styles.textGoodJob}>compare to last month</Text>
                                </View>
                            </View>
                        </View>
                    );
                }
            }
            else {
                return (
                    <View></View>
                );
            }
        }

    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <Loader loading={this.state.loading} />
                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />
                <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>
                    <View style={styles.mainView}>
                        {/* title, weight  and chart details */}
                        <View style={styles.viewTitleCal}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                            <View style={{ marginTop: 20, }} />
                            {/* title */}
                            <Text style={styles.textStyle}>Progress</Text>
                            <View style={styles.viewHeightDet}>
                                <View style={styles.viewHeight}>
                                    <Text style={styles.textHeight}>Start</Text>
                                    <Text style={styles.textHeightValue}>{this.state.resObj.weight_start} kg</Text>
                                </View>
                                {this.state.resObj.is_trgwgt_upd === 1
                                    ?
                                    (
                                        <TouchableOpacity
                                            onPress={() => this.setState({ targetWeight: true })}>
                                            <View style={styles.viewHeight}>
                                                <Text style={styles.textHeight}>Target</Text>
                                                <View style={styles.viewTarWeight}>
                                                    <Text style={styles.textHeightPurpleValue}>{this.state.resObj.weight_target} kg</Text>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_edit_food.png')}
                                                        style={{ width: 15, height: 15, margin: 5, }}
                                                    />
                                                </View>

                                            </View>
                                        </TouchableOpacity>
                                    )
                                    :
                                    (
                                        <View style={styles.viewHeight}>
                                            <Text style={styles.textHeight}>Target</Text>
                                            <Text style={styles.textHeightValue}>{this.state.resObj.weight_target}</Text>
                                        </View>
                                    )}
                                <View style={styles.viewHeight}>
                                    <Text style={styles.textHeight}>{this.state.resObj.weight_text}</Text>
                                    {this.state.resObj.weight_loss === 'NA'
                                        ?
                                        (
                                            <Text style={styles.textHeightValue}>{this.state.resObj.weight_loss}</Text>
                                        )
                                        :
                                        (
                                            <Text style={styles.textHeightValue}>{this.state.resObj.weight_loss} kg</Text>
                                        )}

                                </View>
                            </View>
                            {/* chart */}
                            {this.renderChart()}
                            {this.renderDynanmicInfo()}
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.journalBg}>
                            <View style={{ flexDirection: 'row', }}>
                                <Text style={styles.textJournal}>Journal</Text>
                            </View>
                            <View style={styles.nutrition}>
                                <FlatList
                                    contentContainerStyle={{ paddingBottom: hp('5%') }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.resObj.weight_data}
                                    keyExtractor={(item, index) => index.toString()}
                                    ItemSeparatorComponent={this.FlatListItemSeparator}
                                    scrollEnabled={false}
                                    renderItem={({ item, index }) => {
                                        return <View key={index}>
                                            <View style={styles.nutritionInnerView}>
                                                <Text style={styles.textNutTitle}>{item.cdate}</Text>
                                                <Text style={styles.textNutTitle1}>{item.weight}</Text>

                                            </View>
                                            <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                                        </View>

                                    }} />

                            </View>
                        </View>
                    </View>

                    <Dialog
                        onDismiss={() => {
                            this.setState({ targetWeight: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ targetWeight: false });
                        }}
                        width={0.7}
                        visible={this.state.targetWeight}
                        // rounded
                        // actionsBordered
                        // dialogTitle={
                        //     <DialogTitle
                        //         title="Default Animation Dialog Simple"
                        //         style={{
                        //             backgroundColor: '#8c52ff',
                        //         }}
                        //         hasTitleBar={false}
                        //         align="left"
                        //     />
                        // }
                        footer={
                            <DialogFooter style={{
                                backgroundColor: '#ffffff',
                                alignItems: 'center',
                                alignSelf: 'center',
                            }}>
                                <TouchableOpacity onPress={() => this.updateTargetWeight()}>
                                    <Text style={styles.textSave}>Save Changes</Text>
                                </TouchableOpacity>

                            </DialogFooter>
                        }
                    >
                        <DialogContent
                            style={{
                                backgroundColor: '#8c52ff'
                            }}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={{ flexDirection: 'column', padding: 15 }}>
                                    <Text style={{
                                        alignSelf: 'center', color: '#ffffff', fontSize: 11,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 15,
                                        fontFamily: 'Rubik-Medium',
                                        letterSpacing: 0.23,
                                    }}>Update your target weight</Text>
                                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>

                                        <TouchableOpacity onPress={() => {
                                            if (this.state.resObj.goal_code === 'WL' || this.state.resObj.goal_code === 'PP') {
                                                if (this.state.weight_target > 0.1) {
                                                    this.setState({ weight_target: round(this.state.weight_target - 0.1, 1) });
                                                }
                                            } else if (this.state.resObj.goal_code === 'WG') {
                                                if (this.state.weight_target > this.state.resObj.weight_start) {
                                                    this.setState({ weight_target: round(this.state.weight_target - 0.1, 1) });
                                                }
                                            }

                                        }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_minus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>
                                        <Text style={styles.textWeight}>{this.state.weight_target}</Text>
                                        <TouchableOpacity
                                            onPress={() => {
                                                if (this.state.resObj.goal_code === 'WL' || this.state.resObj.goal_code === 'PP') {
                                                    if (this.state.weight_target < this.state.resObj.weight_start) {
                                                        this.setState({ weight_target: round(this.state.weight_target + 0.1, 1) })
                                                    }
                                                } else if (this.state.resObj.goal_code === 'WG') {
                                                    this.setState({ weight_target: round(this.state.weight_target + 0.1, 1) })
                                                }
                                            }

                                            }>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_plus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>

                                    </View>
                                    <Text style={styles.textKgbg}>kg</Text>
                                </View>

                            </View>

                        </DialogContent>
                    </Dialog>

                    <Dialog
                        onDismiss={() => {
                            this.setState({ visible: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ visible: false });
                        }}
                        width={0.7}
                        visible={this.state.visible}
                        // rounded
                        // actionsBordered
                        // dialogTitle={
                        //     <DialogTitle
                        //         title="Default Animation Dialog Simple"
                        //         style={{
                        //             backgroundColor: '#8c52ff',
                        //         }}
                        //         hasTitleBar={false}
                        //         align="left"
                        //     />
                        // }
                        footer={
                            <DialogFooter style={{
                                backgroundColor: '#ffffff',
                                alignItems: 'center',
                                alignSelf: 'center',
                            }}>
                                <TouchableOpacity onPress={() => this.callService()}>
                                    <Text style={styles.textSave}>Save Changes</Text>
                                </TouchableOpacity>

                            </DialogFooter>
                        }
                    >
                        <DialogContent
                            style={{
                                backgroundColor: '#8c52ff'
                            }}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={{ flexDirection: 'column', padding: 15 }}>
                                    <Text style={{
                                        alignSelf: 'center', color: '#ffffff', fontSize: 11,
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        marginTop: 15,
                                        fontFamily: 'Rubik-Medium',
                                        letterSpacing: 0.23,
                                    }}>Update your weight</Text>
                                    <View style={{ marginTop: 20, alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row' }}>

                                        <TouchableOpacity onPress={() => {
                                            if (this.state.curWeight > 0.1) {
                                                this.setState({ curWeight: round(this.state.curWeight - 0.1, 1) });
                                            }

                                        }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_minus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>
                                        <Text style={styles.textWeight}>{this.state.curWeight}</Text>
                                        <TouchableOpacity
                                            onPress={() => this.setState({ curWeight: round(this.state.curWeight + 0.1, 1) })}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_plus.png')}
                                                style={styles.imgAddDelete}
                                            />
                                        </TouchableOpacity>

                                    </View>
                                    <Text style={styles.textKgbg}>kg</Text>
                                </View>

                            </View>

                        </DialogContent>
                    </Dialog>
                </ScrollView>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                    <TouchableOpacity
                        onPress={() => this.setState({ visible: true })}
                        style={styles.buttonTuch}>
                        <Text style={styles.buttonText}>Update Weight</Text>
                    </TouchableOpacity>
                </LinearGradient>
                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <CustomDialog
                    visible={this.state.isSuccess}
                    title={this.state.sucTitle}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        paddingLeft: 20,
        paddingTop: 20,
        backgroundColor: '#ffffff',
        paddingRight: 20,
    },
    textStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
    },
    viewHeightDet: {
        flexDirection: 'row',
        marginTop: 15,
        width: '100%',
        justifyContent: 'flex-start'
    },
    viewHeight: {
        width: wp('33%'),
        flexDirection: 'column',
        justifyContent: 'flex-start',
        marginRight: 10,
    },
    textHeight: {
        fontSize: 11,
        fontWeight: '800',
        fontFamily: 'Rubik-Regular',
        textAlign: 'left',
        color: '#6d819c',
    },
    viewTarWeight: {
        flexDirection: 'row',
    },
    textHeightValue: {
        fontSize: 13,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 2,
        color: '#282c37',
        textAlign: 'left',
    },
    textHeightPurpleValue: {
        fontSize: 15,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 2,
        color: '#8c52ff',
        textAlign: 'left',
    },
    calViewDet: {
        alignItems: 'center',
        marginTop: 25,
        alignSelf: 'center',
    },
    viewHeightDet: {
        flexDirection: 'row',
        marginTop: 15,
        width: '100%',
        justifyContent: 'flex-start'
    },
    container: {
        width: wp('90%'),
        height: 200,
        borderColor: '#000000',
        borderRadius: 2,
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    viewWelldone: {
        flexDirection: 'row',
        margin: 15,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    imgClaps: {
        width: 45,
        height: 45,
        alignSelf: "center",
    },
    textGoodJob: {
        fontSize: 13,
        fontFamily: 'Rubik-Medium',
        fontWeight: '400',
        color: '#2d3142',
        textAlign: 'left',
        marginLeft: 10,
        alignSelf: 'flex-start',
        letterSpacing: 0.23,
    },
    textAchieve: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-start",
        fontSize: 13,
        textAlign: 'left',
        marginRight: 20,
        fontWeight: '500',
        letterSpacing: 0.23,
    },
    journalBg: {
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        padding: 20,
    },
    textJournal: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        flex: 1,
        alignSelf: "flex-start",
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
    },
    nutritionInnerView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        marginTop: 10,
        marginBottom: 10,
        flex: 1,
    },
    textNutTitle: {
        fontSize: 11,
        fontWeight: '400',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textNutTitle1: {
        fontSize: 12,
        fontWeight: '400',
        color: '#2d3142',
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    imgAddDelete: {
        width: 25,
        height: 25,
        marginRight: 15,
        marginLeft: 15,
        alignSelf: "center",
        tintColor: '#ffffff',
    },
    textWeight: {
        alignSelf: 'center',
        color: '#ffffff',
        fontSize: 38,
        fontWeight: '400',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textKgbg: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#ffffff',
        borderRadius: 8,
        paddingTop: 2,
        marginTop: 20,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
    },
    textSave: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#000000',
        marginTop: 15,
        marginBottom: 15,
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        overflow: 'hidden',
    },

});

const mapStateToProps = state => {
    // const { profdet } = state.procre;
    // const loading = state.procre.loading;
    // const error = state.procre.error;
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_Home_Weight),
);
