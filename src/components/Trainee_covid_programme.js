import React, { Component } from 'react';
import { CustomDialog, Loader, NoInternet } from './common';
import { Actions } from 'react-native-router-flux';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    KeyboardAvoidingView,
    StatusBar,
    FlatList,
    ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils';

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_covid_programme extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    async componentDidMount() {
        StatusBar.setHidden(true);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });

        LogUtils.infoLog1("this.props.breath_tech", this.props.breath_tech);
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    renderBreathingTechniques() {
        if (!isEmpty(this.props.covidObj)) {

            // return (
            //     <View>
            //         {this.props.covidObj.breath_instr.split('|').map((tipDesc, index) => (
            //             <View style={styles.viewBenInclu}>
            //                 <View style={styles.viewWhiteDot}></View>
            //                 <View style={styles.viewBenIncluInner}>
            //                     <Text style={styles.txtDesc}>{tipDesc}</Text>
            //                 </View>
            //             </View>
            //         ))}
            //     </View>
            // );


            let arrBenfitIncludes = this.props.breath_tech.map((item1, i) => {
                return <View key={i} style={styles.viewBenInclu}>
                    <View style={styles.viewWhiteDot}></View>
                    <View style={styles.viewBenIncluInner}>
                        {/* <Text style={styles.txt14}>{item1.title}</Text> */}
                        <Text style={styles.txtDesc}>{item1.description}</Text>
                    </View>
                </View>
            });
            return (
                <View>
                    {arrBenfitIncludes}
                </View>
            );
        }
    }

    renderTodayWorkout() {
        if (this.props.covidObj && !isEmpty(this.props.covidObj)) {

            return (
                <TouchableOpacity activeOpacity={0.7} style={styles.containerListStyle}
                    onPress={() => {
                        Actions.newvideoplay({ videoURL: this.props.covidObj.breath_video });
                    }}>

                    {this.props.covidObj.breath_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: this.props.covidObj.breath_img }}
                                style={styles.todayImage}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={styles.todayImage}
                            />
                        )
                    }

                    <View style={styles.playContainer1}>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={{ flexDirection: 'row' }}
                            onPress={() => {
                                Actions.newvideoplay({ videoURL: this.props.covidObj.breath_video });
                            }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_play_purple.png')}
                                style={styles.playIcon} />
                        </TouchableOpacity>
                    </View>

                </TouchableOpacity>
            );
        }
    }



    render() {

        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                scrollEnabled={false}>
                <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                    {/* <Loader loading={this.state.loading} /> */}

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Breathing Techniques</Text>

                    </View>
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('3%') }}>
                        {/* <View style={styles.containerStyle}> */}

                        {this.renderTodayWorkout()}

                        <View style={styles.mainView}>

                            {this.renderBreathingTechniques()}

                        </View>
                        {/* </View> */}
                    </ScrollView>
                </ImageBackground>
            </KeyboardAwareScrollView>
        );
    }

}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    mainView: {
        flexDirection: 'column',
        margin: 20,
    },
    txtPlanName: {
        fontSize: 20,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'center',
    },
    viewBenInclu: {
        flexDirection: 'row',
        marginTop: 20,
    },
    viewBenIncluInner: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 15,
    },
    txtDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        marginRight: 30,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    viewWhiteDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },

    todayImage: {
        width: wp('92%'),
        height: hp('28%'),
        alignSelf: 'center',
        resizeMode: 'cover',
        borderRadius: 6,
    },
    playContainer1: {
        flexDirection: 'row',
        alignContent: 'center',
        position: 'absolute',
        right: 30,
        bottom: 10,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
    },

});



export default Trainee_covid_programme;
