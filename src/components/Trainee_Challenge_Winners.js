import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, NoInternet, CustomDialog } from './common';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import DeviceInfo from 'react-native-device-info';

import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class Trainee_Challenge_Winners extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            loading: false,
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            isAlert: false,
            isInternet: false,
            alertMsg: '',
            noData: '',
            challengeWinners: {},
            arrChallengeWinners: [],
        };
        this.getAllWinners = this.getAllWinners.bind(this);
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllWinners();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    onBackPressed() {
        Actions.pop();
    }

    async getAllWinners() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        // console.log('Url', `${BASE_URL}/trainee/challengeranking`);
        // console.log('Body', JSON.stringify({
        //     c_id: this.props.cid,
        //     page: 1,
        //     pagesize: 10
        // }));
        fetch(
            `${BASE_URL}/trainee/challengeranking`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    c_id: this.props.cid,
                    page: 1,
                    pagesize: 10
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {
                   
                    this.setState({ loading: false, challengeWinners: data.data });
                    
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }
    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllWinners();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    renderWinnersList() {
        if (this.state.challengeWinners) {
            if (Array.isArray(this.state.challengeWinners.winner_arr) && this.state.challengeWinners.winner_arr.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('10%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.challengeWinners.winner_arr}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                return <View style={styles.aroundListStyle} >
                                    <View>
                                        {item.user_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.user_img }}
                                                    style={styles.profileImage}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={styles.profileImage}
                                                />
                                            )
                                        }

                                    </View>
                                    <View style={{ width: wp('80%'), flexDirection: 'column', paddingLeft: 15, paddingRight: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                        
                                        <Text style={styles.textWorkName}>{`${item.user_name}`}</Text>

                                    </View>

                                    <View style={styles.viewOraDot}>
                                        <Text style={styles.textWorkName}>{`${item.rank}`}</Text>
                                    </View>

                                </View>
                            }} />
                    </View>

                );
            }
            else {
                return (

                    <View style={{ flexDirection: 'row', flex: 1, alignSelf: 'center' }}>
                        <Text style={styles.textNodata}>{this.state.noData}</Text>
                    </View>
                );
            }
        }
    }


    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />
                    {/* top */}
                    <View style={{
                        flexDirection: 'row',
                        paddingTop: 15,
                        alignContent: 'center',
                        paddingBottom: 15,
                        backgroundColor: '#F4F6FA',
                    }}>
                        <Text style={styles.textHeadTitle}>Challenge Winners</Text>
                        <TouchableOpacity
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    {this.renderWinnersList()}


                </View>
                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    textHeadTitle: {
        width: wp('100%'),
        fontSize: 18,
        fontWeight: '500',
        position: 'absolute',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    backImageStyle: {
        width: 19,
        marginLeft: 20,
        height: 16,
        marginTop: 3,
        alignSelf: 'center',
    },
    aroundListStyle: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        padding: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    profileImage: {
        width: 45,
        height: 45,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 45 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start'
    },
    textWorkName: {
        fontSize: 15,
        fontWeight: '500',
        lineHeight: 18,
        marginRight: 40,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    viewOraDot: {
        position: 'absolute',
        alignSelf: 'center',
        right: 0,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },

});

const mapStateToProps = state => {

    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_Challenge_Winners),
);

