import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
} from 'react-native';
import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LiveChat from 'react-native-livechat';
import NetInfo from "@react-native-community/netinfo";
import { allowFunction } from '../utils/ScreenshotUtils.js';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            isInternet: false,
            alertMsg: '',
            noDataMsg: '',
            arrAppointments: [],
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {

            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }
    componentWillUnmount() {
        this.backHandler.remove();
        // Actions.pop();
    }

    onBackPressed() {
        Actions.thome();
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllAppointments();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Live Chat</Text>
                    </View>

                    <LiveChat license="11800824" />

                </View>
                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
    },
});

export default Chat;
