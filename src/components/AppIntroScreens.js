import React, { Component } from 'react';
import {
  StyleSheet, View, Text, Image,
} from 'react-native';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { allowFunction } from '../utils/ScreenshotUtils.js';

class AppIntroScreens extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
    };
  }

  async componentDidMount() {
    allowFunction();

  }

  _onDone = () => {
    Actions.splash({ type: 'reset' });
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
  };
  _renderItem = ({ item }) => {
    return (
      <View style={{
      }}>
        <LinearGradient colors={['#3625af', '#b865d3']} style={styles.linearGradient}>

          <View style={{ width: wp('100%'), height: hp('60%'), justifyContent: 'center', alignItems: 'center' }}>
            <Image style={{ width: wp('140%'), height: hp('85%') }} source={require('../res/info_circle.png')} />
            <Image style={{ width: wp('70%'), height: hp('40%'), position: 'absolute', zIndex: 111, }} source={item.image} />
          </View>


          <Text style={styles.title}>{item.title}</Text>


        </LinearGradient>

      </View>
    );
  };

  render() {
    //If false show the Intro Slides
    if (this.state.showRealApp) {
      //Real Application
      return (
        <View>

        </View>
      );
    } else {
      //Intro slides
      return (
        <AppIntroSlider
          slides={slides}
          renderItem={this._renderItem}
          onDone={this._onDone}
          showSkipButton={false}
          onSkip={this._onSkip}
        />
      );
    }
  }

}

const slides = [
  {
    key: 's1',
    title: "GET THE BEST \nWORKOUT PROGRAMS \nFROM TOP FITNESS \nPROFESSIONALS",
    image: require('../res/info_img3.png'),
  },
  {
    key: 's2',
    title: 'FOOD GUIDE AND \nRECOMMENDATIONS FROM \nBEST DIETICIANS',
    image: require('../res/info_img5.png'),
  },
  {
    key: 's3',
    title: 'GUIDED VIDEO \nWORKOUTS HELP \nYOU NAVIGATE',
    image: require('../res/info_img2.png'),
  },
  {
    key: 's4',
    title: "TRACK YOUR \nNUTRITION, \nFITNESS, & \nHEALTH DATA",
    image: require('../res/info_img4.png'),
  },

];

const styles = StyleSheet.create({

  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'

  },
  text: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    paddingVertical: 30,
  },
  title: {
    fontSize: 30,
    color: 'white',
    marginLeft: 20,
    justifyContent: 'flex-end',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
    lineHeight: 35,
    position: 'absolute',
    bottom: 0,
    marginBottom: hp('15%'),
  },

  linearGradient: {
    width: '100%',
    height: '100%',
  },

});

const mapStateToProps = state => {


  return {};

};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    {},

  )(AppIntroScreens),
);

// export default ProfileCreation;