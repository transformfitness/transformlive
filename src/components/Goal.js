import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getTraineeGoalsByGender } from '../actions';
import AsyncStorage from '@react-native-community/async-storage';
import LogUtils from '../utils/LogUtils.js';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import GetLocation from 'react-native-get-location';
import {  uploadProfile, profileRefresh } from '../actions';


let phno = '', fName = '', lName = '', email = '', gender = '', dob = '', height = '', weight = '', allergies = '', hProblems = '', actLevel = "1", isVeg = '',
  foodPref = '', eqpAcc = '', age = '', goalId = '', trgt_weight = 0, goalSubId = '0', workLocId = "", professionId = '', fitnessFormId = '', fcmToken = '';

class Goal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      goals: [],
      goalId: 0,
      fakeContact: [],
      SelectedFakeContactList: [],
      isAlert: false,
      alertMsg: '',
      goalCode: '',
      isInternet: false,
      lat: 0,
      long: 0,
    };
  }

  async componentDidMount() {
    allowFunction();
    StatusBar.setHidden(true);

    this.getCurrentLocation(); //testing

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.props.getTraineeGoalsByGender({ gender });
      }
      else {
        this.setState({ isInternet: true });
      }
    });

    await AsyncStorage.setItem('goalSubId', '0');
    await AsyncStorage.setItem('trgt_weight', '0');

    phno = await AsyncStorage.getItem('phno');
    fName = await AsyncStorage.getItem('firstName');
    lName = await AsyncStorage.getItem('lastName');
    email = await AsyncStorage.getItem('email');
    gender = await AsyncStorage.getItem('genderId');
    dob = await AsyncStorage.getItem('dob');
    height = await AsyncStorage.getItem('height_value');
    weight = await AsyncStorage.getItem('weight_value');
    allergies = await AsyncStorage.getItem('allergyIds');
    hProblems = await AsyncStorage.getItem('medicaiIds');
    //actLevel = await AsyncStorage.getItem('activeId');
    isVeg = await AsyncStorage.getItem('isVeg');
    foodPref = await AsyncStorage.getItem('foodPrefId');
    eqpAcc = await AsyncStorage.getItem('eqpIds');
    age = await AsyncStorage.getItem('age');
    //goalId = await AsyncStorage.getItem('goalId');
    // languageId = await AsyncStorage.getItem('languageId');
    trgt_weight = await AsyncStorage.getItem('trgt_weight');
    goalSubId = await AsyncStorage.getItem('goalSubId');
    workLocId = await AsyncStorage.getItem('workLocId');
    professionId = await AsyncStorage.getItem('professionId');
    fitnessFormId = await AsyncStorage.getItem('fitnessFormId');
    fcmToken = await AsyncStorage.getItem('fcmToken');

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.isFocused) {
        Actions.pop();
      } else {
        this.props.navigation.goBack(null);
      }
      return true;
    });
  }


  getCurrentLocation() {
    try {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
      })
        .then(location => {
          console.log(location);
          if (location) {
            this.setState({ lat: location.latitude, long: location.longitude })
          }
        })
        .catch(error => {
          const { code, message } = error;
          console.warn(code, message);
        })
    } catch (error) {
      console.log(error);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onMobileNumberChanged(text) {
    this.props.mobileNumberChanged(text);
  }

  onBackPressed() {
    Actions.pop();
  }

  async onButtonPressed() {
    try {
      if (this.state.goalId !== 0) {
        await AsyncStorage.setItem('goalId', this.state.goalId.toString());
        goalId = await AsyncStorage.getItem('goalId');

        LogUtils.firebaseEventLog('click', {
          p_id: 105,
          p_category: 'Registration',
          p_name: 'Goal',
        });

        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            let data = JSON.stringify({
              mobileno: phno,
              fname: fName,
              lname: lName,
              email: email,
              gender_id: gender,
              height: height,
              weight: weight,
              dob: '1991-12-12',
              alergy_id: allergies,
              health_id: hProblems,
              activity_id: actLevel,
              is_veg: isVeg,
              food_id: foodPref,
              eqiupment_id: eqpAcc,
              g_id: goalId,
              age: age,
              profile_id: gender == 1 ? "2" : "3",//this.props.profilePics[this.state.index].id.toString(),
              trgt_weight: trgt_weight,
              lng_id: 1,
              gt_id: goalSubId,
              pf_id: professionId,
              wl_id: workLocId,
              ff_id: fitnessFormId,
              device_key: fcmToken,
              lat: this.state.lat,
              lng: this.state.long,
            });

            LogUtils.infoLog1('Signup Request', data);
            this.props.uploadProfile('', data, '');

          }
          else {
            this.setState({ isInternet: true });
          }
        });
      } else {
        this.setState({ isAlert: true, alertMsg: 'Please select your fitness goal' });
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  async onAccept() {
    this.setState({ isAlert: false, alertMsg: '' });
  }

  async onRetry() {
    let gender = await AsyncStorage.getItem('genderId');
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
        this.props.getTraineeGoalsByGender({ gender });
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  async onSuccess() {
    try {
      LogUtils.firebaseEventLog('click', {
        p_id: 114,
        p_category: 'Registration',
        p_name: 'Registration Completed Successfully',
      });

      LogUtils.appsFlyerEventLog('registrationsuccess', {
        desc: 'Registration Completed',
      });

      this.props.profileRefresh();
      //Actions.traGoalCompl();
      Actions.appIntroSliderDemo();
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>

          <Loader loading={this.props.loading} />

          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />

          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <Text style={styles.textIndicator}>3 / 3</Text>
          </View>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>What is your fitness goal?</Text>
            <Text style={styles.desc}>We need the below information to personalise your experience</Text>

            <FlatList
              contentContainerStyle={{ paddingBottom: hp('45%') }}
              data={this.props.goals}
              keyExtractor={item => item.name}
              extraData={this.state}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => {
                return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                  this.press(item)
                }}>
                  <View >
                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                  </View>
                  <View style={styles.mobileImageStyle}>
                    {item.check
                      ? (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )
                      : (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )}
                  </View>
                </TouchableOpacity>
              }} />

          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onButtonPressed()}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />
          {/* Added by ravi varma */}
          <CustomDialog
            visible={this.props.isSuccess}
            title='Congratulations !'
            desc={this.props.sucMsg}
            onAccept={this.onSuccess.bind(this)}
            no=''
            yes='Ok' />
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

  press = (hey) => {
    this.props.goals.map((item) => {
      if (item.id === hey.id) {
        item.check = true;
        this.setState({ goalId: item.id, goalCode: item.code });
      }
      else {
        item.check = false;
      }
    })
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  img: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  back: {
    width: 25,
    height: 18,
    marginTop: 45,
    marginLeft: 20,
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('9%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  subtextOneStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
});

const mapStateToProps = state => {

  const { goals } = state.procre;
  const loading = state.procre.loading;
  const error = state.procre.error;
  const sucMsg = state.procre.sucMsg;
  const isSuccess = state.procre.isSuccess;
  return { loading, error, goals,sucMsg,isSuccess };

};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { getTraineeGoalsByGender, uploadProfile, profileRefresh },
  )(Goal),
);
