import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import Autocomplete from 'react-native-autocomplete-input';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function getCalories(_text) {
    let lines = _text.split('|');
    let elements = [];
    for (let i = 0; i < lines.length; i++) {
        elements.push(lines[i]);
        if (i < lines.length - 1) {
            elements.push(<br key={i} />);
        }
    }
    let myText = elements[0].split('-');
    return myText[1].replace(' Calories: ', '');
}

function getPeace(_text) {
    let lines = _text.split('|');
    let elements = [];
    for (let i = 0; i < lines.length; i++) {
        elements.push(lines[i]);
        if (i < lines.length - 1) {
            elements.push(<br key={i} />);
        }
    }
    let myText = elements[0].split('-');
    return myText[0];
}

class Trainee_FoodList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: 50,
            mFrom: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            resObj: {},
            isSuccess: false,
            sucMsg: '',
            searchText: '',
            noDataMsg: 'Please search for items you want to add',
            films: [],
            query: '',
            userItems: [],
            isFirstTime: true,
            isSearch: false,
            foodId: 0,
            fsId: 0,

            loadmore: false,
            pageNo: 1,
            searchPageNo: 1,
            searchedFood:[],
        };
        this.getUserFoodItems = this.getUserFoodItems.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                if (this.props.mFrom) {
                    this.setState({ mFrom: this.props.mFrom })
                }
                this.setState({loading : true});
                this.getUserFoodItems();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();

        // if (this.state.mFrom === 'home') {
        //     Actions.popTo('traineeHome');
        // }
        // else {
        //     Actions.traHomeFoodNew({ mdate: this.props.mdate });
        // }
    }

    async getUserFoodItems() {
        try{
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/getmyfoods`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pagesize: 20,
                    page: this.state.pageNo,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, });
                    if (data.data.length > 0) {
                        LogUtils.infoLog1('Data is there');
                        this.setState({
                            loadmore: true ,
                            userItems: [...this.state.userItems, ...data.data],
                            
                            //adding the new data with old one available in Data Source of the List
                        });
                    } else {
                        this.setState({ loadmore: false })

                    }
                    if (this.state.userItems.length === 0) {
                        this.setState({ loading: false, noDataMsg: 'Please search for items you want to add' });
                    }


                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
        }catch(error){
            console.log(error);
        }
    }

    async searchItem(text) {
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/foodautocomplete`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_name: text,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (!this.state.isSearch) {
                        this.setState({ loading: false, films: data.data });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async searchSubmit() {
        this.setState({ isSearch: true, resObj: {} });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('request', JSON.stringify({
            pagesize: 20,
            page: this.state.searchPageNo,
            food_name: this.state.searchText,
        }));
        fetch(
            `${BASE_URL}/trainee/getfooditem`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pagesize: 20,
                    page: this.state.searchPageNo,
                    food_name: this.state.searchText,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isSearch: false, resObj: data.data, isFirstTime: false, userItems: [], });

                    if (Array.isArray(this.state.resObj.food) && this.state.resObj.food.length) {
                        this.setState({loadmore: true ,
                            searchedFood: [...this.state.searchedFood, ...data.data.food]
                        })
                    }
                    else {
                        this.setState({ loading: false, isSearch: false, noDataMsg: 'No data availabe for your search item' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isSearch: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isSearch: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    goToFoodDetails({ response }) {
        // if (this.state.isFirstTime) {
        Actions.traAddFood({ title: this.props.title, foodId: this.state.foodId, fType: this.props.foodType, mdate: this.props.mdate, fsId: this.state.fsId, mFrom: this.state.mFrom, mainObject: response });
        // }
        // else {
        //     Actions.traAddFood({ title: this.props.title, foodId: this.state.foodId, fType: this.props.foodType, mdate: this.props.mdate, fsId: this.state.fsId, mFrom: this.props.mFrom, mainObject: response });
        // }

    }

    async getFoodDetails() {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1(token, JSON.stringify({
            food_id: this.state.foodId,
            fs_id: this.state.fsId,
            is_update: 0,
        }));
        fetch(
            `${BASE_URL}/trainee/getfooditemdetail`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_id: this.state.foodId,
                    fs_id: this.state.fsId,
                    is_update: 0,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.goToFoodDetails({ response: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 5,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        // const interval = setInterval(() => {
        this.setState({ isSuccess: false, sucMsg: '' });

        this.setState({ loading: true });
        // clearInterval(interval);
        // }, 100);
    }

    handleLoadMore = () => {
        LogUtils.infoLog1('Loadmore', this.state.loadmore);
        if (!this.state.loading && this.state.loadmore) {
            LogUtils.infoLog1('Loadmore Clicked');
            this.setState({ loadmore: false, pageNo: this.state.pageNo + 1 });// increase page by 1
            this.getUserFoodItems(); // method for API call 

        }
    };

    handleSearchLoadMore = () => {
        LogUtils.infoLog1('Loadmore', this.state.loadmore);
        if (!this.state.loading && this.state.loadmore) {
            LogUtils.infoLog1('Loadmore Clicked');
            this.setState({ loadmore: false, searchPageNo: this.state.searchPageNo + 1 });// increase page by 1
            this.searchSubmit(); // method for API call 

        }
    };

    renderFoodItemsList() {
        if (this.state.isFirstTime) {

            if (Array.isArray(this.state.userItems) && this.state.userItems.length) {
                return (
                    <View style={{
                        position: 'absolute', marginTop: 100, zIndex: 1, height: hp('100%'),
                        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0, 
                        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
                    }}>
                        <FlatList
                            style={{ marginTop: 10, }}
                            contentContainerStyle={{ paddingBottom: hp('25%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.userItems}
                            extraData={this.state}
                            keyExtractor={item => "userItems"+item.food_id}
                            initialNumToRender={5}
                            onEndReached={this.handleLoadMore.bind(this)}
                            onEndReachedThreshold={0.5}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity
                                    key={item.food_id}
                                    onPress={() => {
                                        this.setState({ foodId: item.food_id, fsId: item.fs_id });
                                        this.getFoodDetails();
                                    }}
                                    style={styles.containerListStyle}>

                                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                                        <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                            {this.renderFoodType(item)}
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_cal.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.textTitle}>{`${item.cal}`} cal</Text>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_serving.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.textMesgment}>per {`${item.qntymesur}`}</Text>
                                        </View>
                                    </View>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_add_feed.png')}
                                        style={styles.imgAdd} />

                                </TouchableOpacity>
                            }} />
                    </View>

                );


            }
            else {
                return (

                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                        <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                    </View>
                );
            }
        }
        else {
            if (Array.isArray(this.state.searchedFood) && this.state.searchedFood.length) {
                return (
                    <View style={{ position: 'absolute', marginTop: 100, zIndex: 1, height: hp('100%'), paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0, }}>
                        <FlatList
                            style={{ marginTop: 10, }}
                            contentContainerStyle={{ paddingBottom: hp('25%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.searchedFood}
                            // extraData={this.state}
                            // initialNumToRender={5}
                            onEndReached={this.handleSearchLoadMore.bind(this)}
                            onEndReachedThreshold={0.5}
                            keyExtractor={item => "searchedFood"+item.food_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity
                                    key={item.food_id}
                                    onPress={() => {
                                        this.setState({ foodId: item.food_id, fsId: item.fs_id });
                                        this.getFoodDetails();

                                    }}
                                    style={styles.containerListStyle}>

                                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                                        <Text style={styles.textWorkName}>{`${item.food_name}`}</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 2, }}>
                                            {this.renderFoodType(item)}

                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_cal.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, marginRight: 3, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.textTitle}>{`${getCalories(item.food_description)}`}</Text>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_serving.png')}
                                                style={{ width: 12, height: 12, marginLeft: 5, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.textMesgment}>{`${getPeace(item.food_description)}`}</Text>
                                        </View>

                                    </View>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_add_feed.png')}
                                        style={styles.imgAdd} />

                                </TouchableOpacity>
                            }} />
                    </View>

                );
            }

            else {
                return (

                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                        <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                    </View>
                );
            }
        }

    }

    renderFoodType = (item) => {
        if (item.food_type) {
            if (item.food_type.length > 20) {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType}>{`${item.food_type}`}</Text>
                    </View>

                );
            }
            else {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType1}>{`${item.food_type}`}</Text>
                    </View>

                );
            }

        } else {
            return (
                <View>
                </View>
            )
        }
    }

    findFilm(query) {
        if (query === '') {
            return [];
        }

        const { films } = this.state;
        const regex = new RegExp(`${query.trim()}`, 'i');
        return films.filter(film => film.name.search(regex) >= 0);
    }

    render() {
        const { searchText } = this.state;
        const films = this.findFilm(searchText);
        const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />
                    <View style={styles.mainView}>

                        {/* title and cal details */}
                        <View style={{
                            flexDirection: 'row',
                            margin: 10,
                        }}>
                            {/* <View style={{ position: 'absolute', zIndex: 111 }}> */}
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            {/* </View> */}

                            <Text style={styles.textHeadTitle}>Add {this.props.title}</Text>

                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: 5,}}
                                onPress={() => Actions.traCreateFood({foodType: this.props.foodType,})}>
                                <View style={styles.viewCarBack}>
                                    <Text style={styles.textCarBack}>+</Text>
                                </View>
                            </TouchableOpacity>

                            {/* <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginRight: -10, right: 0 }}
                                    onPress={() => Actions.traCreateFood()}>
                                    <View style={styles.viewCarBack}>
                                        <Text style={styles.textCarBack}>+</Text>
                                    </View>
                                </TouchableOpacity>
                            </View> */}
                        </View>

                        <View style={styles.container}>

                            <Autocomplete
                                autoCapitalize="none"
                                autoCorrect={false}
                                style={{
                                    backgroundColor: 'transparent',
                                    borderRadius: 0,
                                    height: 40,
                                    fontSize: 13,
                                    padding: 10,
                                    textAlign: 'left',
                                    letterSpacing: 0.23,
                                    fontFamily: 'Rubik-Regular',
                                    color: '#282c37',
                                }}
                                inputContainerStyle={{
                                    borderRadius: 4,
                                    borderColor: '#cccccc',
                                    borderWidth: 1,
                                }}
                                containerStyle={styles.autocompleteContainer}
                                data={films.length === 1 && comp(searchText, films[0].name) ? [] : films}
                                defaultValue={searchText}
                                onChangeText={text => {
                                    this.setState({ searchText: text });
                                    this.searchItem(text);
                                }}
                                placeholder="Search food"
                                returnKeyType='search'
                                onSubmitEditing={() => {
                                    this.setState({searchPageNo : 1, loading: true, searchedFood:[],});
                                    this.searchSubmit()
                                    this.setState({ films: [] })
                                }}
                                renderItem={({ item }) => (
                                    <TouchableOpacity key={item.name} onPress={() => {
                                        this.setState({ searchText: item.name, loading: true, searchPageNo : 1, searchedFood:[],});
                                        this.searchSubmit();
                                        this.setState({ films: [] })
                                        this.searchItem({ text: '' })
                                    }
                                    }>
                                        <Text style={styles.itemText}>
                                            {item.name}
                                        </Text>
                                    </TouchableOpacity>
                                )}
                            />

                        </View>
                        {this.renderFoodItemsList()}
                    </View>

                </View>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <CustomDialog
                    visible={this.state.isSuccess}
                    title='Success '
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground >
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    viewCarBack: {
        width: 30,
        height: 30,
        borderRadius: 35 / 2,
        color: '#282c37',
        borderWidth: 2,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textCarBack: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
    },
    mainView: {
        flexDirection: 'column',
    },
    containerListStyle: {
        width: wp('90%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 6,
        padding: 15,
        position: 'relative',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textWorkName: {
        fontSize: 14,
        width: wp('80%'),
        fontWeight: '500',
        paddingRight: 10,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textFoodType: {
        fontSize: 10,
        width: wp('25%'),
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textFoodType1: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textMesgment: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        flex: 1,
        marginRight: 20,
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
    },
    imgAdd: { width: 25, height: 25, position: 'relative', alignSelf: 'center', right: 15 },
    textNodata: {
        width: wp('100%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 20,
        marginTop: 15,
        marginTop: hp('35%'),
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    // auto complete styles
    container: {
        width: wp('90%'),
        backgroundColor: 'transparent',
        flexDirection: 'row',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        zIndex: 10,
    },
    autocompleteContainer: {
        width: wp('90%'),
        backgroundColor: 'transparent',
        flex: 1,
        alignSelf: 'flex-start',
        position: 'relative',
    },
    itemText: {
        fontSize: 15,
        margin: 10,
        fontFamily: 'Rubik-Regular',
    },
});


export default Trainee_FoodList;
