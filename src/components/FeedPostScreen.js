import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Text,
  ScrollView,
  Dimensions,
  Platform,
} from 'react-native';
const profileImageSize = 45;
const padding = 10;
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { feedUpload } from '../actions';
import { connect } from 'react-redux';
import { ProgressBar, Loader, CustomDialog, NoInternet, ProgressBar1 } from './common';
import FastImage from 'react-native-fast-image'
let token = '', profileImg = '';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import Image1 from 'react-native-scalable-image';
import Dialog, {
  DialogContent,
} from 'react-native-popup-dialog';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import RNFetchBlob from 'rn-fetch-blob';
import { RNS3 } from 'react-native-aws3';
function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}
import ProgressiveImage from './ProgressiveImage';
const w = Dimensions.get('window');

const futch = (url, opts = {}, onProgress) => {
  LogUtils.infoLog1(url, opts)
  return new Promise((res, rej) => {
    var xhr = new XMLHttpRequest();
    xhr.open(opts.method || 'get', url);
    for (var k in opts.headers || {})
      xhr.setRequestHeader(k, opts.headers[k]);
    xhr.onload = e => res(e.target);
    xhr.onerror = rej;
    if (xhr.upload && onProgress)
      xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
    xhr.send(opts.body);
  });
}

const createFormData = (image, body, type) => {
  const data = new FormData();

  if (image) {
    LogUtils.infoLog1("image", image);
    image.forEach((photo) => {

      // if (photo.includes('mp4') || photo.includes('Mov')) {
      if (type === 2) {
        data.append('file', {
          uri: photo,
          name: 'Video',
          type: 'video/mp4', // or photo.type
        });
      }
      else {
        data.append('file', {
          uri: photo,
          name: 'Image',
          type: 'image/jpeg', // or photo.type
        });
      }
    });
  }
  // }
  data.append('data', body);
  return data;
};

class FeedPostScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: '',
      fileId: 0,
      filepath: {
        data: '',
        uri: '',
      },
      fileData: '',
      fileUri: '',
      fileType: '',
      fileName: '',
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: true,

      videoId: 0,
      videopath: {
        data: '',
        uri: '',
      },
      videoData: '',
      videoUri: '',
      videoType: '',
      fileSize: 0,

      isAlert: false,
      alertMsg: '',
      isInternet: false,
      loading: false,
      isSuccess: false,
      progress: 0,
      isProgress: false,

      fileId2: 0,
      filepath2: {
        data: '',
        uri: '',
      },
      fileData2: '',
      fileUri2: '',
      fileType2: '',

      type: 0,
      imageArray: [],
      isAddImgPop: false,
      isTransformImgPop: false,
      transformType: '',
      profileImage: '',

      s3_url_2: '',
      resObjAwsS3: '',
    };
  }

  async componentDidMount() {
    allowFunction();
    token = await AsyncStorage.getItem('token');
    LogUtils.infoLog1("Token", token);
    profileImg = await AsyncStorage.getItem('profileImg');
    LogUtils.infoLog1("profileImg", profileImg);
    this.setState({ profileImage: await AsyncStorage.getItem('profileImg') });
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    if (this.state.isAddImgPop) {
      this.setState({ isAddImgPop: false });
    }
    else if (this.state.isTransformImgPop) {
      this.setState({ isTransformImgPop: false });
    }
    else {
      Actions.pop();
    }
  }

  async onRetry() {
    this.setState({ isInternet: false });
  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }
  async onSuccess() {
    await AsyncStorage.setItem('is_wrkout_subscribe', "1");
    Actions.wall();
    this.setState({ isSuccess: false });

  }

  async savePost(token, image, body) {
    this.setState({ progress: 0 });
    LogUtils.infoLog1('Body', body);
    futch(`${BASE_URL}/trainee/Feed`, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: createFormData(image, body, this.state.type),
    }, (progressEvent) => {
      const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
      this.setState({ progress: progress });
    })
      .then(res => {
        LogUtils.infoLog1('res', res);
        this.setState({ isProgress: false })
        const statusCode = res.status;
        LogUtils.infoLog1('statusCode', statusCode);
        const data = JSON.parse(res.response);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message });
        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
        this.setState({ isProgress: false })
      })
      .catch(function (error) {
        this.setState({ isProgress: false, loading: false, isAlert: true, alertMsg: SWR });
      });
  }

  async saveFeedVideoAwsS3(body) {
    let token = await AsyncStorage.getItem('token');
    let s3Body = JSON.stringify({
      title: body.title,
      desc: body.desc,
      filetype: body.filetype,
      article_link: body.article_link,
      bucket: this.state.resObjAwsS3.bucket,
      etag: this.state.resObjAwsS3.etag,
      key: this.state.resObjAwsS3.key,
      s3_url: this.state.resObjAwsS3.location,
      s3_url_2: this.state.s3_url_2,
    })
    LogUtils.infoLog1('request', s3Body);
    fetch(
      `${BASE_URL}/trainee/FeedS3`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: s3Body,
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1("statusCode", statusCode);
        LogUtils.infoLog1("data", data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ isProgress: false, isAlert: false, isSuccess: true, alertMsg: data.message, alertTitle: 'Success' });
          // this.setState({ isProgress: false, isAlert: true, isSuccess: true, alertMsg: data.message, alertTitle: 'Success' });

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
          } else {
            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
          }
        }
      })
      .catch(function (error) {
        this.setState({ isProgress: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
      });
  }

  async callImagesToAWSS31(body) {
    this.setState({ progress: 0, isProgress: true });
    let userId = await AsyncStorage.getItem('userId');
    let type = '', extension = '';

    if (this.state.fileUri2.includes('.png') || this.state.fileUri2.includes('.PNG')) {
      type = 'image/png';
      extension = '.png';
    } else if (this.state.fileUri2.includes('.jpg') || this.state.fileUri2.includes('.JPG')) {
      type = 'image/jpg';
      extension = '.jpg';
    }
    else {
      if (this.state.fileName) {
        const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
        type = this.state.fileType;
        extension = '.' + fileExtension.toLowerCase();
      }
    }

    const file = {
      uri: this.state.fileUri2,
      name: userId + '_' + new Date().getTime() + extension,
      type: type,
    }

    const options = {
      keyPrefix: await AsyncStorage.getItem('s3_feed_path'),
      bucket: await AsyncStorage.getItem('s3_bucket'),
      region: await AsyncStorage.getItem('s3_region'),
      accessKey: await AsyncStorage.getItem('s3_key'),
      secretKey: await AsyncStorage.getItem('s3_secret'),
      successActionStatus: 201
    }
    LogUtils.infoLog1('file : ', file);
    LogUtils.infoLog1('options : ', options);

    RNS3.put(file, options)
      .progress((e) => {
        const progress = Math.floor((e.loaded / e.total) * 100);
        this.setState({ progress: progress });
      }).then(response => {
        if (response.status === 201) {
          this.setState({ s3_url_2: response.body.postResponse.location, progress: 50 });
          LogUtils.infoLog1('response', response.body);
          LogUtils.infoLog1('response1', response.body.postResponse);

          this.callImagesToAWSS3(body);
        }
        else {
          LogUtils.infoLog('Failed to upload image to S3');
          LogUtils.infoLog1('response', response.body);
          this.setState({ isProgress: false });
        }
      });
  }

  async callImagesToAWSS3(body) {
    this.setState({ progress: this.state.progress, isProgress: true });
    let userId = await AsyncStorage.getItem('userId');
    let type = '', extension = '';

    if (this.state.fileUri.includes('.png') || this.state.fileUri.includes('.PNG')) {
      type = 'image/png';
      extension = '.png';
    } else if (this.state.fileUri.includes('.jpg') || this.state.fileUri.includes('.JPG')) {
      type = 'image/jpg';
      extension = '.jpg';
    }
    else {
      if (this.state.fileName) {
        const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
        type = this.state.fileType;
        extension = '.' + fileExtension.toLowerCase();
      }
    }

    const file = {
      uri: this.state.fileUri,
      name: userId + '_' + new Date().getTime() + extension,
      type: type,
    }

    const options = {
      keyPrefix: await AsyncStorage.getItem('s3_feed_path'),
      bucket: await AsyncStorage.getItem('s3_bucket'),
      region: await AsyncStorage.getItem('s3_region'),
      accessKey: await AsyncStorage.getItem('s3_key'),
      secretKey: await AsyncStorage.getItem('s3_secret'),
      successActionStatus: 201
    }
    LogUtils.infoLog1('file : ', file);
    LogUtils.infoLog1('options : ', options);

    RNS3.put(file, options)
      .progress((e) => {
        const progress = Math.floor((e.loaded / e.total) * 100);
        this.setState({ progress: progress });
      }).then(response => {
        if (response.status === 201) {
          this.setState({ resObjAwsS3: response.body.postResponse });
          LogUtils.infoLog1('response', response.body);
          LogUtils.infoLog1('response1', response.body.postResponse);

          this.saveFeedVideoAwsS3(body);
        }
        else {
          LogUtils.infoLog('Failed to upload image to S3');
          LogUtils.infoLog1('response', response.body);
          this.setState({ isProgress: false });
        }
      });

  }


  async uploadVideoToAWS(body) {
    this.setState({ progress: 0, isProgress: true });
    let userId = await AsyncStorage.getItem('userId');
    LogUtils.infoLog1('this.state.videoUri', this.state.videoUri);
    let type = 'video/mp4', extension = '';
    if ((this.state.videoUri.includes('.mp4') || this.state.videoUri.includes('.MP4'))) {
      type = "video/mp4";
      extension = '.mp4';
    } else if ((this.state.videoUri.includes('.3gp') || this.state.videoUri.includes('.3GP'))) {
      type = "video/3gpp";
      extension = '.3gp';
    } else if ((this.state.videoUri.includes('.mov') || this.state.videoUri.includes('.MOV'))) {
      type = "video/quicktime";
      extension = '.mov';
    } else if ((this.state.videoUri.includes('.avi') || this.state.videoUri.includes('.AVI'))) {
      type = "video/x-msvideo";
      extension = '.avi';
    } else if ((this.state.videoUri.includes('.wmv') || this.state.videoUri.includes('.WMV'))) {
      type = "video/x-ms-wmv";
      extension = '.wmv';
    }
    else {
      // LogUtils.infoLog1('file : ', this.state.fileName);
      if (this.state.fileName) {
        const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
        //   LogUtils.infoLog1(pluginName, fileExtension);
        type = this.state.videoType;
        extension = '.' + fileExtension.toLowerCase();
      } else {
        type = "video/mp4";
        extension = '.mp4';
      }
    }


    const file = {
      // `uri` can also be a file system path (i.e. file://)
      uri: this.state.videoUri,
      name: userId + '_' + new Date().getTime() + extension,
      type: type

    }

    const options = {
      keyPrefix: await AsyncStorage.getItem('s3_feed_path'),
      bucket: await AsyncStorage.getItem('s3_bucket'),
      region: await AsyncStorage.getItem('s3_region'),
      accessKey: await AsyncStorage.getItem('s3_key'),
      secretKey: await AsyncStorage.getItem('s3_secret'),
      successActionStatus: 201
    }
    LogUtils.infoLog1('file : ', file);
    LogUtils.infoLog1('options : ', options);

    RNS3.put(file, options)
      .progress((e) => {
        const progress = Math.floor((e.loaded / e.total) * 100);
        this.setState({ progress: progress });
      }).then(response => {
        if (response.status === 201) {
          this.setState({ resObjAwsS3: response.body.postResponse });
          LogUtils.infoLog1('response', response.body);
          LogUtils.infoLog1('response1', response.body.postResponse);
          // this.setState({ isProgress: false });
          // this.setState({ resObjAwsS3: response.body.postResponse });
          this.saveFeedVideoAwsS3(body);
        }
        else {
          LogUtils.infoLog('Failed to upload image to S3');
          LogUtils.infoLog1('response', response.body);
          this.setState({ isProgress: false });
        }
      });
  }

  feedUpload(postText, imageUri, VideoUri, imageUri2) {
    if (!postText && !imageUri && !VideoUri) {
      this.setState({ isAlert: true, alertMsg: 'Sorry. You cant post a blank post.' });
    } else {
      let body = '';

      if (imageUri && imageUri2) {

        if (imageUri) {
          this.state.imageArray.push(imageUri);
        }

        if (imageUri2) {
          this.state.imageArray.push(imageUri2);
        }

        body = {
          title: postText,
          desc: '',
          filetype: '5',
          article_link: '',
        };
        LogUtils.infoLog1("body", body);
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            // this.setState({ isProgress: true, loading: false });
            // this.savePost(
            //   token,
            //   this.state.imageArray,
            //   body,
            // );
            this.callImagesToAWSS31(body);

          }
          else {
            this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
          }
        });
      } else if (imageUri) {

        if (imageUri) {
          this.state.imageArray.push(imageUri);
        }

        body = {
          title: postText,
          desc: '',
          filetype: '1',
          article_link: '',
        };
        LogUtils.infoLog1("body", body);
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            this.callImagesToAWSS3(body);

          }
          else {
            this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
          }
        });
      } else if (VideoUri) {
        if (VideoUri) {
          this.state.imageArray.push(VideoUri);
        }
        body = {
          title: postText,
          desc: '',
          filetype: '2',
          article_link: '',
        };
        LogUtils.infoLog1("body", body);
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            this.uploadVideoToAWS(body);

          }
          else {
            this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
          }
        });
      } else {
        body = {
          title: '',
          desc: postText,
          filetype: '0',
          article_link: '',
        };
        LogUtils.infoLog1("body", body);
        NetInfo.fetch().then(state => {
          if (state.isConnected) {
            // this.setState({ isProgress: false, loading: true });
            // this.savePost(
            //   token,
            //   this.state.imageArray,
            //   body,
            // );
            this.saveFeedVideoAwsS3(body);

          }
          else {
            this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
          }
        });

      }
    }
  }

  renderVideoData() {
    if (this.state.type === 2 && this.state.videoUri) {
      return (
        <View style={{
          justifyContent: 'center', alignItems: 'center', paddingBottom: '10%'

        }}>

          <Image
            progressiveRenderingEnabled={true}
            resizeMethod="resize"
            source={{ uri: this.state.videoUri }}
            style={styles.mediaPlayer}
          />
          <TouchableOpacity
            style={styles.playButton}
            onPress={() => {
              if (Platform.OS === 'android') {
                Actions.newvideoplay({ videoURL: this.state.videoUri, from: 'feedpost' });
                // Actions.customPlayer({ videoURL: this.state.videoUri, from: 'feedpost' });
              } else {
                Actions.afPlayer({ videoURL: this.state.videoUri, from: 'feedpost' });
              }
            }}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/ic_play_white.png')}
              style={styles.playImageStyle}
            />
          </TouchableOpacity>
        </View>
      );
    }
    else if (this.state.type === 1 && this.state.fileUri) {
      return (
        <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginTop: 10, alignSelf: 'center', paddingBottom: '5%' }}
        >

          <Image1
            style={{ borderRadius: 10, borderColor: '#e1e4e8', borderWidth: 1 }}
            width={Dimensions.get('window').width - 20} // height will be calculated automatically
            source={{ uri: this.state.fileUri }}
          />

        </View>
      );


    } else if (this.state.type === 3) {
      if (this.state.fileUri && this.state.fileUri2) {
        return (
          <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginTop: 10, flexDirection: 'row', paddingBottom: '10%' }}>

            <TouchableOpacity
              style={{ alignSelf: 'center', alignItems: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, marginLeft: 20 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'BF' })
              }}>
              {/* onPress={this.chooseImage}> */}

              <Image1
                style={{
                  borderTopLeftRadius: 6,
                  borderBottomLeftRadius: 6, borderColor: '#e1e4e8', borderWidth: 0.5
                }}
                width={Dimensions.get('window').width - ((Dimensions.get('window').width / 2) + 10)} // height will be calculated automatically
                source={{ uri: this.state.fileUri }}
              />

              {/* <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                resizeMode="cover"
                style={{
                  width: wp('45%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderTopLeftRadius: 6,
                  borderBottomLeftRadius: 6,
                }}
                // source={require('../res/ic_play.png')}
                source={{ uri: this.state.fileUri }}
              /> */}
            </TouchableOpacity>

            <TouchableOpacity style={{ alignSelf: 'center', alignItems: 'center', borderTopRightRadius: 6, borderBottomRightRadius: 6, marginRight: 20 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'AF' })
              }}>
              {/* onPress={this.chooseImage1}> */}

              <Image1
                style={{
                  borderTopRightRadius: 6, borderBottomRightRadius: 6, borderColor: '#e1e4e8', borderWidth: .5
                }}
                width={Dimensions.get('window').width - ((Dimensions.get('window').width / 2) + 10)} // height will be calculated automatically
                source={{ uri: this.state.fileUri2 }}
              />

              {/* 
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                resizeMode="cover"
                style={{
                  width: wp('45%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderTopRightRadius: 6, borderBottomRightRadius: 6,
                }}
                source={{ uri: this.state.fileUri2 }}
              /> */}
            </TouchableOpacity>
          </View>
        );

      } else if (this.state.fileUri && !this.state.fileUri2) {
        return (
          <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginTop: 10, flexDirection: 'row', paddingBottom: '10%' }}>

            <TouchableOpacity style={{ width: '45%', alignSelf: 'center', alignItems: 'center', marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'BF' })
              }}>
              {/* onPress={this.chooseImage}> */}
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                resizeMode="cover"
                style={{
                  width: wp('45%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                source={{ uri: this.state.fileUri }}
              />

            </TouchableOpacity>
            <TouchableOpacity
              style={{ width: '45%', alignSelf: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 6, marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'AF' })
              }}>
              {/* onPress={this.chooseImage1}> */}

              <ProgressiveImage
                source={require('../res/ic_camera_transform.png')}
                style={{
                  width: wp('25%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
              />
              <View style={{ position: 'absolute', alignSelf: 'center', alignItems: 'center' }}>
                <Text style={styles.desc1}>After</Text>
              </View>


            </TouchableOpacity>
          </View>
        );

      } else if (!this.state.fileUri && this.state.fileUri2) {
        return (
          <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', marginTop: 10, flexDirection: 'row', paddingBottom: '10%' }}>

            <TouchableOpacity
              style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 6, marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'BF' })
              }}>
              {/* onPress={this.chooseImage}> */}

              <ProgressiveImage
                source={require('../res/ic_camera_transform.png')}
                style={{
                  width: wp('25%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
              />
              <View style={{ position: 'absolute', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.desc1}>Before</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 6, marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'AF' })
              }}>
              {/* onPress={this.chooseImage1}> */}


              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                resizeMode="cover"
                style={{
                  width: wp('45%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                source={{ uri: this.state.fileUri2 }}
              />
            </TouchableOpacity>
          </View>
        );

      } else if (!this.state.fileUri && !this.state.fileUri2) {
        return (
          <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginTop: 10, flexDirection: 'row', alignSelf: 'center', width: '90%', paddingBottom: '10%' }}>
            <TouchableOpacity
              style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 6, marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'BF' })
              }}>
              {/* onPress={this.chooseImage}> */}

              <ProgressiveImage
                source={require('../res/ic_camera_transform.png')}
                style={{
                  width: wp('25%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
              />
              <View style={{ position: 'absolute', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.desc1}>Before</Text>
              </View>


            </TouchableOpacity>

            <TouchableOpacity
              style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 6, marginLeft: 5 }}
              onPress={() => {
                this.setState({ isTransformImgPop: true, transformType: 'AF' })
              }}>
              {/* // onPress={this.chooseImage1}> */}

              <ProgressiveImage
                source={require('../res/ic_camera_transform.png')}
                style={{
                  width: wp('25%'),
                  height: 215,
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
              />
              <View style={{ position: 'absolute', alignSelf: 'center', alignItems: 'center' }}>
                <Text style={styles.desc1}>After</Text>
              </View>


            </TouchableOpacity>
          </View>
        );

      }

    }
  }

  renderFileData() {
    if (this.state.fileUri) {
      return (
        <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', }}>
          <ProgressiveImage
            thumbnailSource={{ uri: feeditem.file_url + `?w=50&buster=${Math.random()}` }}
            source={{ uri: feeditem.file_url + `?w=${w.width * 2}&buster=${Math.random()}` }}
            style={{
              width: wp('95%'),
              height: 215,
              justifyContent: 'center',
              alignSelf: 'center',
            }}
            resizeMode="contain"
          />

        </View>

      );
    }
  }

  renderProgressBar() {

    if (Platform.OS === 'android') {
      return (
        <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
      );

    } else {
      return (
        <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
      );
    }

  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Loader loading={this.state.loading} />
        {this.renderProgressBar()}

        <ImageBackground source={require('../res/post_back.png')} style={styles.mainContainer}>
          <TouchableOpacity
            onPress={() => this.onBackPressed()}>
            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={require('../res/ic_back.png')}
              style={styles.backImageStyle}
            />
          </TouchableOpacity>
          <Image
            progressiveRenderingEnabled={true}
            resizeMethod="resize"
            source={require('../res/ic_path.png')}
            style={styles.pathImageStyle}
          />

          <View style={styles.postContents}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={styles.createPostStyle}>Create Post</Text>
              <TouchableOpacity
                onPress={() => {
                  LogUtils.infoLog1("this.state.fileSize", this.state.fileSize);
                  this.validateVideoSize(this.state.fileSize);

                }}
                style={styles.postButtonStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_post_button.png')}
                  style={styles.postPollButtonStyle}
                />
              </TouchableOpacity>
            </View>


          </View>

          <ScrollView style={{
            // flex: 1,

            width: '100%',
            height: '100%',
            backgroundColor: '#ffffff',
            paddingTop: 10,
            flexDirection: 'column',
          }}>
            <View style={{
              flexDirection: 'row',
              marginBottom: 15,
              paddingLeft: 23,
            }}>

              {/* <View > */}
              {/* <FastImage
                  style={styles.avatar}
                  source={{
                    uri: profileImg,
                    headers: { Authorization: '12345' },
                    priority: FastImage.priority.high,
                  }}
                /> */}

              <Image
                progressiveRenderingEnabled={true}
                resizeMode="cover"
                source={{ uri: this.state.profileImage }}
                style={styles.avatar}
              />
              {/* </View> */}

              <View style={styles.inputRow}>
                <TextInput
                  style={styles.textInputStyle}
                  placeholder="Share your thought..."
                  placeholderTextColor="#9c9eb9"
                  value={this.state.post}
                  // returnKeyType={ 'next' }
                  multiline={true}
                  onChangeText={text => this.setState({ post: text })}
                />

              </View>
            </View>

            {this.renderVideoData()}
          </ScrollView>
        </ImageBackground>
        <View style={{ flexDirection: 'column', marginBottom: 10, }}>
          <Text style={styles.subtextOneStyle}>Add Attachment</Text>

          <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }}>
            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '31%' }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ type: 1, fileUri: '', isAddImgPop: true })
                  // this.chooseImage()
                }}>
                <View style={styles.containerMaleStyle}>

                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                    source={require('../res/photo.png')}>
                  </Image>

                </View>
              </TouchableOpacity>
              <Text style={styles.desc}>Photo</Text>
            </View>

            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '31%' }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ type: 2, isAddImgPop: true })
                  // this.chooseVideo()
                }}>
                <View style={styles.containerMaleStyle1}>

                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{ width: 35, height: 25, tintColor: '#ffffff' }}
                    source={require('../res/video.png')}>
                  </Image>

                </View>
              </TouchableOpacity>
              <Text style={styles.desc}>Video</Text>
            </View>
            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '37%' }}>
              <TouchableOpacity
                onPress={() => { this.setState({ type: 3, fileUri: '', fileUri2: '' }) }}>
                <View style={styles.containerMaleStyle2}>

                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                    source={require('../res/transformation.png')}>
                  </Image>

                </View>
              </TouchableOpacity>
              <Text style={styles.desc}>Transformation</Text>
            </View>
          </View>
        </View>

        <NoInternet
          image={require('../res/img_nointernet.png')}
          loading={this.state.isInternet}
          onRetry={this.onRetry.bind(this)} />

        <CustomDialog
          visible={this.state.isAlert}
          title='Alert'
          desc={this.state.alertMsg}
          onAccept={this.onAccept.bind(this)}
          no=''
          yes='Ok' />
        <CustomDialog
          visible={this.state.isSuccess}
          title='Congratulations !'
          desc={this.state.alertMsg}
          onAccept={this.onSuccess.bind(this)}
          no=''
          yes='Ok' />

        <Dialog
          onDismiss={() => {
            this.setState({ isAddImgPop: false });
          }}
          onTouchOutside={() => {
            this.setState({ isAddImgPop: false });
          }}
          width={0.7}
          // height={0.5}
          visible={this.state.isAddImgPop}>
          <DialogContent
            style={{
              backgroundColor: '#ffffff'
            }}>
            <View style={{ flexDirection: 'column', padding: 15 }}>
              <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                <View style={{ marginTop: 10, }}></View>
                {this.state.type === 1
                  ?
                  (
                    <Text style={styles.textAddImgTit}>Add Photo</Text>
                  )
                  :
                  (
                    <Text style={styles.textAddImgTit}>Add Video</Text>
                  )}

                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                  <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isAddImgPop: false });
                        if (this.state.type === 1) {
                          this.captureImage();
                        }
                        else if (this.state.type === 2) {
                          this.captureVideo();
                        }

                      }}>
                      <View style={styles.containerMaleStyle2}>

                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                          source={require('../res/photo.png')}>
                        </Image>

                      </View>
                    </TouchableOpacity>
                    <Text style={styles.desc}>Camera</Text>
                  </View>
                  <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isAddImgPop: false });
                        if (this.state.type === 1) {
                          if (Platform.OS === 'android') {
                            this.pickImagesFromGallery();
                          } else {
                            this.pickImagesFromGalleryIOS();
                          }

                        }
                        else if (this.state.type === 2) {
                          if (Platform.OS === 'android') {
                            this.pickVideoFromGallery();
                          } else {
                            this.pickVideoFromGalleryIOS();
                          }

                        }

                      }}>
                      <View style={styles.containerMaleStyle1}>

                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                          source={require('../res/transformation.png')}>
                        </Image>

                      </View>
                    </TouchableOpacity>
                    <Text style={styles.desc}>Gallery</Text>
                  </View>

                </View>
              </View>
            </View>
          </DialogContent>
        </Dialog>


        <Dialog
          onDismiss={() => {
            this.setState({ isTransformImgPop: false });
          }}
          onTouchOutside={() => {
            this.setState({ isTransformImgPop: false });
          }}
          width={0.7}
          // height={0.5}
          visible={this.state.isTransformImgPop}>
          <DialogContent
            style={{
              backgroundColor: '#ffffff'
            }}>
            <View style={{ flexDirection: 'column', padding: 15 }}>
              <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                <View style={{ marginTop: 10, }}></View>

                <Text style={styles.textAddImgTit}>Add Photo</Text>

                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                  <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isTransformImgPop: false });
                        if (this.state.transformType === 'BF') {
                          this.captureImage();
                        }
                        else if (this.state.transformType === 'AF') {
                          this.captureImage1();
                        }

                      }}>
                      <View style={styles.containerMaleStyle2}>

                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                          source={require('../res/photo.png')}>
                        </Image>

                      </View>
                    </TouchableOpacity>
                    <Text style={styles.desc}>Camera</Text>
                  </View>
                  <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ isTransformImgPop: false });
                        if (this.state.transformType === 'BF') {
                          if (Platform.OS === 'android') {
                            this.pickImagesFromGallery();
                          } else {
                            this.pickImagesFromGalleryIOS();
                          }
                        }
                        else if (this.state.transformType === 'AF') {
                          if (Platform.OS === 'android') {
                            this.pickImagesFromGallery1();
                          } else {
                            this.pickImagesFromGalleryIOS1();
                          }

                        }

                      }}>
                      <View style={styles.containerMaleStyle1}>

                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                          source={require('../res/transformation.png')}>
                        </Image>

                      </View>
                    </TouchableOpacity>
                    <Text style={styles.desc}>Gallery</Text>
                  </View>

                </View>
              </View>
            </View>
          </DialogContent>
        </Dialog>
      </View>
    );
  }

  chooseVideo = () => {
    let options = {
      title: 'Video Picker',
      takePhotoButtonTitle: 'Record Video...',
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 120,
      // customButtons: [
      //   {
      //     name: 'customOptionKey',
      //     title: 'Choose Document',
      //   },
      // ],
      storageOptions: {
        skipBackup: true,
        path: 'video',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      }

      else {
        LogUtils.infoLog1('fileUri', response.uri);
        this.state.fileUri = '';
        this.state.fileUri2 = '';

        this.setState({
          videopath: response,
          videoData: response.data,
          videoUri: response.uri,
          videoType: response.type,
        });
      }
    });
  };

  chooseImage = () => {
    let options = {
      title: 'Select Image',

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // You can also display the image using data:
        LogUtils.infoLog1('fileUri', response.uri);
        this.setState({ videoUri: '' });
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileType: response.type,
        });
      }
    });
  };

  captureImage = () => {
    let options = {
      title: 'Select Image',

      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 500,
      maxWidth: 500,

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.7,
    };
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({ videoUri: '' });
        this.setState({
          filePath: response,
          fileData: response.data,
          fileName: response.name,
          fileUri: response.uri,
          fileType: response.type,
        });
      }
    });
  };

  async pickImagesFromGallery() {
    // Pick a single file
    try {
      const response = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });

      this.setState({ videoUri: '' });
      this.setState({
        filePath: response,
        fileData: response.data,
        fileName: response.name,
        fileUri: response.uri,
        fileType: response.type,
      });

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  async pickImagesFromGalleryIOS() {
    // Pick a single file
    let options = {
      title: 'Select Image',

      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 500,
      maxWidth: 500,

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.7,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({ videoUri: '' });
        this.setState({
          filePath: response,
          fileData: response.data,
          fileName: response.name,
          fileUri: response.uri,
          fileType: response.type,
        });

      }
    });
  }

  captureVideo = () => {
    let options = {
      title: 'Select Video',
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 120,
      storageOptions: {
        skipBackup: true,
        path: 'video',
      },
    };
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.state.fileUri = '';
        this.state.fileUri2 = '';

        this.setState({
          videopath: response,
          videoData: response.data,
          videoUri: response.uri,
          videoType: response.type,
        });
      }
    });
  };

  async pickVideoFromGallery() {
    // Pick a single file
    try {
      const response = await DocumentPicker.pick({
        type: [DocumentPicker.types.video],
      });

      // this.setState({fileUri : ''});
      // this.setState({fileUri2 : ''});
      this.state.fileUri = '';
      this.state.fileUri2 = '';
      LogUtils.infoLog1('response', response);
      this.setState({
        videopath: response,
        videoData: response.data,
        videoUri: response.uri,
        videoType: response.type,
        fileName: response.name,
      });

      if (response.fileSize) {
        this.setState({
          fileSize: response.fileSize,

        });

      }

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        LogUtils.infoLog1('err', err)
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  async pickVideoFromGalleryIOS() {
    // Pick a single file
    let options = {
      title: 'Select Image',
      mediaType: 'video',

      storageOptions: {
        skipBackup: true,
        path: 'video',
      },
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.state.fileUri = '';
        this.state.fileUri2 = '';

        this.setState({
          videopath: response,
          videoData: response.data,
          videoUri: response.uri,
          videoType: response.type,
        });

        if (response.fileSize) {
          this.setState({
            fileSize: response.fileSize,

          });

        } else if (response.origURL) {
          RNFetchBlob.fs.stat(response.origURL)
            .then((stats) => {
              LogUtils.infoLog1('stats.size', stats.size);

              this.setState({
                fileSize: stats.size,

              });


            })
            .catch((err) => { LogUtils.infoLog1('err', err); })

        }


      }
    });
  }

  async validateVideoSize(fileSize) {
    let max_video_size = await AsyncStorage.getItem('max_video_size');
    var videoSize = parseInt(max_video_size) / 1048576;
    LogUtils.infoLog1("max_video_size", parseInt(max_video_size));
    if (fileSize < parseInt(max_video_size)) {
      //    this.setState({ isProgress: true })
      //     this.saveChallengeVideo();

      this.feedUpload(
        this.state.post,
        this.state.fileUri,
        this.state.videoUri,
        this.state.fileUri2,
      );
    }
    else {
      this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: `Please make sure your file size should not exceed ${videoSize}MB` });
    }
  }

  chooseImage1 = () => {
    let options = {
      title: 'Select Image',

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // You can also display the image using data:
        LogUtils.infoLog1('fileUri', response.uri);
        this.setState({ videoUri: '' });
        this.setState({
          filepath2: response,
          fileData2: response.data,
          fileUri2: response.uri,
          fileType2: response.type,
        });
      }
    });
  };

  captureImage1 = () => {
    let options = {
      title: 'Select Image',
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 500,
      maxWidth: 500,

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.7,
    };
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({ videoUri: '' });
        this.setState({
          filepath2: response,
          fileData2: response.data,
          fileUri2: response.uri,
          fileName: response.name,
          fileType2: response.type,
        });
      }
    });
  };

  async pickImagesFromGallery1() {
    // Pick a single file
    try {
      const response = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });

      this.setState({ videoUri: '' });
      this.setState({
        filepath2: response,
        fileData2: response.data,
        fileName: response.name,
        fileUri2: response.uri,
        fileType2: response.type,
      });

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  async pickImagesFromGalleryIOS1() {
    // Pick a single file
    let options = {
      title: 'Select Image',
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 500,
      maxWidth: 500,

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.7,
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        LogUtils.infoLog('User cancelled image picker');
      } else if (response.error) {
        LogUtils.infoLog1('ImagePicker Error: ', response.error);
      } else {
        // LogUtils.infoLog1('response', response);
        LogUtils.infoLog1('fileUri', response.uri);

        this.setState({ videoUri: '' });
        this.setState({
          filepath2: response,
          fileData2: response.data,
          fileUri2: response.uri,
          fileType2: response.type,
        });

      }
    });
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
  },
  pathImageStyle: {
    width: 150,
    height: 5,
    marginTop: '12%',
    alignSelf: 'center'
  },
  postContents: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    paddingLeft: 23,
    paddingRight: 23,
    paddingBottom: 15,
    paddingTop: 23,
    marginTop: 20,
  },
  createPostStyle: {
    // width: 137,
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 30,
  },
  inputRow: {
    flexDirection: 'row',
    alignItems: "center",
  },
  avatar: {
    // width: 40,
    // height: 40,
    // aspectRatio: 1,
    // backgroundColor: "#D8D8D8",
    // borderWidth: StyleSheet.hairlineWidth,
    // borderColor: "#979797",
    // alignSelf: 'flex-start',
    // borderRadius: profileImageSize / 2,
    // width: profileImageSize,
    // height: profileImageSize,
    // resizeMode: "cover",
    // marginRight: padding

    width: 40,
    height: 40,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 40 / 2,
    resizeMode: "cover",
    justifyContent: 'flex-end',
    marginRight: 10,
  },
  textInputStyle: {
    width: wp('75%'),
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 0.23,
    alignSelf: 'center',
  },
  postPollButtonStyle: {
    width: 80,
    height: 30,
    alignSelf: 'flex-end',
  },
  postButtonStyle: {
    alignSelf: 'center',
    position: 'absolute',
    alignItems: 'flex-end',
    right: 15,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    alignSelf: 'flex-start',
    marginLeft: 20,
    marginTop: 20,
    tintColor: 'white',
  },
  mediaPlayer: {
    width: '90%',
    height: 215,
    position: 'relative',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    borderWidth: .1,
    borderRadius: 10,
    backgroundColor: 'black',
    marginTop: '15%',
    alignSelf: 'center',
  },
  playButton: {
    padding: 10,
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  playImageStyle: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 50,
  },
  container: {
    flex: 1,
    alignSelf: 'center',
  },
  subtextOneStyle: {
    fontSize: 16,
    marginLeft: 20,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    fontFamily: 'Rubik-Medium',
  },
  containerMaleStyle: {
    width: wp('20%'),
    height: 80,
    margin: hp('2%'),
    backgroundColor: '#83e4ef',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerMaleStyle1: {
    width: wp('20%'),
    height: 80,
    margin: hp('2%'),
    backgroundColor: '#fd9c93',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerMaleStyle2: {
    width: wp('20%'),
    height: 80,
    margin: hp('2%'),
    backgroundColor: '#b79ef7',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  desc: {
    fontSize: 12,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    marginBottom: hp('2%'),
  },
  desc1: {
    fontSize: 16,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 15,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    margin: 10,
  },
  textAddImgTit: {
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  containerMaleStyle1: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#fd9c93',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerMaleStyle2: {
    width: 80,
    height: 80,
    // margin: hp('2%'),
    backgroundColor: '#b79ef7',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 20,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  desc: {
    fontSize: 12,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    marginTop: 5,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
});

const mapStateToProps = state => {
  // const { feedUpload } = state.feed;
  // const loading = state.feed.loading;
  // const isProgress = state.catcre.isProgress;
  // const progress = state.catcre.progress;
  // const error = state.feed.error;
  // const videoUri = state.catcre.videoUri;
  // return { feedUpload, loading, error, isProgress, progress, videoUri };
  return {};
};

export default connect(
  mapStateToProps,
  {
    // feedUpload,
  },
)(FeedPostScreen);
