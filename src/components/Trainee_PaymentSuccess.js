import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

class Trainee_PaymentSuccess extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            isAlert: false,
            loading: false,
            alertMsg: '',
            resObj: {},
        };
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {

            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        // Actions.popTo('traineeHome');
        Actions.traineeHome();
    }

    onOkButtonPressed() {
        if (this.props.from === 'payment') {
            if (this.props.sucResponse.qst_type !== 0) {
                if (this.props.sucResponse.qst_type === 1) {
                    if (this.props.sucResponse.diet_imp_confirm === 0) {
                        Actions.aidietques();
                    } else {
                         Actions.dietHome1({ isRefresh: 'yes'});
                    }

                } else if (this.props.sucResponse.qst_type === 2 || this.props.sucResponse.qst_type === 3) {
                    if (this.props.sucResponse.workout_imp_confirm === 0) {
                        Actions.traDietWorkQues({ qst_type: this.props.sucResponse.qst_type });
                    } else if (this.props.sucResponse.diet_imp_confirm === 0) {
                        Actions.traDietWorkQues({ qst_type: this.props.sucResponse.qst_type });
                    } else {
                        Actions.traineeWorkoutsHome({ isRefresh: 'yes'});
                    }

                }
            }
            else {
                // if (this.props.sucResponse.plan_code === 'SCC' || this.props.sucResponse.plan_code === 'CCW') {
                    Actions.appointments({ isRefresh: 'yes'});
                // }
                // else {
                //     Actions.traineeHome();
                // }
            }
        }
        else if (this.props.from === 'homepage') {
            if (this.props.homeQType === 1) {
                Actions.aidietques();
            } else if (this.props.homeQType === 2 || this.props.homeQType === 3) {
                Actions.traDietWorkQues({ qst_type: this.props.homeQType });
            }
        }
        else if (this.props.from === 'cancel') {
            Actions.myOrders();
        }
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '' });
    }

    renderInfoData() {
        if (this.props.from === 'payment') {
            return (
                <View style={styles.viewTitleCal}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/img_payment_success.png')}
                        style={styles.tickStyle}
                    />
                    <Text style={styles.textCreditCard}>{this.props.sucResponse.title}</Text>
                    {this.props.sucResponse.qst_type !== 0
                        ? (
                            <Text style={styles.textDesc}>{this.props.sucResponse.message}</Text>
                        )
                        : (
                            <Text style={styles.textDesc}>{this.props.sucResponse.message}</Text>
                        )
                    }
                </View>
            );
        }

        else if (this.props.from === 'homepage') {
            return (
                <View style={styles.viewTitleCal}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/img_ask_questions.png')}
                        style={styles.tickStyle}
                    />
                    <Text style={styles.textCreditCard}>To Personalise your Experience</Text>

                    <Text style={styles.textDesc}>Can you kindly answer a few questions before proceeding</Text>

                </View>
            );
        } else if (this.props.from === 'cancel') {
            return (
                <View style={styles.viewTitleCal}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/img_unsubscribe_success.png')}
                        style={styles.imgUnSuscribe}
                    />
                    <View style={{ marginTop: 15 }}></View>
                    <Text style={styles.textCreditCard}>{this.props.sucResponse.title}</Text>
                    {this.props.sucResponse.qst_type !== 0
                        ? (
                            <Text style={styles.textDesc}>{this.props.sucResponse.message}</Text>
                        )
                        : (
                            <Text style={styles.textDesc}>{this.props.sucResponse.message}</Text>
                        )
                    }
                </View>
            );
        }
    }


    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                {/* <Loader loading={this.state.loading} /> */}

                <View style={styles.mainView}>

                    {this.renderInfoData()}

                </View>

                <View style={styles.bottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onOkButtonPressed()}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>OK</Text>

                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    mainView: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    viewTitleCal: {
        flexDirection: 'column',
        padding: 30,
    },
    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 30,
        marginLeft: 20,
        alignSelf: 'flex-start',
    },
    tickStyle: {
        width: 270,
        height: 244,
        marginTop: 15,
        alignSelf: 'center',
    },
    imgUnSuscribe: {
        width: 250,
        height: 250,
        alignSelf: 'center',
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 30,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
});

const mapStateToProps = state => {
    // const { goals, trainerCodes } = state.procre;
    // const loading = state.procre.loading;
    // const error = state.procre.error;
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_PaymentSuccess),
);

// export default ProfileCreation;