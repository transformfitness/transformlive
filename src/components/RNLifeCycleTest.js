/*Example of Reac Native Life Cycle*/
import React, { Component } from 'react';
import { Text, View, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';

class RNLifeCycleTest extends Component {
    constructor() {
        super();
        console.log('Constructor Called.');
    }

    componentWillMount() {
        console.log('componentWillMount called.');
    }

    componentDidMount() {
        console.log('componentDidMount called.');
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            Actions.pop();
            return true;
          });
    }

    componentWillReceiveProps(nextProp) {
        console.log('componentWillReceiveProps called.', nextProp);
    }

    shouldComponentUpdate(nextProp, nextState) {
        console.log('shouldComponentUpdate called.');
        return true;
    }

    componentWillUpdate(nextProp, nextState) {
        console.log('componentWillUpdate called.');
    }

    componentDidUpdate(prevProp, prevState) {
        console.log('componentDidUpdate called.');
    }

    componentWillUnmount() {
        console.log('componentWillUnmount called.');
    }

    componentDidCatch(error, info) {
        console.log('componentDidCatch called.');
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Language is: RN</Text>
                </View>
            </View>

        );
    }
}
export default RNLifeCycleTest;