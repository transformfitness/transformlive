import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    SectionList,
    ImageBackground,
    FlatList, Dimensions,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals } from '../actions';
import { ProgressBar, CustomDialog, Loader, NoInternet, ProgressBar1 } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import Orientation from 'react-native-orientation';
import ImageSlider from 'react-native-image-slider';
const platform = Platform.OS;
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import FastImage from 'react-native-fast-image';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { RNS3 } from 'react-native-aws3';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import { allowFunction } from '../utils/ScreenshotUtils.js';
const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const data = [
    {
        key: 'A', url: require('../res/ic_workout.png'), date: '19 Jan', width: 1080,
        height: 1920, title: '19 Jan',
    },
];


class Trainee_Photos extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            position: 0,
            interval: null,
            onPlayPress: false,
            columns: 3,
            deviceHeight: Dimensions.get('window').height,
            deviceWidth: Dimensions.get('window').width,
            // statusBarPaddingTop: isIPhoneX() ? 30 : platform === "ios" ? 20 : 0,
            isAlert: false,
            alertMsg: '',
            photosArray: '',
            slideArray: [],
            loading: false,
            filepath: {
                data: '',
                uri: '',
            },
            fileData: '',
            fileUri: '',
            fileType: '',
            fileName: '',
            isProgress: false,
            progress: 10,
            isInternet: false,
            noData: '',

            showSlideImage: false,
            img_count: 0,
            isAddImgPop: false,
            resObjAwsS3: {}
        };

        this.onLayout = this.onLayout.bind(this);
        this.getTraineePhotos = this.getTraineePhotos.bind(this);
    }

    componentWillMount() {
        // The getOrientation method is async. It happens sometimes that
        // you need the orientation at the moment the JS runtime starts running on device.
        // `getInitialOrientation` returns directly because its a constant set at the
        // beginning of the JS runtime.

        const initial = Orientation.getInitialOrientation();
        if (initial === 'PORTRAIT') {
            // do something
            LogUtils.infoLog1("orientation", initial.toString())
        } else {
            // do something else
        }
    }

    onLayout(e) {
        this.setState({
            deviceWidth: Dimensions.get('window').width,
            deviceHeight: Dimensions.get('window').height,
        });
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                this.getTraineePhotos();
                this.setState({
                    interval: setInterval(() => {
                        this.setState({
                            position: this.state.position === data.length ? 0 : this.state.position + 1
                        });
                    }, 3000)
                });

            }
            else {
                LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isInternet: true });
            }
        });
        // Orientation.unlockAllOrientations();

        // Orientation.addOrientationListener(this._orientationDidChange);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            // Orientation.lockToPortrait();
            this.onBackPressed();
            return true;
        });


    }

    _orientationDidChange = (orientation) => {
        if (orientation === 'LANDSCAPE') {
            LogUtils.infoLog(`Changed Device Orientation: ${orientation}`);
            // do something with landscape layout
            Orientation.lockToLandscape();
        } else {
            // do something with portrait layout
            LogUtils.infoLog(`Changed Device Orientation: ${orientation}`);
            Orientation.lockToPortrait();
        }
    }

    _orientationRemove = (orientation) => {
        orientation.lockToPortrait();
    }

    async getTraineePhotos() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/traineeimg`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.img_list.length === 0) {
                        this.setState({ photosArray: data.data.img_list, loading: false, showSlideImage: false, img_count: data.data.img_cnt });
                    }
                    else {
                        var tempArry = [];
                        var tempArry1 = [];
                        data.data.img_list.map((item) => {
                            var obj = { title: item.cdate, data: [{ key: item.cdate, list: item.img_arr, }] };
                            tempArry.push(obj);

                            item.img_arr.map((item1) => {
                                var newObj = { title: item.cdate, url: item1.img_url };
                                tempArry1.push(newObj);

                            })
                        })
                        LogUtils.infoLog1("Data Length", tempArry1.length);
                        if (tempArry1.length >= 3) {
                            this.setState({ showSlideImage: true });
                        }

                        this.setState({ photosArray: tempArry });
                        this.setState({ slideArray: tempArry1, loading: false });

                        if (this.state.photosArray) {
                            LogUtils.infoLog(this.state.photosArray);
                            LogUtils.infoLog(this.state.slideArray);
                        }

                    }


                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message, loading: false });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message, loading: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error), loading: false });
            });
    }

    componentWillUnmount() {
        this.backHandler.remove();
        clearInterval(this.state.interval);

        Orientation.getOrientation((err, orientation) => {
            LogUtils.infoLog(`Current Device Orientation: ${orientation}`);
        });


        // Remember to remove listener
        Orientation.removeOrientationListener(this._orientationRemove);
    }

    onBackPressed() {
        if (this.state.onPlayPress) {
            this.setState({ onPlayPress: false });
        }
        else if (this.state.isAddImgPop) {
            this.setState({ isAddImgPop: false });
        }
        else {
            Actions.popTo('traineeHome');
        }

    }
    onSliderBackPressed() {
        this.setState({ onPlayPress: false });
    }

    onPlayPressed(pos) {
        this.setState({ onPlayPress: true, position: pos });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    renderItem = ({ item, index }) => {
        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        return (
            <View
                style={styles.item}>
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={item.url}
                    style={{ width: wp('33.33%'), height: 120, alignSelf: 'center', borderRadius: 5, }}
                />
                {item.date
                    ? (
                        <Text style={styles.itemText}>{item.date}</Text>
                    )
                    : (
                        <Text style={styles.itemText1}></Text>
                    )
                }

            </View>
        );
    };

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderSection = ({ item }) => {
        return (
            <FlatList
                data={item.list}
                numColumns={3}
                renderItem={this.renderListItem}
                keyExtractor={this.keyExtractor}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={this.FlatListItemSeparator}
            />
        )
    }

    renderSectionHeader = ({ section }) => {
        return <Text style={styles.dateHeader}>{section.title}</Text>
    }

    renderListItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    this.onPlayPressed(index)}
                style={styles.item}>
                {/* <FastImage
                    style={{ width: wp('33%'), height: 200, borderRadius: 5, }}
                    source={{
                        uri: item.img_url,
                        headers: { Authorization: '12345' },
                        priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.center}
                /> */}

                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{
                        uri: item.img_url,
                        // cache: 'only-if-cached',
                    }}
                    style={{
                        width: wp('33%'),
                        height: 200,
                        borderRadius: 10,
                        resizeMode: 'cover',
                    }}
                />

            </TouchableOpacity>
        )
    }
    renderSlidemage() {
        if (this.state.showSlideImage) {
            return (
                <View>
                    <TouchableOpacity
                        onPress={() => this.onPlayPressed(0)}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_play_purple.png')}
                            style={styles.playImageStyle}
                        />
                    </TouchableOpacity>
                </View>
            );

        }
    }

    keyExtractor = (item) => {
        return item.ui_id
    }
    renderFlatlist() {
        if (this.state.photosArray) {
            if (Array.isArray(this.state.photosArray) && this.state.photosArray.length) {
                return (
                    <View>
                        <View style={{ marginLeft: 15, marginRight: 15, marginTop: 10, marginBottom: 10, flexDirection: 'row', alignContent: 'center', justifyContent: 'center', alignSelf: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}>My Photos</Text>

                            {this.renderSlidemage()}
                        </View>

                        <SectionList
                            contentContainerStyle={{ paddingBottom: hp('25%') }}
                            sections={this.state.photosArray}
                            style={styles.secList}
                            showsVerticalScrollIndicator={false}
                            renderSectionHeader={this.renderSectionHeader}
                            renderItem={this.renderSection}
                        />
                    </View>

                );
            }
            else {

                return (
                    <View style={{ flexDirection: 'column', flex: 1, }}>
                        <View style={{ marginLeft: 15, marginRight: 15, marginTop: 10, marginBottom: 10, flexDirection: 'row', }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                            {/* <Text style={styles.textStyle}>My Photos</Text>

                            {this.renderSlidemage()}*/}
                        </View>

                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>

                            <Image
                                source={require('../res/img_no_photos.png')}
                                style={styles.imgNoSuscribe}
                            />
                            <Text style={styles.textNodataTitle}>Keep a record</Text>
                            <Text style={styles.textNodata}>Upload your photos at regular intervals to keep track of your physical transformation. Your photos are safe with us they are completely encrypted.</Text>
                        </View>
                    </View>
                );

            }
        }
    }

    renderProgressBar() {

        if (Platform.OS === 'android') {
            return (
                <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
            );

        } else {
            return (
                <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
            );
        }

    }

    renderSlideShow() {

        if (this.state.onPlayPress) {

            return (
                <View
                    style={{
                        width: '100%',
                        height: '100%',
                        backgroundColor: '#1f1f1f',
                        flex: 1,
                    }}>
                    <TouchableOpacity
                        onPress={() => this.onSliderBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.sliderbackImageStyle}
                        />
                    </TouchableOpacity>
                    <View style={{
                        width: '100%',
                        height: '80%',
                        backgroundColor: '#1f1f1f',
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <ImageSlider
                            loopBothSides
                            autoPlayWithInterval={2000}
                            images={this.state.slideArray}
                            titleStyle={styles.sliderDate}
                            indicatorSize={0}
                            customSlide={({ index, item, style, width }) => (
                                <View key={index} style={[style, styles.customSlide]}>

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{
                                            uri: item.url,
                                            // cache: 'only-if-cached', 
                                        }}
                                        style={{ width: wp('100%'), height: hp('80%'), alignSelf: 'center', resizeMode: 'contain' }}
                                    />
                                    {/* <FastImage
                                        style={{ width: wp('100%'), height: hp('80%') }}
                                        source={{
                                            uri: item.url,
                                            headers: { Authorization: '12345' },
                                            priority: FastImage.priority.high,
                                        }}
                                        resizeMode={FastImage.resizeMode.contain}
                                    /> */}
                                    <Text style={styles.sliderDate}> {item.title} </Text>
                                </View>
                            )}
                            customButtons={(position, move) => (
                                <View style={styles.buttons}>
                                    {this.state.slideArray.map((image, index) => {

                                    })}
                                </View>
                            )}

                        />
                    </View>
                </View>

            );

        } else {

            return (

                <View style={styles.mainView}>
                    {this.renderFlatlist()}
                    {this.renderBottom()}
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                </View >
            );

        }

    }

    renderBottom() {
        if (this.state.img_count <= 500) {
            return (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                    <TouchableOpacity
                        onPress={() => this.setState({ isAddImgPop: true })}
                        style={styles.buttonTuch}>
                        <Text style={styles.buttonText}>ADD PHOTO</Text>
                    </TouchableOpacity>
                </LinearGradient>

            );
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true, isInternet: false });
                this.getTraineePhotos();

            }
            else {
                LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

    }



    chooseImage = () => {
        let options = {
            title: 'Select Image',

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                LogUtils.infoLog1('response', JSON.stringify(response));
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.validateImage(response.fileSize);

            }
        });
    };

    captureImage = () => {
        let options = {
            title: 'Select Image',
            mediaType: 'photo',
            includeBase64: false,
            maxHeight: 500,
            maxWidth: 500,

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.7,


        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.validateImage(response.fileSize);

            }
        });
    };

    async pickImagesFromGallery() {
        // Pick a single file
        try {
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            LogUtils.infoLog1('response', response);
            LogUtils.infoLog1('fileUri', response.uri);
            this.setState({
                filePath: response,
                fileData: response.data,
                fileUri: response.uri,
                fileType: response.type,
                fileName: response.name,
            });

            this.validateImage(response.size);


        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    async pickImagesFromGalleryIOS() {
        // Pick a single file
        let options = {
            title: 'Select Image',
            mediaType: 'photo',
            includeBase64: false,
            maxHeight: 500,
            maxWidth: 500,

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.7,

        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
                this.validateImage(response.fileSize);
            }
        });
    }

    async validateImage(fileSize) {
        let max_img_size = await AsyncStorage.getItem('max_img_size');
        var imgSize = parseInt(max_img_size) / 1048576;
        if (fileSize < parseInt(max_img_size)) {

            this.callUserPicsToAWSS3();
        }
        else {
            this.setState({ isAlert: true, alertMsg: `Please make sure your file size should not exceed ${imgSize}MB` });
        }
    }

    async callUserPicsToAWSS3() {
        this.setState({ progress: 0, isProgress: true });
        let userId = await AsyncStorage.getItem('userId');
        let type = '', extension = '';

        if (this.state.fileUri.includes('.png') || this.state.fileUri.includes('.PNG')) {
            type = 'image/png';
            extension = '.png';
        } else if (this.state.fileUri.includes('.jpg') || this.state.fileUri.includes('.JPG')) {
            type = 'image/jpg';
            extension = '.jpg';
        }
        else {
            if (this.state.fileName) {
                const [pluginName, fileExtension] = this.state.fileName.split(/\.(?=[^\.]+$)/);
                type = this.state.fileType;
                extension = '.' + fileExtension.toLowerCase();
            }
        }

        const file = {
            uri: this.state.fileUri,
            name: userId + '_' + new Date().getTime() + extension,
            type: type,
        }

        const options = {
            keyPrefix: await AsyncStorage.getItem('s3_userpics_path'),
            bucket: await AsyncStorage.getItem('s3_bucket'),
            region: await AsyncStorage.getItem('s3_region'),
            accessKey: await AsyncStorage.getItem('s3_key'),
            secretKey: await AsyncStorage.getItem('s3_secret'),
            successActionStatus: 201
        }
        LogUtils.infoLog1('file : ', file);
        LogUtils.infoLog1('options : ', options);

        RNS3.put(file, options)
            .progress((e) => {
                const progress = Math.floor((e.loaded / e.total) * 100);
                this.setState({ progress: progress });
            }).then(response => {
                if (response.status === 201) {
                    this.setState({ resObjAwsS3: response.body.postResponse });
                    LogUtils.infoLog1('response', response.body);
                    LogUtils.infoLog1('response1', response.body.postResponse);

                    this.callPhotoUpload();
                }
                else {
                    LogUtils.infoLog('Failed to upload image to S3');
                    LogUtils.infoLog1('response', response.body);
                    this.setState({ isProgress: false });
                }
            });

    }

    async callPhotoUpload() {
        if (this.state.fileUri) {

            let token = await AsyncStorage.getItem('token');
            let body = JSON.stringify({
                img_url: this.state.resObjAwsS3.location
            });
            LogUtils.infoLog1("body", body);
            fetch(
                `${BASE_URL}/trainee/traineeimgs3`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: body,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1("statusCode", statusCode);
                    LogUtils.infoLog1("data", data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ isProgress: false });
                        this.getTraineePhotos();

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message });
                        } else {
                            this.setState({ isProgress: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ isProgress: false, isAlert: true, alertMsg: SWR, });
                });
            // futch(`${BASE_URL}/trainee/traineeimg`, {
            //     method: 'POST',
            //     body: this.createFormData(this.state.fileUri),
            //     headers: {
            //         'Content-Type': 'multipart/form-data',
            //         Authorization: `Bearer ${token}`,
            //     },
            // }, (progressEvent) => {
            //     const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            //     this.setState({ progress: progress });
            //     LogUtils.infoLog(progress);
            // })
            //     .then(res => {
            //         const statusCode = res.status;
            //         LogUtils.infoLog1('statusCode', statusCode);
            //         const data = res.response;
            //         var obj = JSON.parse(res.response)
            //         LogUtils.infoLog1('data', data);
            //         LogUtils.infoLog1('message', data.message);
            //         if (statusCode >= 200 && statusCode <= 300) {
            //             this.setState({ isProgress: false });
            //             this.getTraineePhotos();
            //         } else {
            //             if (data.message === 'You are not authenticated!') {
            //                 this.setState({ isAlert: true, alertMsg: obj.message, isProgress: false });
            //             } else {
            //                 this.setState({ isAlert: true, alertMsg: obj.message, isProgress: false });
            //             }
            //         }
            //     })
            //     .catch(function (error) {
            //         this.setState({ isProgress: false });
            //         LogUtils.infoLog1('upload error', error);
            //         this.setState({ isAlert: true, alertMsg: JSON.stringify(error), isProgress: false });
            //     });
        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please select/capture image' });
        }
    }

    createFormData = (image) => {
        const data = new FormData();

        if (image) {
            data.append('file', {
                uri: image,
                name: image
                    .toString()
                    .replace(
                        'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
                        '',
                    ),
                type: 'image/jpeg', // or photo.type
            });
        }
        return data;
    };

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <Loader loading={this.state.loading} />


                {this.renderProgressBar()}

                {this.renderSlideShow()}

                <Dialog
                    onDismiss={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isAddImgPop}>
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15 }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <View style={{ marginTop: 10, }}></View>
                                <Text style={styles.textAddImgTit}>Add Photo</Text>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddImgPop: false });
                                                this.captureImage();
                                            }}>
                                            <View style={styles.containerMaleStyle2}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/photo.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Camera</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddImgPop: false });
                                                if (Platform.OS === 'android') {
                                                    this.pickImagesFromGallery();
                                                } else {
                                                    this.pickImagesFromGalleryIOS();
                                                }
                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Gallery</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>

            </ImageBackground>
        )
    }
}




const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
        marginTop: 10,
    },
    sliderbackImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
        tintColor: '#ffffff',
        marginTop: 20,
        marginLeft: 20
    },
    playImageStyle: {
        width: 35,
        height: 35,
        alignSelf: 'center',
    },
    mainView: {
        flexDirection: 'column',
        width: wp('100%'),
        height: hp('100%'),
    },
    textStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        flex: 1,
        fontFamily: 'Rubik-Medium',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    flatList: {
        // flex: 1,
        // marginVertical: 20,
    },
    item: {
        width: wp('33.33%'),
        alignItems: 'center',
        marginTop: 3,
        justifyContent: 'center',
        height: 200, // approximate a square
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#ffffff',
        fontSize: 8,
        textAlign: 'center',
        alignSelf: 'flex-start',
        bottom: 0,
        marginLeft: 15,
        marginBottom: 15,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        position: 'absolute',
        backgroundColor: '#8c52ff',
        borderRadius: 5,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 6,
        paddingRight: 6,
    },
    itemText1: {
        color: '#ffffff',
        fontSize: 10,
        textAlign: 'center',
        alignSelf: 'flex-start',
        bottom: 0,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        borderRadius: 5,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 6,
        paddingRight: 6,
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    sliderDate: {
        color: '#ffffff',
        fontSize: 8,
        textAlign: 'center',
        alignSelf: 'flex-start',
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        backgroundColor: '#8c52ff',
        borderRadius: 5,
        paddingTop: 1,
        position: 'absolute',
        bottom: 0,
        marginBottom: hp('15%'),
        marginLeft: 20,
        paddingBottom: 1,
        paddingLeft: 6,
        paddingRight: 6,
    },
    secList: {

    },
    dateHeader: {
        color: '#8c52ff',
        fontSize: 11,
        textAlign: 'center',
        alignSelf: 'flex-start',
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    slider: { backgroundColor: '#000', height: 350 },
    buttons: {
        zIndex: 1,
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button: {
        margin: 3,
        width: 15,
        height: 15,
        opacity: 0.9,
        alignItems: 'center',
        justifyContent: 'center',
    },
    customSlide: {
        backgroundColor: '#1f1f1f',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textAddImgTit: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    containerMaleStyle1: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle2: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
});

const mapStateToProps = state => {
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(Trainee_Photos),
);
