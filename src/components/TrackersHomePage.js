import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
    StyleSheet,
    FlatList,
    Alert,
    Image,
    TouchableOpacity,
    BackHandler,
    ScrollView,
    Text,
    SafeAreaView,
    RefreshControl
} from 'react-native';
import { CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import AppIntroSlider from 'react-native-app-intro-slider';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import moment from "moment";
import ActionButton from 'react-native-circular-action-menu';

import GoogleFit, { Scopes } from 'react-native-google-fit';
import AppleHealthKit from 'rn-apple-healthkit';
import Macros from './Home/Macros';

import { getTrackersdataAction,saveWaterAction,GetGoalsubctgAction } from '../actions/traineeActions';

const progressCustomStyles = {
    backgroundColor: '#8c52ff',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    marginTop: 15,
    marginBottom: 20,
};
const barWidth = wp('70%');

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
let trackId = 0;
let currentDate, token, fitbitToken, fitbitUserId = '', fitbitUserWeight = '';;
let stepsArray = [], caloriesArray = [], distanceArray = [];


function getData(access_token, user_id) {
    try {
        // fetch('https://api.fitbit.com/1/user/-/activities/date/today.json', {
        fetch('https://api.fitbit.com/1/user/-/activities/tracker/steps/date/today/7d.json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${access_token}`,
            },
            // body: `root=auto&path=${Math.random()}`
        })
            .then(res => res.json())
            .then(res => {
                LogUtils.infoLog1(`res:`, res);
                stepsArray = [];
                caloriesArray = [];
                res["activities-tracker-steps"].map((item) => {

                    stepsArray.push({ 'act': 'summary', 'from': item.dateTime, 'to': item.dateTime, 'steps': item.value, 'cals': 0, 'dist': 0 });
                    // stepsArray.push({ 'act': 'summary', 'c_date': item.dateTime, 'steps': item.value,'cals' : 0 });

                })
                LogUtils.infoLog1("Steps Array", stepsArray);
                if (Array.isArray(stepsArray) && stepsArray.length) {

                    LogUtils.infoLog1("body", stepsArray);
                    // sendStepsData(data);

                    getFitbitCaloriesData(fitbitToken, stepsArray);
                }
            })
            .catch(err => {
                console.error('Error: ', err);
            });
    } catch (error) {
        console.log(error);
    }
}

function getFitbitCaloriesData(access_token, data) {
    try {
        fetch('https://api.fitbit.com/1/user/-/activities/tracker/activityCalories/date/today/7d.json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${access_token}`,
            },
            // body: `root=auto&path=${Math.random()}`
        })
            .then(res => res.json())
            .then(res => {
                LogUtils.infoLog1(`res:`, res);
                if (data) {
                    data.map((dataItem) => {
                        res["activities-tracker-activityCalories"].map((item) => {

                            if (dataItem.from === item.dateTime) {
                                dataItem.cals = item.value
                            }
                        })
                    })
                }
                LogUtils.infoLog1(" Calories data", data);
                getFitbitDistanceData(fitbitToken, data);
            })
            .catch(err => {
                console.error('Error: ', err);
            });
    } catch (error) {
        console.log(error);
    }
}

function getFitbitDistanceData(access_token, data) {
    try {
        fetch('https://api.fitbit.com/1/user/-/activities/tracker/distance/date/today/7d.json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${access_token}`,
            },
            // body: `root=auto&path=${Math.random()}`
        })
            .then(res => res.json())
            .then(res => {

                LogUtils.infoLog1(`res:`, res);

                if (data) {
                    data.map((dataItem) => {

                        res["activities-tracker-distance"].map((item) => {

                            if (dataItem.from === item.dateTime) {
                                dataItem.dist = item.value * 1000
                                dataItem.from = item.dateTime + ' 00:00:00'
                                dataItem.to = item.dateTime + ' 23:59:59'
                            }

                        })

                    })

                }

                LogUtils.infoLog1("Final data", data);

                getFitbitActivitiesData(fitbitToken, data);

            })
            .catch(err => {
                console.error('Error: ', err);
            });
    } catch (error) {
        console.log(error);
    }
}

function getFitbitActivitiesData(access_token, data) {
    try {
        fetch('https://api.fitbit.com/1/user/-/activities/list.json?afterDate=' + moment().subtract(7, 'days').format("YYYY-MM-DD") + '&sort=asc&offset=0&limit=100', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${access_token}`,
            },
            // body: `root=auto&path=${Math.random()}`
        })
            .then(res => res.json())
            .then(res => {

                LogUtils.infoLog1(`res activities:`, res);

                if (res) {
                    if (res.activities.length > 0) {

                        res.activities.map((item) => {

                            if (item.logType != 'manual') {
                                var fromDate = item.originalStartTime.split('T')[0] + ' ' + item.originalStartTime.split('T')[1].split('.')[0];
                                LogUtils.infoLog1("fromDate", fromDate);
                                var milliseconds = Date.parse(item.originalStartTime); // some mock date
                                LogUtils.infoLog1("milliseconds", milliseconds);

                                var totalMillis = 0;
                                if (milliseconds) {
                                    totalMillis = milliseconds + item.originalDuration;
                                    LogUtils.infoLog1("totalMillis", totalMillis);
                                }

                                data.push({ 'act': item.activityName, 'from': fromDate, 'to': moment(totalMillis).format("YYYY-MM-DD HH:mm:ss"), 'steps': item.steps, 'cals': item.calories, 'dist': item.distance * 1000 });
                            }

                        })

                    }
                }

                LogUtils.infoLog1("Final With Activity Data", data);

                if (Array.isArray(data) && data.length) {

                    let body = JSON.stringify({
                        td_id: 1,
                        td_unq_id: fitbitUserId,
                        weight: fitbitUserWeight,
                        fitbit_token: access_token,
                        act_arr: data,
                        // cal_arr: caloriesArray,

                    });
                    LogUtils.infoLog1("body", body);
                    saveuseractivitytran(body);
                }
            })
            .catch(err => {
                console.error('Error: ', err);
            });
    } catch (error) {
        console.log(error);
    }
}

function getUserProfile(access_token) {
    try {
        fetch('https://api.fitbit.com/1/user/-/profile.json', {
            // fetch('https://api.fitbit.com/1/user/-/body/log/weight/date/2020-11-24.json', {

            method: 'GET',
            headers: {
                Authorization: `Bearer ${access_token}`,
            },
            // body: `root=auto&path=${Math.random()}`
        })
            .then(res => res.json())
            .then(res => {

                LogUtils.infoLog1(`res:`, res);
                if (res.user) {
                    fitbitUserWeight = res.user.weight;
                }

            })
            .catch(err => {
                console.error('Error: ', err);
            });
    } catch (error) {
        console.log(error);
    }
}

function saveuseractivitytran(body) {
    try {
        LogUtils.infoLog1('Url', `${BASE_URL}/trainee/saveuseractivitytran`);
        LogUtils.infoLog1("body", body);
        fetch(
            `${BASE_URL}/trainee/saveuseractivitytran`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: body,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {


                } else {
                    if (data.message === 'You are not authenticated!') {
                        // this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        // this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                // this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
            });
    } catch (error) {
        console.log(error);
    }

}

const HKPERMS = AppleHealthKit.Constants.Permissions;
const HKOPTIONS = {
    permissions: {
        read: [
            HKPERMS.StepCount,
            HKPERMS.ActiveEnergyBurned,
            HKPERMS.DistanceWalkingRunning,
            HKPERMS.Workout,
        ],
    }
};

class TrackersHomePage extends Component {
    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);
        this.state = {
            loading: false,
            resObj: {},
            noDataWorkouts: '',
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            glsArray: [],
            slideindex: 0,
            slideHeadText: '',
            isInternet: false,
            isSaveWater: false,
            refreshing: false,
            disabled: false,
            isSetUpCalorieCount:false,

            active: false,

        };
    }

    async componentDidMount() {
        try {
            allowFunction();
            token = await AsyncStorage.getItem('token');
            trackId = await AsyncStorage.getItem('trackId', 0);
            LogUtils.infoLog1('trackId', trackId);

            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    if (trackId === '1' && fitbitToken) {
                        getUserProfile(fitbitToken);
                        getData(fitbitToken);

                    }
                    else if (trackId === '2' && Platform.OS === 'android') {
                        this.sendGoogleFitData();
                    } else if (trackId === '2' && Platform.OS === 'ios') {
                        this.appleAuth();
                    }

                    // this.setState({ loading: true });
                    this.getTrackerData();

                    // let params = { "g_id": "0" };
                    // this.props.GetGoalsubctgAction(token, params);
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
                }
            });

            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {
            console.log("Error in TrackersHomepage componentDidMount : ", error);
        }
    }

    async getTrackerData() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog(`${BASE_URL}/trainee/trackerdata`);
            await this.props.getTrackersdataAction(token).then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode ', statusCode)
                LogUtils.infoLog1('data trackerdata', data)
                
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data) {
                        //let obj = data.data;
                        this.setState({ loading: false, refreshing: false, resObj: this.props.trainee.trackersData });

                        if (this.state.resObj) {

                            var myloop = [];
                            for (let i = 0; i < 8; i++) {
                                if (i < this.state.resObj.glass_drink) {
                                    myloop.push({ id: i + 1, isFill: true });
                                }
                                else {
                                    myloop.push({ id: i + 1, isFill: false });
                                }
                            }
                            this.setState({ glsArray: myloop })

                            if (this.state.resObj.food) {
                                this.setState({ slideHeadText: this.state.resObj.food[0].date_name });
                            }
                        }
                    }
                    else {
                        this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: 'No data available' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!' || statusCode === 420) {
                        this.setState({ isLogoutApp: true, loading: false, refreshing: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                    } else {
                        this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: data.message, alertTitle: 'Alert' });
                    }
                }
            }).catch(function (error) {
                this.setState({ loading: false, refreshing: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
        } catch (error) {
            console.log("Error in getTrackerData : ", error);
        }
    }

    async componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.props.from === 'feed' || this.props.from === 'rewards') {
            Actions.pop();
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    async onAccept() {
        try{
            if (this.state.alertMsg === 'You are not authenticated!') {
                AsyncStorage.clear().then(() => {
                    Actions.auth({ type: 'reset' });
                });
            }
            this.setState({ isAlert: false, alertMsg: '' });
    
            if (this.state.isSaveWater) {
                this.state.resObj.glass_drink = this.state.resObj.glass_drink + 1;
    
                let waterDrank = parseFloat(this.state.resObj.glass_drink * 0.25);
                this.state.resObj.water_drank = `${waterDrank} Ltrs`;
    
                var myloop = [];
                for (let i = 0; i < 8; i++) {
                    if (i < parseInt(this.state.resObj.glass_drink)) {
                        myloop.push({ id: i + 1, isFill: true });
                    }
                    else {
                        myloop.push({ id: i + 1, isFill: false });
                    }
                }
                this.setState({ glsArray: myloop })
    
                this.setState({ isSaveWater: false, isAlert: false, alertMsg: '', alertTitle: '' });
                // this.getHomepageDet();
                // this.getProducts();
            }
        }catch(error){
            console.log('Error in onAccept : ',error);
        }
    }

    async saveWater() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            // await saveWaterAction(token).then(data=>{
            //     console.log(data,'--data water');
            // })
            fetch(
                `${BASE_URL}/trainee/waterdet`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isSaveWater: true, isAlert: true, alertMsg: data.message, alertTitle: data.title, disabled: false });
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log('Error in saveWater : ', error);
        }
    }

    renderFoodCalDetails() {
        if (this.state.resObj) {
            if (this.state.resObj.food) {
                return (
                    <View style={{ flexDirection: 'row', width: wp('100%'), justifyContent: 'center' }}>
                        <AppIntroSlider
                            ref={ref => this.AppIntroSlider = ref}
                            style={{ width: wp('100%'), alignSelf: 'center', }}
                            slides={this.state.resObj.food}
                            // onDone={this._onDone}
                            showSkipButton={false}
                            showDoneButton={false}
                            showNextButton={false}
                            // hidePagination={false}
                            bottomButton={false}
                            dotStyle={{ backgroundColor: 'transparent' }}
                            activeDotStyle={{ backgroundColor: 'transparent' }}
                            // onSkip={this._onSkip}
                            hidePagination={true}
                            extraData={this.state}
                            // onSlideChange={this.slideChange}
                            scrollEnabled={false}
                            renderItem={({ item }) => {
                                return (
                                    <View style={{ flexDirection: 'row', flex: 1, width: wp('90%'), justifyContent: 'center', }}>
                                        <View style={styles.calViewDet}>
                                            <Text style={styles.calTitle}>{item.cal_food}</Text>
                                            <Text style={styles.calDesc}>Consumed</Text>
                                        </View>
                                        <View style={styles.calViewDet}>
                                            <AnimatedCircularProgress
                                                size={140}
                                                width={7}
                                                fill={item.cal_remain_per}
                                                tintColor="#8c52ff"
                                                arcSweepAngle={290}
                                                rotation={215}
                                                lineCap="round"
                                                backgroundColor="#e9e9e9">
                                                {
                                                    (fill) => (
                                                        <View>
                                                            <Text style={styles.calTitleBig}>{item.cal_remain}</Text>
                                                            <Text style={styles.calDesc}>Calories left</Text>
                                                        </View>

                                                    )
                                                }
                                            </AnimatedCircularProgress>

                                            <TouchableOpacity onPress={() => {
                                                LogUtils.firebaseEventLog('click', {
                                                    p_id: 213,
                                                    p_category: 'Home',
                                                    p_name: 'Home->FoodHome',
                                                });
                                                Actions.traHomeFoodNew({ mdate: item.date_food });
                                            }} style={styles.calViewDetails}>
                                                <Text style={styles.calDetail}>DETAIL</Text>
                                            </TouchableOpacity>

                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            width: wp('30%'),
                                            alignItems: 'center',
                                            padding: 15,
                                            justifyContent: 'center',
                                            marginTop: 20,
                                            marginLeft: 2,
                                        }}>
                                            <Text style={styles.calTitle}>{item.cal_burn}</Text>
                                            <Text style={styles.calDesc}>Active calories burned</Text>
                                            {this.state.resObj.info_source_url != ''
                                                ? (
                                                    <TouchableOpacity onPress={() => {
                                                        Actions.setDetail({ from: '', url: this.state.resObj.info_source_url })
                                                    }} style={{
                                                        position: 'absolute',
                                                        bottom: 15,
                                                    }}>
                                                        <Text style={styles.sourceText}>Source</Text>
                                                    </TouchableOpacity>

                                                ) : (
                                                    <View />
                                                )}
                                        </View>
                                    </View>
                                );
                            }}
                        />
                    </View>
                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textNodata}>{this.state.noDataWorkouts}</Text>
                    </View>
                );
            }
        }
    }

    myStyle = function (dyLeft) {
        if (dyLeft) {
            var v1 = parseInt(dyLeft);
            var v2 = parseInt(this.state.resObj.max_steps);

            var val1 = parseFloat(v1 / v2);
            var val2 = parseFloat(val1 * 65);
            // LogUtils.infoLog1("Val 2: ", val2);

            var val3 = Math.round(val2);

            if (val3 > 64) {
                val3 = 64;
            }

            // LogUtils.infoLog1("Val 3: ", val3);

            return {
                width: 2,
                height: 30,
                alignSelf: 'center',
                position: 'absolute',
                left: 0,
                marginLeft: wp(`${val3}%`),
            }
        }
        else {
            return {
                width: 2,
                height: 30,
                alignSelf: 'center',
                position: 'absolute',
                marginLeft: wp('1%'),
                left: 0,
            }
        }
    }

    onRefresh() {
        this.setState({
            refreshing: true,
        });
        if (trackId === '1' && fitbitToken) {
            getUserProfile(fitbitToken);
            getData(fitbitToken);
        }
        else if (trackId === '2' && Platform.OS === 'android') {
            this.sendGoogleFitData();
        } else if (trackId === '2' && Platform.OS === 'ios') {
            this.appleAuth();
        }
        setTimeout(() => { this.getTrackerData() }, 2000);
    };

    async sendGoogleFitData() {

        const options = {
            scopes: [
                Scopes.FITNESS_ACTIVITY_READ,
                // Scopes.FITNESS_ACTIVITY_READ_WRITE,
                Scopes.FITNESS_BODY_READ_WRITE,
                Scopes.FITNESS_LOCATION_READ,
            ],
        }

        GoogleFit.authorize(options)
            .then((res) => {
                if (res.success) {
                    let opt = {
                        startDate: new Date(moment().subtract(7, 'days').format("YYYY-MM-DD")).valueOf(), // simply outputs the number of milliseconds since the Unix Epoch
                        endDate: new Date().valueOf()
                    };

                    GoogleFit.getActivitySamples(opt, (err, res) => {
                        LogUtils.infoLog1("Googlefit org data", stepsArray);

                        if (res && Array.isArray(res) && res.length) {
                            stepsArray = [];

                            res.map((item) => {

                                if (item.tracked === true) {
                                    var element = { 'act': item.activityName, 'from': moment.unix(item.start / 1000).format("YYYY-MM-DD HH:mm:ss"), 'to': moment.unix(item.end / 1000).format("YYYY-MM-DD HH:mm:ss"), 'steps': 0, 'cals': 0, 'dist': 0 };

                                    if (item.quantity) {

                                        element["steps"] = item.quantity;

                                    } else {
                                        element["steps"] = 0;
                                    }

                                    if (item.calories) {

                                        element["cals"] = item.calories;

                                    } else {
                                        element["cals"] = 0;
                                    }

                                    if (item.distance) {

                                        element["dist"] = item.distance;

                                    } else {
                                        element["dist"] = 0;
                                    }

                                    stepsArray.push(element);

                                }

                            });

                            LogUtils.infoLog1("Googlefit data", stepsArray);

                            let body = JSON.stringify({
                                td_id: 2,
                                td_unq_id: '',
                                fitbit_token: '',
                                act_arr: stepsArray,

                            });

                            saveuseractivitytran(body);

                        }

                    });
                }

            })
            .catch((err) => {
                LogUtils.infoLog1('err >>> ', err)
            })
    }

    async appleAuth() {
        AppleHealthKit.isAvailable((err, available) => {
            if (available) {
                AppleHealthKit.initHealthKit(HKOPTIONS, (err, results) => {
                    if (err) {
                        //console.log("error initializing Healthkit: ", err);
                        return;
                    }
                    stepsArray = [];
                    caloriesArray = [];
                    this._fetchStepsToday_Apple();
                    // this._fetchCalories_Apple();
                });
            }
        });
    }

    _fetchStepsToday_Apple() {

        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            // type: 'Workout',
        }

        // AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
        AppleHealthKit.getDailyStepCountSamples(options, (err, res) => {

            if (err) {
                LogUtils.infoLog(err);
                return;
            }
            // LogUtils.infoLog(res);

            if (res && Array.isArray(res) && res.length) {
                let dummyStepsArray = [];

                res.map((item) => {
                    dummyStepsArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': item.value, 'cals': 0, 'dist': 0 });
                });

                if (Array.isArray(dummyStepsArray) && dummyStepsArray.length) {
                    let cDate = '';
                    let stepCount = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyStepsArray.length; ++i) {
                        prevDate = dummyStepsArray[i].from;
                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyStepsArray[i].to);
                            stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });

                            cDate = '';
                            stepCount = 0;

                        }
                        cDate = prevDate;
                        prevDate = '';
                        stepCount = stepCount + dummyStepsArray[i].steps;
                        if (i === dummyStepsArray.length - 1) {
                            stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });
                        }
                    }
                }
            }
            this._fetchDistance_Apple(stepsArray);
        });
    }

    _fetchDistance_Apple(data) {
        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            // type: 'Workout',
        }
        AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
            if (err) {
                LogUtils.infoLog(err);
                return;
            }
            // LogUtils.infoLog(res);

            if (res && Array.isArray(res) && res.length) {
                let dummyDistanceArray = [];
                res.map((item) => {
                    dummyDistanceArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': 0, 'dist': item.value });
                    // dummyDistanceArray.push(item);
                });

                if (Array.isArray(dummyDistanceArray) && dummyDistanceArray.length) {
                    let cDate = '';
                    let distance = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyDistanceArray.length; ++i) {
                        prevDate = dummyDistanceArray[i].from;

                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                            distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });
                            cDate = '';
                            distance = 0;

                        }
                        cDate = prevDate;
                        prevDate = '';
                        distance = distance + dummyDistanceArray[i].dist;
                        if (i === dummyDistanceArray.length - 1) {
                            distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });
                        }
                    }
                }

                data.map((dataItem) => {
                    distanceArray.map((item) => {
                        if (dataItem.from === item.from) {
                            dataItem.dist = item.dist
                        }
                    })
                })
                this._fetchCalories_Apple(data);
            }
        });
    }

    _fetchCalories_Apple(data) {
        const opt = {
            // startDate: "2020-03-01T00:00:17.971Z", // required
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T00:00:17.971Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required
            // type: 'walking',

        };
        AppleHealthKit.getActiveEnergyBurned(opt, (err, res) => {
            if (err) {
                LogUtils.infoLog(err);
                return;
            }

            if (res && Array.isArray(res) && res.length) {
                let dummyCaloriesArray = [];

                res.map((item) => {
                    if (!item.sourceName.includes('Health')) {
                        dummyCaloriesArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': item.value, 'dist': 0 });
                    }
                });

                if (Array.isArray(dummyCaloriesArray) && dummyCaloriesArray.length) {
                    let cDate = '';
                    let calories = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyCaloriesArray.length; ++i) {
                        prevDate = dummyCaloriesArray[i].from;

                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                            caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });
                            cDate = '';
                            calories = 0;
                        }
                        cDate = prevDate;
                        prevDate = '';
                        calories = calories + dummyCaloriesArray[i].cals;
                        if (i === dummyCaloriesArray.length - 1) {
                            caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });
                        }
                    }
                }
                data.map((dataItem) => {

                    caloriesArray.map((item) => {

                        if (dataItem.from === item.from) {
                            dataItem.cals = item.cals
                        }

                    })

                })
            }
            // LogUtils.infoLog1("Health data", data);
            this._fetchApple_Workouts(data);
        });
    }

    _fetchApple_Workouts(data) {

        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            type: 'Workout',
        }

        AppleHealthKit.getSamples(options, (err, res) => {

            if (err) {
                LogUtils.infoLog1(err);
                return;
            }

            if (res && Array.isArray(res) && res.length) {
                res.map((item) => {
                    if (item.tracked === true) {
                        data.push({ 'act': item.activityName, 'from': item.start.split('T')[0] + ' ' + item.start.split('T')[1].split('.')[0], 'to': item.end.split('T')[0] + ' ' + item.end.split('T')[1].split('.')[0], 'steps': 0, 'cals': item.calories, 'dist': item.distance });
                    }
                });

            }
            LogUtils.infoLog1("Health Final data", data);
            let body = JSON.stringify({
                td_id: 3,
                td_unq_id: '',
                fitbit_token: '',
                act_arr: data,

            });
            saveuseractivitytran(body);
        });
    }

    onWaterPressed() {
        LogUtils.firebaseEventLog('click', {
            p_id: 216,
            p_category: 'Home',
            p_name: 'Home->WaterHome',
        });
        Actions.traWaterDet();
    }

    // async calorieCounter() {
    //     try{
    //         let token = await AsyncStorage.getItem('token');
    //         let params = { "g_id": "0" };
    //         await this.props.GetGoalsubctgAction(token, params);
            
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    renderTrackerImage() {
        try{
            if (trackId) {
                return (
                    <View />
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', width: wp('90%'), }}>
                        <TouchableOpacity
                            onPress={() => {
                                Actions.trackdevices({ from: 'trackhome' });
                            }}
                        >
                            {this.state.resObj.banner_img
                                ? (
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{ uri: this.state.resObj.banner_img }}
                                        style={{
                                            width: wp('90%'),
                                            height: undefined,
                                            borderRadius: 6,
                                            alignSelf: 'center',
                                            resizeMode: 'cover',
                                            aspectRatio: 2 / 1,
                                        }}
                                    />
                                )
                                : (
                                    <Image
                                        source={require('../res/ic_noimage.png')}
                                        style={{
                                            width: wp('90%'),
                                            height: hp('25%'),
                                            borderRadius: 6,
                                            alignSelf: 'center',
                                            resizeMode: 'cover',
                                        }}
                                    />
                                )
                            }
                        </TouchableOpacity>
                    </View>
                );
            }
        }catch(error){
            console.log("Error in renderTrackerImage : ",error);
        }
    }

    renderCalorieDetails() {
        try {
            if(this.state.resObj.counter_img==""){
                return (
                    <View>
                        <View style={{ height: 15 }}></View>
                        <View style={styles.workOutBg}>
                            <View style={{ flexDirection: 'row', padding: 15, width: wp('90%'), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={styles.textPlan}>{'Food'.toUpperCase()}</Text>
    
                                <TouchableOpacity
                                    onPress={() => {
                                        LogUtils.firebaseEventLog('click', {
                                            p_id: 213,
                                            p_category: 'Home',
                                            p_name: 'Home->FoodHome',
                                        });
                                        Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") });
                                    }}>
                                    <Text style={styles.textAdd}>{'+ add'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: 0.5, backgroundColor: '#e9e9e9' }}></View>
                            <View style={styles.vHeaderTitMore}>
                                <TouchableOpacity onPress={() => {
                                    LogUtils.infoLog1('Before Index : ', this.state.slideindex);
                                    if (this.state.slideindex < this.state.resObj.food.length - 1) {
                                        this.AppIntroSlider.goToSlide(this.state.slideindex + 1);
                                        this.setState({ slideHeadText: this.state.resObj.food[this.state.slideindex + 1].date_name });
                                        this.setState({ slideindex: this.state.slideindex + 1 });
                                        LogUtils.infoLog1('After Index : ', this.state.slideindex);
                                    }
                                    LogUtils.infoLog1('After Index : ', this.state.slideindex);
                                }}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_left.png')}
                                        style={{
                                            width: 24,
                                            height: 24,
                                            alignSelf: 'center',
                                            marginLeft: 10,
                                        }}
                                    />
                                </TouchableOpacity>
                                <Text style={styles.textToday}>{this.state.slideHeadText.toUpperCase()}</Text>
                                <TouchableOpacity onPress={() => {
                                    LogUtils.infoLog1('Before Index : ', this.state.slideindex);
                                    if (this.state.slideindex !== 0) {
                                        this.AppIntroSlider.goToSlide(this.state.slideindex - 1);
                                        this.setState({ slideindex: this.state.slideindex - 1 });
                                        this.setState({ slideHeadText: this.state.resObj.food[this.state.slideindex - 1].date_name });
                                    }
                                }}>
                                    {this.state.slideindex !== 0
                                        ? (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_right.png')}
                                                style={{
                                                    width: 24,
                                                    height: 24,
                                                    alignSelf: 'center',
                                                }}
                                            />
                                        )
                                        : (
                                            <View
                                                style={{
                                                    width: 24,
                                                    height: 24,
                                                    alignSelf: 'center',
                                                }}
                                            />
                                        )}
                                </TouchableOpacity>
                            </View>
                            {this.renderFoodCalDetails()}
                        </View>
                    </View>
                )
            }else{
                return (
                    <View>
                        <View style={{ height: 15 }}></View>
                        <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', width: wp('90%'), }}>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.ageCnf();
                                }}
                            >
                                {this.state.resObj.counter_img
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: this.state.resObj.counter_img }}
                                            style={{
                                                width: wp('90%'),
                                                height: undefined,
                                                borderRadius: 6,
                                                alignSelf: 'center',
                                                resizeMode: 'cover',
                                                aspectRatio: 2 / 1,
                                            }}
                                        />
                                    )
                                    : (
                                        <Image
                                            source={require('../res/ic_noimage.png')}
                                            style={{
                                                width: wp('90%'),
                                                height: hp('25%'),
                                                borderRadius: 6,
                                                alignSelf: 'center',
                                                resizeMode: 'cover',
                                            }}
                                        />
                                    )
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderMacrosDetails(){
        try{
            if(this.state.resObj.counter_img==""){
                return (
                    <View>
                        <View style={{ height: 15 }}></View>
                        <View style={styles.workOutBg}>
                            <Macros macros={this.state.resObj} />
                        </View>
                    </View>
                )
            }
            // else{
            //     return (
            //         <View>
            //             <View style={{ height: 15 }}></View>
            //             <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', width: wp('90%'), }}>
            //                 <TouchableOpacity>
            //                     {this.state.resObj.macros_img
            //                         ? (
            //                             <Image
            //                                 progressiveRenderingEnabled={true}
            //                                 resizeMethod="resize"
            //                                 source={{ uri: this.state.resObj.macros_img }}
            //                                 style={{
            //                                     width: wp('90%'),
            //                                     height: undefined,
            //                                     borderRadius: 6,
            //                                     alignSelf: 'center',
            //                                     resizeMode: 'cover',
            //                                     aspectRatio: 2 / 1,
            //                                 }}
            //                             />
            //                         )
            //                         : (
            //                             <Image
            //                                 source={require('../res/ic_noimage.png')}
            //                                 style={{
            //                                     width: wp('90%'),
            //                                     height: hp('25%'),
            //                                     borderRadius: 6,
            //                                     alignSelf: 'center',
            //                                     resizeMode: 'cover',
            //                                 }}
            //                             />
            //                         )
            //                     }
            //                 </TouchableOpacity>
            //             </View>
            //         </View>
            //     );
            // }
        }catch(error){
            console.log("Error in renderMacrosDetails : ",error);
        }
    }

    
  animateButton() {
    if (this.state.active) {
      this.reset();
      return;
    }

    Animated.spring(this.state.anim, {
      toValue: 1,
      duration: 250,
    }).start();

    this.setState({ active: true });
  }

  reset() {
    Animated.spring(this.state.anim, {
      toValue: 0,
      duration: 250,
    }).start();

    setTimeout(() => {
      this.setState({ active: false });
    }, 250);
  }

    render() {
        return (
            <SafeAreaView style={styles.mainContainer}>
                {/* <View style={styles.mainContainer}> */}
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111, }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Trackers</Text>

                    {/* {trackId === 0
                        ? (
                            <View >

                            </View>
                        )
                        : (
                            <View style={{ position: 'absolute', zIndex: 111, right: 0 }}>
                                <TouchableOpacity
                                    style={{ marginTop: -5, backgroundColor: '#8c52ff', justifyContent: 'center', borderRadius: 5,paddingLeft : 10,paddingRight : 10,paddingTop : 5, paddingBottom : 5}}
                                    onPress={() => this.onBackPressed()}>
                                    <Text style={{
                                        fontSize: 12,
                                        fontWeight: '400',
                                        color: '#ffffff',
                                        fontFamily: 'Rubik-Regular',
                                        letterSpacing: 0.2,
                                        textAlign: 'center'
                                    }}>SYNC DATA</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    } */}


                </View>

                <ScrollView
                    ref={ref => (this.scrollView = ref)}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('15%'), marginTop: 20 }}
                    style={{ marginBottom: hp('6%'), position: 'relative', zIndex: -1 }}
                    refreshControl={
                        <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.onRefresh()} />}>

                    {/* <TouchableWithoutFeedback
                        onPress={() => Actions.trackdevices()}>

                        <View style={{ flexDirection: 'row', margin: 15 }}>

                            <AnimatedCircularProgress
                                size={40}
                                width={1}
                                fill={100}
                                rotation={-360}
                                tintColor="#8c52ff"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_linkmytracker.png')}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                                padding: 10,
                                            }}
                                        />
                                    )
                                }
                            </AnimatedCircularProgress>

                            <Text style={styles.textTitle} numberOfLines={1}>Connect with a smart tracker</Text>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={{
                                    width: 17,
                                    height: 17,
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    right: 10,
                                }}
                            />
                        </View>

                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback
                        onPress={() => Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") })}>

                        <View style={{ flexDirection: 'row', margin: 15 }}>
                            <AnimatedCircularProgress
                                size={40}
                                width={1}
                                fill={0}
                                rotation={-360}
                                tintColor="#8c52ff"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal_burn.png')}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                                padding: 10,
                                            }}
                                        />
                                    )
                                }
                            </AnimatedCircularProgress>



                            <Text style={styles.textTitle} numberOfLines={1}>0 of 500 cal eaten</Text>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={{
                                    width: 17,
                                    height: 17,
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    right: 10,
                                }}
                            />

                        </View>

                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback
                        onPress={() => Actions.traWalkDet()}>

                        <View style={{ flexDirection: 'row', margin: 15 }}>

                            <AnimatedCircularProgress
                                size={40}
                                width={1}
                                fill={50}
                                rotation={-360}
                                tintColor="#8c52ff"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_steps_tracker.png')}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                                padding: 10,
                                            }}
                                        />
                                    )
                                }
                            </AnimatedCircularProgress>



                            <Text style={styles.textTitle} numberOfLines={1}>5000 of 10000 steps walked</Text>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={{
                                    width: 17,
                                    height: 17,
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    right: 10,
                                }}
                            />
                        </View>

                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback
                        onPress={() => Actions.traWaterDet()}>

                        <View style={{ flexDirection: 'row', margin: 15 }}>

                            <AnimatedCircularProgress
                                size={40}
                                width={1}
                                fill={70}
                                rotation={-360}
                                tintColor="#8c52ff"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_glass_filled.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                                alignSelf: 'center',
                                                padding: 10,
                                            }}
                                        />
                                    )
                                }
                            </AnimatedCircularProgress>



                            <Text style={styles.textTitle} numberOfLines={1}>7 of 10 glases consumed</Text>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={{
                                    width: 17,
                                    height: 17,
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    right: 10,
                                }}
                            />
                        </View>

                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback
                        onPress={() => Actions.traCalBurned()}>

                        <View style={{ flexDirection: 'row', margin: 15 }}>

                            <AnimatedCircularProgress
                                size={40}
                                width={1}
                                fill={90}
                                rotation={-360}
                                tintColor="#8c52ff"
                                backgroundColor="#e9e9e9">
                                {
                                    (fill) => (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal_burn.png')}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                                padding: 10,
                                            }}
                                        />
                                    )
                                }
                            </AnimatedCircularProgress>



                            <Text style={styles.textTitle} numberOfLines={1}>450 of 500 cal burnt</Text>

                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_plus.png')}
                                style={{
                                    width: 17,
                                    height: 17,
                                    alignSelf: 'center',
                                    position: 'absolute',
                                    right: 10,
                                }}
                            />
                        </View>

                    </TouchableWithoutFeedback> */}

                    {/* {trackId != null
                        ? (
                            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', width: wp('90%'), }}>
                                <TouchableOpacity
                                    onPress={() => Actions.trackdevices()}>
                                    {this.state.resObj.banner_img
                                        ? (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: this.state.resObj.banner_img }}
                                                style={{
                                                    width: wp('90%'),
                                                    height: undefined,
                                                    borderRadius: 6,
                                                    alignSelf: 'center',
                                                    resizeMode: 'cover',
                                                    aspectRatio: 2 / 1,
                                                }}
                                            />
                                        )
                                        : (
                                            <Image
                                                source={require('../res/ic_noimage.png')}
                                                style={{
                                                    width: wp('90%'),
                                                    height: hp('25%'),
                                                    borderRadius: 6,
                                                    alignSelf: 'center',
                                                    resizeMode: 'cover',
                                                }}
                                            />
                                        )
                                    }
                                </TouchableOpacity>
                            </View>
                        )
                        : (
                            <View></View>
                        )
                    } */}

                    {this.renderTrackerImage()}

                    {/* calories details */}
                    {this.renderCalorieDetails()}

                   {/* Macro details */}
                   {this.renderMacrosDetails()}

                    {/* steps  details */}
                    <View style={{ height: 15 }}></View>
                    <View style={styles.workOutBg}>
                        <View style={{ flexDirection: 'row', padding: 15, width: wp('90%'), alignItems: 'center', justifyContent: 'center' }}>

                            <Text style={styles.textPlan}>
                                {'Steps'.toUpperCase()}
                            </Text>

                            <View style={{ flex: 1 }}></View>

                            <TouchableOpacity
                                onPress={() => {
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 217,
                                        p_category: 'Home',
                                        p_name: 'Home->StepsHome',
                                    });
                                    Actions.traWalkDet();
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_info_blueround.png')}
                                    style={styles.infoAppBlue}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                        <View style={styles.vSteps}>
                            <View style={styles.nutrition1}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_steps1.png')}
                                    style={{
                                        width: 30,
                                        height: 31,
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <Text style={styles.textNutTitle}>Daily Steps</Text>
                                    <Text style={styles.textNutDesc}>{this.state.resObj.step_cnt} / {this.state.resObj.max_steps} steps</Text>
                                </View>
                                <View style={styles.nutLevelBack}>
                                    <Text style={styles.nutLevel}>{this.state.resObj.steps_staus}</Text>
                                </View>
                            </View>
                            <View style={styles.sliderImageContainer}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_home_slider.png')}
                                    style={styles.sliderImageStyle}
                                />
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_home_verline.png')}
                                    style={this.myStyle(this.state.resObj.step_cnt)}
                                />
                            </View>
                        </View>
                    </View>

                    {/* water plan details */}
                    <View style={{ height: 15 }}></View>
                    <View style={styles.workOutBg}>
                        <View style={{ flexDirection: 'row', padding: 15, width: wp('90%'), alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={styles.textPlan}>{'Water'.toUpperCase()}</Text>
                            <TouchableOpacity
                                onPress={() => this.onWaterPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_info_blueround.png')}
                                    style={styles.infoAppBlue}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                        <View style={{ flexDirection: 'column', flex: 1, }}>
                            <FlatList
                                data={this.state.glsArray}
                                numColumns={8}
                                extraData={this.state}
                                style={{ marginTop: 10, marginBottom: 10, }}
                                // keyExtractor={item => item.id}
                                keyExtractor={(item, index) => item.id}
                                showsVerticalScrollIndicator={false}
                                // ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    return <View key={item.id} style={{ flexDirection: 'row', flex: 1, padding: 5 }}>
                                        {item.isFill
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_glass_det_fill.png')}
                                                    style={{
                                                        width: 30,
                                                        height: 39,
                                                        marginRight: 1,
                                                        alignSelf: 'center',
                                                    }}
                                                />
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    if (!this.state.disabled) {
                                                        this.setState({ disabled: true })
                                                        this.saveWater();
                                                    }

                                                }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_glass_add.png')}
                                                        style={{
                                                            width: 30,
                                                            height: 39,
                                                            marginRight: 1,
                                                            alignSelf: 'center',
                                                        }}
                                                    />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                }} />

                            <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                            <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center', justifyContent: 'flex-start', padding: 10 }}>
                                <Text style={styles.textValue}>{this.state.resObj.water_drank}</Text>

                                <Text style={styles.textRecWater}>{this.state.resObj.water_limit}</Text>
                            </View>
                        </View>
                    </View>

                    {/* Calories  burned  */}
                    <View style={{ height: 15 }}></View>
                    <View style={styles.workOutBg}>
                        <View style={{ flexDirection: 'row', padding: 15, width: wp('90%'), alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={styles.textPlan}>{'Calories Burned'.toUpperCase()}</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    LogUtils.firebaseEventLog('click', {
                                        p_id: 217,
                                        p_category: 'Home',
                                        p_name: 'Home->CaloriesBurned',
                                    });
                                    Actions.traCalBurned();
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_info_blueround.png')}
                                    style={styles.infoAppBlue}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                        <View style={styles.vSteps}>
                            <View style={styles.nutrition1}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal_burn.png')}
                                    style={{
                                        width: 30,
                                        height: 30,
                                        // tintColor: '#E52D12',
                                        alignSelf: 'flex-start',
                                    }}
                                />
                                <View style={styles.nutritionInnerView}>
                                    <Text style={styles.textNutTitle}>Active Calories Burned</Text>
                                    <Text style={styles.textNutDesc}>{this.state.resObj.cal_burn} / {this.state.resObj.cal_limit} cal</Text>
                                </View>
                                <View style={styles.nutLevelBack}>
                                    <Text style={styles.nutLevel}>{this.state.resObj.cal_staus}</Text>
                                </View>
                            </View>
                            <View style={styles.sliderImageContainer}>
                                <ProgressBarAnimated
                                    {...progressCustomStyles}
                                    width={barWidth}
                                    height={10}
                                    style={styles.activityIndicatorWrapper}
                                    // value={this.getCalBurnVal(this.state.resObj.cal_burn)}
                                    value={(this.state.resObj.cal_burn / this.state.resObj.cal_limit) * 100}
                                    // maxValue={100}
                                    onComplete={() => {
                                        // Alert.alert('Hey!', 'onComplete event fired!');
                                        // LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                                        // this.setState({ isRewardComplete: true, taskType: item.task_type, taskName: item.title });
                                        // LogUtils.infoLog1('isRewardComplete', this.state.isRewardComplete);
                                    }}
                                />
                            </View>
                        </View>
                    </View>


                </ScrollView>

                {
                    this.state.resObj.counter_img == "" &&

                    <ActionButton size={65}
                        radius={110}
                        position='right'
                        bgColor="rgba(0, 0, 0, 0.7)"
                        buttonColor="#8c52ff">

                        <ActionButton.Item
                            buttonColor={'transparent'}
                            size={60}
                            title="New Task"
                            onPress={() => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: 218,
                                    p_category: 'Home',
                                    p_name: 'Home->Breakfast',
                                });
                                Actions.traFoodList({ mFrom: "home", title: "Breakfast", foodType: 2, mdate: moment(new Date()).format("YYYY-MM-DD") });
                            }}>

                            <View style={styles.vArcMenu}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_breakfast_blue.png')}
                                    style={styles.arcImg}
                                />
                                <Text style={styles.arcTitle}>Breakfast</Text>
                            </View>
                        </ActionButton.Item>

                        <ActionButton.Item
                            buttonColor={'transparent'}
                            size={60}
                            title="Notifications"
                            onPress={() => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: 219,
                                    p_category: 'Home',
                                    p_name: 'Home->Lunch',
                                });
                                Actions.traFoodList({ mFrom: "home", title: "Lunch", foodType: 4, mdate: moment(new Date()).format("YYYY-MM-DD") });
                            }}>
                            <View style={styles.vArcMenu}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_lunch_blue.png')}
                                    style={styles.arcImg}
                                />
                                <Text style={styles.arcTitle}>Lunch</Text>
                            </View>
                        </ActionButton.Item>

                        <ActionButton.Item
                            buttonColor={'transparent'}
                            size={60}
                            title="All Tasks"
                            onPress={() => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: 220,
                                    p_category: 'Home',
                                    p_name: 'Home->Dinner',
                                });
                                Actions.traFoodList({ mFrom: "home", title: "Dinner", foodType: 6, mdate: moment(new Date()).format("YYYY-MM-DD") });
                            }}>
                            <View style={styles.vArcMenu}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_dinner_blue.png')}
                                    style={styles.arcImg}
                                />
                                <Text style={styles.arcTitle}>Dinner</Text>
                            </View>
                        </ActionButton.Item>

                        <ActionButton.Item
                            buttonColor={'transparent'}
                            size={60}
                            title="All Tasks"
                            onPress={() => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: 221,
                                    p_category: 'Home',
                                    p_name: 'Home->Snack',
                                });
                                Actions.traFoodList({ mFrom: "home", title: "Snack", foodType: 5, mdate: moment(new Date()).format("YYYY-MM-DD") });
                            }}>
                            <View style={styles.vArcMenu}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_snacks_blue.png')}
                                    style={styles.arcImg}
                                />
                                <Text style={styles.arcTitle}>Snack</Text>
                            </View>
                        </ActionButton.Item>

                    </ActionButton>
                }


                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
               
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },

    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
    },
    textTitle: {
        fontSize: 13,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        // lineHeight: 14,
        color: '#2d3142',
        marginLeft: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        letterSpacing: .7,
    },

    workOutBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 5,
    },
    vHeaderTitMore: {
        width: wp('87%'),
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
    },
    textPlan: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
    },
    calViewDet: {
        flexDirection: 'column',
        width: wp('30%'),
        alignItems: 'center',
        padding: 15,
        justifyContent: 'center'
    },

    calTitle: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calDesc: {
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
        lineHeight: 15,
    },
    calTitleBig: {
        fontSize: 30,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calViewDetails: {
        width: 60,
        borderRadius: 100,
        borderColor: '#e9e9e9',
        borderStyle: 'solid',
        padding: 5,
        marginTop: -25,
        borderWidth: 1,
        backgroundColor: '#ffffff',
    },
    calDetail: {
        color: '#6d819c',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        alignSelf: 'center',
        fontWeight: '800',
    },
    sourceText: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#8c52ff',
        textAlign: 'center',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    infoAppBlue: {
        width: 25,
        height: 25,
        // marginRight: 10,
        alignSelf: 'center',
    },
    vSteps: {
        flexDirection: 'column',
        width: wp('90%'),
        padding: 15,
        // height: hp('19%'),
    },
    nutrition1: {
        flexDirection: 'row',
    },
    nutritionInnerView: {
        flexDirection: 'column',
        marginLeft: wp('4%'),
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
    },
    textNutTitle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textNutDesc: {
        fontSize: 14,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    nutLevelBack: {
        width: wp('25%'),
        height: hp('5%'),
        borderRadius: 15,
        alignSelf: 'flex-end',
        backgroundColor: '#e1ddf5',
        justifyContent: 'center',
    },
    nutLevel: {
        fontSize: 12,
        fontWeight: '500',
        color: '#8c52ff',
        lineHeight: 14,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.17,
        alignSelf: 'center',
    },
    sliderImageContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: wp('2%'),
        marginLeft: wp('10%'),
        flex: 1,
    },
    sliderImageStyle: {
        width: wp('70%'),
        height: 30,
        resizeMode: 'contain'
    },
    textValue: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 15,
        color: '#282c37',
    },
    textRecWater: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
        flex: 1,
        textAlign: 'right',
        alignSelf: 'flex-end',
        alignSelf: 'center',
        lineHeight: 18,
    },
    activityIndicatorWrapper: {
        backgroundColor: '#f1f1f1',
        width: '95%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 10,
        paddingBottom: 20,
    },
    textToday: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
    },
    arcImg: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        zIndex: -2,
    },
    arcTitle: {
        alignSelf: 'center',
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        lineHeight: 14,
        marginTop: 5,
        zIndex: -2,
        marginBottom: 50,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
    },
    vArcMenu: {
        width: 65,
        height: 65,
        flexDirection: 'column',
    }
});


const mapStateToProps = state => {
    const trainee = state.traineeFood;
    const calCountSetup = state.traineeFood.calCounterSetupStatus;
    
    return { trainee,calCountSetup };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTrackersdataAction ,saveWaterAction,GetGoalsubctgAction},
    )(TrackersHomePage),
);

//export default TrackersHomePage;