import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    FlatList,
    Dimensions,
    Image,
    BackHandler,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25);
import Slider from '@react-native-community/slider';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isSelected(selArray) {
    let selected = false;
    selArray.map((item) => {
        if (item.check) {
            selected = true;
        }
    })
    return selected;
}

import { CustomDialog, Loader } from './common';
let subCatIds = '', subcatVals = '';
let weight = '';

class MyProfile_SubGoal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sliderValue: 0,
            maxSliderValue: 0,
            minSliderValue: 0,
            arrSubCatList: [],
            noDataMsg: '',
            loading: false,
            isAlert: '',
            alertMsg: '',
            goalSubId: 0,
            tarWeight: '',
            goalCode: '',
            goalName: '',
            mBMTitle: 'Select type of marathon you want to run?',
            measurementvalue: 0,

        };
    }

    async componentDidMount() {
        allowFunction();
        StatusBar.setHidden(true);
        // if (this.props.targetWeight) {
        //     var value = parseInt(this.props.targetWeight);
        // } else {
        var value = parseInt(this.props.weight);
        // }

        weight = parseInt(this.props.weight);

        this.setState({
            goalId: this.props.goalId, goalSubId: this.props.goalSubId,
            goalName: this.props.goalName,

        });

        if (this.props.goalCode === 'WL' || this.props.goalCode === 'PP') {
            this.setState({ maxSliderValue: value, sliderValue: value });
        }
        else if (this.props.goalCode === 'WG') {
            if (value < 150) {
                this.setState({ minSliderValue: value, maxSliderValue: 150, sliderValue: value });
            }
            else {
                this.setState({ minSliderValue: value, maxSliderValue: 200, sliderValue: value });
            }
        }

        if (this.props.goalCode === 'RM') {
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.getSubGoals();
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
                }
            });
        }

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.props.isFocused) {
                Actions.pop();
            } else {
                this.props.navigation.goBack(null);
            }
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async getSubGoals() {
        LogUtils.infoLog1(JSON.stringify({
            g_id: this.props.goalId,
        }))
        fetch(
            `${BASE_URL}/master/goalsubctg`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    // Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    g_id: this.props.goalId,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data) {
                        data.data.map((item) => {
                            if (item.id === this.state.goalSubId) {
                                item.check = true;
                            } else {
                                item.check = false;
                            }
                            return data.data;
                        })

                        this.setState({ loading: false, arrSubCatList: data.data });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }




    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onButtonPressed() {
        if (this.props.goalCode === 'WL' || this.props.goalCode === 'PP') {
            if (Math.floor(this.state.sliderValue) === Math.floor(weight)) {
                this.setState({ isAlert: true, alertMsg: 'Target weight cannot be equal to your weight.Please change your target weight' });
            } else {
                await AsyncStorage.setItem('trgt_weight', this.state.sliderValue.toString());
                this.setState({ tarWeight: this.state.sliderValue.toString() });
                this.saveSubGoalDetails();
            }
        }
        else if (this.props.goalCode === 'WG') {
            if (Math.floor(this.state.sliderValue) === Math.floor(weight)) {
                this.setState({ isAlert: true, alertMsg: 'Desired weight cannot be equal to your weight.Please change your desired weight' });
            } else {
                await AsyncStorage.setItem('trgt_weight', this.state.sliderValue.toString());
                this.setState({ tarWeight: this.state.sliderValue.toString() });
                this.saveSubGoalDetails();
            }
        }
        else if (this.props.goalCode === 'RM') {
            if (this.state.goalSubId !== 0 && isSelected(this.state.arrSubCatList)) {
                this.setState({ tarWeight: '0' });
                await AsyncStorage.setItem('trgt_weight', '0');
                this.saveSubGoalDetails();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please select a marathon type' });
            }
        }
    }

    async saveSubGoalDetails() {
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            g_id: this.state.goalId,
            gt_id: this.state.goalSubId,
            target_weight: this.state.tarWeight,
            mg_id: subCatIds,
            mg_val: subcatVals,
        }));
        fetch(
            `${BASE_URL}/user/updategoaldet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    g_id: this.state.goalId,
                    gt_id: this.state.goalSubId,
                    target_weight: this.state.tarWeight,
                    mg_id: subCatIds,
                    mg_val: subcatVals,

                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data) {
                        this.setState({ loading: false });
                        Actions.myProGoalInfo({ goalName: this.state.goalName, resObj: data });

                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    press = (hey) => {
        this.state.arrSubCatList.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalSubId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }

    renderListAndDet() {

        return (
            <FlatList
                contentContainerStyle={{ paddingBottom: hp('35%') }}
                data={this.state.arrSubCatList}
                keyExtractor={item => item.id}
                extraData={this.state}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item }) => {
                    return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                        this.press(item)
                    }}>
                        <View >
                            <Text style={styles.countryText}>{`${item.name}`}</Text>
                        </View>
                        <View style={styles.mobileImageStyle}>
                            {item.check
                                ? (
                                    <TouchableOpacity onPress={() => {
                                        this.press(item)
                                    }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/check.png')}
                                            style={styles.mobileImageStyle} />
                                    </TouchableOpacity>
                                )
                                : (
                                    <TouchableOpacity onPress={() => {
                                        this.press(item)
                                    }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/uncheck.png')}
                                            style={styles.mobileImageStyle} />
                                    </TouchableOpacity>
                                )}
                        </View>
                    </TouchableOpacity>
                }} />
        );
    }

    renderSubGoalData() {
        if (this.props.goalCode === 'WL' || this.props.goalCode === 'PP') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your target weight?
                    </Text>
                    <Text style={styles.weighttextStyle}>
                        {Math.floor(this.state.sliderValue)} Kg
                    </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                        What is your weight loss goal?
                                    </Text> */}
                </View>
            )
        }
        else if (this.props.goalCode === 'WG') {
            return (
                <View>

                    <Text style={styles.subtextOneStyle}>
                        What is your desired weight?
                    </Text>
                    <Text style={styles.weighttextStyle}>
                        {Math.floor(this.state.sliderValue)} Kg
                    </Text>
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={this.state.minSliderValue}
                        maximumValue={this.state.maxSliderValue}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                    {/* <Text style={styles.subtextOneStyle}>
                    What is your weight loss goal?
            </Text> */}
                </View>
            )
        }
        else if (this.props.goalCode === 'RM') {
            return (
                <View>
                    <Text style={styles.subtextOneStyle}>
                        Select type of marathon you want to run?
                    </Text>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('35%') }}
                        data={this.state.arrSubCatList}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                this.press(item)
                            }}>
                                <View >
                                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                                </View>
                                <View style={styles.mobileImageStyle}>
                                    {item.check
                                        ? (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/check.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => {
                                                this.press(item)
                                            }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/uncheck.png')}
                                                    style={styles.mobileImageStyle} />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            )
        }
    }

    render() {

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <View style={styles.optionInnerContainer}>
                </View>
                <TouchableOpacity
                    onPress={() => this.onBackPressed()}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_back.png')}
                        style={styles.backImageStyle}
                    />
                </TouchableOpacity>

                <View style={styles.optionInnerContainer}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_app.png')}
                        style={styles.appImageStyle}
                    />
                </View>

                <View style={styles.containericonStyle}>
                    {this.renderSubGoalData()}
                </View>

                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onButtonPressed()}>
                            <Text style={styles.buttonText}>Submit</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    interval: {
        width: 1,
        backgroundColor: '#8c52ff',
    },
    medium: {
        height: 25,
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('7%'),
        marginLeft: 20,
        marginBottom: 20,
        alignSelf: 'flex-start',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: -45,
    },
    textStyle: {
        fontSize: 18,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    txtMeasurement: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        letterSpacing: 0.23,
        marginLeft: -3,
        marginRight: 5,
        fontFamily: 'Rubik-Regular',
        marginBottom: 2,
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('3%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        marginBottom: hp('2%'),
        fontFamily: 'Rubik-Medium',
    },
    sliderImageStyle: {
        width: wp('90%'),
        height: hp('5%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    weighttextStyle: {
        fontSize: 20,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'left',
        marginLeft: wp('10%'),
        alignSelf: 'flex-start',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    viewPlusMinus: {
        width: wp('25%'),
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    countryText1: {
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    txtAvgVal: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        marginLeft: -5,
        marginRight: 5,
        fontFamily: 'Rubik-Medium',
    },
    profileImage: {
        width: 60,
        height: 60,
        aspectRatio: 1,
        backgroundColor: "#ffffff",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 60 / 2,
        resizeMode: "cover",
        alignSelf: 'center',
    },
});

const mapStateToProps = state => {
    const { subGoals } = state.masters;
    const loading = state.masters.loading;
    const error = state.masters.error;
    return { loading, error, subGoals };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(MyProfile_SubGoal),
);