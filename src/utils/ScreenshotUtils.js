// screenshot handling code in android
import { NativeModules } from 'react-native';
import {Platform} from 'react-native';

export const forbidFunction = async () => {
  try {
    if (Platform.OS === 'android') {
      const result = await NativeModules.PreventScreenshotModule.forbid();
      //console.log(result);
    }

  } catch (e) {
    console.log(e);
  }
}

export const allowFunction = async () => {
  try {
    if (Platform.OS === 'android') {
      const result = await NativeModules.PreventScreenshotModule.allow();
      //console.log(result);
    }
  } catch (e) {
    console.log(e);
  }
}