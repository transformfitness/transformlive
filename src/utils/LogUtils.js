const isShowLog = true; //dev -- true live -- false

import slowlog from 'react-native-slowlog';
import firebase from 'react-native-firebase';
import NetInfo from "@react-native-community/netinfo";
import { AppEventsLogger } from "react-native-fbsdk";
import appsFlyer from 'react-native-appsflyer';

const LogUtils = {

    infoLog: (msg) => {
        if (isShowLog) {
            if (msg) {
                return console.log(msg);
            }
        }
        else {
            return;
        }
    },

    infoLog1: (msg1, msg2) => {
        if (isShowLog) {
            return console.log(msg1, msg2);
        }
        else {
            return;
        }
    },

    showSlowLog: (context) => {
        if (isShowLog) {
            return slowlog(context, /.*/);
        }
        else {
            return;
        }
    },

    // firebaseSetScreenView: (eventName, screenName) => {
    //     NetInfo.fetch().then(state => {
    //         if (state.isConnected) {
    //             console.log('firebase set screen called');
    //             return firebase.analytics().logEvent(eventName, screenName);
    //             // return firebase.analytics().setCurrentScreen(screenName, screenName);
    //         }
    //         else {
    //             return;
    //         }
    //     });
    // },

    firebaseEventLog: (eventName, object) => {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                // console.log('firebase event log called');
                return firebase.analytics().logEvent(eventName, object);
            }
            else {
                return;
            }
        });

    },

    facebookEventLog: (eventName, number, object) => {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                return AppEventsLogger.logEvent(eventName, number, object);
            }
            else {
                return;
            }
        });

    },

    facebookEventLog1: (eventName) => {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                return AppEventsLogger.logEvent(eventName);
            }
            else {
                return;
            }
        });

    },

    appsFlyerEventLog: (eventName, object) => {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                // console.log('firebase event log called');
                return appsFlyer.logEvent(eventName, object, (res) => {
                    console.log(eventName, res);
                }, (err) => { console.log(eventName, err); });
            }
            else {
                return;
            }
        });
    },


}

export default LogUtils;