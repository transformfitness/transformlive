import { BASE_URL,
  GET_TRAINEE_FEEDS, 
  GET_TRAINEE_FEEDS_SUCCESS, 
  GET_TRAINEE_FEEDS_FAILURE,GET_TRAINEE_FEEDS_LOADMORE, 
  GET_TRAINEE_FEEDS_LOADMORE_SUCCESS,SAVE_COMMENT,SAVE_LIKE,SAVE_POLL,SAVE_POLL_SUCCESS,
  SAVE_POLL_FAILURE,SAVE_FEED,SAVE_FEED_SUCCESS,SAVE_FEED_FAILURE,SAVE_LIKE_FAILURE,PROGRESS_BAR_CHANGED,
  SAVE_FOLLOW,SAVE_FOLLOW_SUCCESS,SAVE_FOLLOW_FAILURE,
    } from '../actions/types';
  import {Alert} from 'react-native';
  import {Actions} from 'react-native-router-flux';
  import AsyncStorage from '@react-native-community/async-storage';
 
 
   export const getTraineeFeed = ({token, pageNo}) => {
     console.log(token);
     return dispatch => {
       dispatch({type: GET_TRAINEE_FEEDS});
      console.log('url ', `${BASE_URL}/trainee/feed?page=${pageNo}&pagesize=10`);
     
       fetch(
         `${BASE_URL}/trainee/feed?page=${pageNo}&pagesize=10`,
         {
           method: 'GET',
           headers: {
             Accept: 'application/json',
             'Content-Type': 'application/json',
             Authorization: `Bearer ${token}`,
           },
          //  body: JSON.stringify({
          //   page: pageNo,
          //   pagesize: 10,
          // }),
         },
       )
         .then(processResponse)
         .then(res => {
           const {statusCode, data} = res;
           console.log('statusCode', statusCode);
           console.log('data', data);
           if (statusCode >= 200 && statusCode <= 300) {
            data.data.map((item) => {

              if (item.is_follow === 1) {
                item.followcheck = true;
              }else {
                item.followcheck = false;
              }
              if (item.is_poll === 1) {
                item.pollcheck = true;
              }else {
                item.pollcheck = false;
              }
              if (item.is_like === 1) {
                item.check = true;
              }else {
                item.check = false;
              }
              
              return data.data;
            })
            dispatch({
              type: GET_TRAINEE_FEEDS_SUCCESS,
              payload: data.data,
            });
           } else {
             if (data.message === 'You are not authenticated!') {
              getFeedsFailedFailed(dispatch, data.message);
             } else {
              getFeedsFailed(dispatch, data.message);
             }
           }
         })
         
         .catch(function(error) {
           // console.log(error);
           getFeedsFailed(dispatch, error);
         
         });
        };
    };

    export const getTraineeFeedLoadMore = ({token, pageNo}) => {
      console.log(token);
      return dispatch => {
        dispatch({type: GET_TRAINEE_FEEDS_LOADMORE});
       console.log('url ', `${BASE_URL}/trainee/feed?page=${pageNo}&pagesize=10`);
      
        fetch(
          `${BASE_URL}/trainee/feed?page=${pageNo}&pagesize=10`,
          {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
           //  body: JSON.stringify({
           //   page: pageNo,
           //   pagesize: 10,
           // }),
          },
        )
          .then(processResponse)
          .then(res => {
            const {statusCode, data} = res;
            console.log('statusCode', statusCode);
            console.log('data', data);
            if (statusCode >= 200 && statusCode <= 300) {
             dispatch({
               type: GET_TRAINEE_FEEDS_LOADMORE_SUCCESS,
               payload: data.data,
             });
            } else {
              if (data.message === 'You are not authenticated!') {
               getFeedsFailedFailed(dispatch, data.message);
              } else {
               getFeedsFailed(dispatch, data.message);
              }
            }
          })
          
          .catch(function(error) {
            // console.log(error);
            getFeedsFailed(dispatch, error);
          
          });
         };
     };
 
   
 
   const getFeedsFailedFailed = (dispatch, error) => {
     dispatch({
       type: GET_TRAINEE_FEEDS_FAILURE,
       payload: error,
     });
     if (error) {
       setTimeout(() => {
         Alert.alert(
           'Alert',
           error,
           [
             {
               text: 'OK',
               onPress: () => {
                AsyncStorage.clear().then(() => {
                  Actions.auth({type: 'reset'});
                });
                 // Actions.dailyReport();
               },
             },
           ],
           {cancelable: false},
         );
       }, 1000);
     }
   };
   
   const getFeedsFailed = (dispatch, error) => {
     dispatch({
       type: GET_TRAINEE_FEEDS_FAILURE,
       payload: error,
     });
     if (error) {
       setTimeout(() => {
         Alert.alert(
           'Alert',
           error,
           [
             {
               text: 'OK',
               onPress: () => {
               
                 // Actions.dailyReport();
               },
             },
           ],
           {cancelable: false},
         );
       }, 1000);
     }
   };

   export const uploadComment = (body, token) => {
    return dispatch => {
      console.log("body",body);
      dispatch({type: SAVE_COMMENT});
      fetch(`${BASE_URL}/trainee/comment`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: body,
      })
        .then(processResponse)
        .then(res => {
          const {statusCode, data} = res;
          console.log('statusCode', statusCode);
          console.log('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            // dispatch({
            //   type: SAVE_COMMENT_SUCCESS,
            //   payLoad: text,
            // });
          } else {
            if (data.message === 'You are not authenticated!') {
              getFeedsFailedFailed(dispatch, data.message);
            } else {
              getFeedsFailed(dispatch, data.message);
            }
          }
        })
        .catch(function(error) {
          //console.log(error);
          getFeedsFailed(dispatch, error);
        });
    };
  };

  export const uploadLike = (body, token) => {
    return dispatch => {
      console.log("body",body);
      dispatch({type: SAVE_LIKE});
      fetch(`${BASE_URL}/trainee/like`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: body,
      })
        .then(processResponse)
        .then(res => {
          const {statusCode, data} = res;
          console.log('statusCode', statusCode);
          console.log('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            // dispatch({
            //   type: SAVE_COMMENT_SUCCESS,
            //   payLoad: text,
            // });
          } else {
            if (data.message === 'You are not authenticated!') {
              getFeedsFailedFailed(dispatch, data.message);
            } else {
              getFeedsFailed(dispatch, data.message);
            }
          }
        })
        .catch(function(error) {
          //console.log(error);
          getFeedsFailed(dispatch, error);
        });
    };
  };

  export const uploadPoll = (body, token) => {
    return dispatch => {
      console.log("body",body);
      dispatch({type: SAVE_POLL});
      fetch(`${BASE_URL}/trainee/Feedpolloptn`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: body,
      })
        .then(processResponse)
        .then(res => {
          const {statusCode, data} = res;
          console.log('statusCode', statusCode);
          console.log('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            dispatch({
              type: SAVE_POLL_SUCCESS,
              payload: data.data,
            });
          } else {
            if (data.message === 'You are not authenticated!') {
              getFeedsFailedFailed(dispatch, data.message);
            } else {
              getFeedsFailed(dispatch, data.message);
            }
          }
        })
        .catch(function(error) {
          //console.log(error);
          getFeedsFailed(dispatch, error);
        });
    };
  };

  export const uploadFollow = (body, token) => {
    return dispatch => {
      console.log("body",body);
      dispatch({type: SAVE_FOLLOW});
      fetch(`${BASE_URL}/trainee/saveUserFollow`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: body,
      })
        .then(processResponse)
        .then(res => {
          const {statusCode, data} = res;
          console.log('statusCode', statusCode);
          console.log('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            dispatch({
              type: SAVE_FOLLOW_SUCCESS,
              payload: data.data,
            });
          } else {
            if (data.message === 'You are not authenticated!') {
              getFeedsFailedFailed(dispatch, data.message);
            } else {
              getFeedsFailed(dispatch, data.message);
            }
          }
        })
        .catch(function(error) {
          //console.log(error);
          getFeedsFailed(dispatch, error);
        });
    };
  };

  const futch = (url, opts={}, onProgress) => {
    console.log(url, opts)
    return new Promise( (res, rej)=>{
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers||{})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
  }


  export const feedUpload = (token, image, video, body) => {
    // console.log('reimburseMentsUpload this.state.fileType', type);
    return dispatch => {
      console.log("url",`${BASE_URL}/trainee/Feed`);
      console.log("body",body);
      dispatch({type: SAVE_FEED});
      futch(`${BASE_URL}/trainee/Feed`, {
        method: 'POST',
        body: createFormData(image,video, body),
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`,
        },
      }, (progressEvent) => {
        const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
        dispatch({type: PROGRESS_BAR_CHANGED,payLoad: progress});
        console.log(progress);
      })
      // .then(processResponse)  
      .then(res => 
        {
          const statusCode = res.status;
          console.log('statusCode', statusCode);
          const data = res.response;
          // const {statusCode, data} = res;
          console.log('data', data);
          console.log('message', "Feed Uploaded Successfully");
          if (statusCode >= 200 && statusCode <= 300) {
            feedUploadSucess(dispatch, 'Feed Uploaded Successfully');
          } else {
            if (data.message === 'You are not authenticated!') {
              feedPostErrorAuth(dispatch, data.message);
            } else {
              feedPostError(dispatch, data.message);
            }
          }
      })
      .catch(function(error) {
       console.log('upload error', error);
       feedPostError(dispatch, error);
      });
      // .then((res) => console.log(res), (err) => console.log(err))
    };
  };

  const createFormData = (image,video, body) => {
    const data = new FormData();
    // console.log('createFormData this.state.fileType', filetype);
    // if (imageVideo) {
      // const photos = [video, image1,image2,image3,image4,image5,image6]
      console.log("video url", video);
      if(video)
      {
        data.append('file', {
          uri: video,
          name: video
            .toString()
            .replace(
              'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
              '',
            ),
          type: 'video/mp4', // or photo.type
        }); 
      }
      else if(image){
        data.append('file', {
          uri: image,
          name: image
            .toString()
            .replace(
              'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
              '',
            ),
          type: 'image/jpeg', // or photo.type
        }); 
      }        
    // }
    data.append('data', body);
    // console.log(data);
    return data;
  };


  const feedUploadSucess = (dispatch, text) => {
    dispatch({
      type: SAVE_FEED_SUCCESS,
      payLoad: text,
    });
  
    if (text) {
      setTimeout(() => {
        Alert.alert(
          'Alert',
          text,
          [
            {
              text: 'OK',
              onPress: () => {
                Actions.wall();
              },
            },
          ],
          {cancelable: false},
        );
      }, 1000);
    }
  };

  const feedPostError = (dispatch, error) => {
    dispatch({
      type: SAVE_FEED_FAILURE,
      payLoad: error,
    });
  
    if (error) {
      setTimeout(() => {
        Alert.alert(
          'Alert',
          JSON.stringify(error),
          [
            {
              text: 'OK',
              onPress: () => {
                // Actions.dailyReport();
              },
            },
          ],
          {cancelable: false},
        );
      }, 1000);
    }
  };
  const feedPostErrorAuth = (dispatch, error) => {
    dispatch({
      type: SAVE_FEED_FAILURE,
      payLoad: error,
    });
  
    if (error) {
      setTimeout(() => {
        Alert.alert(
          'Alert',
          error,
          [
            {
              text: 'OK',
              onPress: () => {
                // Actions.dailyReport();
                try {
                  // db.transaction(tx => {
                  //   tx.executeSql('DROP TABLE IF EXISTS table_projects', []);
                  //   tx.executeSql('DROP TABLE IF EXISTS table_spentfor', []);
                  //   tx.executeSql('DROP TABLE IF EXISTS table_leavetype', []);
                  //   console.log('Table Deleted');
                  // });
                  // eslint-disable-next-line no-undef
                  AsyncStorage.clear().then(() => {
                    Actions.auth({type: 'reset'});
                  });
                } catch (error) {
                  //console.log(error.message);
                }
              },
            },
          ],
          {cancelable: false},
        );
      }, 1000);
    }
  };

   
 
  function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
      statusCode: res[0],
      data: res[1],
    }));
  }
 
   
 