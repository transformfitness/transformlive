import {
  BASE_URL,
  PROFILE_REFRESH,
  GET_TRAINEE_GOALS,
  GET_TRAINEE_GOALS_SUCCESS,
  GET_TRAINEE_GOALS_FAILURE,
  GET_TRAINER_CODES,
  GET_TRAINER_CODES_SUCCESS,
  GET_TRAINER_CODES_FAILURE,
  SAVE_PROFILE,
  SAVE_PROFILE_SUCCESS,
  SAVE_PROFILE_FAILURE,
  GET_PROFILE_DET,
  GET_PROFILE_DET_SUCCESS,
  UPDATE_PROFILE_DET_LIST,
  UPDATE_PROFILE_DET_SUCCESS,
  UPDATE_PROFILE_BASIC_INFO,
  UPDATE_PROFILE_BASIC_INFO_SUCCESS,
  GET_WEIGHT_DET,
  GET_WEIGHT_DET_SUCCESS,
  UPDATE_WEIGHT,
  UPDATE_WEIGHT_SUCCESS,
  SAVE_PROFILE_PIC,
  SAVE_PROFILE_PIC_SUCCESS,
  SAVE_PROFILE_PIC_FAILURE,
} from '../actions/types';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CustomDialog } from '../components/common';
import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export const profileRefresh = () => {
  return dispatch => {
    dispatch({
      type: PROFILE_REFRESH,
    });
  };
};

const getTrainerCodesFailedFailed = (dispatch, error) => {
  dispatch({
    type: GET_TRAINER_CODES_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const getTrainerCodesFailed = (dispatch, error) => {
  dispatch({
    type: GET_TRAINER_CODES_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

export const getTrainerCodes = () => {
  // console.log(token);
  return dispatch => {
    dispatch({ type: GET_TRAINER_CODES });
    fetch(
      `${BASE_URL}/master/trainer`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        //console.log('statusCode', statusCode);
        //console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          dispatch({
            type: GET_TRAINER_CODES_SUCCESS,
            payload: data.data,
          });
        } else {
          if (data.message === 'You are not authenticated!') {
            getTrainerCodesFailed(dispatch, data.message);
          } else {
            getTrainerCodesFailedFailed(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        getTrainerCodesFailedFailed(dispatch, error);

      });
  };
};





const profilePostError = (dispatch, error) => {
  dispatch({
    type: SAVE_PROFILE_FAILURE,
    payLoad: error,
  });

  if (error) {
    if (JSON.stringify(error).includes('line')) {
      setTimeout(() => {
        Alert.alert(
          'Alert',
          'There seems to be a network issue.Please try again later',
          [
            {
              text: 'OK',
              onPress: () => {
                // Actions.dailyReport();
              },
            },
          ],
          { cancelable: false },
        );
      }, 1000);
    }else {
      setTimeout(() => {
        Alert.alert(
          'Alert',
          JSON.stringify(error),
          [
            {
              text: 'OK',
              onPress: () => {
                // Actions.dailyReport();
              },
            },
          ],
          { cancelable: false },
        );
      }, 1000);
    }
    
  }
};

const profilePostErrorAuth = (dispatch, error) => {
  dispatch({
    type: SAVE_PROFILE_FAILURE,
    payLoad: error,
  });

  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              try {

                AsyncStorage.clear().then(() => {
                  Actions.auth({ type: 'reset' });
                });
              } catch (error) {
                //console.log(error.message);
              }
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};



const profileUploadSucess = (dispatch, text) => {
  dispatch({
    type: SAVE_PROFILE_SUCCESS,
    payload: text,
  });
};


export const uploadProfile = (photo, body, type) => {
  try{
    return dispatch => {
      // console.log("body", body);
      // console.log("photo", photo);
      dispatch({ type: SAVE_PROFILE });
      fetch(`${BASE_URL}/user/signup`, {
        method: 'POST',
        body: createFormData(photo, body, type),
        headers: {
          // Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(processResponse)
        .then(res => {
          console.log('-----res', res);
          const { statusCode, data } = res;
          //console.log('statusCode', statusCode);
          //console.log('data', data);

          if (statusCode >= 200 && statusCode <= 300) {
            storeItem(dispatch, 'token', data.token, data.is_trainer.toString(),
              data.userId.toString(), data.phno.toString(), data.goal_id.toString(), 
              data.goal_name.toString(), data.weight.toString(), data.height.toString(),
               data.userName.toString(), data.gender_id.toString(), data.prf_img.toString(),
               data.language_id.toString(),data.language_name.toString(), data.message,
               data.goal_code, data.present_weight.toString(), data.weight_target.toString(), data.complete_date,
                data.calperday.toString(), data.sucessmsg, data.goal_text, data.quotationtext, data.quotationby,JSON.stringify(data.info_android),JSON.stringify(data.info_ios));
          } else {
            if (data.message === 'You are not authenticated!') {
              profilePostErrorAuth(dispatch, data.message);
            } else {
              profilePostError(dispatch, data.message);
            }
          }
        })
        .catch(function (error) {
          console.log(error);
          profilePostError(dispatch, error);
        });
    };
  }catch(error){
    console.log('Error in uploadProfile',error)
  }
};

const storeItem = async (dispatch, key, item, trainer, userId, phno, goalId, goalName, 
  weight, height, userName, gender_id, profImg,language_id,language_name, message,
  goal_code, present_weight, weight_target, complete_date,calperday, sucessmsg, goalText, quotationtext, quotationby,info_android,info_ios) => {
  try {
    console.log('-----storeItem', key, item, trainer, userId, phno, goalId, goalName, 
    weight, height, userName, gender_id, profImg,language_id,language_name, message,
    goal_code, present_weight, weight_target, complete_date,calperday, sucessmsg, goalText, quotationtext, quotationby,info_android,info_ios);
    await AsyncStorage.setItem('token', item);
    await AsyncStorage.setItem('istrainer', trainer);
    await AsyncStorage.setItem('userId', userId);
    await AsyncStorage.setItem('phno', phno);
    await AsyncStorage.setItem('goalId', goalId);
    await AsyncStorage.setItem('goalName', goalName);
    await AsyncStorage.setItem('weight', weight);
    await AsyncStorage.setItem('height', height);
    await AsyncStorage.setItem('userName', userName);
    await AsyncStorage.setItem('genderId', gender_id);
    await AsyncStorage.setItem('profileImg', profImg);
    await AsyncStorage.setItem('languageId', language_id);
    await AsyncStorage.setItem('language_name', language_name);

    await AsyncStorage.setItem('goal_code', goal_code);
    await AsyncStorage.setItem('present_weight', present_weight);
    await AsyncStorage.setItem('weight_target', weight_target);
    await AsyncStorage.setItem('complete_date', complete_date);
    await AsyncStorage.setItem('calperday', calperday);
    await AsyncStorage.setItem('sucessmsg', sucessmsg);
    await AsyncStorage.setItem('goal_text', goalText);
    await AsyncStorage.setItem('quotes', quotationtext);
    await AsyncStorage.setItem('author', quotationby);

    await AsyncStorage.setItem('info_android', info_android);
    await AsyncStorage.setItem('info_ios', info_ios);

    await AsyncStorage.setItem(key, item).then(
      profileUploadSucess(dispatch, message),
    );
  } catch (error) {
    console.log(error.message);
  }
};

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

const createFormData = (photo, body, type) => {
  const data = new FormData();
  // console.log('createFormData this.state.fileType', filetype);
  if (photo) {
    data.append('file', {
      uri: photo,
      name: photo
        .toString()
        .replace(
          'content://com.fitness.provider/root/storage/emulated/0/Pictures/images/',
          '',
        ),
      type: 'image/jpeg',
    });
  }

  data.append('data', body);
  // console.log(data);
  return data;
};

const createFormData1 = (photo) => {
  const data = new FormData();
  // console.log('createFormData this.state.fileType', filetype);
  if (photo) {
    data.append('file', {
      uri: photo,
      name: photo
        .toString()
        .replace(
          'content://com.fitness.provider/root/storage/emulated/0/Pictures/images/',
          '',
        ),
      type: 'image/jpeg',
    });
  }

  // data.append('data', body);
  // console.log(data);
  return data;
};

export const updateProfilePic = (photo,token) => {
  return dispatch => {
    dispatch({ type: SAVE_PROFILE_PIC });
    fetch(`${BASE_URL}/trainee/updateprofileimg`, {
      method: 'POST',
      body: createFormData1(photo),
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },

    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        //console.log('statusCode', statusCode);
        //console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          dispatch({
            type: SAVE_PROFILE_PIC_SUCCESS,
            payload: data.data,
          });
        } else {
          if (data.message === 'You are not authenticated!') {
            profilePostErrorAuth(dispatch, data.message);
          } else {
            profilePostError(dispatch, data.message);
          }
        }
      })
      .catch(function (error) {
        //console.log(error);
        profilePostError(dispatch, error);
      });
  };
};

export const getProfileDet = ({ token }) => {
  //console.log(token);
  return dispatch => {
    dispatch({ type: GET_PROFILE_DET });
    fetch(
      `${BASE_URL}/user/getuserdet`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        // console.log('statusCode', statusCode);
        //console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          dispatch({
            type: GET_PROFILE_DET_SUCCESS,
            payload: data.data,
          });
        } else {
          if (data.message === 'You are not authenticated!') {
            profilePostErrorAuth(dispatch, data.message);
          } else {
            profilePostError(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        profilePostError(dispatch, error);

      });
  };
};

export const updateProfileDet = ({ token, type_id, type_data, isveg, others}) => {
 
  return dispatch => {
    dispatch({ type: UPDATE_PROFILE_DET_LIST });
    fetch(
      `${BASE_URL}/user/updateprofiledet`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          type_id: type_id,
          type_data: type_data.toString(),
          is_veg: isveg,
          other_mst: others,
          like_food: '',
          dislike_food: ''
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

          profileUpdateSucess(dispatch, data.message);
        } else {
          if (data.message === 'You are not authenticated!') {
            profilePostErrorAuth(dispatch, data.message);
          } else {
            profilePostError(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        profilePostError(dispatch, error);

      });
  };
};



const profileUpdateSucess = (dispatch, text) => {
  dispatch({
    type: UPDATE_PROFILE_DET_SUCCESS,
    payload: text,
  });
  // if (text) {
  //   setTimeout(() => {
  //     Alert.alert(
  //       'Alert',
  //       text,
  //       [
  //         {
  //           text: 'OK',
  //           onPress: () => {
  //            Actions.myProfile();
  //           },
  //         },
  //       ],
  //       {cancelable: false},
  //     );
  //   }, 1000);
  // }
};

export const updateProfileBasicInfo = ({ token, data }) => {
  return dispatch => {
    dispatch({ type: UPDATE_PROFILE_BASIC_INFO });
    fetch(
      `${BASE_URL}/user/updateuserinfo`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: data,
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

          profileInfoSucess(dispatch, data.message);
        } else {
          if (data.message === 'You are not authenticated!') {
            profilePostErrorAuth(dispatch, data.message);
          } else {
            profilePostError(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        profilePostError(dispatch, error);

      });
  };
};

const profileInfoSucess = (dispatch, text) => {
  dispatch({
    type: UPDATE_PROFILE_BASIC_INFO_SUCCESS,
    payload: text,
  });
};

export const getHeightDetails = ({ token }) => {
  try{

    return dispatch => {
      dispatch({ type: GET_WEIGHT_DET });
      fetch(
        `${BASE_URL}/trainee/weightdet`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          console.log('statusCode', statusCode);
          console.log('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            dispatch({
              type: GET_WEIGHT_DET_SUCCESS,
              payload: data.data,
            });
          } else {
            if (data.message === 'You are not authenticated!') {
              profilePostErrorAuth(dispatch, data.message);
            } else {
              profilePostError(dispatch, data.message);
            }
          }
        })
  
        .catch(function (error) {
          // console.log(error);
          profilePostError(dispatch, error);
  
        });
    };
  }catch(error){
    console.log(error.message)
  }
};



