import React,{useEffect} from 'react';
import LogUtils from './utils/LogUtils.js';
import { Scene, Router, Actions } from 'react-native-router-flux';

//Splash , Launch and Login
import LaunchingScreen from './components/LaunchingScreen';
import AppIntroScreens from './components/AppIntroScreens';
import SplashScreen from './components/SplashScreen';
import LoginForm from './components/LoginForm';
import OtpVerification from './components/OtpVerification';

// Registration
import ProfileCreation from './components/ProfileCreation';
import Age from './components/Age';
import Gender from './components/Gender';
import Height_New from './components/Height_New';
import Goal from './components/Goal';
import TargetWeight from './components/TargetWeight';
import ActivityLevel from './components/ActivityLevel';
import Profession from './components/Profession';
import WorkoutLocation from './components/WorkoutLocation';
import EquipmentAccess from './components/EquipmentAccess';
import FitnessForm from './components/FitnessForm';
import Medicalcondition from './components/Medicalcondition';
import ProfilePicture from './components/ProfilePicture';
import Trainee_GoalCompleted from './components/Trainee_GoalCompleted';
import AppIntroSliderDemo from './components/AppIntroSlider_Demo';
import FoodAllergies from './components/FoodAllergies';
import FoodPreferences from './components/FoodPreferences';
import TraineeLanguage from './components/TraineeLanguage';

//Calorie counter
import AgeCnf from './components/caloriesetup/AgeConfirm';
import GenderCnf from './components/caloriesetup/GenderConfirm';
import HeightWeightCnf from './components/caloriesetup/HeightWeightConfirm';


// Home Root 

// App HomePage
import TraineeHome from './components/TraineeHome';

//My profile
import MyProfile from './components/MyProfile';
import MyProfile_Update from './components/MyProfile_Update';
import MyProfile_Update_BasicInfo from './components/MyProfile_Update_BasicInfo';
import MyProfile_SubGoal from './components/MyProfile_SubGoal';
import MyProfile_GoalInfo from './components/MyProfile_GoalInfo';
import BodyMeasurements from './components/BodyMeasurements';
import BodyMeasurements_Home from './components/BodyMeasurements_Home';

// Programs
import Trainee_WorkoutsHome from './components/Trainee_WorkoutsHome';
import WorkoutsHomePage from './components/WorkoutsHomePage';
import Trainee_PlanDetailsNew from './components/Trainee_PlanDetailsNew';
import Trainee_PlanDayIntro from './components/Trainee_PlanDayIntro';
import ProgramVideoPlayer from './components/ProgramVideoPlayer';
import ProgramVideoPlayerIOS from './components/ProgramVideoPlayerIOS';
import Trainee_AllWorkoutList from './components/Trainee_AllWorkoutList';
import Trainee_Discover from './components/Trainee_Discover';
import TrainersList from './components/TrainersList';
import TraineefitnessformsList from './components/TraineefitnessformsList';
import DiscoverHomePage from './components/DiscoverHomePage';

// MySubscriptions
import MyOrders from './components/MyOrders';
import Trainee_AllBuyPlansLIst from './components/Trainee_AllBuyPlansLIst';
import TraineeAllbuyPlansList from './components/TraineeAllbuyPlansList';
import Trainee_BuyPlanDetails from './components/Trainee_BuyPlanDetails';
import Trainee_BuyPlanDetailsNew from './components/Trainee_BuyPlanDetailsNew';
import Trainee_BuyPlanModify from './components/Trainee_BuyPlanModify';
import Trainee_PaymentSuccess from './components/Trainee_PaymentSuccess';
import TraineeDietWorkQuestions from './components/TraineeDietWorkQuestions';
import AIDietPlanQuestions from './components/AIDietPlanQuestions';
import Trainee_BuyPlanPayMethod from './components/Trainee_BuyPlanPayMethod';

// Feed
import Trainee_Wall from './components/Trainee_Wall';
import FeedPostScreen from './components/FeedPostScreen';
import Trainee_FeedDetails from './components/Trainee_FeedDetails';
import CustomVideoPlayer from './components/CustomVideoPlayer';
import AFVideoPlayer from './components/AFVideoPlayer';
import WebView from './components/Webview';

// Food
import Trainee_Home_Food_New from './components/Trainee_Home_Food_New';
import Trainee_FoodList from './components/Trainee_FoodList';
import Trainee_AddFood from './components/Trainee_AddFood';
import Trainee_Food_Details from './components/Trainee_Food_Details';
import Trainee_CreateFood from './components/Trainee_CreateFood';

// Appointments
import Appointments from './components/Appointments';
import Appointment_Book from './components/Appointment_Book';
import Appointment_FreeBook from './components/Appointment_FreeBook';
import Appointment_Success from './components/Appointment_Success';

// Rewards
import Trainee_Rewards_Home from './components/Trainee_Rewards_Home';
import Trainee_RewardRedemption from './components/Trainee_RewardRedemption';

//Diet
import Trainee_DietHome1 from './components/Trainee_DietHome1';
import Trainee_FoodPhotos from './components/Trainee_FoodPhotos';

// Help
import Trainee_Help from './components/Trainee_Help';
import Trainee_Help_Book from './components/Trainee_Help_Book';
import Trainee_Help_Success from './components/Trainee_Help_Success';

// Weight Home
import Trainee_Home_Weight from './components/Trainee_Home_Weight';

// Water Home
import Trainee_WaterDetails from './components/Trainee_WaterDetails';

// Steps Home
import Trainee_WalkDetails from './components/Trainee_WalkDetails';

// cal burned Home
import Trainee_CaloriesBurned from './components/Trainee_CaloriesBurned';


// Photos
import Trainee_Photos from './components/Trainee_Photos';

// Link my tracker
import TrackingScreen from './components/TrackingScreen';
import CalorieCounterSetup from '../src/components/caloriesetup/CalorieCounterSetup';

// Refer A Friend
import ReferFriend from './components/ReferFriend';

// Settings
import Settings from './components/Settings';
import Settings_Detail from './components/Settings_Detail';

// Notifications
import Notifications from './components/Notifications';

//Challenges
import Trainee_Challenges from './components/Trainee_Challenges';
import ChallengeDetails from './components/ChallengeDetails';
import Trainee_Challenge_Winners from './components/Trainee_Challenge_Winners';
import ChallengeRewardRedemption from './components/ChallengeRewardRedemption';
import AcceptedChallengeDetails from './components/AcceptedChallengeDetails';

//MyMedicalRecords
import MyMedicalRecords from './components/MyMedicalRecords';
import MyMedicalRecords_Add from './components/MyMedicalRecords_Add';

//Mental Fitness
import MentalFitnessList from './components/MentalFitnessList';
import MentalFitnessDetails from './components/MentalFitnessDetails';

// Order Food
import OrderFoodList from './components/OrderFoodList';

import Chat from './components/Chat';
import BarcodeScan from './components/BarcodeScan';
import RecordVideo from './components/RecordVideo';
import PlayVideo from './components/PlayVideo';
import VideoPlayer from './components/VideoPlayer';
import NewVideoPlayer from './components/NewVideoPlayer';
import GymVersion from './components/GymVersion';
import ShortVideoPlayer from './components/ShortVideoPlayer.js';
import Trainee_PCFDetails from './components/Trainee_PCFDetails';
import OneOnOneChat from './components/OneOnOneChat';
import ChatList from './components/ChatList';
import AppTour from './components/Apptour';
import WorkoutsMore from './components/WorkoutsMore.js';
import TrackersHomePage from './components/TrackersHomePage.js';
import Trainee_covid_programme from './components/Trainee_covid_programme';
import NetInfo from "@react-native-community/netinfo";

// import GoogleCastExample from './components/GoogleCastExample';
// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}

const Routers = () => {
  return (
    <Router
      onStateChange={(prevState, currentState) => {
        const currentRouteName = getActiveRouteName(currentState);
        const previousRouteName = getActiveRouteName(prevState);
        // console.log('currentRouteName', currentRouteName);
        // console.log('previousRouteName', previousRouteName);
        if (previousRouteName !== currentRouteName) {
          LogUtils.firebaseEventLog('openScreen', {
            screenName: getComponentName(currentRouteName),
          });

          LogUtils.appsFlyerEventLog(getComponentName(currentRouteName).toLowerCase(), {
            screenName: getComponentName(currentRouteName),
          });


          // LogUtils.facebookEventLog('openScreen', 0, {
          //   screenName: getComponentName(currentRouteName),
          // });

          LogUtils.facebookEventLog1(getComponentName(currentRouteName));
        }
      }}>
      <Scene key="root" hideNavBar>
        <Scene key="launch">
          <Scene key="launch" component={LaunchingScreen} hideNavBar initial />
        </Scene>

        <Scene key="splash">
          <Scene key="splash" component={SplashScreen} hideNavBar />
        </Scene>

        <Scene key="auth">
          <Scene key="login" component={LoginForm} hideNavBar />
        </Scene>

        <Scene key="appintro">
          <Scene key="Intro" component={AppIntroScreens} hideNavBar />
        </Scene>

        <Scene key="otp">
          <Scene key="otpVerification" component={OtpVerification} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="webview" component={WebView} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
        </Scene>
        <Scene key="web">
          <Scene key="webview" component={WebView} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
        </Scene>

        {/* Registration */}
        <Scene key="profile">
          <Scene key="profileCreation" component={ProfileCreation} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="age" component={Age} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="gender" component={Gender} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="heightnew" component={Height_New} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="goal" component={Goal} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="targetweight" component={TargetWeight} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="actLevel" component={ActivityLevel} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="profession" component={Profession} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="workLoc" component={WorkoutLocation} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="eqpAcc" component={EquipmentAccess} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="fitnessForm" component={FitnessForm} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="medical" component={Medicalcondition} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="profilePic" component={ProfilePicture} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traGoalCompl" component={Trainee_GoalCompleted} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="appIntroSliderDemo" component={AppIntroSliderDemo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="foodAlgs" component={FoodAllergies} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="foodPref" component={FoodPreferences} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traineelanguage" component={TraineeLanguage} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
        </Scene>


        {/* Main Root */}
        <Scene
          key="thome"
          headerLayoutPreset="center"
          titleStyle={{
            color: '#282c37',
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 16,
            width: '100%',
          }}
          backButtonTextStyle={{ color: '#282c37' }}
          barButtonIconStyle={{ tintColor: '#282c37' }}
          navigationBarStyle={{ backgroundColor: '#ffffff' }}
          backButtonTintColor="#282c37">

          {/* App Home */}
          <Scene key="traineeHome" component={TraineeHome} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* My profile */}
          <Scene key="myProfile" component={MyProfile} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="myProfUpdate" component={MyProfile_Update} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="myProfBasicInfo" component={MyProfile_Update_BasicInfo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="myProSubGoal" component={MyProfile_SubGoal} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="myProGoalInfo" component={MyProfile_GoalInfo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="bodymeasure" component={BodyMeasurements} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="bodymeasureHome" component={BodyMeasurements_Home} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Programs */}
          <Scene key="traineeWorkoutsHome" component={Trainee_WorkoutsHome} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="WorkoutsHome" component={WorkoutsHomePage} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traineePlanNew" component={Trainee_PlanDetailsNew} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traPlanIntro" component={Trainee_PlanDayIntro} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="proVideoPlay" component={ProgramVideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="proVideoPlayIOS" component={ProgramVideoPlayerIOS} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traineeWorkouts" component={Trainee_AllWorkoutList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="workoutsmore" component={WorkoutsMore} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traDiscover" component={Trainee_Discover} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="trainersList" component={TrainersList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traFormlist" component={TraineefitnessformsList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="disHomePage" component={DiscoverHomePage} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          

          {/* MySubscriptions */}
          <Scene key="myOrders" component={MyOrders} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traAllBuyPlans" component={Trainee_AllBuyPlansLIst} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traAllBuyPlans1" component={TraineeAllbuyPlansList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traBuyPlanDet" component={Trainee_BuyPlanDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traBuyPlanDetNew" component={Trainee_BuyPlanDetailsNew} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traBuyPlanModify" component={Trainee_BuyPlanModify} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traBuySuccess" component={Trainee_PaymentSuccess} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traDietWorkQues" component={TraineeDietWorkQuestions} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="aidietques" component={AIDietPlanQuestions} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traSelPayMethod" component={Trainee_BuyPlanPayMethod} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />



          {/* Feed */}
          <Scene key="wall" component={Trainee_Wall} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="feedpost" component={FeedPostScreen} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="feeddetails" component={Trainee_FeedDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="customPlayer" component={CustomVideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="afPlayer" component={AFVideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="webview" component={WebView} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Food */}
          <Scene key="traHomeFoodNew" component={Trainee_Home_Food_New} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traFoodList" component={Trainee_FoodList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traAddFood" component={Trainee_AddFood} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traFoodDet" component={Trainee_Food_Details} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traCreateFood" component={Trainee_CreateFood} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />


          {/* Appointments */}
          <Scene key="appointments" component={Appointments} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="appBook" component={Appointment_Book} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="appFreeBook" component={Appointment_FreeBook} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="appSuccess" component={Appointment_Success} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Rewards */}
          <Scene key="rewardHome" component={Trainee_Rewards_Home} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="rewardRedeem" component={Trainee_RewardRedemption} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Diet */}
          <Scene key="dietHome1" component={Trainee_DietHome1} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traFoodPhotos" component={Trainee_FoodPhotos} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Help */}
          <Scene key="traHelp" component={Trainee_Help} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traHelpBook" component={Trainee_Help_Book} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traHelpSucc" component={Trainee_Help_Success} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Weight Home */}
          <Scene key="traHomeWeight" component={Trainee_Home_Weight} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Water Home */}
          <Scene key="traWaterDet" component={Trainee_WaterDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Steps Home */}
          <Scene key="traWalkDet" component={Trainee_WalkDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Calories Burned Home */}
          <Scene key="traCalBurned" component={Trainee_CaloriesBurned} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />


          {/* Photos */}
          <Scene key="traPhotos" component={Trainee_Photos} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Link My Tracker */}
          <Scene key="trackdevices" component={TrackingScreen} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* calorie counter setup AgeCnf GenderCnf HeightWeightCnf */}
          <Scene key="ageCnf" component={AgeCnf} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="genderCnf" component={GenderCnf} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="heightWeightCnf" component={HeightWeightCnf} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="actLevel" component={ActivityLevel} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="calorieCounterSetup" component={CalorieCounterSetup} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="appIntroSliderDemo" component={AppIntroSliderDemo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Refer A Friend */}
          <Scene key="refer" component={ReferFriend} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Settings */}
          <Scene key="settings" component={Settings} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="setDetail" component={Settings_Detail} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Notifications */}
          <Scene key="notifi" component={Notifications} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Challenges */}
          <Scene key="traChallenges" component={Trainee_Challenges} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="challengedetails" component={ChallengeDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="accChallenge" component={AcceptedChallengeDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="chaRewards" component={ChallengeRewardRedemption} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="traineechallengWinners" component={Trainee_Challenge_Winners} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />


          {/* MyMedicalRecords */}
          <Scene key="myMdlReds" component={MyMedicalRecords} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="myMdlRedsAdd" component={MyMedicalRecords_Add} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Mental Fitness */}
          <Scene key="mentList" component={MentalFitnessList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="mentDetails" component={MentalFitnessDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          {/* Order Food */}
          <Scene key="orderFood" component={OrderFoodList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />


          <Scene key="liveChat" component={Chat} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="barcodeScan" component={BarcodeScan} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="recordVideo" component={RecordVideo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="play" component={PlayVideo} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="videoplay" component={VideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="newvideoplay" component={NewVideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="gymVersion" component={GymVersion} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="shortVideoPlayer" component={ShortVideoPlayer} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          
          <Scene key="pcfdetails" component={Trainee_PCFDetails} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="oneononechat" component={OneOnOneChat} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          <Scene key="chatlist" component={ChatList} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          <Scene key="trackershome" component={TrackersHomePage} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />

          <Scene key="covidprogramme" component={Trainee_covid_programme} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} />
          
          {/* <Scene key="apptour" component={AppTour} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false}/> */}

          {/* <Scene key="googleCast" component={GoogleCastExample} hideNavBar drawerLockMode='locked-closed' gesturesEnabled={false} /> */}
          

        </Scene>

      </Scene>
    </Router>
  );
};

function getComponentName(key) {
  let CName = '';
  // login & registration
  if (key === 'launch') {
    CName = 'SplashScreen';
  }
  else if (key === 'Intro') {
    CName = 'AppIntroPage';
  }
  else if (key === 'splash') {
    CName = 'AboutApp';
  }
  else if (key === 'login') {
    CName = 'RequestOTP';
  }
  else if (key === 'otpVerification') {
    CName = 'VerifyOTP';
  }
  else if (key === 'profileCreation') {
    CName = 'ProfileCreation';
  }
  else if (key === 'gender') {
    CName = 'ProfileGender';
  }
  else if (key === 'age') {
    CName = 'ProfileAge';
  }
  else if (key === 'heightnew') {
    CName = 'ProfileHeight';
  }
  else if (key === 'targetweight') {
    CName = 'ProfileTargetWeight';
  }
  else if (key === 'foodAlgs') {
    CName = 'ProfileFoodAllergies';
  }
  else if (key === 'medical') {
    CName = 'ProfileMedicalcondition';
  }
  else if (key === 'actLevel') {
    CName = 'ProfileActivityLevel';
  }
  else if (key === 'foodPref') {
    CName = 'ProfileFoodPreferences';
  }
  else if (key === 'eqpAcc') {
    CName = 'ProfileEquipmentAccess';
  }
  else if (key === 'goal') {
    CName = 'ProfileGoal';
  }
  else if (key === 'profession') {
    CName = 'ProfileProfession';
  }
  else if (key === 'workLoc') {
    CName = 'ProfileWorkoutLocation';
  }
  else if (key === 'fitnessForm') {
    CName = 'ProfileFitnessForm';
  }
  else if (key === 'profilePic') {
    CName = 'ProfilePicture';
  }
  else if (key === 'traGoalCompl') {
    CName = 'ProfileUserGoal';
  }
  else if (key === 'traineeHome') {
    CName = 'AppHomePage';
  }
  //programs 
  else if (key === 'traineeWorkoutsHome') {
    CName = 'ProgramHomePage';
  }else if (key === 'WorkoutsHome') {
    CName = 'Workouts Home Page';
  }
  else if (key === 'traineePlanNew') {
    CName = 'ProgramIntroDetails';
  }
  else if (key === 'traPlanIntro') {
    CName = 'ProgramDayIntro';
  }
  else if (key === 'traineeWorkouts') {
    CName = 'ProgramsMore';
  }
  else if (key === 'traDiscover') {
    CName = 'ProgramsDiscover';
  }
  else if (key === 'proVideoPlay') {
    CName = 'ProgramVideoPlayer';
  }
  else if (key === 'proVideoPlayIOS') {
    CName = 'ProgramVideoPlayerIOS';
  }
  //feed
  else if (key === 'wall') {
    CName = 'Feed';
  }
  else if (key === 'feeddetails') {
    CName = 'FeedDetails';
  }
  else if (key === 'feedpost') {
    CName = 'FeedPostScreen';
  }
  else if (key === 'webview') {
    CName = 'CommonWebView';
  }
  else if (key === 'traHomeFoodNew') {
    CName = 'FoodHomePage';
  }
  else if (key === 'traFoodList') {
    CName = 'FoodListPage';
  }
  else if (key === 'traAddFood') {
    CName = 'FoodAddPage';
  }
  else if (key === 'traFoodDet') {
    CName = 'FoodDetailsPage';
  }
  else if (key === 'traHomeWeight') {
    CName = 'WeightHomePage';
  }
  else if (key === 'traWaterDet') {
    CName = 'WaterHomePage';
  }
  else if (key === 'traWalkDet') {
    CName = 'StepsHomePage';
  }
  else if (key === 'notifi') {
    CName = 'Notifications';
  }
  else if (key === 'traChallenges') {
    CName = 'ChallengesHomePage';
  }
  else if (key === 'traBuySuccess') {
    CName = 'PaymentResultScreen';
  }
  else if (key === 'refer') {
    CName = 'ReferAFriend';
  }
  else if (key === 'traPhotos') {
    CName = 'PhotosHome';
  }
  else if (key === 'myOrders') {
    CName = 'MySubscriptions';
  }
  else if (key === 'myProfile') {
    CName = 'MyProfile';
  }
  else if (key === 'myProfUpdate') {
    CName = 'MyProfileUpdate';
  }
  else if (key === 'myProfBasicInfo') {
    CName = 'MyProfileUpdateBasicInfo';
  }
  else if (key === 'myProSubGoal') {
    CName = 'MyProfileSubGoal';
  }
  else if (key === 'myProGoalInfo') {
    CName = 'MyProfileCurrentGoal';
  }
  else if (key === 'settings') {
    CName = 'Settings';
  }
  else if (key === 'setDetail') {
    CName = 'SettingsAboutAndTC';
  }
  else if (key === 'appointments') {
    CName = 'Appointments';
  }
  else if (key === 'appBook') {
    CName = 'AppointmentsBooking';
  }
  else if (key === 'appFreeBook') {
    CName = 'AppointmentsFreeBooking';
  }
  else if (key === 'appSuccess') {
    CName = 'AppointmentSuccess';
  }
  else if (key === 'trackdevices') {
    CName = 'LinkMyTracker';
  }
  else if (key === 'challengedetails') {
    CName = 'ChallengeDetails';
  }
  else if (key === 'traAllBuyPlans') {
    CName = 'AllPlansList';
  } else if (key === 'traAllBuyPlans1') {
    CName = 'AllPlansList';
  }
  else if (key === 'traBuyPlanDet') {
    CName = 'PlanDetails';
  }
  else if (key === 'traBuyPlanDetNew') {
    CName = 'PlanDetails';
  }
  else if (key === 'traBuyPlanModify') {
    CName = 'ModifyPlans';
  }
  else if (key === 'trainersList') {
    CName = 'TrainersList';
  }
  else if (key === 'barcodeScan') {
    CName = 'FoodBarcodeScan';
  }
  else if (key === 'traineechallengWinners') {
    CName = 'ChallengeWinners';
  }
  else if (key === 'traDietWorkQues') {
    CName = 'DietWorkQuestions';
  }
  else if (key === 'aidietques') {
    CName = 'AIDietPlanQuestions';
  }
  else if (key === 'traSelPayMethod') {
    CName = 'Trainee_BuyPlanPayMethod';
  }
  else if (key === 'chaRewards') {
    CName = 'ChallengeRewardRedemption';
  }
  else if (key === 'accChallenge') {
    CName = 'AcceptedChallengeDetails';
  }
  else if (key === 'traHelp') {
    CName = 'HelpHome';
  }
  else if (key === 'traHelpBook') {
    CName = 'HelpBooking';
  }
  else if (key === 'traHelpSucc') {
    CName = 'HelpBookingSuccess';
  }
  else if (key === 'rewardHome') {
    CName = 'RewardsHome';
  }
  else if (key === 'rewardRedeem') {
    CName = 'RewardRedemption';
  }
  else if (key === 'afPlayer') {
    CName = 'AFVideoPlayerIOS';
  }
  else if (key === 'customPlayer') {
    CName = 'CustomVideoPlayerAndroid';
  }
  else if (key === 'videoplay') {
    CName = 'VideoPlayer';
  }
  else if (key === 'bodymeasure') {
    CName = 'BodyMeasurements';
  }
  else if (key === 'bodymeasureHome') {
    CName = 'BodyMeasurementsHome';
  }
  else if (key === 'myMdlReds') {
    CName = 'MyMedicalRecords';
  }
  else if (key === 'myMdlRedsAdd') {
    CName = 'MyMedicalRecordsAdd';
  }
  else if (key === 'traCalBurned') {
    CName = 'CaloriedBurned';
  }
  else if (key === 'traFoodPhotos') {
    CName = 'FoodPhotos';
  }
  else if (key === 'pcfdetails') {
    CName = 'PCFDetails';
  }
  else if (key === 'oneononechat') {
    CName = 'OneOnOneChat';
  }
  else if (key === 'chatlist') {
    CName = 'ChatList';
  }
  else if (key === 'traFormlist') {
    CName = 'TraineefitnessformsList';
  }
  else if (key === 'traCreateFood') {
    CName = 'TraineeCreateFood';
  }
  else if (key === 'dietHome1') {
    CName = 'TraineeDietHome';
  }
  else if (key === 'play') {
    CName = 'PlayVideo';
  }
  else if (key === 'newvideoplay') {
    CName = 'NewVideoPlayer';
  }
  else if (key === 'gymVersion') {
    CName = 'GymVersion';
  }
  else if (key === 'shortVideoPlayer') {
    CName = 'ShortVideoPlayer';
  }
  else if (key === 'mentList') {
    CName = 'MentalHealthList';
  }
  else if (key === 'mentDetails') {
    CName = 'MentalHealthDetails';
  }
  else if (key === 'orderFood') {
    CName = 'OrderFoodList';
  }
  else if (key === 'trackershome') {
    CName = 'TrackersHomePage';
  }
  else if (key === 'disHomePage') {
    CName = 'DiscoverHomePage';
  }
  else if (key === 'calorieCounterSetup'){
    CName = 'CalorieCounterSetupPage';
  }
  else if (key === 'appIntroSliderDemo'){
    CName = 'AppIntroductionSliderPage';
  }
  return CName;
}

export default Routers;
