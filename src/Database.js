import SQLite from "react-native-sqlite-storage";
SQLite.DEBUG(true);
SQLite.enablePromise(true);

const database_name = "Reactoffline.db";
const database_version = "1.0";
const database_displayname = "SQLite React Offline Database";
const database_size = 200000;

import LogUtils from './utils/LogUtils.js';

export default class Database {

  initDB() {
    let db;
    return new Promise((resolve) => {
      SQLite.echoTest()
        .then(() => {
          LogUtils.infoLog("Opening database ...");
          SQLite.openDatabase(
            database_name,
            database_version,
            database_displayname,
            database_size
          )
            .then(DB => {
              db = DB;
              LogUtils.infoLog("Database OPEN");
              db.executeSql('SELECT 1 FROM table_notifications LIMIT 1').then(() => {
                LogUtils.infoLog("Database is ready ... executing query ...");
              }).catch((error) =>{
                LogUtils.infoLog1("Received error: ", error);
                LogUtils.infoLog("Database not yet ready ... populating data");
                  db.transaction((tx) => {
                      tx.executeSql('CREATE TABLE IF NOT EXISTS table_notifications (id, title, body, image, type, typeId,key)');
                  }).then(() => {
                    LogUtils.infoLog("Table created successfully");
                  }).catch(error => {
                    LogUtils.infoLog(error);
                  });
              });
              resolve(db);
            })
            .catch(error => {
              LogUtils.infoLog(error);
            });
        })
        .catch(error => {
          LogUtils.infoLog("echoTest failed - plugin not functional");
        });
      });
  };

  closeDatabase(db) {
    if (db) {
      db.close()
        .then(status => {
          LogUtils.infoLog("Database CLOSED");
        })
        .catch(error => {
          LogUtils.infoLog(error);
        });
    } else {
      LogUtils.infoLog("Database was not OPENED");
    }
  };

  listNotifications() {
    return new Promise((resolve) => {
      const notifications = [];
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql('SELECT DISTINCT p.title, p.body, p.image FROM table_notifications p', []).then(([tx,results]) => {
            var len = results.rows.length;
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              // console.log(`Noti ID: ${row.id}, Title: ${row.title}`)
              const { id, title, body,image,type, typeId} = row;
              notifications.push({
                id,
                title,
                body,
                image,
                type,
                typeId
              });
            }
            // console.log(notifications);
            resolve(notifications);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          LogUtils.infoLog(err);
        });
      }).catch((err) => {
        LogUtils.infoLog(err);
      });
    });  
  }

  notificationById(id) {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql('SELECT * FROM table_notifications WHERE key = ?', [id]).then(([tx,results]) => {
          // LogUtils.infoLog(results);
            if(results.rows.length > 0) {
              let row = results.rows.item(0);
              resolve(row);
            }
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          LogUtils.infoLog(err);
        });
      }).catch((err) => {
        LogUtils.infoLog(err);
      });
    });  
  }

  addProduct(id, title, body, image, type, typeId,key) {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql('INSERT INTO table_notifications VALUES (?, ?, ?, ?, ?, ?, ?)', [id, title, body, image, type, typeId,key]).then(([tx, results]) => {
            resolve(results);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          LogUtils.infoLog(err);
        });
      }).catch((err) => {
        LogUtils.infoLog(err);
      });
    });  
  }

  updateProduct(id, title, body, image, type, typeId) {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql('UPDATE table_notifications SET title = ?, body = ?, image = ?, type = ?, typeId = ?, key = ? WHERE prodId = ?', [title, body, image, type, typeId,key,id]).then(([tx, results]) => {
            resolve(results);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          LogUtils.infoLog(err);
        });
      }).catch((err) => {
        LogUtils.infoLog(err);
      });
    });  
  }

  deleteNotifications() {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql('DELETE FROM table_notifications').then(([tx, results]) => {
            // LogUtils.infoLog(results);
            resolve(results);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          LogUtils.infoLog(err);
        });
      }).catch((err) => {
        LogUtils.infoLog(err);
      });
    });  
  }
}