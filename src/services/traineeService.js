
import Services from './httpServices';

module.exports = {

    
    // Used to get trainee details
    getHomepageDetails(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/trainee/homedet2`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    // Used to get trending workout details
    getTrendingWorkouts(BASE_URL,token) {
        return new Promise((resolve,reject)=>{
            Services.httpGet(`${BASE_URL}/trainee/gethometrndworkout`,token).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    // Used to get ecommerce product details
    getProducts(BASE_URL,token) {
        return new Promise((resolve,reject)=>{
            Services.httpGet(`${BASE_URL}/trainee/products`,token).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    // Used to get trackers details
    getTrackersData(BASE_URL,token) {
        return new Promise((resolve,reject)=>{
            Services.httpGet(`${BASE_URL}/trainee/trackerdata`,token).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    
    // Used to save intake water details
    saveWaterIntakeDetails(BASE_URL,token) {
        return new Promise((resolve,reject)=>{
            Services.httpGet(`${BASE_URL}/trainee/waterdet`,token).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    // Used to get the trainee last one wk food details
    getTraineeLastWeekFoodDetails(BASE_URL,token,selDate) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/trainee/getlastweekfooddet`,token,selDate).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    // Used to delete the selected food details in trainee weekly diet
    deleteTraineeFoodItem(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/trainee/deleteuserfood`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

     // Used to delete the selected food details in trainee weekly diet
     getSelFoodItemDetails(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/trainee/getfooditemdetail`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },
    
    // Used to delete the selected food details in trainee weekly diet
    plansCheckCount(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/trainee/logplppclick`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

     // Used to get ActiveLevels
     GetGoalsubctg(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/master/goalsubctg`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

    saveCalorieCounterSetup(BASE_URL,token,params) {
        return new Promise((resolve,reject)=>{
            Services.httpPost(`${BASE_URL}/user/updategoaldet2`,token,params).then((response)=>{
                resolve(response);
            },(error)=>{
                reject(error);
            })
        })
    },

}