
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

module.exports = {
    httpPost: function (uri, token, data) {
        return new Promise((resolve, reject) => {
            try {
                fetch(uri,
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                        },
                        body: JSON.stringify(data),
                    },
                )
                    .then(processResponse)
                    .then(res => {
                        resolve(res);
                    }).catch(function (error) {
                        reject(error);
                    })
            }
            catch (error) {
                reject(error);
            }
        })
    },

    httpGet: function (uri, token) {
        return new Promise((resolve, reject) => {
            try {
                fetch(
                    uri,
                    {
                        method: 'GET',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                        },
                        //body: JSON.stringify(data),
                    },
                )
                    .then(processResponse)
                    .then(res => {
                        resolve(res);
                    }).catch(function (error) {
                        reject(error);
                    })
            }
            catch (error) {
                reject(error);
            }
        })
    }
}