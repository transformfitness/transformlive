import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import ProfileReducer from './ProfileReducer';
import MasterReducer from './MasterReducer';
import FeedReducer from './FeedReducer';
import traineeReducer from './traineeReducer';

export default combineReducers({
  auth: AuthReducer,
  procre: ProfileReducer,
  masters: MasterReducer,
  feed: FeedReducer,
  traineeFood:traineeReducer
});
