import {
    TRACKERSDATA,
    TRAINEE_LASTWK_FOOD_DET,
    TRAINEE_SELECTED_FOOD_DETAILS,
    TRAINEE_FOOD_DELETE_STATUS,
    SET_SLIDERINDEX,
    SETTRAINEEDETAILS,
    SETTRENDINGWORKOUTS,
    ECMRSPRODUCTS,
    GOALSUBCTG,
    CAL_COUNTER_SETUP_STATUS
} from '../actions/types';

const INITIAL_STATE = {
    traineeDetails: {},
    eComProducrs: [],
    trendingWorkouts: [],
    trackersData: {},
    traineeLastWkFood: '',
    message: '',
    selectedFoodDetails: {},
    selectedFoodDelstatus: false,
    slideIndex: 0,
    goalSubCtgs: [],
    calCounterSetup:{},
    calCounterSetupStatus:false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TRACKERSDATA:
            return {
                ...state,
                trackersData: action.payLoad.data,
            }
        case TRAINEE_LASTWK_FOOD_DET:
            return {
                ...state,
                traineeLastWkFood: action.payLoad.data,
                message: action.payLoad.message,
            };
        case TRAINEE_SELECTED_FOOD_DETAILS:
            return {
                ...state,
                selectedFoodDetails: action.payLoad.data,
            };
        case TRAINEE_FOOD_DELETE_STATUS:
            return {
                ...state,
                selectedFoodDelstatus: action.payLoad.data,
            };
        case SET_SLIDERINDEX:
            return {
                ...state,
                slideIndex: action.payLoad,
            }
        case SETTRAINEEDETAILS:
            return {
                ...state,
                traineeDetails: action.payLoad.data,
            };
        case SETTRENDINGWORKOUTS:
            return {
                ...state,
                trendingWorkouts: action.payLoad.workouts,
            };
        case ECMRSPRODUCTS:
            return {
                ...state,
                eComProducrs: action.payLoad.data,
            };

        case GOALSUBCTG:

            return {
                ...state,
                goalSubCtgs: action.payLoad.data,
                calCounterSetup: {

                    bmi_ideal: action.payLoad.bmi_ideal,
                    bmi_max: action.payLoad.bmi_max,
                    bmi_max_msg: action.payLoad.bmi_max_msg,
                    bmi_min: action.payLoad.bmi_min,
                    bmi_min_msg: action.payLoad.bmi_min_msg,
                    
                    fwg_cals: action.payLoad.fwg_cals,
                    fwl_cals: action.payLoad.fwl_cals,
                    mwg_cals: action.payLoad.mwg_cals,
                    mwl_cals: action.payLoad.mwl_cals,
                    wg_bad: action.payLoad.wg_bad,
                    wg_good: action.payLoad.wg_good,
                    wl_bad: action.payLoad.wl_bad,
                    wl_good: action.payLoad.wl_good,
                    wg_cals_check: action.payLoad.wg_cals_check,
                    wl_cals_check: action.payLoad.wl_cals_check,

                }
            };
            case CAL_COUNTER_SETUP_STATUS:
                return {
                    ...state,
                    calCounterSetupStatus: action.payLoad,
                };
        default:
            return state;
    }
};
