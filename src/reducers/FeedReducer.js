import { 
  GET_TRAINEE_FEEDS, 
  GET_TRAINEE_FEEDS_SUCCESS, 
  GET_TRAINEE_FEEDS_FAILURE,GET_TRAINEE_FEEDS_LOADMORE, 
  GET_TRAINEE_FEEDS_LOADMORE_SUCCESS, 
  GET_TRAINEE_FEEDS_LOADMORE_FAILURE,
  SAVE_COMMENT,SAVE_COMMENT_SUCCESS, SAVE_COMMENT_FAILURE, 
  SAVE_LIKE, SAVE_POLL, SAVE_POLL_SUCCESS, SAVE_POLL_FAILURE, 
  SAVE_LIKE_SUCCESS, SAVE_LIKE_FAILURE,
  SAVE_FEED, SAVE_FEED_SUCCESS, SAVE_FEED_FAILURE,PROGRESS_BAR_CHANGED,PAGENO_CHANGED,
  SAVE_FOLLOW,SAVE_FOLLOW_SUCCESS,SAVE_FOLLOW_FAILURE,} from '../actions/types';  
  const INITIAL_STATE = {
      error: '',
      loading: false,
      feeds: [],
      poll: [],
      pageNo: 1,
      refreshing: false,
      isProgress: false,
      progress: 10,
    };
  
    export default (state = INITIAL_STATE, action) => {
      switch (action.type) {

        case PROGRESS_BAR_CHANGED:
        return {...state, error: '', progress: action.payLoad};

        case PAGENO_CHANGED:
        return {
          ...state,
          feeds: null,
          pageNo: 1,
        };
        
        case GET_TRAINEE_FEEDS:
          return {
            ...state,
            loading: true,
            refreshing: false,
            error: '',
          };
        case GET_TRAINEE_FEEDS_FAILURE:
          return {
            ...state,
            loading: false,
            refreshing: false,
            error: action.payload,
          };
        case GET_TRAINEE_FEEDS_SUCCESS:
          return {
            ...state,
            feeds: action.payload,
            loading: false,
            pageNo: state.pageNo + 1,
            refreshing: false,
            error: '',
          };
        case GET_TRAINEE_FEEDS_LOADMORE:
            return {
              ...state,
              loading: false,
              refreshing: true,
              error: '',
            };
        case GET_TRAINEE_FEEDS_LOADMORE_SUCCESS: {
          // let workDetails = this.state.workDetails;
          // let data = workDetails.concat(action.payload.items); //concate list with response
          return {
            ...state,
            feeds: [...state.feeds, ...action.payload],
            loading: false,
            pageNo: state.pageNo + 1,
            refreshing: false,
            error: '',
          };
        }

          case SAVE_COMMENT:
          return {
            ...state,
            loading: false,
            error: '',
          };
          case SAVE_COMMENT_SUCCESS:
          return {
            ...state,
            message: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_COMMENT_FAILURE:
          return {
            ...state,
            loading: false,
            error: action.payload,
          };
          case SAVE_LIKE:
          return {
            ...state,
            loading: false,
            error: '',
          };
          case SAVE_LIKE_SUCCESS:
          return {
            ...state,
            message: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_LIKE_FAILURE:
          return {
            ...state,
            loading: false,
            error: action.payload,
          };
          case SAVE_POLL:
          return {
            ...state,
            poll: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_POLL_SUCCESS:
          return {
            ...state,
            message: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_POLL_FAILURE:
          return {
            ...state,
            loading: false,
            error: action.payload,
          };

          case SAVE_FOLLOW:
          return {
            ...state,
            poll: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_FOLLOW_SUCCESS:
          return {
            ...state,
            message: action.payload,
            loading: false,
            error: '',
          };
          case SAVE_FOLLOW_FAILURE:
          return {
            ...state,
            loading: false,
            error: action.payload,
          };

          case SAVE_FEED:
          return {
            ...state,
            loading: true,
            error: '',
          };
          case SAVE_FEED_SUCCESS:
          return {...state, message: action.payload, error: '', loading: false};

          case SAVE_FEED_FAILURE:
          return {...state, error: action.payload, loading: false};
    
        default:
          return state;
      }
    };
    