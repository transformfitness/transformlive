import {
  PROFILE_REFRESH,
  GET_TRAINEE_GOALS,
  GET_TRAINEE_GOALS_SUCCESS,
  GET_TRAINEE_GOALS_FAILURE,
  GET_TRAINER_CODES,
  GET_TRAINER_CODES_SUCCESS,
  GET_TRAINER_CODES_FAILURE,
  SAVE_PROFILE,
  SAVE_PROFILE_SUCCESS,
  SAVE_PROFILE_FAILURE,
  GET_PROFILE_DET,
  GET_PROFILE_DET_SUCCESS,
  UPDATE_PROFILE_DET_LIST,
  UPDATE_PROFILE_DET_SUCCESS,
  UPDATE_PROFILE_BASIC_INFO,
  UPDATE_PROFILE_BASIC_INFO_SUCCESS,
  GET_WEIGHT_DET,
  GET_WEIGHT_DET_SUCCESS,
  SAVE_PROFILE_PIC,
  SAVE_PROFILE_PIC_SUCCESS,
} from '../actions/types';
const INITIAL_STATE = {
  error: '',
  loading: false,
  goals: [],
  trainerCodes: [],
  profdet: {},
  sucMsg: '',
  isSuccess: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PROFILE_REFRESH:
      return {
        ...state,
        isSuccess: false,
        sucMsg: '',
        error: '',
        loading: false,
      };
    case GET_TRAINEE_GOALS:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_TRAINEE_GOALS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case GET_TRAINEE_GOALS_SUCCESS:
      return {
        ...state,
        goals: action.payload,
        loading: false,
        error: '',
      };
    case GET_TRAINER_CODES:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_TRAINER_CODES_SUCCESS:
      return {
        ...state,
        trainerCodes: action.payload,
        loading: false,
        error: '',
      };
    case GET_TRAINER_CODES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case SAVE_PROFILE:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SAVE_PROFILE_SUCCESS:
      return {
        ...state,
        message: action.payload,
        loading: false,
        error: '',
        sucMsg: action.payload,
        isSuccess: true,
      };
    case SAVE_PROFILE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case GET_PROFILE_DET:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_PROFILE_DET_SUCCESS:
      return {
        ...state,
        profdet: action.payload,
        loading: false,
        error: '',
      };
    case UPDATE_PROFILE_DET_LIST:
      return {
        ...state,
        loading: true,
        error: '',
        sucMsg: '',
        isSuccess: false,
      };
    case UPDATE_PROFILE_DET_SUCCESS:
      return {
        ...state,
        sucMsg: action.payload,
        loading: false,
        error: '',
        sucMsg: action.payload,
        isSuccess: true,
      };

    case UPDATE_PROFILE_BASIC_INFO:
      return {
        ...state,
        loading: true,
        error: '',
        sucMsg: '',
        isSuccess: false,
      };
    case UPDATE_PROFILE_BASIC_INFO_SUCCESS:
      return {
        ...state,
        sucMsg: action.payload,
        loading: false,
        error: '',
        sucMsg: action.payload,
        isSuccess: true,
      };
    case GET_WEIGHT_DET:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_WEIGHT_DET_SUCCESS:
      return {
        ...state,
        profdet: action.payload,
        loading: false,
        error: '',
      };
    case SAVE_PROFILE_PIC:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SAVE_PROFILE_PIC_SUCCESS:
      return {
        ...state,
        message: action.payload,
        loading: false,
        error: '',
        // sucMsg: action.payload,
        // isSuccess: true,
      };

    default:
      return state;
  }
};
