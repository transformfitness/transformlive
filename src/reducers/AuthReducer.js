import {
  INTERNETSTATUS,
  MOBILE_CHANGED,
  GET_OTP,
  GET_OTP_FAIL,
  GET_OTP_SUCCESS,
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAIL,
} from '../actions/types';
const INITIAL_STATE = {
  error: '',
  loading: false,
  token: '',
  mobileNumber: '',
  internetStatus:{}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INTERNETSTATUS:
      return {...state, internetStatus: action.payLoad};
    case MOBILE_CHANGED:
      return {...state, error: '', mobileNumber: action.payLoad};
    case GET_OTP:
      return {...state, loading: true, error: ''};
    case GET_OTP_SUCCESS:
      return {...state, loading: false, error: '', token: ''};
    case GET_OTP_FAIL:
      return {
        ...state,
        error: action.payLoad,
        loading: false,
      };
    case USER_LOGIN:
      return {...state, loading: true, error: ''};
    case USER_LOGIN_SUCCESS:
      return {...state, ...INITIAL_STATE, token: action.payLoad};
    case USER_LOGIN_FAIL:
      return {
        ...state,
        error: action.payLoad,
        password: '',
        loading: false,
      };
    default:
      return state;
  }
};
